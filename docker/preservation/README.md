Containerized setup for the CASTOR central servers for preservation purposes
===

This container runs the CASTOR nsd and vmgrd daemons connected to the legacy
CASTOR central databases, to help consult, respectively, the state of the files
namespace and the tape volumes archive, at the time of their migration to CTA.

For the former, you can use the ns* command-line tools, and for the latter,
you can use the vmgr* command-line tools, all available in this container.
The legacy cupvd (User Privileges daemon) has been disabled in this setup.

The container is available for use in the `eoscta-operations` Gitlab project,
and can be spinned up with:

```
podman run -it gitlab-registry.cern.ch/cta/eoscta-operations/registry/container_registry/castor:production
```

