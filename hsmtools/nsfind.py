#!/usr/bin/python2
#/******************************************************************************
# *                      nsfind.py
# *
# * This file is part of the Castor project.
# * See http://castor.web.cern.ch/castor
# *
# * Copyright (C) 2003  CERN
# * This program is free software; you can redistribute it and/or
# * modify it under the terms of the GNU General Public License
# * as published by the Free Software Foundation; either version 2
# * of the License, or (at your option) any later version.
# * This program is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU General Public License for more details.
# * You should have received a copy of the GNU General Public License
# * along with this program; if not, write to the Free Software
# * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# *
# *
# * @author Castor Dev team, castor-dev@cern.ch
# *****************************************************************************/

'''
Finds files in the CASTOR namespace going directly to the DB.

Originally contributed by RAL, heavily refactored to compute the size like du.
Recent additions: prints the fileclass of each entry as well as the min-max atime
and min-max mtime calculated among each path's contents.
'''

import os
import re
import sys
import getopt
import castor_tools
import ConfigParser
import argparse
import errno

fmtstring = '%4d %6d %5d  %8d %18d  %10d-%10d %10d-%10d  %s'

# generate the recursive query to the Namespace
def nsfind_query(path):
    if path[0] != '/':
        # relative path
        print('Non-absolute path %s not supported, giving up.' % path)
        sys.exit(errno.EINVAL)
    # Remove // and final / if present
    path = re.sub('[/]+', '/', path)
    if path[-1] == '/':
        path = path[0:-1]
    if path == '':
        # a very special case to work around the Oracle error
        # ORA-30004: when using SYS_CONNECT_BY_PATH function, cannot have separator as part of column value
        path = '/castor'
    bpath = path[:path.rfind('/')]
    dirs = path.split("/")
    del dirs[0]
    # get the starting fileid
    startid = "(SELECT fileid FROM Cns_file_metadata WHERE parent_fileid=0)"   # top level '/' has parent = 0
    for d in dirs:
        startid = "(SELECT fileid FROM Cns_file_metadata WHERE name='" + d + "' and parent_fileid=" + startid + ")"
    columns = "fileclass, owner_uid, gid, atime, mtime"    # filemode, checksum...
    # build the query
    query = "SELECT fileid, parent_fileid, filesize, decode(bitand(filemode, 4*8*8*8*8), 16384, 1, 0) isdir, " + \
            "'" + bpath + "' || SYS_CONNECT_BY_PATH(name, '/') path, " + columns + \
            " FROM Cns_file_metadata START WITH fileid=" + startid + " CONNECT BY NOCYCLE PRIOR fileid = parent_fileid"
    return query

# generic function to iterate over an iterable keeping track of the next item at each loop
def neighborhood(iterable):
    iterator = iter(iterable)
    #prev = None
    item = iterator.next()
    for next in iterator:
        #yield (prev,item,next)
        yield (item, next)
        #prev = item
        item = next
    #yield (prev,item,None)
    yield (item, None)


# parse the arguments
parser = argparse.ArgumentParser()
parser.add_argument('directory')
parser.add_argument('-type', choices=['d','f'], help='filter by file type', dest='type')
#parser.add_argument('--checksum', dest='checksum', action='store_const', const = True, default=False, help='Returns checksums of the files')
args = parser.parse_args()

# dictionaries to keep track of dirs metadata and their size
dirs = {}
dsizes = {}
dct = {}
dminatime = {}
dmaxatime = {}
dminmtime = {}
dmaxmtime = {}
nsconn = castor_tools.connectToNS()
nscur = nsconn.cursor()
nscur.arraysize = 10000
# build and execute the query
query = nsfind_query(path=args.directory)
nscur.execute(query)
# process the results to compute the total sizes
for row, next in neighborhood(nscur):
    fileid, parent, filesize, isdir, path, fc, uid, gid, atime, mtime = row
    if fc is None:
        fc = 0
    if not dirs and next is None:
        # special handling for single entry result
        if isdir:
            if args.type != 'f':
                print(fmtstring % (fc, uid, gid, 1, 0, atime, atime, mtime, mtime, path))
        else:
            print('Path %s is not a directory' % args.directory)
            sys.exit(errno.ENOTDIR)
        continue
    if isdir:
        # a new directory starts
        dirs[fileid] = (parent, fc, uid, gid, path)
        dsizes[fileid] = 0
        dct[fileid] = 1
        dminatime[fileid] = atime
        dmaxatime[fileid] = atime
        dminmtime[fileid] = mtime
        dmaxmtime[fileid] = mtime
        if next is None or next[1] != fileid:    # next[1] = next file's parent
            # empty directory, print it straight
            if args.type != 'f':
                print(fmtstring % (fc, uid, gid, 1, 0, atime, atime, mtime, mtime, path))
            dct[parent] += 1
    else:
        # print the file and update its parent dir's size
        if args.type != 'd':
            print(fmtstring % (fc, uid, gid, 1, filesize, atime, atime, mtime, mtime, path))
        dsizes[parent] += filesize
        dct[parent] += 1
        dminatime[parent] = min(dminatime[parent], atime)
        dmaxatime[parent] = max(dmaxatime[parent], atime)
        dminmtime[parent] = min(dminmtime[parent], mtime)
        dmaxmtime[parent] = max(dmaxmtime[parent], mtime)
    # check for termination of current path
    if next == None or next[4].count('/') < path.count('/'):
        currid = parent
        nextdir = '/' if next == None else next[4][:next[4].rfind('/')]
        sizetoupd = cttoupd = 0
        while currid in dirs and dirs[currid][4] != nextdir:
            # we are at the end of a directory, update all counters to the top
            dsizes[currid] = dsizes[currid] + sizetoupd
            dct[currid] = dct[currid] + cttoupd
            sizetoupd = dsizes[currid]
            cttoupd = dct[currid]
            # and print the directories up to where we'll be next
            if args.type != 'f':
                print(fmtstring % (dirs[currid][1], dirs[currid][2], dirs[currid][3], dct[currid], dsizes[currid], \
                                   dminatime[currid], dmaxatime[currid], dminmtime[currid], dmaxmtime[currid], dirs[currid][4]))
            currid = dirs[currid][0]   # go to its parent
        # update the parent with the just completed directory's figures
        if currid in dirs:
            dsizes[currid] = dsizes[currid] + sizetoupd
            dct[currid] = dct[currid] + cttoupd
nscur.close()
if 'row' not in locals():
    print('Path %s not found in the Namespace' % args.directory)
    sys.exit(errno.ENOENT)
