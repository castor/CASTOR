.TH NSUNDELETE "1castor" "2016" CASTOR "namespace administrative commands"
.SH NAME
nsundelete \- restore deleted files in the CASTOR namespace from the logs
.SH SYNOPSIS
.B nsundelete
[
.BI -h
]
[
.BI <filelist>
]
[
.BI <nsdlogfile>
]
.SH DESCRIPTION
.B nsundelete
parses the provided nsd log file and restores the deleted files from filelist found in the log. The principle is that a merged log file shall be provided by extracting the relevant information from the production nameserver logs, including at minimum the log lines resulting from the unlink operation.
Log lines referring to files not in the <filelist> or to other operations are ignored. If the log is not complete (e.g. the segment information is missing), the restore fails and an error is printed. Likewise if an Oracle error takes place when reinserting the metadata (e.g. because the parent directory is missing or the fileid is already used). Only if all the information is correctly found are the files restored in the namespace.
.TP
.BI \-h,\ \-\-help
Get usage information
.TP
.BI <filelist>
file containing a list of CASTOR file names to be undeleted.
.BI <nsdlogfile>
name of a log file extracted from the nsd logs.

.SH EXAMPLES
.nf
.ft CW
# nsundelete list.txt nsdtorestore.log
Parsing the nsd log...
Found 1 file(s) to be recovered and 1 file(s) referenced in the log. Starting the recovery in the Namespace...
Successfully restored /castor/cern.ch/user/i/itglp/tape/testrestore

Completed 1 file(s). Please try and stage them in to validate their restore:
stager_get -M /castor/cern.ch/user/i/itglp/tape/testrestore

# nsundelete list.txt nsdtorestore.log   # second attempt
Parsing the nsd log...
Found 1 file(s) to be recovered and 1 file(s) referenced in the log. Starting the recovery in the Namespace...
Failed to restore /castor/cern.ch/user/i/itglp/tape/testrestore : ORA-20000: A file already exists with fileid 1548587160

Warning: the following files could not be recovered:
/castor/cern.ch/user/i/itglp/tape/testrestore

.SH NOTES
This command requires database client access to the Stager and the Nameserver DBs.

.SH SEE ALSO
.BR nslistsegs
.BR fixfilesize

.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
