.TH PRINTFILECLASS "1castor" "2011" CASTOR "Prints out the given file class(es)"
.SH NAME
printfileclass
.SH SYNOPSIS
.B printfileclass
[
.BI -h
]
<fileClassName>
[...]


.SH DESCRIPTION
.B printfileclass
prints out definitions of the given file classes, according to the stager database
.LP
.BI \-h,\ \-\-help
Get usage information
.TP
.BI <fileClassName>
when file class names are given, only the definitions of the listed file class are printed.
If no file class name is given, all file class definitions are printed.

.SH EXAMPLES
.nf
.ft CW
# printfileclass
           NAME CLASSID NBCOPIES           ID
---------------------------------------------
           temp      58        0            2
      largeuser      95        1            3
threeCopyOnTape      96        3            4
          test2      27        2     88801474

# printfileclass temp test2 nonexisting
 NAME CLASSID NBCOPIES           ID
-----------------------------------
 temp      58        0            2
test2      27        2     88801474
WARNING : the following file classes do not exist : nonexisting

.SH NOTES
This command requires database client access to the stager catalogue.
Configuration for the database access is taken from castor.conf.

.SH SEE ALSO
.BR enterfileclass
.BR deletefileclass
.BR printmigrationroute
.BR adminMultiInstance

.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
