#!/usr/bin/python
#/******************************************************************************
# *                      deletediskserver
# *
# * This file is part of the Castor project.
# * See http://castor.web.cern.ch/castor
# *
# * Copyright (C) 2003  CERN
# * This program is free software; you can redistribute it and/or
# * modify it under the terms of the GNU General Public License
# * as published by the Free Software Foundation; either version 2
# * of the License, or (at your option) any later version.
# * This program is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU General Public License for more details.
# * You should have received a copy of the GNU General Public License
# * along with this program; if not, write to the Free Software
# * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# *
# * @author Castor Dev team, castor-dev@cern.ch
# *****************************************************************************/

'''command line deleting the diskserver(s)'''

import sys
import getopt
import castor_tools

# usage function
def usage(exitCode):
    '''prints usage'''
    print 'Usage : ' + sys.argv[0] + ' [-h|--help] <diskServerName> [...]'
    sys.exit(exitCode)

# first parse the options
try:
    options, args = getopt.getopt(sys.argv[1:], 'hv', ['help', 'verbose'])
except Exception, e:
    print e
    usage(1)
verbose = False
for f, v in options:
    if f == '-h' or f == '--help':
        usage(0)
    elif f == '-v' or f == '--verbose':
        verbose = True
    else:
        print "unknown option : " + f
        usage(1)

# Deal with arguments
if len(args) != 0:
    diskServers = set(args)
else:
    print "Missing arguments"
    usage(1)

try:
    # connect to stager
    stconn = castor_tools.connectToStager()
    stcur = stconn.cursor()
    # check disk server existence
    sqlStatement = 'SELECT id FROM DiskServer WHERE name=:dsname'
    existingDiskServers = set([])
    diskServerIds = []
    for diskServer in diskServers:
        stcur.execute(sqlStatement, dsname=diskServer)
        row = stcur.fetchone()
        if row:
            diskServerIds.append(row[0])
            existingDiskServers.add(diskServer)
    nonexitingDiskServers = diskServers - existingDiskServers
    if not diskServerIds:
        print 'None of the provided diskserver could be found. Giving up.'
        usage(1)
    # check that the disk servers are empty
    stcur.execute('''SELECT UNIQUE DiskServer.name
                       FROM FileSystem, DiskServer
                      WHERE FileSystem.diskServer = DiskServer.id
                        AND FileSystem.diskserver IN (''' + ', '.join([str(id) for id in diskServerIds]) + ''')
                        AND EXISTS (SELECT /*+ INDEX(DiskCopy I_DiskCopy_FileSystem) */ 1
                                      FROM DiskCopy
                                     WHERE DiskCopy.fileSystem = FileSystem.id)''')
    rows = stcur.fetchall()
    if len(rows) > 0:
        print 'Some diskServers cannot be dropped as they still contain files : %s' % \
              ', '.join([dsname for dsname, in rows])
        print 'You may want to drain them to correct this'
        sys.exit(1)
    # drop filesystems and diskservers
    stcur.execute('DELETE FROM FileSystem WHERE diskServer IN (' + \
                  ', '.join([str(id) for id in diskServerIds]) + ')')
    stcur.execute('DELETE FROM DiskServer WHERE id IN (' + ', '.join([str(id) for id in diskServerIds]) + ')')
    stconn.commit()
    print 'successfully dropped the following disk servers : ' + ', '.join(existingDiskServers)
    # warn for non existing disk pools
    if nonexitingDiskServers:
        print 'WARNING : some disk server(s) did not exist : ' + ', '.join(nonexitingDiskServers)
    # close DB connections
    try:
        castor_tools.disconnectDB(stconn)
    except Exception:
        pass
except Exception, e:
    print e
    if verbose:
        import traceback
        traceback.print_exc()
    sys.exit(-1)
