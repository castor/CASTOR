.TH "PRINTRECALLSTATUS" "8castor" "2.1.14" "CASTOR" "Castor stager tools"
.SH "NAME"
printrecallstatus \- Prints the pending recall statistics by recall group
.SH "SYNOPSIS"
[CASTOR_STAGER=<stager_name>]
.B 
printrecallstatus
[
.BI \-h
]

.SH "DESCRIPTION"
.B printrecallstatus
prints out pending recall statistics, by recall group. For each recall group, 3 totals are available: the total of data (tapes, files and bytes) to be recalled, the total submitted to VDQM (currently mounted or not) and the total for tapes currently mounted. Then the global numbers are printed. The global numbers are not the sum over all recall groups as a tape can contain files being recalled under various recall groups.
.LP 
.BI \-h,\ \-\-help
Get usage information
.TP 

.SH "EXAMPLES"
.nf 
.ft CW
# printrecallstatus
RECALLGROUP FILES FILES_QUEUED FILES_RECALLING    TOTAL_SIZE  SIZE_QUEUED SIZE_RECALLING TAPES TAPES_QUEUED TAPES_RECALLING MIN_AGE_DAYS MAX_AGE_DAYS
-----------------------------------------------------------------------------------------------------------------------------------------------------
    default   382           92              46  488969357461  81124408398    73020114678    78           15               4         0.00         0.17
    compass   369           10              10  361251774140  10213057448    10213057448   218            5               5         0.00         0.17
        ams   547           25              23  959545280560 127322346400   126211579592    91           10               8         0.00         0.17
-----------------------------------------------------------------------------------------------------------------------------------------------------
        ALL  1298          127              79 1809766412161 218659812246   209444751718   381           28              15         0.00         0.17


.SH "NOTES"
This command requires database client access to the stager catalogue.
Configuration for the database access is taken from ORASTAGERCONFIG.

.SH "SEE ALSO"
.BR printmigrationstatus
.BR printrecallfilestatus
.BR printrecalltapequeue
.BR printrecalluserqueue
.SH "AUTHOR"
\fBCASTOR\fP Team <castor.support@cern.ch>
