.TH ENTERPOOL "1castor" "2011" CASTOR "stager catalog administrative commands"
.SH NAME
enterpool \- enter a new pool in the stager catalog
.SH SYNOPSIS
.B enterpool
[
.BI -h
]
[
.BI -p
<priority>
]
.BI <poolName>
.SH DESCRIPTION
.B enterpool
enters a new disk pool in the CASTOR stager catalog
.TP
.BI \-h,\ \-\-help
Get usage information
.TP
.BI \-p,\ \-\-migrationpriority
Migration priority for this pool. Bigger priority pools are used first for migrations to tape. Default priority is 0
.TP
.BI <poolName>
name of the disk pool to create.

.SH EXAMPLES
.nf
.ft CW
# enterpool -p 10 newdiskpool
inserted disk pool newdiskpool successfully

# printpool newpool
       NAME MIGPRIORITY NBDISKSERVERS  FREE   %FREE  SIZE SVCCLASSES       ID EXTUSER EXTPOOL
---------------------------------------------------------------------------------------------
newdiskpool          10             0     -       -     -          - 93301471       -       -

.SH NOTES
Creation of data pools is handled automatically by the internal monitoring system of CASTOR.
You do not need to take any action for them.

This command requires database client access to the stager catalog and nameserver DBs.
Configuration for the database accesses is taken from castor.conf.

.SH SEE ALSO
.BR deletepool
.BR modifypool
.BR printpool
.BR entersvcclass
.BR adminMultiInstance

.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
