.TH ENTERMIGRATIONROUTE "1castor" "2011" CASTOR "stager catalogue administrative commands"
.SH NAME
entermigrationroute \- define (a) new migration route(s) in the stager catalog
.SH SYNOPSIS
.B entermigrationroute
[
.BI -h
]
<fileClassName>
<copyNb>[:<isSmallFile>]:<tapePoolName>
[\.\.\.]
.SH DESCRIPTION
.B entermigrationroute
enters (a) new migration route(s) in the CASTOR stager catalog.

The routes entered are concerning a given file class and must cover all cases for this file class. For example, if the given file class is configured to have 3 copies on tape, the routes for all three copies must be given in one go. Similarly, if distinction is made between small and big files for a given copy, both routes for small and big files must be given.
.TP
.BI \-h,\ \-\-help
Get usage information
.TP
.BI <fileClassName>
name of the file class concerned by the new route(s)
.TP
.BI <copyNb>
the copy number that this route concerns
.TP
.BI <isSmallFile>
whether the route concerns small files or big files. This parameter can be omitted, meaning that the route applies to all files, whatever their size is.
.TP
.BI <tapePoolName>
name of the destination tape pool for files matching this route

.SH EXAMPLES
.nf
.ft CW
#entermigrationroute default largeuser 1:stager_dev01
inserted route(s) successfully for SvcClass default and FileClass largeuser

#printmigrationroute
FILECLASS COPYNB ISSMALLFILE     TAPEPOOL LASTEDITOR          LASTEDITION
-------------------------------------------------------------------------
largeuser      1           - stager_dev01   sponcec3 31-Aug-2011 10:07:27

#entermigrationroute test2 1:stager_dev01 2:stager_dev02

#printmigrationroute
FILECLASS COPYNB ISSMALLFILE     TAPEPOOL LASTEDITOR          LASTEDITION
-------------------------------------------------------------------------
largeuser      1           - stager_dev01   sponcec3 31-Aug-2011 10:07:27
    test2      1           - stager_dev01   sponcec3 31-Aug-2011 10:10:38
    test2      2           - stager_dev02   sponcec3 31-Aug-2011 10:10:38

#entermigrationroute threeCopyOnTape 1:stager_dev01 2:stager_dev02 3:stager_dev01

#printmigrationroute
      FILECLASS COPYNB ISSMALLFILE     TAPEPOOL LASTEDITOR          LASTEDITION
-------------------------------------------------------------------------------
      largeuser      1           - stager_dev01   sponcec3 31-Aug-2011 10:07:27
          test2      1           - stager_dev01   sponcec3 31-Aug-2011 10:10:38
          test2      2           - stager_dev02   sponcec3 31-Aug-2011 10:10:38
threeCopyOnTape      1           - stager_dev01   sponcec3 31-Aug-2011 10:11:06
threeCopyOnTape      2           - stager_dev02   sponcec3 31-Aug-2011 10:11:06
threeCopyOnTape      3           - stager_dev01   sponcec3 31-Aug-2011 10:11:06

#entermigrationroute filesizebased 1:yes:stager_dev01 1:no:stager_dev02
inserted route(s) successfully for SvcClass dev and FileClass largeuser

#printmigrationroute
      FILECLASS COPYNB ISSMALLFILE     TAPEPOOL LASTEDITOR          LASTEDITION
-------------------------------------------------------------------------------
  filesizebased      1       False stager_dev02   sponcec3 31-Aug-2011 10:13:14
  filesizebased      1        True stager_dev01   sponcec3 31-Aug-2011 10:13:14
      largeuser      1           - stager_dev01   sponcec3 31-Aug-2011 10:07:27
          test2      1           - stager_dev01   sponcec3 31-Aug-2011 10:10:38
          test2      2           - stager_dev02   sponcec3 31-Aug-2011 10:10:38
threeCopyOnTape      1           - stager_dev01   sponcec3 31-Aug-2011 10:11:06
threeCopyOnTape      2           - stager_dev02   sponcec3 31-Aug-2011 10:11:06
threeCopyOnTape      3           - stager_dev01   sponcec3 31-Aug-2011 10:11:06

#entermigrationroute largeuser 1:stager_dev01
Migration route(s) already exist for FileClass largeuser. Please remove first
Giving up

#entermigrationroute temp 1:stager_dev01
CopyNb 1 too large for fileClass temp that has nbCopies=0
Giving up

#entermigrationroute test2 1:stager_dev01
No route for copyNb 2 while nbCopies=2 for fileClass test2.
The migration route is thus incomplete, Giving up

#entermigrationroute test2 1:stager_dev01 1:no:stager_dev02 2:stager_dev02
Colliding routes for copyNb 1 :
  isSmallFile=None -> stager_dev01
  isSmallFile=False -> stager_dev02
Giving up

#entermigrationroute test2 1:yes:stager_dev01 1:no:stager_dev02 1:stager_dev02 2:stager_dev02
Too many routes given for copyNb 1 :
  isSmallFile=True -> stager_dev01
  isSmallFile=False -> stager_dev02
  isSmallFile=None -> stager_dev02
They are thus colliding. Giving up

.SH NOTES
This command requires database client access to the stager catalog DB.
Configuration for the database accesses is taken from castor.conf.

.SH SEE ALSO
.BR modifymigrationroute
.BR deletemigrationroute
.BR printmigrationroute
.BR enterfileclass
.BR entertapepool
.BR adminMultiInstance

.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
