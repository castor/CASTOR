.TH PRINTTAPEPOOL "1castor" "2011" CASTOR "Prints out the given tape pool(s)"
.SH NAME
printtapepool
.SH SYNOPSIS
.B printtapepool
[
.BI -h
]
[
<tapePoolName>
[...]
]

.SH DESCRIPTION
.B printtapepool
prints out definitions of the given tape pools
.LP
.BI \-h,\ \-\-help
Get usage information
.TP
.BI <TapePoolName>
when tape pool names are given, only the definitions of the listed tape pool are printed.
If no tape pool name is given, all tape pool definitions are printed.

.SH EXAMPLES
.nf
.ft CW
# printtapepool
        NAME NBDRIVES MINAMOUNTDATA MINNBFILES MAXFILEAGE ID LASTEDITOR          LASTEDITION
--------------------------------------------------------------------------------------------
 newtapepool        0        100GiB       1000        12h  7   sponcec3 30-Aug-2011 16:32:15
testtapepool        2         23MiB       1000    12h23mn  8   sponcec3 30-Aug-2011 16:42:32

# printtapepool testtapepool nonexisting
        NAME NBDRIVES MINAMOUNTDATA MINNBFILES MAXFILEAGE ID LASTEDITOR          LASTEDITION
--------------------------------------------------------------------------------------------
testtapepool        2         23MiB       1000    12h23mn  8   sponcec3 30-Aug-2011 16:42:32
WARNING : the following tape pools do not exist : nonexisting

.SH NOTES
This command requires database client access to the stager catalogue.
Configuration for the database access is taken from castor.conf.

.SH SEE ALSO
.BR entertapepool
.BR modifytapepool
.BR deletetapepool
.BR printmigrationroute
.BR adminMultiInstance

.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
