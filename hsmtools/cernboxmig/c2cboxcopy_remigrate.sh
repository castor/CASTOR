#!/bin/bash
#/******************************************************************************
# *                      c2cboxcopy_remigrate.sh
# *
# * This file is part of the Castor project.
# * See http://cern.ch/castor and http://cern.ch/eoscta
# * Copyright (C) 2019  CERN
# *
# * This program is free software; you can redistribute it and/or
# * modify it under the terms of the GNU General Public License
# * as published by the Free Software Foundation; either version 2
# * of the License, or (at your option) any later version.
# * This program is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU General Public License for more details.
# * You should have received a copy of the GNU General Public License
# * along with this program; if not, write to the Free Software
# * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# *
# * CASTOR user folders to CERNBox migration
# *
# * This tool runs a namespace scan and executes an xrootd 3rd-party copy
# * of all files from CASTOR to CERNBox for a given user.
# *
# * Assumptions: the tool is run from an EOS gateway (e.g. a Samba box), with
# * `ldapsearch` available as well as krb-based root access to all eoshome
# * head nodes. On the CASTOR side, the castor_tools.py must be available
# * and configured for a direct access to the CASTOR Namespace DB.
# * Finally, a keytab for the stage user must be available in the current dir.
# *
# * @author Castor Dev team, castor-dev@cern.ch
# *****************************************************************************/


export xrdcpfail=`mktemp xrdXXXXX --tmpdir`

print_usage() {
  echo Executes a rerun+check of the migration of a given user\'s data from CASTOR to CERNBox
  echo Usage: $0 --dryrun\|--doit username \[xrdlogfile\]
  echo '       --dryrun:    no copy and no quota changes are executed, but commands are printed on screen'
  echo '       username:    CASTOR user directory without prefix'
  echo '       xrdlogfile:  xrootd logs file, defaults to /var/log/c2cboxcopy_remigrate.log'
  exit 1
}

xrd3cp() {    # args: source, targetname, targetpath, mtime, logiferrors
  xrdlog=`mktemp`
  if [[ "$dryrun" == "--doit" ]]; then
    xrdcp --cksum adler32 --tpc only "root://castorpublic/$1" "root://eoshome-${2:0:1}//eos/user/${2:0:1}/$2/$3?eos.ruid=${ruid}&eos.rgid=${rgid}&eos.mtime=$4&eos.app=castormig" 2> $xrdlog
    rc=$?
    if [[ $rc -eq 54 && $5 -ne 0 ]]; then
      # failure, and the file size is > 0 in CASTOR: for rc=54, just give it another try before failing
      echo -n x >> $log
      sleep 1
      xrdcp --cksum adler32 --tpc only "root://castorpublic/$1" "root://eoshome-${2:0:1}//eos/user/${2:0:1}/$2/$3?eos.ruid=${ruid}&eos.rgid=${rgid}&eos.mtime=$4&eos.app=castormig" 2> $xrdlog
      rc=$?
    fi
  else
    statres=`xrdfs "root://eoshome-${2:0:1}" stat "/eos/user/${2:0:1}/$2/$3?eos.ruid=${ruid}&eos.rgid=${rgid}" 2> $xrdlog`
    rc=$?
  fi
  if [[ $rc -ne 0 && $rc -ne 50 && $5 -ne 0 ]]; then
    # failure (ignoring "cannot overwrite" errors), and the file size is > 0 in CASTOR:
    # keep the logs for further analysis
    echo `date +%Y-%m-%dT%H:%M:%S`"  Failed to copy $1"
    echo 1 > $xrdcpfail
    echo "---" >> $log
    echo "Copy failed with rc=${rc}, command and outputs were:" >> $log
    echo "xrdcp --cksum adler32 --tpc only "\""root://castorpublic/$1"\"" "\""root://eoshome-${2:0:1}//eos/user/${2:0:1}/$2/$3?eos.ruid=${ruid}&eos.rgid=${rgid}&eos.mtime=$4&eos.app=castormig"\" >> $log
    cat $xrdlog >> $log
  else
    # success, show some progress bar
    echo -n . >> $log
  fi
  rm -rf $xrdlog
}
export -f xrd3cp

ldapcheck() {   # arg: username
  # perform an LDAP search looking for the (CERN-specific) signature of expired or non-primary accounts
  # `userAccountControl: 512` => active, `514` => disabled
  ldapsearch -o ldif-wrap=no -x -h xldap.cern.ch -b 'OU=Users,OU=Organic Units,DC=cern,DC=ch' '(&(objectClass=user)(sAMAccountName='$1'))' | egrep cernAccountOwner\|userAccountControl | xargs
}


# check and prepare arguments
if [[ $# -ne 2 && $# -ne 3 ]]; then
  print_usage
fi
export dryrun=$1
username=$2
nthreads=40
export log=${3:-/var/log/c2cboxcopy_remigrate.log}
kinit stage -kt stage.keytab
krbday=`date +%d`

# check source folder
export folder=/castor/cern.ch/user/${username:0:1}/$username
if [[ "$dryrun" == "--doit" ]]; then
  ssh root@castorpublic "nschmod 555 ${folder}; nschown root:root ${folder}"    # to block write access in CASTOR
fi
fileclass=`ssh root@castorpublic "nsls --class -d ${folder} | cut -d\  -f 1"`

# check user account
ldap=`ldapcheck $username`
# in case of secondary account, the output looks like:
# cernAccountOwner: CN=lopresti,OU=Users,OU=Organic Units,DC=cern,DC=ch [extensionAttribute2: Password expired.]
targetname=`echo $ldap | grep cernAccountOwner | cut -d= -f 2 | cut -d, -f 1`
if [[ "$targetname" != "" ]]; then
  # this is a secondary account, let's probe the primary
  ldapp=`ldapcheck $targetname`
  echo $ldapp | grep -q 'userAccountControl: 514'
  if [[ $? -eq 0 ]]; then
    echo `date +%Y-%m-%dT%H:%M:%S`"  Skipping ${username}, primary account ${targetname} is expired"
    echo ${targetname} >> account_to_quarantine
    exit -1
  fi
  echo $ldap | grep -q 'userAccountControl: 514'
  if [[ $? -eq 0 ]]; then
    # account is blocked
    echo `date +%Y-%m-%dT%H:%M:%S`"  Starting migration of ${username} to primary account ${targetname}"
  else
    # account is active, use it
    targetname=$username
    echo `date +%Y-%m-%dT%H:%M:%S`"  Starting migration for secondary account ${username}"
  fi
else
  # this is a primary account
  echo $ldap | grep -q 'userAccountControl: 514'
  if [[ $? -eq 0 ]]; then
    echo `date +%Y-%m-%dT%H:%M:%S`"  Skipping expired primary account ${username}"
    echo ${username} >> account_to_quarantine
    exit -1
  fi
  targetname=$username
  echo `date +%Y-%m-%dT%H:%M:%S`"  Starting migration for primary account ${username}"
fi

# check quota and validate user in EOS
currentquota=`ssh root@eoshome-${targetname:0:1} "eos quota ls -m -u $targetname /eos/user"`
if [[ "$currentquota" == "" ]]; then
  # user does not have home in CERNBox?
  echo `date +%Y-%m-%dT%H:%M:%S`"  Aborting active user ${targetname}, does NOT exist in EOSHOME"
  exit -1
fi

# dump namespace
if [[ "$skipquota" != "--skipquota" ]]; then
  ./nsfind.py $folder > all_${username}_files
fi
count=$(tail -1 all_${username}_files | awk '{print $4}')
totalsize=$(tail -1 all_${username}_files | awk '{print $5}')
if [[ $totalsize -eq 0 ]]; then
  echo `date +%Y-%m-%dT%H:%M:%S`"  Skipping empty user ${username}"
  rm -rf all_${username}_files
  exit -1
fi

if [[ "$dryrun" != "--doit" ]]; then
    echo `date +%Y-%m-%dT%H:%M:%S`"  DRY-RUN mode enabled, no real action will take place on CASTOR nor on CERNBox"
fi

if [[ "$targetname" == "$username" ]]; then
  finallocation=Imported_from_CASTOR
else
  finallocation=Imported_from_CASTOR_${username}
fi

# extract list of files - each line in all_username_files looks like:
#  58  22103  1028         1               1000  1497539007-1497539007 1497539008-1497539008  /castor/cern.ch/user/i/itglp/notape/testxroot
grep  '       1   ' all_${username}_files > onlyfiles_${username}
# perform data migration with $nthreads copy operations in parallel
export ruid=`getent passwd ${targetname} | cut -d: -f 3`
export rgid=`getent passwd ${targetname} | cut -d: -f 4`
echo `date +%Y-%m-%dT%H:%M:%S`"  Migrating $(( totalsize/1000000000 )) GB in ${count} files for user ${username}..."
echo "===" >> $log
echo `date +%Y-%m-%dT%H:%M:%S`"  Migrating $(( totalsize/1000000000 )) GB in ${count} files for user ${username}..." >> $log
#if [[ "$dryrun" == "--doit" ]]; then
  IFS=
  while read line; do
    source=`echo $line | cut -c94-`
    target=`echo $source | sed 's|'${folder}'||'`
    mtime=`echo $line | awk '{print $7}' | cut -d\- -f 1 | xargs`
    echo $line | awk '{print $4 "@" $5}' | grep -q '1@0'
    nonzerobytes=$?
    xrd3cp "$source" "$targetname" "${finallocation}${target}" "$mtime" "$nonzerobytes"
    # check roughly every 1000 files
    if [[ $RANDOM -lt 30 ]]; then
      krbhour=`date +%k`
      if [[ $krbhour -eq 0 ]]; then
        today=`date +%d`
        if [[ $today -ne $krbday ]]; then
          # renew tgt at around midnight every day
          krbday=`date +%d`
          kinit stage -kt stage.keytab
          echo `date +%Y-%m-%dT%H:%M:%S`"  Renewed TGT for stage"
        fi
      fi
    fi
  done < onlyfiles_${username}
  echo >> $log
#fi

if [[ -s $xrdcpfail ]]; then
  # at least one file copy failed, halt the migration
  echo `date +%Y-%m-%dT%H:%M:%S`"  Remigration for ${username} FAILED"
  exit -2
fi

if [[ "$dryrun" == "--doit" ]]; then
  echo `date +%Y-%m-%dT%H:%M:%S`"  Remigration for ${username} completed successfully"
else
  echo `date +%Y-%m-%dT%H:%M:%S`"  Remigration for ${username} completed successfully (DRY-RUN mode)"
fi
