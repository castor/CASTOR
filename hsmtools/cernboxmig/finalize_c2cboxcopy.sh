#!/bin/bash
#/******************************************************************************
# *                      finalize_c2cboxcopy.sh
# *
# * This file is part of the Castor project.
# * See http://cern.ch/castor and http://cern.ch/eoscta
# * Copyright (C) 2019  CERN
# *
# * This program is free software; you can redistribute it and/or
# * modify it under the terms of the GNU General Public License
# * as published by the Free Software Foundation; either version 2
# * of the License, or (at your option) any later version.
# * This program is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU General Public License for more details.
# * You should have received a copy of the GNU General Public License
# * along with this program; if not, write to the Free Software
# * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# *
# * CASTOR user folders to CERNBox migration
# *
# * This tool executes the last part of a CASTOR to CERNBox migration
# * for a given username to a targetname.
# *
# * Assumptions: the tool is run from an EOS gateway (e.g. a Samba box), with
# * `ldapsearch` available as well as krb-based root access to all eoshome
# * head nodes. On the CASTOR side, the castor_tools.py must be available
# * and configured for a direct access to the CASTOR Namespace DB.
# *
# * @author Castor Dev team, castor-dev@cern.ch
# *****************************************************************************/

if [[ $# -ne 2 ]]; then
  exit 1
fi

username=$1
targetname=$2
folder=/castor/cern.ch/user/${username:0:1}/$username

# check if already completed
fileclass=`ssh root@castorpublic "nsls --class -d ${folder}" | awk '{print $1}'`
if [[ $fileclass -eq 6031 ]]; then
  echo `date +%Y-%m-%dT%H:%M:%S`"  Migration for ${username} already finalized"
  exit -1
fi

# clean temporary files
rm -rf onlyfiles_${username} all_${username}_files ${xrdcpfail}

# move to final location in the user's home space: this way the folder appears atomically,
# and won't be synchronized if larger than 0.5 GB
if [[ "$targetname" == "$username" ]]; then
  finallocation=/eos/user/${targetname:0:1}/${targetname}/Imported_from_CASTOR
else
  finallocation=/eos/user/${targetname:0:1}/${targetname}/Imported_from_CASTOR_${username}
fi

ssh eoshome-${targetname:0:1} eos mv /eos/user/.castormigration/${username} ${finallocation}

# mail user that the migration has taken place
echo `date +%Y-%m-%dT%H:%M:%S`"  Notifying user by email"
cat notification_email.txt | sed "s/USERNAME/${targetname}/" | sed "s|CASTORDIR|${folder}|" | sed "s|EOSDIR|${finallocation}|" |\
mail -r castor-noreply@cern.ch -s "CASTOR to CERNBox migration" ${targetname}@cern.ch

# mark CASTOR files as migrated to CERNBox
./castor_flag_cernbox_migration.py -u ${username} --doit
ssh root@castorpublic "nschclass 6031 ${folder}"

echo `date +%Y-%m-%dT%H:%M:%S`"  Migration for ${username} completed successfully"
