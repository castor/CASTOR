#!/usr/bin/python2
#/******************************************************************************
# *                      castor_flag_cernbox_migration.py
# *
# * This file is part of the Castor project.
# * See http://cern.ch/castor and http://cern.ch/eoscta
# * Copyright (C) 2019  CERN
# *
# * This program is free software; you can redistribute it and/or
# * modify it under the terms of the GNU General Public License
# * as published by the Free Software Foundation; either version 2
# * of the License, or (at your option) any later version.
# * This program is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU General Public License for more details.
# * You should have received a copy of the GNU General Public License
# * along with this program; if not, write to the Free Software
# * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# *
# * @author Castor Dev team, castor-dev@cern.ch
# *****************************************************************************/

'''Command line helper tool for the migration from CASTOR to CERNBox'''

import sys
import getopt
from time import sleep, time
from datetime import datetime

import castor_tools


def usage(exitcode):
    '''prints usage'''
    print __doc__
    print 'Usage : ' + sys.argv[0] + ' [-h|--help] -u|--username <username> --doit'
    sys.exit(exitcode)


def run():
    '''main code'''
    success = False
    doit = None
    username = None
    # first parse the options
    try:
        options, _ = getopt.getopt(sys.argv[1:], 'hu:', ['help', 'username=', 'doit'])
    except Exception, e:
        print e
        usage(1)
    for f, v in options:
        if f == '-h' or f == '--help':
            usage(0)
        elif f == '-u' or f == '--username':
            username = v
        elif f == '--doit':
            doit = True
        else:
            print "unknown option : " + f
            usage(1)

    # deal with arguments
    if not username or not doit:
        print 'Missing argument(s). A --doit argument is mandatory.\n'
        usage(1)

    try:
        # connect to nameserver and execute the PL/SQL procedure
        nsconn = castor_tools.connectToNS()
        nscur = nsconn.cursor()
        nscur.execute('BEGIN markUserAsMigratedToCERNBox(:username); END;', username=username)
        castor_tools.disconnectDB(nsconn)
    except Exception, e:
        print e
        import traceback
        traceback.print_exc()
        sys.exit(-1)


if __name__ == '__main__':
    run()
