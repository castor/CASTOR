#!/usr/bin/python
#******************************************************************************
#                      draindiskserver
#
# This file is part of the Castor project.
# See http://castor.web.cern.ch/castor
#
# Copyright (C) 2003  CERN
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#
# Tool to manage the draining of filesystems and diskservers.
#
# @author Castor Dev team, castor-dev@cern.ch
#******************************************************************************

'''Tool to manage the draining of filesystems and diskservers'''

import sys
import time
import getopt
import castor_tools
import cx_Oracle

# Usage function
def usage(exitCode):
    '''prints usage'''
    print 'Usage :'
    print '  draindiskserver -h/--help'
    print '  draindiskserver -a/--add [-S serviceclass] [--file-mask=(NOTONTAPE|ALL)] [--comment=comment] <targets>'
    print '  draindiskserver -q/--query [-c/--config] [-r/--running] [-x/--extra] [--script] [<targets>]'
    print '  draindiskserver -q/--query -f/--failures [<targets>]'
    print '  draindiskserver -d/--delete <targets>'
    print 'where <targets> = [-m|--mountpoints <mountpoint>[:...]] <diskpool>|<diskServerName> [...]'
    sys.exit(exitCode)

class FileMask(object):
  '''Constants defining the existing types of transfer'''
  NOTONTAPE = 0
  ALL = 1
  def __init__(self):
    '''empty constructor, raises an exception'''
    raise NotImplementedError
  @staticmethod
  def toStr(tType):
    '''prints a readable version of the transfer types'''
    if tType == FileMask.NOTONTAPE:
      return 'NotOnTape'
    elif tType == FileMask.ALL:
      return 'All'
    else:
      return 'UNKNOWN'
  
class DiskServerStatus(object):
  '''Constants defining the status for DiskServers'''
  PRODUCTION = 0
  DRAINING = 1
  DISABLED = 2
  READONLY = 3
  def __init__(self):
    '''empty constructor, raises an exception'''
    raise NotImplementedError
  @staticmethod
  def toStr(dsStatus):
    '''prints a readable version of the diskServer status'''
    if dsStatus == DiskServerStatus.PRODUCTION:
      return 'PRODUCTION'
    elif dsStatus == DiskServerStatus.DRAINING:
      return 'DRAINING'
    elif dsStatus == DiskServerStatus.DISABLED:
      return 'DISABLED'
    elif dsStatus == DiskServerStatus.READONLY:
      return 'READONLY'
    else:
      return 'UNKNOWN'
  
class FileSystemStatus(object):
  '''Constants defining the status for FileSystems'''
  PRODUCTION = 0
  DRAINING = 1
  DISABLED = 2
  READONLY = 3
  def __init__(self):
    '''empty constructor, raises an exception'''
    raise NotImplementedError
  @staticmethod
  def toStr(dsStatus):
    '''prints a readable version of the diskServer status'''
    if dsStatus == FileSystemStatus.PRODUCTION:
      return 'PRODUCTION'
    elif dsStatus == FileSystemStatus.DRAINING:
      return 'DRAINING'
    elif dsStatus == FileSystemStatus.DISABLED:
      return 'DISABLED'
    elif dsStatus == FileSystemStatus.READONLY:
      return 'READONLY'
    else:
      return 'UNKNOWN'

def drainingJobStatusToStr(status):
  '''prints a readable version of the drainingJobStatus'''
  if status == 0:
    return 'SUBMITTED'
  elif status == 1:
    return 'STARTING'
  elif status == 2:
    return 'RUNNING'
  elif status == 4:
    return 'FAILED'
  elif status == 5:
    return 'FINISHED'
  else:
    return 'UNKNOWN'

# first parse the options
try:
    options, arguments = getopt.getopt(sys.argv[1:], 'ham:S:qxcfrdv',
                                  ['help', 'add', 'mountpoints=',
                                   'svcclass=', 'file-mask=', 'comment=',
                                   'query', 'config', 'extra', 'failures',
                                   'running', 'script', 'delete', 'verbose'])
except Exception, parsingException:
    print parsingException
    usage(1)
    
addFlag = False
queryFlag = False
deleteFlag = False
mountPoints = None
svcClass = None
fileMask = 1 # default is ALL
fileMaskFlag = False
configFlag = False
failuresFlag = False
runningFlag = False
scriptFlag = False
comment = None
verbose = False
displayFSFlag = False
for opt, arg in options:
    if opt in ('-h', '--help'):
        usage(0)
    elif opt in ('-a', '--add'):
        addFlag = True
    elif opt in ('-q', '--query'):
        queryFlag = True
    elif opt in ('-d', '--delete'):
        deleteFlag = True
    elif opt in ('-m', '--mountpoints'):
        mountPoints = tuple(arg.split(':'))
    elif opt in ('-S', '--svcclass'):
        svcClass = arg
    elif opt == '--file-mask':
        fileMaskFlag = True
        # Check that the user supplied filemask argument is valid
        values = { 'NOTONTAPE': 0,
                   'ALL':       1 }
        if not values.has_key(arg.upper()):
            print 'invalid option for --file-mask, must be one of NOTONTAPE or ALL'
            usage(1)
        fileMask = values[arg.upper()]
    elif opt in ('-c', '--config'):
        configFlag = True
    elif opt in ('-f', '--failures'):
        failuresFlag = True
    elif opt in ('-x', '--extra'):
        displayFSFlag = True
    elif opt in ('-r', '--running'):
        runningFlag = True
    elif opt == '--script':
        scriptFlag = True
    elif opt == '--comment':
        comment = arg
    elif opt == '-v' or opt == '--verbose':
        verbose = True
    else:
        print "unknown option : " + opt
        usage(1)

targets = arguments

# Check consistency of arguments
if mountPoints and queryFlag:
    displayFSFlag = True
if not (addFlag or queryFlag or deleteFlag):
    print 'missing operation, must specify one of --add, --delete or --query'
    usage(1)
if [addFlag, queryFlag, deleteFlag].count(True) > 1:
    print 'options --add, --delete and --query are mutually exclusive'
    usage(1)
if len(targets) == 0 and (addFlag or deleteFlag):
    print 'Nothing to do ! Please give a few diskpools/diskservers'
    usage(1)
if mountPoints != None and not targets:
    print 'targets are mandatory when --mountPoints is used'
    usage(1)
if (addFlag or deleteFlag) and (configFlag or failuresFlag or runningFlag or scriptFlag or displayFSFlag):
    print 'incompatible set of options : --config, --failures, --extra, --running, and --script are only valid with --query'
    usage(1)
if (queryFlag or deleteFlag) and (svcClass != None or fileMaskFlag or comment != None):
    print 'incompatible set of options : --query. svcclass, file-mask and comment are only valid with --add'
    usage(1)
if failuresFlag and (configFlag or runningFlag or scriptFlag):
    print 'option --running, --config and --scripts can not be used with --failures'
    usage(1)

    
def getFileSystems(stcur, shouldNotBeEmpty=False):
    '''check the targets and mountPoints, and returns the list of filesytems concerned
       in the form of tuples (diskServerId, diskServerName, FileSystemId, mountPoint)'''
    # get list of target diskservers from the mix diskpool/diskserver targets
    unknownTargets, diskServerIds = castor_tools.parseAndCheckTargets(targets, stcur)
    # and complain if some are unknown
    if unknownTargets:
        print 'ERROR : the following diskpools/diskservers do not exist : ' + \
              ', '.join(unknownTargets)
        print 'Giving up'
        usage(1)
    if not diskServerIds:
        if shouldNotBeEmpty:
            print 'None of the provided diskpools/diskservers could be found. Giving up'
            sys.exit(1)
        else:
            return None
    # get the list of filesystems from the diskservers and the mounpoints given
    fileSystems = castor_tools.parseAndCheckMountPoints(diskServerIds, mountPoints, stcur)
    # and complain if nothing was found
    if not fileSystems:
        print 'Unable to find concerned filesystems. Please check node name and/or mount point.'
        print 'In particular make sure that diskserver name is fully qualified and that mountPoint ends with /'
        sys.exit(1)
    return fileSystems


def getSvcClassId(stcur, fileSystemIds):
    '''checks the given svcClass if any and gets its id. Else find out
       the service class to be used, that is the one of the involved filesytems'''
    # if any service class is given, get its id
    if svcClass != None:
        stCheckSvcClass = '''
        BEGIN
          :svcClassId := checkForValidSvcClass(:svcClassName, 0, 1);
        END;'''
        try:
            varClassId = stcur.var(cx_Oracle.NUMBER)
            stcur.execute(stCheckSvcClass, svcClassId=varClassId, svcClassName=svcClass)
            return varClassId.getvalue()
        except cx_Oracle.Error:
            # not found, print an error and exit
            print 'Unknown service class "%s". Giving up' % svcClass
            sys.exit(1)
    else:
        # no service class given, get the one of the involved filesystems
        stGetSvcClass = '''
        SELECT UNIQUE DiskPool2SvcClass.child
          FROM FileSystem, DiskPool2SvcClass
         WHERE FileSystem.id IN (''' + ', '.join([str(x) for x in fileSystemIds]) + ''')
           AND FileSystem.diskpool = DiskPool2SvcClass.parent'''
        stcur.execute(stGetSvcClass)
        svcClassIds = stcur.fetchall()
        if len(svcClassIds) == 0:
            print 'Unable to find service class. Giving up'
            sys.exit(1)
        elif len(svcClassIds) > 1:
            print 'Found several possible service classes for this drain. Please specify --svcclass option'
            sys.exit(1)
        return svcClassIds[0][0]


def submitDrain():
    '''submits new draining jobs'''
    # prepare DB connection
    stconn = castor_tools.connectToStager()
    try:
        stcur = stconn.cursor()
        stcur.arraysize = 50
        # precompute some useful values about us
        machine, euid, egid, pid, userName = castor_tools.getIdentity()[0:5]
        # check the targets and mountPoints, and get the list of filesytem ids concerned
        fileSystems = getFileSystems(stcur, True)
        # check the service class and gets its id
        svcClassId = getSvcClassId(stcur, [fs[2] for fs in fileSystems])
        # create DrainingJob statement
        stCreateDrainingJob = '''
        INSERT INTO DrainingJob (id, userName, euid, egid, pid, machine,
                                 creationTime, lastModificationTime, status, fileSystem,
                                 svcClass, fileMask, totalFiles, totalBytes,
                                 nbFailedBytes, nbFailedFiles, nbSuccessBytes,
                                 nbSuccessFiles, userComment)
        VALUES (ids_seq.nextval, :userName, :euid, :egid, :pid, :machine,
                getTime(), getTime(), 0, :fsId, :svcClassId, :fileMask,
                0, 0, 0, 0, 0, 0, :userComment)'''
        # First check DiskServers status
        stCheckDSState = 'SELECT name, status FROM DiskServer WHERE id IN (' + \
                         ', '.join([str(fs[0]) for fs in fileSystems]) + ')'
        stcur.execute(stCheckDSState)
        for dsName, dsStatus in stcur.fetchall():
            if dsStatus == DiskServerStatus.DISABLED:
                print 'DiskServer %s is in DISABLED state. Skiping it' % dsName
                fileSystems = [fs for fs in fileSystems if fs[1] != dsName]
        # In case no filesystem are given by the user, switch the status of the
        # diskservers to DRAINING. In case filesystems are mentionned, the DiskServer
        # status is not touched
        if mountPoints == None:
            # status 1 is DRAINING
            stUpdateDSState = 'UPDATE DiskServer SET status = 1 WHERE id IN (' + \
                              ', '.join([str(fs[0]) for fs in fileSystems]) + ')'
            stcur.execute(stUpdateDSState)
        # SQL statements
        stCheckDrainingJob = '''
        SELECT username, machine, creationTime
          FROM DrainingJob
         WHERE fileSystem = :fsId'''
        stUpdateFSState = 'UPDATE FileSystem SET status = 1 WHERE id = :fsId' # DRAINING
        stCheckFSState = 'SELECT status FROM FileSystem WHERE id = :fsId'
        # go through all the concerned filesystem
        for dsId, dsName, fsId, fsMountPoint in fileSystems: # pylint: disable=W0612
            # first check the filesystem status
            stcur.execute(stCheckFSState, fsId=fsId)
            dsStatus = stcur.fetchall()[0][0]
            if dsStatus == FileSystemStatus.DISABLED:
                print 'Filesystem %s:%s is in DISABLED state. Skipping it' % (dsName, fsMountPoint)
                continue
            # then check that the filesystem is not already draining
            stcur.execute(stCheckDrainingJob, fsId=fsId)
            drains = stcur.fetchall()
            if (len(drains)) > 0:
                print 'Filesystem %s:%s already has a draining session. Use --delete to cancel it' % \
                  (dsName, fsMountPoint)
                print '    for info, drain was issued by %s on %s at %s' % \
                  (drains[0][0], drains[0][1], castor_tools.secsToDate(drains[0][2]))
                continue
            # change the state of the filesystem to DRAINING, i.e. trigger passive draining
            stcur.execute(stUpdateFSState, fsId=fsId)
            # finally start active draining
            stcur.execute(stCreateDrainingJob, userName=userName, euid=euid, egid=egid,
                          pid=pid, machine=machine, fsId=fsId, svcClassId=svcClassId,
                          fileMask=fileMask, userComment=comment)
            print 'Started draining for %s:%s' % (dsName, fsMountPoint)
        stconn.commit()
    finally:
        # close DB connection
        castor_tools.disconnectDB(stconn)

def queryFailures():
    '''queries draining failures'''
    # prepare DB connection
    stconn = castor_tools.connectToStager()
    try:
        stcur = stconn.cursor()
        stcur.arraysize = 50
        # check the targets and mountPoints, and get the list of filesytem ids concerned
        fileSystems = getFileSystems(stcur)
        # build SQL statement
        stGetFailures = '''
        SELECT DiskServer.name, DrainingErrors.fileId,
               fileSystem.mountPoint || DiskCopy.path as filePath,
               DrainingErrors.timestamp, DrainingErrors.errorMsg
          FROM DiskServer, FileSystem, DrainingJob, DrainingErrors, DiskCopy
         WHERE FileSystem.diskServer = DiskServer.id
           AND DrainingJob.fileSystem = FileSystem.id
           AND DrainingErrors.drainingJob = DrainingJob.id
           AND DrainingErrors.castorFile = DiskCopy.castorFile(+)'''
        if fileSystems:
            stGetFailures += ' AND FileSystem.id IN (' + ', '.join([str(fs[2]) for fs in fileSystems]) + ')'
        # get data
        stcur.execute(stGetFailures)
        results = stcur.fetchall()
        # pretty printing
        titles = ('DiskServer', 'FileId', 'FilePath', 'Timestamp', 'Error')
        data = [(name, fileId, filePath, castor_tools.secsToDate(timestamp), errorMsg)
                for name, fileId, filePath, timestamp, errorMsg in results]
        castor_tools.prettyPrintTable(titles, data)
    finally:
        # close DB connection
        castor_tools.disconnectDB(stconn)            
        
def queryDrain():
    '''queries ongoing drainings'''
    # prepare DB connection
    stconn = castor_tools.connectToStager()
    try:
        stcur = stconn.cursor()
        stcur.arraysize = 50
        # check the targets and mountPoints, and get the list of filesytem ids concerned
        fileSystems = getFileSystems(stcur)
        # build SQL statement
        stGetDrains = '''
        SELECT DiskServer.name,
               CASE WHEN GROUPING (FileSystem.mountPoint) = 1
                    THEN TO_CHAR(COUNT(FileSystem.mountPoint))
                    ELSE FileSystem.mountPoint END mountPoint,
               MIN(DrainingJob.username), MIN(DrainingJob.machine),
               CAST(COLLECT(DISTINCT DrainingJob.userComment) AS strListTable) AS userComment,
               MIN(DrainingJob.creationTime) creationTime,
               CASE WHEN GROUPING (FileSystem.mountPoint) = 1
                    THEN CASE WHEN MIN(fileMask) = MAX(fileMask) THEN MIN(fileMask) ELSE -1 END
                    ELSE MIN(fileMask) END fileMask,
               getSvcClassName(MIN(DrainingJob.svcClass)) svcClassName,
               MIN(DrainingJob.status) status,
               SUM(DrainingJob.totalFiles) totalFiles, SUM(DrainingJob.totalBytes) totalBytes,
               SUM(DrainingJob.nbFailedBytes) nbFailedBytes, SUM(DrainingJob.nbSuccessBytes) nbSuccessBytes,
               SUM(DrainingJob.nbFailedFiles) nbFailedFiles, SUM(DrainingJob.nbSuccessFiles) nbSuccessFiles,
               DECODE(MIN(DrainingJob.status), 4, MAX(lastModificationTime), 5, MAX(lastModificationTime), getTime()) currentTime,
               GROUPING (FileSystem.mountPoint)
          FROM DrainingJob, FileSystem, DiskServer
         WHERE DrainingJob.fileSystem = FileSystem.id
           AND FileSystem.diskServer = DiskServer.id'''
        if fileSystems:
            stGetDrains += ' AND FileSystem.id IN (' + ', '.join([str(fs[2]) for fs in fileSystems]) + ')'        
        if runningFlag:
            stGetDrains += ' AND DrainingJob.status IN (0, 1, 2)'  # SUBMITTED, STARTING, RUNNING
        stGetDrains += '''
           GROUP BY GROUPING SETS ((DiskServer.name, FileSystem.mountPoint), (DiskServer.name))
           ORDER BY DiskServer.name, GROUPING(DiskServer.name), GROUPING(FileSystem.mountPoint) DESC'''
        # get data
        stcur.execute(stGetDrains)
        results = stcur.fetchall()
        # Check if we've found something
        if len(results) == 0:
            print 'Nothing found'
            return
        # depending on the presence of the config and the displayFS flags, we have four different displays
        if configFlag:
            if displayFSFlag:
                titles = ['DiskServer', 'MountPoint', 'UserName', 'Machine', 'SvcClass',
                          'FileMask', 'Comment']
                data = [(diskServerName, fsMountPoint, userName, machine, svcClassName,
                         FileMask.toStr(DJFileMask), DJComment if DJComment != [] else None)
                        for diskServerName, fsMountPoint, userName, machine,
                        DJComment, creationTime, DJFileMask, svcClassName, DJStatus,
                        totalFiles, totalBytes, nbFailedBytes, nbSuccessBytes, nbFailedFiles,
                        nbSuccessFiles, currentTime, grouping in results if not grouping]
            else:
                titles = ['DiskServer', 'UserName', 'Machine', 'SvcClass',
                          'FileMask', 'Comment']
                data = [(diskServerName, userName, machine, svcClassName,
                         FileMask.toStr(DJFileMask), DJComment if DJComment != [] else None)
                        for diskServerName, fsMountPoint, userName, machine,
                        DJComment, creationTime, DJFileMask, svcClassName, DJStatus,
                        totalFiles, totalBytes, nbFailedBytes, nbSuccessBytes, nbFailedFiles,
                        nbSuccessFiles, currentTime, grouping in results if grouping]
        else:
            if displayFSFlag:
              titles = ['DiskServer', 'MountPoint', 'SvcClass', 'Created', 'TFiles', 'TSize',
                        'RFiles', 'RSize', 'Done', 'Failed', 'RunTime', 'Progress', 'ETC', 'Status']
              data = [(diskServerName, fsMountPoint, svcClassName, castor_tools.secsToDate(creationTime), totalFiles,
                       castor_tools.nbToDataAmount(totalBytes), totalFiles-nbFailedFiles-nbSuccessFiles,
                       castor_tools.nbToDataAmount(totalBytes-nbFailedBytes-nbSuccessBytes),
                       nbSuccessFiles, nbFailedFiles,
                       castor_tools.nbToAge(int(currentTime-creationTime)),
                       castor_tools.printPercentage((nbSuccessBytes+nbFailedBytes), totalBytes),
                       castor_tools.printETC(nbSuccessBytes+nbFailedBytes, totalBytes, currentTime-creationTime),
                       drainingJobStatusToStr(DJStatus))
                      for diskServerName, fsMountPoint, userName, machine,
                      DJComment, creationTime, DJFileMask, svcClassName, DJStatus,
                      totalFiles, totalBytes, nbFailedBytes, nbSuccessBytes, nbFailedFiles,
                      nbSuccessFiles, currentTime, grouping in results if not grouping]
            else:
              titles = ['DiskServer', 'SvcClass', 'Created', 'TFiles', 'TSize',
                        'RFiles', 'RSize', 'Done', 'Failed', 'RunTime', 'Progress', 'ETC', 'Status']
              data = [(diskServerName, svcClassName, castor_tools.secsToDate(creationTime), totalFiles,
                       castor_tools.nbToDataAmount(totalBytes), totalFiles-nbFailedFiles-nbSuccessFiles,
                       castor_tools.nbToDataAmount(totalBytes-nbFailedBytes-nbSuccessBytes),
                       nbSuccessFiles, nbFailedFiles,
                       castor_tools.nbToAge(int(currentTime-creationTime)),
                       castor_tools.printPercentage((nbSuccessBytes+nbFailedBytes), totalBytes),
                       castor_tools.printETC(nbSuccessBytes+nbFailedBytes, totalBytes, currentTime-creationTime),
                       drainingJobStatusToStr(DJStatus))
                      for diskServerName, fsMountPoint, userName, machine,
                      DJComment, creationTime, DJFileMask, svcClassName, DJStatus,
                      totalFiles, totalBytes, nbFailedBytes, nbSuccessBytes, nbFailedFiles,
                      nbSuccessFiles, currentTime, grouping in results if grouping]
            if not scriptFlag:
                zr = zip(*results)
                # here we divide every total by 2 because results includes everything twice, with grouping = 0 and 1
                totFiles, totBytes, totFailedBytes, totSuccessBytes, totFailedFiles, totSuccessFiles = \
                  sum(zr[9])/2, sum(zr[10])/2, sum(zr[11])/2, sum(zr[12])/2, sum(zr[13])/2, sum(zr[14])/2
                data.append((('', '') if displayFSFlag else ('',)) + ('TOTAL', castor_tools.secsToDate(time.time()),
                             totFiles, castor_tools.nbToDataAmount(totBytes),
                             totFiles-totFailedFiles-totSuccessFiles,
                             castor_tools.nbToDataAmount(totBytes-totFailedBytes-totSuccessBytes),
                             totSuccessFiles, totFailedFiles, '',
                             castor_tools.printPercentage((totSuccessBytes+totFailedBytes), totBytes),
                             '', ''))
        # depending on the presence of the script flags, we use pretty
        # printing or script printing
        if scriptFlag:
            castor_tools.scriptPrintTable(data)
        else:
            castor_tools.prettyPrintTable(titles, data, hasSummary=(not configFlag))
    finally:
        # close DB connection
        castor_tools.disconnectDB(stconn)
        
def deleteDrain():
    '''interrupts and ongoing draining job'''
    # prepare DB connection
    stconn = castor_tools.connectToStager()
    try:
        stcur = stconn.cursor()
        stcur.arraysize = 50
        # check the targets and mountPoints, and get the list of filesytem ids concerned
        fileSystems = getFileSystems(stcur, True)
        # check draining job statement
        stCheckDrainingJob = '''
        SELECT id
          FROM DrainingJob
         WHERE fileSystem = :fsId'''
        drainingJobs = []
        for dsId, dsName, fsId, fsMountPoint in fileSystems:  # pylint: disable=W0612
            # first check that the filesystem is not already draining
            stcur.execute(stCheckDrainingJob, fsId=fsId)
            drains = stcur.fetchall()
            if len(drains) == 0:
                print 'No draining activity found for %s:%s. Ignoring it' % (dsName, fsMountPoint)
                continue
            print 'Draining activity for %s:%s will be stopped' % (dsName, fsMountPoint)
            drainingJobs.append((dsName, fsMountPoint, drains[0][0]))
        if drainingJobs:
            # get user confirmation
            try:
                confirmation = castor_tools.parseBool('entry', raw_input("Is this ok ? [N/y] "), False)
            except castor_tools.ParsingError, e:
                print e
                exit(1)
            if not confirmation:
                print 'Ok, giving up'
                return
            # create DrainingJob statement
            stDeleteDrainingJob = 'BEGIN deleteDrainingJob(:djId); END;'
            # call it for each filesystem
            for dsName, fsMountPoint, djId in drainingJobs:
                print 'Stopping drain for %s:%s' % (dsName, fsMountPoint)
                stcur.execute(stDeleteDrainingJob, djId=djId)
            stconn.commit()
    finally:
        # close DB connection
        castor_tools.disconnectDB(stconn)

# process the request
try:
    if addFlag:
        submitDrain()
    elif queryFlag:
        if failuresFlag:
            queryFailures()
        else:
            queryDrain()
    elif deleteFlag:
        deleteDrain()
    else:
        print "No option specified\n"
        usage(1)
except Exception, e:
    print e
    if verbose:
        import traceback
        traceback.print_exc()
    sys.exit(-1)
