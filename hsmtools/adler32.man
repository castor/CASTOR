.\" ******************************************************************************
.\"                      adler32
.\"
.\" This file is part of the Castor project.
.\" See http://castor.web.cern.ch/castor
.\"
.\" Copyright (C) 2003  CERN
.\" This program is free software; you can redistribute it and/or
.\" modify it under the terms of the GNU General Public License
.\" as published by the Free Software Foundation; either version 2
.\" of the License, or (at your option) any later version.
.\" This program is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\" You should have received a copy of the GNU General Public License
.\" along with this program; if not, write to the Free Software
.\" Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
.\"
.\"
.\" man page for the adler32 command
.\"
.\" @author Castor Dev team, castor-dev@cern.ch
.\" *****************************************************************************/
.TH ADLER32 "1castor" "$Date: 2007/06/07 14:11:11 $" CASTOR "Gets the path of a castor file from its fileId"
.SH NAME
adler32
.SH SYNOPSIS
.B adler32
file1 file2 ...
.SH DESCRIPTION
.B  adler32
calculates and prints the adler32 checksum for the listed files.

.TP 18
.B file1 file2 ...
the names of the files for which the checksum should be calculated. The files
may be read remotely via RFIO.

.SH EXAMPLES
.BI #\ adler32\ myfile 
.fi
adler32(myfile) = 3119931364, 0xb9f65fe4

.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
