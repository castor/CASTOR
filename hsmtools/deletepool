#!/usr/bin/python
#/******************************************************************************
# *                      deletepool
# *
# * This file is part of the Castor project.
# * See http://castor.web.cern.ch/castor
# *
# * Copyright (C) 2003  CERN
# * This program is free software; you can redistribute it and/or
# * modify it under the terms of the GNU General Public License
# * as published by the Free Software Foundation; either version 2
# * of the License, or (at your option) any later version.
# * This program is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU General Public License for more details.
# * You should have received a copy of the GNU General Public License
# * along with this program; if not, write to the Free Software
# * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# *
# * @author Castor Dev team, castor-dev@cern.ch
# *****************************************************************************/

'''command line deleting the pool(s)'''

import sys
import getopt
import castor_tools

# usage function
def usage(exitCode):
    '''prints usage'''
    print 'Usage : ' + sys.argv[0] + ' [-h|--help] <poolName> [...]'
    sys.exit(exitCode)

# first parse the options
try:
    options, args = getopt.getopt(sys.argv[1:], 'hv', ['help', 'verbose'])
except Exception, e:
    print e
    usage(1)
verbose = False
for f, v in options:
    if f == '-h' or f == '--help':
        usage(0)
    elif f == '-v' or f == '--verbose':
        verbose = True
    else:
        print "unknown option : " + f
        usage(1)

# Deal with arguments
pools = set([])
if len(args) != 0:
    pools = set(args)
else:
    print "Missing arguments"
    usage(1)

try:
    # connect to stager
    stconn = castor_tools.connectToStager()
    stcur = stconn.cursor()
    # check disk pool existence
    stcur.execute("SELECT name FROM DiskPool WHERE name IN ('" + "', '".join(pools) + "')")
    existingDiskPools = set([row[0] for row in stcur.fetchall()])
    stcur.execute("SELECT name FROM DataPool WHERE name IN ('" + "', '".join(pools) + "')")
    existingDataPools = set([row[0] for row in stcur.fetchall()])
    nonexitingPools = pools - existingDiskPools - existingDataPools
    # check that the pools are not used by service classes
    poolsWithSvcClass = []
    if existingDiskPools:
        stcur.execute('''SELECT SvcClass.name, DiskPool.name, 'disk pool'
                           FROM SvcClass, DiskPool2SvcClass, DiskPool
                          WHERE SvcClass.id = DiskPool2SvcClass.child
                          AND DiskPool2SvcClass.parent = DiskPool.id
                          AND DiskPool.name IN (\'''' + "', '".join(existingDiskPools) + "')")
        poolsWithSvcClass = stcur.fetchall()
    if existingDataPools:
        stcur.execute('''SELECT SvcClass.name, DataPool.name, 'data pool'
                           FROM SvcClass, DataPool2SvcClass, DataPool
                          WHERE SvcClass.id = DataPool2SvcClass.child
                          AND DataPool2SvcClass.parent = DataPool.id
                          AND DataPool.name IN (\'''' + "', '".join(existingDataPools) + "')")
        poolsWithSvcClass.extend(stcur.fetchall())
    if poolsWithSvcClass:
        print 'Pools cannot be dropped when used by some service classes and'
        for sc, dp, poolType in poolsWithSvcClass:
            print '  - %s %s is in use by service class %s' % (poolType, dp, sc)
        print 'You may want to use modifysvcclass to correct this'
        sys.exit(1)
    # check that the diskpools are empty
    nonEmptyPools = []
    if existingDiskPools:
        stcur.execute('''SELECT DiskPool.name, count(*), 'disk pool', 'filesystems(s)'
                           FROM DiskPool, FileSystem
                          WHERE DiskPool.id = FileSystem.diskPool
                            AND DiskPool.name IN (\'''' + "', '".join(existingDiskPools) + '''\')
                          GROUP BY DiskPool.name''')
        nonEmptyPools = stcur.fetchall()
    if existingDataPools:
        stcur.execute('''SELECT DataPool.name, count(*), 'data pool', 'diskserver(s)'
                           FROM DataPool, DiskServer
                          WHERE DataPool.id = DiskServer.dataPool
                            AND DataPool.name IN (\'''' + "', '".join(existingDataPools) + '''\')
                          GROUP BY DataPool.name''')
        nonEmptyPools.extend(stcur.fetchall())
    if nonEmptyPools:
        print 'Pools cannot be dropped if not empty and'
        for name, nbfs, poolType, childType in nonEmptyPools:
            print '  - %s %s still has %d %s' % (poolType, name, nbfs, childType)
        print 'You may want to use delete/moveDiskServer to correct this'
        sys.exit(1)
    # drop disk pools
    if existingDiskPools:
        stcur.execute("DELETE FROM DiskPool WHERE name IN ('" + "', '".join(existingDiskPools) + "')")
        stconn.commit()
        print 'successfully dropped the following disk pools : ' + ', '.join(existingDiskPools)
    # drop data pools
    if existingDataPools:
        stcur.execute("DELETE FROM DataPool WHERE name IN ('" + "', '".join(existingDataPools) + "')")
        stconn.commit()
        print 'successfully dropped the following data pools : ' + ', '.join(existingDataPools)
    # warn for non existing disk pools
    if nonexitingPools:
        print 'WARNING : some pool(s) did not exist : ' + ', '.join(nonexitingPools)
    # close DB connections
    try:
        castor_tools.disconnectDB(stconn)
    except Exception:
        pass
except Exception, e:
    print e
    if verbose:
        import traceback
        traceback.print_exc()
    sys.exit(-1)
