.TH ENTERSVCCLASS "1castor" "2011" CASTOR "stager catalogue administrative commands"
.SH NAME
entersvcclass \- define a new service class in the stager catalog
.SH SYNOPSIS
.B entersvcclass
[
.BI -h
]
[
.BI --defaultfilesize
<defaultFileSize>
]
[
.BI --replicanb 
<replicaNb>
]
[
.BI --failjobswhennospace 
<bool>
]
[
.BI --disk1behavior 
<bool>
]
[
.BI --forcedfileclass 
<forcedFileClass>
]
.BI --gcpolicy 
<gcPolicyName>
[
.BI --pools 
<poolName>:[...]
]
[
.BI --replicateto 
<svcClassName>
]
<svcClassName>

.SH DESCRIPTION
.B entersvcclass
enters a new service class definition in the CASTOR stager catalog
.TP
.BI \-h,\ \-\-help
Get usage information
.TP
.BI \-\-defaultfilesize\ <defaultFileSize>
The default file size allocation for write requests not specifying any filesize.
Default is 2 GiB if not specified.
The value can be given using the standard K/M/G/T extensions, with or without B (i.e. both KB and K are accepted).
These have the ISO meaning of powers of 10. Ki/Mi/Gi/Ti[B] extensions are also accepted and deal with powers of 2.
.TP
.BI \-\-replicanb\ <replicaNb>
Specifies the number of disk resident replicas for a given CASTOR file in this service class.
Default is 1 if not specified.
.TP
.BI \-\-failjobswhennospace\ <boolean\ value>
Defines whether jobs requesting new space are failed when the pool(s) connected
with this service class is(are) full. Example of supported values are yes/no, y/n, true/false, t/f, 1/0.
Default is yes if not specified.
.TP
.BI \-\-disk1behavior\ <boolean\ value>
Defines whether this service class has Disk1 behavior, which means that garbage
collection is disabled and the flag FailJobsWhenNoSpace is set to yes.
Example of supported values are yes/no, y/n, true/false, t/f, 1/0.
Default is no if not specified.
.TP
.BI \-\-forcedfileclass\ <forcedFileClassName>
Allows to force the file class of files written to this service class to the given value.
The forced file class overrides the file class that the user specifies. This parameter allows
specifying for this service class any TapeN retention policy, where N is the number of
tape copies of the forced file class.
When not provided, the user specified file class is not overwritten.
.TP
.BI \-\-gcpolicy\ <gcPolicyName>
Specifies the name of the garbage collection policy to be used for this service class.
This value is irrelevant if Disk1Behavior is set thus you can omit it in that case and "default" will then be used.
When Disk1Behavior is not true, this option is mandatory.
.TP
.BI \-\-pools\ <pool1:pool2:...>
Lists the names of the pools that this service class is allowed to use.
More than one pool can be specified, colon separated. All specified
pools must already exist.
.TP
.BI \-\-replicateto\ <svcClassName>
Specifies the name of a service class where the files written to the current one should be replicated.
The replication will happen automatically at the closing of the files.
.TP
.BI\ <svcClassName>
name of the service class to create.

.SH EXAMPLES
.nf
.ft CW
#entersvcclass --pools default --defaultfilesize 2GiB public
inserted service class public successfully

#printsvcclass -a
  NAME DEFAULTFILESIZE FAILJOBSWHENNOSPACE REPLICANB FORCEDFILECLASS DISK1BEHAVIOR GCPOLICY     POOLS REPLICATETO ID LASTEDITOR          LASTEDITION
----------------------------------------------------------------------------------------------------------------------------------------------------
public         2048MiB                True         1               -         False        -   default           - 13   sponcec3 30-Aug-2011 16:59:13

#entersvcclass --pools default public
SvcClass public already exists in the stager DB
Giving up

.SH NOTES
This command requires database client access to the stager catalog and nameserver DBs.
Configuration for the database accesses is taken from castor.conf.

.SH SEE ALSO
.BR modifysvcclass
.BR deletesvcclass
.BR printsvcclass
.BR enterpool
.BR adminMultiInstance

.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
