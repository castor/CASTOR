.\" Copyright (C) 2005 by CERN IT/ADC
.\" All rights reserved
.\"
.TH PRINTDISKCOPY "1castor" "$Date: 2006/08/04 14:46:47 $" CASTOR "List disk copies in a disk server or data pool"
.SH NAME
printdiskcopy
.SH SYNOPSIS
.B printdiskcopy
[
.B -h, 
.B --help
]
[
.B -i, 
.B --fileId
]
[
.B -d, 
.B --diskCopyId
]
diskServerName|dataPoolName
.SH DESCRIPTION
.B printdiskcopy 
Lists the files on a given DiskServer and give their internal
status (fileid, diskCopyId, Status, physical file name and last known filename).
Note that the last known file name may be a number when not known.
Warning: if the output list is longer than 1000 entries, only the first 1000 are
displayed and a warning is shown at the top.

.TP 20
.B \-h,\ \-\-help
Get usage information
.TP
.B \-n,\ \-\-no-castor-name
Only prints the physical file names and not the fileids and CASTOR file names.
.TP
.B \-s,\ \-\-summary
Prints a summary of the content of the selected disk server or data pool.
.TP
.B diskServerName|dataPoolName
The name of the disk server or the data pool to be queried. Machine names need to be fully qualified.


.SH EXAMPLES
.BI #\ printdiskcopy\ lxfsrk60a05.cern.ch
.fi
FileId     Status                   Physical FileName                                             Last known FileName
.fi
61263405   DISKCOPY_VALID           lxfsrk60a05.cern.ch:/srv/castor/01/05/61263405@lxs5010.450    /castor/cern.ch/some/hsm/path
.fi
61270821   DISKCOPY_VALID_TOBEMIGR  lxfsrk60a05.cern.ch:/srv/castor/01/21/61270821@lxs5010.529    /castor/cern.ch/some/hsm/path
.fi
61312846   DISKCOPY_STAGEOUT        lxfsrk60a05.cern.ch:/srv/castor/01/46/61312846@lxs5010.718    /castor/cern.ch/some/hsm/path
.ft

.BI #\ printdiskcopy\ cephpool
.fi
Warning: too many files for the selected diskserver/datapool. Showing only 1000 entries
.fi
FileId     Status                   Physical FileName                                            Last known FileName
.fi
1494825238 DISKCOPY_VALID           castorrepack@castorrepack:1494825238@castorns.4124466711     /castor/cern.ch/some/hsm/path
.fi
1494845676 DISKCOPY_VALID           castorrepack@castorrepack:1494845676@castorns.4124562277     /castor/cern.ch/some/hsm/path
.fi
1494821825 DISKCOPY_VALID           castorrepack@castorrepack:1494821825@castorns.4124456324     /castor/cern.ch/some/hsm/path
.fi
...
.ft

.BI #\ printdiskcopy\ -n\ lxfsrk60a05.cern.ch
.fi
Status                   Physical FileName
.fi
DISKCOPY_VALID           lxfsrk60a05.cern.ch:/srv/castor/01/05/61263405@lxs5010.450
.fi
DISKCOPY_VALID_TOBEMIGR  lxfsrk60a05.cern.ch:/srv/castor/01/21/61270821@lxs5010.529
.fi
DISKCOPY_STAGEOUT        lxfsrk60a05.cern.ch:/srv/castor/01/46/61312846@lxs5010.718
.ft

.BI #\ printdiskcopy\ lxfsrk60a05
.fi
Unknown diskserver/datapool lxfsrk60a05. Did you provide a fully qualified name ?
.ft

.BI #\ printdiskcopy\ -s\ cephpool
.fi
Status                   Disk copies count
.fi
DISKCOPY_FAILED          1289
.fi
DISKCOPY_INVALID         3
.fi
DISKCOPY_VALID           49904
.fi
DISKCOPY_VALID_TOBEMIGR  1
.ft

.SH NOTES
Configuration for the database access is taken from /etc/castor/ORASTAGERCONFIG.

.SH SEE ALSO
.BR adminMultiInstance(1castor)

.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
