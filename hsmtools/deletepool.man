.TH DELETEPOOL "1castor" "2011" CASTOR "stager catalogue administrative commands"
.SH NAME
deletepool \- deletes existing pools from the stager catalogue

.SH SYNOPSIS
.B deletepool
[
.BI -h
]
.BI <poolName>
[...]

.SH DESCRIPTION
.B deletepool
deletes existing pools from the CASTOR stager catalog
.LP
.BI \-h,\ \-\-help
Get usage information
.TP
.BI <poolName>
name of pools to delete. Several names can be given, space separated

.SH EXAMPLES
.nf
.ft CW
# printpool -a
     NAME MIGPRIORITY NBDISKSERVERS  FREE   %FREE  SIZE SVCCLASSES    ID EXTUSER EXTPOOL
----------------------------------------------------------------------------------------
  default           0             1     -       -     -    default     6       -       -
diskpool1           0             0     -       -     -          -     8       -       -
diskpool2           0             2     -       -     -          -     8       -       -
     test           0             2 28GiB 100.0 % 28GiB          - 53205    test    test
datapool1           0             2 13GiB 100.0 % 13GiB          - 53208   user1   pool1
datapool2           0             0 34GiB 100.0 % 34GiB          - 53213   user2   pool2

# deletepool diskpool1
successfully dropped the following disk pools : diskpool1

# deletepool default
Pools cannot be dropped when used by some service classes and
  - disk pool default is in use by service class dev
You may want to use modifysvcclass to correct this

# deletepool diskpool2
Pools cannot be dropped if not empty and
  - disk pool diskpool2 still has 10 filesystem(s)
You may want to use modifyDiskServer to correct this

# deletepool datapool1
Pools cannot be dropped if not empty and
  - data pool datapool1 still has 2 diskserver(s)
You may want to use modifyDiskServer to correct this

# deletepool datapool2
successfully dropped the following data pools : datapool2

.SH NOTES
This command requires database client access to the stager catalogue.
Configuration for the database access is taken from castor.conf.

.SH SEE ALSO
.BR enterpool
.BR modifypool
.BR printpool
.BR deletesvcclass
.BR deletediskserver
.BR adminMultiInstance

.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
