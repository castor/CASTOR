.TH PRINTPOOL "1castor" "2011" CASTOR "Prints out the given pool(s)"
.SH NAME
printpool
.SH SYNOPSIS
.B printpool
[
.BI -h
]
<poolName>
[...]

.SH DESCRIPTION
.B printpool
prints out definitions of pools (disk pools as well as data pools)
.LP
.BI \-h,\ \-\-help
Get usage information
.TP
.BI <poolName>
when disk/data pool names are given, only the definitions of the listed pools are printed.
If no pool name is given, all pool definitions are printed.

.SH EXAMPLES
.nf
.ft CW
# printpool
   NAME MIGPRIORITY NBDISKSERVERS  FREE  MINFREE MAXFREE   %FREE  SIZE SVCCLASSES    ID EXTUSER  EXTPOOL
--------------------------------------------------------------------------------------------------------
default           0             1     -        -       -       -     -    default     6       -        -
  extra           0             1     -        -       -       -     -   diskonly     8       -        -
   test           0             3 28GiB     2.0%    5.0%  100.0% 28GiB          - 53205  castor testpool

# printpool extra nonexisting
 NAME MIGPRIORITY NBDISKSERVERS FREE  MINFREE MAXFREE %FREE SIZE SVCCLASSES ID EXTUSER  EXTPOOL
-----------------------------------------------------------------------------------------------
extra           0             1    -        -       -     -    -   diskonly  8       -        -
WARNING : the following pools do not exist : nonexisting

.SH NOTES
This command requires database client access to the stager catalogue.
Configuration for the database access is taken from castor.conf.

.SH SEE ALSO
.BR enterpool
.BR modifypool
.BR deletepool
.BR printsvcclass
.BR printdiskserver
.BR adminMultiInstance

.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
