.\" ******************************************************************************
.\"                      repack
.\"
.\" This file is part of the Castor project.
.\" See http://castor.web.cern.ch/castor
.\"
.\" Copyright (C) 2003  CERN
.\" This program is free software; you can redistribute it and/or
.\" modify it under the terms of the GNU General Public License
.\" as published by the Free Software Foundation; either version 2
.\" of the License, or (at your option) any later version.
.\" This program is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\" You should have received a copy of the GNU General Public License
.\" along with this program; if not, write to the Free Software
.\" Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
.\"
.\" man page for the repack command.
.\"
.\" @author Castor Dev team, castor-dev@cern.ch
.\" *****************************************************************************/
.TH repack "8castor" "July, 2011" CASTOR "Moves data away from a tape"
.SH NAME
repack \- Move files from tape(s) to other tape destination
.SH SYNOPSIS
.B repack
.BI -h
.br
.B repack
.BI \-V\ <VID>[:<VID>[...]]\ \-o\ OutputServiceClass
.br
.B repack
.BI \-\-bulkvolumeid\ <file>\ \-o\ OutputServiceClass
.br
.B repack
.BI \-R\ <VID>[:<VID>[...]]
.br
.B repack
.BI \-\-bulkdelete\ <file>
.br
.B repack
.BI \-s|--statusAll\ [-x|--extra]
.br
.B repack
.BI \-S\ <VID>[:<VID>[...]]\ [-x|--extra]
.br
.B repack
.BI \-e\ <VID>[:<VID>[...]]\ [\-n\ <nbErrorsDisplayed>]
.br

.SH DESCRIPTION
.B repack
allows to move files from one or several tapes to new media.
It also allows to get the status overview of the ongoing repack activity in the system.
Note that only volumes having
.B FULL
status may be repacked.

When the repack process has been successfully completed (FINISHED status), the volume can be
reclaimed using the reclaim command.

Note that repack will also trigger the remigration/repair of missing segments for the files that will
be repacked. A typical case is when the second copy of a two copies file has been lost and
the tape containing the first copy is repacked.
However, segments on EXPORTED tapes will not be considered as lost. This is different behavior
from standard recalls triggered e.g. by stager_get for which they will be.

.SH OPTIONS

.TP
.BI \-h,\ \-\-help
displays command usage help.
.TP
.BI \-V,\ \-\-volumeid
submits the repacking of the given tapes and returns immediately. The tapes are given by their VID
and the list is colon separated. The actual repack processes are asynchronously started, one by one,
by the database.
.TP
.BI \-\-bulkvolumeid
submits the repacking of the tapes given in the provided file. The file must contain a list of VIDs,
one per line. Spaces and anything appearing after the '#' sign on a given line are ignored.
.TP
.BI \-o,\ \-\-svcclass
the service class to be used for repacking the given tapes.
.TP
.BI \-R,\ \-\-delete
cancels the repacking of the given tapes. The tapes are given by their VID and the list is colon separated.
Note that the cancelation may be refused in case too many repacks are already being launched or aborted.
The maximum number is defined in the Repack/MaxNbConcurrentClients entry of the CastorConfig table
in the stager database. The default value is 5.
.TP
.BI \-\-bulkdelete
cancels the repacking of the tapes given in the provided file. The file must contain alist of VIDs,
one per line. Spaces and anything appearing after the '#' sign on a given line are ignored.
Note that the cancelation may be refused in case too many repacks are already being launched or aborted.
The maximum number is defined in the Repack/MaxNbConcurrentClients entry of the CastorConfig table
in the stager database. The default value is 5.
.TP
.BI \-s,\ \-\-statusAll
shows the status of all ongoing and finished repacks. If multiple repack requests were issued for a given
tape, only the last request is shown. Note that old completed repacks are automatically removed
from the database and are not shown any longer.
.TP
.BI \-S,\ \-\-status
shows the status of ongoing and finished repacks for the given tape(s). The tape(s) is(are) given by their
VID and the list is colon separated. As opposed to the \-s option, here the entire history is shown.
.TP
.BI \-x,\ \-\-extra
shows the status of ongoing and finished repacks as requested with the -s or -S options, including extra
detailed figures for the count of ongoing or failed recalls and migrations, and a completion percentage.
.TP
.BI \-e,\ \-\-errors
displays the errors that occured during the repack of a given tape, file by file. By default, the number of
errors displayed is limited to 10. This can be changed using the \-n option
.TP
.BI \-n,\ \-\-nbErrorsDisplayed
in conjunction with \-e, sets the maximum number of errors to display.

.SH EXAMPLE
.nf
.ft CW
> repack -V V00000:V10025:V13049 -o default
vmgrlisttape: No such tape
Caught exception when executing vmgrlisttape for tape V00000, skipping
Tape V10025 is not marked FULL, ignoring it
Tape V13049 is already being repacked by user root, ignoring it

> repack -V I10551:I10552 -o default
Repack submitted for tape I10551
Repack submitted for tape I10552

> repack -s
=====================================================================================================================
SubmitTime        RepackTime             User                      Machine      Vid      Total        Size     Status
---------------------------------------------------------------------------------------------------------------------
11-Jun-13 18:57       1mn20s    itglp@CERN.CH             lxc2dev2.cern.ch   V21002          4     3.29KiB   FINISHED
10-Jun-13 15:42       1mn40s             root             lxc2dev2.cern.ch   V22003          1     2.93KiB   FINISHED
---------------------------------------------------------------------------------------------------------------------
11-Jun-13 19:29            -                -                        TOTAL        2          5     6.22KiB   FINISHED

> repack -sx
=====================================================================================================================================================================
SubmitTime        RepackTime             User                      Machine      Vid      Total        Size  toRecall    toMigr    Failed  Migrated  Compl%     Status
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
11-Jun-13 18:57       1mn20s    itglp@CERN.CH             lxc2dev2.cern.ch   V21002          4     3.29KiB         0         0         0         4    100%   FINISHED
10-Jun-13 15:42       1mn40s             root             lxc2dev2.cern.ch   V22003          1     2.93KiB         0         0         0         1    100%   FINISHED
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
11-Jun-13 19:29            -                -                        TOTAL        2          5     6.22KiB         0         0         0         5    100%   FINISHED

> repack -S V21002 -x
=====================================================================================================================================================================
SubmitTime        RepackTime             User                      Machine      Vid      Total        Size  toRecall    toMigr    Failed  Migrated  Compl%     Status
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
11-Jun-13 18:57       1mn20s    itglp@CERN.CH             lxc2dev2.cern.ch   V21002          4     3.29KiB         0         0         0         4    100%   FINISHED
10-Jun-13 15:00       1mn30s    itglp@CERN.CH             lxc2dev2.cern.ch   V21002          4     3.29KiB         0         0         2         2    100%     FAILED
---------------------------------------------------------------------------------------------------------------------------------------------------------------------

> repack -S V21003
=====================================================================================================================
SubmitTime        RepackTime             User                      Machine      Vid      Total        Size     Status
---------------------------------------------------------------------------------------------------------------------
11-Jun-13 17:43       1mn10s    itglp@CERN.CH             lxc2dev2.cern.ch   V21003          4     3.29KiB    ONGOING
---------------------------------------------------------------------------------------------------------------------

> repack -R V21003

> repack -S V21003
=====================================================================================================================
SubmitTime        RepackTime             User                      Machine      Vid      Total        Size     Status
---------------------------------------------------------------------------------------------------------------------
11-Jun-13 17:43       1mn15s    itglp@CERN.CH             lxc2dev2.cern.ch   V21003          4     3.29KiB   ABORTING
---------------------------------------------------------------------------------------------------------------------

> repack -S V21003 -x
=====================================================================================================================================================================
SubmitTime        RepackTime             User                      Machine      Vid      Total        Size  toRecall    toMigr    Failed  Migrated  Compl%     Status
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
11-Jun-13 17:43       1mn20s    itglp@CERN.CH             lxc2dev2.cern.ch   V21003          4     3.29KiB         0         0         1         3    100%    ABORTED
---------------------------------------------------------------------------------------------------------------------------------------------------------------------

> repack -e I10551

     *** Tape  I10551  ***

--------------------------------------------------------------------------------------------
Fileid         CopyNo         ErrorCode      ErrorMessage
--------------------------------------------------------------------------------------------
5000157562     n/a            1701           Aborted explicitely
5000157580     n/a            1701           Aborted explicitely
5000156311     n/a            1701           Aborted explicitely
5000157587     n/a            1701           Aborted explicitely
5000157605     n/a            1701           Aborted explicitely
5000157611     n/a            1701           Aborted explicitely
5000157625     n/a            1701           Aborted explicitely
5000157632     n/a            1701           Aborted explicitely
5000157638     n/a            1701           Aborted explicitely
5000157640     n/a            1701           Aborted explicitely

Output restricted to 10 errors. There are more errors for this tape

> repack -e I10551 -n 2

     *** Tape  I10551  ***

--------------------------------------------------------------------------------------------
Fileid         CopyNo         ErrorCode      ErrorMessage
--------------------------------------------------------------------------------------------
5000157562     n/a            1701           Aborted explicitely
5000157580     n/a            1701           Aborted explicitely

Output restricted to 2 errors. There are more errors for this tape

.SH NOTES
This command requires database client access to the stager catalogue.
Configuration for the database access is taken from castor.conf.

.SH SEE ALSO
.BR stager_get(1castor) ,

.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch
