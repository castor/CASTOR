.TH PRINTSVCCLASS "1castor" "2011" CASTOR "Prints out the given service class(es)"
.SH NAME
printsvcclass
.SH SYNOPSIS
.B printsvcclass
[
.BI -h
]
<svcClassName>
[...]


.SH DESCRIPTION
.B printsvcclass
prints out definitions of the given services classes
.LP
.BI \-h,\ \-\-help
Get usage information
.TP
.BI <svcClassName>
when service class names are given, only the definitions of the listed service class are printed.
If no service class name is given, all service class definitions are printed.

.SH EXAMPLES
.nf
.ft CW
# printsvcclass
    NAME DEFAULTFILESIZE FAILJOBSWHENNOSPACE REPLICANB FORCEDFILECLASS DISK1BEHAVIOR GCPOLICY     POOLS REPLICATETO ID LASTEDITOR          LASTEDITION
------------------------------------------------------------------------------------------------------------------------------------------------------
 default           10MiB                True         1               -         False        -   default           - 15   sponcec3 30-Aug-2011 17:02:00
     dev           10MiB                True         1               -         False        -     extra           - 16   sponcec3 30-Aug-2011 17:02:05
diskonly           10MiB                True         1            temp          True        -     extra           - 17   sponcec3 30-Aug-2011 17:02:10

# printsvcclass dev nonexisting
NAME DEFAULTFILESIZE FAILJOBSWHENNOSPACE REPLICANB FORCEDFILECLASS DISK1BEHAVIOR GCPOLICY     POOLS REPLICATETO ID LASTEDITOR          LASTEDITION
--------------------------------------------------------------------------------------------------------------------------------------------------
 dev           10MiB                True         1               -         False        -     extra           - 16   sponcec3 30-Aug-2011 17:02:05
WARNING : the following service classes do not exist : nonexisting

.SH NOTES
This command requires database client access to the stager catalogue.
Configuration for the database access is taken from castor.conf.

.SH SEE ALSO
.BR entersvcclass
.BR modifysvcclass
.BR deletesvcclass
.BR printpool
.BR adminMultiInstance

.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
