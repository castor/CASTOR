.TH MODIFYPOOL "1castor" "2011" CASTOR "stager catalogue administrative commands"
.SH NAME
modifypool \- modifies an existing pool in the stager catalog
.SH SYNOPSIS
.B modifypool
[
.BI -h
]
[
.BI -p
<priority>
]
<poolName>

.SH DESCRIPTION
.B modifypool
modifies an existing pool in the CASTOR stager catalog

.TP
.BI \-h,\ \-\-help
Get usage information
.TP
.BI \-p,\ \-\-migrationpriority\ <priority>
Migration priority for this pool. Bigger priority pools are used first for migrations to tape. Default priority is 0
.TP
.BI <poolName>
Name of the pool to modify.

.SH EXAMPLES
.nf
.ft CW
# printpool
   NAME MIGPRIORITY NBDISKSERVERS  FREE %FREE  SIZE      SVCCLASSES      ID EXTUSER  EXTPOOL
--------------------------------------------------------------------------------------------
default           0             1     -     -     - 2copies,default       6      -        -
  extra           0             1     -     -     -        diskonly       8      -        -
   test           0             0 45TiB 99.9% 45TiB            test   61592  castor testpool

# modifypool -p 12 default
modified pool default successfully

# printpool
   NAME MIGPRIORITY NBDISKSERVERS  FREE %FREE  SIZE      SVCCLASSES      ID EXTUSER  EXTPOOL
--------------------------------------------------------------------------------------------
default          12             1     -     -     - 2copies,default       6      -        -
  extra           0             1     -     -     -        diskonly       8      -        -
   test           0             0 45TiB 99.9% 45TiB            test   61592  castor testpool

.SH NOTES
This command requires database client access to the stager catalog and nameserver DBs.
Configuration for the database accesses is taken from castor.conf.

.SH SEE ALSO
.BR enterpool
.BR deletepool
.BR printpool
.BR modifymigrationroute
.BR adminMultiInstance

.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
