.TH C2CEPHSTAT "1castor" "2017" CASTOR "Prints out the given object in a ceph pool"
.SH NAME
c2cephstat
.SH SYNOPSIS
.B c2cephstat
<filename>
[
.B setchecksum <checksumvalue>
]
.SH DESCRIPTION
.B c2cephstat
retrieves object information from a ceph pool. 
It can also set a new checksum for the object.
.LP
.SH EXAMPLES
.nf
.ft CW

# c2cephstat twigs@alice1:1572269809@castorns.27206292204
MTIME                       SIZE        CSTYPE  CSVALUE    STRIPEROBJSIZE  STRIPECOUNT  STRIPEUNIT  STRIPESIZE  FILENAME
2017-03-22_21:35:53.000000  1858563016  ADLER32  e2e4645d  33554432        4            33554432    1858563016  1572269809@castorns.27206292204

# c2cephstat twigs@alice1:1572269809@castorns.27206292204 setchecksum e2e4645d
OK

.SH NOTES
This command needs to be executed in a castor ceph gateway.

.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
