.TH NSLISTSEGS "1castor" "2014" CASTOR "Prints out the segments of a tape and some summary statistics"
.SH NAME
nslistsegs
.SH SYNOPSIS
.B nslistsegs
[
.BI -h
]
-V <VID>

.SH DESCRIPTION
.B nslistsegs
prints out the list of segments in the given tape. The list is retrieved from the nameserver catalogue in bulk with a direct
query, and no information is given about the files beyond their file ids for performance reasons.
If the tape contains more than 2 segments, a summary line is printed as well with some statistics including the total
size of the segments, the average creation time and its standard deviation in the form of a time interval.
.TP
.BI \-h,\ \-\-help
Get usage information
.TP
.BI \-V,\ \-\-VID
The Volume ID of the tape to be listed

.SH EXAMPLES
.nf
.ft CW
# nslistsegs -V T80784
STATUS      FILEID COPYNO   FSEQ  GID        SIZE COMPRESSION  CHECKSUM          CREATIONTIME  LASTMODIFICATIONTIME
-------------------------------------------------------------------------------------------------------------------
     -   161824199      1      1 1665   201636792         126  547421A6  04-Nov-2007 12:42:44  11-Jan-2014 11:20:30
     -   161795001      1      2 1665     3228132         126  30F15D30  04-Nov-2007 08:44:34  11-Jan-2014 11:20:30
...
     -   696028399      1 154634 1665    11663567     3588789  853505DB  30-Mar-2011 06:30:53  23-Jan-2014 05:02:34
     -   695960979      1 154635 1665    11357774     3594232  E7883BF5  30-Mar-2011 04:40:20  23-Jan-2014 05:02:34
     -   696015257      1 154636 1665    11662226     3588377  7C64BD82  30-Mar-2011 06:15:00  23-Jan-2014 05:02:34
     - --SUMMARY--      - 154636    -        9TiB           -         -  01-Feb-2008 06:29:58    +/- 1135d2h47mn49s

.SH NOTES
This command requires database client access to the nameserver catalogue.

.SH SEE ALSO
.BR nslisttape(1) ,
.BR vmgrlisttape(1)

.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
