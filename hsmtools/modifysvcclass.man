.TH MODIFYSVCCLASS "1castor" "2011" CASTOR "stager catalogue administrative commands"
.SH NAME
modifysvcclass \- modifies an existing service class in the stager catalog
.SH SYNOPSIS
.B modifysvcclass
[
.BI -h
]
[
.BI --defaultfilesize
<defaultFileSize>
]
[
.BI --replicanb
<replicaNb>
]
[
.BI --failjobswhennospace
<bool>
]
[
.BI --disk1behavior
<bool>
]
[
.BI --forcedfileclass
<forcedFileClass>
]
[
.BI --gcpolicy
<gcPolicyName>
]
[
.BI --addpools
<poolName>:[...]
]
[
.BI --removepools
<poolName>:[...]
]
[
.BI --replicateto
<svcClassName>
]
<svcClassName>


.SH DESCRIPTION
.B modifysvcclass
modifies an existing service class in the CASTOR stager catalog
.TP
.BI \-h,\ \-\-help
Get usage information
.TP
.BI \-\-defaultfilesize <defaultFileSize>
If given, modifies the default file size allocation for write requests not specifying any filesize.
The value can be given using the standard K/M/G/T extensions, with or without B (i.e. both KB and K are accepted).
These have the ISO meaning of powers of 10. Ki/Mi/Gi/Ti[B] extensions are also accepted and deal with powers of 2.
.TP
.BI \-\-replicanb <replicaNb>
If given, modifies the number of disk resident replicas for a given CASTOR file
in this service class.
.TP
.BI \-\-failjobswhennospace <boolean value>
If given, modifies whether jobs requesting new space are failed when the pool(s) connected
with this service class is(are) full. Example of supported values are yes/no, y/n, true/false, t/f, 1/0.
.TP
.BI \-\-disk1behavior <boolean value>
If given, modifies whether this service class has Disk1 behavior, which means that garbage
collection is disabled and the flag FailJobsWhenNoSpace is set to yes.
Example of supported values are yes/no, y/n, true/false, t/f, 1/0.
.TP
.BI \-\-forcedfileclass <forcedFileClassName>
If given, modifies the forced file class of files written to this service class to the given value.
The forced file class overrides the file class that the user specifies. This parameter allows
specifying for this service class any TapeN retention policy, where N is the number of
tape copies of the forced file class.
In order to disable the forcing, just provide an empty class name. Do not for get the quotes
so that the shell sees that empty argument
.TP
.BI \-\-gcpolicy <gcPolicyName>
If given, modifies the name of the garbage collection policy to be used for this service class.
This value is irrelevant if Disk1Behavior is set to yes.
.TP
.BI \-\-addpools <pool1:pool2:...>
If given, adds the given pools to the set that this service class is allowed to use.
More than one pool can be specified, colon separated. All specified pools must already exist.
.TP
.BI \-\-removepools <pool1:pool2:...>
If given, removes the given pools from the set that this service class is allowed to use.
More than one pool can be specified, colon separated. All specified pools must already exist.
.TP
.BI \-\-replicateto\ <svcClassName>
Specifies the name of a service class where the files written to the current one should be replicated.
The replication will happen automatically at the closing of the files.
In order to stop any replication, give the empty string : ''
.TP
.BI <svcClassName>
name of the service class to modify.

.SH EXAMPLES
.nf
.ft CW
#printsvcclass default
   NAME DEFAULTFILESIZE FAILJOBSWHENNOSPACE REPLICANB FORCEDFILECLASS DISK1BEHAVIOR GCPOLICY     POOLS REPLICATETO ID LASTEDITOR          LASTEDITION
-----------------------------------------------------------------------------------------------------------------------------------------------------
default           10MiB                True         1               -         False        -   default           - 15   sponcec3 30-Aug-2011 17:02:00
test               1MiB                True         1               -         False        -   default           - 16   sponcec3 30-Aug-2011 17:02:03

# modifysvcclass --defaultfilesize 20GiB --replicanb 2 --gcpolicy dropall --forcedfileclass temp --removepools default --replicateto test default
modified service class default successfully

# printsvcclass test
   NAME DEFAULTFILESIZE FAILJOBSWHENNOSPACE REPLICANB FORCEDFILECLASS DISK1BEHAVIOR GCPOLICY     POOLS REPLICATETO ID LASTEDITOR          LASTEDITION
-----------------------------------------------------------------------------------------------------------------------------------------------------
default           10MiB                True         2            temp         False  dropall         -        test 15   sponcec3 30-Aug-2011 17:09:44
test               1MiB                True         1               -         False        -   default           - 16   sponcec3 30-Aug-2011 17:02:03

# modifysvcclass --forcedfileclass '' --addpools default --replicateto '' default
modified service class default successfully

# printsvcclass test
   NAME DEFAULTFILESIZE FAILJOBSWHENNOSPACE REPLICANB FORCEDFILECLASS DISK1BEHAVIOR GCPOLICY     POOLS REPLICATETO ID LASTEDITOR          LASTEDITION
-----------------------------------------------------------------------------------------------------------------------------------------------------
default           10MiB                True         2               -         False  dropall   default           - 15   sponcec3 30-Aug-2011 17:10:39
test               1MiB                True         1               -         False        -   default           - 16   sponcec3 30-Aug-2011 17:02:03

.SH NOTES
This command requires database client access to the stager catalog and nameserver DBs.
Configuration for the database accesses is taken from castor.conf.

.SH SEE ALSO
.BR entersvcclass
.BR deletesvcclass
.BR printsvcclass
.BR modifymigrationroute
.BR adminMultiInstance

.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
