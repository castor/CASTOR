.TH MODIFYRECALLGROUP "1castor" "2011" CASTOR "stager catalogue administrative commands"
.SH NAME
modifyrecallgroup \- modifies an existing recall group in the stager catalog
.SH SYNOPSIS
.B modifyrecallgroup
[
.BI -h
]
[
.BI --nbdrives
<nbDrives>
]
[
.BI --minamountdata
.B <minAmountDataForAMount>
]
[
.BI --minnbfiles
.B <minNbFilesForAMount>
]
[
.BI --maxfileage
.B <maxFileAgeBeforeForcedMount>
]
[
.BI --priority
.B <VDQMpriority>
]
<recallGroupName>

.SH DESCRIPTION
.B modifyrecallgroup
modifies an existing recall group in the CASTOR stager catalog

The parameters of the recallgroup define the policy used to trigger tape mounts for recalls.
The behavior is the following :
  - never more than <nbDrives> drives can be used concurrently
  - a new tape will be mounted whenever we have either more than <minAmountDataForAMount> of data or <minNbFilesForAMount> files waiting for recall. In practice, if some migrations are already running, a new one is trigerred if one of these 2 numbers will still be exceeded per mount, even with the new mount.
  - a tape mount will be forced if nothing is mounted and one file becomes older than maxFileAgeBeforeForcedMount
  - if a tape mount is triggered, VDQM will be called to get a drive with the given priority
.TP
.BI \-h,\ \-\-help
Get usage information
.TP
.BI \-\-nbdrives <nbDrives>
The maximum number of tape drives this recall group is allowed to use concurrently
for migration.
.TP
.BI \-\-minamountdata <minAmountDataForAMount>
The minimum amount of data needed to trigger a new mount in bytes.
The value can be given using the standard K/M/G/T extensions, with or without B (i.e. both KB and K are accepted).
These have the ISO meaning of powers of 10. Ki/Mi/Gi/Ti[B] extensions are also accepted and deal with powers of 2.
.TP
.BI \-\-minnbfiles <minNbFilesForAMount>
The minimum number of files needed to trigger a new mount.
.TP
.BI \-\-maxfileage <maxFileAgeBeforeForcedMount>
The maximum age of a file before a new mount is triggered.
The value can be given using the extensions s/m/mn/h/d for seconds, minutes(both m and mn), hours and days.
.TP
.BI \-\-priority <VDQMpriority>
The priority to be used when accessing VDQM to get a drive.
.TP
.BI <recallGroupName>
name of the recall group to modify.

.SH EXAMPLES
.nf
.ft CW
# printrecallgroup
          NAME NBDRIVES MINAMOUNTDATA MINNBFILES MAXFILEAGE VDQMPRIORITY     ID            LASTEDITOR          LASTEDITION
----------------------------------------------------------------------------------------------------------------------
newrecallgroup        0        100GiB       1000        12h            0 263333                  root 30-May-2012 17:29:12
       default       20        100GiB       1000        12h            0 133068 2.1.13 upgrade script 03-May-2012 16:44:50

# modifyrecallgroup --nbdrives 1 --minamountdata 57MiB --minnbfiles 234 --maxfileage 1d5h --priority 2 newrecallgroup
modified recall group newrecallgroup successfully

# printrecallgroup
          NAME NBDRIVES MINAMOUNTDATA MINNBFILES MAXFILEAGE VDQMPRIORITY     ID            LASTEDITOR          LASTEDITION
----------------------------------------------------------------------------------------------------------------------
newrecallgroup        1         57MiB        234       1d5h            2 263333                  root 06-Jun-2012 09:35:11
       default       20        100GiB       1000        12h            0 133068 2.1.13 upgrade script 03-May-2012 16:44:50

.SH NOTES
This command requires database client access to the stager catalog and nameserver DBs.
Configuration for the database accesses is taken from castor.conf.

.SH SEE ALSO
.BR enterrecallgroup
.BR deleterecallgroup
.BR printrecallgroup
.BR modifyrecalluser
.BR adminMultiInstance

.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
