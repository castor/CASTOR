.\" Copyright (C) 2002 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH RMCD "8castor" "$Date: 2009/08/18 09:43:01 $" CASTOR "rmc Administrator Commands"
.SH NAME
rmcd \- start the Remote Media Changer daemon
.SH SYNOPSIS
.B rmcd
.I smc_ldr
.SH DESCRIPTION
.LP
The
.B rmcd
command starts the Remote Media Changer daemon.
This command is usually executed at system startup time
.RB ( /etc/rc.local ).
.LP
.I smc_ldr
is the picker device as defined in /dev.
.LP
If the Remote Media Changer daemon is not active,
the requests are automatically retried by the client API.
.LP
All error messages and statistical information are kept in a log.
.LP
The Remote Media Changer daemon listen port number can be defined on client
hosts and on the Remote Media Changer host itself in either of the following
ways:
.RS
.LP
setting an environment variable RMC_PORT
.RS
.HP
setenv RMC_PORT 5014
.RE
.LP
an entry in
.B /etc/castor/castor.conf
like:
.RS
.HP
RMC	PORT	5014
.RE
.LP
an entry in
.B /etc/services
like:
.RS
.HP
rmc           5014/tcp                        # CASTOR Remote Media Changer
.RE
.RE
.LP
If none of these methods is used, the default port number is taken from the
definition of RMC_PORT in rmc_constants.h.
.LP
.RE
.RE
.LP
In the log each entry has a timestamp.
For each user command there is one message RMC92 giving information about
the requestor (hostname, uid, gid) and one message RMC98 giving the command
itself.
The completion code of the command is also logged.
.SH FILES
.TP 1.5i
.B /var/log/castor/rmcd.log
.SH EXAMPLES
.TP
Here is a small log:
.nf
12/06 11:40:58  7971 rmc_srv_mount: RMC92 - mount request by 0,0 from tpsrv015.cern.ch
12/06 11:40:58  7971 rmc_srv_mount: RMC98 - mount 000029/0 on drive 2
12/06 11:41:08  7971 rmc_srv_mount: returns 0
12/06 11:42:43  7971 rmc_srv_unmount: RMC92 - unmount request by 0,0 from tpsrv015.cern.ch
12/06 11:42:43  7971 rmc_srv_unmount: RMC98 - unmount 000029 2 0
12/06 11:42:48  7971 rmc_srv_unmount: returns 0
.fi
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
