import re
from castor_base_file_parser import BaseFileParser
from slplib.utils import ConfigError

class XrootdFileHeaderParser(BaseFileParser):
    """
    A FileParser which supports the parsing of XROOTD headers
    """

    def __init__(self, config):
        BaseFileParser.__init__(self)

        # Regular expression to parse the header components of an rsyslog
        # formatted log message.
        self._regexhdr = re.compile(r"""
            (\d{6}) \s+                            # DATE
            (\d\d:\d\d:\d\d) \s+                   # TIME
            time\s*\=\s*(\d+).(\d+)                # TIMESTAMP (time='...')
            (.*)                                   # Body
            """, re.VERBOSE | re.MULTILINE)

        self.hostname = config['hostname']
        self.castor_instance = config['castor_instance']
        if not self.castor_instance:
            raise ConfigError('Empty castor_instance entry in configuration. Giving up')

    def parseline(self, dic, data):
        """
        Parse the given message and return a dictionnary containing the key-value
        pairs.
        """

        try:
            # Try to parse the message header using the standard format:
            # The when using the group method, the groups are 1-indexed
            match = self._regexhdr.match(data)
            if match:
                # Ignore lines of xrootd files that do not contain a level= entry
                if match.group(5).find('level=') < 0:
                    return {}, ''
                usecs = match.group(4)
                result = { 'TIMESTAMP'      : self._get_timestamp(match.group(3), usecs), # should be 2015-08-17T14:52:17.210950+02:00, but we don't have TIMEZONE, so I shall get +02:00 using 'import time...'
                           'USECS'          : usecs, # after the dot in 'time=...'
                           'EPOCH'          : match.group(3), # i have it already
                           'HOSTNAME'       : self.hostname, # it is find like this
                           'DAEMON'         : "xrootd", # not hardcoded in a '~near' future
                           'PID'            : 0, # not hardcoded in a '~near' future
                           'INSTANCE'       : self.castor_instance }
                return dict(result.items() + dic.items()), match.group(5)
            return {}, ''

        except Exception, e:
            # Failed to parse the message, skip it
            return {}, ''

