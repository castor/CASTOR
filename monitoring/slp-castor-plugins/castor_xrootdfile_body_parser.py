import re
from castor_base_file_parser import BaseFileParser

class XrootdFileBodyParser(BaseFileParser):
    """
    A FileParser which supports the parsing of Xrootd files
    """
    
    # Fields and Values that have to be well formated
    COMPOUNDED_FIELDS = ['opaque']
    KEY_FIELDS = {'path': 'Path', 'level': 'LVL', 'logid': 'REQID'}

    def __init__(self, config):
        BaseFileParser.__init__(self)


    def _parse_opaque(self, compound):
        partial_result = dict()
        for component in compound.split('&'):
            try:
                k = component[:component.index('=')]
                v = component[component.index('=')+1:]
            except ValueError:
                k = component
                v = ''
            partial_result[k] = v

        return partial_result
        

    def parseline(self, dic, data):
        """
        (1) Check for fields in the dictionary that might be composed of sub-fields. 
        (2) Check that the important fields keys are well formated
        (3) Check that values are well formated
        * IT ASSUMES THAT THE PARAMETER data IS EMPTY as RSyslogFileBodyParser has already been run before!
        """
        try:
            for k in self.COMPOUNDED_FIELDS: # (1)
                if k in dic:
                    v = dic[k]
                    if v[-1] == ',':
                        partial_result = self._parse_opaque(v[:-1])
                    else:
                        partial_result = self._parse_opaque(v)
                    
                    dic.pop(k)
                else:
                    pass
            for field_key in self.KEY_FIELDS: # (2)
		if field_key in dic:
                    dic[self.KEY_FIELDS[field_key]] = dic[field_key]
                    del dic[field_key]

            if 'Path' in dic and dic['Path'][-1] == ',':
                dic['Path'] = dic['Path'][:-1]
            if 'castor.pfn1' in partial_result:
                pfn = partial_result['castor.pfn1']
                dic['NSFILEID'] = pfn[pfn.rfind('/')+1:pfn.rfind('@')]
                if ':' in dic['NSFILEID']:     # files stored on a data pool have a prefix
                    dic['NSFILEID'] = dic['NSFILEID'][dic['NSFILEID'].find(':')+1:]
            if 'msg' in dic:
                dic['MSG'] = dic['msg']    # this puts the message in the right column
            elif 'func' in dic:
                dic['MSG'] = 'Function=' + dic['func']   # when msg is missing use the function as MSG
            return dict(partial_result.items() + dic.items()), ""

        except Exception, e:
            # If we are here we failed, probably because we have an "old" format log message.
            # We try to parse it with old format, otherwise just give up...
            return dic, data


