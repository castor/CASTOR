import re
from castor_base_file_parser import BaseFileParser

class XrootdFileHeaderParser(BaseFileParser):
    """
    A FileParser which supports the parsing of RSYSLOG_FileFormat based log 
    files. E.g: 2011-01-09T04:08:33.759308+01:00 c2atlassrv102 stagerd[28041]:
    """

    def __init__(self, config):
        BaseFileParser.__init__(self)

        # Regular expression to parse the header components of an rsyslog
        # formatted log message.
        self._regexhdr = re.compile(r"""
            (\d{6}) \s+                            # DATE
            (\d\d:\d\d:\d\d) \s+                   # TIME
            (\d+) \s+                              # PID ?
            (.*)                                   # Body
            """, re.VERBOSE | re.MULTILINE)

    def parseline(self, dic, data):
        """
        Just ignore these lines. These are "unparsable" xrootd logs.
        """
        for match in self._regexhdr.findall("".join(data)):
            return {}, ''
        return dic, data
