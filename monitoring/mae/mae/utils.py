#!/usr/bin/python
# -*- coding: utf-8 -*-

#******************************************************************************
#                     mae-consumer
#
# Copyright (C) 2013  CERN
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# @author: Castor dev team
#******************************************************************************

"""
Module providing utils functions for the MAE
"""

# Imports
import os
import sys
import logging
import subprocess
import re
import select
import inspect

def redirect_output(log_file_path):
    """ Simply redirect stdout and stderr to the log file. """
    out_log = file(log_file_path, 'a+')
    sys.stdout.flush()
    sys.stderr.flush()
    os.close(sys.stdout.fileno())
    os.close(sys.stderr.fileno())
    os.dup2(out_log.fileno(), sys.stdout.fileno())
    os.dup2(out_log.fileno(), sys.stderr.fileno())



def print_usage(script_name):
    """
    Simply print the usage help message.

    :param script_name: sys.argv[0], name of the calling script
    """
    sys.exit("Usage : python " + script_name + " /path/to/config/file.conf")
    
    
def clean_folder(folder, extension=None):
    """ 
    Remove files from folder. If extension is set, just file with same extension will be deleted.
    
    :param folder: Folder to clean
    :param extension: Filter on the extension. Default None
    """
    
    filelist = os.listdir(folder)
    
    if extension is not None :
        filelist = [ f for f in filelist if f.endswith(extension) ] 
    
    for f in filelist:
        os.remove(folder+"/"+f)
        
def _popenSTD_to_func(popen_process, func):  
    #TODO Verify results ...
    stdout = []
    stderr = []
    while True:
        reads = [popen_process.stdout.fileno(), popen_process.stderr.fileno()]
        ret = select.select(reads, [], []) #(reading, writting, exceptions)
    
        for fd in ret[0]:
            if fd == popen_process.stdout.fileno():
                read = popen_process.stdout.readline().rstrip('\n')
                stdout.append(read)
                func(read)
            if fd == popen_process.stderr.fileno():
                read = popen_process.stderr.readline().rstrip('\n')
                stderr.append(read)
                func(read)
    
        if popen_process.poll() != None:
            break
    stdout = "\n".join(stdout)
    stderr = "\n".join(stderr)
    return (stdout, stderr)
    
    
            
def execShellCommand(command, callbackSTD=None): 
    """ Execute a shell command.
    The execution is synchronous. The execution will be wait to get the return code !
    :param command: Commande to execute.
    :param callbackSTD: callback function who will receive all lines from stdout and stderr
    :return: if asynchronous return Popen object else tuple(returnCode, stdout, stderr)
    """               
    logging.info("Executing command >>> : "+command+"\n")           
    command_result = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    
    #if asynchronous:
    #    return command_result
    if callbackSTD is not None:
        command_communicate = _popenSTD_to_func(command_result, callbackSTD)
        logSTDOUT=False
    else :    
        command_communicate = command_result.communicate()  # waiting to get returncode
        logSTDOUT=True
        
    logCommand(command, command_communicate, command_result.returncode, logSTDOUT)
    
        
    return (command_result.returncode, command_communicate[0], command_communicate[1])
    
    
    
def logCommand(command, communicate, returnCode, logSTDOUT=True):
    logging.info("------") 
    logging.info(command)  
    
    if returnCode != 0 :
        logging.error("COMMAND FAILED !") 
        
    logging.info("Return code : "+str(returnCode)) 
      
    if logSTDOUT and communicate[0] != "":
        logging.info("stdout : \n----\n"+communicate[0]+"\n----\n") 
          
    if communicate[1] != "":  
        logging.info("stderr : \n----\n"+communicate[1]+"\n----\n")
        
def is_a_date(date):
    regexDatePattern = "(((\d{4}-\d\d-\d\d)T\d\d:\d\d:\d\d)\.?(\d{6})?([+\-]\d\d:\d\d)\s*)"
    # like      2012-11-02T00:02:34.290468+01:00
    # pattern (((2012-11-02)T00:02:34).(290468)(+01:00))

    try:
        matchDateStart = re.search(regexDatePattern, date)
        return matchDateStart is not None and matchDateStart.group(0) == date
    except Exception:
        return False
    
 
class ConfigError(Exception):
    """
    ConfigError 
    :param msg: 
    """
    def __init__(self, msg):
        Exception.__init__(self, msg)
           
def create_plugin(module_name, class_name, config, params=[]):
    """
    Create plugin of given class located in given module
    
    :param module_name: name of the module
    :param class_name: name of the class
    :param config: config dictionnary given to the plugin

    :returns: a instance of the plugin
    """
    if not module_name in sys.modules.keys():
        try:
            module = __import__(module_name)
        except ImportError, exc:
            raise ConfigError('Unable to load module: ' +\
                               module_name + ': ' + str(exc))
    else:
        module = sys.modules[module_name]

    classes = dict(inspect.getmembers(module, inspect.isclass))
    if not class_name in classes:
        raise ConfigError('Unable to find ' + \
                           class_name + ' class in ' + \
                           module_name )

    cls = classes[class_name]
    return cls(config, params)

