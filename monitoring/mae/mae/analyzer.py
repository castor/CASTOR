#!/usr/bin/python
# -*- coding: utf-8 -*-

#******************************************************************************
#                     mae-consumer
#
# Copyright (C) 2013  CERN
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# @author: Castor dev team
#******************************************************************************

# Imports
# import multiprocessing
import sys
import threading
import logging
import traceback
import time

import mae.utils
from metric import Metrics

# class Analyzer(multiprocessing.Process):
class Analyzer(threading.Thread):
    """
    Thread responsible for applying the metrics to the incoming messages

    :param metrics_path: path to the metrics directory
    :param source_path: the path to the local message queue
    :param source_type: type of source, default is "queue" used by stompclients. It also possible to define "file".
    :param STOP_FLAG: flag to notify the stop of the component
    """

    def __init__(self, metrics_path, source_path, STOP_FLAG, config, processNumber="0", pluginType=('dirq_reader', 'DirQReader')):
        
        threading.Thread.__init__(self, name='Analyzer ' + str(processNumber))
        self.config = config
        self.STOP_FLAG = STOP_FLAG
        self.source_path = source_path
        self.reader = ''
        self.loadReader(pluginType)
        self._metrics_path = metrics_path
            
        self.metrics = []
        self.processNumber = processNumber
        
    def loadReader(self, pluginType):      
        input_plugin_path = self.config.get('inputPlugin', 'plugin_path') 
        if not input_plugin_path in sys.path:
            sys.path.append(input_plugin_path)
            
        try:
            self.reader = mae.utils.create_plugin(pluginType[0], pluginType[1], self.config, [self.source_path, self.STOP_FLAG]) 
        except mae.utils.ConfigError, exc:
            logging.critical(str(exc))
        logging.info('Following plugin are loaded for consumer '+self.name+' : ' + str(self.reader))
            
        pass
        
    def _loadMetrics(self):
        """ Load Metrics """
        self.metrics = Metrics.loadMetrics(self._metrics_path + '/*.metric')
        logging.info("starting with metrics : " + str([m.name for m in self.metrics]))
        logging.info("reading messages at : " + self.source_path)
        
    def _processMessages(self):
        for log in self.reader.streamParsedData():
            if self.STOP_FLAG.is_set():
                break
            self._applyMetrics(log)
    
    def _applyMetrics(self, msg):   
        """ 
        Apply the metrics to the message 
        
        :param msg: the message
        """
        for metric in self.metrics:
            try:                
                logging.debug("analyzer apply ") 
                metric.apply(msg)              
                    
            # catch *all* exceptions
            except Exception, e:
                logging.critical("error: "+str(e))
                logging.error("Error in processing metric " + \
                              metric.name + " on message :\n" + str(msg) + \
                              "\n" + traceback.format_exc())

    def run(self):
        """ Run process to analyze files """
        self._loadMetrics()

        # Then, infinite polling on the messages source to process new message
        while not self.STOP_FLAG.is_set():
            try:
                self._processMessages()
            except Exception, e:
                logging.error("Unexpected error caught in analzer : %s" % str(e) + "\n" + traceback.format_exc())
            time.sleep(0.1)  # wait new batch files before a new streamData
            
        logging.info("EXIT analyzer")
        
