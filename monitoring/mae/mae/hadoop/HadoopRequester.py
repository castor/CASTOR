#!/usr/bin/python
# -*- coding: utf-8 -*-

#******************************************************************************
#                     mae-consumer
#
# Copyright (C) 2013  CERN
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# @author: Castor dev team
#******************************************************************************

import logging
import datetime
from mae.utils import execShellCommand

class HadoopRequester(object):
    def __init__(self, config, metrics, instanceName, typeName, nbReduces, dateStart, dateEnd, 
                 name="hadoopRequest", status_callback=None, output_destination="hadoop"):
        
        self.dateRequest = (str(datetime.datetime.now().isoformat())).replace(':', '-')
        
        self.name = name
            
        self.config = config
        
        self.instanceName = instanceName
        self.typeName = typeName
        
        self.dateStart = dateStart
        self.dateEnd = dateEnd
        
        self.status_callback = status_callback
        
        self.nbReduces = nbReduces
        self.fs = self.config.get("Hadoop", 'file_system')
        self.input_base = self.config.get('Hadoop', 'base_request_input_dir') + "/"
        self.runuser = self.config.get('Hadoop', 'runuser')
        
        self.output_destination = output_destination
        
    
    def _log(self, message):        
        if self.status_callback is not None:
            self.status_callback(message)
        logging.info(message)
        
    def run(self):
        # make command
        self._log("Build Hadoop job")
        job = self._build_job()
        
        self._log("Executing Hadoop job")
        command = job[0]
        self.hadoop_output_result = job[1]
        self._log("hadoop cmd : \n\n"+ command+"\n")
        command_result = execShellCommand(command, self._log)
        
        self._log("Output will be saved in "+ self.hadoop_output_result)
        
        dl_mapreduce_result = self._download_mapreduce_result()
        if dl_mapreduce_result[0] == 0 :
            self._cleanHadoop()
        
        self._log("Hadoop request done.")
        return
        
        
    def _cleanHadoop(self):
        cmd_rm =  "hadoop fs -rm -R " + self.fs+self.hadoop_output_result
        cmd_for_user = 'runuser '+self.runuser+' -c "'+cmd_rm+'"'
        command_result = execShellCommand(cmd_for_user, self._log)
        return command_result
       
        
        
    def _input_path_filter(self, dateStart, dateEnd, instanceName, typeName): 
        grep_instance_type = None
        grep_instance = None
        grep_type = None
        
        if len(instanceName) != 0 and instanceName[0] != "*":
            instances = '|'.join(instanceName)
            grep_instance = "(" + instances + ")"
        
            
        if len(typeName) != 0 and instanceName[0] != "*":
            types = '|'.join(typeName)
            grep_type = "(" + types + ")"
            
        if grep_instance is not None:
            grep_instance_type = "egrep '" + grep_instance + "'"
           
        if grep_type is not None:
            grep_instance_type = "egrep '" + grep_type + "'"
            
        if grep_instance is not None and grep_type is not None :
            grep_instance_type = "egrep '" + grep_instance + "\/" + grep_type + "'"
          
        # This awk filter all line who end with date between dateStart and dateEnd. 
        # In the same time it remove .errors files  
        pathFilter = "awk '{ DATE = substr($8, (length($8)-7), 8)} {if (\"" + dateStart + "\" <= DATE && DATE <= \"" + dateEnd + "\") print $8 }'"

        
        if grep_instance_type is not None:
            pathFilter += " | " + grep_instance_type
        
        return pathFilter
    
    def _hadoop_ls(self, pasth_to_list, recursive=False):    
        if recursive :     
            return "hadoop fs -fs " + self.fs + " -ls " + pasth_to_list
        else :
            return "hadoop fs -fs " + self.fs + " -ls -R " + pasth_to_list
        
    def _hadoop_get(self, path_source, path_dest):
        return "hadoop fs -get " + self.fs+path_source + " " + path_dest
        
    def _find_files(self):
        
        smallDateStart = str(int(self.dateStart[0:10].replace("-", "")) - 1)
        smallDateEnd = str(int(self.dateEnd[0:10].replace("-", "")) + 1)
        
        shell_path_filter = self._input_path_filter(smallDateStart, smallDateEnd, self.instanceName, self.typeName) 
        
        instanceName = self.instanceName[0] if len(self.instanceName) == 1 else  "\*"
        typeName = self.typeName[0] if len(self.typeName) == 1 else  "\*"
        pasth_to_list = self.input_base + instanceName + "/" + typeName
        ls_command = self._hadoop_ls(pasth_to_list) + " | " + shell_path_filter
        
        logging.info(ls_command)
        
        self._log("looking for hadoop files")
        self._log(ls_command)
        ls_result = execShellCommand(ls_command)  # tuple(returnCode, stdout, stderr)
               
        #split string result to list and remove last blank line
        files = (ls_result[1].split("\n"))[0:-1]
        self._log("inputs file : \n"+str(files))
        
          
        return files
    
    def _build_job(self):
        
        streaming_jar = self.config.get('Hadoop', 'streaming_jar')
        d_options = ['option1', 'option2']
        
        hadoop_fs = self.fs
        hadoop_jt = self.config.get('Hadoop', 'job_tracker') 
        
        
        files = self.config.get('Hadoop', 'uploaded_Files')
        files = [x.strip() for x in files.split(',')]
        
        
        mapper = self.config.get('Hadoop', 'default_mapper')
        mapper = "'" + mapper + " " + self.dateStart + " " + self.dateEnd + "'"
            
        reducer = self.config.get('Hadoop', 'default_reducer')
        reducer = "'" + reducer + "'"
        
        partitionner = None
        if self.nbReduces > 1 :
            partitionner = self.config.get('Hadoop', 'default_reducer')
        
        # find files
        output_path = self.config.get('Hadoop', 'base_request_output_dir') + "/" + self.dateRequest + "_" +self.name
        
        inputs = self._find_files()
        
        builded_command = self._build_hadoop_command(streaming_jar, d_options, hadoop_fs, hadoop_jt, mapper, reducer, files, output_path, inputs, partitionner)
        return (builded_command, output_path)

    def _build_hadoop_command(self, streaming_jar, d_options, hadoop_fs, hadoop_jt, mapper, reducer, files, output, inputs, partitionner=None):
        """ 
        Build the streaming hadoop command
        """
        
        command = ""
        command += "hadoop "
        
        command += 'jar ' + streaming_jar + " "
        
        # for d_option in d_options:
        #    command += "-D " + d_option + " "
            
        command += "-fs " + hadoop_fs + " "
        command += "-jt " + hadoop_jt + " "
        command += "-mapper " + mapper + " "
        command += "-reducer " + reducer + " "
        
        
        if partitionner is not None :
            command += "-partitionner " + partitionner + " "
          
        for _file in files:
            command += "-file " + str(_file) + " "
           
        command += "-output " + output + " "
         
        for input_file in inputs:
            command += "-input " + input_file + " "
            
            
        command = 'runuser '+self.runuser+' -c "'+command+'"'    
        return command
    
    def _download_mapreduce_result(self):
        #self.hadoop_output_result = "/project/castorlogs/maeRequest/2013-07-25T15-35-30.413139_demo"
        #self._hadoop_ls(self.hadoop_output_result, recursive=True)
        
        source = self.hadoop_output_result
        destination = self.output_destination+"/"+self.dateRequest+"_"+self.name
        
        hadoop_get = self._hadoop_get(source, destination)
        
        self._log(hadoop_get)
        command_result = execShellCommand(hadoop_get)
        
        cmd_make_local_data_dir = "mkdir "+destination+"/data"
        cmd_local_move = "mv " +destination+"/part* "+destination+"/data"
        
        self._log(cmd_make_local_data_dir)
        execShellCommand(cmd_make_local_data_dir, )
        
        self._log(cmd_local_move)
        execShellCommand(cmd_local_move)
        
        self._log("file stored in "+destination)
        
        return command_result
