#!/usr/bin/python
# -*- coding: utf-8 -*-

#******************************************************************************
#                     mae-consumer
#
# Copyright (C) 2013  CERN
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# @author: Castor dev team
#******************************************************************************

import logging
import simplejson as json

from messaging.message import Message
from messaging.queue.dqs import DQS
from messaging.error import MessageError

""" 
DequeueReader input plugin 
Allow to read messages from a Directory Queue
"""
    
class DirQReader():     
    def __init__(self, config, params):
        source = params[0]
        STOP_FLAG = params[1]
        self._source = DQS(path=source)
        self.STOP_FLAG = STOP_FLAG
        logging.info("DirQReader initialized")
           
    def _clean(self):
        try:
            self._source.purge(maxtemp=60, maxlock=60)
        except OSError, exc:
            logging.error(str(exc))
            
    def _remove(self, name):
        try:
            self._source.remove(name)
        except OSError, exc:
            logging.error(str(exc))
            
    def _parse(self, msg):
        """
        Parse the incoming message
        
        :param msg: the message to parse
        :return: None if the message can not be parsed or there is no data in the message
        """
        try : 
            body = json.loads(msg.body)
        except:
            logging.error("Analyzer: Unable to parse from file the json string :\n"+msg)
            return None
    
        if 'data' in body.keys() and isinstance(body['data'], list):
            return body['data']
        return None
    
    def streamParsedData(self):
        for name in self._source:
            if self.STOP_FLAG.is_set():
                return
            
            if self._source.lock(name):
                try:
                    msg = self._source.get_message(name)
                except MessageError, exc:
                    self._remove(name)
                    logging.warning("DirQReader : " + str(exc))
                    continue
                for m in self._parse(msg):
                    yield m
                self._remove(name)
                
                    
        # purge old messages
        self._clean()