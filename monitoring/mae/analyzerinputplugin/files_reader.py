#!/usr/bin/python
# -*- coding: utf-8 -*-

#******************************************************************************
#                     mae-consumer
#
# Copyright (C) 2013  CERN
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# @author: Castor dev team
#******************************************************************************

import os
import simplejson as json

""" 
FileReader input plugin 
Allow to read messages from files
"""
    
class FilesReader():  
    def __init__(self, config, params):
        source = params[0]
        STOP_FLAG = params[1]
        self._source = source
        self.STOP_FLAG = STOP_FLAG
             
    def _clean(self, filename):
        os.remove(filename)
        
    def _parse(self, msg):
        """
        Parse the incoming message
        
        :param msg: the message to parse
        :return: None if the message can not be parsed or there is no data in the message. Else the msg parsed
        """
        try : 
            msg = json.loads(msg.strip())
        except:
            self.logging.error("Analyzer: Unable to parse from file the json string :\n"+msg)
            return None
        return msg
                
    def _listFileByMtime(self):                            
        files = os.listdir(self._source)
        files.sort(key=lambda x: os.path.getmtime(x))
        for _file in files:
            yield _file
            
            
    def _listFileByName(self):                            
        files = os.listdir(self._source)
        files.sort(key=lambda x: x)
        for _file in files:
            yield _file
              
    def streamParsedData(self):   
        """
        Stream data from the source.
        
        """     
        for filename in self._listFileByMtime():
            if self.STOP_FLAG.isSet():
                return
            
            with open(filename) as f:
                for data in f:
                    msg = self._parse(data)
                    if msg is not None:
                        yield msg
                                 
            self._clean(filename)  