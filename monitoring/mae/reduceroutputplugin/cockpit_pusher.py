#!/usr/bin/python
# -*- coding: utf-8 -*-

#******************************************************************************
#                     mae-consumer
#
# Copyright (C) 2013  CERN
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# @author: Castor dev team
#******************************************************************************

import traceback
import logging
import json
import urllib
import urllib2
import time


class CockpitPusher():
    def __init__ (self, config, params=[]):
        self.name = "Cockpit pusher"
        self._cockpit_push_url = config.get("OutputPluginCockpitPusher", "push_url")
        logging.info('CockpitPlugin output :' +self._cockpit_push_url)

        self.urlreq = urllib2.Request(self._cockpit_push_url)

    def apply(self, data_to_push):
        try :
            values = {'data' : json.dumps(data_to_push)}
        except Exception:
            logging.error("json exception : "+data_to_push)
            
        data = urllib.urlencode(values)
        logging.debug(self.name + " Data to send : " +str(values['data']))
        
        
        self._send(urllib2.Request(self._cockpit_push_url) , data)
        logging.debug(self.name + " Data successfully send to cockpit")
        
        
        
    def _send(self, req, data):
        notSent = True
        nbTries = 0
        while notSent and nbTries < 3:
            try:                        
                #logging.debug(self.name + " :: Request to send : "+str(req))
                #logging.debug(self.name + " :: Data to send: "+str(data))
                response = urllib2.urlopen(req, data)
                # all was fine, return
                return
            except urllib2.HTTPError, error:
                contents = error.read()
                logging.error(str(contents))
            except Exception, e:
                # if there is any problem while sending the data,
                # wait 5sec and send again
                logging.error(self.name
                              + " :: Could not send data to cockpit: Exception : "
                              + str(e)
                              + "\n" + str(traceback.format_exc())
                              + "\n Try again in 3 secondes")
            # some error occured, sleep 3 seconds and retry
            time.sleep(3)
