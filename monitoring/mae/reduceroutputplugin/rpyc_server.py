#!/usr/bin/python
# -*- coding: utf-8 -*-

#******************************************************************************
#                     rpyc_server
#
# Copyright (C) 2013  CERN
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# @author: Castor dev team
#******************************************************************************

"""
Plugin running an rpyc server and serving latest state of metrics data
In order to use it, you need to add few lines to the mae config file :
  - in the "output_plugins" entry of the [Reducer] section, add the line :
      rpyc_server RpycServer
  - add a [OutputPluginRpycServer] section with the following line :
      rpyc_port = 18862
    of course you can put any port number you'd like to use
"""

import threading
import logging
import commands
import time
import os
import rpyc
from rpyc.utils.server import ThreadedServer
from mae.metric import Metrics

class RpycServer():
    """This plugin keeps an up to date status of all metrics in memory and
    serves it via rpyc"""

    # lock to synchronize access to the cached data in the different
    # instances of the plugin
    _lock = threading.Lock()

    # cached metrics. Note that it's a class variable shared among
    # all instances of the plugin and protected by the previous lock
    # _metric_cache is a dictionnary of metrics and values are dicts
    # with keys 'timestamp' and 'metricData'
    _metric_cache = {}

    # thread running the rpyc service
    _rpyc_thread = None

    def __init__ (self, config, params=[]):
        self.name = "Rpyc output plugin"
        self.config = config
        self.STOP_FLAG = False
        logging.info("Starting %s" % self.name)
        # initialize cache and rpyc service if needed
        RpycServer._lock.acquire()
        try:
            if None == RpycServer._rpyc_thread:
                logging.info("%s : creating the Rpyc Server" % self.name)
                RpycServer._rypc_thread = RPyCServerDeamon(config, self.STOP_FLAG)
                RpycServer._rypc_thread.start()
        finally:
            RpycServer._lock.release()

    def __del__(self):
        # first see whether we should join the rpyc thread
        # but do not do it with the lock taken, as the thread
        # may use it and we could get a dead lock
        self.STOP_FLAG = True
        do_join = False
        thread_to_join = RpycServer._rypc_thread
        RpycServer._lock.acquire()
        try:
            if RpycServer._rypc_thread != None:
                doJoin = True
                RpycServer._rypc_thread = None
        finally:
            RpycServer._lock.release()
        # now that we released the lock, we can safely join
        if do_join:
            thread_to_join.join()

    def apply(self, data):
        # data is a dictionnary of metrics and values are dicts
        # with keys 'timestamp' and 'metricData'
        RpycServer._lock.acquire()
        try:
            for metric in data:
                # keep data if more recent than our actual state
                if metric not in RpycServer._metric_cache or data[metric]['timestamp'] > RpycServer._metric_cache[metric]['timestamp']:
                    RpycServer._metric_cache[metric] = data[metric]
        finally:
            RpycServer._lock.release()


class RPyCServerDeamon(threading.Thread):
    """
    Simple thread that starts the RPyC server independently, because t.start() is blocking
    """

    def __init__(self, config, STOP_FLAG):
        threading.Thread.__init__(self, name="RPyCOutputServer")
        logging.info("Starting output plugin rpyc server")
        self.STOP_FLAG = STOP_FLAG
        RPyCServerDeamon.port = int(config.get("OutputPluginRpycServer", "rpyc_port"))
        RPyCServerDeamon.metrics_path = config.get("mae", "metrics_path")
        RPyCServerDeamon.metrics_source_path_editTime = 0
        RPyCServerDeamon.metrics = Metrics.loadMetrics(RPyCServerDeamon.metrics_path+'/*.metric')
        logging.info("Metrics loaded for output plugin rpyc server :\n "+str(RPyCServerDeamon.metrics))
        self.setDaemon(True)

    def __del__(self):
        logging.info("Rpyc Output plugin : waiting 16 sec to wait all threads before exit (metric updater timer)")
        time.sleep(16)

    def metricUpdater(self):
        if self.STOP_FLAG:
            return

        timer = threading.Timer(15, self.metricUpdater)
        timer.start()

        # Access on folder metric ? If yes, probably for changes.
        stats = os.stat(RPyCServerDeamon.metrics_path)
        # if same time, do nothing
        if RPyCServerDeamon.metrics_source_path_editTime == stats.st_mtime :
            return

        RPyCServerDeamon.metrics_source_path_editTime  = stats.st_mtime
        RPyCServerDeamon.metrics = Metrics.loadMetrics(RPyCServerDeamon.metrics_path+'/*.metric')
        logging.debug("Metrics reloaded for rpyc server = "+str(RPyCServerDeamon.metrics))

    def run(self):
        logging.info("Output Plugin RPyC Server : started")
        self.metricUpdater()
        logging.info("Output Plugin Metric list cron updater : started")

        # Spawn a thread for each connection
        logging.info("Spawning rpyc service on port %d" % RPyCServerDeamon.port)
        self.service = ThreadedServer(RPyCService,
                       port=RPyCServerDeamon.port,
                       protocol_config={"allow_public_attrs":True},
                       auto_register=False)

        self.service.start()


class RPyCService(rpyc.Service):
    """RPyC Service providing data to the outside world"""

    def __init__(self, conn):
        """Init"""
        threading.current_thread().name = "RPycService"+threading.current_thread().name
        rpyc.Service.__init__(self, conn)

        logging.debug("rpycservice connection requested")

    def _to_tuples(self, d):
        """transforms dictionnaries into tuples recursively"""
        if type(d) is dict:
            return tuple([(k, self._to_tuples(d[k])) for k in d])
        elif type(d) is list:
            return tuple([self._to_tuples(item) for item in d])
        else:
            return d

    def exposed_get_metric(self, name=None):
        """If name is provided, return the metric with name == name
        Else, return a tuple of all the running metrics"""
        if name:
            for metric in RPyCServerDeamon.metrics:
                if metric.name == name :
                    logging.info("request for "+name)
                    return metric
            logging.warning("request for "+name+" but not found")
            return None
        logging.info("request for all metric")
        return tuple(RPyCServerDeamon.metrics)

    def exposed_get_metric_file(self, name):
        """
        cat the metric file and send the data
        :param name: name of the metric you want
        """
        for metric in RPyCServerDeamon.metrics:
            if metric.name != name :
                continue
            logging.info("request for file "+name)
            return commands.getoutput("cat "+RPyCServerDeamon.metrics_path+"/"+name+".metric")
        logging.warning("request for file "+name+" but not found")

    def exposed_get_data(self, metric=None):
        """Send data for the specified metric or all if no name provided"""
        RpycServer._lock.acquire()
        try:
            if not metric:
                return tuple([(metric, self._to_tuples(RpycServer._metric_cache[metric]['metricData']))
                              for metric in RpycServer._metric_cache])
            elif metric in RpycServer._metric_cache:
                return self._to_tuples(RpycServer._metric_cache[metric]['metricData'])
            else:
                return ()
        finally:
            RpycServer._lock.release()

    def exposed_get_json_data(self, metric=None):
        """Send data for the specified metric or all if no name provided, in json format"""
        return json.dumps(self.exposed_get_data(metric))
