/*
 * Copyright (C) 2001 by CERN/IT/PDP/DM
 * All rights reserved
 */

#pragma once

#include "osdep.h"

EXTERN_C void rmc_logreq(const char *const func, char *const logbuf);

