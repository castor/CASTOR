/*
 * $Id: Cthread_flags.h,v 1.8 2002/09/30 16:28:27 jdurand Exp $
 */

/*
 * Copyright (C) 1999-2002 by CERN/IT/DS/HSM
 * All rights reserved
 */

#pragma once

#include <pthread.h>
#include <stdlib.h>           /* malloc, etc... */
#include <unistd.h>           /* fork usleep... */
#include <sys/time.h>         /* gettimeofday   */
#include <errno.h>            /* errno ...      */
#include <string.h>           /* strerror ...   */












