/*
 * Copyright (C) 2005 by CERN/IT/ADC/CA 
 * All rights reserved
 */

/*
 * $Id: Cstrerror.h,v 1.1 2005/08/10 09:07:58 jdurand Exp $
 */

#pragma once

#include "osdep.h"

EXTERN_C char *Cstrerror (int);

