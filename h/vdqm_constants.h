/*
 * $Id: vdqm_constants.h,v 1.8 2008/10/11 11:33:34 murrayc3 Exp $
 */

/*
 * Copyright (C) 1999-2001 by CERN/IT/PDP/DM
 * All rights reserved
 */

/*
 */

/*
 * vdqm_constants.h - definitions of all VDQM constants
 */

#pragma once
/*
 * VDQM magic numbers
 */
#define VDQM_MAGIC  (0x8537)
#define VDQM_MAGIC2 (0x8538)
#define VDQM_MAGIC3 (0x8539)
#define VDQM_MAGIC4 (0x8540)

/*
 * Request types for magic number: VDQM_MAGIC
 */
#define VDQM_BASE_REQTYPE (0x3100)
#define VDQM_REQ_MIN      (VDQM_BASE_REQTYPE)
#define VDQM_VOL_REQ      (VDQM_BASE_REQTYPE+0x01)
#define VDQM_DRV_REQ      (VDQM_BASE_REQTYPE+0x02)
#define VDQM_PING         (VDQM_BASE_REQTYPE+0x03)
#define VDQM_CLIENTINFO   (VDQM_BASE_REQTYPE+0x04)
#define VDQM_SHUTDOWN_DEPRECATED (VDQM_BASE_REQTYPE+0x05)
#define VDQM_HOLD_DEPRECATED (VDQM_BASE_REQTYPE+0x06)
#define VDQM_RELEASE_DEPRECATED (VDQM_BASE_REQTYPE+0x07)
#define VDQM_REPLICA      (VDQM_BASE_REQTYPE+0x08)
#define VDQM_RESCHEDULE   (VDQM_BASE_REQTYPE+0x09)
#define VDQM_ROLLBACK     (VDQM_BASE_REQTYPE+0x0A)
#define VDQM_COMMIT       (VDQM_BASE_REQTYPE+0x0B)
#define VDQM_DEL_VOLREQ   (VDQM_BASE_REQTYPE+0x0C)
#define VDQM_DEL_DRVREQ   (VDQM_BASE_REQTYPE+0x0D)
#define VDQM_GET_VOLQUEUE (VDQM_BASE_REQTYPE+0x0E)
#define VDQM_GET_DRVQUEUE (VDQM_BASE_REQTYPE+0x0F)
#define VDQM_SET_PRIORITY (VDQM_BASE_REQTYPE+0x10)
#define VDQM_DEDICATE_DRV (VDQM_BASE_REQTYPE+0x11)
#define VDQM_HANGUP       (VDQM_BASE_REQTYPE+0x12)
#define VDQM_REQ_MAX      (VDQM_BASE_REQTYPE+0x13)
#define VDQM_VALID_REQTYPE(X) ((X)>VDQM_REQ_MIN && (X)<VDQM_REQ_MAX)

/*
 * Request types for magic number: VDQM_MAGIC2
 *
 * Note that the VDQM2_ prefix refers to the protocol version of the VDQM
 * messages and not to the VDQM2 server.
 */
#define VDQM2_BASE_REQTYPE     (0x4100)
#define VDQM2_REQ_MIN          (VDQM2_BASE_REQTYPE)
#define VDQM2_SET_VOL_PRIORITY (VDQM2_BASE_REQTYPE+0x01)
#define VDQM2_REQ_MAX          (VDQM2_BASE_REQTYPE+0x02)
#define VDQM2_VALID_REQTYPE(X) ((X)>VDQM2_REQ_MIN && (X)<VDQM2_REQ_MAX)

/*
 * Request types for magic number: VDQM_MAGIC3
 */
#define VDQM3_BASE_REQTYPE (0x4110)
#define VDQM3_REQ_MIN      (VDQM3_BASE_REQTYPE)
#define VDQM3_DEL_DRV      (VDQM3_BASE_REQTYPE+0x01)
#define VDQM3_DEDICATE     (VDQM3_BASE_REQTYPE+0x02)
#define VDQM3_REQ_MAX      (VDQM3_BASE_REQTYPE+0x03)
#define VDQM3_VALID_REQTYPE(X) ((X)>VDQM3_REQ_MIN && (X)<VDQM3_REQ_MAX)

/**
 * Request types for magic number: VDQM_MAGIC4
 */
#define VDQM4_BASE_REQTYPE       (0x4120)
#define VDQM4_REQ_MIN            (VDQM4_BASE_REQTYPE)
#define VDQM4_TAPEBRIDGE_VOL_REQ (VDQM4_BASE_REQTYPE+0x01)
#define VDQM4_REQ_MAX            (VDQM3_BASE_REQTYPE+0x02)
#define VDQM4_VALID_REQTYPE(X) ((X)>VDQM4_REQ_MIN && (X)<VDQM4_REQ_MAX)

/*
 * Define string constants for logging purpose. Don't forget to update
 * if a new request value is added ! The constants should be in same order
 * as the request values.
 */
#define VDQM_REQ_VALUES {VDQM_VOL_REQ, VDQM_DRV_REQ, VDQM_PING, \
    VDQM_CLIENTINFO, \
    VDQM_REPLICA, VDQM_RESCHEDULE, VDQM_ROLLBACK, VDQM_COMMIT, \
    VDQM_DEL_VOLREQ, VDQM_DEL_DRVREQ, VDQM_GET_VOLQUEUE, VDQM_GET_DRVQUEUE,\
    VDQM_SET_PRIORITY, VDQM_DEDICATE_DRV, -1}
#define VDQM_REQ_STRINGS {"VOL_REQ", "DRV_REQ", "PING", \
    "CLIENTINFO", \
    "REPLICA", "RESCHEDULE", "ROLLBACK", "COMMIT", \
    "DEL_VOLREQ", "DEL_DRVREQ", "GET_VOLQUEUE", "GET_DRVQUEUE", \
    "SET_PRIORITY", "DEDICATE_DRV",""}

/*
 * Unit status types
 */
#define VDQM_STATUS_MIN    (0x0000)
#define VDQM_UNIT_UP       (0x0001)
#define VDQM_UNIT_DOWN     (0x0002)
#define VDQM_UNIT_WAITDOWN (0x0004)
#define VDQM_UNIT_ASSIGN   (0x0008)
#define VDQM_UNIT_RELEASE  (0x0010)
#define VDQM_UNIT_BUSY     (0x0020)
#define VDQM_UNIT_FREE     (0x0040)
#define VDQM_VOL_MOUNT     (0x0080)
#define VDQM_VOL_UNMOUNT   (0x0100)
#define VDQM_UNIT_UNKNOWN  (0x0200)
#define VDQM_UNIT_ERROR    (0x0400)
#define VDQM_UNIT_MBCOUNT  (0x0800)
#define VDQM_UNIT_QUERY    (0x1000)
#define VDQM_FORCE_UNMOUNT (0x2000)
#define VDQM_TPD_STARTED   (0x4000)
#define VDQM_STATUS_MAX    (0x8000)
#define VDQM_VALID_STATUS(X) ((X)>VDQM_STATUS_MIN && (X)<VDQM_STATUS_MAX)
/*
 * Define string constants for logging purpose. Don't forget to update
 * if a new status value is added ! The constants should be in same order
 * as the status values.
 */
#define VDQM_STATUS_VALUES { VDQM_UNIT_UP , VDQM_UNIT_DOWN ,\
    VDQM_UNIT_WAITDOWN , VDQM_UNIT_ASSIGN , VDQM_UNIT_RELEASE , \
    VDQM_UNIT_BUSY , VDQM_UNIT_FREE , VDQM_VOL_MOUNT , VDQM_VOL_UNMOUNT , \
    VDQM_UNIT_UNKNOWN , VDQM_UNIT_ERROR , VDQM_UNIT_MBCOUNT, \
    VDQM_UNIT_QUERY, VDQM_FORCE_UNMOUNT, VDQM_TPD_STARTED, -1}
 
#define VDQM_STATUS_STRINGS {"VDQM_UNIT_UP","VDQM_UNIT_DOWN",\
   "VDQM_UNIT_WAITDOWN","VDQM_UNIT_ASSIGN","VDQM_UNIT_RELEASE", \
   "VDQM_UNIT_BUSY","VDQM_UNIT_FREE","VDQM_VOL_MOUNT","VDQM_VOL_UNMOUNT", \
   "VDQM_UNIT_UNKNOWN","VDQM_UNIT_ERROR","VDQM_UNIT_MBCOUNT", \
   "VDQM_UNIT_QUERY", "VDQM_FORCE_UNMOUNT", "VDQM_TPD_STARTED", ""}

/*
 * Volume request priority levels
 */
#define VDQM_PRIORITY_MIN     (0x0)
#define VDQM_PRIORITY_NORMAL  (0x1)
#define VDQM_PRIORITY_MAX     (0x2)
#define VDQM_PRIORITY_DEFAULT (VDQM_PRIORITY_NORMAL)
#define VALID_PRIORITY(X) ((X)>=VDQM_PRIORITY_MIN && (X)<=VDQM_PRIORITY_MAX)


#define VDQM_PRIORITY_VALUES { VDQM_PRIORITY_MIN , VDQM_PRIORITY_DEFAULT , \
    VDQM_PRIORITY_MAX , -1}

#define VDQM_PRIORITY_STRINGS {"VDQM_PRIORITY_MIN","VDQM_PRIORITY_DEFAULT", \
   "VDQM_PRIORITY_MAX",""}

/*
 * Dedication keywords. Define them all here so that nothing is forgotten
 * if format changes. Any new search criteria must be added to each one 
 * of the constants below.
 */
#define VDQM_DEDICATE_PREFIX   {"uid=","gid=","name=","host=","vid=", \
                                "mode=","datestr=","timestr=","age=",""}
#define VDQM_DEDICATE_DEFAULTS {"uid=.*","gid=.*","name=.*","host=.*", \
                                "vid=.*","mode=.*","datestr=.*", \
                                "timestr=.*","age=.*",""}
#define VDQM_DEDICATE_FORMAT   {"uid=%d","gid=%d","name=%s","host=%s", \
                                "vid=%s","mode=%d","datestr=%s", \
                                "timestr=%s","age=%d",""}
#define VDQM_DEDICATE_DATEFORM  "%d/%m/%C%y"
#define VDQM_DEDICATE_TIMEFORM  "%H:%M:%S"
#define FILL_MATCHSTR(STR,FORM,FORMS)   {int __i = 0; *(STR) = *(FORM) = '\0'; \
        while ( *(FORMS)[__i]!='\0') {strcat(FORM,FORMS[__i]);  \
            strcat(FORM,","); __i++;}; \
        if ( strlen(FORM) > 0) (FORM)[strlen(FORM)-1] = '\0'; \
        sprintf(STR,FORM, \
                uid,gid,name,host,vid,mode,datestr,timestr,age);}
/*
 * Defaults
 */

#ifndef VDQM_PORT
#ifdef VDQMCSEC
#define SVDQM_PORT       (5512)
#endif
#define VDQM_PORT        (5012)
#endif /* VDQM_PORT */

#define VDQM_TIMEOUT     (300)
#define VDQM_THREADPOOL  (10)
#define VDQM_MAXTIMEDIFF (600)        /* Deferred unmount: max time diff to oldest request */
#define VDQM_RETRYDRVTIM (600)        /* Time between retries on drive */
#define VDQM_HDRBUFSIZ (3*LONGSIZE)
#define VDQM_MSGBUFSIZ (1024)

#ifndef VDQM_REPLICA_PIPELEN
#define VDQM_REPLICA_PIPELEN (10)
#endif /* VDQM_REPLICA_PIPELEN */

#ifndef VDQM_REPLICA_RETRIES
#define VDQM_REPLICA_RETRIES (3)
#endif /* VDQM_REPLICA_RETRIES */

#if defined(SOMAXCONN)
#define VDQM_BACKLOG (SOMAXCONN)
#else  /* VDQM_BACKLOG */
#define VDQM_BACKLOG (5)
#endif /* VDQM_BACKLOG */

#ifndef VALID_VDQM_MSGLEN
#define VALID_VDQM_MSGLEN(X) ((X)>0 && (X)<(VDQM_MSGBUFSIZ))
#endif /* VALID_VDQM_MSGLEN */

