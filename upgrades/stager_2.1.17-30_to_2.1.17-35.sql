/******************************************************************************
 *                 stager_2.1.17-30_to_2.1.17-35.sql
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This script upgrades a CASTOR v2.1.17-30 STAGER database to v2.1.17-35
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

/* Stop on errors */
WHENEVER SQLERROR EXIT FAILURE
BEGIN
  -- If we have encountered an error rollback any previously non committed
  -- operations. This prevents the UPDATE of the UpgradeLog from committing
  -- inconsistent data to the database.
  ROLLBACK;
  UPDATE UpgradeLog
     SET failureCount = failureCount + 1
   WHERE schemaVersion = '2_1_15_18'
     AND release = '2_1_17_35'
     AND state != 'COMPLETE';
  COMMIT;
END;
/

/* Verify that the script is running against the correct schema and version */
DECLARE
  unused VARCHAR(100);
BEGIN
  SELECT release INTO unused FROM CastorVersion
   WHERE schemaName = 'STAGER'
     AND (release = '2_1_17_30');
EXCEPTION WHEN NO_DATA_FOUND THEN
  -- Error, we cannot apply this script
  raise_application_error(-20000, 'PL/SQL release mismatch. Please run previous upgrade scripts for the STAGER before this one.');
END;
/

INSERT INTO UpgradeLog (schemaVersion, release, type)
VALUES ('2_1_15_18', '2_1_17_35', 'TRANSPARENT');
COMMIT;


/* PL/SQL method to either force GC of the given diskCopies or delete them when the physical files behind have been lost */
CREATE OR REPLACE PROCEDURE internalDeleteDiskCopies(inForce IN INTEGER,
                                                     inDryRun IN INTEGER,
                                                     outRes OUT castor.DiskCopyResult_Cur) AS
  varNsHost VARCHAR2(100);
  varFileName VARCHAR2(2048);
  varCfId INTEGER;
  varNbRemaining INTEGER;
  varStatus INTEGER;
  varLogParams VARCHAR2(2048);
  varFileSize INTEGER;
  varNsOpenTime NUMBER;
  varSvcClassId INTEGER;
  varEuid INTEGER;
  varEgid INTEGER;
BEGIN
  -- gather all remote Nameserver statuses. This could not be
  -- incorporated in the INSERT query, because Oracle would give:
  -- PLS-00739: FORALL INSERT/UPDATE/DELETE not supported on remote tables.
  -- Note that files that are not found in the Nameserver remain with fStatus = 'd',
  -- which means they can be safely deleted: we're anticipating the NS synch.
  UPDATE DeleteDiskCopyHelper
     SET fStatus = '-'
   WHERE EXISTS (SELECT 1 FROM Cns_file_metadata@RemoteNS F
                  WHERE status = '-' AND F.fileId IN
                    (SELECT fileId FROM DeleteDiskCopyHelper));
  UPDATE DeleteDiskCopyHelper
     SET fStatus = 'm'
   WHERE EXISTS (SELECT 1 FROM Cns_file_metadata@RemoteNS F
                  WHERE status = 'm' AND F.fileId IN
                    (SELECT fileId FROM DeleteDiskCopyHelper));
  -- A better and more generic implementation would have been:
  -- UPDATE DeleteDiskCopyHelper H
  --    SET fStatus = nvl((SELECT F.status
  --                         FROM Cns_file_metadata@RemoteNS F
  --                        WHERE F.fileId = H.fileId), 'd');
  -- Unfortunately, that one is much less efficient as Oracle does not use
  -- the DB link in bulk, therefore making the query extremely slow (several mins)
  -- when handling large numbers of files (e.g. an entire mount point).
  COMMIT;
  FOR dc IN (SELECT dcId, fileId, fStatus FROM DeleteDiskCopyHelper) LOOP
    DECLARE
      varDCFileSystem INTEGER;
      varDCPool INTEGER;
    BEGIN
      -- get data and lock
      SELECT castorFile, status, diskCopySize, owneruid, ownergid, fileSystem, dataPool
        INTO varCfId, varStatus, varFileSize, varEuid, varEgid, varDCFileSystem, varDCPool
        FROM DiskCopy
       WHERE DiskCopy.id = dc.dcId;
      SELECT nsHost, lastKnownFileName, lastUpdateTime INTO varNsHost, varFileName, varNsOpenTime
        FROM CastorFile
       WHERE id = varCfId
         FOR UPDATE;
      -- get a service class where to put the new copy. Note that we have to choose
      -- potentially among several and we take randomly the first one. This may cause
      -- the creation of a new copy of the file in a different place from the lost
      -- copy, maybe also visible from different service classes in tricky cases.
      -- However, the essential will be preserved : a second copy will be rebuilt
      SELECT child INTO varSvcClassId
        FROM (SELECT DiskPool2SvcClass.child
                FROM FileSystem, DiskPool2SvcClass
               WHERE FileSystem.id = varDCFileSystem
                 AND DiskPool2SvcClass.parent = FileSystem.diskPool
               UNION ALL
               SELECT DataPool2SvcClass.child
                 FROM DataPool2SvcClass
                WHERE DataPool2SvcClass.parent = varDCPool)
       WHERE ROWNUM < 2;
      varLogParams := 'FileName="'|| varFileName ||'"" fileSize='|| varFileSize
        ||' dcId='|| dc.dcId ||' svcClass=' || varSvcClassId || ', status='
        || getObjStatusName('DiskCopy', 'status', varStatus);
    EXCEPTION WHEN NO_DATA_FOUND THEN
      -- diskcopy not found in stager
      UPDATE DeleteDiskCopyHelper
         SET rc = dconst.DELDC_NOOP,
             msg = 'not found in stager, skipping'
       WHERE dcId = dc.dcId;
      COMMIT;
      CONTINUE;
    END;
    -- count remaining ones
    SELECT count(*) INTO varNbRemaining FROM DiskCopy
     WHERE castorFile = varCfId
       AND status = dconst.DISKCOPY_VALID
       AND id != dc.dcId;
    -- and update their importance if needed (other copy exists and dropped one was valid)
    IF varNbRemaining > 0 AND varStatus = dconst.DISKCOPY_VALID AND inDryRun = 0 THEN
      UPDATE DiskCopy SET importance = importance + 1
       WHERE castorFile = varCfId
         AND status = dconst.DISKCOPY_VALID;
    END IF;
    IF inForce != 0 THEN
      -- the physical diskcopy is deemed lost: delete the diskcopy entry
      -- and potentially drop dangling entities
      IF inDryRun = 0 THEN
        DELETE FROM DiskCopy WHERE id = dc.dcId;
        IF varStatus = dconst.DISKCOPY_STAGEOUT THEN
          -- fail outstanding requests
          UPDATE SubRequest
             SET status = dconst.SUBREQUEST_FAILED,
                 errorCode = serrno.SEINTERNAL,
                 errorMessage = 'File got lost while being written to'
           WHERE diskCopy = dc.dcId
             AND status = dconst.SUBREQUEST_READY;
        END IF;
      END IF;
      -- was it the last active one?
      IF varNbRemaining = 0 THEN
        IF inDryRun = 0 THEN
          -- yes, drop the (now bound to fail) migration job(s)
          deleteMigrationJobs(varCfId);
          -- check if the entire castorFile chain can be dropped
          deleteCastorFile(varCfId);
          -- log
          logToDLF(NULL,
                   CASE dc.fStatus WHEN 'm' THEN dlf.LVL_SYSTEM
                                   WHEN 'd' THEN dlf.LVL_SYSTEM
                                   ELSE dlf.LVL_WARNING END,
                   CASE dc.fStatus WHEN 'm' THEN dlf.DELETEDISKCOPY_RECALL
                                   WHEN 'd' THEN dlf.DELETEDISKCOPY_GC
                                   ELSE dlf.DELETEDISKCOPY_LOST END,
                   dc.fileId, varNsHost, 'stagerd', varLogParams);
        END IF;
        UPDATE DeleteDiskCopyHelper
           SET rc = CASE dc.fStatus WHEN 'm' THEN dconst.DELDC_NOOP
                                    WHEN 'd' THEN dconst.DELDC_NOOP
                                    ELSE dconst.DELDC_LOST END,
               msg = CASE dc.fStatus WHEN 'm' THEN 'dropped from disk pool'
                                     WHEN 'd' THEN 'NOT garbage collected from stager'
                                     ELSE 'dropped LAST COPY from stager, file is LOST' END
         WHERE dcId = dc.dcId;
      ELSE
        -- it was not the last valid copy, replicate from another one
        UPDATE DeleteDiskCopyHelper
           SET rc = dconst.DELDC_NOOP, msg = 'dropped from disk pool'
         WHERE dcId = dc.dcId;
        IF inDryRun = 0 THEN
          logToDLF(NULL, dlf.LVL_SYSTEM, dlf.DELETEDISKCOPY_REPLICATION,
                   dc.fileId, varNsHost, 'stagerd', varLogParams);
          handleReplication(dc.fileId, varNsHost, varCfId, varNsOpenTime, varSvcClassId, varEuid, varEgid);
        END IF;
      END IF;
    ELSE
      -- similarly to stageRm, check that the deletion is allowed:
      -- basically only files on tape may be dropped in case no data loss is provoked,
      -- or files already dropped from the namespace. The rest is forbidden.
      IF (varStatus IN (dconst.DISKCOPY_VALID, dconst.DISKCOPY_FAILED) AND varNbRemaining > 0)
         OR dc.fStatus in ('m', 'd') OR varFileSize = 0 THEN
        UPDATE DeleteDiskCopyHelper
           SET rc = dconst.DELDC_NOOP,
               msg = 'garbage collected from stager'
         WHERE dcId = dc.dcId;
        IF inDryRun = 0 THEN
          IF varStatus = dconst.DISKCOPY_VALID THEN
            UPDATE DiskCopy
               SET status = dconst.DISKCOPY_INVALID, gcType = dconst.GCTYPE_ADMIN
             WHERE id = dc.dcId;
          ELSE
            DELETE FROM DiskCopy WHERE ID = dc.dcId;
          END IF;
          -- do not forget to cancel pending migrations in case we've lost that last DiskCopy
          IF varNbRemaining = 0 THEN
            deleteMigrationJobs(varCfId);
          ELSE
            -- try to recreate lost copy if possible
            handleReplication(dc.fileId, varNsHost, varCfId, varNsOpenTime, varSvcClassId, varEuid, varEgid);
          END IF;
          logToDLF(NULL, dlf.LVL_SYSTEM, dlf.DELETEDISKCOPY_GC, dc.fileId, varNsHost, 'stagerd', varLogParams);
        END IF;
      ELSE
        -- nothing is done, just record no-action
        UPDATE DeleteDiskCopyHelper
           SET rc = dconst.DELDC_NOOP,
               msg = 'NOT garbage collected from stager'
         WHERE dcId = dc.dcId;
        IF inDryRun = 0 THEN
          logToDLF(NULL, dlf.LVL_SYSTEM, dlf.DELETEDISKCOPY_NOOP, dc.fileId, varNsHost, 'stagerd', varLogParams);
        END IF;
      END IF;
    END IF;
    COMMIT;   -- release locks file by file
  END LOOP;
  -- return back all results for the python script to post-process them,
  -- including performing all required actions
  OPEN outRes FOR
    SELECT dcId, fileId, msg, rc FROM DeleteDiskCopyHelper;
END;
/


/* Recompile all invalid procedures, triggers and functions */
/************************************************************/
BEGIN
  recompileAll();
END;
/

/* Flag the schema upgrade as COMPLETE */
/***************************************/
UPDATE UpgradeLog SET endDate = systimestamp, state = 'COMPLETE'
 WHERE release = '2_1_17_35';
COMMIT;
