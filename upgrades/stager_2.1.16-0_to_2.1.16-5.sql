/******************************************************************************
 *                 stager_2.1.16-0_to_2.1.16-5.sql
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This script upgrades a CASTOR v2.1.16-0 STAGER database to v2.1.16-5
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

/* Stop on errors */
WHENEVER SQLERROR EXIT FAILURE
BEGIN
  -- If we have encountered an error rollback any previously non committed
  -- operations. This prevents the UPDATE of the UpgradeLog from committing
  -- inconsistent data to the database.
  ROLLBACK;
  UPDATE UpgradeLog
     SET failureCount = failureCount + 1
   WHERE schemaVersion = '2_1_15_18'
     AND release = '2_1_16_5'
     AND state != 'COMPLETE';
  COMMIT;
END;
/

/* Verify that the script is running against the correct schema and version */
DECLARE
  unused VARCHAR(100);
BEGIN
  SELECT release INTO unused FROM CastorVersion
   WHERE schemaName = 'STAGER'
     AND release LIKE '2_1_16_0%';
EXCEPTION WHEN NO_DATA_FOUND THEN
  -- Error, we cannot apply this script
  raise_application_error(-20000, 'PL/SQL release mismatch. Please run previous upgrade scripts for the STAGER before this one.');
END;
/

INSERT INTO UpgradeLog (schemaVersion, release, type)
VALUES ('2_1_15_18', '2_1_16_5', 'TRANSPARENT');
COMMIT;

/* Job management */
BEGIN
  FOR a IN (SELECT * FROM user_scheduler_jobs)
  LOOP
    -- Stop any running jobs
    IF a.state = 'RUNNING' THEN
      dbms_scheduler.stop_job(a.job_name, force=>TRUE);
    END IF;
    -- Schedule the start date of the job to 15 minutes from now. This
    -- basically pauses the job for 15 minutes so that the upgrade can
    -- go through as quickly as possible.
    dbms_scheduler.set_attribute(a.job_name, 'START_DATE', SYSDATE + 15/1440);
  END LOOP;
END;
/

DROP INDEX I_DiskServer_DP_ST_HW;


/* Upgrade PL/SQL code base */
/****************************/

/* PL/SQL method implementing selectFiles2Delete
   This is the standard garbage collector: it sorts VALID diskcopies
   that do not need to go to tape by gcWeight and selects them for deletion up to
   the desired free space watermark */
CREATE OR REPLACE PROCEDURE selectFiles2Delete(diskServerName IN VARCHAR2,
                                               files OUT castorGC.SelectFiles2DeleteLine_Cur) AS
  dcIds "numList";
  freed INTEGER;
  deltaFree INTEGER;
  toBeFreed INTEGER;
  dontGC INTEGER;
  totalCount INTEGER;
  unused INTEGER;
  backoff INTEGER;
  CastorFileLocked EXCEPTION;
  PRAGMA EXCEPTION_INIT (CastorFileLocked, -54);
BEGIN
  -- First of all, check if we are in a Disk1 pool
  dontGC := 0;
  FOR sc IN (SELECT disk1Behavior
               FROM SvcClass, DiskPool2SvcClass D2S, DiskServer, FileSystem
              WHERE SvcClass.id = D2S.child
                AND D2S.parent = FileSystem.diskPool
                AND FileSystem.diskServer = DiskServer.id
                AND DiskServer.name = diskServerName
              UNION ALL
             SELECT disk1Behavior
               FROM SvcClass, DataPool2SvcClass, DiskServer
              WHERE SvcClass.id = DataPool2SvcClass.child
                AND DataPool2SvcClass.parent = DiskServer.dataPool
                AND DiskServer.name = diskServerName) LOOP
    -- If any of the service classes to which we belong (normally a single one)
    -- say this is Disk1, we don't GC files.
    IF sc.disk1Behavior = 1 THEN
      dontGC := 1;
      EXIT;
    END IF;
  END LOOP;

  -- Loop on all concerned fileSystems/DataPools in a random order.
  totalCount := 0;
  FOR fs IN (SELECT * FROM (SELECT DBMS_Random.value, FileSystem.id AS fsId, 0 AS dpId
                              FROM FileSystem, DiskServer
                             WHERE FileSystem.diskServer = DiskServer.id
                               AND DiskServer.name = diskServerName
                             UNION ALL
                            SELECT DBMS_Random.value, 0 AS fsId, DiskServer.dataPool AS dpId
                              FROM DiskServer
                             WHERE DiskServer.name = diskServerName)
             ORDER BY 1) LOOP
    -- Count the number of diskcopies on this filesystem that are in a
    -- BEINGDELETED state. These need to be reselected in any case.
    freed := 0;
    SELECT totalCount + count(*), nvl(sum(DiskCopy.diskCopySize), 0)
      INTO totalCount, freed
      FROM DiskCopy
     WHERE (fileSystem = fs.fsId OR dataPool = fs.dpId)
       AND decode(status, 9, status, NULL) = 9;  -- BEINGDELETED (decode used to use function-based index)

    -- estimate the number of GC running the "long" query, that is the one dealing with the GCing of
    -- VALID files.
    SELECT COUNT(*) INTO backoff
      FROM v$session s, v$sqltext t
     WHERE s.sql_id = t.sql_id AND t.sql_text LIKE '%I_DiskCopy_FS_GCW%';

    -- Process diskcopies that are in an INVALID state.
    UPDATE /*+ INDEX_RS_ASC(DiskCopy I_DiskCopy_Status_7_FS_DP)) */ DiskCopy
       SET status = 9, -- BEINGDELETED
           gcType = decode(gcType, NULL, dconst.GCTYPE_USER, gcType)
     WHERE (nvl(fileSystem,0)+nvl(dataPool,0) = fs.fsId + fs.dpId)
       AND decode(status, 7, status, NULL) = 7  -- INVALID (decode used to use function-based index)
       AND rownum <= 10000 - totalCount
    RETURNING id BULK COLLECT INTO dcIds;
    COMMIT;

    -- If we have more than 10,000 files to GC, exit the loop. There is no point
    -- processing more as the maximum sent back to the client in one call is
    -- 10,000. This protects the garbage collector from being overwhelmed with
    -- requests and reduces the stager DB load. Furthermore, if too much data is
    -- sent back to the client, the transfer time between the stager and client
    -- becomes very long and the message may timeout or may not even fit in the
    -- clients receive buffer!
    totalCount := totalCount + dcIds.COUNT();
    EXIT WHEN totalCount >= 10000;

    -- Continue processing but with VALID files, only in case we are not already loaded
    IF dontGC = 0 AND backoff < 4 THEN
      -- Do not delete VALID files from non production hardware
      BEGIN
        IF fs.fsId > 0 THEN
          SELECT FileSystem.id INTO unused
            FROM DiskServer, FileSystem
           WHERE FileSystem.id = fs.fsId
             AND FileSystem.status IN (dconst.FILESYSTEM_PRODUCTION, dconst.FILESYSTEM_READONLY)
             AND FileSystem.diskserver = DiskServer.id
             AND DiskServer.status IN (dconst.DISKSERVER_PRODUCTION, dconst.DISKSERVER_READONLY)
             AND DiskServer.hwOnline = 1;
        ELSE
          SELECT DiskServer.id INTO unused
            FROM DiskServer
           WHERE dataPool = fs.dpId
             AND status IN (dconst.DISKSERVER_PRODUCTION, dconst.DISKSERVER_READONLY)
             AND hwOnline = 1
             AND ROWNUM < 2;
        END IF;
      EXCEPTION WHEN NO_DATA_FOUND THEN
        EXIT;
      END;
      -- Calculate the amount of space that would be freed on the filesystem
      -- if the files selected above were to be deleted.
      IF dcIds.COUNT > 0 THEN
        SELECT /*+ INDEX(DiskCopy PK_DiskCopy_Id) */ freed + sum(diskCopySize) INTO freed
          FROM DiskCopy
         WHERE DiskCopy.id IN
             (SELECT /*+ CARDINALITY(fsidTable 5) */ *
                FROM TABLE(dcIds) dcidTable);
      END IF;
      -- Get the amount of space to be liberated
      IF fs.fsId > 0 THEN
        SELECT decode(sign(maxFreeSpace * totalSize - free), -1, 0, maxFreeSpace * totalSize - free)
          INTO toBeFreed
          FROM FileSystem
         WHERE id = fs.fsId;
      ELSE
        SELECT decode(sign(maxFreeSpace * totalSize - free), -1, 0, maxFreeSpace * totalSize - free)
          INTO toBeFreed
          FROM DataPool
         WHERE id = fs.dpId;
      END IF;
      -- If space is still required even after removal of INVALID files, consider
      -- removing VALID files until we are below the free space watermark
      IF freed < toBeFreed THEN
        -- Loop on file deletions
        FOR dc IN (SELECT /*+ INDEX_RS_ASC(DiskCopy I_DiskCopy_FS_DP_GCW) */ DiskCopy.id, castorFile
                     FROM DiskCopy, CastorFile
                    WHERE (nvl(fileSystem,0)+nvl(dataPool,0) = fs.fsId + fs.dpId)
                      AND status = dconst.DISKCOPY_VALID
                      AND CastorFile.id = DiskCopy.castorFile
                      AND CastorFile.tapeStatus IN (dconst.CASTORFILE_DISKONLY, dconst.CASTORFILE_ONTAPE)
                      ORDER BY gcWeight ASC) LOOP
          BEGIN
            -- Lock the CastorFile
            SELECT id INTO unused FROM CastorFile
             WHERE id = dc.castorFile FOR UPDATE NOWAIT;
            -- Mark the DiskCopy as being deleted
            UPDATE DiskCopy
               SET status = dconst.DISKCOPY_BEINGDELETED,
                   gcType = dconst.GCTYPE_AUTO
             WHERE id = dc.id RETURNING diskCopySize INTO deltaFree;
            totalCount := totalCount + 1;
            -- Update freed space
            freed := freed + deltaFree;
            -- update importance of remianing copies of the file if any
            UPDATE DiskCopy
               SET importance = importance + 1
             WHERE castorFile = dc.castorFile
               AND status = dconst.DISKCOPY_VALID;
            -- Shall we continue ?
            IF toBeFreed <= freed THEN
              EXIT;
            END IF;
            IF totalCount >= 10000 THEN
              EXIT;
            END IF;           
          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              -- The file no longer exists or has the wrong state
              NULL;
            WHEN CastorFileLocked THEN
              -- Go to the next candidate, processing is taking place on the
              -- file
              NULL;
          END;
          COMMIT;
        END LOOP;
      END IF;
    END IF;
    -- We have enough files to exit the loop ?
    EXIT WHEN totalCount >= 10000;
  END LOOP;

  -- Now select all the BEINGDELETED diskcopies in this diskserver for the GC daemon
  OPEN files FOR
    SELECT /*+ INDEX(CastorFile PK_CastorFile_ID) */
           DC.path, DC.id,
           Castorfile.fileid, Castorfile.nshost,
           DC.lastAccessTime, DC.nbCopyAccesses, DC.gcWeight,
           DC.gcType, DC.svcClassList
      FROM CastorFile,
           (SELECT DiskCopy.castorFile,
                   FileSystem.mountPoint || DiskCopy.path AS path, DiskCopy.id,
                   DiskCopy.lastAccessTime, DiskCopy.nbCopyAccesses, DiskCopy.gcWeight,
                   getObjStatusName('DiskCopy', 'gcType', DiskCopy.gcType) AS gcType,
                   getSvcClassList(FileSystem.id) AS svcClassList
              FROM FileSystem, DiskServer, DiskCopy
             WHERE decode(DiskCopy.status, 9, DiskCopy.status, NULL) = 9 -- BEINGDELETED
               AND DiskCopy.fileSystem = FileSystem.id
               AND FileSystem.diskServer = DiskServer.id
               AND DiskServer.name = diskServerName
             UNION ALL
            SELECT DiskCopy.castorFile,
                   DataPool.externalUser || '@' || DataPool.externalPool || ':' || DiskCopy.path AS path, DiskCopy.id,
                   DiskCopy.lastAccessTime, DiskCopy.nbCopyAccesses, DiskCopy.gcWeight,
                   getObjStatusName('DiskCopy', 'gcType', DiskCopy.gcType) AS gcType,
                   getSvcClassListDP(DiskServer.dataPool) AS svcClassList
              FROM DiskServer, DiskCopy, DataPool
             WHERE decode(DiskCopy.status, 9, DiskCopy.status, NULL) = 9 -- BEINGDELETED
               AND DiskCopy.dataPool = DataPool.id
               AND DiskCopy.dataPool = DiskServer.dataPool
               AND DiskServer.name = diskServerName
           ) DC
     WHERE DC.castorfile = CastorFile.id
       AND rownum <= 10000;
END;
/

/* PL/SQL method implementing filesDeletionFailedProc */
CREATE OR REPLACE PROCEDURE filesDeletionFailedProc
(dcIds IN castor."cnumList") AS
  varUnused NUMBER;
  DCLocked EXCEPTION;
  PRAGMA EXCEPTION_INIT (DCLocked, -54);
BEGIN
  IF dcIds.COUNT > 0 THEN
    -- Loop over the files
    FOR i IN 1..dcIds.COUNT LOOP
      BEGIN
        -- try to lock the next file
        SELECT id INTO varUnused FROM DiskCopy
         WHERE id = dcIds(i) FOR UPDATE NOWAIT;
      EXCEPTION
        WHEN DCLocked THEN
          -- lock already taken by someone else
          -- we commit our current locks so to avoid
          -- dead locks and retry without NOWAIT
          COMMIT;
          SELECT id INTO varUnused FROM DiskCopy
           WHERE id = dcIds(i) FOR UPDATE;
        WHEN NO_DATA_FOUND THEN
          -- this diskCopy has disappeared in the mean time
          -- not much we can do
          CONTINUE;
      END;
      -- we have the lock, perform the update
      UPDATE DiskCopy SET status = 4 -- FAILED
       WHERE id = dcIds(i);
    END LOOP;
    COMMIT;
  END IF;
END;
/

/* PL/SQL method implementing bestFileSystemForRecall */
CREATE OR REPLACE PROCEDURE bestFileSystemForRecall(inCfId IN INTEGER, outFilePath OUT VARCHAR2) AS
  varCfId INTEGER;
  nb NUMBER;
BEGIN
  outFilePath := '';
  -- try and select a good FileSystem/DataPool for this recall
  FOR f IN (SELECT * FROM (
              SELECT /*+ INDEX_RS_ASC(RecallJob I_RecallJob_Castorfile_VID) */
                     DiskServer.name ||':'|| FileSystem.mountPoint AS remotePath, FileSystem.id,
                     CastorFile.fileSize, CastorFile.fileId, CastorFile.nsHost,
                     1 isDiskPool
                FROM DiskServer, FileSystem, DiskPool2SvcClass, CastorFile, RecallJob
               WHERE CastorFile.id = inCfId
                 AND RecallJob.castorFile = inCfId
                 AND RecallJob.svcClass = DiskPool2SvcClass.child
                 AND FileSystem.diskPool = DiskPool2SvcClass.parent
                 -- a priori, we want to have enough free space. However, if we don't, we accept to start writing
                 -- if we have a minimum of 30GB free and count on gerbage collection to liberate space while writing
                 -- We still check that the file fit on the disk, and actually keep a 30% margin so that very recent
                 -- files can be kept
                 AND (FileSystem.free - FileSystem.minAllowedFreeSpace * FileSystem.totalSize > CastorFile.fileSize
                   OR (FileSystem.free - FileSystem.minAllowedFreeSpace * FileSystem.totalSize > 30000000000
                   AND FileSystem.totalSize * 0.7 > CastorFile.fileSize))
                 AND FileSystem.status = dconst.FILESYSTEM_PRODUCTION
                 AND DiskServer.id = FileSystem.diskServer
                 AND DiskServer.status = dconst.DISKSERVER_PRODUCTION
                 AND DiskServer.hwOnline = 1
            ORDER BY -- order by filesystem load first: this works if the feedback loop is fast enough, that is
                     -- the transfer of the selected files in bulk does not take more than a couple of minutes
                     FileSystem.nbMigratorStreams + FileSystem.nbRecallerStreams ASC,
                     -- then use randomness for tie break
                     DBMS_Random.value)
             UNION ALL
            SELECT * FROM (
              -- take max 3 random diskservers in the data pools involved
              SELECT /*+ INDEX_RS_ASC(RecallJob I_RecallJob_Castorfile_VID) */
                     'radosstriper:///' || DataPool.externalUser || '@' ||
                     DataPool.externalPool || ':' AS remotePath, 0 as id,
                     CastorFile.fileSize, CastorFile.fileId, CastorFile.nsHost, 0 isDiskPool
                FROM DataPool, DataPool2SvcClass, CastorFile, RecallJob
               WHERE CastorFile.id = inCfId
                 AND RecallJob.castorFile = inCfId
                 AND RecallJob.svcClass = DataPool2SvcClass.child
                 -- a priori, we want to have enough free space. However, if we don't, we accept to start writing
                 -- if we have a minimum of 30GB free and count on gerbage collection to liberate space while writing
                 -- We still check that the file fit on the disk, and actually keep a 30% margin so that very recent
                 -- files can be kept
                 AND (DataPool.free - DataPool.minAllowedFreeSpace * DataPool.totalSize > CastorFile.fileSize
                   OR (DataPool.free - DataPool.minAllowedFreeSpace * DataPool.totalSize > 30000000000
                   AND DataPool.totalSize * 0.7 > CastorFile.fileSize))
               ORDER BY DBMS_Random.value))
  LOOP
    buildPathFromFileId(f.fileId, f.nsHost, ids_seq.nextval, outFilePath, f.isDiskPool = 1);
    outFilePath := f.remotePath || outFilePath;
    -- In case we selected a filesystem (and no datapool)
    IF f.id > 0 THEN
      -- Check that we don't already have a copy of this file on the selected filesystem.
      -- This will never happen in normal operations but may be the case if a filesystem
      -- was disabled and did come back while the tape recall was waiting.
      -- Even if we optimize by cancelling remaining unneeded tape recalls when a
      -- fileSystem comes back, the ones running at the time of the come back will have
      -- the problem.
      SELECT /*+ INDEX_RS_ASC(DiskCopy I_DiskCopy_CastorFile) */ count(*) INTO nb
        FROM DiskCopy
       WHERE fileSystem = f.id
         AND castorfile = inCfid
         AND status = dconst.DISKCOPY_VALID;
      IF nb != 0 THEN
        raise_application_error(-20115, 'Recaller could not find a FileSystem in production in the requested SvcClass and without copies of this file');
      END IF;
      RETURN;
    END IF;
  END LOOP;
  IF outFilePath IS NULL THEN
    raise_application_error(-20115, 'No suitable filesystem found for this recalled file');
  END IF;
END;
/

/* Get next candidates for a given migration mount.
 * input:  VDQM transaction id, count and total size
 * output: outVid    the target VID,
           outFiles  a cursor for the set of migration candidates. 
 * Locks are taken on the selected migration jobs.
 *
 * We should only propose a migration job for a file that does not
 * already have another copy migrated to the same tape.
 * The already migrated copies are kept in MigratedSegment until the whole set
 * of siblings has been migrated.
 */
CREATE OR REPLACE PROCEDURE tg_getBulkFilesToMigrate(inLogContext IN VARCHAR2,
                                                     inMountTrId IN NUMBER,
                                                     inCount IN INTEGER,
                                                     inTotalSize IN INTEGER,
                                                     outFiles OUT castorTape.FileToMigrateCore_cur) AS
  varMountId NUMBER;
  varCount INTEGER;
  varTotalSize INTEGER;
  varOldestAcceptableAge NUMBER;
  varVid VARCHAR2(10);
  varNewFseq INTEGER;
  varFileTrId NUMBER;
  varTpId INTEGER;
  varUnused INTEGER;
  CONSTRAINT_VIOLATED EXCEPTION;
  PRAGMA EXCEPTION_INIT(CONSTRAINT_VIOLATED, -00001);
BEGIN
  BEGIN
    -- Get id, VID and last valid fseq for this migration mount, lock
    SELECT id, vid, tapePool, lastFSeq INTO varMountId, varVid, varTpId, varNewFseq
      FROM MigrationMount
     WHERE mountTransactionId = inMountTrId
       FOR UPDATE;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    -- migration mount is over or unknown request: return an empty cursor
    OPEN outFiles FOR
      SELECT fileId, nsHost, lastKnownFileName, filePath, fileTransactionId, fseq, fileSize
        FROM FilesToMigrateHelper;
    RETURN;
  END;
  varCount := 0;
  varTotalSize := 0;
  SELECT TapePool.maxFileAgeBeforeMount INTO varOldestAcceptableAge
    FROM TapePool, MigrationMount
   WHERE MigrationMount.id = varMountId
     AND MigrationMount.tapePool = TapePool.id;
  -- We will use this value as a divisor, so we set the oldest age to at least
  -- one second.
  IF varOldestAcceptableAge = 0 THEN
    varOldestAcceptableAge := 1;
  END IF;
  -- Get candidates up to inCount or inTotalSize
  FOR Cand IN (
    SELECT /*+ FIRST_ROWS(100)
               LEADING(Job CastorFile Location)
               USE_NL(Job CastorFile Location)
               INDEX(CastorFile PK_CastorFile_Id) */
           Job.id mjId, Location.filePath,
           CastorFile.fileId, CastorFile.nsHost, CastorFile.fileSize, CastorFile.lastKnownFileName,
           Castorfile.id as castorfile
      FROM (SELECT * FROM
             (SELECT /*+ FIRST_ROWS(100) INDEX_RS_ASC(MigrationJob I_MigrationJob_TPStatusCT) */
                     id, castorfile, destCopyNb, creationTime
                FROM MigrationJob
               WHERE tapePool = varTpId
                 AND status = tconst.MIGRATIONJOB_PENDING
               ORDER BY creationTime)
             WHERE ROWNUM < TO_NUMBER(getConfigOption('Migration', 'NbMigCandConsidered', 10000))) Job,
           CastorFile,
           (SELECT /*+ LEADING(DiskCopy FileSystem DiskServer DiskPool)
                       USE_NL(DiskCopy FileSystem DiskServer DiskPool)
                       INDEX_RS_ASC(DiskCopy I_DiskCopy_CastorFile) */
                   DiskCopy.castorFile,
                   DiskServer.name || ':' || FileSystem.mountPoint || DiskCopy.path AS filePath,
                   FileSystem.nbRecallerStreams + FileSystem.nbMigratorStreams AS rate,
                   DiskPool.migrationPriority AS poolPrio
              FROM DiskPool, FileSystem, DiskServer, DiskCopy
             WHERE DiskCopy.status = dconst.DISKCOPY_VALID
               AND FileSystem.id = DiskCopy.fileSystem
               AND FileSystem.diskPool = DiskPool.id
               AND FileSystem.status IN (dconst.FILESYSTEM_PRODUCTION,
                                         dconst.FILESYSTEM_DRAINING,
                                         dconst.FILESYSTEM_READONLY)
               AND DiskServer.id = FileSystem.diskServer
               AND DiskServer.status IN (dconst.DISKSERVER_PRODUCTION,
                                         dconst.DISKSERVER_DRAINING,
                                         dconst.DISKSERVER_READONLY)
               AND DiskServer.hwOnline = 1
             UNION ALL
            SELECT /*+ LEADING(DiskCopy DataPool)
                       USE_NL(DiskCopy DataPool)
                       INDEX_RS_ASC(DiskCopy I_DiskCopy_CastorFile) */
                   DiskCopy.castorFile,
                   'radosstriper:///' || DataPool.externalUser || '@' ||
                   DataPool.externalPool || ':' || DiskCopy.path AS filePath, 0 AS rate,
                   DataPool.migrationPriority AS poolPrio
              FROM DiskCopy, DataPool
             WHERE DiskCopy.status = dconst.DISKCOPY_VALID
               AND DiskCopy.dataPool = DataPool.id) Location
     WHERE CastorFile.id = Job.castorFile
       AND CastorFile.id = Location.castorFile
       AND CastorFile.tapeStatus = dconst.CASTORFILE_NOTONTAPE
       AND NOT EXISTS (SELECT /*+ USE_NL(MigratedSegment)
                                  INDEX_RS_ASC(MigratedSegment I_MigratedSegment_CFCopyNBVID) */ 1
                         FROM MigratedSegment
                        WHERE MigratedSegment.castorFile = Job.castorfile
                          AND MigratedSegment.copyNb != Job.destCopyNb
                          AND MigratedSegment.vid = varVid)
       ORDER BY -- we first order by a multi-step function, which gives old guys incrasingly more priority:
                -- migrations younger than varOldestAcceptableAge will be taken last
                TRUNC(Job.creationTime/varOldestAcceptableAge) ASC,
                -- then we take into account the migration priority of the pools in case we have many,
                -- higher priority pools come first
                Location.poolPrio DESC,
                -- and then, for all migrations between (N-1)*varOldestAge and N*varOldestAge, by filesystem load
                Location.rate ASC,
                -- final possibility : random choice
                DBMS_Random.value)
  LOOP
    -- last part of the above statement. Could not be part of it as ORACLE insisted on not
    -- optimizing properly the execution plan
    BEGIN
      SELECT /*+ INDEX_RS_ASC(MJ I_MigrationJob_CFVID) */ 1 INTO varUnused
        FROM MigrationJob MJ
       WHERE MJ.castorFile = Cand.castorFile
         AND MJ.vid = varVid
         AND MJ.vid IS NOT NULL;
      -- found one, so skip this candidate
      CONTINUE;
    EXCEPTION WHEN NO_DATA_FOUND THEN
      -- nothing, it's a valid candidate. Let's lock it and revalidate the status
      DECLARE
        MjLocked EXCEPTION;
        PRAGMA EXCEPTION_INIT (MjLocked, -54);
      BEGIN
        SELECT id INTO varUnused
          FROM MigrationJob
         WHERE id = Cand.mjId
           AND status = tconst.MIGRATIONJOB_PENDING
           FOR UPDATE NOWAIT;
      EXCEPTION WHEN MjLocked THEN
        -- this migration job is being handled else where, let's go to next one
        CONTINUE;
                WHEN NO_DATA_FOUND THEN
        -- this migration job has already been handled else where, let's go to next one
        CONTINUE;
      END;
    END;
    BEGIN
      -- Try to take this candidate on this mount
      INSERT INTO FilesToMigrateHelper (fileId, nsHost, lastKnownFileName, filePath, fileTransactionId, fileSize, fseq)
        VALUES (Cand.fileId, Cand.nsHost, Cand.lastKnownFileName, Cand.filePath, ids_seq.NEXTVAL, Cand.fileSize, varNewFseq)
        RETURNING fileTransactionId INTO varFileTrId;
    EXCEPTION WHEN CONSTRAINT_VIOLATED THEN
      -- If we fail here, it means that another copy of this file was already selected for this mount.
      -- Not a big deal, we skip this candidate and keep going.
      CONTINUE;
    END;
    varCount := varCount + 1;
    varTotalSize := varTotalSize + Cand.fileSize;
    UPDATE MigrationJob
       SET status = tconst.MIGRATIONJOB_SELECTED,
           vid = varVid,
           fSeq = varNewFseq,
           mountTransactionId = inMountTrId,
           fileTransactionId = varFileTrId
     WHERE id = Cand.mjId;
    varNewFseq := varNewFseq + 1;    -- we still decide the fseq for each migration candidate
    IF varCount >= inCount OR varTotalSize >= inTotalSize THEN
      -- we have enough candidates for this round, exit loop
      EXIT;
    END IF;
  END LOOP;
  -- Update last fseq
  UPDATE MigrationMount
     SET lastFseq = varNewFseq
   WHERE id = varMountId;
  -- Return all candidates (potentially an empty cursor). Don't commit now, this will be done
  -- in C++ after the results have been collected as the temporary table will be emptied.
  OPEN outFiles FOR
    SELECT fileId, nsHost, lastKnownFileName, filePath, fileTransactionId, fseq, fileSize
      FROM FilesToMigrateHelper;
END;
/


/* Recompile all invalid procedures, triggers and functions */
/************************************************************/
BEGIN
  recompileAll();
END;
/

/* Flag the schema upgrade as COMPLETE */
/***************************************/
UPDATE UpgradeLog SET endDate = systimestamp, state = 'COMPLETE'
 WHERE release = '2_1_16_5';
COMMIT;
