/******************************************************************************
 *                 stager_2.1.16-10_to_2.1.16-13.sql
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This script upgrades a CASTOR v2.1.16-10 STAGER database to v2.1.16-13
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

/* Stop on errors */
WHENEVER SQLERROR EXIT FAILURE
BEGIN
  -- If we have encountered an error rollback any previously non committed
  -- operations. This prevents the UPDATE of the UpgradeLog from committing
  -- inconsistent data to the database.
  ROLLBACK;
  UPDATE UpgradeLog
     SET failureCount = failureCount + 1
   WHERE schemaVersion = '2_1_15_18'
     AND release = '2_1_16_13'
     AND state != 'COMPLETE';
  COMMIT;
END;
/

/* Verify that the script is running against the correct schema and version */
DECLARE
  unused VARCHAR(100);
BEGIN
  SELECT release INTO unused FROM CastorVersion
   WHERE schemaName = 'STAGER'
     AND release LIKE '2_1_16_10%';
EXCEPTION WHEN NO_DATA_FOUND THEN
  -- Error, we cannot apply this script
  raise_application_error(-20000, 'PL/SQL release mismatch. Please run previous upgrade scripts for the STAGER before this one.');
END;
/

INSERT INTO UpgradeLog (schemaVersion, release, type)
VALUES ('2_1_15_18', '2_1_16_13', 'TRANSPARENT');
COMMIT;

/* Job management */
BEGIN
  FOR a IN (SELECT * FROM user_scheduler_jobs)
  LOOP
    -- Stop any running jobs
    IF a.state = 'RUNNING' THEN
      dbms_scheduler.stop_job(a.job_name, force=>TRUE);
    END IF;
    -- Schedule the start date of the job to 15 minutes from now. This
    -- basically pauses the job for 15 minutes so that the upgrade can
    -- go through as quickly as possible.
    dbms_scheduler.set_attribute(a.job_name, 'START_DATE', SYSDATE + 15/1440);
  END LOOP;
END;
/

/* PL/SQL code upgrade */

CREATE OR REPLACE VIEW LateMigrationsView AS
  SELECT /*+ LEADING(CF MJ CnsFile DC FileSystem DiskServer) CARDINALITY(MJ 10) CARDINALITY(CF 10) */
         CF.fileId, CF.lastKnownFileName AS filePath, CF.fileSize,
         decode(MJ.creationTime, NULL, -1, getTime() - MJ.creationTime) AS mjElapsedTime, nvl(MJ.status, -1) AS mjStatus,
         DC.creationTime AS dcCreationTime, DiskServer.name || ':' || FileSystem.mountPoint || DC.path AS location,
         decode(DiskServer.hwOnline, 0, 'N',
           decode(DiskServer.status, 2, 'N',
             decode(FileSystem.status, 2, 'N', 'Y'))) AS available
    FROM CastorFile CF, DiskCopy DC, MigrationJob MJ, cns_file_metadata@remotens CnsFile,
         FileSystem, DiskServer
   WHERE CF.fileId = CnsFile.fileId
     AND DC.castorFile = CF.id
     AND MJ.castorFile(+) = CF.id
     AND DC.fileSystem = FileSystem.id
     AND FileSystem.diskServer = DiskServer.id
     AND CF.tapeStatus = 0  -- CASTORFILE_NOTONTAPE
     AND DC.status = 0  -- DISKCOPY_VALID
     AND CF.fileSize > 0
     AND DC.creationTime < getTime() - 86400
  UNION
  SELECT /*+ LEADING(CF MJ CnsFile DC FileSystem DiskServer) CARDINALITY(MJ 10) CARDINALITY(CF 10) */
         CF.fileId, CF.lastKnownFileName AS filePath, CF.fileSize,
         decode(MJ.creationTime, NULL, -1, getTime() - MJ.creationTime) AS mjElapsedTime, nvl(MJ.status, -1) AS mjStatus,
         DC.creationTime AS dcCreationTime, DataPool.externalUser || '@' || DataPool.externalPool || ':' || DC.path AS location, 'Y' as available
    FROM CastorFile CF, DiskCopy DC, MigrationJob MJ, DataPool, cns_file_metadata@remotens CnsFile
   WHERE CF.fileId = CnsFile.fileId
     AND DC.castorFile = CF.id
     AND MJ.castorFile(+) = CF.id
     AND DC.dataPool = DataPool.id
     AND CF.tapeStatus = 0  -- CASTORFILE_NOTONTAPE
     AND DC.status = 0  -- DISKCOPY_VALID
     AND CF.fileSize > 0
     AND DC.creationTime < getTime() - 86400
  ORDER BY dcCreationTime DESC;


/* CastorMon Package Body */
CREATE OR REPLACE PACKAGE BODY CastorMon AS

  /**
   * PL/SQL method implementing waitTapeMigrationStats
   * See the castorMon package specification for documentation.
   */
  PROCEDURE waitTapeMigrationStats AS
    CURSOR rec IS
      SELECT tapePoolName tapePool,
             nvl(sum(fileSize), 0) totalFileSize,
             count(fileSize) nbFiles,
             count(distinct(vid)) nbTapes,
             nvl(trunc(getTime()-max(creationTime)), 0) minAge,
             nvl(trunc(getTime()-min(creationTime)), 0) maxAge
      FROM (
        SELECT
               TapePool.name tapePoolName, MigrationJob.*
        FROM MigrationJob, TapePool
        WHERE TapePool.id = MigrationJob.tapePool(+) -- Left outer join to have zeroes when there is no migrations
      )
      GROUP BY tapePoolName;
  BEGIN
    FOR r in rec LOOP
      logToDLF(NULL, dlf.LVL_SYSTEM, 'waitTapeMigrationStats', 0, '', 'stagerd', 
            'TapePool="' || r.tapePool ||
            '" totalFileSize="' || r.totalFileSize || 
            '" nbFiles="' || r.nbFiles ||
            '" nbTapes="' || r.nbTapes ||
            '" minAge="' || r.minAge ||
            '" maxAge="' || r.maxAge || '"');
    END LOOP;
  END waitTapeMigrationStats;

  /**
   * PL/SQL method implementing waitTapeRecallStats
   * See the castorMon package specification for documentation.
   */
  PROCEDURE waitTapeRecallStats AS
    CURSOR rec IS
      SELECT svcClassName svcClass,
             nvl(sum(fileSize), 0) totalFileSize,
             count(fileSize) nbFiles,
             count(distinct(vid)) nbTapes,
             nvl(trunc(getTime()-max(creationTime)), 0) minAge,
             nvl(trunc(getTime()-min(creationTime)), 0) maxAge
        FROM (
        SELECT SvcClass.name svcClassName, RecallJob.*
          FROM RecallJob, SvcClass
         WHERE SvcClass.id = RecallJob.svcClass(+) -- Left outer join to have zeroes when there is no recall
      )
      GROUP BY svcClassName;
  BEGIN
    FOR r in rec LOOP
      logToDLF(NULL, dlf.LVL_SYSTEM, 'waitTapeRecallStats', 0, '', 'stagerd', 
            'SvcClass="' || r.svcClass ||
            '" totalFileSize="' || r.totalFileSize || 
            '" nbFiles="' || r.nbFiles ||
            '" nbTapes="' || r.nbTapes ||
            '" minAge="' || r.minAge ||
            '" maxAge="' || r.maxAge || '"');
    END LOOP;
  END waitTapeRecallStats;

END CastorMon;
/



/* PL/SQL declaration for the castorDebug package */
CREATE OR REPLACE PACKAGE castorDebug AS
  TYPE DiskCopyDebug_typ IS RECORD (
    id INTEGER,
    status VARCHAR2(2048),
    creationtime VARCHAR2(2048),
    pool VARCHAR2(2048),
    location VARCHAR2(2048),
    available CHAR(1),
    diskCopySize NUMBER,
    castorFileSize NUMBER,
    gcWeight NUMBER);
  TYPE DiskCopyDebug IS TABLE OF DiskCopyDebug_typ;
  TYPE SubRequestDebug IS TABLE OF SubRequest%ROWTYPE;
  TYPE RequestDebug_typ IS RECORD (
    id NUMBER,
    status VARCHAR2(100),
    creationTime VARCHAR2(2048),
    transferId VARCHAR2(2048),
    username VARCHAR2(2048),
    machine VARCHAR2(2048),
    svcClassName VARCHAR2(2048),
    ReqId NUMBER,
    ReqType VARCHAR2(20));
  TYPE RequestDebug IS TABLE OF RequestDebug_typ;
  TYPE RecallJobDebug_typ IS RECORD (
    id INTEGER,
    status VARCHAR2(2048),
    creationtime VARCHAR2(2048),
    fseq INTEGER,
    copyNb INTEGER,
    recallGroup VARCHAR(2048),
    svcClass VARCHAR(2048),
    euid INTEGER,
    egid INTEGER,
    vid VARCHAR(2048),
    nbRetriesWithinMount INTEGER,
    nbMounts INTEGER);
  TYPE RecallJobDebug IS TABLE OF RecallJobDebug_typ;
  TYPE MigrationJobDebug_typ IS RECORD (
    id INTEGER,
    status VARCHAR2(2048),
    creationTime VARCHAR2(2048),
    fileSize INTEGER,
    tapePoolName VARCHAR2(2048),
    destCopyNb INTEGER,
    fseq INTEGER,
    mountTransactionId INTEGER,
    originalVID VARCHAR2(2048),
    originalCopyNb INTEGER,
    nbRetries INTEGER,
    fileTransactionId INTEGER);
  TYPE MigrationJobDebug IS TABLE OF MigrationJobDebug_typ;
  TYPE Disk2DiskCopyJobDebug_typ IS RECORD (
    id INTEGER,
    status VARCHAR2(2048),
    creationTime VARCHAR2(2048),
    transferId VARCHAR2(2048),
    retryCounter INTEGER,
    nsOpenTime INTEGER,
    destSvcClassName VARCHAR2(2048),
    replicationType VARCHAR2(2048),
    srcDCId INTEGER,
    destDCId INTEGER,
    drainingJob INTEGER);
  TYPE Disk2DiskCopyJobDebug IS TABLE OF Disk2DiskCopyJobDebug_typ;
  TYPE NameserverDebug_typ IS RECORD (
    fileid INTEGER,
    status CHAR(1),
    className VARCHAR2(100),
    filePath VARCHAR2(2048),
    fileSize INTEGER,
    fileChecksum VARCHAR2(10),
    aTime VARCHAR2(100),
    mTime VARCHAR2(100),
    stagerOpenTime INTEGER);
  TYPE NameserverDebug IS TABLE OF NameserverDebug_typ;
  TYPE NameserverSegDebug_typ IS RECORD (
    fileid INTEGER,
    copyno NUMBER(1),
    segStatus CHAR(1),
    vid VARCHAR2(10),
    tapeStatus VARCHAR2(100),
    fseq NUMBER(10),
    segSize NUMBER,
    segChecksum VARCHAR2(10),
    segCreationTime VARCHAR2(100),
    segLastModificationTime VARCHAR2(100));
  TYPE NameserverSegDebug IS TABLE OF NameserverSegDebug_typ;
END;
/

/* Get the Namespace segments associated with the input fileid */
CREATE OR REPLACE FUNCTION getNSegs(inId number) RETURN castorDebug.NameserverSegDebug PIPELINED AS
BEGIN
  FOR s IN (SELECT s_fileid, copyno, s_status AS segStatus, NSSeg.vid, tapeStatusToString(VmgrTape.status) AS tapeStatus,
                   fseq, NSSeg.segSize, TRIM(TO_CHAR(NSSeg.checksum, 'xxxxxxxx')) AS segChecksum,
                   getTimeString(nvl(creationTime, 0)) AS segCreationTime, getTimeString(nvl(lastModificationTime, 0)) AS segLastModTime
              FROM Cns_File_Metadata@RemoteNS NSFile, Cns_Seg_Metadata@RemoteNS NSSeg, Vmgr_Tape_Status_View@RemoteNS VmgrTape
             WHERE NSFile.fileid = NSSeg.s_fileid
               AND NSSeg.vid = VmgrTape.vid
               AND NSFile.fileid = inId) LOOP
    PIPE ROW(s);
  END LOOP;
END;
/

/* Get the recalljobs associated with the reference number */
CREATE OR REPLACE FUNCTION getRJs(ref number) RETURN castorDebug.RecallJobDebug PIPELINED AS
BEGIN
  FOR t IN (SELECT /*+ USE_NL(RecallJob RecallGroup SvcClass) INDEX(RecallJob I_RecallJob_CastorFile_VID) */
                   RecallJob.id, getObjStatusName('RecallJob', 'status', RecallJob.status) as status,
                   getTimeString(RecallJob.creationTime) as creationTime,
                   RecallJob.fseq, RecallJob.copyNb, RecallGroup.name as recallGroupName,
                   SvcClass.name as svcClassName, RecallJob.euid, RecallJob.egid, RecallJob.vid,
                   RecallJob.nbRetriesWithinMount, RecallJob.nbMounts
              FROM RecallJob, RecallGroup, SvcClass
             WHERE RecallJob.castorfile = getCF(ref)
               AND RecallJob.recallGroup = RecallGroup.id
               AND RecallJob.svcClass = SvcClass.id) LOOP
     PIPE ROW(t);
  END LOOP;
END;
/



/* Recompile all invalid procedures, triggers and functions */
/************************************************************/
BEGIN
  recompileAll();
END;
/

/* Flag the schema upgrade as COMPLETE */
/***************************************/
UPDATE UpgradeLog SET endDate = systimestamp, state = 'COMPLETE'
 WHERE release = '2_1_16_13';
COMMIT;
