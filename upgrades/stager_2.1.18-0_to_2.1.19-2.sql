/******************************************************************************
 *                 stager_2.1.18-0_to_2.1.19-2.sql
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This script upgrades a CASTOR v2.1.18-0 STAGER database to v2.1.19-2
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

/* Stop on errors */
WHENEVER SQLERROR EXIT FAILURE
BEGIN
  -- If we have encountered an error rollback any previously non committed
  -- operations. This prevents the UPDATE of the UpgradeLog from committing
  -- inconsistent data to the database.
  ROLLBACK;
  UPDATE UpgradeLog
     SET failureCount = failureCount + 1
   WHERE schemaVersion = '2_1_15_18'
     AND release = '2_1_19_2'
     AND state != 'COMPLETE';
  COMMIT;
END;
/

/* Verify that the script is running against the correct schema and version */
DECLARE
  unused VARCHAR(100);
BEGIN
  SELECT release INTO unused FROM CastorVersion
   WHERE schemaName = 'STAGER'
     AND release LIKE '2_1_18_%';
EXCEPTION WHEN NO_DATA_FOUND THEN
  -- Error, we cannot apply this script
  raise_application_error(-20000, 'PL/SQL release mismatch. Please run previous upgrade scripts for the STAGER before this one.');
END;
/

INSERT INTO UpgradeLog (schemaVersion, release, type)
VALUES ('2_1_15_18', '2_1_19_2', 'TRANSPARENT');
COMMIT;

CREATE OR REPLACE PROCEDURE checkNbReplicas AS
  varSvcClassId INTEGER;
  varCfId INTEGER;
  varReplicaNb NUMBER;
  varNbFiles NUMBER;
  varDidSth BOOLEAN;
BEGIN
  -- Loop over the CastorFiles to be processed
  LOOP
    varCfId := NULL;
    DELETE FROM TooManyReplicasHelper
     WHERE ROWNUM < 2
    RETURNING svcClass, castorFile INTO varSvcClassId, varCfId;
    IF varCfId IS NULL THEN
      -- we can exit, we went though all files to be processed
      EXIT;
    END IF;
    BEGIN
      -- Lock the castorfile
      SELECT id INTO varCfId FROM CastorFile
       WHERE id = varCfId FOR UPDATE;
    EXCEPTION WHEN NO_DATA_FOUND THEN
      -- the file was dropped meanwhile, ignore and continue
      CONTINUE;
    END;
    -- Get the max replica number of the service class
    SELECT replicaNb INTO varReplicaNb
      FROM SvcClass WHERE id = varSvcClassId;
    -- Produce a list of diskcopies to invalidate should too many replicas be online.
    varDidSth := False;
    FOR b IN (SELECT id FROM (
                SELECT rownum ind, id FROM (
                  SELECT * FROM (
                    SELECT /*+ INDEX_RS_ASC (DiskCopy I_DiskCopy_Castorfile) */
                           FileSystem.status AS FsStatus, DiskServer.status AS DsStatus,
                           DiskCopy.gcWeight, DiskCopy.id
                      FROM DiskCopy, FileSystem, DiskPool2SvcClass,
                           DiskServer
                     WHERE DiskCopy.filesystem = FileSystem.id
                       AND FileSystem.diskpool = DiskPool2SvcClass.parent
                       AND FileSystem.diskserver = DiskServer.id
                       AND DiskPool2SvcClass.child = varSvcClassId
                       AND DiskCopy.castorfile = varCfId
                       AND DiskCopy.status = dconst.DISKCOPY_VALID
                     UNION ALL
                    SELECT /*+ INDEX_RS_ASC (DiskCopy I_DiskCopy_Castorfile) */
                           (SELECT MIN(status) FROM DiskServer
                             WHERE dataPool = DiskCopy.dataPool) AS FsStatus,
                           (SELECT MIN(status) FROM DiskServer
                             WHERE dataPool = DiskCopy.dataPool) AS DsStatus,
                           DiskCopy.gcWeight, DiskCopy.id
                      FROM DiskCopy, DataPool2SvcClass
                     WHERE DiskCopy.dataPool = DataPool2SvcClass.parent
                       AND DataPool2SvcClass.child = varSvcClassId
                       AND DiskCopy.castorfile = varCfId
                       AND DiskCopy.status = dconst.DISKCOPY_VALID)
                   -- Select non-PRODUCTION hardware first
                   ORDER BY decode(fsStatus, 0, decode(dsStatus, 0, 0, 1), 1) ASC, gcWeight DESC))
               WHERE ind > varReplicaNb)
    LOOP
      -- Sanity check, make sure that the last copy is never dropped!
      SELECT /*+ INDEX_RS_ASC(DiskCopy I_DiskCopy_CastorFile) */ count(*) INTO varNbFiles
        FROM DiskCopy, FileSystem, DiskPool2SvcClass, SvcClass, DiskServer
       WHERE DiskCopy.filesystem = FileSystem.id
         AND FileSystem.diskpool = DiskPool2SvcClass.parent
         AND FileSystem.diskserver = DiskServer.id
         AND DiskPool2SvcClass.child = SvcClass.id
         AND DiskCopy.castorfile = varCfId
         AND DiskCopy.status = dconst.DISKCOPY_VALID
         AND SvcClass.id = varSvcClassId;
      IF varNbFiles = 1 THEN
        EXIT;  -- Last file, so exit the loop
      END IF;
      -- Invalidate the diskcopy
      UPDATE DiskCopy
         SET status = dconst.DISKCOPY_INVALID,
             gcType = dconst.GCTYPE_TOOMANYREPLICAS
       WHERE id = b.id;
      varDidSth := True;
      -- update importance of remaining diskcopies
      UPDATE DiskCopy SET importance = importance + 1
       WHERE castorFile = varCfId
         AND status = dconst.DISKCOPY_VALID;
    END LOOP;
    IF varDidSth THEN COMMIT; END IF;
  END LOOP;
  -- commit the deletions in case no modification was done that commited them before
  COMMIT;
END;
/

/* PL/SQL method implementing checkForD2DCopyOrRecall
 * dcId is the DiskCopy id of the best candidate for replica, 0 if none is found (tape recall), -1 in case of user error
 */
CREATE OR REPLACE PROCEDURE checkForD2DCopyOrRecall(cfId IN NUMBER, srId IN NUMBER, reuid IN NUMBER, regid IN NUMBER,
                                                    svcClassId IN NUMBER, dcId OUT NUMBER, srcSvcClassId OUT NUMBER) AS
  destSvcClass VARCHAR2(2048);
  userid NUMBER := reuid;
  groupid NUMBER := regid;
BEGIN
  -- First check whether we are a disk only pool that is already full.
  -- In such a case, we should fail the request with an ENOSPACE error
  IF (checkFailJobsWhenNoSpace(svcClassId) = 1) THEN
    dcId := -1;
    UPDATE /*+ INDEX(Subrequest PK_Subrequest_Id)*/ SubRequest
       SET status = dconst.SUBREQUEST_FAILED,
           errorCode = serrno.ENOSPC, -- No space left on device
           errorMessage = 'File creation canceled since pool is full'
     WHERE id = srId;
    RETURN;
  END IF;
  -- Resolve the destination service class id to a name
  SELECT name INTO destSvcClass FROM SvcClass WHERE id = svcClassId;
  -- Determine if there are any copies of the file in the same service class
  -- on non PRODUCTION hardware. If we found something then set the user
  -- and group id to -1 this effectively disables the later privilege checks
  -- to see if the user can trigger a d2d or recall. (#55745)
  BEGIN
    SELECT -1, -1 INTO userid, groupid
      FROM (
        SELECT /*+ INDEX_RS_ASC (DiskCopy I_DiskCopy_CastorFile) */ 1
          FROM DiskCopy, FileSystem, DiskServer, DiskPool2SvcClass
         WHERE DiskCopy.fileSystem = FileSystem.id
           AND DiskCopy.castorFile = cfId
           AND DiskCopy.status = dconst.DISKCOPY_VALID
           AND FileSystem.diskPool = DiskPool2SvcClass.parent
           AND DiskPool2SvcClass.child = svcClassId
           AND FileSystem.diskServer = DiskServer.id
           AND (DiskServer.status != dconst.DISKSERVER_PRODUCTION
            OR  FileSystem.status != dconst.FILESYSTEM_PRODUCTION)
        UNION ALL
        SELECT /*+ INDEX_RS_ASC (DiskCopy I_DiskCopy_Castorfile) */ 1
          FROM DiskCopy, DataPool2SvcClass
         WHERE DiskCopy.dataPool = DataPool2SvcClass.parent
           AND DataPool2SvcClass.child = svcClassId
           AND DiskCopy.castorfile = cfId
           AND DiskCopy.status = dconst.DISKCOPY_VALID
        )
       WHERE ROWNUM < 2;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    NULL;  -- Nothing
  END;
  -- If we are in this procedure then we did not find a copy of the
  -- file in the target service class that could be used. So, we check
  -- to see if the user has the rights to create a file in the destination
  -- service class. I.e. check for StagePutRequest access rights
  IF checkPermission(destSvcClass, userid, groupid, 40) != 0 THEN
    -- Fail the subrequest and notify the client
    dcId := -1;
    UPDATE /*+ INDEX(Subrequest PK_Subrequest_Id)*/ SubRequest
       SET status = dconst.SUBREQUEST_FAILED,
           errorCode = serrno.EACCES,
           errorMessage = 'Insufficient user privileges to trigger a tape recall or file replication to the '''||destSvcClass||''' service class'
     WHERE id = srId;
    RETURN;
  END IF;
  -- Try to find a diskcopy to replicate
  findDiskCopyToReplicate(cfId, userid, groupid, dcId, srcSvcClassId);
  -- We found at least one, therefore we schedule a disk2disk
  -- copy from the existing diskcopy not available to this svcclass
EXCEPTION WHEN NO_DATA_FOUND THEN
  -- We found no diskcopies at all. We should not schedule
  -- and make a tape recall... except ... in 4 cases :
  --   - if there is some temporarily unavailable diskcopy
  --     that is in STAGEOUT or is VALID but not on tape yet
  -- in such a case, what we have is an existing file, that
  -- was migrated, then overwritten but never migrated again.
  -- So the unavailable diskCopy is the only copy that is valid.
  -- We will tell the client that the file is unavailable
  -- and he/she will retry later
  --   - if we have an available STAGEOUT copy. This can happen
  -- when the copy is in a given svcclass and we were looking
  -- in another one. Since disk to disk copy is impossible in this
  -- case, the file is declared BUSY.
  --   - if we have an available WAITFS, WAITFSSCHEDULING copy in such
  -- a case, we tell the client that the file is BUSY
  --   - if we have some temporarily unavailable diskcopy(ies)
  --     that is in status VALID and the file is disk only.
  -- In this case nothing can be recalled and the file is inaccessible
  -- until we have one of the unvailable copies back
  DECLARE
    dcStatus NUMBER;
    fsStatus NUMBER;
    dsStatus NUMBER;
    varNbCopies NUMBER;
  BEGIN
    SELECT * INTO dcStatus, fsStatus, dsStatus FROM (
      SELECT DiskCopy.status, dconst.FILESYSTEM_PRODUCTION, dconst.DISKSERVER_PRODUCTION AS dsStatus
        FROM DiskCopy, CastorFile
       WHERE DiskCopy.castorfile = cfId
         AND Castorfile.id = cfId
         AND DiskCopy.status IN (dconst.DISKCOPY_WAITFS, dconst.DISKCOPY_WAITFS_SCHEDULING)
       UNION ALL
      SELECT DiskCopy.status, nvl(FileSystem.status, dconst.FILESYSTEM_PRODUCTION),
             DiskServer.status AS dsStatus
        FROM DiskCopy, FileSystem, DiskServer, CastorFile
       WHERE DiskCopy.castorfile = cfId
         AND Castorfile.id = cfId
         AND (DiskCopy.status = dconst.DISKCOPY_STAGEOUT
              OR (DiskCopy.status = dconst.DISKCOPY_VALID AND
                  CastorFile.tapeStatus = dconst.CASTORFILE_NOTONTAPE))
         AND FileSystem.id(+) = DiskCopy.fileSystem
         AND (DiskServer.id = FileSystem.diskserver OR DiskServer.dataPool = DiskCopy.dataPool)
       ORDER BY dsStatus ASC) -- PRODUCTION first (useful for datapool cases)
     WHERE ROWNUM < 2;
    -- We are in one of the 3 first special cases. Don't schedule, don't recall
    dcId := -1;
    UPDATE /*+ INDEX(Subrequest PK_Subrequest_Id)*/ SubRequest
       SET status = dconst.SUBREQUEST_FAILED,
           errorCode = CASE
             WHEN dcStatus IN (dconst.DISKCOPY_WAITFS, dconst.DISKCOPY_WAITFS_SCHEDULING) THEN serrno.EBUSY
             WHEN dcStatus = dconst.DISKCOPY_STAGEOUT
               AND fsStatus IN (dconst.FILESYSTEM_PRODUCTION, dconst.FILESYSTEM_READONLY)
               AND dsStatus IN (dconst.DISKSERVER_PRODUCTION, dconst.DISKSERVER_READONLY) THEN serrno.EBUSY
             ELSE serrno.ESTNOTAVAIL -- File is currently not available
           END,
           errorMessage = CASE
             WHEN dcStatus IN (dconst.DISKCOPY_WAITFS, dconst.DISKCOPY_WAITFS_SCHEDULING) THEN
               'File is being (re)created right now by another user'
             WHEN dcStatus = dconst.DISKCOPY_STAGEOUT
               AND fsStatus IN (dconst.FILESYSTEM_PRODUCTION, dconst.FILESYSTEM_READONLY)
               AND dsStatus IN (dconst.DISKSERVER_PRODUCTION, dconst.DISKSERVER_READONLY) THEN
               'File is being written to in another service class'
             ELSE
               'All copies of this file are unavailable for now. Please retry later'
           END
     WHERE id = srId;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    DECLARE
      varFileSize INTEGER;
    BEGIN
      -- we are not in one of the 3 first special cases. Let's check the 4th one
      -- by checking whether the file is diskonly
      -- Also we should allow "recalls" for 0 size files
      SELECT nbCopies, fileSize INTO varNbCopies, varFileSize
        FROM FileClass, CastorFile
       WHERE FileClass.id = CastorFile.fileClass
         AND CastorFile.id = cfId;
      IF varNbCopies = 0 AND varFileSize != 0 THEN
        -- we have indeed a disk only file, so fail the request
        dcId := -1;
        UPDATE /*+ INDEX(Subrequest PK_Subrequest_Id)*/ SubRequest
           SET status = dconst.SUBREQUEST_FAILED,
               errorCode = serrno.ESTNOTAVAIL, -- File is currently not available
               errorMessage = 'All disk copies of this disk-only file are unavailable for now. Please retry later'
         WHERE id = srId;
      ELSE
        -- We did not find the very special case so we should recall from tape.
        -- Check whether the user has the rights to issue a tape recall to
        -- the destination service class.
        IF varFileSize != 0 AND checkPermission(destSvcClass, userid, groupid, 161) != 0 THEN
          -- Fail the subrequest and notify the client
          dcId := -1;
          UPDATE /*+ INDEX(Subrequest PK_Subrequest_Id)*/ SubRequest
             SET status = dconst.SUBREQUEST_FAILED,
                 errorCode = serrno.EACCES, -- Permission denied
                 errorMessage = 'Insufficient user privileges to trigger a tape recall to the '''||destSvcClass||''' service class'
           WHERE id = srId;
        ELSE
          -- user has enough rights, green light for the recall
          dcId := 0;
        END IF;
      END IF;
    END;
  END;
END;
/

/* A little generic method to delete efficiently */
CREATE OR REPLACE PROCEDURE bulkDelete(sel IN VARCHAR2, tab IN VARCHAR2) AS
BEGIN
  EXECUTE IMMEDIATE
  'DECLARE
    CURSOR s IS '||sel||'
    ids "numList";
  BEGIN
    OPEN s;
    LOOP
      FETCH s BULK COLLECT INTO ids LIMIT 100000;
      EXIT WHEN ids.count = 0;
      FORALL i IN 1 .. ids.COUNT
        DELETE FROM '||tab||' WHERE id = ids(i);
      COMMIT;
    END LOOP;
    CLOSE s;
  END;';
END;
/

/* Deal with old D2D jobs */
CREATE OR REPLACE PROCEDURE deleteStaleDisk2DiskCopyJobs(timeOut IN NUMBER) AS
-- Select stale Disk2DiskCopyJob entries, that is either already scheduled/running
-- or still pending but originated by a user request (other types of replication may take very long)
  CURSOR s IS
    SELECT id FROM Disk2DiskCopyJob
     WHERE (replicationType = dconst.REPLICATIONTYPE_USER
         OR status IN (dconst.DISK2DISKCOPYJOB_SCHEDULED, dconst.DISK2DISKCOPYJOB_RUNNING))
       AND creationTime < getTime() - timeOut;
  ids "numList";
  varCfId INTEGER;
  varFileId INTEGER;
  varNsHost VARCHAR2(100);
BEGIN
  OPEN s;
  LOOP
    FETCH s BULK COLLECT INTO ids LIMIT 10000;
    EXIT WHEN ids.count = 0;
    FOR i IN 1..ids.COUNT LOOP
      SELECT castorFile INTO varCfId FROM Disk2DiskCopyJob WHERE id = ids(i);
      SELECT fileid, nsHost INTO varFileId, varNsHost FROM CastorFile
       WHERE id = varCfId
       FOR UPDATE;
      DELETE FROM Disk2DiskCopyJob WHERE id = ids(i);
      UPDATE SubRequest
         SET status = dconst.SUBREQUEST_RESTART
       WHERE status = dconst.SUBREQUEST_WAITSUBREQ
         AND castorFile = varCfId;
      COMMIT;
      logToDLF(NULL, dlf.LVL_WARNING, dlf.D2D_DROPPED_BY_CLEANING, varFileId, varNsHost, 'stagerd', '');
    END LOOP;
  END LOOP;
  CLOSE s;
END;
/


/* Recompile all invalid procedures, triggers and functions */
/************************************************************/
BEGIN
  recompileAll();
END;
/

/* Flag the schema upgrade as COMPLETE */
/***************************************/
UPDATE UpgradeLog SET endDate = systimestamp, state = 'COMPLETE'
 WHERE release = '2_1_19_2';
COMMIT;
