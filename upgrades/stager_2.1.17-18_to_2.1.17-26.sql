/******************************************************************************
 *                 stager_2.1.17-18_to_2.1.17-26.sql
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This script upgrades a CASTOR v2.1.17-18 STAGER database to v2.1.17-26
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

/* Stop on errors */
WHENEVER SQLERROR EXIT FAILURE
BEGIN
  -- If we have encountered an error rollback any previously non committed
  -- operations. This prevents the UPDATE of the UpgradeLog from committing
  -- inconsistent data to the database.
  ROLLBACK;
  UPDATE UpgradeLog
     SET failureCount = failureCount + 1
   WHERE schemaVersion = '2_1_15_18'
     AND release = '2_1_17_26'
     AND state != 'COMPLETE';
  COMMIT;
END;
/

/* Verify that the script is running against the correct schema and version */
DECLARE
  unused VARCHAR(100);
BEGIN
  SELECT release INTO unused FROM CastorVersion
   WHERE schemaName = 'STAGER'
     AND release LIKE '2_1_17_18%';
EXCEPTION WHEN NO_DATA_FOUND THEN
  -- Error, we cannot apply this script
  raise_application_error(-20000, 'PL/SQL release mismatch. Please run previous upgrade scripts for the STAGER before this one.');
END;
/

INSERT INTO UpgradeLog (schemaVersion, release, type)
VALUES ('2_1_15_18', '2_1_17_26', 'TRANSPARENT');
COMMIT;

/* Job management */
BEGIN
  FOR a IN (SELECT * FROM user_scheduler_jobs)
  LOOP
    -- Stop any running jobs
    IF a.state = 'RUNNING' THEN
      dbms_scheduler.stop_job(a.job_name, force=>TRUE);
    END IF;
    -- Schedule the start date of the job to 15 minutes from now. This
    -- basically pauses the job for 15 minutes so that the upgrade can
    -- go through as quickly as possible.
    dbms_scheduler.set_attribute(a.job_name, 'START_DATE', SYSDATE + 15/1440);
  END LOOP;
END;
/


/* A view to spot late or stuck migrations */
/*******************************************/
/* It returns all files that are not yet on tape, that are existing in the namespace
 * and for which migration is pending for more than 24h.
 */
CREATE OR REPLACE VIEW LateMigrationsView AS
  SELECT /*+ LEADING(CF MJ CnsFile DC FileSystem DiskServer) USE_NL(CF MJ CnsFile DC FileSystem DiskServer) */
         CF.fileId, CF.lastKnownFileName AS filePath, CF.fileSize,
         decode(MJ.creationTime, NULL, -1, getTime() - MJ.creationTime) AS mjElapsedTime, nvl(MJ.status, -1) AS mjStatus,
         DC.creationTime AS dcCreationTime, DiskServer.name || ':' || FileSystem.mountPoint || DC.path AS location,
         decode(DiskServer.hwOnline, 0, 'N',
           decode(DiskServer.status, 2, 'N',
             decode(FileSystem.status, 2, 'N', 'Y'))) AS available
    FROM CastorFile CF, DiskCopy DC, MigrationJob MJ, cns_file_metadata@remotens CnsFile,
         FileSystem, DiskServer
   WHERE CF.fileId = CnsFile.fileId
     AND DC.castorFile = CF.id
     AND MJ.castorFile(+) = CF.id
     AND DC.fileSystem = FileSystem.id
     AND FileSystem.diskServer = DiskServer.id
     AND CF.tapeStatus = 0  -- CASTORFILE_NOTONTAPE
     AND DC.status = 0  -- DISKCOPY_VALID
     AND CF.fileSize > 0
     AND DC.creationTime < getTime() - 86400
  UNION
  SELECT /*+ LEADING(CF MJ CnsFile DC FileSystem DiskServer) USE_NL(CF MJ CnsFile DC FileSystem DiskServer) */
         CF.fileId, CF.lastKnownFileName AS filePath, CF.fileSize,
         decode(MJ.creationTime, NULL, -1, getTime() - MJ.creationTime) AS mjElapsedTime, nvl(MJ.status, -1) AS mjStatus,
         DC.creationTime AS dcCreationTime, DataPool.externalUser || '@' || DataPool.externalPool || ':' || DC.path AS location, 'Y' as available
    FROM CastorFile CF, DiskCopy DC, MigrationJob MJ, DataPool, cns_file_metadata@remotens CnsFile
   WHERE CF.fileId = CnsFile.fileId
     AND DC.castorFile = CF.id
     AND MJ.castorFile(+) = CF.id
     AND DC.dataPool = DataPool.id
     AND CF.tapeStatus = 0  -- CASTORFILE_NOTONTAPE
     AND DC.status = 0  -- DISKCOPY_VALID
     AND CF.fileSize > 0
     AND DC.creationTime < getTime() - 86400;


/* PL/SQL method implementing selectCastorFile */
CREATE OR REPLACE PROCEDURE selectCastorFileInternal (inFileId IN INTEGER,
                                                      inNsHost IN VARCHAR2,
                                                      inClassId IN INTEGER,
                                                      inFileSize IN INTEGER,
                                                      inFileName IN VARCHAR2,
                                                      inSrId IN NUMBER,
                                                      inNsOpenTime IN NUMBER,
                                                      inWaitForLock IN BOOLEAN,
                                                      outId OUT INTEGER,
                                                      outFileSize OUT INTEGER) AS
  varPreviousLastKnownFileName VARCHAR2(2048);
  varNsOpenTime NUMBER;
  varFcId NUMBER;
BEGIN
  BEGIN
    -- try to find an existing file
    SELECT id, fileSize, lastKnownFileName, nsOpenTime
      INTO outId, outFileSize, varPreviousLastKnownFileName, varNsOpenTime
      FROM CastorFile
     WHERE fileId = inFileId AND nsHost = inNsHost;
    -- take a lock on the file. Note that the file may have disappeared in the
    -- meantime, this is why we first select (potentially having a NO_DATA_FOUND
    -- exception) before we update.
    IF inWaitForLock THEN
      SELECT id INTO outId FROM CastorFile WHERE id = outId FOR UPDATE;
    ELSE
      SELECT id INTO outId FROM CastorFile WHERE id = outId FOR UPDATE NOWAIT;
    END IF;
    -- In case its filename has changed, fix it
    IF inFileName != varPreviousLastKnownFileName THEN
      fixLastKnownFileName(inFileName, outId);
    END IF;
    -- The file is still there, so update timestamps
    UPDATE CastorFile SET lastAccessTime = getTime(), fileSize = inFileSize WHERE id = outId;
    outFileSize := inFileSize;
    IF varNsOpenTime = 0 AND inNsOpenTime > 0 THEN
      -- We have a CastorFile entry, but it had not been created for an open operation
      -- (effectively, only a putDone operation on a non-existing file can do this).
      -- On the contrary, now we have been called after an open() as inNsOpenTime > 0.
      -- Therefore, we set the nsOpenTime and lastUpdateTime like in createCastorFile()
      UPDATE CastorFile SET nsOpenTime = inNsOpenTime, lastUpdateTime = TRUNC(inNsOpenTime)
       WHERE id = outId;
    END IF;
    UPDATE /*+ INDEX(Subrequest PK_Subrequest_Id)*/ SubRequest SET castorFile = outId
     WHERE id = inSrId;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    -- we did not find the file, let's try to create a new one.
    -- First resolve the fileclass
    BEGIN
      SELECT id INTO varFcId FROM FileClass WHERE classId = inClassId;
    EXCEPTION WHEN NO_DATA_FOUND THEN
      RAISE_APPLICATION_ERROR (-20010, 'File class '|| inClassId ||' not found in database');
    END;
    -- This may fail with a low probability (subtle race condition, see comments
    -- in the method), so we try in a loop
    DECLARE
      varSuccess BOOLEAN := False;
    BEGIN
      WHILE NOT varSuccess LOOP
        varSuccess := createCastorFile(inFileId, inNsHost, varFcId, inFileSize, inFileName,
                                       inSrId, inNsOpenTime, inWaitForLock, outId, outFileSize);
      END LOOP;
    END;
  END;
END;
/

/* PL/SQL method implementing handlePrepareToGet
 * returns whether the client should be answered
 */
CREATE OR REPLACE FUNCTION handlePrepareToGet(inCfId IN INTEGER, inSrId IN INTEGER,
                                              inFileId IN INTEGER, inNsHost IN VARCHAR2,
                                              inFileSize IN INTEGER, inNsOpenTimeInUsec IN INTEGER)
RETURN INTEGER AS
  varNsOpenTime INTEGER;
  varEuid NUMBER;
  varEgid NUMBER;
  varSvcClassId NUMBER;
  varReqUUID VARCHAR(2048);
  varReqId INTEGER;
  varSrUUID VARCHAR(2048);
  varIsAnswered INTEGER;
BEGIN
  -- lock the castorFile to be safe in case of concurrent subrequests
  SELECT nsOpenTime INTO varNsOpenTime FROM CastorFile WHERE id = inCfId FOR UPDATE;
  -- retrieve the svcClass, user and log data for this subrequest
  SELECT /*+ INDEX(Subrequest PK_Subrequest_Id)*/
         Request.euid, Request.egid, Request.svcClass,
         Request.reqId, Request.id, SubRequest.subreqId, SubRequest.answered
    INTO varEuid, varEgid, varSvcClassId, varReqUUID, varReqId, varSrUUID, varIsAnswered
    FROM (SELECT /*+ INDEX(StageGetRequest PK_StageGetRequest_Id) */
                 id, euid, egid, svcClass, reqId from StagePrepareToGetRequest) Request,
         SubRequest
   WHERE Subrequest.request = Request.id
     AND Subrequest.id = inSrId;
  -- log
  logToDLF(varReqUUID, dlf.LVL_DEBUG, dlf.STAGER_PREPARETOGET, inFileId, inNsHost, 'stagerd',
           'SUBREQID=' || varSrUUID);

  -- We should actually check whether our disk cache is stale,
  -- that is IF CF.nsOpenTime < inNsOpenTime THEN invalidate our diskcopies.
  -- This is pending the full deployment of the 'new open mode' as implemented
  -- in the fix of bug #95189: Time discrepencies between
  -- disk servers and name servers can lead to silent data loss on input.
  -- The problem being that in 'Compatibility' mode inNsOpenTime is the
  -- namespace's mtime, which can be modified by nstouch,
  -- hence nstouch followed by a Get would destroy the data on disk!

  -- First look for available diskcopies. Note that we never wait on other requests.
  -- and we include Disk2DiskCopyJobs as they are going to produce available DiskCopies.
  DECLARE
    varDcIds castor."cnumList";
  BEGIN
    SELECT * BULK COLLECT INTO varDcIds FROM (
      SELECT /*+ INDEX_RS_ASC (DiskCopy I_DiskCopy_CastorFile) */ DiskCopy.id
        FROM DiskCopy, FileSystem, DiskServer, DiskPool2SvcClass
       WHERE DiskCopy.castorfile = inCfId
         AND DiskCopy.fileSystem = FileSystem.id
         AND FileSystem.diskpool = DiskPool2SvcClass.parent
         AND DiskPool2SvcClass.child = varSvcClassId
         AND FileSystem.status IN (dconst.FILESYSTEM_PRODUCTION, dconst.FILESYSTEM_READONLY)
         AND FileSystem.diskserver = DiskServer.id
         AND DiskServer.status IN (dconst.DISKSERVER_PRODUCTION, dconst.DISKSERVER_READONLY)
         AND DiskServer.hwOnline = 1
         AND DiskCopy.status IN (dconst.DISKCOPY_VALID, dconst.DISKCOPY_STAGEOUT)
       UNION ALL
      SELECT DiskCopy.id
        FROM DiskCopy, DataPool2SvcClass
       WHERE DiskCopy.castorfile = inCfId
         AND DiskCopy.dataPool = DataPool2SvcClass.parent
         AND DataPool2SvcClass.child = varSvcClassId
         AND EXISTS (SELECT 1 FROM DiskServer
                     WHERE DiskServer.dataPool = DiskCopy.dataPool
                       AND DiskServer.status IN (dconst.DISKSERVER_PRODUCTION,
                                                 dconst.DISKSERVER_READONLY))
         AND DiskCopy.status IN (dconst.DISKCOPY_VALID, dconst.DISKCOPY_STAGEOUT)
       UNION ALL
      SELECT id
        FROM Disk2DiskCopyJob
       WHERE destSvcclass = varSvcClassId
         AND castorfile = inCfId);
    IF varDcIds.COUNT > 0 THEN
      -- some available diskcopy was found.
      logToDLF(varReqUUID, dlf.LVL_DEBUG, dlf.STAGER_DISKCOPY_FOUND, inFileId, inNsHost, 'stagerd',
              'SUBREQID=' || varSrUUID);
      -- update and archive SubRequest
      UPDATE SubRequest
         SET getNextStatus = dconst.GETNEXTSTATUS_FILESTAGED
       WHERE id = inSrId;
      archiveSubReq(inSrId, dconst.SUBREQUEST_FINISHED);
      -- update gcWeight of the existing diskcopies
      DECLARE
        gcwProc VARCHAR2(2048);
        gcw NUMBER;
      BEGIN
        gcwProc := castorGC.getPrepareHook(varSvcClassId);
        IF gcwProc IS NOT NULL THEN
          EXECUTE IMMEDIATE 'BEGIN :newGcw := ' || gcwProc || '(); END;' USING OUT gcw;
          FORALL i IN 1..vardcIds.COUNT
            UPDATE DiskCopy SET gcWeight = gcw WHERE id = varDcIds(i);
        END IF;
      END;
      -- all went fine, answer to client if needed
      IF varIsAnswered > 0 THEN
         RETURN 0;
      END IF;
    ELSE
      DECLARE
        varRc INTEGER;
      BEGIN
        varRc := triggerD2dOrRecall(inCfId, varNsOpenTime, inSrId, inFileId, inNsHost, varEuid, varEgid,
                                    varSvcClassId, inFileSize, varReqUUID, varSrUUID, false);
        IF varRc = 1 THEN
          -- recall started, we are done, update answer to client
          UPDATE SubRequest SET answered=1 WHERE id = inSrId;
        ELSE IF varRc = 0 THEN
          -- could not start recall, SubRequest has been marked as FAILED, no need to answer
          RETURN 0;
        -- if 2 : D2D copy was created
        -- if 3 : empty file created
        -- in both case, nothing is to be done
        END IF; END IF;
      END;
    END IF;
    -- answer client
    RETURN 1;
  END;
END;
/


/* Recompile all invalid procedures, triggers and functions */
/************************************************************/
BEGIN
  recompileAll();
END;
/

/* Flag the schema upgrade as COMPLETE */
/***************************************/
UPDATE UpgradeLog SET endDate = systimestamp, state = 'COMPLETE'
 WHERE release = '2_1_17_26';
COMMIT;
