/******************************************************************************
 *                 srm_2.1.16-10_to_2.1.17-18.sql
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This script upgrades a CASTOR v2.1.16-10+ SRM database to v2.1.17-18
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

/* Stop on errors */
WHENEVER SQLERROR EXIT FAILURE
BEGIN
  -- If we have encountered an error rollback any previously non committed
  -- operations. This prevents the UPDATE of the UpgradeLog from committing
  -- inconsistent data to the database.
  ROLLBACK;
  UPDATE UpgradeLog
     SET failureCount = failureCount + 1
   WHERE schemaVersion = '2_14_0'
     AND release = '2_1_17_18'
     AND state != 'COMPLETE';
  COMMIT;
END;
/

/* Verify that the script is running against the correct schema and version */
DECLARE
  unused VARCHAR(100);
BEGIN
  SELECT release INTO unused FROM CastorVersion
   WHERE schemaName = 'SRM'
     AND release LIKE '2_1_16_1%';
EXCEPTION WHEN NO_DATA_FOUND THEN
  -- Error, we cannot apply this script
  raise_application_error(-20000, 'PL/SQL release mismatch. Please run previous upgrade scripts for the SRM before this one.');
END;
/

INSERT INTO UpgradeLog (schemaVersion, release, type)
VALUES ('2_14_0', '2_1_17_18', 'TRANSPARENT');
COMMIT;


/* PL/SQL method to create a UserFile entry if missing */
CREATE OR REPLACE PROCEDURE createUserFile(fileName IN VARCHAR2,
                                           nsFileId IN INTEGER,
                                           nsHostName IN VARCHAR2,
                                           nsFileSize IN INTEGER,
                                           srId IN INTEGER,
                                           userFileId OUT INTEGER) AS
  CONSTRAINT_VIOLATED EXCEPTION;
  PRAGMA EXCEPTION_INIT(CONSTRAINT_VIOLATED, -1);
  previousFN VARCHAR2(2048);
BEGIN
  BEGIN
    -- Try to select an existing entry and lock it
    IF nsFileId > 0 THEN
      SELECT id, castorFileName INTO userFileId, previousFN FROM UserFile
       WHERE fileId = nsFileId FOR UPDATE;
      IF fileName != previousFN AND fileName != '' THEN
        dropReusedFile(fileName);
      END IF;
    ELSE
      SELECT id INTO userFileId FROM UserFile
       WHERE castorFileName = normalizePath(fileName) FOR UPDATE;
    END IF;
    -- we found one, update data; on prepareToPut, fileId is 0
    -- and we insert NULL waiting for the backend daemon to fill the value
    UPDATE UserFile
       SET fileId = decode(nsFileId, 0, NULL, nsFileId), fileSize = nsFileSize,
           nsHost = nsHostName, castorFileName = normalizePath(fileName)
     WHERE id = userFileId;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    -- we didn't find the file, create a new one and drop other old ones
    -- if any, that have the same filename and different fileid due to renaming
    dropReusedFile(fileName);
    INSERT INTO UserFile (id, fileId, nsHost, castorFileName, fileSize)
      VALUES (ids_seq.nextval, decode(nsFileId, 0, NULL, nsFileId),
        nsHostName, normalizePath(fileName), nsFileSize)
      RETURNING id INTO userFileId;
  END;
  UPDATE SubRequest
     SET userFile = userFileId,
         castorFileName = normalizePath(fileName)
   WHERE id = srId;
EXCEPTION WHEN CONSTRAINT_VIOLATED THEN
  -- retry the selection since a creation was done in between
  SELECT id INTO userFileId FROM UserFile
   WHERE castorFileName = normalizePath(fileName) FOR UPDATE;
  UPDATE UserFile
     SET fileId = decode(nsFileId, 0, NULL, nsFileId),
         fileSize = nsFileSize, nsHost = nsHostName
   WHERE id = userFileId;
  UPDATE SubRequest
     SET userFile = userFileId,
         castorFileName = normalizePath(fileName)
   WHERE id = srId;
END;
/


/* Recompile all invalid procedures, triggers and functions */
/************************************************************/
BEGIN
  recompileAll();
END;
/

/* Flag the schema upgrade as COMPLETE */
/***************************************/
UPDATE UpgradeLog SET endDate = systimestamp, state = 'COMPLETE'
 WHERE release = '2_1_17_18';
COMMIT;

