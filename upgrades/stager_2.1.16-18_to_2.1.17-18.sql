/******************************************************************************
 *                 stager_2.1.16-18_to_2.1.17-18.sql
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This script upgrades a CASTOR v2.1.16-18+ STAGER database to v2.1.17-18
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

/* Stop on errors */
WHENEVER SQLERROR EXIT FAILURE
BEGIN
  -- If we have encountered an error rollback any previously non committed
  -- operations. This prevents the UPDATE of the UpgradeLog from committing
  -- inconsistent data to the database.
  ROLLBACK;
  UPDATE UpgradeLog
     SET failureCount = failureCount + 1
   WHERE schemaVersion = '2_1_15_18'
     AND release = '2_1_17_18'
     AND state != 'COMPLETE';
  COMMIT;
END;
/

/* Verify that the script is running against the correct schema and version */
DECLARE
  unused VARCHAR(100);
BEGIN
  SELECT release INTO unused FROM CastorVersion
   WHERE schemaName = 'STAGER'
     AND (release = '2_1_16_18' OR release = '2_1_16_19' OR release = '2_1_16_21');
EXCEPTION WHEN NO_DATA_FOUND THEN
  -- Error, we cannot apply this script
  raise_application_error(-20000, 'PL/SQL release mismatch. Please run previous upgrade scripts for the STAGER before this one.');
END;
/

INSERT INTO UpgradeLog (schemaVersion, release, type)
VALUES ('2_1_15_18', '2_1_17_18', 'TRANSPARENT');
COMMIT;

/* A view to spot late or stuck migrations */
/*******************************************/
/* It returns all files that are not yet on tape, that are existing in the namespace
 * and for which migration is pending for more than 24h.
 */
CREATE OR REPLACE VIEW LateMigrationsView AS
  SELECT /*+ LEADING(CF MJ CnsFile DC FileSystem DiskServer) USE_NL(CF MJ CnsFile DC FileSystem DiskServer) */
         CF.fileId, CF.lastKnownFileName AS filePath, CF.fileSize,
         decode(MJ.creationTime, NULL, -1, getTime() - MJ.creationTime) AS mjElapsedTime, nvl(MJ.status, -1) AS mjStatus,
         DC.creationTime AS dcCreationTime, DiskServer.name || ':' || FileSystem.mountPoint || DC.path AS location,
         decode(DiskServer.hwOnline, 0, 'N',
           decode(DiskServer.status, 2, 'N',
             decode(FileSystem.status, 2, 'N', 'Y'))) AS available
    FROM CastorFile CF, DiskCopy DC, MigrationJob MJ, cns_file_metadata@remotens CnsFile,
         FileSystem, DiskServer
   WHERE CF.fileId = CnsFile.fileId
     AND DC.castorFile = CF.id
     AND MJ.castorFile(+) = CF.id
     AND DC.fileSystem = FileSystem.id
     AND FileSystem.diskServer = DiskServer.id
     AND CF.tapeStatus = 0  -- CASTORFILE_NOTONTAPE
     AND DC.status = 0  -- DISKCOPY_VALID
     AND CF.fileSize > 0
     AND DC.creationTime < getTime() - 86400
  UNION
  SELECT /*+ LEADING(CF MJ CnsFile DC FileSystem DiskServer) USE_NL(CF MJ CnsFile DC FileSystem DiskServer) */
         CF.fileId, CF.lastKnownFileName AS filePath, CF.fileSize,
         decode(MJ.creationTime, NULL, -1, getTime() - MJ.creationTime) AS mjElapsedTime, nvl(MJ.status, -1) AS mjStatus,
         DC.creationTime AS dcCreationTime, DataPool.externalUser || '@' || DataPool.externalPool || ':' || DC.path AS location, 'Y' as available
    FROM CastorFile CF, DiskCopy DC, MigrationJob MJ, DataPool, cns_file_metadata@remotens CnsFile
   WHERE CF.fileId = CnsFile.fileId
     AND DC.castorFile = CF.id
     AND MJ.castorFile(+) = CF.id
     AND DC.dataPool = DataPool.id
     AND CF.tapeStatus = 0  -- CASTORFILE_NOTONTAPE
     AND DC.status = 0  -- DISKCOPY_VALID
     AND CF.fileSize > 0
     AND DC.creationTime < getTime() - 86400
  ORDER BY dcCreationTime DESC;


/* Search and delete old archived/failed subrequests and their requests */
CREATE OR REPLACE PROCEDURE deleteTerminatedRequests AS
  failuresTimeOut INTEGER;
  successesTimeOut INTEGER;
  rate INTEGER;
  srIds "numList";
  ct NUMBER;
BEGIN
  -- select requested timeout for failed requests from configuration table
  failuresTimeOut := 3600*TO_NUMBER(getConfigOption('cleaning', 'failedRequestsTimeout', '168'));  -- 1 week
  -- compute a rate-dependent timeout for the successful requests by looking at the
  -- last half-hour of activity: keep max 1M of them.
  SELECT 1800 * 1000000 / (count(*)+1) INTO successesTimeOut
    FROM SubRequest
   WHERE status = dconst.SUBREQUEST_ARCHIVED
     AND lastModificationTime > getTime() - 1800;
  IF successesTimeOut > failuresTimeOut THEN
    -- in case of light load, don't keep successful request for longer than failed ones
    successesTimeOut := failuresTimeOut;
  END IF;

  -- Delete castorFiles if nothing is left for them. Here we use
  -- a temporary table as we need to commit every ~1000 operations
  -- and keeping a cursor opened on the original select may take
  -- too long, leading to ORA-01555 'snapshot too old' errors.
  EXECUTE IMMEDIATE 'TRUNCATE TABLE DeleteTermReqHelper';
  INSERT /*+ APPEND */ INTO DeleteTermReqHelper (srId, cfId)
    (SELECT SR.id, castorFile FROM SubRequest SR
      WHERE (SR.status = dconst.SUBREQUEST_ARCHIVED
             AND SR.lastModificationTime < getTime() - successesTimeOut)
         -- failed subrequests are kept according to the configured timeout
         OR (SR.status = dconst.SUBREQUEST_FAILED_FINISHED
             AND reqType != 119 AND SR.lastModificationTime < getTime() - failuresTimeOut));  -- StageRepackRequest
  COMMIT;  -- needed otherwise the next statement raises
           -- ORA-12838: cannot read/modify an object after modifying it in parallel
  -- 2nd part, separated from above for efficiency reasons
  INSERT /*+ APPEND */ INTO DeleteTermReqHelper (srId, cfId)
    (SELECT SR.id, castorFile FROM SubRequest SR, StageRepackRequest R
      WHERE SR.status = dconst.SUBREQUEST_FAILED_FINISHED
         -- only for the Repack case, we keep all failed subrequests around until
         -- the whole Repack request is over for more than <timeOut> seconds
        AND reqType = 119 AND R.lastModificationTime < getTime() - failuresTimeOut  -- StageRepackRequest
        AND R.id = SR.request);
  COMMIT;
  SELECT count(*) INTO ct FROM DeleteTermReqHelper;
  logToDLF(NULL, dlf.LVL_SYSTEM, dlf.DELETING_REQUESTS, 0, '', 'stagerd',
    'SubRequestsCount=' || ct);
  ct := 0;
  FOR cf IN (SELECT UNIQUE cfId FROM DeleteTermReqHelper) LOOP
    deleteCastorFile(cf.cfId);
    ct := ct + 1;
    IF ct = 1000 THEN
      COMMIT;
      ct := 0;
    END IF;
  END LOOP;

  -- Now delete all old subRequests. We reuse here the
  -- temporary table, which serves as a snapshot of the
  -- entries to be deleted, and we use the FORALL logic
  -- (cf. bulkDelete) instead of a simple DELETE ...
  -- WHERE id IN (SELECT srId FROM DeleteTermReqHelper)
  -- for efficiency reasons. Moreover, we don't risk
  -- here the ORA-01555 error keeping the cursor open
  -- between commits as we are selecting on our
  -- temporary table.
  DECLARE
    CURSOR s IS
      SELECT srId FROM DeleteTermReqHelper;
    ids "numList";
  BEGIN
    OPEN s;
    LOOP
      FETCH s BULK COLLECT INTO ids LIMIT 10000;
      EXIT WHEN ids.count = 0;
      FORALL i IN 1 .. ids.COUNT
        DELETE FROM SubRequest WHERE id = ids(i);
      COMMIT;
    END LOOP;
    CLOSE s;
  END;
  EXECUTE IMMEDIATE 'TRUNCATE TABLE DeleteTermReqHelper';

  -- And then related Requests, now orphaned.
    ---- Get ----
  bulkDeleteRequests('StageGetRequest');
    ---- Put ----
  bulkDeleteRequests('StagePutRequest');
    ---- PrepareToGet -----
  bulkDeleteRequests('StagePrepareToGetRequest');
    ---- PrepareToPut ----
  bulkDeleteRequests('StagePrepareToPutRequest');
    ---- PutDone ----
  bulkDeleteRequests('StagePutDoneRequest');
    ---- Rm ----
  bulkDeleteRequests('StageRmRequest');
    ---- SetGCWeight ----
  bulkDeleteRequests('SetFileGCWeight');

  -- Finally deal with Repack: this case is different because StageRepackRequests may be empty
  -- at the beginning. Therefore we only drop repacks that are in a completed state
  -- for more than the requested time.
  -- First failed ones (status FAILED, ABORTED)
  bulkDelete('SELECT id FROM StageRepackRequest R WHERE status IN (3, 5)
    AND NOT EXISTS (SELECT 1 FROM SubRequest WHERE request = R.id AND status NOT IN (9, 11))
    AND lastModificationTime < getTime() - ' || failuresTimeOut || ';',
    'StageRepackRequest');
  -- Then successful ones (status FINISHED)
  bulkDelete('SELECT id FROM StageRepackRequest R WHERE status = 2
    AND NOT EXISTS (SELECT 1 FROM SubRequest WHERE request = R.id AND status NOT IN (9, 11))
    AND lastModificationTime < getTime() - ' || successesTimeOut || ';',
    'StageRepackRequest');
END;
/

/* PL/SQL method to either force GC of the given diskCopies or delete them when the physical files behind have been lost */
CREATE OR REPLACE PROCEDURE internalDeleteDiskCopies(inForce IN INTEGER,
                                                     inDryRun IN INTEGER,
                                                     outRes OUT castor.DiskCopyResult_Cur) AS
  varNsHost VARCHAR2(100);
  varFileName VARCHAR2(2048);
  varCfId INTEGER;
  varNbRemaining INTEGER;
  varStatus INTEGER;
  varLogParams VARCHAR2(2048);
  varFileSize INTEGER;
  varNsOpenTime NUMBER;
  varSvcClassId INTEGER;
  varEuid INTEGER;
  varEgid INTEGER;
BEGIN
  -- gather all remote Nameserver statuses. This could not be
  -- incorporated in the INSERT query, because Oracle would give:
  -- PLS-00739: FORALL INSERT/UPDATE/DELETE not supported on remote tables.
  -- Note that files that are not found in the Nameserver remain with fStatus = 'd',
  -- which means they can be safely deleted: we're anticipating the NS synch.
  UPDATE DeleteDiskCopyHelper
     SET fStatus = '-'
   WHERE EXISTS (SELECT 1 FROM Cns_file_metadata@RemoteNS F
                  WHERE status = '-' AND F.fileId IN
                    (SELECT fileId FROM DeleteDiskCopyHelper));
  UPDATE DeleteDiskCopyHelper
     SET fStatus = 'm'
   WHERE EXISTS (SELECT 1 FROM Cns_file_metadata@RemoteNS F
                  WHERE status = 'm' AND F.fileId IN
                    (SELECT fileId FROM DeleteDiskCopyHelper));
  -- A better and more generic implementation would have been:
  -- UPDATE DeleteDiskCopyHelper H
  --    SET fStatus = nvl((SELECT F.status
  --                         FROM Cns_file_metadata@RemoteNS F
  --                        WHERE F.fileId = H.fileId), 'd');
  -- Unfortunately, that one is much less efficient as Oracle does not use
  -- the DB link in bulk, therefore making the query extremely slow (several mins)
  -- when handling large numbers of files (e.g. an entire mount point).
  COMMIT;
  FOR dc IN (SELECT dcId, fileId, fStatus FROM DeleteDiskCopyHelper) LOOP
    DECLARE
      varDCFileSystem INTEGER;
      varDCPool INTEGER;
    BEGIN
      -- get data and lock
      SELECT castorFile, status, diskCopySize, owneruid, ownergid, fileSystem, dataPool
        INTO varCfId, varStatus, varFileSize, varEuid, varEgid, varDCFileSystem, varDCPool
        FROM DiskCopy
       WHERE DiskCopy.id = dc.dcId;
      SELECT nsHost, lastKnownFileName, lastUpdateTime INTO varNsHost, varFileName, varNsOpenTime
        FROM CastorFile
       WHERE id = varCfId
         FOR UPDATE;
      -- get a service class where to put the new copy. Note that we have to choose
      -- potentially among several and we take randomly the first one. This may cause
      -- the creation of a new copy of the file in a different place from the lost
      -- copy, maybe also visible from different service classes in tricky cases.
      -- However, the essential will be preserved : a second copy will be rebuilt
      SELECT child INTO varSvcClassId
        FROM (SELECT DiskPool2SvcClass.child
                FROM FileSystem, DiskPool2SvcClass
               WHERE FileSystem.id = varDCFileSystem
                 AND DiskPool2SvcClass.parent = FileSystem.diskPool
               UNION ALL
               SELECT DataPool2SvcClass.child
                 FROM DataPool2SvcClass
                WHERE DataPool2SvcClass.parent = varDCPool)
       WHERE ROWNUM < 2;
      varLogParams := 'FileName="'|| varFileName ||'"" fileSize='|| varFileSize
        ||' dcId='|| dc.dcId ||' svcClass=' || varSvcClassId || ', status='
        || getObjStatusName('DiskCopy', 'status', varStatus);
    EXCEPTION WHEN NO_DATA_FOUND THEN
      -- diskcopy not found in stager
      UPDATE DeleteDiskCopyHelper
         SET rc = dconst.DELDC_NOOP,
             msg = 'not found in stager, skipping'
       WHERE dcId = dc.dcId;
      COMMIT;
      CONTINUE;
    END;
    -- count remaining ones
    SELECT count(*) INTO varNbRemaining FROM DiskCopy
     WHERE castorFile = varCfId
       AND status = dconst.DISKCOPY_VALID
       AND id != dc.dcId;
    -- and update their importance if needed (other copy exists and dropped one was valid)
    IF varNbRemaining > 0 AND varStatus = dconst.DISKCOPY_VALID AND inDryRun = 0 THEN
      UPDATE DiskCopy SET importance = importance + 1
       WHERE castorFile = varCfId
         AND status = dconst.DISKCOPY_VALID;
    END IF;
    IF inForce != 0 THEN
      -- the physical diskcopy is deemed lost: delete the diskcopy entry
      -- and potentially drop dangling entities
      IF inDryRun = 0 THEN
        DELETE FROM DiskCopy WHERE id = dc.dcId;
        IF varStatus = dconst.DISKCOPY_STAGEOUT THEN
          -- fail outstanding requests
          UPDATE SubRequest
             SET status = dconst.SUBREQUEST_FAILED,
                 errorCode = serrno.SEINTERNAL,
                 errorMessage = 'File got lost while being written to'
           WHERE diskCopy = dc.dcId
             AND status = dconst.SUBREQUEST_READY;
        END IF;
      END IF;
      -- was it the last active one?
      IF varNbRemaining = 0 THEN
        IF inDryRun = 0 THEN
          -- yes, drop the (now bound to fail) migration job(s)
          deleteMigrationJobs(varCfId);
          -- check if the entire castorFile chain can be dropped
          deleteCastorFile(varCfId);
          -- log
          logToDLF(NULL,
                   CASE dc.fStatus WHEN 'm' THEN dlf.LVL_SYSTEM
                                   WHEN 'd' THEN dlf.LVL_SYSTEM
                                   ELSE dlf.LVL_WARNING END,
                   CASE dc.fStatus WHEN 'm' THEN dlf.DELETEDISKCOPY_RECALL
                                   WHEN 'd' THEN dlf.DELETEDISKCOPY_GC
                                   ELSE dlf.DELETEDISKCOPY_LOST END,
                   dc.fileId, varNsHost, 'stagerd', varLogParams);
        END IF;
        UPDATE DeleteDiskCopyHelper
           SET rc = CASE dc.fStatus WHEN 'm' THEN dconst.DELDC_NOOP
                                    WHEN 'd' THEN dconst.DELDC_NOOP
                                    ELSE dconst.DELDC_LOST END,
               msg = CASE dc.fStatus WHEN 'm' THEN 'dropped from disk pool'
                                     WHEN 'd' THEN 'NOT garbage collected from stager'
                                     ELSE 'dropped LAST COPY from stager, file is LOST' END
         WHERE dcId = dc.dcId;
      ELSE
        -- it was not the last valid copy, replicate from another one
        UPDATE DeleteDiskCopyHelper
           SET rc = dconst.DELDC_NOOP, msg = 'dropped from disk pool'
         WHERE dcId = dc.dcId;
        IF inDryRun = 0 THEN
          logToDLF(NULL, dlf.LVL_SYSTEM, dlf.DELETEDISKCOPY_REPLICATION,
                   dc.fileId, varNsHost, 'stagerd', varLogParams);
          handleReplication(dc.fileId, varNsHost, varCfId, varNsOpenTime, varSvcClassId, varEuid, varEgid);
        END IF;
      END IF;
    ELSE
      -- similarly to stageRm, check that the deletion is allowed:
      -- basically only files on tape may be dropped in case no data loss is provoked,
      -- or files already dropped from the namespace. The rest is forbidden.
      IF (varStatus IN (dconst.DISKCOPY_VALID, dconst.DISKCOPY_FAILED) AND (varNbRemaining > 0 OR dc.fStatus = 'm' OR varFileSize = 0))
         OR dc.fStatus = 'd' THEN
        UPDATE DeleteDiskCopyHelper
           SET rc = dconst.DELDC_NOOP,
               msg = 'garbage collected from stager'
         WHERE dcId = dc.dcId;
        IF inDryRun = 0 THEN
          IF varStatus = dconst.DISKCOPY_VALID THEN
            UPDATE DiskCopy
               SET status = dconst.DISKCOPY_INVALID, gcType = dconst.GCTYPE_ADMIN
             WHERE id = dc.dcId;
          ELSE
            DELETE FROM DiskCopy WHERE ID = dc.dcId;
          END IF;
          -- do not forget to cancel pending migrations in case we've lost that last DiskCopy
          IF varNbRemaining = 0 THEN
            deleteMigrationJobs(varCfId);
          ELSE
            -- try to recreate lost copy if possible
            handleReplication(dc.fileId, varNsHost, varCfId, varNsOpenTime, varSvcClassId, varEuid, varEgid);
          END IF;
          logToDLF(NULL, dlf.LVL_SYSTEM, dlf.DELETEDISKCOPY_GC, dc.fileId, varNsHost, 'stagerd', varLogParams);
        END IF;
      ELSE
        -- nothing is done, just record no-action
        UPDATE DeleteDiskCopyHelper
           SET rc = dconst.DELDC_NOOP,
               msg = 'NOT garbage collected from stager'
         WHERE dcId = dc.dcId;
        IF inDryRun = 0 THEN
          logToDLF(NULL, dlf.LVL_SYSTEM, dlf.DELETEDISKCOPY_NOOP, dc.fileId, varNsHost, 'stagerd', varLogParams);
        END IF;
      END IF;
    END IF;
    COMMIT;   -- release locks file by file
  END LOOP;
  -- return back all results for the python script to post-process them,
  -- including performing all required actions
  OPEN outRes FOR
    SELECT dcId, fileId, msg, rc FROM DeleteDiskCopyHelper;
END;
/

CREATE OR REPLACE PROCEDURE deleteDiskCopies(inArgs IN castor."strList",
                                             inForce IN INTEGER, inDryRun IN INTEGER,
                                             outRes OUT castor.DiskCopyResult_Cur) AS
BEGIN
  DELETE FROM DeleteDiskCopyHelper;
  -- Go through arguments one by one, parse them and fill temporary table with the files to process
  -- 3 formats are accepted :
  --    host:/mountpoint : drop all files on a given filesystem
  --    host:<fullFilePath> : drop the given file from a given disk server
  --    datapoolurlprefix:///extuser@extpool:fullFilePath : drop the given file from a given data pool
  FOR i IN 1..inArgs.COUNT LOOP
    DECLARE
      varMountPoint VARCHAR2(2048);
      varDataPool VARCHAR2(2048);
      varPath VARCHAR2(2048);
      varDcId INTEGER;
      varFileId INTEGER;
      varNsHost VARCHAR2(2048);
    BEGIN
      parsePath(inArgs(i), varMountPoint, varDataPool, varPath, varDcId, varFileId, varNsHost);
      -- we've got a file path
      INSERT INTO DeleteDiskCopyHelper (dcId, fileId, fStatus, rc)
      VALUES (varDcId, varFileId, 'd', -1);
    EXCEPTION WHEN OTHERS THEN
      DECLARE
        varColonPos INTEGER;
        varDiskServerName VARCHAR2(2048);
        varMountPoint VARCHAR2(2048);
        varFsId INTEGER;
      BEGIN
        -- not a file path, probably a mountPoint
        varColonPos := INSTR(inArgs(i), ':', 1, 1);
        IF varColonPos = 0 THEN
          raise_application_error(-20100, 'incorrect/incomplete value found as argument, giving up');
        END IF;
        varDiskServerName := SUBSTR(inArgs(i), 1, varColonPos-1);
        varMountPoint := SUBSTR(inArgs(i), varColonPos+1);
        -- check existence of this DiskServer/mountPoint
        BEGIN
          SELECT FileSystem.id INTO varFsId
            FROM FileSystem, DiskServer
           WHERE FileSystem.mountPoint = varMountPoint
             AND FileSystem.diskServer = DiskServer.id
             AND DiskServer.name = varDiskServerName;
        EXCEPTION WHEN NO_DATA_FOUND THEN
          raise_application_error(-20100, 'no filesystem found for ' || inArgs(i) || ', giving up. Note that machine names must be fully qualified.');
        END;
        -- select the disk copies to be deleted
        INSERT INTO DeleteDiskCopyHelper (dcId, fileId, fStatus, msg, rc)
          SELECT DiskCopy.id, CastorFile.fileid, 'd', '', -1
            FROM DiskCopy, CastorFile
           WHERE DiskCopy.castorFile = CastorFile.id
             AND DiskCopy.fileSystem = varFsId;
      END;
    END;
  END LOOP;
  -- now call the internal method doing the real job
  internalDeleteDiskCopies(inForce, inDryRun, outRes);
END;
/


/* Recompile all invalid procedures, triggers and functions */
/************************************************************/
BEGIN
  recompileAll();
END;
/

/* Flag the schema upgrade as COMPLETE */
/***************************************/
UPDATE UpgradeLog SET endDate = systimestamp, state = 'COMPLETE'
 WHERE release = '2_1_17_18';
COMMIT;
