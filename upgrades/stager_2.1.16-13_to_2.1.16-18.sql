/******************************************************************************
 *                 stager_2.1.16-13_to_2.1.16-18.sql
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This script upgrades a CASTOR v2.1.16-13 STAGER database to v2.1.16-18
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

/* Stop on errors */
WHENEVER SQLERROR EXIT FAILURE
BEGIN
  -- If we have encountered an error rollback any previously non committed
  -- operations. This prevents the UPDATE of the UpgradeLog from committing
  -- inconsistent data to the database.
  ROLLBACK;
  UPDATE UpgradeLog
     SET failureCount = failureCount + 1
   WHERE schemaVersion = '2_1_15_18'
     AND release = '2_1_16_18'
     AND state != 'COMPLETE';
  COMMIT;
END;
/

/* Verify that the script is running against the correct schema and version */
DECLARE
  unused VARCHAR(100);
BEGIN
  SELECT release INTO unused FROM CastorVersion
   WHERE schemaName = 'STAGER'
     AND release LIKE '2_1_16_13%';
EXCEPTION WHEN NO_DATA_FOUND THEN
  -- Error, we cannot apply this script
  raise_application_error(-20000, 'PL/SQL release mismatch. Please run previous upgrade scripts for the STAGER before this one.');
END;
/

INSERT INTO UpgradeLog (schemaVersion, release, type)
VALUES ('2_1_15_18', '2_1_16_18', 'TRANSPARENT');
COMMIT;

/* Clean up old PrepareToPut requests left behind. See CASTOR-5466: stager_rm may not fail ongoing PrepareToPut requests */
BEGIN
  FOR s IN (select id from subrequest where status=6 and reqtype=37 and creationtime < gettime()-100000) LOOP
    UPDATE subrequest set status = 7 where id = s.id;
    COMMIT;
  END LOOP;
END;
/

/* PL/SQL method to process bulk abort on a given Repack request */
CREATE OR REPLACE PROCEDURE processBulkAbortForRepack(origReqId IN INTEGER) AS
  abortedSRstatus INTEGER := -1;
  srsToUpdate "numList";
  dcmigrsToUpdate "numList";
  nbItems INTEGER;
  nbItemsDone INTEGER := 0;
  SrLocked EXCEPTION;
  PRAGMA EXCEPTION_INIT (SrLocked, -54);
  cfId INTEGER;
  srId INTEGER;
  firstOne BOOLEAN := TRUE;
  commitWork BOOLEAN := FALSE;
  varOriginalVID VARCHAR2(2048);
BEGIN
  -- get the VID of the aborted repack request
  SELECT repackVID INTO varOriginalVID FROM StageRepackRequest WHERE id = origReqId;
  -- Gather the list of subrequests to abort
  INSERT INTO ProcessBulkAbortFileReqsHelper (srId, cfId, fileId, nsHost, uuid) (
    SELECT /*+ INDEX_RS_ASC(Subrequest I_Subrequest_CastorFile)*/
           SubRequest.id, CastorFile.id, CastorFile.fileId, CastorFile.nsHost, SubRequest.subreqId
      FROM SubRequest, CastorFile
     WHERE SubRequest.castorFile = CastorFile.id
       AND request = origReqId
       AND status IN (dconst.SUBREQUEST_START, dconst.SUBREQUEST_RESTART, dconst.SUBREQUEST_RETRY,
                      dconst.SUBREQUEST_WAITSUBREQ, dconst.SUBREQUEST_WAITTAPERECALL,
                      dconst.SUBREQUEST_REPACK));
  SELECT COUNT(*) INTO nbItems FROM processBulkAbortFileReqsHelper;
  -- handle aborts in bulk while avoiding deadlocks
  WHILE nbItems > 0 LOOP
    FOR sr IN (SELECT srId, cfId, fileId, nsHost, uuid FROM processBulkAbortFileReqsHelper) LOOP
      BEGIN
        IF firstOne THEN
          -- on the first item, we take a blocking lock as we are sure that we will not
          -- deadlock and we would like to process at least one item to not loop endlessly
          SELECT id INTO cfId FROM CastorFile WHERE id = sr.cfId FOR UPDATE;
          firstOne := FALSE;
        ELSE
          -- on the other items, we go for a non blocking lock. If we get it, that's
          -- good and we process this extra subrequest within the same session. If
          -- we do not get the lock, then we close the session here and go for a new
          -- one. This will prevent dead locks while ensuring that a minimal number of
          -- commits is performed.
          SELECT id INTO cfId FROM CastorFile WHERE id = sr.cfId FOR UPDATE NOWAIT;
        END IF;
        -- note the revalidation of the status and even of the existence of the subrequest
        -- as it may have changed before we got the lock on the Castorfile in processBulkAbortFileReqs
        SELECT /*+ INDEX(Subrequest PK_Subrequest_Id)*/ status
          INTO abortedSRstatus
          FROM SubRequest
         WHERE id = sr.srId;
        CASE
          WHEN abortedSRstatus = dconst.SUBREQUEST_START
            OR abortedSRstatus = dconst.SUBREQUEST_RESTART
            OR abortedSRstatus = dconst.SUBREQUEST_RETRY
            OR abortedSRstatus = dconst.SUBREQUEST_WAITSUBREQ THEN
            -- easy case, we only have to fail the subrequest
            INSERT INTO ProcessRepackAbortHelperSR (srId) VALUES (sr.srId);
          WHEN abortedSRstatus = dconst.SUBREQUEST_WAITTAPERECALL THEN
            -- recall case, fail the subRequest and cancel the recall if needed
            failRecallSubReq(sr.srId, sr.cfId);
          WHEN abortedSRstatus = dconst.SUBREQUEST_REPACK THEN
            -- trigger the update the subrequest status to FAILED
            INSERT INTO ProcessRepackAbortHelperSR (srId) VALUES (sr.srId);
            -- delete migration jobs of this repack, hence stopping selectively the migrations
            DELETE FROM MigrationJob WHERE castorfile = sr.cfId AND originalVID = varOriginalVID;
            -- delete migrated segments if no migration jobs remain
            BEGIN
              SELECT id INTO cfId FROM MigrationJob WHERE castorfile = sr.cfId AND ROWNUM < 2;
            EXCEPTION WHEN NO_DATA_FOUND THEN
              DELETE FROM MigratedSegment WHERE castorfile = sr.cfId;
            END;
            -- trigger the restore of the CastorFile's tapeStatus to ONTAPE in all cases:
            -- if the migration had failed, the file remained in the original tape
            INSERT INTO ProcessRepackAbortHelperDCmigr (cfId) VALUES (sr.cfId);
          WHEN abortedSRstatus IN (dconst.SUBREQUEST_FAILED,
                                   dconst.SUBREQUEST_FINISHED,
                                   dconst.SUBREQUEST_FAILED_FINISHED,
                                   dconst.SUBREQUEST_ARCHIVED) THEN
            -- nothing to be done here
            NULL;
        END CASE;
        DELETE FROM processBulkAbortFileReqsHelper WHERE srId = sr.srId;
        nbItemsDone := nbItemsDone + 1;
      EXCEPTION WHEN SrLocked THEN
        commitWork := TRUE;
      END;
      -- commit anyway from time to time, to avoid too long redo logs
      IF commitWork OR nbItemsDone >= 1000 THEN
        -- exit the current loop and restart a new one, in order to commit without getting invalid ROWID errors
        EXIT;
      END IF;
    END LOOP;
    -- do the bulk updates
    SELECT srId BULK COLLECT INTO srsToUpdate FROM ProcessRepackAbortHelperSR;
    FORALL i IN 1 .. srsToUpdate.COUNT
      UPDATE /*+ INDEX(Subrequest PK_Subrequest_Id)*/ SubRequest
         SET diskCopy = NULL, lastModificationTime = getTime(),
             status = dconst.SUBREQUEST_FAILED_FINISHED,
             errorCode = 1701, errorMessage = 'Aborted explicitely'  -- ESTCLEARED
       WHERE id = srsToUpdate(i);
    SELECT cfId BULK COLLECT INTO dcmigrsToUpdate FROM ProcessRepackAbortHelperDCmigr;
    FORALL i IN 1 .. dcmigrsToUpdate.COUNT
      UPDATE CastorFile SET tapeStatus = dconst.CASTORFILE_ONTAPE WHERE id = dcmigrsToUpdate(i);
    -- commit
    COMMIT;
    -- reset all counters
    nbItems := nbItems - nbItemsDone;
    nbItemsDone := 0;
    firstOne := TRUE;
    commitWork := FALSE;
  END LOOP;
  -- archive the request
  BEGIN
    SELECT id, status INTO srId, abortedSRstatus
      FROM SubRequest
     WHERE request = origReqId
       AND status IN (dconst.SUBREQUEST_FINISHED, dconst.SUBREQUEST_FAILED_FINISHED)
       AND ROWNUM = 1;
    -- This procedure should really be called 'terminateSubReqAndArchiveRequest', and this is
    -- why we call it here: we need to trigger the logic to mark the whole request and all of its subrequests
    -- as ARCHIVED, so that they are cleaned up afterwards. Note that this is effectively
    -- a no-op for the status change of the single fetched SubRequest.
    archiveSubReq(srId, abortedSRstatus);
  EXCEPTION WHEN NO_DATA_FOUND THEN
    -- Should never happen, anyway ignore as there's nothing else to do
    NULL;
  END;
  COMMIT;
END;
/

/* PL/SQL method implementing bestFileSystemForRecall */
CREATE OR REPLACE PROCEDURE bestFileSystemForRecall(inCfId IN INTEGER, outFilePath OUT VARCHAR2) AS
  varCfId INTEGER;
  varUnused NUMBER;
  varChooseDataPool NUMBER;
  varRemotePath VARCHAR2(2048);
  varFileId INTEGER;
  varNsHost VARCHAR2(100);
BEGIN
  outFilePath := '';
  varChooseDataPool := TO_NUMBER(getConfigOption('Recall', 'DataPoolUsage', '0.5'));
  IF DBMS_Random.value < varChooseDataPool THEN
    BEGIN
      -- We try and use a data pool if available
      SELECT /*+ INDEX_RS_ASC(RecallJob I_RecallJob_Castorfile_VID) */
             'radosstriper:///' || DataPool.externalUser || '@' || DataPool.externalPool || ':' AS remotePath,
             CastorFile.fileId, CastorFile.nsHost
        INTO varRemotePath, varFileId, varNsHost
        FROM DataPool, DataPool2SvcClass, CastorFile, RecallJob
       WHERE CastorFile.id = inCfId
         AND RecallJob.castorFile = inCfId
         AND RecallJob.svcClass = DataPool2SvcClass.child
         -- a priori, we want to have enough free space. However, if we don't, we accept to start writing
         -- if we have a minimum of 30GB free and count on gerbage collection to liberate space while writing
         -- We still check that the file fit on the disk, and actually keep a 30% margin so that very recent
         -- files can be kept
         AND (DataPool.free - DataPool.minAllowedFreeSpace * DataPool.totalSize > CastorFile.fileSize
           OR (DataPool.free - DataPool.minAllowedFreeSpace * DataPool.totalSize > 30000000000
           AND DataPool.totalSize * 0.7 > CastorFile.fileSize))
         AND ROWNUM = 1;   -- we might have multiple data pools
        buildPathFromFileId(varFileId, varNsHost, ids_seq.nextval, outFilePath, FALSE);  -- no diskPool
        outFilePath := varRemotePath || outFilePath;
    EXCEPTION WHEN NO_DATA_FOUND THEN
      -- No data pools available, move on to disk pools
      NULL;
    END;
  END IF;
  IF outFilePath IS NULL THEN
    -- Try and select a good FileSystem for this recall
    FOR f IN (SELECT * FROM (
                SELECT /*+ INDEX_RS_ASC(RecallJob I_RecallJob_Castorfile_VID) */
                       DiskServer.name ||':'|| FileSystem.mountPoint AS remotePath, FileSystem.id,
                       CastorFile.fileSize, CastorFile.fileId, CastorFile.nsHost
                  FROM DiskServer, FileSystem, DiskPool2SvcClass, CastorFile, RecallJob
                 WHERE CastorFile.id = inCfId
                   AND RecallJob.castorFile = inCfId
                   AND RecallJob.svcClass = DiskPool2SvcClass.child
                   AND FileSystem.diskPool = DiskPool2SvcClass.parent
                   -- a priori, we want to have enough free space. However, if we don't, we accept to start writing
                   -- if we have a minimum of 30GB free and count on gerbage collection to liberate space while writing
                   -- We still check that the file fit on the disk, and actually keep a 30% margin so that very recent
                   -- files can be kept
                   AND (FileSystem.free - FileSystem.minAllowedFreeSpace * FileSystem.totalSize > CastorFile.fileSize
                     OR (FileSystem.free - FileSystem.minAllowedFreeSpace * FileSystem.totalSize > 30000000000
                     AND FileSystem.totalSize * 0.7 > CastorFile.fileSize))
                   AND FileSystem.status = dconst.FILESYSTEM_PRODUCTION
                   AND DiskServer.id = FileSystem.diskServer
                   AND DiskServer.status = dconst.DISKSERVER_PRODUCTION
                   AND DiskServer.hwOnline = 1
              ORDER BY -- order by filesystem load first: this works if the feedback loop is fast enough, that is
                       -- the transfer of the selected files in bulk does not take more than a couple of minutes
                       FileSystem.nbMigratorStreams + FileSystem.nbRecallerStreams ASC,
                       -- then use randomness for tie break
                       DBMS_Random.value))
    LOOP
      -- Check that we don't already have a copy of this file on the selected filesystem.
      -- This will never happen in normal operations but may be the case if a filesystem
      -- was disabled and did come back while the tape recall was waiting.
      -- Even if we optimize by cancelling remaining unneeded tape recalls when a
      -- fileSystem comes back, the ones running at the time of the come back will have
      -- the problem.
      BEGIN
        SELECT /*+ INDEX_RS_ASC(DiskCopy I_DiskCopy_CastorFile) */ 1 INTO varUnused
          FROM DiskCopy
         WHERE fileSystem = f.id
           AND castorfile = inCfid
           AND status = dconst.DISKCOPY_VALID
           AND ROWNUM < 2;
        -- Found it, go to next choice
        CONTINUE;
      EXCEPTION WHEN NO_DATA_FOUND THEN
        -- OK, we didn't find it, this destination is good to go
        buildPathFromFileId(f.fileId, f.nsHost, ids_seq.nextval, outFilePath, TRUE);  -- this is a diskPool
        outFilePath := f.remotePath || outFilePath;
        EXIT;
      END;
    END LOOP;
  END IF;
  -- We could not find anything, give up. Note that if only a data pool was available but DataPoolUsage < 1,
  -- we may get here despite the availability of the data pool.
  IF outFilePath IS NULL THEN
    raise_application_error(-20115, 'No suitable filesystem or datapool found for this recall');
  END IF;
END;
/


/* PL/SQL declaration for the castorDebug package */
CREATE OR REPLACE PACKAGE castorDebug AS
  TYPE DiskCopyDebug_typ IS RECORD (
    id INTEGER,
    status VARCHAR2(2048),
    creationtime VARCHAR2(2048),
    pool VARCHAR2(2048),
    location VARCHAR2(2048),
    available CHAR(1),
    diskCopySize NUMBER,
    castorFileSize NUMBER,
    gcWeight NUMBER);
  TYPE DiskCopyDebug IS TABLE OF DiskCopyDebug_typ;
  TYPE SubRequestDebug IS TABLE OF SubRequest%ROWTYPE;
  TYPE RequestDebug_typ IS RECORD (
    id NUMBER,
    status VARCHAR2(100),
    creationTime VARCHAR2(2048),
    transferId VARCHAR2(2048),
    username VARCHAR2(2048),
    protocol VARCHAR2(2048),
    machine VARCHAR2(2048),
    svcClassName VARCHAR2(2048),
    ReqId NUMBER,
    ReqType VARCHAR2(20));
  TYPE RequestDebug IS TABLE OF RequestDebug_typ;
  TYPE RecallJobDebug_typ IS RECORD (
    id INTEGER,
    status VARCHAR2(2048),
    creationtime VARCHAR2(2048),
    fseq INTEGER,
    copyNb INTEGER,
    recallGroup VARCHAR(2048),
    svcClass VARCHAR(2048),
    euid INTEGER,
    egid INTEGER,
    vid VARCHAR(2048),
    nbRetriesWithinMount INTEGER,
    nbMounts INTEGER);
  TYPE RecallJobDebug IS TABLE OF RecallJobDebug_typ;
  TYPE MigrationJobDebug_typ IS RECORD (
    id INTEGER,
    status VARCHAR2(2048),
    creationTime VARCHAR2(2048),
    fileSize INTEGER,
    tapePoolName VARCHAR2(2048),
    destCopyNb INTEGER,
    fseq INTEGER,
    mountTransactionId INTEGER,
    originalVID VARCHAR2(2048),
    originalCopyNb INTEGER,
    nbRetries INTEGER,
    fileTransactionId INTEGER);
  TYPE MigrationJobDebug IS TABLE OF MigrationJobDebug_typ;
  TYPE Disk2DiskCopyJobDebug_typ IS RECORD (
    id INTEGER,
    status VARCHAR2(2048),
    creationTime VARCHAR2(2048),
    transferId VARCHAR2(2048),
    retryCounter INTEGER,
    nsOpenTime INTEGER,
    destSvcClassName VARCHAR2(2048),
    replicationType VARCHAR2(2048),
    srcDCId INTEGER,
    destDCId INTEGER,
    drainingJob INTEGER);
  TYPE Disk2DiskCopyJobDebug IS TABLE OF Disk2DiskCopyJobDebug_typ;
  TYPE NameserverDebug_typ IS RECORD (
    fileid INTEGER,
    status CHAR(1),
    className VARCHAR2(100),
    filePath VARCHAR2(2048),
    fileSize INTEGER,
    fileChecksum VARCHAR2(10),
    aTime VARCHAR2(100),
    mTime VARCHAR2(100),
    stagerOpenTime INTEGER);
  TYPE NameserverDebug IS TABLE OF NameserverDebug_typ;
  TYPE NameserverSegDebug_typ IS RECORD (
    fileid INTEGER,
    copyno NUMBER(1),
    segStatus CHAR(1),
    vid VARCHAR2(10),
    tapeStatus VARCHAR2(100),
    fseq NUMBER(10),
    segSize NUMBER,
    segChecksum VARCHAR2(10),
    segCreationTime VARCHAR2(100),
    segLastModificationTime VARCHAR2(100));
  TYPE NameserverSegDebug IS TABLE OF NameserverSegDebug_typ;
END;
/

/* Get the requests associated with the reference number. */
CREATE OR REPLACE FUNCTION getRs(ref number) RETURN castorDebug.RequestDebug PIPELINED AS
BEGIN
  FOR d IN (SELECT SubRequest.id, getObjStatusName('SubRequest', 'status', SubRequest.status) as status,
                   getTimeString(creationtime) AS creationTime, SubRequest.subReqId as transferId,
                   username, protocol, machine, svcClassName, Request.id AS ReqId, Request.type AS ReqType
              FROM SubRequest,
                    (SELECT /*+ INDEX(StageGetRequest PK_StageGetRequest_Id) */ id, username, machine, svcClassName, 'Get' AS type FROM StageGetRequest UNION ALL
                     SELECT /*+ INDEX(StagePrepareToGetRequest PK_StagePrepareToGetRequest_Id) */ id, username, machine, svcClassName, 'PGet' AS type FROM StagePrepareToGetRequest UNION ALL
                     SELECT /*+ INDEX(StagePutRequest PK_StagePutRequest_Id) */ id, username, machine, svcClassName, 'Put' AS type FROM StagePutRequest UNION ALL
                     SELECT /*+ INDEX(StagePrepareToPutRequest PK_StagePrepareToPutRequest_Id) */ id, username, machine, svcClassName, 'PPut' AS type FROM StagePrepareToPutRequest UNION ALL
                     SELECT /*+ INDEX(StageRepackRequest PK_StageRepackRequest_Id) */ id, username, machine, svcClassName, 'Repack' AS type FROM StageRepackRequest UNION ALL
                     SELECT /*+ INDEX(StagePutDoneRequest PK_StagePutDoneRequest_Id) */ id, username, machine, svcClassName, 'PutDone' AS type FROM StagePutDoneRequest UNION ALL
                     SELECT /*+ INDEX(SetFileGCWeight PK_SetFileGCWeight_Id) */ id, username, machine, svcClassName, 'SetGCW' AS type FROM SetFileGCWeight) Request
             WHERE castorfile = getCF(ref)
               AND Request.id = SubRequest.request) LOOP
     PIPE ROW(d);
  END LOOP;
END;
/

/* PL/SQL method implementing stageRm */
CREATE OR REPLACE PROCEDURE stageRm (srId IN INTEGER,
                                     fid IN INTEGER,
                                     nh IN VARCHAR2,
                                     svcClassId IN INTEGER,
                                     ret OUT INTEGER) AS
  nsHostName VARCHAR2(2048);
  cfId INTEGER;
  dcsToRm "numList";
  dcsToRmStatus "numList";
  dcsToRmCfStatus "numList";
  nbRJsDeleted INTEGER;
  varNbSRsDeleted INTEGER := 0;
  varNbValidRmed INTEGER;
BEGIN
  ret := 0;
  -- Get the stager/nsHost configuration option
  nsHostName := getConfigOption('stager', 'nsHost', nh);
  BEGIN
    -- Lock the access to the CastorFile
    -- This, together with triggers will avoid new migration/recall jobs
    -- or DiskCopies to be added
    SELECT id INTO cfId FROM CastorFile
     WHERE fileId = fid AND nsHost = nsHostName FOR UPDATE;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    -- This file does not exist in the stager catalog
    -- so we just fail the request
    UPDATE /*+ INDEX(Subrequest PK_Subrequest_Id)*/ SubRequest
       SET status = dconst.SUBREQUEST_FAILED,
           errorCode = serrno.ENOENT,
           errorMessage = 'File not found on disk cache'
     WHERE id = srId;
    RETURN;
  END;

  -- select the list of DiskCopies to be deleted
  SELECT id, status, tapeStatus BULK COLLECT INTO dcsToRm, dcsToRmStatus, dcsToRmCfStatus FROM (
    SELECT /*+ INDEX_RS_ASC(DC I_DiskCopy_CastorFile) */
           DiskCopy.id, DiskCopy.status, CastorFile.tapeStatus
      FROM DiskCopy, FileSystem, DiskPool2SvcClass, CastorFile
     WHERE DiskCopy.castorFile = cfId
       AND DiskCopy.status IN (dconst.DISKCOPY_VALID, dconst.DISKCOPY_STAGEOUT)
       AND DiskCopy.fileSystem = FileSystem.id
       AND FileSystem.diskPool = DiskPool2SvcClass.parent
       AND (DiskPool2SvcClass.child = svcClassId OR svcClassId = 0)
       AND CastorFile.id = cfId)
     UNION ALL
    SELECT /*+ INDEX_RS_ASC(DC I_DiskCopy_CastorFile) */
           DiskCopy.id, DiskCopy.status, CastorFile.tapeStatus
      FROM DiskCopy, DataPool2SvcClass, CastorFile
     WHERE DiskCopy.castorFile = cfId
       AND DiskCopy.status IN (dconst.DISKCOPY_VALID, dconst.DISKCOPY_STAGEOUT)
       AND DiskCopy.dataPool = DataPool2SvcClass.parent
       AND (DataPool2SvcClass.child = svcClassId OR svcClassId = 0)
       AND CastorFile.id = cfId;
  -- in case we are dropping diskcopies not yet on tape, ensure that we have at least one copy left on disk
  IF dcsToRmStatus.COUNT > 0 THEN
    IF dcsToRmStatus(1) = dconst.DISKCOPY_VALID AND dcsToRmCfStatus(1) = dconst.CASTORFILE_NOTONTAPE THEN
      BEGIN
        SELECT castorFile INTO cfId
          FROM DiskCopy
         WHERE castorFile = cfId
           AND status = dconst.DISKCOPY_VALID
           AND id NOT IN (SELECT /*+ CARDINALITY(dcidTable 5) */ * FROM TABLE(dcsToRm) dcidTable)
           AND ROWNUM < 2;
      EXCEPTION WHEN NO_DATA_FOUND THEN
        -- nothing left, so we would lose the file. Better to forbid stagerm
        UPDATE /*+ INDEX(Subrequest PK_Subrequest_Id)*/ SubRequest
           SET status = dconst.SUBREQUEST_FAILED,
               errorCode = serrno.EBUSY,
               errorMessage = 'The file is not yet migrated'
         WHERE id = srId;
        RETURN;
      END;
    END IF;

    -- fail diskcopies : WAITFS[_SCHED] -> FAILED, others -> INVALID
    UPDATE DiskCopy
       SET status = decode(status, dconst.DISKCOPY_WAITFS, dconst.DISKCOPY_FAILED,
                                   dconst.DISKCOPY_WAITFS_SCHEDULING, dconst.DISKCOPY_FAILED,
                                   dconst.DISKCOPY_INVALID),
           gcType = decode(status, dconst.DISKCOPY_WAITFS, NULL,
                                   dconst.DISKCOPY_WAITFS_SCHEDULING, NULL,
                                   dconst.GCTYPE_USER)
     WHERE id IN (SELECT /*+ CARDINALITY(dcidTable 5) */ * FROM TABLE(dcsToRm) dcidTable)
    RETURNING SUM(decode(status, dconst.DISKCOPY_VALID, 1, 0)) INTO varNbValidRmed;

    -- update importance of remaining DiskCopies, if any
    UPDATE DiskCopy SET importance = importance + varNbValidRmed
     WHERE castorFile = cfId AND status = dconst.DISKCOPY_VALID;
  END IF;

  -- fail the subrequests linked to the deleted diskcopies
  FOR sr IN (SELECT /*+ INDEX_RS_ASC(SR I_SubRequest_DiskCopy) */ id, subreqId
               FROM SubRequest SR
              WHERE diskcopy IN (SELECT /*+ CARDINALITY(dcidTable 5) */ * FROM TABLE(dcsToRm) dcidTable)
                AND status IN (dconst.SUBREQUEST_START, dconst.SUBREQUEST_RESTART,
                               dconst.SUBREQUEST_RETRY, dconst.SUBREQUEST_WAITTAPERECALL,
                               dconst.SUBREQUEST_WAITSUBREQ, dconst.SUBREQUEST_READY,
                               dconst.SUBREQUEST_READYFORSCHED)
             UNION
             -- also fail the requests linked to this file, regardless whether the diskCopy exists or not:
             -- note we don't join with DiskCopy here. Previously we did - and we included such in-flight diskcopies
             -- in the query above to populate dcsToRm, but with the new scheduling the disk copies are dropped
             -- straight in case of transfer failures, so a subsequent stageRm would not find e.g. ongoing PrepareToPut reqs
             SELECT /*+ INDEX_RS_ASC(SR I_SubRequest_Castorfile) */ SR.id, SR.subreqId
               FROM (SELECT /*+ INDEX(StagePrepareToPutRequest PK_StagePrepareToPutRequest_Id) */ id
                       FROM StagePrepareToPutRequest WHERE svcClass = svcClassId OR svcClassId = 0 UNION ALL
                     SELECT /*+ INDEX(StagePutRequest PK_StagePutRequest_Id) */ id
                       FROM StagePutRequest WHERE svcClass = svcClassId OR svcClassId = 0) Request,
                    SubRequest SR, CastorFile
              WHERE SR.castorFile = CastorFile.id
                AND Request.id = SR.request
                AND CastorFile.id = cfId
                ) LOOP
    UPDATE SubRequest
       SET status = dconst.SUBREQUEST_FAILED,
           errorCode = serrno.EINTR,
           errorMessage = 'Canceled by another user request'
     WHERE id = sr.id
        OR (castorFile = cfId AND status = dconst.SUBREQUEST_WAITSUBREQ);
    varNbSRsDeleted := varNbSRsDeleted + SQL%ROWCOUNT;
    -- make the scheduler aware so that it can remove the transfer from the queues if needed
    DECLARE
      CONSTRAINT_VIOLATED EXCEPTION;
      PRAGMA EXCEPTION_INIT(CONSTRAINT_VIOLATED, -1);
    BEGIN
      INSERT INTO TransfersToAbort (uuid) VALUES (sr.subreqId);
    EXCEPTION WHEN CONSTRAINT_VIOLATED THEN
      -- Nothing to do : the transfer is already in the list of transfers to be aborted
      NULL;
    END;
  END LOOP;

  -- delete RecallJobs that should be canceled
  DELETE FROM RecallJob
   WHERE castorfile = cfId AND (svcClass = svcClassId OR svcClassId = 0);
  nbRJsDeleted := SQL%ROWCOUNT;
  -- in case we've dropped something, check whether we still have recalls ongoing
  IF nbRJsDeleted > 0 THEN
    BEGIN
      SELECT castorFile INTO cfId
        FROM RecallJob
       WHERE castorFile = cfId;
    EXCEPTION WHEN NO_DATA_FOUND THEN
      -- all recalls are canceled for this file
      -- deal with potential waiting migrationJobs
      deleteMigrationJobsForRecall(cfId);
      -- fail corresponding requests
      UPDATE SubRequest
         SET status = dconst.SUBREQUEST_FAILED,
             errorCode = serrno.EINTR,
             errorMessage = 'Canceled by another user request'
       WHERE castorFile = cfId
         AND status = dconst.SUBREQUEST_WAITTAPERECALL;
    END;
  END IF;

  -- In case nothing was dropped at all, complain
  IF dcsToRm.COUNT = 0 AND nbRJsDeleted = 0 AND varNbSRsDeleted = 0 THEN
    UPDATE /*+ INDEX(Subrequest PK_Subrequest_Id)*/ SubRequest
       SET status = dconst.SUBREQUEST_FAILED,
           errorCode = serrno.ENOENT,
           errorMessage = CASE WHEN svcClassId = 0 THEN 'File not found on disk cache'
                               ELSE 'File not found on this service class' END
     WHERE id = srId;
    RETURN;
  END IF;

  -- In case of something to abort, first commit and then signal the service.
  -- This procedure is called by the stager with autocommit=TRUE, hence
  -- committing here is safe.
  IF dcsToRmStatus.COUNT > 0 THEN
    COMMIT;
    -- wake up the scheduler so that it can remove the transfer from the queues now
    alertSignalNoLock('transfersToAbort');
  END IF;

  ret := 1;  -- ok
END;
/


/* Recompile all invalid procedures, triggers and functions */
/************************************************************/
BEGIN
  recompileAll();
END;
/

/* Flag the schema upgrade as COMPLETE */
/***************************************/
UPDATE UpgradeLog SET endDate = systimestamp, state = 'COMPLETE'
 WHERE release = '2_1_16_18';
COMMIT;
