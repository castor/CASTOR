/******************************************************************************
 *                 stager_2.1.17-26_to_2.1.17-30.sql
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This script upgrades a CASTOR v2.1.17-26 STAGER database to v2.1.17-30
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

/* Stop on errors */
WHENEVER SQLERROR EXIT FAILURE
BEGIN
  -- If we have encountered an error rollback any previously non committed
  -- operations. This prevents the UPDATE of the UpgradeLog from committing
  -- inconsistent data to the database.
  ROLLBACK;
  UPDATE UpgradeLog
     SET failureCount = failureCount + 1
   WHERE schemaVersion = '2_1_15_18'
     AND release = '2_1_17_30'
     AND state != 'COMPLETE';
  COMMIT;
END;
/

/* Verify that the script is running against the correct schema and version */
DECLARE
  unused VARCHAR(100);
BEGIN
  SELECT release INTO unused FROM CastorVersion
   WHERE schemaName = 'STAGER'
     AND (release = '2_1_17_26' OR release = '2_1_17_28');
EXCEPTION WHEN NO_DATA_FOUND THEN
  -- Error, we cannot apply this script
  raise_application_error(-20000, 'PL/SQL release mismatch. Please run previous upgrade scripts for the STAGER before this one.');
END;
/

INSERT INTO UpgradeLog (schemaVersion, release, type)
VALUES ('2_1_15_18', '2_1_17_30', 'TRANSPARENT');
COMMIT;

/* Config change */

INSERT INTO CastorConfig
  VALUES ('Recall', 'DataBonusPerDay', '100000000000', 'The amount of data bonus a recall request will get for every day of delay');

/* Update code */

/* insert new Migration Mount */
CREATE OR REPLACE PROCEDURE insertMigrationMount(inTapePoolId IN NUMBER,
                                                 minimumSize IN INTEGER,
                                                 minimumAge IN INTEGER,
                                                 outMountId OUT INTEGER) AS
  backlogSize INTEGER;
BEGIN
  -- Check that the mount would be honoured by running a dry-run file selection:
  -- note that in case the mount was triggered because of age, we check that
  -- we have a valid candidate that is at least minimumAge seconds old.
  -- This is almost a duplicate of the query in tg_getFilesToMigrate.
  SELECT /*+ LEADING(MigrationJob CastorFile DiskCopy FileSystem DiskServer)
             USE_NL(MigrationJob CastorFile DiskCopy FileSystem DiskServer)
             INDEX(CastorFile PK_CastorFile_Id)
             INDEX_RS_ASC(DiskCopy I_DiskCopy_CastorFile)
             INDEX_RS_ASC(MigrationJob I_MigrationJob_TPStatusCT) */
         sum(CastorFile.filesize) INTO backlogSize
    FROM MigrationJob, DiskCopy, CastorFile
   WHERE MigrationJob.tapePool = inTapePoolId
     AND MigrationJob.status = tconst.MIGRATIONJOB_PENDING
     AND (minimumAge = 0 OR MigrationJob.creationTime < getTime() - minimumAge)
     AND CastorFile.id = MigrationJob.castorFile
     AND CastorFile.id = DiskCopy.castorFile
     AND CastorFile.tapeStatus = dconst.CASTORFILE_NOTONTAPE
     AND DiskCopy.status = dconst.DISKCOPY_VALID
     AND EXISTS (SELECT 1 FROM FileSystem, DiskServer
                  WHERE FileSystem.id = DiskCopy.fileSystem
                    AND FileSystem.status IN (dconst.FILESYSTEM_PRODUCTION,
                                              dconst.FILESYSTEM_DRAINING,
                                              dconst.FILESYSTEM_READONLY)
                    AND DiskServer.id = FileSystem.diskServer
                    AND DiskServer.status IN (dconst.DISKSERVER_PRODUCTION,
                                              dconst.DISKSERVER_DRAINING,
                                              dconst.DISKSERVER_READONLY)
                    AND DiskServer.hwOnline = 1
                  UNION ALL
                 SELECT 1 FROM DiskServer
                  WHERE DiskServer.dataPool = DiskCopy.dataPool
                    AND DiskServer.status IN (dconst.DISKSERVER_PRODUCTION,
                                              dconst.DISKSERVER_DRAINING,
                                              dconst.DISKSERVER_READONLY)
                    AND DiskServer.hwOnline = 1
                    AND ROWNUM < 2);
  IF backlogSize > minimumSize THEN
    -- The select worked out and we have enough available data: create a mount for this tape pool
    INSERT INTO MigrationMount
                (mountTransactionId, id, startTime, VID, label, density,
                 lastFseq, lastVDQMPingTime, tapePool, status)
      VALUES (NULL, ids_seq.nextval, gettime(), NULL, NULL, NULL,
              NULL, 0, inTapePoolId, tconst.MIGRATIONMOUNT_WAITTAPE)
      RETURNING id INTO outMountId;
  ELSE
    -- we do not have enough available data, abort the mount.
    -- This could happen e.g. when candidates exist but reside on
    -- non-available hardware. In this case we drop the mount and log
    outMountId := 0;
  END IF;
END;
/


/* DB job to start new migration mounts */
CREATE OR REPLACE PROCEDURE startMigrationMounts AS
  varNbPreExistingMounts INTEGER;
  varTotalNbMounts INTEGER := 0;
  varDataAmount INTEGER;
  varNbFiles INTEGER;
  varOldestCreationTime NUMBER;
  varMountId INTEGER;
BEGIN
  -- loop through tapepools
  FOR t IN (SELECT id, name, nbDrives, minAmountDataForMount,
                   minNbFilesForMount, maxFileAgeBeforeMount
              FROM TapePool) LOOP
    -- get number of mounts already running for this tapepool
    SELECT nvl(count(*), 0) INTO varNbPreExistingMounts
      FROM MigrationMount
     WHERE tapePool = t.id;
    varTotalNbMounts := varNbPreExistingMounts;
    -- get the amount of data and number of files to migrate, plus the age of the oldest file
    SELECT nvl(SUM(fileSize), 0), COUNT(*), nvl(MIN(creationTime), 0)
      INTO varDataAmount, varNbFiles, varOldestCreationTime
      FROM MigrationJob
     WHERE tapePool = t.id
       AND status = tconst.MIGRATIONJOB_PENDING;
    -- Create as many mounts as needed according to amount of data and number of files
    WHILE (varTotalNbMounts < t.nbDrives) AND
          ((varDataAmount/(varTotalNbMounts+1) >= t.minAmountDataForMount) OR
           (varNbFiles/(varTotalNbMounts+1) >= t.minNbFilesForMount)) AND
          (varTotalNbMounts+1 <= varNbFiles) LOOP   -- in case minAmountDataForMount << avgFileSize, stop creating more than one mount per file
      insertMigrationMount(t.id, t.minAmountDataForMount, 0, varMountId);
      IF varMountId = 0 THEN
        -- log "startMigrationMounts: failed migration mount creation due to lack of files"
        logToDLF(NULL, dlf.LVL_DEBUG, dlf.MIGMOUNT_NO_FILE, 0, '', 'tapegatewayd',
                 'tapePool=' || t.name ||
                 ' nbPreExistingMounts=' || TO_CHAR(varNbPreExistingMounts) ||
                 ' nbMounts=' || TO_CHAR(varTotalNbMounts) ||
                 ' dataAmountInQueue=' || TO_CHAR(varDataAmount) ||
                 ' nbFilesInQueue=' || TO_CHAR(varNbFiles) ||
                 ' oldestCreationTime=' || TO_CHAR(TRUNC(varOldestCreationTime)));
        -- no need to continue as we could not find enough files to migrate
        EXIT;
      ELSE
        -- log "startMigrationMounts: created new migration mount"
        logToDLF(NULL, dlf.LVL_SYSTEM, dlf.MIGMOUNT_NEW_MOUNT, 0, '', 'tapegatewayd',
                 'MigrationMountId=' || TO_CHAR(varMountId) ||
                 ' tapePool=' || t.name ||
                 ' nbPreExistingMounts=' || TO_CHAR(varNbPreExistingMounts) ||
                 ' nbMounts=' || TO_CHAR(varTotalNbMounts) ||
                 ' dataAmountInQueue=' || TO_CHAR(varDataAmount) ||
                 ' nbFilesInQueue=' || TO_CHAR(varNbFiles) ||
                 ' oldestCreationTime=' || TO_CHAR(TRUNC(varOldestCreationTime)));
        varTotalNbMounts := varTotalNbMounts + 1;
      END IF;
    END LOOP;
    -- force creation of a unique mount in case no mount was created at all and some files are too old
    IF varNbFiles > 0 AND varTotalNbMounts = 0 AND t.nbDrives > 0 AND
       gettime() - varOldestCreationTime > t.maxFileAgeBeforeMount THEN
      insertMigrationMount(t.id, 0, t.maxFileAgeBeforeMount, varMountId);
      IF varMountId = 0 THEN
        -- log "startMigrationMounts: failed migration mount creation based on age due to lack of files"
        logToDLF(NULL, dlf.LVL_SYSTEM, dlf.MIGMOUNT_AGE_NO_FILE, 0, '', 'tapegatewayd',
                 'tapePool=' || t.name ||
                 ' nbPreExistingMounts=' || TO_CHAR(varNbPreExistingMounts) ||
                 ' nbMounts=' || TO_CHAR(varTotalNbMounts) ||
                 ' dataAmountInQueue=' || TO_CHAR(varDataAmount) ||
                 ' nbFilesInQueue=' || TO_CHAR(varNbFiles) ||
                 ' oldestCreationTime=' || TO_CHAR(TRUNC(varOldestCreationTime)));
      ELSE
        -- log "startMigrationMounts: created new migration mount based on age"
        logToDLF(NULL, dlf.LVL_SYSTEM, dlf.MIGMOUNT_NEW_MOUNT_AGE, 0, '', 'tapegatewayd',
                 'MigrationMountId=' || TO_CHAR(varMountId) ||
                 ' tapePool=' || t.name ||
                 ' nbPreExistingMounts=' || TO_CHAR(varNbPreExistingMounts) ||
                 ' nbMounts=' || TO_CHAR(varTotalNbMounts) ||
                 ' dataAmountInQueue=' || TO_CHAR(varDataAmount) ||
                 ' nbFilesInQueue=' || TO_CHAR(varNbFiles) ||
                 ' oldestCreationTime=' || TO_CHAR(TRUNC(varOldestCreationTime)));
      END IF;
    ELSE
      IF varTotalNbMounts = varNbPreExistingMounts THEN 
        -- log "startMigrationMounts: no need for new migration mount"
        logToDLF(NULL, dlf.LVL_DEBUG, dlf.MIGMOUNT_NOACTION, 0, '', 'tapegatewayd',
                 'tapePool=' || t.name ||
                 ' nbPreExistingMounts=' || TO_CHAR(varNbPreExistingMounts) ||
                 ' nbMounts=' || TO_CHAR(varTotalNbMounts) ||
                 ' dataAmountInQueue=' || TO_CHAR(nvl(varDataAmount,0)) ||
                 ' nbFilesInQueue=' || TO_CHAR(nvl(varNbFiles,0)) ||
                 ' oldestCreationTime=' || TO_CHAR(TRUNC(nvl(varOldestCreationTime,0))));
      END IF;
    END IF;
    COMMIT;
  END LOOP;
END;
/

/* DB job to start new recall mounts */
CREATE OR REPLACE PROCEDURE startRecallMounts AS
   varNbMounts INTEGER;
   varNbExtraMounts INTEGER := 0;
   varNewMounts INTEGER;
BEGIN
  -- loop through RecallGroups
  FOR rg IN (SELECT id, name, nbDrives, minAmountDataForMount,
                    minNbFilesForMount, maxFileAgeBeforeMount
               FROM RecallGroup
              ORDER BY vdqmPriority DESC) LOOP
    -- get number of mounts already running for this recallGroup
    SELECT COUNT(*) INTO varNbMounts
      FROM RecallMount
     WHERE recallGroup = rg.id;
    -- check whether some tapes should be mounted
    IF varNbMounts < rg.nbDrives THEN
      DECLARE
        varVID VARCHAR2(2048);
        varDataAmount INTEGER;
        varNbFiles INTEGER;
        varOldestCreationTime NUMBER;
      BEGIN
        -- loop over the best candidates until we have enough mounts
        WHILE varNbMounts + varNbExtraMounts < rg.nbDrives LOOP
          SELECT * INTO varVID, varDataAmount, varNbFiles, varOldestCreationTime FROM (
            SELECT vid, SUM(fileSize) dataAmount, COUNT(*) nbFiles, MIN(creationTime)
              FROM RecallJob
             WHERE recallGroup = rg.id
               AND status = tconst.RECALLJOB_PENDING
             GROUP BY vid
            HAVING (SUM(fileSize) >= rg.minAmountDataForMount OR
                    COUNT(*) >= rg.minNbFilesForMount OR
                    gettime() - MIN(creationTime) > rg.maxFileAgeBeforeMount)
               AND VID NOT IN (SELECT vid FROM RecallMount)
             -- order by data amount, but give a prize of 100G for each day passed in the queue to avoid starvation
             ORDER BY dataAmount + (getTime() - MIN(creationTime))*TO_NUMBER(getConfigOption('Recall', 'DataBonusPerDay', 100000000000))/86400 DESC)
           WHERE ROWNUM < 2;
          -- trigger a new mount, with checks
          insertRecallMount(rg.id, varVID, varNewMounts);
          IF varNewMounts > 0 THEN
            varNbExtraMounts := varNbExtraMounts + varNewMounts;
            -- log "startRecallMounts: created new recall mount"
            logToDLF(NULL, dlf.LVL_SYSTEM, dlf.RECMOUNT_NEW_MOUNT, 0, '', 'tapegatewayd',
                     'recallGroup=' || rg.name ||
                     ' TPVID=' || varVid ||
                     ' nbExistingMounts=' || TO_CHAR(varNbMounts) ||
                     ' nbNewMountsSoFar=' || TO_CHAR(varNbExtraMounts) ||
                     ' dataAmountInQueue=' || TO_CHAR(varDataAmount) ||
                     ' nbFilesInQueue=' || TO_CHAR(varNbFiles) ||
                     ' oldestCreationTime=' || TO_CHAR(TRUNC(varOldestCreationTime)));
          ELSE
            -- The sanity check failed: log and report no recall mount got created for
            -- tape.
            -- "startRecallMounts: not creating mount that would have been empty (possible issue with destination diskpools)"
            logToDLF(NULL, dlf.LVL_WARNING, dlf.RECMOUNT_FAILED_NEW_MOUNT, 0, '', 'tapegatewayd',
                     'recallGroup=' || rg.name ||
                     ' TPVID=' || varVid);
          END IF;
        END LOOP;
      EXCEPTION WHEN NO_DATA_FOUND THEN
        -- nothing left to recall, just exit nicely
        NULL;
      END;
      IF varNbExtraMounts = 0 THEN
        -- log "startRecallMounts: no candidate found for a mount"
        logToDLF(NULL, dlf.LVL_DEBUG, dlf.RECMOUNT_NOACTION_NOCAND, 0, '',
                 'tapegatewayd', 'recallGroup=' || rg.name);
      END IF;
    ELSE
      -- log "startRecallMounts: not allowed to start new recall mount. Maximum nb of drives has been reached"
      logToDLF(NULL, dlf.LVL_DEBUG, dlf.RECMOUNT_NOACTION_NODRIVE, 0, '',
               'tapegatewayd', 'recallGroup=' || rg.name);
    END IF;
    COMMIT;
  END LOOP;
END;
/


/* PL/SQL method to process bulk abort on a given Repack request */
CREATE OR REPLACE PROCEDURE processBulkAbortForRepack(origReqId IN INTEGER) AS
  abortedSRstatus INTEGER := -1;
  srsToUpdate "numList";
  dcmigrsToUpdate "numList";
  nbItems INTEGER;
  nbItemsDone INTEGER := 0;
  SrLocked EXCEPTION;
  PRAGMA EXCEPTION_INIT (SrLocked, -54);
  cfId INTEGER;
  srId INTEGER;
  firstOne BOOLEAN := TRUE;
  commitWork BOOLEAN := FALSE;
  varOriginalVID VARCHAR2(2048);
BEGIN
  -- get the VID of the aborted repack request
  SELECT repackVID INTO varOriginalVID FROM StageRepackRequest WHERE id = origReqId;
  -- Gather the list of subrequests to abort
  INSERT INTO ProcessBulkAbortFileReqsHelper (srId, cfId, fileId, nsHost, uuid) (
    SELECT /*+ INDEX_RS_ASC(Subrequest I_Subrequest_CastorFile)*/
           SubRequest.id, CastorFile.id, CastorFile.fileId, CastorFile.nsHost, SubRequest.subreqId
      FROM SubRequest, CastorFile
     WHERE SubRequest.castorFile = CastorFile.id
       AND request = origReqId);
  SELECT COUNT(*) INTO nbItems FROM processBulkAbortFileReqsHelper;
  -- handle aborts in bulk while avoiding deadlocks
  WHILE nbItems > 0 LOOP
    FOR sr IN (SELECT srId, cfId, fileId, nsHost, uuid FROM processBulkAbortFileReqsHelper) LOOP
      BEGIN
        IF firstOne THEN
          -- on the first item, we take a blocking lock as we are sure that we will not
          -- deadlock and we would like to process at least one item to not loop endlessly
          SELECT id INTO cfId FROM CastorFile WHERE id = sr.cfId FOR UPDATE;
          firstOne := FALSE;
        ELSE
          -- on the other items, we go for a non blocking lock. If we get it, that's
          -- good and we process this extra subrequest within the same session. If
          -- we do not get the lock, then we close the session here and go for a new
          -- one. This will prevent dead locks while ensuring that a minimal number of
          -- commits is performed.
          SELECT id INTO cfId FROM CastorFile WHERE id = sr.cfId FOR UPDATE NOWAIT;
        END IF;
        -- note the revalidation of the status and even of the existence of the subrequest
        -- as it may have changed before we got the lock on the Castorfile in processBulkAbortFileReqs
        SELECT /*+ INDEX(Subrequest PK_Subrequest_Id)*/ status
          INTO abortedSRstatus
          FROM SubRequest
         WHERE id = sr.srId;
        CASE
          WHEN abortedSRstatus = dconst.SUBREQUEST_START
            OR abortedSRstatus = dconst.SUBREQUEST_RESTART
            OR abortedSRstatus = dconst.SUBREQUEST_RETRY
            OR abortedSRstatus = dconst.SUBREQUEST_WAITSUBREQ THEN
            -- easy case, we only have to fail the subrequest
            INSERT INTO ProcessRepackAbortHelperSR (srId) VALUES (sr.srId);
          WHEN abortedSRstatus = dconst.SUBREQUEST_WAITTAPERECALL THEN
            -- recall case, fail the subRequest and cancel the recall if needed
            failRecallSubReq(sr.srId, sr.cfId);
          WHEN abortedSRstatus = dconst.SUBREQUEST_REPACK THEN
            -- trigger the update the subrequest status to FAILED
            INSERT INTO ProcessRepackAbortHelperSR (srId) VALUES (sr.srId);
            -- delete migration jobs of this repack, hence stopping selectively the migrations
            DELETE FROM MigrationJob WHERE castorfile = sr.cfId AND originalVID = varOriginalVID;
            -- delete migrated segments if no migration jobs remain
            BEGIN
              SELECT id INTO cfId FROM MigrationJob WHERE castorfile = sr.cfId AND ROWNUM < 2;
            EXCEPTION WHEN NO_DATA_FOUND THEN
              DELETE FROM MigratedSegment WHERE castorfile = sr.cfId;
            END;
            -- trigger the restore of the CastorFile's tapeStatus to ONTAPE in all cases:
            -- if the migration had failed, the file remained in the original tape
            INSERT INTO ProcessRepackAbortHelperDCmigr (cfId) VALUES (sr.cfId);
          WHEN abortedSRstatus IN (dconst.SUBREQUEST_FAILED,
                                   dconst.SUBREQUEST_FAILED_FINISHED) THEN
            -- also for failed requests, trigger the restore of the CastorFile's tapeStatus
            INSERT INTO ProcessRepackAbortHelperDCmigr (cfId) VALUES (sr.cfId);
          WHEN abortedSRstatus IN (dconst.SUBREQUEST_FINISHED,
                                   dconst.SUBREQUEST_ARCHIVED) THEN
            -- nothing to be done here
            NULL;
        END CASE;
        DELETE FROM processBulkAbortFileReqsHelper WHERE srId = sr.srId;
        nbItemsDone := nbItemsDone + 1;
      EXCEPTION WHEN SrLocked THEN
        commitWork := TRUE;
      END;
      -- commit anyway from time to time, to avoid too long redo logs
      IF commitWork OR nbItemsDone >= 1000 THEN
        -- exit the current loop and restart a new one, in order to commit without getting invalid ROWID errors
        EXIT;
      END IF;
    END LOOP;
    -- do the bulk updates
    SELECT srId BULK COLLECT INTO srsToUpdate FROM ProcessRepackAbortHelperSR;
    FORALL i IN 1 .. srsToUpdate.COUNT
      UPDATE /*+ INDEX(Subrequest PK_Subrequest_Id)*/ SubRequest
         SET diskCopy = NULL, lastModificationTime = getTime(),
             status = dconst.SUBREQUEST_FAILED_FINISHED,
             errorCode = 1701, errorMessage = 'Aborted explicitely'  -- ESTCLEARED
       WHERE id = srsToUpdate(i);
    SELECT cfId BULK COLLECT INTO dcmigrsToUpdate FROM ProcessRepackAbortHelperDCmigr;
    FORALL i IN 1 .. dcmigrsToUpdate.COUNT
      UPDATE CastorFile SET tapeStatus = dconst.CASTORFILE_ONTAPE WHERE id = dcmigrsToUpdate(i);
    -- commit
    COMMIT;
    -- reset all counters
    nbItems := nbItems - nbItemsDone;
    nbItemsDone := 0;
    firstOne := TRUE;
    commitWork := FALSE;
  END LOOP;
  -- archive the request
  BEGIN
    SELECT id, status INTO srId, abortedSRstatus
      FROM SubRequest
     WHERE request = origReqId
       AND status IN (dconst.SUBREQUEST_FINISHED, dconst.SUBREQUEST_FAILED_FINISHED)
       AND ROWNUM = 1;
    -- This procedure should really be called 'terminateSubReqAndArchiveRequest', and this is
    -- why we call it here: we need to trigger the logic to mark the whole request and all of its subrequests
    -- as ARCHIVED, so that they are cleaned up afterwards. Note that this is effectively
    -- a no-op for the status change of the single fetched SubRequest.
    archiveSubReq(srId, abortedSRstatus);
  EXCEPTION WHEN NO_DATA_FOUND THEN
    -- Should never happen, anyway ignore as there's nothing else to do
    NULL;
  END;
  COMMIT;
END;
/

CREATE OR REPLACE PACKAGE BODY CastorMon AS

  /**
   * PL/SQL method implementing waitTapeMigrationStats
   * See the castorMon package specification for documentation.
   */
  PROCEDURE waitTapeMigrationStats AS
    CURSOR rec IS
      SELECT tapePoolName tapePool,
             nvl(sum(fileSize), 0) totalFileSize,
             count(fileSize) nbFiles,
             count(distinct(vid)) nbTapes,
             nvl(trunc(getTime()-max(creationTime)), 0) minAge,
             nvl(trunc(getTime()-min(creationTime)), 0) maxAge
      FROM (
        SELECT TapePool.name tapePoolName, MigrationJob.*
          FROM MigrationJob, TapePool
         WHERE TapePool.id = MigrationJob.tapePool(+) -- Left outer join to have zeroes when there is no migrations
           AND MigrationJob.status != tconst.MIGRATIONJOB_WAITINGONRECALL
      )
      GROUP BY tapePoolName;
  BEGIN
    FOR r in rec LOOP
      logToDLF(NULL, dlf.LVL_SYSTEM, 'waitTapeMigrationStats', 0, '', 'stagerd', 
            'TapePool="' || r.tapePool ||
            '" totalFileSize="' || r.totalFileSize || 
            '" nbFiles="' || r.nbFiles ||
            '" nbTapes="' || r.nbTapes ||
            '" minAge="' || r.minAge ||
            '" maxAge="' || r.maxAge || '"');
    END LOOP;
  END waitTapeMigrationStats;

  /**
   * PL/SQL method implementing waitTapeRecallStats
   * See the castorMon package specification for documentation.
   */
  PROCEDURE waitTapeRecallStats AS
    CURSOR rec IS
      SELECT svcClassName svcClass,
             nvl(sum(fileSize), 0) totalFileSize,
             count(fileSize) nbFiles,
             count(distinct(vid)) nbTapes,
             nvl(trunc(getTime()-max(creationTime)), 0) minAge,
             nvl(trunc(getTime()-min(creationTime)), 0) maxAge
        FROM (
        SELECT SvcClass.name svcClassName, RecallJob.*
          FROM RecallJob, SvcClass
         WHERE SvcClass.id = RecallJob.svcClass(+) -- Left outer join to have zeroes when there is no recall
      )
      GROUP BY svcClassName;
  BEGIN
    FOR r in rec LOOP
      logToDLF(NULL, dlf.LVL_SYSTEM, 'waitTapeRecallStats', 0, '', 'stagerd', 
            'SvcClass="' || r.svcClass ||
            '" totalFileSize="' || r.totalFileSize || 
            '" nbFiles="' || r.nbFiles ||
            '" nbTapes="' || r.nbTapes ||
            '" minAge="' || r.minAge ||
            '" maxAge="' || r.maxAge || '"');
    END LOOP;
  END waitTapeRecallStats;

END CastorMon;
/

/* Recompile all invalid procedures, triggers and functions */
/************************************************************/
BEGIN
  recompileAll();
END;
/

/* Flag the schema upgrade as COMPLETE */
/***************************************/
UPDATE UpgradeLog SET endDate = systimestamp, state = 'COMPLETE'
 WHERE release = '2_1_17_30';
COMMIT;
