/******************************************************************************
 *                 stager_2.1.16-6_to_2.1.16-9.sql
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This script upgrades a CASTOR v2.1.16-6 STAGER database to v2.1.16-9
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

/* Stop on errors */
WHENEVER SQLERROR EXIT FAILURE
BEGIN
  -- If we have encountered an error rollback any previously non committed
  -- operations. This prevents the UPDATE of the UpgradeLog from committing
  -- inconsistent data to the database.
  ROLLBACK;
  UPDATE UpgradeLog
     SET failureCount = failureCount + 1
   WHERE schemaVersion = '2_1_15_18'
     AND release = '2_1_16_9'
     AND state != 'COMPLETE';
  COMMIT;
END;
/

/* Verify that the script is running against the correct schema and version */
DECLARE
  unused VARCHAR(100);
BEGIN
  SELECT release INTO unused FROM CastorVersion
   WHERE schemaName = 'STAGER'
     AND release LIKE '2_1_16_6%';
EXCEPTION WHEN NO_DATA_FOUND THEN
  -- Error, we cannot apply this script
  raise_application_error(-20000, 'PL/SQL release mismatch. Please run previous upgrade scripts for the STAGER before this one.');
END;
/

INSERT INTO UpgradeLog (schemaVersion, release, type)
VALUES ('2_1_15_18', '2_1_16_9', 'TRANSPARENT');
COMMIT;

/* Job management */
BEGIN
  FOR a IN (SELECT * FROM user_scheduler_jobs)
  LOOP
    -- Stop any running jobs
    IF a.state = 'RUNNING' THEN
      dbms_scheduler.stop_job(a.job_name, force=>TRUE);
    END IF;
    -- Schedule the start date of the job to 15 minutes from now. This
    -- basically pauses the job for 15 minutes so that the upgrade can
    -- go through as quickly as possible.
    dbms_scheduler.set_attribute(a.job_name, 'START_DATE', SYSDATE + 15/1440);
  END LOOP;
END;
/

/* Schema changes */

ALTER TABLE Files2Delete MODIFY (svcClassName DEFAULT 'notUsed', userTag DEFAULT '', svcClass DEFAULT 0);
ALTER TABLE FilesDeleted MODIFY (svcClassName DEFAULT 'notUsed', userTag DEFAULT '', svcClass DEFAULT 0);
ALTER TABLE NsFilesDeleted MODIFY (svcClassName DEFAULT 'notUsed', userTag DEFAULT '', svcClass DEFAULT 0);
ALTER TABLE StgFilesDeleted MODIFY (svcClassName DEFAULT 'notUsed', userTag DEFAULT '', svcClass DEFAULT 0);
ALTER TABLE FilesDeletionFailed MODIFY (svcClassName DEFAULT 'notUsed', userTag DEFAULT '', svcClass DEFAULT 0);


/* PL/SQL code changes */
/* parse a path to give back the FileSystem and path */
CREATE OR REPLACE PROCEDURE parsePath(inFullPath IN VARCHAR2,
                                      outFileSystem OUT INTEGER,
                                      outDataPool OUT INTEGER,
                                      outPath OUT VARCHAR2,
                                      outDcId OUT INTEGER,
                                      outFileId OUT INTEGER,
                                      outNsHost OUT VARCHAR2) AS
  varUrlPrefixPos INTEGER;
  varPathPos INTEGER;
  varLastDotPos INTEGER;
  varFileIdPos INTEGER;
  varAtPos INTEGER;
  varColonPos INTEGER;
  varDiskServerName VARCHAR2(2048);
  varMountPoint VARCHAR2(2048);
  varExtPool VARCHAR2(2048);
BEGIN
  -- DcId is the part after the last '.'
  varLastDotPos := INSTR(inFullPath, '.', -1, 1);
  outDcId := TO_NUMBER(SUBSTR(inFullPath, varLastDotPos+1));
  -- the nsHost is between last '@' and last '.'
  varAtPos := INSTR(inFullPath, '@', -1, 1);
  outNsHost := SUBSTR(inFullPath, varAtPos+1, varLastDotPos-varAtPos-1);
  -- the diskserver is before the first ':' after the URL prefix if present
  varUrlPrefixPos := INSTR(inFullPath, ':///', 1, 1);
  IF 0 = varUrlPrefixPos THEN
    varUrlPrefixPos := 1;
  ELSE
    varUrlPrefixPos := varUrlPrefixPos + 4;   -- position after the ':///' prefix
  END IF;
  varColonPos := INSTR(inFullPath, ':', varUrlPrefixPos, 1);
  varDiskServerName := SUBSTR(inFullPath, 1, varColonPos-1);
  -- if the URL ':///' prefix was found, then we are dealing with a data pool
  IF varUrlPrefixPos > 1 THEN
    -- the fileid is between last ':' and '@'
    varFileIdPos := INSTR(inFullPath, ':', -1, 1);
    outFileId := TO_NUMBER(SUBSTR(inFullPath, varFileIdPos+1, varAtPos-varFileIdPos-1));
    -- the data pool is between the first '@' and the ':' before the fileId
    varAtPos := INSTR(inFullPath, '@', varUrlPrefixPos, 1);
    varExtPool := SUBSTR(inFullPath, varAtPos+1, varFileIdPos-varAtPos-1);
    -- the file name starts with the FileId
    outPath := SUBSTR(inFullPath, varFileIdPos+1);
    -- find out the dataPool Id
    SELECT ID INTO outDataPool FROM DataPool WHERE externalPool = varExtPool;
    outFileSystem := NULL;
  ELSE
    -- path starts after the second '/' from the end if we are dealing with a diskpool
    varPathPos := INSTR(inFullPath, '/', -1, 2);
    outPath := SUBSTR(inFullPath, varPathPos+1);
    -- the fileid is between last '/' and '@'
    varFileIdPos := INSTR(inFullPath, '/', -1, 1);
    outFileId := TO_NUMBER(SUBSTR(inFullPath, varFileIdPos+1, varAtPos-varFileIdPos-1));
    -- the mountPoint is between the ':' and the start of the path
    varMountPoint := SUBSTR(inFullPath, varColonPos+1, varPathPos-varColonPos);
    -- find out the filesystem Id
    SELECT FileSystem.id INTO outFileSystem
      FROM DiskServer, FileSystem
     WHERE DiskServer.name = varDiskServerName
       AND FileSystem.diskServer = DiskServer.id
       AND FileSystem.mountPoint = varMountPoint;
    outDataPool := NULL;
  END IF;
END;
/

/* PL/SQL method implementing stageForcedRm. This is executed as part of the NS synchronization. */
CREATE OR REPLACE PROCEDURE stageForcedRm (fid IN INTEGER,
                                           nh IN VARCHAR2,
                                           inGcType IN INTEGER DEFAULT NULL) AS
  cfId INTEGER;
  nbRes INTEGER;
  dcsToRm "numList";
  nsHostName VARCHAR2(2048);
BEGIN
  -- Get the stager/nsHost configuration option
  nsHostName := getConfigOption('stager', 'nsHost', nh);
  -- Lock the access to the CastorFile
  -- This, together with triggers will avoid new migration/recall jobs
  -- or DiskCopies to be added
  SELECT id INTO cfId FROM CastorFile
   WHERE fileId = fid AND nsHost = nsHostName FOR UPDATE;
  -- list diskcopies
  SELECT /*+ INDEX_RS_ASC(DiskCopy I_DiskCopy_CastorFile) */ id
    BULK COLLECT INTO dcsToRm
    FROM DiskCopy
   WHERE castorFile = cfId
     AND status IN (dconst.DISKCOPY_VALID, dconst.DISKCOPY_WAITFS,
                    dconst.DISKCOPY_STAGEOUT, dconst.DISKCOPY_WAITFS_SCHEDULING);
  -- Stop ongoing recalls and migrations
  deleteRecallJobs(cfId);
  deleteMigrationJobs(cfId);
  -- clean up any outstanding repack requests as the file is now gone
  archiveOrFailRepackSubReq(cfId, serrno.ENOENT);
  -- mark all get/put requests for those diskcopies
  -- and the ones waiting on them as failed
  -- so that clients eventually get an answer
  FOR sr IN (SELECT /*+ INDEX_RS_ASC(Subrequest I_Subrequest_DiskCopy)*/ id, status FROM SubRequest
              WHERE diskcopy IN
                (SELECT /*+ CARDINALITY(dcidTable 5) */ *
                   FROM TABLE(dcsToRm) dcidTable)
                AND status IN (0, 1, 2, 5, 6, 12, 13)) LOOP   -- START, RESTART, RETRY, WAITSUBREQ, READY, READYFORSCHED
    UPDATE SubRequest
       SET status = dconst.SUBREQUEST_FAILED,
           errorCode = serrno.EINTR,
           errorMessage = 'Canceled by another user request'
     WHERE (castorfile = cfId AND status = dconst.SUBREQUEST_WAITSUBREQ)
        OR id = sr.id;
  END LOOP;
  -- Set selected DiskCopies to INVALID
  FORALL i IN 1 .. dcsToRm.COUNT
    UPDATE DiskCopy
       SET status = dconst.DISKCOPY_INVALID,
           gcType = inGcType
     WHERE id = dcsToRm(i);
END;
/

/* Dumps the logs generated in the Nameserver DB without reqId (e.g. because of the undeleteFile() procedure) */
CREATE OR REPLACE PROCEDURE dumpNsLogs AS
  varNsHost VARCHAR2(2048);
  varReqId VARCHAR2(36);
  varNSTimeInfos floatList;
  varNSErrorCodes "numList";
  varNSMsgs strListTable;
  varNSFileIds "numList" := "numList"();
  varNSParams strListTable;
BEGIN
  varNsHost := getConfigOption('stager', 'nsHost', '');
  varReqId := uuidGen();
  -- get the current logs
  SELECT timeinfo, errorCode, msg, fileId, params
    BULK COLLECT INTO varNSTimeInfos, varNSErrorCodes, varNSMsgs, varNSFileIds, varNSParams
    FROM SetSegsForFilesResultsHelper@RemoteNS
   WHERE reqId IS NULL;
  DELETE FROM SetSegsForFilesResultsHelper@RemoteNS
   WHERE reqId IS NULL;
  -- this commits the remote deletion
  COMMIT;
  FOR i IN 1 .. varNSFileIds.COUNT LOOP
    -- Log on behalf of the NS
    logToDLFWithTime(varNSTimeinfos(i), varReqId,
                     CASE varNSErrorCodes(i)
                       WHEN 0                 THEN dlf.LVL_SYSTEM
                       WHEN serrno.ENOENT     THEN dlf.LVL_WARNING
                       WHEN serrno.ENSFILECHG THEN dlf.LVL_WARNING
                       ELSE                        dlf.LVL_ERROR
                     END,
                     varNSMsgs(i), varNSFileIds(i), varNsHost, 'nsd', varNSParams(i));
  END LOOP;
  COMMIT;
END;
/

/* update the db after a successful recall */
CREATE OR REPLACE PROCEDURE tg_setFileRecalled(inMountTransactionId IN INTEGER,
                                               inFseq IN INTEGER,
                                               inFilePath IN VARCHAR2,
                                               inCksumName IN VARCHAR2,
                                               inCksumValue IN INTEGER,
                                               inReqId IN VARCHAR2,
                                               inLogContext IN VARCHAR2) AS
  varFileId         INTEGER;
  varNsHost         VARCHAR2(2048);
  varVID            VARCHAR2(2048);
  varCopyNb         INTEGER;
  varSvcClassId     INTEGER;
  varEuid           INTEGER;
  varEgid           INTEGER;
  varLastOpenTime   NUMBER;
  varCfId           INTEGER;
  varFSId           INTEGER;
  varDPId           INTEGER;
  varDCPath         VARCHAR2(2048);
  varDcId           INTEGER;
  varFileSize       INTEGER;
  varFileClassId    INTEGER;
  varNbMigrationsStarted INTEGER;
  varGcWeight       NUMBER;
  varGcWeightProc   VARCHAR2(2048);
  varRecallStartTime NUMBER;
BEGIN
  -- get diskserver, filesystem and path from full path in input
  BEGIN
    parsePath(inFilePath, varFSId, varDPId, varDCPath, varDCId, varFileId, varNsHost);
  EXCEPTION WHEN OTHERS THEN
    -- log "setFileRecalled : unable to parse input path. giving up"
    logToDLF(inReqId, dlf.LVL_ERROR, dlf.RECALL_INVALID_PATH, 0, '', 'tapegatewayd',
             'mountTransactionId=' || TO_CHAR(inMountTransactionId) || ' TPVID=' || varVID ||
             ' fseq=' || TO_CHAR(inFseq) || ' filePath=' || inFilePath ||
             ' errorMessage="' || SQLERRM || '" stackTrace="' || dbms_utility.format_error_backtrace() ||
             '" ' || inLogContext);
    RETURN;
  END;

  -- first lock Castorfile, check NS and parse path
  -- Get RecallJob and lock Castorfile
  BEGIN
    SELECT CastorFile.id, CastorFile.fileId, CastorFile.nsHost, CastorFile.nsOpenTime,
           CastorFile.fileSize, CastorFile.fileClass, RecallMount.VID, RecallJob.copyNb,
           RecallJob.euid, RecallJob.egid
      INTO varCfId, varFileId, varNsHost, varLastOpenTime, varFileSize, varFileClassId, varVID,
           varCopyNb, varEuid, varEgid
      FROM RecallMount, RecallJob, CastorFile
     WHERE RecallMount.mountTransactionId = inMountTransactionId
       AND RecallJob.vid = RecallMount.vid
       AND RecallJob.fseq = inFseq
       AND (RecallJob.status = tconst.RECALLJOB_SELECTED
         OR RecallJob.status = tconst.RECALLJOB_SELECTED2NDCOPY)
       AND RecallJob.castorFile = CastorFile.id
       AND ROWNUM < 2
       FOR UPDATE OF CastorFile.id;
    -- the ROWNUM < 2 clause is worth a comment here :
    -- this select will select a single CastorFile and RecallMount, but may select
    -- several RecallJobs "linked" to them. All these recall jobs have the same copyNb
    -- but different uid/gid. They exist because these different uid/gid are attached
    -- to different recallGroups.
    -- In case of several recallJobs present, they are all equally responsible for the
    -- recall, thus we pick the first one as "the" responsible. The only consequence is
    -- that it's uid/gid will be used for the DiskCopy creation
  EXCEPTION WHEN NO_DATA_FOUND THEN
    -- log "Unable to identify Recall. giving up"
    logToDLF(inReqId, dlf.LVL_ERROR, dlf.RECALL_NOT_FOUND, varFileId, varNsHost, 'tapegatewayd',
             'mountTransactionId=' || TO_CHAR(inMountTransactionId) ||
             ' fseq=' || TO_CHAR(inFseq) || ' filePath=' || inFilePath || ' ' || inLogContext);
    RETURN;
  END;

  -- Deal with the DiskCopy: it is created now as the recall is effectively over. The subsequent
  -- check in the NS may make it INVALID, which is fine as opposed to forget about it and generating dark data.

  -- compute GC weight of the recalled diskcopy
  -- first get the svcClass
  IF varFSId > 0 THEN
    SELECT Diskpool2SvcClass.child INTO varSvcClassId
      FROM Diskpool2SvcClass, FileSystem
     WHERE FileSystem.id = varFSId
       AND Diskpool2SvcClass.parent = FileSystem.diskPool
       AND ROWNUM < 2;
  ELSE
    SELECT DataPool2SvcClass.child INTO varSvcClassId
      FROM DataPool2SvcClass
     WHERE DataPool2SvcClass.parent = varDPId
       AND ROWNUM < 2;
  END IF;
  -- Again, the ROWNUM < 2 is worth a comment : the pool may be attached
  -- to several svcClasses. However, we do not support that these different
  -- SvcClasses have different GC policies (actually the GC policy should be
  -- moved to the DiskPool/DataPool table in the future). Thus it is safe
  -- to take any SvcClass from the list
  varGcWeightProc := castorGC.getRecallWeight(varSvcClassId);
  EXECUTE IMMEDIATE 'BEGIN :newGcw := ' || varGcWeightProc || '(:size); END;'
    USING OUT varGcWeight, IN varFileSize;
  -- create the DiskCopy, after getting how many copies on tape we have, for the importance number
  DECLARE
    varNbCopiesOnTape INTEGER;
  BEGIN
    SELECT nbCopies INTO varNbCopiesOnTape FROM FileClass WHERE id = varFileClassId;
    INSERT INTO DiskCopy (path, gcWeight, creationTime, lastAccessTime, diskCopySize, nbCopyAccesses,
                          ownerUid, ownerGid, id, gcType, fileSystem, dataPool,
                          castorFile, status, importance)
    VALUES (varDCPath, varGcWeight, getTime(), getTime(), varFileSize, 0,
            varEuid, varEgid, varDCId, NULL, varFSId, varDPId, varCfId, dconst.DISKCOPY_VALID,
            -1-varNbCopiesOnTape*100);
  END;

  -- Check that the file is still there in the namespace (and did not get overwritten)
  -- Note that error handling and logging is done inside the function
  IF NOT checkRecallInNS(varCfId, inMountTransactionId, varVID, varCopyNb, inFseq, varFileId, varNsHost,
                         inCksumName, inCksumValue, varLastOpenTime, inReqId, inLogContext) THEN
    RETURN;
  END IF;

  -- Then deal with recalljobs and potential migrationJobs
  -- Find out starting time of oldest recall for logging purposes
  SELECT MIN(creationTime) INTO varRecallStartTime FROM RecallJob WHERE castorFile = varCfId;
  -- Delete recall jobs
  DELETE FROM RecallJob WHERE castorFile = varCfId;
  -- trigger waiting migrations if any
  -- Note that we reset the creation time as if the MigrationJob was created right now
  -- this is because "creationTime" is actually the time of entering the "PENDING" state
  -- in the cases where the migrationJob went through a WAITINGONRECALL state
  UPDATE /*+ INDEX_RS_ASC (MigrationJob I_MigrationJob_CFVID) */ MigrationJob
     SET status = tconst.MIGRATIONJOB_PENDING,
         creationTime = getTime()
   WHERE status = tconst.MIGRATIONJOB_WAITINGONRECALL
     AND castorFile = varCfId;
  varNbMigrationsStarted := SQL%ROWCOUNT;
  -- in case there are migrations, update CastorFile's tapeStatus to NOTONTAPE, otherwise it is ONTAPE
  UPDATE CastorFile
     SET tapeStatus = CASE varNbMigrationsStarted
                        WHEN 0
                        THEN dconst.CASTORFILE_ONTAPE
                        ELSE dconst.CASTORFILE_NOTONTAPE
                      END
   WHERE id = varCfId;

  -- Finally deal with user requests
  UPDATE SubRequest
     SET status = decode(reqType,
                         119, dconst.SUBREQUEST_REPACK, -- repack case
                         dconst.SUBREQUEST_RESTART),    -- standard case
         getNextStatus = dconst.GETNEXTSTATUS_FILESTAGED,
         lastModificationTime = getTime()
   WHERE castorFile = varCfId
     AND status = dconst.SUBREQUEST_WAITTAPERECALL;

  -- trigger the creation of additional copies of the file, if necessary.
  replicateOnClose(varCfId, varEuid, varEgid, varSvcClassId);

  -- log success
  logToDLF(inReqId, dlf.LVL_SYSTEM, dlf.RECALL_COMPLETED_DB, varFileId, varNsHost, 'tapegatewayd',
           'mountTransactionId=' || TO_CHAR(inMountTransactionId) || ' TPVID=' || varVID ||
           ' fseq=' || TO_CHAR(inFseq) || ' filePath=' || inFilePath || ' recallTime=' ||
           to_char(trunc(getTime() - varRecallStartTime, 0)) || ' ' || inLogContext);
END;
/

/* PL/SQL method implementing selectFiles2Delete
   This is the standard garbage collector: it sorts VALID diskcopies
   that do not need to go to tape by gcWeight and selects them for deletion up to
   the desired free space watermark */
CREATE OR REPLACE PROCEDURE selectFiles2Delete(diskServerName IN VARCHAR2,
                                               files OUT castorGC.SelectFiles2DeleteLine_Cur) AS
  dsId INTEGER;
  dpId INTEGER;
  varUnused INTEGER;
BEGIN
  BEGIN
    SELECT DiskServer.id, DiskServer.dataPool INTO dsId, dpID
      FROM DiskServer WHERE DiskServer.name = diskServerName;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    raise_application_error(-20200, 'DiskServer not found in selectFiles2Delete : ' || diskServerName);
  END;
  -- Call the dedicated procedure for our case (Regular DiskServers or DataPool)
  BEGIN
    SELECT id INTO varUnused FROM FileSystem WHERE diskServer = dsId AND ROWNUM < 2;
    -- we have filesystems, GC them
    selectFiles2DeleteDiskServer(dsId, files);
  EXCEPTION WHEN NO_DATA_FOUND THEN
    -- no filesystem, probably dealing with a datapool;
    NULL;
  END;
  -- DataPool case
  IF dpID > 0 THEN
    selectFiles2DeleteDataPool(dpId, files);
  END IF;
END;
/

/* PL/SQL method implementing filesDeletionFailedProc */
CREATE OR REPLACE PROCEDURE filesDeletionFailedProc
(dcIds IN castor."cnumList") AS
  varUnused NUMBER;
  DCLocked EXCEPTION;
  PRAGMA EXCEPTION_INIT (DCLocked, -54);
BEGIN
  -- Loop over the files
  FOR i IN 1..dcIds.COUNT LOOP
    BEGIN
      -- try to lock the next file
      SELECT id INTO varUnused FROM DiskCopy
       WHERE id = dcIds(i) FOR UPDATE NOWAIT;
    EXCEPTION
      WHEN DCLocked THEN
        -- lock already taken by someone else
        -- we commit our current locks so to avoid
        -- dead locks and retry without NOWAIT
        COMMIT;
        BEGIN
          SELECT id INTO varUnused FROM DiskCopy
           WHERE id = dcIds(i) FOR UPDATE;
        EXCEPTION WHEN NO_DATA_FOUND THEN
          -- it just disappeared, keep going
          CONTINUE;
        END;
      WHEN NO_DATA_FOUND THEN
        -- this diskCopy had disappeared in the mean time, keep going
        CONTINUE;
    END;
    -- we have the lock, perform the update
    UPDATE DiskCopy SET status = 4 -- FAILED
     WHERE id = dcIds(i);
  END LOOP;
  COMMIT;
END;
/

/* prechecks common to the user-related insert*Request methods (all except GC-related ones) */
CREATE OR REPLACE FUNCTION insertPreChecks
  (euid IN INTEGER,
   egid IN INTEGER,
   svcClassName IN VARCHAR2,
   reqType IN INTEGER) RETURN NUMBER AS
  reqName VARCHAR2(100);
BEGIN
  -- Check permissions
  IF 0 != checkPermission(svcClassName, euid, egid, reqType) THEN
    -- permission denied
    SELECT object INTO reqName FROM Type2Obj WHERE type = reqType;
    raise_application_error(-20121, 'Insufficient privileges for user ' || euid ||','|| egid
        ||' performing a '|| reqName ||' request on svcClass '''|| svcClassName ||'''');
  END IF;  
  -- check the validity of the given service class and return its internal id
  RETURN checkForValidSvcClass(svcClassName, 1, 1);
END;
/

/* inserts Files2Delete Requests in the stager DB */   
CREATE OR REPLACE PROCEDURE insertFiles2DeleteRequest
  (machine IN VARCHAR2,
   euid IN INTEGER,
   egid IN INTEGER,
   pid IN INTEGER,
   userName IN VARCHAR2,
   reqUUID IN VARCHAR2,
   reqType IN INTEGER,
   clientIP IN INTEGER,
   clientPort IN INTEGER,
   clientVersion IN INTEGER,
   clientSecure IN INTEGER,
   diskServerName IN VARCHAR2) AS
  reqId NUMBER;
  clientId NUMBER;
  creationTime NUMBER;
BEGIN
  -- get unique ids for the request and the client and get current time
  SELECT ids_seq.nextval INTO reqId FROM DUAL;
  SELECT ids_seq.nextval INTO clientId FROM DUAL;
  creationTime := getTime();
  -- insert the request itself
  CASE
    WHEN reqType = 73 THEN -- Files2Delete
      INSERT INTO Files2Delete (flags, userName, euid, egid, mask, pid, machine, reqId, creationTime, lastModificationTime, diskServer, id, client)
      VALUES (0,userName,euid,egid,0,pid,machine,reqUUID,creationTime,creationTime,diskServerName,reqId,clientId);
    ELSE
      raise_application_error(-20122, 'Unsupported request type in insertFiles2Delete : ' || TO_CHAR(reqType));
  END CASE;
  -- insert the client information
  INSERT INTO Client (ipAddress, port, version, secure, id)
  VALUES (clientIP,clientPort,clientVersion,clientSecure,clientId);
  -- insert a row into newRequests table to trigger the processing of the request
  INSERT INTO newRequests (id, type, creation) VALUES (reqId, reqType, to_date('01011970','ddmmyyyy') + 1/24/60/60 * creationTime);
  -- send an alert to accelerate the processing of the request
  alertSignalNoLock('wakeUpGCSvc');
END;
/

/* inserts GC Requests in the stager DB
 * This handles NsFilesDeleted, FilesDeleted, FilesDeletionFailed and StgFilesDeleted 
 * requests.
 */
CREATE OR REPLACE PROCEDURE insertGCRequest
  (machine IN VARCHAR2,
   euid IN INTEGER,
   egid IN INTEGER,
   pid IN INTEGER,
   userName IN VARCHAR2,
   reqUUID IN VARCHAR2,
   reqType IN INTEGER,
   clientIP IN INTEGER,
   clientPort IN INTEGER,
   clientVersion IN INTEGER,
   clientSecure IN INTEGER,
   nsHost IN VARCHAR2,
   diskCopyIds IN castor."cnumList") AS
  reqId NUMBER;
  clientId NUMBER;
  creationTime NUMBER;
  gcFileId INTEGER;
BEGIN
  -- get unique ids for the request and the client and get current time
  SELECT ids_seq.nextval INTO reqId FROM DUAL;
  SELECT ids_seq.nextval INTO clientId FROM DUAL;
  creationTime := getTime();
  -- insert the request itself
  CASE
    WHEN reqType = 142 THEN -- NsFilesDeleted
      INSERT INTO NsFilesDeleted (flags, userName, euid, egid, mask, pid, machine, reqId, creationTime, lastModificationTime, nsHost, id, client)
      VALUES (0,userName,euid,egid,0,pid,machine,reqUUID,creationTime,creationTime,nsHost,reqId,clientId);
    WHEN reqType = 74 THEN -- FilesDeleted
      INSERT INTO FilesDeleted (flags, userName, euid, egid, mask, pid, machine, reqId, creationTime, lastModificationTime, id, client)
      VALUES (0,userName,euid,egid,0,pid,machine,reqUUID,creationTime,creationTime,reqId,clientId);
    WHEN reqType = 83 THEN -- FilesDeletionFailed
      INSERT INTO FilesDeletionFailed (flags, userName, euid, egid, mask, pid, machine, reqId, creationTime, lastModificationTime, id, client)
      VALUES (0,userName,euid,egid,0,pid,machine,reqUUID,creationTime,creationTime,reqId,clientId);
    WHEN reqType = 149 THEN -- StgFilesDeleted
      INSERT INTO StgFilesDeleted (flags, userName, euid, egid, mask, pid, machine, reqId, creationTime, lastModificationTime, nsHost, id, client)
      VALUES (0,userName,euid,egid,0,pid,machine,reqUUID,creationTime,creationTime,nsHost,reqId,clientId);
    ELSE
      raise_application_error(-20122, 'Unsupported request type in insertGCRequest : ' || TO_CHAR(reqType));
  END CASE;
  -- insert the client information
  INSERT INTO Client (ipAddress, port, version, secure, id)
  VALUES (clientIP,clientPort,clientVersion,clientSecure,clientId);
  -- Loop on diskCopies
  FOR i IN 1..diskCopyIds.COUNT LOOP
    -- get unique ids for the diskCopy
    SELECT ids_seq.nextval INTO gcFileId FROM DUAL;
    -- insert the fileid
    INSERT INTO GCFile (diskCopyId, id, request)
    VALUES (diskCopyIds(i), gcFileId, reqId);
  END LOOP;
  -- insert a row into newRequests table to trigger the processing of the request
  INSERT INTO newRequests (id, type, creation) VALUES (reqId, reqType, to_date('01011970','ddmmyyyy') + 1/24/60/60 * creationTime);
  -- send an alert to accelerate the processing of the request
  alertSignalNoLock('wakeUpGCSvc');
END;
/

/* Recompile all invalid procedures, triggers and functions */
/************************************************************/
BEGIN
  recompileAll();
END;
/

/* Flag the schema upgrade as COMPLETE */
/***************************************/
UPDATE UpgradeLog SET endDate = systimestamp, state = 'COMPLETE'
 WHERE release = '2_1_16_9';
COMMIT;
