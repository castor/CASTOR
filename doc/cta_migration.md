CASTOR to CTA Production Migration
==================================

_Version 0.8 - December 5, 2019_

Motivation
----------
Operating a critical production service like CASTOR with minimal cost (<1 FTE, including the rota) has been achieved for the last 3+ years by freezing the system and working around any issue that would have required dedicated software or service developments, on the grounds that the service would be replaced in a timeframe corresponding to the LS2.

Therefore, the objective of this document is to provide a viable plan to migrate **ALL CASTOR instances and users to CTA before LHC Run3**.

It is assumed that the ongoing negotiations with the LHC experiments are on track to get them migrated before LHC Run3. If it turns out that some LHC experiment could not be migrated, this entire migation plan is to be revisited.

This document will focus in particular on the migration of `castorpublic`, exploiting the peculiarities of the SME (Small-Medium Experiments) communities.

The envisaged `eosctapublic` service detailed below covers all their use-cases by proposing a full drop-in replacement of CASTOR, as opposed to what is being proposed to the LHC experiments. In addition, appropriate configuration can be put in place in order to perform the migration **without needing to contact each of the several users** and VOs using `castorpublic`.


Targeted communities and usage analysis
---------------------------------------

The SME communities to be targeted are the following:
* AMS
* AWAKE
* COMPASS
* ILC/CLIC
* NA61
* NA62
* n_ToF
* ProtoDUNE

Following discussions at recent SME/IT meetings, the largest VOs are all ready to be migrated:
* AMS: use CASTOR as backup/archive, ready for CTA
* COMPASS: use FTS, mostly ready for CTA except for reprocessing
* ILC/CLIC: use FTS, ready for CTA
* NA62: use FTS for DAQ (they prototyped it), asked to use CTA; mostly ready
* ProtoDUNE: use FTS, ready for CTA

In addition, the following activities are currently supported by the `castorpublic/backup` pool. As they are all xrootd-based with a well-defined workflow, they are by definition ready for CTA:
* AFS backup
* EOS metadata backup
* Hadoop HDFS backup
* NFS (Filers) backup
* MIC (Microelectronics group) backup/archive
* Mailboxes backup
* Web archives

On the other side, the "tail" of smaller VOs is composed as follows:
* AWAKE: no information available.
* NA61: no information available.
* n_ToF: use their own xrootd-based DAQ and reprocessing, do not use EOS at all.
* Others: about **260 distinct CASTOR users** having accessed `castorpublic` less than ~25 times in the past year-and-half. This even includes access to legacy LEP data. Write activity is about **40%** of the read activity.

To be noted that this tail includes "HSM-like" workflows from users that generally do NOT have access to high-level data management frameworks like Rucio, and use a single storage element for simplicity. There are about 10 power-users, who regularly (>50% of distinct days in the past year-and-half) access `castorpublic` for data analysis and reprocessing.

It is also to be noted that while this tail is rather large and diverse in terms of number of distinct users and use-cases, its generated traffic is marginal compared to the total traffic supported by `castorpublic`: as a measure of its usage as of mid October 2019, **80%** of the disk buffer is occupied by large VOs' data, **12%** by backup data, with the tail totalling **8%** of disk usage (cf. [Grafana monitoring](https://monit-grafana.cern.ch/d/000000889/castor-activity?orgId=40&from=now-30d&to=now&refresh=5m&fullscreen&panelId=55)).

The "tail effect" in `castorpublic` usage is observed also when looking at SRM and GridFTP:

* SRM serves a large number of external clients (1.5K distinct IPs in 15 days). Non-FTS clients represent **26%** of the incoming requests. Such requests largely correspond to "HSM-like" workflows (`srmBringOnline`, `srmPrepareToGet`), which can be mapped to xrootd-based commands supported by CTA.

* GridFTP transfers served by `castorpublic` primarily target CERN IP addresses (87% of the requests in 15 days). The remaining 13% of the requests target 1.3K non-CERN IPs (**79%** of all distinct IPs), which are NOT expected to support xrootd as an alternative protocol in the immediate future (This depends on the evolution of the DOMA TPC working group). Therefore, _GridFTP support is a requirement for `eosctapublic`_ in the context of a full drop-in replacement of CASTOR.

In all the above, "HSM-like" means that users expect to pre-stage their files and subsequently access them on the disk cache hours or days later, without transferring them to a separate EOS disk instance. At the same time, current `castorpublic` users are expected to pre-stage files with `xrdfs prepare`, poll for the files to be online, and then access them via `xrdcp` or the xrootd API. They are NOT allowed to synchronously stage from tape and read files with `xrdcp`, as `xrdcp` of a _nearline_ file fails right away.

For reference, some pre-digested logs of the above analysis are available at https://cernbox.cern.ch/index.php/s/LQHf6YC1tOkPhLZ


The Plan
--------

A viable plan for the `castorpublic` migration is as follows:
* Up to end of Nov 2019: TAB to gather input from ALICE and LHCb for the feasibility of their migration to CTA.

* **Dec 2019/Jan 2020: checkpoint meeting between FDO and TAB and GO/NO GO decision for this plan, if all LHC experiments are on track for migration before LHC Run3.**
    * In the eventuality that not all LHC experiments are on track, an appropriate plan B must be devised, which must include a significant rehauling of CASTOR (cf. the _Alternate plan_ section below).

* Jan 2020: setup a standard SSD-based `eosctabackup` instance, to replace the current `castorpublic/backup` pool.
    * See the _Alternate plan_ below for details if this cannot happen.

* Jan 2020: setup an `eosctapublic` instance based on **spindles** as opposed to fast SSDs, and expose it to early users (e.g. NA62, AMS) + advertise it to all other SMEs for them to prepare the migration. The bottom-line message would be _"You do NOT need to change your workflows, but if you do (in particular using FTS), you'll get the best out of the system"_.
    * Headnodes for the EOS MGM and QuarkDB **to be setup** out of the recently-received hardware.
    * The required diskservers could be taken out of the current CASTOR instances, but in a RAID-0 configuration as opposed to the current RAID-6 one.
    * EOS should be run with 2 replicas, given the age and higher failure rate of HDs vs. SSDs.
    * "Open" authz/authc permission model to be configured (not like the LHC instances).
    * **GridFTP gateways** need to be provisioned, targeting about 1 GB/s of bandwidth to start with. If large VOs are encouraged to switch to xrootd at least for local traffic, this requirement may get relaxed.
    * Performance penalty from the tape servers in accessing such a disk buffer is expected to be negligible: the current tape pool policies in `castorpublic` do not allow more than **4** concurrent tape drives for the most demanding VOs (COMPASS, NA62, ProtoDUNE), **5** for AMS because of their one-shot, archive-like workflow. Today, this is easily and flawlessly sustained by spindle-based disk servers in CASTOR (the `castorpublic/cdr` pool runs on **13** diskservers).

* Early 2020: run a Data Challenge with ideally all LHC experiments, or at least 3 of them, to validate all their workflows.

* Q1 2020: `castoratlas` and `castorcms` migration to CTA (TBC).

* End Q1 2020: start `castorpublic` migration.
    * Inject more hardware to `eosctapublic`, possibly reusing diskservers from decommissioned CASTOR instances. Ideally, the capacity currently deployed in `castoratlas` and `castorcms` would provide `eosctapublic` with 3.7 PB of usable space, 100% spindle-based, which is far sufficient for supporting all public activities.
    * Migrate some larger SMEs to `eosctapublic` (hopefully after they have run some tests, but that's not a pre-condition).
    * Decommission `castorpublic/backup` and retire its hardware (`lxfs*` nodes).
    * `eospublic` might need to be refurbished to support the additional usage induced by those SMEs that will include it in their DAQ workflow. However, only AMS is projected to take data in 2020.

* Early Q2 2020: migrate ALL `castorpublic` files and users to `eosctapublic`.
    * The CTA GC policy shall support typical "HSM-like" (in the sense detailed above) reprocessing use-cases that come from general `castorpublic` users, ensuring multi-day persistency on the disk cache.
    * To accommodate all "non-CTA-ready" VOs and users, an **xrootd-based redirection** may be put in place on `castorpublic` for transparent access to `eosctapublic`. The required namespace mapping from `root://castorpublic//castor/cern.ch/my/file` to `root://eosctapublic//eos/ctapublic/my/file` is easily configurable in xrootd.
    * Instructions on how to use `xrdfs stat` or `eos ls` as opposed to `nsls` are to be widely exposed (KB articles, ITUM, etc.). Reminder: the CASTOR Nameserver cannot be partially black-listed.
        * Possibly, a special version of `nsls` could be deployed to return a dedicated error message suggesting to use `xrdfs stat` or `eos ls`.

* Q2 2020: decommission all other `castorpublic` diskservers, including the Ceph `erin` cluster. Most hardware is to be retired. Leave redirection in place, possibly configured on the `eosctapublic` head node.

* Q3 2020: `castoralice` and `castorlhcb` migration to CTA (TBC), and decommissioning of all CASTOR instances. Part of the hardware may be reinjected in the EOS disk instances.
    * The CASTOR Nameservers are kept in place for the foreseeable future, but external user access is fully blocked.

* 2021+: TAB may wish to contact all "public" users to progressively adopt the new EOS-centric workflow and use `eospublic` and FTS as opposed to direct accessing the staging area in `eosctapublic`. This is anticipated to take a pretty long time.
    * The switch to an SSD-only deployment for `eosctapublic` is likely to only make sense once the majority of the public VOs use the new workflow, and at the same time the minority can live with a small buffer without significant impact.


Alternate plan: run CASTOR during Run3
--------------------------------------

If for any reasons one or more CASTOR instances need to run through Run3, this can be made possible at a cost that is anticipated to require not less than 2 FTEs. Such significant manpower investment would be required to compensate the accumulated technical debt and bring the service back to an appropriate level of operability.

A non-exaustive list of required tasks follows:
* Refresh the operations procedures, cleaning up old/obsoleted ones, and keep them up to date.
    * This includes procedures for the Repair team:with the loss of the SysAdmins, any RAID-related issue is just not followed up, and knowledge about sw-RAID setups is rapidly fading as the group runs JBODs for all services. Cf.  INC1862320, INC1860546, INC2301429.
* Retrain the rota team to regain independence/dependability.
* Refresh the hardware.
    * The entire pool supporting backups is due to retirement by **March 2020**. If an `eosctabackup` instance cannot be setup on time, either SSDs are to be provisioned in `castorpublic/backup` or some other old hardware is to be moved in. It is NOT an option to provision new standard diskservers here as 10+ PB raw would need to be "stolen" from the experiments' pledges in order to meet the required bandwidth of the backup activities.
    * The Ceph pool currently run on 5-7 years old hw, due to retirement by **2020** (exact dates not yet known).
    * Draining campaigns requiring ops follow-ups.
    * Automated installation of new CASTOR boxes likely not working any longer, as it relies on infrastructural components that keep changing over time. Past experience shows that adjustments had to be done at every procurement cycle, and last time a CASTOR box has been installed dates back in 2017.
* Support xrootd third-party copy evolutions within WLCG DOMA.
    * VOs like ProtoDUNE, AMS, COMPASS do use WLCG and will need xrootd TPC in its latest incarnation. Though X509 (grid) based authentication is expected to work, it would be the only Grid-level authentication method supported by CASTOR.
* Refresh the software.
    * A number of bugs around xrootd handling and scheduling need developer-level effort.
        * Complete list available in [Jira](https://its.cern.ch/jira/issues/?filter=16213) (though that includes a number of non relevant legacy tickets).
    * The Ceph libraries evolve and will likely require fixes in the code if Ceph versions > 14 are to be supported. The current Ceph long-term supported version is 14 (aka Nautilus).
    * ~~The python-Oracle bindings library has been version-locked to an old release from years ago, because of yet-to-be-debugged incompatibilities with the CASTOR scheduling system.~~ _This has been done in CASTOR version 2.1.19-2_.
* Refresh the CASTOR web site. It currently runs on Drupal 7, which is expected to be terminated in June 2020.
* Keep the central CASTOR Nameserver daemons running and openly accessible until the last CASTOR user is not migrated: this is going to be a likely source of confusion for many users if the migration is delayed over a long period of time, as there is no provision in CASTOR to protect/blacklist only part of the CASTOR Namespace.
* Keep two tape systems running and competing for the same (hw AND ops!) resources, which will be in high demand during data-taking activities.


CASTOR User spaces and OC11/DP
==============================

CASTOR used to provide users with user ("home"-like) spaces located at e.g. `/castor/cern.ch/user/j/joe`. While in the past this has been heavily used to store Physics-related derived data of various forms, the usage of those spaces has lowered to a minimum today, as experiments generally use other areas in the namespace. To be noted that LHC users do not have access to CASTOR for several years, and other users typically use different resources, with less than **5%** of user spaces created in 2018 having ever been used. The user areas have been "quarantined" to a protected area once the corresponding account does not exist any longer in LDAP.

Given the current usage and the new SLD for CTA, the new OC11 coming to force in 2020, and Data Protection issues with the current data stored in those user spaces, it is proposed to stop creating user spaces for new accounts, and freeze all existing data, including the quarantined part, **without deleting it**. The data shall be moved to the relevant experiments part of the namespace for the LHC accounts, and to a common legacy area for all other accounts. This will ease the migration to CTA as there won't be any need to clone multiple times the entire `/castor/cern.ch/user` tree to all CTA instances.

More in details:

* Dec 2019: announce the service change in ITSSB and stop creating user spaces in CASTOR.
    * This was done by stopping the related acron job. SSB at https://cern.service-now.com/service-portal/view-outage.do?n=OTG0053583.
* Early 2020:
    * Delete all empty (i.e. typically never used) user directories.
    * Move all LHC-related user areas (both "active" and "quarantined") to a suitable place in the CASTOR namespace, for instance `/castor/cern.ch/<experiment>/castoruser`, in read-only mode for data preservation. The split is driven by the fileclass of the user top-level folder.
        * In the unlikely case that inner files belong to different fileclasses, they should be changed and a repack be executed to fully disentangle them.
* At a later time: evaluate whether to block access to the remaining `public_user` user spaces, even after having migrated them to CTA. Those user spaces are still actively accessed in r/w mode by non-LHC experiments, therefore a freeze may not be possible before the migration to CTA.
