The Cern Advanced STORage manager (CASTOR) is a modular Hierarchical
Storage Management (HSM) system designed to handle tens of millions of
files (with sizes in the megabyte to gigabyte range) which result in
an aggregated storage capacity of tens of petabytes of tape archive
and petabytes of disk storage.

CASTOR builds on SHIFT (Scalable Heterogeneous
Integrated FaciliTy) system, which provided users with access
to the CERN tape system but had limited scalability, up to
approximately 10,000 files and 10MB/sec of data throughput. The
first version of CASTOR had a greater scalability than SHIFT by
several orders of magnitude, handling several million files on tape
and 200,000 files in its disk cache. Although this proved adequate
for LEP era computing it could not provide the performance or
scalability necessary for LHC era computing requirements \cite{castor}.

The CASTOR2 project started in 2003 and aimed at providing 
the data storage capacity and performance for managing the data
produced by the LHC experiments. These data
will be processed globally using the LHC Computing Grid\cite{LCG}.

The main data store, called Tier 0, needs to store all data coming from
the LHC experiments, i.e. ATLAS, ALICE, CMS and LHCb, and to run the
initial data reconstruction. CASTOR provides a \textit{Central Data 
Recording} facility (CDR) and storage for the 
associated \textit{Reconstruction} facilities. CASTOR handles
data transfers to the Tier 1 sites, where the data are replicated and
further reconstruction and analysis take place.
Finally, many Tier 2 sites are linked to each Tier 1 and act as data customers
for the physics analysis data. Figure \ref{fig:rates} gives a
graphical view of the different concurrent activities
CASTOR is handling.

\begin{figure}[htbp]
\centering
\includegraphics[scale=.18]{rates.eps}
\caption{CASTOR data rates}
\label{fig:rates}
\end{figure}

%-----------------------------------------------------------------------

CASTOR~2 was deployed in production at CERN in 2006. It
currently (July 2007) manages 73.43 million files, requiring
over 7.5~PB of tape and a disk cache of over 1.5~PB.
Figure \ref{fig:timeevol} shows how the CASTOR namespace has grown over time at
CERN. CASTOR is used as well at several other high
energy physics institutes around the world, notably
ASGC\footnote{Academia Sinica Grid Computing,
Taipei, Taiwan
%http://www.twgrid.org/
}, INFN\footnote{Istituto Nazionale di Fisica Nucleare,
Bologna, Italy
%http://www.\-cnaf.\-infn.it/
}, RAL\footnote{Rutherford Appleton Laboratory,
Didcot, United Kingdom
%http://hepwww.\-rl.\-ac.\-uk
} and IHEP\footnote{Institute for High Energy Physics,
Moscow, Russia
%http://www.ihep.su/index.html
}.
These institutes have deployed
or are in the process of deploying CASTOR~2.

\begin{figure}[htbp]
\centering
\includegraphics[scale=.9]{stats.eps}
\caption{Time evolution of CASTOR data storage at CERN}
\label{fig:timeevol}
\end{figure}

Outside the particle physics domain, CASTOR is used at INFN to store
data from astrophysics projects, and at CERN for satellite data from
UNOSAT and data from various computer science projects.

In this paper we present the operational aspects of
running a CASTOR instance and the performances
achieved by the current version, both in a production environment and
during internal tests. We conclude with a brief outline of our roadmap
for the continued development of the system and the enhancements we plan
to provide.

%-------------------------------------------------------------------------

\subsection{Related Work}

Of other storage solutions \cite{EDGStorage}, the High Performance Storage System
\cite{HPSS} is the most comparable to CASTOR.

HPSS provides HSM and archive services. It is a joint development by IBM and five US Government
laboratories. Its design follows the IEEE Mass Storage Refererence Model and
allows data to be moved from an intelligent disk or tape controller to the client.
All metadata information is kept on a RDBMS-based metadata engine, while movers
control the raw data transfer from heterogeneous hardware.

HPSS was used at CERN in late 1990s. It was not adpoted for LHC
computing because at the time the system was optimized for serial access
to large files, but it did not have the performances required for random access.
Because it is a collaborative effort, it was not possible to guarantee
development would meet our objectives, and an in-house solution was considered preferable.
