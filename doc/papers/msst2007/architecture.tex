The CASTOR system exposes a global hierarchical namespace that allows
users to name files in a UNIX like manner. It provides transparent tape
storage and automated disk cache management to ensure performance and reliability.
%from both the user and the tape storage point of view.
Hence, given the requirements
depicted in Figure~\ref{fig:rates}, the scalability
and reliability needs have a large impact on the architecture.
%Both suppose the ability to parallelize processing on several nodes, for
%fault tolerance and for load balancing purposes.
Reliability imposes strong constraints on the consistency
of the system state (e.g. the disk cache state), especially in case of crashes.
Scalability adds constraints on the size.
CASTOR~2 architecture is thus database centric, with a number
of stateless daemons, as shown in Figure \ref{fig:architecture}.

\begin{figure}[htbp]
\centering
\includegraphics[scale=.18]{architecture.eps}
\caption{The CASTOR system architecture}
\label{fig:architecture}
\end{figure}

A relational database contains the system state, as well as all 
requests and their status. All daemons continuously
query the database for the next operation to perform, e.g. schedule
the next transfer for a client, issue a tape recall or garbage collect
some filesystem. This design allows for a number of key features, including
easy scalability and better fault tolerance by replicating the daemons for
the same component on different machines, and simplified operation
by allowing the updating and restarting of daemons while
another instance is supporting the load.

With clusters of hundreds of disk servers using thousands of
commodity disks, the management of storage resources increasingly
faces the same issues and challenges as the management of standard
computing resources. Therefore, the CASTOR system is viewed
as a Storage Resource Sharing Facility, where load has to be properly
scheduled on the available resources. 
CASTOR benefits from the existing tools by 
externalizing scheduling activities, as well as most of the
decision making processes, where policies can be easily
plugged in a variety of scripting languages. Typical examples
are migration/recall policies and file replication policy.

A large project like CASTOR needs to be developed according to a
robust software process.
The UML methodology is used at all levels, from describing the workflow to
designing the different components.
UML modelling has also allowed to use automated code generation for
services like a database abstraction layer, and streaming of
objects for the interprocess data exchange. Standard design patterns
like \emph{Abstract interfaces}, \emph{Pluggable services} and \emph{Factories}
are heavily used, allowing for easy code evolution.

The rest of this section deals with the most important CASTOR components, the disk
cache layer and the tape archive layer.

\subsection{Disk Cache Management}

In the management of large disk caches, the primary challenge
is to define efficient garbage collection policies in order
to optimize cache usage and avoid inefficient recalls from tape.
However, handling user requests, and especially
scheduling their accesses to the disks, is also of primary
importance if good bandwidth is to be achieved. This
implies proper monitoring of the disk status which must be fed to the scheduling
system. All these activities are implemented in CASTOR 2 as separate
components.

The disk cache management layer is also responsible for interaction with clients.
Clients first interact with the {\bf request handler}, a very light weight gateway that
only stores requests in the central database. CASTOR has
demonstrated the ability to manage peaks of more than 100 requests per second without
service degradation.

In the context of Grid applications, clients may not interact directly with the request
handler but rather go through the more generic SRM interface. The SRM specifications have
evolved over time as a worldwide effort. As of July 2007 SRM version 1.1 is the production
version. The CASTOR implementation has been proved to scale up to 1.7 million requests/day.
The new version 2.2 of the SRM interface has been implemented for CASTOR and is being
tested. For further details, please refer to \cite{srmmsst07}.

The CASTOR system supports a number of file transfer protocols. These can either
be tightly or loosely integrated with CASTOR. They are respectively called
internal and external protocols.
Internal protocols deal with CASTOR transfer URLs
(e.g. \texttt{protocol://disk\-server:port\-//castor/...}) and will
internally connect to the CASTOR core services to gather metadata information.
The transfer will then take place using the native protocol in the CASTOR
disk cache. CASTOR internal protocols are RFIO, ROOT\cite{root},
XROOT\cite{xroot}, and GridFTP v2\cite{globus}.
External protocols do not support CASTOR URLs, nor contact
the CASTOR core services directly. For these protocols the client contacts a gateway
machine running a modified version of the external protocol daemon. This will
request the data from CASTOR using one of the internal protocols (usually RFIO).
GridFTPv1 falls into this category.

%Each protocol may be better under certain
%circumstances. RFIO is the original native CASTOR protocol, and provides a POSIX
%compliant file I/O API. The ROOT protocol is more efficient when handling ROOT files.
%XROOT reduces to the very minimum the file opening latency via a sophisticated
%load balancing and redirection system. Finally, GridFTP, in conjunction with the
%SRM interface, enables Grid users to use CASTOR in a world wide Grid environment.

The {\bf stager} is the daemon responsible for handling user requests. Like all
CASTOR daemons, it is stateless and relies on the central database.
Different services are defined for handling different types of
requests. They are implemented using separate thread pools in order
to decouple the different loads. The stager is also responsible
for the management of file replication within the disk cache.

The {\bf resource monitoring} daemon collects monitoring information from
the available disk servers e.g. CPU load and number of I/O streams.
This information is used as input to the I/O scheduling system.
An external scheduler is responsible for implementing the resource
sharing and scheduling\cite{msst04} in order to optimize hardware usage
and avoid overloading. CASTOR currently supports two different
schedulers: MAUI~\cite{MAUI}, an open source solution for small
sized clusters, and LSF \cite{LSF}, a commercial
scheduling facility usually used for large CPU farms.
At CERN, the size of our resources demands the use of LSF.

The {\bf Nameserver} provides the CASTOR namespace, which presents
CASTOR files under the form of a hierarchical filesystem rooted
with ``castor'' followed by the domain, e.g. \texttt{/castor/cern.ch}
or \texttt{/castor/cnaf.infn.it}.
The next level in the hierarchy usually identifies the hostname (or alias) of the
node with a running instance of the CASTOR Name Server. The naming of
all directories below the third level hierarchy is entirely up to the
service administrator managing the sub-trees.

The {\bf Distributed Logging Facility} (DLF) provides a centralized recording of
logging and accounting information from multiple machines and
an intuitive and easy-to-use web interface to browse the logging data.
This is essential within a distributed architecture to
diagnose and understand problems as well as to compute statistics
about the system. \label{DLF}
The DLF framework consists of a central server, which can process
thousands of messages a second, and a client-side API, widely used
in all CASTOR components. The server stores all the data collected
in a relational database.

Garbage collection in the disk cache is mostly implemented as a
database job which selects, according to dynamic policies, the
files to be removed from cache. An external daemon can then query
the database for files to delete and effectively remove them.
As an example, typical policies used at CERN includes deletion
of all migrated files (Data recording mode) and deletion of old
and unused files (analysis facility).

\input{tape.tex}
