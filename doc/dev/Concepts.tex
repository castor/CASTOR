%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                      Concepts.tex
%
% This file is part of the Castor project.
% See http://castor.web.cern.ch/castor
%
% Copyright (C) 2003  CERN
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
%
%
% Concept chapter of the castor developer guide.
%
% @author Castor Dev team, castor-dev@cern.ch
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter{Concepts}

\section{Object orientation}

The Castor 2 framework is based on object oriented concepts, essentially
based on \cpp and Java languages. The main concepts we deal with are the following :

\begin{description}
\item{\bf Interfaces} An interface definition a set of methods declarations
with a well defined signature and expected behavior. An Interface contains
no code, that is no implementation, it is purely declarative.
In Java, interfaces exist natively, in \cpp, this concept maps
to classes that have no members and all their methods pure virtual.
\item{\bf Classes} A class is a kind of structure with associated methods.
The items in the structure are called members of the class.
A Class is actually only a type, that is the abstract declaration
(and implementation) of the struct and its associated methods. It does
not exist in memory by itself.
\item{\bf Objects} An Object is an instance of a class, that is the
memory representation of a Class. When an object is instantiated,
each member of its class gets a value.
\item{\bf Abstract classes} An abstract class is a class where some of the
methods have been left unimplemented. An abstract class can never be
instantiated which means that the class of an object is never abstract.
In \cpp, this means that at least one method was defined as = 0 in the
header file.
\item{\bf Inheritance} A given class may inherit from another (single) class
and from any number of interfaces. When a class inherits from a given
interface, it has to implement all methods of this interface unless it is
abtract. When a class inherits from another class, it inherits all the members
of its parent as well as its methods.
Java exactly matches this view of inheritance. \cpp has a wider concept
where classes may inherit from multiple parent classes. We do not
use this feature in castor.
\end{description}


\section{Data objects and stateless services}

Among all classes, you could distinguish two extreme cases : classes
with no members (and only methods) and classes with no methods
(and only members). Classes with no methods are actually structs,
that is pure data containers. Classes with no members are pure
code containers and are completely stateless, meaning that their
state does not change after any call to one of their methods (since
they have no members and thus no state).

Without going to the extreme of having either no members or no methods,
Data Objects and Stateless Services follow the same track :
\begin{description}
\item {A \bf Data Object} is a class that does not contain any ``clever''
functionnality. The only code present in such a class is dedicated
to data access (like setters and getters) and potentially to very
simple data output.
\item {A \bf stateless Service} is a class that provides some functionnality
(often implementing some well defined interface) but has no state from
the caller point of view..
Having no state from the caller point of view does mean having no state
in general and hence no member. As an example, a database interface
service may hold a dababase handler, and as such a state (connected or not).
But it could be stateless for the user in the sense that whenever
a database connection will be used, it will find one, be it already
present, or should it be created.
\end{description}

Note that in many cases the methods of a stateless service are taking
as first argument a kind of context object which serves as current state
of the service. The context is quite often a data object.


\section{Dynamically loaded services and factories}

\subsection {Factories}

Interfaces exist to be implemented. They start to be really useful
when they are implemented more than once, e.g. a database interface
could be implemented for several databases backends or a logging
interface could be implemented once to log into a file and another
time to log into a database. In theory, the nice thing is that the
caller of the interface does not need to change any code for having
one or the other behavior.

But the practice is slighly different since the client has to
instantiate the service before calling it.
Even if it will use the interface for the call, it has to use the
actual class for the instantiation. This means that the class name
is hardcoded into the code and implies a recompilation each time you
change it, that is each time you change implementation of the service
you want to use.

The way to avoid this problem is to use factories. These are small
objects which purpose is to instantiate a given service. All factory
objects are instances of the same class although they will instantiate
different objects, that is potentially different implementation of
a given service. Now the client code only has to call the right factory
to get the right service.
In order to achieve that, each factory registers itself in a central
list, together with an id describing which class is will instantiate.

So the client code for getting a given implementation of a service
will look like this :
\begin{itemize}
\item choose the implementation to use, and take its id (typically
this is done by reading an external config file)
\item get the correponding factory from the list of registered factories
\item call the instantiate method on it
\item use the service
\end{itemize}

Note that the factories are static objects that are instantiated when
the program is loaded into memory. The instantiation of a factory calls
its constructor, which registers the factory the the central list.

\subsection{dynamic libraries}

Factories, as presented in the previous subsection help to remove any
code that is dependent of a given service implementation from the client.
However, the problem of linking the executable is not solved : all the
possible implementations of a given sevrice should be included in the
executable if we want the client to be able to use them.

This can be very tedious. Imagine a case where the client uses a database
related service with different databases backend. You cannot expect a given
client instance to be able to load at starting time the libraries of
all databases, even the ones that he will not use. He may just not have
the right libraries.

The way to avoid the link problem is to use dynamic libraries. The different
implementations of a given service should be included in different
libraries and only the right one should be loaded at run time.
In the C world, this is using the \verb1dlopen1 interface.
Using the previous factory approach, the factories of the services
defined in a given library will register themselves when the library
will be loaded and the services will be immediately available to the
clients.

To make it fully dynamic, one can even give the name of the library
to be loaded for a given service into a config file. The system will
then automatically load the right library if it founds no appropriate
factory for a given service.


\section{Conversion services and converters}

Conversion services are a special kind of services that allow
conversion of data from/to a given representation to/from memory
(that is to/from a data object in memory). This can typically be
used for streaming (the representation being a byte stream) or
for database storage (the representation being data in the database).

The typical interface of a convertion service provides two main
methods, createRep and createObj that are respectively creating
the representation of a data object from the object and creating
a data object from its representation.

An easy implementation of these method is to have a big switch
on the object type and dedicated code in each case. This is
highly not dynamic. It means that the service implementation has
to be changed for each new object.

Another approach is to use converters. These dedicated objects are
able to convert one type of object from/to one given representation.
They are in some sense very similar to services : they are also
stateless, they have factories that are listed in a central list.
This list contains a two level entry : representation type and
object type.

The conversion service can now work in the same way the client
is working with services. When a conversion needs to be done, it
will get the right converter, using the central list (and maybe
loading the right library), it will then instantiate it and use it.
If a new object type is added to the system, the new converter should
be put in a library and declared in the config file. The system will
be able to use it without recompilation and without relinking.
Actually, if if can read the new version of the config file at
run time, it could be without restarting at all.

