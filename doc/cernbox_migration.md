# CASTOR to CERNBox Migration Plan

* [OTG0064878](https://cern.service-now.com/service-portal?id=outage&n=OTG0064878) Closure of CASTOR home directories

## Summary of progress

Uncategorised files in CASTOR user home directories

|   | Number of "active" user accounts | Number of files | Cumulative size (TB) |
--- | ----------------------- | --------------- | -------------------- |
30/6/2021 | 922               | 60,205,654      | 4,868                |
30/8/2021 | 793               | 44,291,078      | 2,643                |

"active" means "not deleted", i.e. there is a corresponding account in LDAP. File count (11,490,387) and size (481 TB) for deleted user accounts are included in the above totals.

There are 45 users with >10 TB in their home directory and 162 with >1 TB.

There are 88 users with 1 file in their home directory and 306 with <100 files.

## Phase I : Clean-up and categorize CASTOR home directories

All CASTOR users have been contacted by e-mail and asked to review their files.

Users with >100 TB in their CASTOR home directory were contacted individually to resolve their cases. Two of these cases are waiting for a final decision but we are in contact with the relevant data owners.

We have also enlisted the help of the SME experiment data managers to help identify files which are of interest to the various collaborations.

* Identify files which should be preserved (experiment data, MC data, archived versions of software, etc.) and move them to the appropriate experiment area/fileclass in preparation for migration to CTA
* Delete obsolete files which are no longer needed

## Phase II : Migrate home directories to CERNBox

For cases where the total file size < 1 TB

Until ITUM-34, this will be done on a case-by-case basis with the explicit consent of the user. This will allow us to refine the procedure for migration to CERNBox.

There will be a presentation at ITUM-34 (18/10/2021) reporting progress and giving the schedule for the automated migration of the remaining user accounts.

From late October to end of the year, remaining user accounts will be automatically migrated to CERNBox.

Before starting the automated migrations, we will do a round of "quarantine" to archive home directories for which there is no corresponding account in LDAP. These directories will not be migrated to CERNBox, they will be archived in cold storage in CTA.

Users with >10 TB in their home directory will be contacted individually.

Closure of CASTOR home directories on 31/01/2022.

### Special cases

* User files to be preserved where total file size > 1 TB (Move either to an experiment area or to an EOS project)
* Files belonging to deleted users who can no longer be contacted (Archive in cold storage in CTA?)
* Some users have asked to keep using their data in CASTOR for a limited time, after which it is no longer needed and can be deleted. These files will not be migrated anywhere. They are marked with the special `do_not_migrate` fileclass.

### Data to be migrated to CTA

Files destined for archival in CTA will be repacked into experiment file classes/tapepools and eventually migrated once repacking is complete.

## CERNBox Migration Procedure

* Files will be migrated to a subdirectory of the user's home directory in CERNBox, e.g. `~/ImportedFromCASTOR`

* Giuseppe will write a script which will stage the files in CASTOR and copy them to CERNBox. This script will be executed on the CERNBox side, masquerading as the CERNBox user.

* For initial tests, files can be staged on a per-user basis. Once we are happy with the procedure, we should have a mechanism for bulk staging of batches of users to be migrated.

* Steve has added an SSS key to the CASTOR superuser `stage:st` on the CASTOR public headnodes (see `/etc/archive.keytab`). The key values can be viewed using `tbag`. Relevant keys are `eosarchive` and `eosarchive_just_stage`. Comments from Steve:

> For your convenience I have created a second key named `eosarchive_just_stage` the contents of which is just the key for the `stage:st` user.  You can use this value to create your own client keytab file.  Please note that you put the exact contents of the key into your file.  Do not add any new line of line feed characters.

* We need a mechanism to mark the files as migrated to CERNBox.
  * **Proposal:** create a new column `on_CERNBox` on the `cns_file_metadata` table (analagous to the existing `on_CTA` column). This column will accept NULL values and will default to NULL. The migration script should check each file migrated to CERNBox and set the column to "1"
* After users have been migrated, read/write access to their files in CASTOR must be blocked.

### Details agreed after meeting on 01/09/2021

* Will try and preserve metadata (timestamp, mode bits), but ACLs will be ignored.

* Data will stay in CASTOR in a `do_not_migrate` tape pool, to be eventually deleted.

* Rough procedure for each user:
  1. Identify all files and their metadata via `nsfind.py` on a CASTOR node
  2. Pre-stage all files via `stager_get -f`
  3. `nschmod 000` at the beginning of the process
  4. Copy from CASTOR via `xrdcp` 3rd-party copy: details in Jira (https://its.cern.ch/jira/browse/CERNBOX-2224)
  5. Flag files as _cernbox-migrated_ in the CASTOR Namespace DB

### Strategy

* Prefer a massive stage-in campaign and a few rounds of data transfers to CERNBox as opposed to user-by-user.

* Steve will pre-stage most of the data by early October (or after top users have been sorted out).

* Michael will provide Giuseppe with the top-20 usernames to be migrated, and Giuseppe will check with Roberto in terms of capacity on the eoshome instances.
  * New capacity is expected to be provisioned, therefore we should have plenty of space.

* Giuseppe will script the actual procedure and will execute it from a Samba node taken out of production. This to benefit from high bandwidth and full gateway privileges towards EOS.

* Michael will keep the communication with users, including some automatic mails for the migration day.

* Users with online sync clients may get their PCs (over)filled with the new data. Giuseppe will check how the sync client limit of 500 MB for a "new folder" applies in this case.
  * This can be fixed by storing the files in a hidden folder outside of the user home, as it is done for the DFS migration. Then, the folder is moved in one go to the final destination, ensuring that it won't be synchronized if larger than 500 MB.

* Quota is to be checked but a priori any user with < 1 TB of data in CASTOR can be accommodated by granting up to 2 TB in CERNBox.
