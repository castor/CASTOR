.\" Copyright (C) 1995-2000 by CERN IT-PDP/CS
.\" All rights reserved
.\"
.TH GETACCT "3castor" "$Date: 2001/09/26 09:13:49 $" CASTOR "Common Library Functions"
.SH NAME
.B getacct 
\- get SHIFT account identifier
.SH SYNOPSIS
.B
char *getacct()
.LP
.B
.BI "char *getacctent(struct passwd *" pwd ,
.BI "char *" account,
.BI "char *" buffer ,
.BI "int " bufferlen )
.SH DESCRIPTION
The
.B getacct
function returns the current SHIFT account identifier of the user, while
.B getacctent
returns the full entry. They use the local
copy of the SHIFT account file or the copy held under NIS (Yellow Pages)
if the local file includes the '+' character in the first field of an
account entry.

Function
.B getacct
tests the invoking environment for the variable
.I ACCOUNT.
If set, its value is used as the account identifier to select the possibly
multiple entries for the calling real uid (see account(1) for definitions of
the fields of a SHIFT account file entry). If not set, the default account
for the calling real uid is used (i.e. that account entry with a sequence number
of 0 - see account(1)). The account file uses the username of the calling real uid
as the primary indexing key.
.SH RETURN VALUE
The
.B getacctent
function returns a pointer whose value is either a string containing the
required SHIFT account
entry or NULL, indicating any of the following problems: no entry exists for the user,
the user's entry(s) are malformed, no local SHIFT account file could be found, the real uid
has no entry in the password file.

If not NULL, the resulting string should be copied to the calling procedure as subsequent
calls to
.B getacct()
will overwrite it.

The
.B getacct
function actually calls
.B getacctent
with the passwd entry for the calling real uid and a value for account if the
.I ACCOUNT
environment variable is set, or NULL indicating the default account.

The normal interface is
.B getacct()
but 
.B getacctent()
is also provided for flexibility.
.SH HISTORY
The semantics of
.B getacct
are such as to be backwards compatible with its
predecessor of the same name. In particular, the algorithm for using the
NIS (YP) map of the SHIFT account file has been refined to avoid calling
.B yp_all()
unless all other methods of matching the required account file entry fail.
This, hopefully, avoids the bug in
.B yp_all()
whereby the network connection remains open from the client to the NIS (YP)
map server.
.SH FILES
.B /etc/account
.SH SEE ALSO
.BR account (4), 
.BR newacct (1)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
