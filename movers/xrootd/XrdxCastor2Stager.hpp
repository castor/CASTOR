/*******************************************************************************
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2012  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *
 ******************************************************************************/

#pragma once

/*-----------------------------------------------------------------------------*/
#include "XrdxCastor2Logging.hpp"
#include "XrdxCastorClient.hpp"
/*-----------------------------------------------------------------------------*/
#include <XrdOuc/XrdOucHash.hh>
#include <XrdOuc/XrdOucErrInfo.hh>
#include <XrdOuc/XrdOucString.hh>
/*-----------------------------------------------------------------------------*/


//! Forward declarations
namespace castor
{
  namespace stager
  {
    class FileRequest;
  }

  namespace client
  {
    class IResponseHandler;
  }

  namespace rh
  {
    class Response;
  }
}


//------------------------------------------------------------------------------
//! Class XrdxCastor2Stager
//------------------------------------------------------------------------------
class XrdxCastor2Stager : public LogId
{
public:

  //----------------------------------------------------------------------------
  //! Request information structure
  //----------------------------------------------------------------------------
  struct ReqInfo
  {
    uid_t mUid;
    gid_t mGid;
    const char* mPath;
    const char* mServiceClass;
    uint64_t mSize;

    //--------------------------------------------------------------------------
    //! Constructor
    //--------------------------------------------------------------------------
    ReqInfo(uid_t uid, gid_t gid, const char* path, const char* serviceclass,
	    uint64_t size = 0ull):
      mUid(uid), mGid(gid), mPath(path), mServiceClass(serviceclass),
      mSize(size)
    {
      // empty
    }
  };



  //----------------------------------------------------------------------------
  //! Response information structure
  //----------------------------------------------------------------------------
  struct RespInfo
  {
    XrdOucString mRedirectionHost;
    XrdOucString mRedirectionPfn1;
    XrdOucString mRedirectionPfn2;
    XrdOucString mStageStatus;

    //-------------------------------------------------------------------------
    //! Constructor
    //-------------------------------------------------------------------------
    RespInfo():
      mRedirectionHost(""),
      mRedirectionPfn1(""),
      mRedirectionPfn2(""),
      mStageStatus("")
    {
      // empty
    }
  };


  //----------------------------------------------------------------------------
  //! Constructor
  //----------------------------------------------------------------------------
  XrdxCastor2Stager();


  //----------------------------------------------------------------------------
  //! Destructor
  //----------------------------------------------------------------------------
  ~XrdxCastor2Stager();


  //----------------------------------------------------------------------------
  //! Get a delay value for the corresponding tag
  //!
  //! @param tag is made by concatenating the tident with the path of the req
  //!
  //! @return delay in seconds
  //----------------------------------------------------------------------------
  static int GetDelayValue(const char* tag);


  //----------------------------------------------------------------------------
  //! Drop delay tag from mapping
  //!
  //! @param tag tag to be dropped from mapping
  //----------------------------------------------------------------------------
  static void DropDelayTag(const char* tag);


  //----------------------------------------------------------------------------
  //! Delete the request and response objects from the maps
  //!
  //! @param req request object
  //! @param resp response object
  //! @param respvect response vector
  //----------------------------------------------------------------------------
  static void DeleteReqResp(castor::stager::FileRequest* req,
                            castor::client::IResponseHandler* resp,
                            std::vector<castor::rh::Response*>* respvec);


  //----------------------------------------------------------------------------
  //! Process response received form the stager
  //!
  //! @param error error object
  //! @param respElem ReqElemement structure defined in XrdxCastorClient.hh
  //! @param opType opertation type
  //! @param reqInfo request informatio structure
  //! @param respInfo response info structure to be filled in
  //!
  //! @return SFS_OK if successful, otherwise SFS_ERROR.
  //--------------------------------------------------------------------------
  static int ProcessResponse(XrdOucErrInfo& error,
                             struct xcastor::XrdxCastorClient::ReqElement*& respElem,
                             const std::string& opType,
                             struct ReqInfo* reqInfo,
                             struct RespInfo& respInfo);


  //----------------------------------------------------------------------------
  //! Prepare2Get
  //!
  //! @param error error object, which is filled in case of errors
  //! @param uid, gid user identity for the request
  //! @param paths a list of at least one CASTOR file name
  //! @param serviceclass request service class
  //!
  //! @return the request UUID for later tracking, or "" in case of error
  //----------------------------------------------------------------------------
  static std::string Prepare2Get(XrdOucErrInfo& error,
                                 uid_t uid, gid_t gid,
                                 const std::vector<std::string>& paths,
                                 const char* serviceclass);


  //----------------------------------------------------------------------------
  //! Send an async request to the stager. This request can be a GET or a PUT
  //! or an UPDATE.
  //!
  //! @param error error object
  //! @param opType type of operation: get, put or update
  //! @param reqInfo request information stucture
  //! @param respInfo response information structure
  //!
  //! @return SFS_OK answer received and successfully parsed
  //!         SFS_ERROR there was an error
  //!         SFS_STALL response not available yet, stall the client
  //----------------------------------------------------------------------------
  static int DoAsyncReq(XrdOucErrInfo& error,
                        const std::string& opType,
                        struct ReqInfo* reqInfo,
                        struct RespInfo& respInfo);


  //----------------------------------------------------------------------------
  //! Send stage abort request to the stager daemon
  //!
  //! @param req_uuid request uuid which we are aborting
  //! @param svc_class request service class
  //! @param uid client uid
  //! @param gid client gid
  //! @param error error object
  //!
  //! @return True if successful, otherwise false.
  //----------------------------------------------------------------------------
  static bool StageAbortRequest(const std::string& req_uuid,
                                const std::string& svc_class,
                                uid_t uid, gid_t gid,
                                XrdOucErrInfo& error);


  //----------------------------------------------------------------------------
  //! Rm
  //----------------------------------------------------------------------------
  static bool Rm(XrdOucErrInfo& error,
                 uid_t uid, gid_t gid,
                 const char* path,
                 const char* serviceclass);


  //----------------------------------------------------------------------------
  //! StagerQuery
  //----------------------------------------------------------------------------
  static bool StagerQuery(XrdOucErrInfo& error,
                          uid_t uid, gid_t gid,
                          const char* path,
                          const char* serviceclass,
                          std::string& stagestatus);

  //----------------------------------------------------------------------------
  //! StageDiskPoolQuery
  //----------------------------------------------------------------------------
  static bool StageDiskPoolQuery(std::string& result,
                                 uid_t uid, gid_t gid,
                                 const char* serviceclass);

  static XrdOucHash<XrdOucString>* msDelayStore; ///< delay store for each of the users

private:

  static XrdSysRWLock msLockStore; ///< RW lock for the delay map

};
