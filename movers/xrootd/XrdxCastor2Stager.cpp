/*******************************************************************************
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2012  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *
 ******************************************************************************/


/*-----------------------------------------------------------------------------*/
#include <sys/types.h>
#include <iostream>
#include <ctime>
/*-----------------------------------------------------------------------------*/
#include "castor/BaseObject.hpp"
#include "castor/Constants.hpp"
#include "castor/client/VectorResponseHandler.hpp"
#include "castor/client/BaseClient.hpp"
#include "castor/stager/Request.hpp"
#include "castor/Services.hpp"
#include "castor/IService.hpp"
#include "castor/System.hpp"
#include "castor/stager/StagePrepareToGetRequest.hpp"
#include "castor/stager/StagePrepareToPutRequest.hpp"
#include "castor/stager/StagePutRequest.hpp"
#include "castor/stager/StageGetRequest.hpp"
#include "castor/stager/StageAbortRequest.hpp"
#include "castor/stager/SubRequest.hpp"
#include "castor/query/DiskPoolQuery.hpp"
#include "castor/query/DiskPoolQueryType.hpp"
#include "castor/rh/IOResponse.hpp"
#include "castor/exception/Exception.hpp"
#include "castor/exception/Communication.hpp"
#include "h/stager_api.h"
#include "h/stager_client_api_common.hpp"
#include "client/src/stager/stager_client_api_query.hpp"
/*-----------------------------------------------------------------------------*/
#include "XrdxCastor2Stager.hpp"
#include "XrdxCastor2Fs.hpp"
/*-----------------------------------------------------------------------------*/

XrdSysRWLock XrdxCastor2Stager::msLockStore; ///< delay store lock

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
XrdxCastor2Stager::XrdxCastor2Stager():
  LogId()
{
  // empty
}


//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
XrdxCastor2Stager::~XrdxCastor2Stager()
{
  // empty
}


//------------------------------------------------------------------------------
// Delete the request and response objects
//------------------------------------------------------------------------------
void
XrdxCastor2Stager::DeleteReqResp(castor::stager::FileRequest* req,
                                 castor::client::IResponseHandler* resp,
                                 std::vector<castor::rh::Response*>* respvec)
{
  // Delete request
  delete req;
  // Delete the responses and the vector
  delete resp;
  castor::rh::Response* ptr = 0;

  for (unsigned int i = 0; i < respvec->size(); i++)
  {
    ptr = respvec->at(i);
    delete ptr;
  }

  respvec->clear();
  delete respvec;
}


//------------------------------------------------------------------------------
// Prepare to get
//------------------------------------------------------------------------------
std::string
XrdxCastor2Stager::Prepare2Get(XrdOucErrInfo& error,
                               uid_t uid,
                               gid_t gid,
                               const std::vector<std::string>& paths,
                               const char* serviceclass)
{
  xcastor_static_debug("uid=%i gid=%i svcClass=%s",
                       uid, gid, serviceclass);
  // Construct the request and subrequest objects
  struct stage_options Opts;
  std::string reqid = "";
  castor::stager::StagePrepareToGetRequest getReq_ro;
  std::vector<castor::rh::Response*>respvec;
  castor::client::VectorResponseHandler rh(&respvec);
  int mask = umask(0);
  umask(mask);
  getReq_ro.setUserTag("xCastor2P2Get");
  getReq_ro.setMask(mask);
  for(auto it = paths.begin(); it != paths.end(); it++) {
    castor::stager::SubRequest* subreq = new castor::stager::SubRequest();
    subreq->setRequest(&getReq_ro);
    subreq->setProtocol(std::string("xroot"));
    subreq->setFileName(*it);
    subreq->setModeBits(0744);
    getReq_ro.addSubRequests(subreq);
    xcastor_static_debug("uid=%i gid=%i svcClass=%s path=%s",
           uid, gid, serviceclass, (*it).c_str());
  }
  castor::client::BaseClient cs2client(stage_getClientTimeout(), -1);
  Opts.stage_host = (char*)gMgr->GetStagerHost().c_str();
  Opts.service_class = (char*)serviceclass;
  Opts.stage_version = 2;
  Opts.stage_port = 0;
  cs2client.setOptions(&Opts);
  cs2client.setAuthorizationId(uid, gid);

  try
  {
    reqid = cs2client.sendRequest(&getReq_ro, &rh);
  }
  catch (castor::exception::Communication e)
  {
    xcastor_static_debug("msg=\"communication error %s\"", e.getMessage().str().c_str());
    error.setErrInfo(ECOMM, e.getMessage().str().c_str());
    return reqid;
  }
  catch (castor::exception::Exception e)
  {
    xcastor_static_debug("msg=\"sendRequest exception %s\"", e.getMessage().str().c_str());
    error.setErrInfo(ECOMM, e.getMessage().str().c_str());
    return reqid;
  }

  if (respvec.size() <= 0)
  {
    xcastor_static_debug("msg=\"Prepare2Get no response\"");
    error.setErrInfo(ECOMM, "No response for Prepare2Get");
    return reqid;
  }

  // Process the responses
  for(auto it = respvec.begin(); it != respvec.end(); it++)
  {
    // we can safely assume the cast works because the vector already contains
    // valid castor::rh::Response objects and the only response type for a PrepareToGet
    // is castor::rh::IOResponse or NULL.
    castor::rh::IOResponse* fr = dynamic_cast<castor::rh::IOResponse*>(*it);
    if (fr->errorCode())
    {
      std::stringstream sstr;
      sstr << "uid=" << (int)uid << " gid=" << (int)gid
           << " serviceclass=" << serviceclass;
      XrdOucString ErrPrefix = sstr.str().c_str();
      xcastor_static_debug("reqid=%s subreqid=%s errc=%i errmsg=\"%s/%s\"",
                           fr->reqAssociated().c_str(), fr->subreqId().c_str(),
                           fr->errorCode(), fr->errorMessage().c_str(),
                           ErrPrefix.c_str());
      XrdOucString emsg = "received error errc=";
      emsg += (int)fr->errorCode();
      emsg += " errmsg=\"";
      emsg += fr->errorMessage().c_str();
      emsg += "\" ";
      emsg += ErrPrefix;
      emsg += " subreqid=";
      emsg += fr->subreqId().c_str();
      emsg += " reqid=";
      emsg += fr->reqAssociated().c_str();
      error.setErrInfo(fr->errorCode(), emsg.c_str());
    }
    else
    {
      xcastor_static_debug("reqid=%llu status=\"%s\" status_rc=%u diskserver=%s lpath=%s castorpath=%s",
                           fr->id(), stage_requestStatusName(fr->status()), fr->status(),
                           fr->server().c_str(), fr->fileName().c_str(),
                           fr->castorFileName().c_str());
    }
  }
  for (auto it = respvec.begin(); it != respvec.end(); it++) {
    delete *it;
  }
  return reqid;
}


//------------------------------------------------------------------------------
// Send an async request to the stager. This request can be a GET or a PUT
//------------------------------------------------------------------------------
int
XrdxCastor2Stager::DoAsyncReq(XrdOucErrInfo& error,
                              const std::string& opType,
                              struct ReqInfo* reqInfo,
                              struct RespInfo& respInfo)
{
  xcastor_static_debug("uid=%i gid=%i path=%s svcClass=%s op=%s",
                       reqInfo->mUid, reqInfo->mGid, reqInfo->mPath,
                       reqInfo->mServiceClass, opType.c_str());
  // Build the user id
  std::stringstream sstr;
  const char* tident = error.getErrUser();
  sstr << tident << ":" << reqInfo->mPath << ":" << opType;
  std::string user_id = sstr.str();

  // Check if we are coming back for an old request
  bool found = false;
  struct xcastor::XrdxCastorClient::ReqElement* elem = 0;
  elem = gMgr->msCastorClient->GetResponse(user_id, found, false);

  // We are looking for a previous request
  if (found)
  {
    if (elem)
    {
      // Response was received, we just need to process it
      xcastor_static_debug("path=%s msg=\"found response for previous request\"",
                           reqInfo->mPath);
      DropDelayTag(user_id.c_str());
      return ProcessResponse(error, elem, opType, reqInfo, respInfo);
    }
    else
    {
      // Response not received, we need to stall the client some more
      xcastor_static_debug("path=%s msg=\"response for previous request hasn't "
                           "arrived yet\"", reqInfo->mPath);
      return SFS_STALL;
    }
  }

  // Build the request and response objects
  std::string user_tag;
  castor::stager::FileRequest* request;

  if (opType == "get")
  {
    user_tag = "xCasto2Get";
    request = new castor::stager::StageGetRequest();
  }
  else if (opType == "put")
  {
    user_tag = "xCastor2Put";
    request = new castor::stager::StagePutRequest();
  }
  else
  {
    xcastor_static_err("msg=\"unknown operation type=%s\"", opType.c_str());
    return SFS_ERROR;
  }

  castor::stager::SubRequest* subreq = new castor::stager::SubRequest();
  std::vector<castor::rh::Response*>* respvec =  new
  std::vector<castor::rh::Response*>();
  castor::client::VectorResponseHandler* rh = new
  castor::client::VectorResponseHandler(respvec);
  int mask = umask(0);
  umask(mask);
  request->setUserTag(user_tag);
  request->setMask(mask);
  request->addSubRequests(subreq);
  request->setEuid(reqInfo->mUid);
  request->setEgid(reqInfo->mGid);
  request->setUserName(std::string(tident));
  request->setPid(getpid());
  request->setSvcClassName(std::string(reqInfo->mServiceClass));
  subreq->setRequest(request);
  subreq->setProtocol(std::string("xroot"));
  subreq->setFileName(std::string(reqInfo->mPath));
  subreq->setModeBits(0744);
  subreq->setXsize(reqInfo->mSize);

  try
  {
    // Sending asynchronous get requests
    int retc = gMgr->msCastorClient->SendAsyncRequest(user_id,
                                                      gMgr->GetStagerHost(),
                                                      0, request, rh, respvec);

    if (retc == SFS_ERROR)
    {
      // Error occured, free memory
      xcastor_static_err("path=%s msg=\"error while sending the async request\"",
                         reqInfo->mPath);
      DeleteReqResp(request, rh, respvec);
      return retc;
    }
    else if (retc > SFS_OK)
    {
      // Free memory and stall the client
      DeleteReqResp(request, rh, respvec);
      return SFS_STALL;
    }
  }
  catch (castor::exception::Communication e)
  {
    xcastor_static_err("msg=\"communication error %s\"", e.getMessage().str().c_str());
    error.setErrInfo(ECOMM, e.getMessage().str().c_str());
    return SFS_ERROR;
  }
  catch (castor::exception::Exception e)
  {
    xcastor_static_err("op=%s msg=\"request exception %s\"", opType.c_str(),
                       e.getMessage().str().c_str());
    error.setErrInfo(ECOMM, e.getMessage().str().c_str());
    return SFS_ERROR;
  }

  // Try to get the response, maybe we are lucky ...
  elem = 0;
  found = false;
  elem = gMgr->msCastorClient->GetResponse(user_id, found, true);

  if (elem)
  {
    // Response found, process it
    return ProcessResponse(error, elem, opType, reqInfo, respInfo);
  }
  else
  {
    if (found)
    {
      // Request found, but response not yet ready => stall the client
      xcastor_static_debug("path=%s op=%s msg=\"request found, but no response\"",
                           reqInfo->mPath, opType.c_str());
      return SFS_STALL;
    }
    else
    {
      // Request not found => return error to the client
      xcastor_static_err("path=%s op=%s msg=\"expected request not found\"",
                         reqInfo->mPath, opType.c_str());
      return SFS_ERROR;
    }
  }
}


//------------------------------------------------------------------------------
// Remove
//------------------------------------------------------------------------------
bool
XrdxCastor2Stager::Rm(XrdOucErrInfo& error,
                      uid_t uid,
                      gid_t gid,
                      const char* path,
                      const char* serviceclass)
{
  xcastor_static_debug("uid=%i gid=%i path=%s svcClass=%s",
                       uid, gid, path, serviceclass);
  struct stage_filereq requests[1];
  struct stage_fileresp* resp;
  struct stage_options Opts;
  int i;
  int nbresps;
  char* reqid;
  char errbuf[1024];
  Opts.stage_host = (char*)gMgr->GetStagerHost().c_str();
  Opts.service_class = (char*)serviceclass;
  Opts.stage_version = 2;
  Opts.stage_port = 0;
  requests[0].filename = (char*) path;
  stage_setid(uid, gid);
  stager_seterrbuf(errbuf, sizeof(errbuf));

  if (stage_rm(requests, 1, &resp, &nbresps, &reqid, &Opts) < 0)
  {
    if (serrno != 0)
    {
      xcastor_static_debug("msg=\"stage_rm error=%s\"", sstrerror(serrno));
      error.setErrInfo(ECOMM, sstrerror(serrno));
    }

    if (*errbuf)
    {
      xcastor_static_debug("msg=\"stage_rm err_buff=%s\"", errbuf);
      error.setErrInfo(ECOMM, errbuf);
    }

    return false;
  }
  else
  {
    xcastor_static_debug("msg=\"received %i stage_rm responses\"", nbresps);

    for (i = 0; i < nbresps; i++)
    {
      xcastor_static_debug("path=%s reqid=%s errc=%i errmsg=\"%s\"", path,
                           reqid, resp[i].errorCode, resp[i].errorMessage);

      if (resp[i].errorCode)
      {
        error.setErrInfo(EINVAL, resp[i].errorMessage);
        free_fileresp(resp, nbresps);
        return false;
      }
    }

    free_fileresp(resp, nbresps);
  }

  return true;
}


//------------------------------------------------------------------------------
// Stager query
//------------------------------------------------------------------------------
bool
XrdxCastor2Stager::StagerQuery(XrdOucErrInfo& error,
                               uid_t uid,
                               gid_t gid,
                               const char* path,
                               const char* serviceclass,
                               std::string& status)
{
  xcastor_static_debug("uid=%i gid=%i path=%s svcClass=%s",
                       uid, gid, path, serviceclass);
  struct stage_query_req requests[1];
  struct stage_filequery_resp* resp;
  int  nbresps, i;
  char errbuf[1024];
  struct stage_options Opts;
  errbuf[0] = 0;
  Opts.stage_host = (char*)gMgr->GetStagerHost().c_str();;
  Opts.service_class = (char*)serviceclass;
  Opts.stage_version = 2;
  Opts.stage_port = 0;
  requests[0].type = BY_FILENAME;
  requests[0].param = (char*) path;
  stager_seterrbuf(errbuf, sizeof(errbuf));
  stage_setid(uid, gid);

  if (stage_filequery(requests, 1, &resp, &nbresps, &Opts) < 0)
  {
    if (serrno != 0)
    {
      xcastor_static_debug("msg=\"stage_filequery error=%s\"", sstrerror(serrno));
      error.setErrInfo(ECOMM, sstrerror(serrno));
    }

    if (*errbuf)
    {
      xcastor_static_debug("msg=\"stage_filequery err_buff=%s\"", errbuf);
      error.setErrInfo(ECOMM, errbuf);
    }

    return false;
  }
  else
  {
    xcastor_static_debug("msg=\"received %i stage_filequery responses\"", nbresps);

    for (i = 0; i < nbresps; i++)
    {
      xcastor_static_debug("status=%s errc=%i diskserver=%s lpath=%s castor_path=%s",
                           stage_fileStatusName(resp[i].status),
                           resp[i].errorCode, resp[i].diskserver,
                           resp[i].filename, resp[i].castorfilename);
      status = stage_fileStatusName(resp[i].status);

      if (*resp[i].castorfilename)
      {
        xcastor_static_debug("path=%s status=%s errc=%i",
                             path, status.c_str(), resp[i].errorCode);

        if (resp[i].errorCode)
        {
          free_filequery_resp(resp, nbresps);
          error.setErrInfo(EBUSY, "file is not staged in requested stager");
          return false;
        }
      }
      else
      {
        status = "NA";
      }
    }

    free_filequery_resp(resp, nbresps);
  }

  return true;
}

//------------------------------------------------------------------------------
// Diskpool query
//------------------------------------------------------------------------------
bool
XrdxCastor2Stager::StageDiskPoolQuery(std::string& result,
                                      uid_t uid,
                                      gid_t gid,
                                      const char* serviceclass)
{
  xcastor_static_debug("uid=%i gid=%i svcClass=%s", uid, gid, serviceclass);
  struct stage_diskpoolquery_resp* resp;
  int  nbresps;
  char errbuf[1024];
  char res[1024];
  struct stage_options Opts;
  errbuf[0] = 0;
  res[0] = 0;
  Opts.stage_host = (char*)gMgr->GetStagerHost().c_str();
  Opts.service_class = (char*)serviceclass;
  Opts.stage_port = 0;
  stager_seterrbuf(errbuf, sizeof(errbuf));
  stage_setid(uid, gid);
  time_t now = std::time(0);

  if (stage_diskpoolsquery_internal(&resp, &nbresps, &Opts, castor::query::DISKPOOLQUERYTYPE_AVAILABLE) != 0)
  {
    if (*errbuf)
    {
      xcastor_static_debug("msg=\"stage_diskpoolsquery failed, error=%s\"", errbuf);
      sprintf(res, "    { \"name\": \"%s\", \"accesslatency\": \"nearline\", \"status_message\": \"%s\", \"path\": [\"/castor\"], \"totalsize\": -1, \"usedsize\": -1, \"numberoffiles\": -1, \"timestamp\": %ld }",
              serviceclass, errbuf, now);
    }
    result = res;
    return false;
  }
  else
  {
    xcastor_static_debug("msg=\"received %i stage_diskpoolsquery responses\"", nbresps);
    u_signed64 totSpace  = 0;
    u_signed64 freeSpace = 0;
    for (int i = 0; i < nbresps; i++) {
        totSpace  += resp[i].totalSpace;
        freeSpace += resp[i].freeSpace;
        stage_delete_diskpoolquery_resp(&(resp[i]));
    }
    totSpace -= totSpace % 100000000000;
    freeSpace -= freeSpace % 100000000000;
    sprintf(res, "    { \"name\": \"%s\", \"accesslatency\": \"nearline\", \"path\": [\"/castor\"], \"totalsize\": %llu, \"usedsize\": %llu, \"numberoffiles\": -1, \"timestamp\": %ld }",
            serviceclass, totSpace, totSpace-freeSpace, now);
    result = res;
  }
  return true;
}


//------------------------------------------------------------------------------
// Stager abort request
//------------------------------------------------------------------------------
bool
XrdxCastor2Stager::StageAbortRequest(const std::string& uuid,
                                     const std::string& serviceclass,
                                     uid_t uid,
                                     gid_t gid,
                                     XrdOucErrInfo& error)
{
  xcastor_static_debug("uid=%i gid=%i uuid=%s svcClass=%s",
                       uid, gid, uuid.c_str(), serviceclass.c_str());
  struct stage_options Opts;
  castor::stager::StageAbortRequest abort_req;
  std::vector<castor::rh::Response*> respvec;
  castor::client::VectorResponseHandler rh(&respvec);
  int mask = umask(0);
  umask(mask);
  abort_req.setUserTag("xCastor2AbortReq");
  abort_req.setMask(mask);
  abort_req.setParentUuid(uuid);
  castor::client::BaseClient cs2client(stage_getClientTimeout());
  Opts.stage_host = (char*)gMgr->GetStagerHost().c_str();
  Opts.service_class = (char*)serviceclass.c_str();
  Opts.stage_version = 2;
  Opts.stage_port = 0;
  cs2client.setOptions(&Opts);
  cs2client.setAuthorizationId(uid, gid);

  try
  {
    std::string reqid = cs2client.sendRequest(&abort_req, &rh);
  }
  catch (castor::exception::Communication e)
  {
    xcastor_static_debug("msg=\"communication error=%s\"", e.getMessage().str().c_str());
    error.setErrInfo(ECOMM, e.getMessage().str().c_str());
    return false;
  }
  catch (castor::exception::Exception e)
  {
    xcastor_static_debug("msg=\"sendRequest exception=%s\"", e.getMessage().str().c_str());
    error.setErrInfo(ECOMM, e.getMessage().str().c_str());
    return false;
  }

  if (respvec.size() <= 0)
  {
    xcastor_static_debug("uuid=%s msg=\"no response for abort request\"", uuid.c_str());
    error.setErrInfo(ECOMM, "No response for abort request");
    return false;
  }

  // Proccess the response.
  castor::rh::FileResponse* fr = dynamic_cast<castor::rh::FileResponse*>
                                 (respvec[0]);

  if (0 == fr)
  {
    xcastor_static_debug("msg=\"invalid abort response object uuid=%s\"", uuid.c_str());
    error.setErrInfo(ECOMM, "Invalid response object for StageAbortRequest");
    delete respvec[0];
    return false;
  }

  if (fr->fileId() == 0 && respvec.size() == 1)
  {
    xcastor_static_err("uuid=%s errc=%i errmsg=\"%s\"", uuid.c_str(), fr->errorCode(),
                       fr->errorMessage().c_str());
    delete respvec[0];
    return false;
  }
  else
  {
    if (0 != fr->errorCode())
    {
      std::stringstream sstr;
      sstr << "msg=\"StageAbortReq error\" uuid=" << uuid << " uid=" << (int)uid
           << " gid=" << (int)gid << " errc=" << fr->errorCode() << " errmsg=\""
           << fr->errorMessage() << "\"";
      xcastor_static_err("%s", sstr.str().c_str());
      error.setErrInfo(fr->errorCode(), sstr.str().c_str());
      delete respvec[0];
      return false;
    }
    else
    {
      xcastor_static_debug("uuid=%s fileId=%llu msg=\"aborted\"", uuid.c_str(),
                           fr->fileId());
      delete respvec[0];
      return true;
    }
  }
}



//------------------------------------------------------------------------------
// Process response received form the stager
//------------------------------------------------------------------------------
int
XrdxCastor2Stager::ProcessResponse(XrdOucErrInfo& error,
                                   struct xcastor::XrdxCastorClient::ReqElement*& respElem,
                                   const std::string& opType,
                                   struct ReqInfo* reqInfo,
                                   struct RespInfo& respInfo)
{
  if (respElem->mRespVec->size() <= 0)
  {
    xcastor_static_debug("path=%s op=%s msg=\"no stager response\"",
                         reqInfo->mPath, opType.c_str());
    error.setErrInfo(ECOMM, "No stager response");
    delete respElem;
    return SFS_ERROR;
  }

  // Proccess the response
  castor::rh::IOResponse* fr =
    dynamic_cast<castor::rh::IOResponse*>(respElem->mRespVec->at(0));

  if (0 == fr)
  {
    xcastor_static_err("path=%s op=%s msg=\"invalid response object\"",
                       reqInfo->mPath, opType.c_str());
    error.setErrInfo(ECOMM, "Invalid response object");
    delete respElem;
    return SFS_ERROR;
  }

  if (fr->errorCode())
  {
    std::stringstream sstr;
    sstr.str(std::string());
    sstr << "uid=" << (int)reqInfo->mUid << " gid=" << (int)reqInfo->mGid
         << " path=" << reqInfo->mPath << " svcClass=" << reqInfo->mServiceClass;
    XrdOucString ErrPrefix = sstr.str().c_str();
    xcastor_static_err("reqid=%s errc=%i errmsg=\"%s\" errprefix=\"%s\" "
                       "subreqid=%s msg=\"received error\"",
                       fr->reqAssociated().c_str(), fr->errorCode(),
                       fr->errorMessage().c_str(), ErrPrefix.c_str(),
                       fr->subreqId().c_str());
    error.setErrInfo(fr->errorCode(), fr->errorMessage().c_str());
    delete respElem;
    return SFS_ERROR;
  }
  else
  {
    xcastor_static_debug("reqid=%llu status=\"%s\" rc=%i diskserver=%s lpath=%s castorpath=%s",
                         fr->id(), stage_requestStatusName(fr->status()),
                         fr->errorCode(), fr->server().c_str(),
                         fr->fileName().c_str(), fr->castorFileName().c_str());
  }

  respInfo.mStageStatus = stage_requestStatusName(fr->status());
  respInfo.mRedirectionHost = fr->server().c_str();
  respInfo.mRedirectionPfn1 = fr->fileName().c_str();

  char sid[4096];
  sprintf(sid, "%llu", fr->id());
  respInfo.mRedirectionPfn2 = sid;  // request id

  // Attach the port for the local host connection on a diskserver to talk
  // to the diskmanager
  respInfo.mRedirectionPfn2 += ":";
  respInfo.mRedirectionPfn2 += fr->port();

  // Attach the sub req ID needed to identify the local host connection
  respInfo.mRedirectionPfn2 += ":";
  respInfo.mRedirectionPfn2 += fr->subreqId().c_str();
  delete respElem;
  return SFS_OK;
}


//------------------------------------------------------------------------------
// Get delay value
//------------------------------------------------------------------------------
int
XrdxCastor2Stager::GetDelayValue(const char* tag)
{
  XrdOucString* delayval;
  msLockStore.ReadLock(); // -->

  if ((delayval = msDelayStore->Find(tag)))
  {
    float oldval = atoi(delayval->c_str());
    oldval *= 1.6;

    if (oldval > 300)
    {
      // More than 5 min doesn't make sense
      oldval = 290;
    }

    // We double always the delay value
    (*delayval) = "";
    *(delayval) += (int) oldval;
  }
  else
  {
    msLockStore.UnLock(); // <--
    delayval = new XrdOucString();
    *delayval = 2 + (rand() % 5);
    msLockStore.WriteLock(); // -->
    msDelayStore->Add(tag, delayval, 3600);
  }

  msLockStore.UnLock(); // <--
  return atoi(delayval->c_str());
}


//------------------------------------------------------------------------------
// Drop delay tag from mapping
//------------------------------------------------------------------------------
void
XrdxCastor2Stager::DropDelayTag(const char* tag)
{
  XrdSysRWLockHelper wr_lock(msLockStore, 0);
  msDelayStore->Del(tag);
}
