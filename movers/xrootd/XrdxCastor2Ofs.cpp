/*******************************************************************************
 *                      XrdxCastor2Ofs.cc
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2012  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *
 ******************************************************************************/

/*-----------------------------------------------------------------------------*/
#include <sstream>
#include <string>
#include <utility>
#include <iomanip>
#include <sys/types.h>
#include <sys/stat.h>
#include <grp.h>
#include <fcntl.h>
#include "Cns_api.h"
#include "serrno.h"
#include <zlib.h>
#include "movers/moveropenclose.h"
#include "castor/System.hpp"
#include "castor/common/CastorConfiguration.hpp"
/*----------------------------------------------------------------------------*/
#include "XrdVersion.hh"
#include "XrdAcc/XrdAccAuthorize.hh"
#include "XrdOfs/XrdOfs.hh"
#include "XrdSys/XrdSysDNS.hh"
#include "XrdSys/XrdSysTimer.hh"
#include "XrdSys/XrdSysXAttr.hh"
#include "XrdOss/XrdOssApi.hh"
#include "XrdOuc/XrdOucTrace.hh"
#include "XrdNet/XrdNetSocket.hh"
#include "XrdSec/XrdSecEntity.hh"
#include "XrdSfs/XrdSfsAio.hh"
/*----------------------------------------------------------------------------*/
#include "XrdxCastor2Ofs.hpp"
#include "XrdxCastor2Timing.hpp"
#include "XrdxCastorConstants.hpp"
#include "XrdxCastorIoPrio.hh"
/*----------------------------------------------------------------------------*/

XrdxCastor2Ofs* gSrv; ///< global diskserver OFS handle

// Extern symbols
extern XrdOfs*     XrdOfsFS;
extern XrdSysError OfsEroute;
extern XrdOssSys*  XrdOfsOss;
extern XrdOss*     XrdOssGetSS(XrdSysLogger*, const char*, const char*);
extern XrdSysXAttr *XrdSysXAttrActive;

XrdVERSIONINFO(XrdSfsGetFileSystem2, xCastor2Ofs);

// One minute for destination to contact us for tpc.key rendez-vous
const int XrdxCastor2OfsFile::sKeyExpiry = 60;


//------------------------------------------------------------------------------
// XrdSfsGetFileSystem2 version 2 also passes the envP info pointer
//------------------------------------------------------------------------------
extern "C"
{
  XrdSfsFileSystem* XrdSfsGetFileSystem2(XrdSfsFileSystem* native_fs,
                                         XrdSysLogger* lp,
                                         const char* configfn,
                                         XrdOucEnv* envP)
  {
    static XrdxCastor2Ofs myFS;
    // Do the herald thing
    OfsEroute.SetPrefix("castor2ofs_");
    OfsEroute.logger(lp);
    OfsEroute.Say("++++++ (c) 2014 CERN/IT-DSS xCastor2Ofs v1.0");

    // Initialize the subsystems
    gSrv = &myFS;
    gSrv->ConfigFN = (configfn && *configfn ? strdup(configfn) : 0);

    if (gSrv->Configure(OfsEroute, envP)) return 0;

    // All done, we can return the callout vector to these routines
    XrdOfsFS = static_cast<XrdOfs*>(gSrv);
    return XrdOfsFS;
  }
}


/******************************************************************************/
/*                         x C a s t o r O f s                                */
/******************************************************************************/

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
XrdxCastor2Ofs::XrdxCastor2Ofs():
  XrdOfs(),
  LogId(),
  mLogLevel(LOG_INFO),
  mMoverHandlerPort(0),
  mUseIoPriorities(false),
  mFSync(false),
  mManagerHost(""),
  mManagerPort(1094)
{
  // empty
}


//------------------------------------------------------------------------------
// Configure
//------------------------------------------------------------------------------
int XrdxCastor2Ofs::Configure(XrdSysError& Eroute, XrdOucEnv* envP)
{
  int rc = 0;
  char* var;
  const char* val;
  int  cfgFD;
  // Extract the manager from the config file
  XrdOucStream config_stream(&Eroute, getenv("XRDINSTANCE"));

  // Read the CASTOR configuration from /etc/castor.conf
  try
  {
    castor::common::CastorConfiguration& castor_conf =
      castor::common::CastorConfiguration::getConfig();

    mMoverHandlerPort = castor_conf.getConfEntInt("DiskManager",
      "MoverHandlerPort", XCASTOR2OFS_MOVERHANDLER_PORT, NULL);
    Eroute.Say("=====> Using DiskManager MoverHandlerPort: ",
               std::to_string((long long int)mMoverHandlerPort).c_str());
  }
  catch (...)
  {
    xcastor_err("could not retrieve DiskManager MoverHandlerPort value");
    return Eroute.Emsg("Config", EINVAL, "get DiskManager MoverHandlerPort");
  }

  // Parse the xrd.cf.server configuration file
  if (ConfigFN && *ConfigFN)
  {
    // Try to open the configuration file.
    if ((cfgFD = open(ConfigFN, O_RDONLY, 0)) < 0)
      return Eroute.Emsg("Config", errno, "open config file fn=", ConfigFN);

    config_stream.Attach(cfgFD);

    // Now start reading records until eof.
    while ((var = config_stream.GetMyFirstWord()))
    {
      if (!strncmp(var, "xcastor2.", 9))
      {
        var += 9;

        // Get the log level
        if (!strncmp("loglevel", var, 8))
        {
          if (!(val = config_stream.GetWord()))
          {
            Eroute.Emsg("Config", "argument for debug level invalid set to INFO.");
            mLogLevel = LOG_INFO;
          }
          else
          {
            long int log_level = Logging::GetPriorityByString(val);

            if (log_level == -1)
            {
              // Maybe the log level is specified as an int from 0 to 7
              errno = 0;
              char* end;
              log_level = (int) strtol(val, &end, 10);

              if ((errno == ERANGE && ((log_level == LONG_MIN) || (log_level == LONG_MAX))) ||
                  ((errno != 0) && (log_level == 0)) ||
                  (end == val))
              {
                // There was an error default to LOG_INFO
                log_level = 6;
              }
            }

            mLogLevel = log_level;
            Eroute.Say("=====> xcastor2.loglevel: ",
                       Logging::GetPriorityString(mLogLevel), "");
          }
        }

        // Get any debug filter name
        if (!strncmp(var, "debugfilter", 11))
        {
          if (!(val = config_stream.GetWord()))
          {
            Eroute.Emsg("Config", "argument for debug filter invalid set to none.");
          }
          else
          {
            Logging::SetFilter(val);
            Eroute.Say("=====> xcastor2.debugfileter: ", val, "");
          }
        }

        // Get IO priorities map
        if (!strncmp(var, "iopriomap", 9))
        {
          std::ostringstream oss;
          std::string tx_type;
          int ioprio_class, ioprio_prio;
          std::vector<std::string> header = {"Transfer tag", "IO sched. class", "IO priority"};
          std::map< std::string, std::pair<int, std::string> > io_class_map =
            {
              {"IOPRIO_CLASS_NONE", { 0, "none" } },
              {"IOPRIO_CLASS_RT",   { 1, "realtime" } },
              {"IOPRIO_CLASS_BE",   { 2, "best-effort" } },
              {"IOPRIO_CLASS_IDLE", { 3, "idle"} }
            };

          oss << "=====> xcastor2.iopriomap" << std::endl
              << std::setfill(' ') << std::left
              << std::setw(header[0].length()) << header[0] << " | "
              << std::setw(header[1].length()) << header[1] << " | "
              << std::setw(header[2].length()) << header[2] << " | "
              << std::endl << std::setfill('-')
              << std::setw(header[0].length() + header[1].length() + header[2].length() + 10)
              << "" << std::setfill(' ')
              << std::endl;

          // Add default IO priority: best-effort, 4
          auto pair = mTxPriorityMap.insert(std::make_pair(
             XCASTOR2OFS_IOPRIO_UNKNOWN, std::make_pair((int)IOPRIO_CLASS_BE, 4)));

          oss << std::setw(header[0].length()) << XCASTOR2OFS_IOPRIO_UNKNOWN << " | "
              << std::setw(header[1].length()) << "best-effort" << " | "
              << std::setw(header[2].length()) << "4" << " | "
              << std::endl;

          // Entries are in the form of: tx_type io_class io_priority
          while ((val = config_stream.GetWord()))
          {
            tx_type = val;

            if (!(val = config_stream.GetWord()))
            {
              Eroute.Emsg("Config", "ioprio map wrong format, failed parsing ioprio class");
              rc = 1;
              break;
            }

            // IO can be a string or an int
            auto iter = io_class_map.find(val);

            if (iter != io_class_map.end())
              ioprio_class = iter->second.first;
            else
            {
              ioprio_class = strtol(val, NULL, 10);

              if (ioprio_class < 0 || ioprio_class > 3)
              {
                Eroute.Emsg("Config", "ioprio map wrong format, IO schedling class"
                            " needs to be between 0 and 3 inclussive");
                rc = 1;
                break;
              }

              for (iter = io_class_map.begin(); iter != io_class_map.end(); ++iter)
              {
                if (iter->second.first == ioprio_class)
                  break;
              }
            }

            if (!(val = config_stream.GetWord()))
            {
              Eroute.Emsg("Config", "ioprio map wrong format, failed parsing ioprio priority");
              rc = 1;
              break;
            }

            // IO priorities need to be values between 0 and 7
            ioprio_prio = strtol(val, NULL, 10);

            if (ioprio_prio < 0 || ioprio_prio > 7)
            {
              Eroute.Emsg("Config", "ioprio map wrong format, IO priority needs"
                          " to be between 0 and 7");
              rc = 1;
              break;
            }

            pair = mTxPriorityMap.insert(std::make_pair(tx_type,
              std::make_pair(ioprio_class, ioprio_prio)));

            // If insert successful then enable the use of IO priorities
            if (pair.second)
            {
              oss << std::setw(header[0].length()) << tx_type << " | "
                  << std::setw(header[1].length()) << iter->second.second << " | "
                  << std::setw(header[2].length()) << ioprio_prio << " | "
                  << std::endl;

              if (!mUseIoPriorities)
                mUseIoPriorities = true;
            }
          }

          // Print a summary if IO priorities are used
          if (mUseIoPriorities)
            Eroute.Say(oss.str().c_str());
        }

        // enable or disable fsync
        if (!strncmp(var, "sync", 4))
        {
          if (!(val = config_stream.GetWord()))
          {
            Eroute.Emsg("Config", "failed to parse sync option");
            rc = 1;
            break;
          }
          if (!(strcmp(val, "true")) || !(strcmp(val, "1")))
            mFSync = true;
          else
          {
            if (!(strcmp(val, "false")) || !(strcmp(val, "0")))
              mFSync = false;
            else
            {
              Eroute.Emsg("Config", "Error - argument 2 for sync invalid. "
                          "Can be <true>/1 or <false>/0");
              rc = 1;
              break;
            }
          }
        }
      }
    }

    config_stream.Close();
  }

  // Abort configuration if any errors so far
  if (rc)
  {
    Eroute.Emsg("Config", "configuration failed");
    return rc;
  }

  // Setup the circular in-memory logging buffer
  XrdOucString unit = "ds@";
  unit += XrdSysDNS::getHostName();
  unit += ":1094";
  Logging::Init();
  Logging::SetLogPriority(mLogLevel);
  Logging::SetUnit(unit.c_str());
  xcastor_info("msg=\"logging configured\"");
  // Parse the default XRootD directives
  rc = XrdOfs::Configure(Eroute, envP);
  // Set the effective user for all the XrdClients used to issue 'prepares'
  // to redirector
  setenv("XrdClientEUSER", "stage", 1);
  return rc;
}


//------------------------------------------------------------------------------
// Set the log level for the XRootD server daemon
//------------------------------------------------------------------------------
void
XrdxCastor2Ofs::SetLogLevel(int logLevel)
{
  if (mLogLevel != logLevel)
  {
    xcastor_notice("msg=\"update log level from %s to %s\"",
                   Logging::GetPriorityString(mLogLevel),
                   Logging::GetPriorityString(logLevel));
    mLogLevel = logLevel;
    Logging::SetLogPriority(mLogLevel);
  }
}


//------------------------------------------------------------------------------
// Checksum query is redirected to the manager node
//------------------------------------------------------------------------------
int
XrdxCastor2Ofs::chksum(csFunc Func, const char* csName, const char* Path,
		       XrdOucErrInfo& out_error, const XrdSecEntity* client,
		       const char* opaque)
{
  EPNAME("chksum");
  int ecode = 1094;
  XrdOucString redirect_mgr = "";

  // Reply to checksum capability request
  if (Func == XrdSfsFileSystem::csSize) {
    xcastor_debug("got checkusm size request");
    out_error.setErrCode(20);
    return SFS_OK;
  }

  // Redirect to to manager in case of a checksum request
  {
    XrdSysMutexHelper scope_lock(mManagerInfoMtx);

    if (mManagerHost.length() == 0) {
      xcastor_err("msg=\"manger info is empty, can not redirect\"");
      return Emsg(epname, out_error, ENOSYS, epname, Path);
    }

    ecode = mManagerPort;
    redirect_mgr = mManagerHost;
  }

  out_error.setErrInfo(ecode, redirect_mgr.c_str());
  return SFS_REDIRECT;
}


//------------------------------------------------------------------------------
// Stat file path
//------------------------------------------------------------------------------
int
XrdxCastor2Ofs::stat(const char* path,
                     struct stat* buf,
                     XrdOucErrInfo& einfo,
                     const XrdSecEntity* client,
                     const char* opaque)
{
  XrdOucString mask_opaque = Logging::MaskKey(opaque, "authz");
  xcastor_debug("path=%s, opaque=%s", path, mask_opaque.c_str());
  return XrdOfs::stat(path, buf, einfo, client, opaque);
}


//----------------------------------------------------------------------------
// Perform a filesystem control operation - in our case it returns the list
// of ongoing transfers in the current diskserver
//
// struct XrdSfsFSctl //!< SFS_FSCTL_PLUGIN/PLUGIO parameters
// {
//   const char            *Arg1;      //!< PLUGIO & PLUGIN
//         int              Arg1Len;   //!< Length
//         int              Arg2Len;   //!< Length
//   const char            *Arg2;      //!< PLUGIN opaque string
// };
//----------------------------------------------------------------------------
int
XrdxCastor2Ofs::FSctl(const int cmd,
                      XrdSfsFSctl& args,
                      XrdOucErrInfo& eInfo,
                      const XrdSecEntity* client)
{
  xcastor_debug("cmd=%i arg1=%s arg1len=%i arg2=%s arg2len=%i", cmd,
                (args.Arg1 ? args.Arg1: ""), args.Arg1Len,
                (args.Arg2 ? args.Arg2 : ""), args.Arg2Len);

  if (!client || !client->host || strncmp(client->host, "localhost", 9))
  {
    eInfo.setErrInfo(ENOTSUP, "operation possible only from localhost");
    return SFS_ERROR;
  }

  if (cmd == SFS_FSCTL_PLUGIO)
  {
    if (strncmp(args.Arg1, "/transfers", args.Arg1Len) == 0)
    {
      /*
      // Create authz object used to check the signature - requires authz
      // object which currently is not accessibe from XrdOfs i.e it's private
      XrdOucEnv tx_env(args.Arg2 ? args.Arg2 : 0, 0, client);

      if (client && ((XrdOfs*)this)->Authorization &&
          !((XrdOfs*)this)->Authorization->Access(client, args.Arg1, AOP_Read, tx_env))
       {
         return gSrv->Emsg(epname, eInfo, EACCES, "list transfers", pathp);
       }
      */
      std::ostringstream oss;
      oss << "[";

      {
        // Dump all the running transfers
        XrdSysMutexHelper scope_lock(mMutexTransfers);

        if (!mSetTransfers.empty())
        {
          std::set<std::string>::const_iterator iter = mSetTransfers.begin();
          oss << *iter ;

          for (++iter; iter != mSetTransfers.end(); ++iter)
            oss << ", " << *iter;
        }
      }

      oss << "]";
      size_t buff_sz = oss.str().length();
      // Ownership transferred to XrdOucBuffer
      char* buff = (char*)malloc(buff_sz + 1);
      size_t cpy_sz = strlcpy(buff, oss.str().c_str(), buff_sz + 1);

      if (cpy_sz != buff_sz)
      {
        xcastor_err("msg=\"failed copy response buff_sz=%i, cpy_sz=%i\"", buff_sz, cpy_sz);
        return SFS_ERROR;
      }

      // Ownership transferred to eInfo object
      XrdOucBuffer* xrd_buff = new XrdOucBuffer(buff, buff_sz + 1);
      eInfo.setErrInfo(xrd_buff->BuffSize(), xrd_buff);
      return SFS_DATA;
    }
    else
    {
      xcastor_err("msg=\"unkown type of command requested=%s\"", args.Arg1);
      eInfo.setErrInfo(ENOTSUP, "unknown query command");
      return SFS_ERROR;
    }
  }
  else
  {
    eInfo.setErrInfo(ENOTSUP, "operation not implemented");
    return SFS_ERROR;
  }
}


//------------------------------------------------------------------------------
// Add entry to the set of transfers
//------------------------------------------------------------------------------
bool
XrdxCastor2Ofs::AddTransfer(const std::string transfer_id)
{
  std::pair<std::set<std::string>::iterator, bool> ret;
  XrdSysMutexHelper scope_lock(mMutexTransfers);
  ret = mSetTransfers.insert(transfer_id);
  return ret.second;
}

//------------------------------------------------------------------------------
// Remove entry from the set of transfers
//------------------------------------------------------------------------------
size_t
XrdxCastor2Ofs::RemoveTransfer(const std::string transfer_id)
{
  if (!transfer_id.empty())
  {
    XrdSysMutexHelper scope_lock(mMutexTransfers);
    return mSetTransfers.erase(transfer_id);
  }

  return 0;
}

//------------------------------------------------------------------------------
// Get the scheduling class and the priority based on the type of transfer
//------------------------------------------------------------------------------
const std::pair<int, int>&
XrdxCastor2Ofs::GetDesiredIoPriority(const std::string& tx_type) const
{
  if (mTxPriorityMap.count(tx_type))
    return mTxPriorityMap.find(tx_type)->second;
  else
    return mTxPriorityMap.find(XCASTOR2OFS_IOPRIO_UNKNOWN)->second;
}


/******************************************************************************/
/*                         x C a s t o r O f s F i l e                        */
/******************************************************************************/


//------------------------------------------------------------------------------
// Constuctor
//------------------------------------------------------------------------------
XrdxCastor2OfsFile::XrdxCastor2OfsFile(const char* user, int MonID) :
  XrdOfsFile(user, MonID),
  mIsClosed(true),
  mIsRW(false),
  mHasWrite(false),
  mViaDestructor(false),
  mHasAdlerErr(false),
  mHasAdler(true),
  mIsTpc(false),
  mAdlerOffset(0),
  mPfnCastor(""),
  mXsValue(""),
  mXsType(""),
  mTpcKey(""),
  mReqId("0"),
  mTransferId(""),
  mTpcFlag(TpcFlag::kTpcNone),
  mTxType(XCASTOR2OFS_IOPRIO_UNKNOWN),
  mEnvOpaque(NULL)
{
  mAdlerXs = adler32(0L, Z_NULL, 0);
  std::pair<int, int> io_pair = gSrv->GetDesiredIoPriority(mTxType);
  mIoPriority = IOPRIO_PRIO_VALUE(io_pair.first, io_pair.second);
}

//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
XrdxCastor2OfsFile::~XrdxCastor2OfsFile()
{
  mViaDestructor = true;
  close();

  if (mEnvOpaque)
  {
    delete mEnvOpaque;
    mEnvOpaque = NULL;
  }

  // Remove entry from the set of transfers
  if (!mTransferId.empty() && !gSrv->RemoveTransfer(mTransferId)) {
    xcastor_warning("tx_id=%s msg=\"not in the set of transfers\"", mTransferId.c_str());
  }
}

//------------------------------------------------------------------------------
// Update manager host and port info if necessary
//------------------------------------------------------------------------------
void
XrdxCastor2Ofs::UpdateMgrInfo(XrdOucString& input)
{
  int pos = input.find(':');

  if (pos == STR_NPOS) {
    return;
  }

  XrdSysMutexHelper scope_lock(mManagerInfoMtx);
  XrdOucString mgr_host(input, 0, pos - 1);
  input.erase(0, pos + 1);
  int mgr_port = atoi(input.c_str());

  if (mgr_host.length() != 0) {
    if (mManagerHost != mgr_host) {
      mManagerHost = mgr_host;
      mManagerPort = mgr_port;
    }
  }
}

//------------------------------------------------------------------------------
// Open file
//------------------------------------------------------------------------------
int
XrdxCastor2OfsFile::open(const char*         path,
                         XrdSfsFileOpenMode  open_mode,
                         mode_t              create_mode,
                         const XrdSecEntity* client,
                         const char*         opaque)
{
  xcastor::Timing open_timing("open");
  TIMING("START", &open_timing);
  XrdOucString spath = path;
  XrdOucString newopaque = opaque;
  mIsTpcExec = false;

  if (!newopaque.length())
  {
    xcastor_err("msg=\"missing opaque information for path=%s\"", path);
    return gSrv->Emsg("open", error, EINVAL, "open due to missing opaque info");
  }

  // If there is explicit user opaque information we find two ?,
  // so we just replace it with a seperator
  while (newopaque.replace("?", "&")) { };
  while (newopaque.replace("&&", "&")) { };

  // Sanitize the opaque information and check if this is a TPC transfer
  SanitizeOpaque(newopaque);

  // Set the open flags and type of operation rd_only/rdwr
  mEnvOpaque = new XrdOucEnv(newopaque.c_str());
  mIsTpcExec = (mEnvOpaque->Get("castor.tpcexecutor") ? true : false);
  mPfnCastor = (mEnvOpaque->Get("castor.pfn1") ?
                mEnvOpaque->Get("castor.pfn1") : "");
  open_mode |= SFS_O_MKPTH;
  create_mode |= SFS_O_MKPTH;

  if (open_mode & (SFS_O_CREAT | SFS_O_TRUNC | SFS_O_WRONLY | SFS_O_RDWR))
    mIsRW = true;

  SetLogId(logId, error.getErrUser());
  XrdOucString mask_opaque = Logging::MaskKey(opaque, "authz");
  xcastor_info("path=%s, opaque=%s, isRW=%d, open_mode=%x, isTPCExec=%d, file_ptr=%p",
               path, mask_opaque.c_str(), mIsRW, open_mode, mIsTpcExec, this);
  // Update manager info
  XrdOucString mgr_info = (mEnvOpaque->Get("castor.manager") ?
			   mEnvOpaque->Get("castor.manager") : "") ;
  gSrv->UpdateMgrInfo(mgr_info);

  // A TPC executor is an xrdcp command running locally to execute the third-party
  // copy as a destination, where the destination is to be served by xrootd as opposed
  // to a local file in the filesystem. The case is to support Ceph as a backend.
  // Basically, we skip all checks and pass the open/close operations to the OSS layer.
  if (mIsTpcExec)
  {
    if (strstr(client->tident, "stage") == NULL || strstr(client->tident, "@localhost") == NULL)
    {
      xcastor_err("msg=\"TPC executor requires tident = stage@localhost\"");
      return SFS_ERROR;
    }
    int openrc = XrdOfsFile::open(path, open_mode, create_mode, client, "");
    xcastor_info("msg=\"TPC Executor open\" client=%s rc=%d", client->tident, openrc);
    mIsClosed = false;
    mPfnCastor = path;
    return openrc;
  }

  // Native TPC transfers are passed directly to the OFS layer
  if (mIsTpc)
  {
    if (PrepareTPC(path, newopaque, client))
      return SFS_ERROR;
  }
  else
  {
    if (ExtractTransferInfo(*mEnvOpaque))
      return SFS_ERROR;
  }

  // Fail if no physical file name set
  if (mPfnCastor.empty())
  {
    xcastor_err("msg=\"empty PFN castor mIsTpc=%i\"", mIsTpc);
    return SFS_ERROR;
  }

  // Build the transfer id if either this a normal transfer or a TPC transfer
  // in TpcSrcRead or TpcDstSetup state. It's not built in kTpcSrcCanDo and
  // kTpcSrcSetup.
  if (mTpcFlag == TpcFlag::kTpcNone ||
      mTpcFlag == TpcFlag::kTpcSrcRead ||
      mTpcFlag == TpcFlag::kTpcDstSetup)
  {
    BuildTransferId(client, mEnvOpaque);
  }

  // Enforce IO priorities if required
  if (gSrv->UseIoPriorities())
  {
    int old_ioprio = ioprio_get();
    std::pair<int, int> io_pair = gSrv->GetDesiredIoPriority(mTxType);
    int new_ioprio = IOPRIO_PRIO_VALUE(io_pair.first, io_pair.second);

    if (mIoPriority != new_ioprio)
      mIoPriority = new_ioprio;

    xcastor_debug("old_ioprio_class=%i, old_ioprio_prio=%i, new_ioprio_class=%i, "
                  "new_ioprio_prio=%i", IOPRIO_PRIO_CLASS(old_ioprio),
                  IOPRIO_PRIO_DATA(old_ioprio), IOPRIO_PRIO_CLASS(new_ioprio),
                  IOPRIO_PRIO_DATA(new_ioprio));
  }

  // Try to get the file checksum type and value if file exists
  if (!XrdOfsOss->Stat(mPfnCastor.c_str(), &mStatInfo, 0, mEnvOpaque))
  {
    int nattr = 0;
    char buf[32];

    // Check that checksum interface of Xroot is present
    if (0 == XrdSysXAttrActive) {
      xcastor_err("error getting Xattr plugin for path=%s, giving up", mPfnCastor.c_str());
      return SFS_ERROR;
    }
    
    nattr = XrdSysXAttrActive->Get("user.castor.checksum.type", buf, 32, mPfnCastor.c_str());
    if (nattr < 0) {
      xcastor_err("error getting user.castor.checksum.type xattr for path=%s, errno=%d",
                  mPfnCastor.c_str(), -nattr);
      return SFS_ERROR;
    }
    buf[nattr] = '\0';
    mXsType = buf;
    
    nattr = XrdSysXAttrActive->Get("user.castor.checksum.value", buf, 32, mPfnCastor.c_str());
    if (nattr < 0) {
      xcastor_err("error getting user.castor.checksum.value xattr for path=%s, errno=%d",
                  mPfnCastor.c_str(), -nattr);
      return SFS_ERROR;
    }
    buf[nattr] = '\0';
    mXsValue = buf;

    if (!mXsType.empty() || !mXsValue.empty())
      xcastor_debug("xs_type=%s xs_val=%s", mXsType.c_str(), mXsValue.c_str());
  }
  else
  {
    // Initial file size is 0
    mStatInfo.st_size = 0;
  }

  TIMING("OFS_OPEN", &open_timing);
  int openrc = XrdOfsFile::open(mPfnCastor.c_str(), open_mode, create_mode, client,
                                newopaque.c_str());
  int rc = openrc;

  // If response is SFS_STARTED then we return without calling the diskmanager
  // as the client will retry and this file object will be destroyed
  if (rc == SFS_STARTED)
  {
    xcastor_info("rc=%i msg=\"open delayed by the OFS layer, client will "
                 "retry\"", rc);
    return rc;
  }

  if (rc == SFS_OK)
    mIsClosed = false;

  // Notify the diskmanager
  if (!mTransferId.empty())
  {
    TIMING("NOTIFY_DM", &open_timing);
    char* errmsg = (char*)0;
    xcastor_debug("tx_id=\"%s\" errc=%i msg=\"contact diskmanager\"", mTransferId.c_str(), rc);
    int dm_errno = mover_open_file(gSrv->GetMoverPort(), mTransferId.c_str(), &rc, &errmsg);

    if (dm_errno)
    {
      // The diskmanager got a failure, return error
      rc = gSrv->Emsg("open", error, dm_errno, "open due to diskmanager error: ", errmsg);
    }
    else if (openrc)
    {
      // The local open failed, return unrecoverable error (kXR_NotAuthrized)
      error.setErrCode(openrc);
      rc = SFS_ERROR;
    }
    else
    {
      TIMING("ADD_TX", &open_timing);

      if (!gSrv->AddTransfer(mTransferId))
      {
        xcastor_err("msg=\"failed to insert tx_id=%s\"", mTransferId.c_str());
        rc = gSrv->Emsg("open", error, EIO, "add entry to transfer set");
      }
    }

    // Free memory
    free(errmsg);
  }

  if ((mTpcFlag == TpcFlag::kTpcDstSetup) && mIsRW && (rc == SFS_OK))
  {
    mHasWrite = true;
    // If this is a TPC destination and the TPC open in the OFS layer was successful,
    // we force the recomputation of the checksum at the end for local files (absolute paths)
    // as all writes go directly to the file without passing through the write method
    // in our plugin. Otherwise, if we have a Ceph path (relative), the checksum is actually
    // computed by the TPC Executor (see above and in the close method).
    mHasAdler = mPfnCastor[0] != '/';
  }

  if (gSrv->mLogLevel == LOG_DEBUG)
  {
    TIMING("DONE", &open_timing);
    open_timing.Print();
  }

  return rc; // -1 in case of any error, 0 for success
}


//------------------------------------------------------------------------------
// Close file
//------------------------------------------------------------------------------
int
XrdxCastor2OfsFile::close()
{
  int rc = SFS_OK;

  if (mIsClosed)
   return rc;

  mIsClosed = true;
  xcastor::Timing close_timing("close");
  TIMING("START", &close_timing);

  // If this is a TPC transfer then we can drop the key from the map
  if (mIsTpc && (mTpcFlag == TpcFlag::kTpcSrcSetup))
  {
    xcastor_debug("msg=\"drop from map tpc.key=%s\"", mTpcKey.c_str());
    XrdSysMutexHelper tpc_lock(gSrv->mTpcMapMutex);
    gSrv->mTpcMap.erase(mTpcKey);
    mTpcKey.clear();

    // Remove keys which are older than one hour
    std::map<std::string, struct TpcInfo>::iterator iter = gSrv->mTpcMap.begin();
    time_t now = time(NULL);

    while (iter != gSrv->mTpcMap.end())
    {
      if (now - iter->second.expire > 3600)
      {
        xcastor_debug("msg=\"expire tpc.key=%s\"", iter->first.c_str());
        gSrv->mTpcMap.erase(iter++);
      }
      else
      {
        ++iter;
      }
    }
  }

  TIMING("CLEAN_TPC", &close_timing);

  char ckSumbuf[32 + 1];
  sprintf(ckSumbuf, "%x", mAdlerXs);
  char* ckSumalg = "ADLER32";
  bool doNotSetCksum = false;
  if (mHasWrite)
  {
    // Get also the checksum of the file from the xattr in case of a TPC on Ceph, because the checksum was already stored
    if (mIsTpc && mHasAdler && (mAdlerXs == 1)) {
      // mAdlerXs == 1 means the file buffer was empty (as the transfer was served by the TPC executor)
      int nattr = XrdSysXAttrActive->Get("user.castor.checksum.value", ckSumbuf, 32, mPfnCastor.c_str());
      if (nattr < 0) {
        xcastor_err("error getting user.castor.checksum.value xattr for path=%s, errno=%d",
                    mPfnCastor.c_str(), -nattr);
        return SFS_ERROR;
      }

      ckSumbuf[nattr] = '\0';
      doNotSetCksum = true;
    }

    if (!mHasAdler)
    {
      // Rescan the file and compute checksum because the data stream came out-of-order
      xcastor_debug("path=%s msg=\"rescan file and compute checksum\"", mPfnCastor.c_str());
      char blk_xs_buf[512*1024];
      mAdlerXs = adler32(0L, Z_NULL, 0);
      mAdlerOffset = 0;
      XrdSfsFileOffset xs_offset = 0;
      XrdSfsXferSize xs_size = 0;

      while ((xs_size = XrdOfsFile::read(xs_offset, blk_xs_buf, sizeof(blk_xs_buf))) > 0)
      {
        mAdlerXs = adler32(mAdlerXs, (const Bytef*)blk_xs_buf, xs_size);
        xs_offset += xs_size;
      }

      if (xs_size == SFS_ERROR) {
        xcastor_err("path=%s msg=\"failed recomputing the checksum\"", mPfnCastor.c_str());
        rc = gSrv->Emsg("close", error, EIO, "checksumming file");
        // we failed to rescan the file, don't attempt to set the checksum and fail the transfer later on
        mHasAdler = false;
      }
      else {
        sprintf(ckSumbuf, "%x", mAdlerXs);
        mHasAdler = true;
      }
    }

    if(mHasAdler && !doNotSetCksum) {
      // Check that checksum interface of Xroot is present
      if (0 == XrdSysXAttrActive) {
        xcastor_err("error getting Xattr plugin for path=%s, giving up", mPfnCastor.c_str());
        rc = gSrv->Emsg("xattr", error, EEXIST, "Checking for Xattr plugin");
      } else {
        if (XrdSysXAttrActive->Set("user.castor.checksum.type", ckSumalg, strlen(ckSumalg), mPfnCastor.c_str())) {
          xcastor_err("path=%s unable to set xs type", mPfnCastor.c_str());
          rc = gSrv->Emsg("xattr", error, EIO, "set checksum type");
        } else {
          if (XrdSysXAttrActive->Set("user.castor.checksum.value", ckSumbuf, strlen(ckSumbuf), mPfnCastor.c_str())) {
            xcastor_err("path=%s unable to set xs value", mPfnCastor.c_str());
            rc = gSrv->Emsg("xattr", error, EIO, "set checksum");
          }
        }
      }
    }
  }

  TIMING("DONE_XS", &close_timing);
  int close_rc = 0;
  if(mHasWrite && gSrv->mFSync)
  {
    close_rc = XrdOfsFile::sync() || XrdOfsFile::close();
  }
  else
  {
    close_rc = XrdOfsFile::close();
  }
  if (rc == SFS_OK && close_rc == SFS_ERROR)
  {
    xcastor_err("path=%s msg=\"failed closing ofs file\" isTPCExec=%d", mPfnCastor.c_str(), mIsTpcExec);
    rc = gSrv->Emsg("close", error, EIO, "closing ofs file");
  }

  // The TPC executor is now done. For normal cases, deal with metadata and CASTOR
  if (!mIsTpcExec)
  {

    // This means the file was not properly closed
    if (mViaDestructor)
    {
      xcastor_err("path=%s msg=\"closed via destructor\"", mPfnCastor.c_str());
      rc = gSrv->Emsg("close", error, ECONNRESET, "close file - delete via destructor");
    }
  
    // This means the file was corrupted on read
    if (mHasAdlerErr)
    {
      xcastor_err("path=%s msg=\"checksum mismatch\"", mPfnCastor.c_str());
      rc = gSrv->Emsg("close", error, EIO, "read file - checksum mismatch");
    }
  
    uint64_t sz_file = 0;
  
    // Get the size of the file
    if (XrdOfsOss->Stat(mPfnCastor.c_str(), &mStatInfo, 0, mEnvOpaque))
    {
      // Failure to stat on write is an error, otherwise we ignore the error
      if (mHasWrite)
      {
        xcastor_err("path=%s msg=\"failed stat\"", mPfnCastor.c_str());
        rc = gSrv->Emsg("close", error, EIO, "stat file");
      }
    }
    else
      sz_file = mStatInfo.st_size;

    TIMING("NOTIFY_DM", &close_timing);
    if (!mTransferId.empty())
    {
      int errc = (rc ? error.getErrInfo() : rc);
      char* errmsg = (rc ? strdup(error.getErrText()) : (char*)0);
      xcastor_info("msg=\"send to diskmanager\" cksum=\"%s\" errc=%i errmsg=\"%s\"", ckSumbuf,
                   errc, (errmsg ? errmsg : ""));
      int dm_errno = mover_close_file(gSrv->GetMoverPort(), mReqId.c_str(), sz_file,
                                      const_cast<const char*>(ckSumalg),
                                      const_cast<const char*>(ckSumbuf),
                                      &errc, &errmsg);
  
      // If failed to commit to diskmanager then return error
      if (dm_errno)
      {
        xcastor_err("path=%s errmsg=\"%s\" msg=\"diskmanager error\"", mPfnCastor.c_str(), errmsg);
        rc = gSrv->Emsg("close", error, dm_errno, "close due to diskmanager error");
      }
  
      // Free errmsg memory
      free(errmsg);
    }
  }

  if (gSrv->mLogLevel == LOG_DEBUG)
  {
    TIMING("DONE", &close_timing);
    close_timing.Print();
  }

  return rc;
}


//------------------------------------------------------------------------------
// Build transfer identifier
//------------------------------------------------------------------------------
void
XrdxCastor2OfsFile::BuildTransferId(const XrdSecEntity* client, XrdOucEnv* env)
{
  std::string castor_path;
  std::string tident = client->tident;

  if (env && env->Get("castor.txtype"))
    mTxType = env->Get("castor.txtype");

  if (env && env->Get("castor.pfn1"))
    castor_path = env->Get("castor.pfn1");

  // Try to use the FQDN of the client origin if it is present
  if (client->host)
  {
    tident = tident.erase(tident.find("@") + 1);
    tident += client->host;
  }

  std::ostringstream oss;
  oss << "("
      << "\"" << tident << "\", "
      << "\"" << castor_path << "\", "
      << "\"" << mTxType << "\", "
      << "\"" << mIsRW << "\", "
      << "\"" << mReqId << "\""
      << ")";

  mTransferId = oss.str();
}


//------------------------------------------------------------------------------
// Prepare TPC transfer - save info in the global TPC map, do checks etc.
//------------------------------------------------------------------------------
int
XrdxCastor2OfsFile::PrepareTPC(const XrdOucString& path,
                               XrdOucString& opaque,
                               const XrdSecEntity* client)
{
  char* val;
  xcastor_debug("path=%s msg=\"logical path\"", path.c_str());

  // Get the current state of the TPC transfer
  if ((val = mEnvOpaque->Get("tpc.stage")))
  {
    if (strncmp(val, "placement", 9) == 0)
    {
      mTpcFlag = TpcFlag::kTpcSrcCanDo;
    }
    else if (strncmp(val, "copy", 4) == 0)
    {
      if (!(val = mEnvOpaque->Get("tpc.key")))
      {
        xcastor_err("msg=\"missing tpc.key information\"");
        return gSrv->Emsg("open", error, EACCES, "open - missing tpc.key info");
      }

      if ((val = mEnvOpaque->Get("tpc.lfn")))
      {
        mTpcFlag = TpcFlag::kTpcDstSetup;
      }
      else if ((val = mEnvOpaque->Get("tpc.dst")))
      {
        mTpcFlag = TpcFlag::kTpcSrcSetup;
        mTpcKey = mEnvOpaque->Get("tpc.key"); // save key only at this stage
      }
      else
      {
        xcastor_err("msg=\"missing tpc options in copy stage\"");
        return gSrv->Emsg("open", error, EACCES, "open - missing tpc options",
                          "in copy stage");
      }
    }
    else
    {
      xcastor_err("msg=\"unknown tpc.stage=%s\"", val);
      return gSrv->Emsg("open", error, EACCES, "open - unknown tpc.stage=", val);
    }
  }
  else if ((val = mEnvOpaque->Get("tpc.key")) &&
           (val = mEnvOpaque->Get("tpc.org")))
  {
    mTpcFlag = TpcFlag::kTpcSrcRead;
  }

  if (mTpcFlag == TpcFlag::kTpcSrcSetup)
  {
    // This is a source TPC file and we just received the second open reuqest
    // from the initiator. We save the mapping between the tpc.key and the
    // castor pfn for future open requests from the destination of the TPC transfer.
    if (!(val = mEnvOpaque->Get("castor.pfn1")))
    {
      xcastor_err("msg=\"missing castor.pfn1 info in TpcSrcSetup\"");
      return gSrv->Emsg("open", error, EACCES, "open - missing castor.pfn1 info");
    }

    std::string castor_pfn = val;
    std::string tpc_org = client->tident;
    tpc_org.erase(tpc_org.find(":"));
    tpc_org += "@";
    tpc_org += client->addrInfo->Name();;
    struct TpcInfo transfer = (struct TpcInfo)
    {
      castor_pfn,
      tpc_org,
      std::string(opaque.c_str()),
      time(NULL) + sKeyExpiry
    };
    {
      // Insert new key-transfer pair in the map
      XrdSysMutexHelper tpc_lock(gSrv->mTpcMapMutex);
      std::pair< std::map<std::string, struct TpcInfo>::iterator, bool> pair =
        gSrv->mTpcMap.insert(std::make_pair(mTpcKey, transfer));

      if (pair.second == false)
      {
        xcastor_err("tpc.key=%s msg=\"tpc key already in the map\"", mTpcKey.c_str());
        gSrv->mTpcMap.erase(pair.first);
        return gSrv->Emsg("open", error, EINVAL,
                          "tpc.key already in the map for file=", path.c_str());
      }
    }
  }
  else if (mTpcFlag == TpcFlag::kTpcSrcRead)
  {
    // This is a source TPC file and we just received the third open request which
    // comes directly from the destination of the transfer. Here we need to retrieve
    // the castor lfn from the map which we saved previously as the destination has
    // no knowledge of the castor physical file name.
    if (!(val = mEnvOpaque->Get("tpc.key")))
    {
      xcastor_err("msg=\"missing tpc.key info in TpcSrcRead\"");
      return gSrv->Emsg("open", error, EACCES, "open - missing tpc.key info");
    }

    std::string check_key = val;
    // Get init opaque just for stager job information and don't touch the current
    // opaque info as it contains the tpc.key and tpc.org info needed by the OFS
    // layer to perform the actual tpc transfer
    std::string init_opaque;
    {
      bool found_key = false;
      int num_tries = 100;
      XrdSysMutexHelper tpc_lock(gSrv->mTpcMapMutex);
      std::map<std::string, struct TpcInfo>::iterator iter =
        gSrv->mTpcMap.find(check_key);

      while ((num_tries > 0) && !found_key)
      {
        num_tries--;

        if (iter != gSrv->mTpcMap.end())
        {
          // Check if the key is still valid
          if (iter->second.expire > time(NULL))
          {
            if (!(val = mEnvOpaque->Get("tpc.org")))
            {
              xcastor_err("msg=\"missing tpc.org info in TpcSrcRead\"");
              return gSrv->Emsg("open", error, EACCES, "open - missing tpc.org info");
            }

            std::string tpc_org = val;

            if (tpc_org != iter->second.org)
            {
              xcastor_err("msg=\"tpc.org from dest=%s, not matching initiator orig=%s\"",
                          tpc_org.c_str(), iter->second.org.c_str());
              return gSrv->Emsg("open", error, EPERM, "PrepareTPC - destination "
                                "tpc.org not matching initiator origin");
            }

            mPfnCastor = iter->second.path.c_str();
            init_opaque = iter->second.opaque;
            found_key = true;
          }
          else
          {
            xcastor_err("msg=\"tpc key expired\"");
            return gSrv->Emsg("open", error, EKEYEXPIRED, "PrepareTPC - "
                              "tpc key expired");
          }
        }
        else
        {
          // Wait for the initiator to show up with the proper key and unlock
          // the map so that he can add the key.
          // A better implementation would be with a condition variable or an async
          // callback mechanism. Here we're spinning on the lock!
          gSrv->mTpcMapMutex.UnLock(); // <--
          XrdSysTimer timer;
          timer.Wait(200);
          xcastor_debug("msg=\"wait for initator to come with tpc.key=%s\"", check_key.c_str());
          gSrv->mTpcMapMutex.Lock();  // -->
          // check again if it's there
          iter = gSrv->mTpcMap.find(check_key);

        }
      }

      if (!found_key)
      {
        xcastor_err("msg=\"tpc key from destination not in map\"");
        return gSrv->Emsg("open", error, EINVAL, "PrepareTPC - can not find "
                          "tpc.key in map");
      }
    }

    // Now we can extract the transfer info and update the mEnvOpaque to the
    // inital value from the first request where we have the CASTOR opaque
    // information from the headnode
    delete mEnvOpaque;
    mEnvOpaque = new XrdOucEnv(init_opaque.c_str());

    if (ExtractTransferInfo(*mEnvOpaque))
      return SFS_ERROR;
  }
  else if (mTpcFlag == TpcFlag::kTpcDstSetup)
  {
    // Extract the original transfer info
    XrdOucEnv env_opaque(opaque.c_str());

    if (ExtractTransferInfo(env_opaque))
      return SFS_ERROR;
  }

  return SFS_OK;
}


//------------------------------------------------------------------------------
// Extract diskmanager info from the opaque data
//------------------------------------------------------------------------------
int
XrdxCastor2OfsFile::ExtractTransferInfo(XrdOucEnv& env_opaque)
{
  char* val = env_opaque.Get("castor.pfn2");

  if (!val || !strlen(val))
  {
    xcastor_err("msg=\"no diskmanager opaque information i.e. castor.pfn2 missing\"");
    return gSrv->Emsg("open", error, EINVAL, "get diskmanager opaque info (pfn2)");
  }

  size_t pos;
  std::string connect_info = val;

  // Two supported formats: old - <id:diskmanagerport:subReqId> and new <id:subReqId>
  if ((pos = connect_info.rfind(":")) != std::string::npos)
  {
    mReqId.assign(connect_info, pos + 1, std::string::npos);
  }
  else
  {
    xcastor_err("msg=\"castor.pfn2 not in the expected format\"");
    return gSrv->Emsg("open", error, EINVAL, "parse castor.pfn2");
  }

  return SFS_OK;
}


//----------------------------------------------------------------------------
//! Sanitize the opaque information
//----------------------------------------------------------------------------
void
XrdxCastor2OfsFile::SanitizeOpaque(XrdOucString& opaque)
{
  size_t pos;
  char delim = '&';
  std::istringstream iss(std::string(opaque.c_str()));
  std::string token, key, val;
  std::map<std::string, std::string> map_opaque;

  while (std::getline(iss, token, delim))
  {
    pos = token.find('=');

    // If not in "key=value" format then skip
    if (pos == std::string::npos)
      continue;

    key = token.substr(0, pos);
    val = token.substr(pos + 1);

    // We discard any "tried" tokens
    if (key == "tried")
      continue;

    // Detect if this is a tpc transfer
    if (key.find("tpc.") == 0)
      mIsTpc = true;

    std::pair<std::string, std::string> pair = std::make_pair(key, val);
    std::pair<std::map<std::string, std::string>::iterator, bool> ret_insert;
    ret_insert = map_opaque.insert(pair);

    // If element already in map, remove it and insert the last occurence
    // note: useful in case we have several castor.sfn tokens in the opaque info
    if (!ret_insert.second)
    {
      map_opaque.erase(ret_insert.first);
      ret_insert = map_opaque.insert(pair);
    }
  }

  std::ostringstream oss;
  oss << "&";

  // Construct new opaque info
  for (std::map<std::string, std::string>::iterator it = map_opaque.begin();
       it != map_opaque.end(); ++it)
  {
    oss << it->first << "=" << it->second << "&";
  }

  opaque = oss.str().c_str();
}


//------------------------------------------------------------------------------
// Verify checksum
//------------------------------------------------------------------------------
bool
XrdxCastor2OfsFile::VerifyChecksum()
{
  bool rc = true;
  std::string xs_val;
  std::string xs_type;

  if (!mXsValue.empty() && !mXsType.empty())
  {
    char ckSumbuf[32 + 1];
    sprintf(ckSumbuf, "%x", mAdlerXs);
    xs_val = ckSumbuf;
    xs_type = "ADLER32";

    if (xs_val != mXsValue)
    {
      gSrv->Emsg("VerifyChecksum", error, EIO, "access the file, checksum mismatch");
      rc = false;
    }

    if (xs_type != mXsType)
    {
      gSrv->Emsg("VerifyChecksum", error, EIO, "access the file, checksum type mismatch");
      rc = false;
    }

    if (!rc)
    {
      xcastor_err("msg=\"checksum %s != %s with algorithms [ %s <=> %s ]\"",
                  xs_val.c_str(), mXsValue.c_str(),
                  xs_type.c_str(), mXsType.c_str());
    }
    else
    {
      xcastor_debug("xs_val=%s msg=\"checksum OK\"", xs_val.c_str());
    }
  }

  return rc;
}


//------------------------------------------------------------------------------
// Read
//------------------------------------------------------------------------------
int
XrdxCastor2OfsFile::read(XrdSfsFileOffset fileOffset,   // Preread only
                         XrdSfsXferSize   amount)
{
  int rc = XrdOfsFile::read(fileOffset, amount);
  mHasAdler = false;
  return rc;
}


//------------------------------------------------------------------------------
// Read
//------------------------------------------------------------------------------
XrdSfsXferSize
XrdxCastor2OfsFile::read(XrdSfsFileOffset fileOffset,
                         char*            buffer,
                         XrdSfsXferSize   buffer_size)
{
  // Set IO priority if enforced
  if (gSrv->UseIoPriorities())
    SetIoPriority();

  // If we once got an adler checksum error, we fail all reads
  if (mHasAdlerErr)
  {
    xcastor_err("off=%ll size=%i msg=\"found xs error, fail read\"",
                fileOffset, buffer_size);
    return SFS_ERROR;
  }

  int rc = XrdOfsFile::read(fileOffset, buffer, buffer_size);

  // Disable adler checksum if the read failed or there was a seek
  if ((rc == -1) || (fileOffset != mAdlerOffset))
    mHasAdler = false;

  if (mHasAdler)
  {
    mAdlerXs = adler32(mAdlerXs, (const Bytef*) buffer, rc);

    if (rc > 0)
    {
      mAdlerOffset += rc;

      if (fileOffset + rc >= mStatInfo.st_size)
      {
        // Invoke the checksum verification
        if (!VerifyChecksum())
        {
          mHasAdlerErr = true;
          rc = SFS_ERROR;
        }
      }
    }
  }

  return rc;
}


//------------------------------------------------------------------------------
// Read
//------------------------------------------------------------------------------
int
XrdxCastor2OfsFile::read(XrdSfsAio* aioparm)
{
  int rc = XrdOfsFile::read(aioparm);
  mHasAdler = false;
  return rc;
}


//------------------------------------------------------------------------------
// Write
//------------------------------------------------------------------------------
XrdSfsXferSize
XrdxCastor2OfsFile::write(XrdSfsFileOffset fileOffset,
                          const char*      buffer,
                          XrdSfsXferSize   buffer_size)
{
  // Set IO priority if enforced
  if (gSrv->UseIoPriorities())
    SetIoPriority();

  int rc = XrdOfsFile::write(fileOffset, buffer, buffer_size);

  // Computation of adler checksum - we disable it if seek happened
  if (fileOffset != mAdlerOffset)
    mHasAdler = false;

  if (mHasAdler)
  {
    mAdlerXs = adler32(mAdlerXs, (const Bytef*) buffer, buffer_size);

    if (rc > 0)
      mAdlerOffset += rc;
  }

  mHasWrite = true;
  return rc;
}


//------------------------------------------------------------------------------
// Write
//------------------------------------------------------------------------------
int
XrdxCastor2OfsFile::write(XrdSfsAio* aioparm)
{
  // Set IO priority if enforced
  if (gSrv->UseIoPriorities()) {
    mIoPriority = aioparm->sfsAio.aio_reqprio;
    SetIoPriority();
  }

  int rc = XrdOfsFile::write(aioparm);

  // Computation of adler checksum - we disable it if seek happened
  if (aioparm->sfsAio.aio_offset != mAdlerOffset)
    mHasAdler = false;

  if (mHasAdler)
  {
    mAdlerXs = adler32(mAdlerXs, (const Bytef*) aioparm->sfsAio.aio_buf, aioparm->sfsAio.aio_nbytes);
    mAdlerOffset += aioparm->sfsAio.aio_nbytes;
  }

  mHasWrite = true;
  return rc;
}


//----------------------------------------------------------------------------
// Set the IO priority corresponding to this transfer type
//----------------------------------------------------------------------------
void
XrdxCastor2OfsFile::SetIoPriority() const
{
  errno = 0;

  if (ioprio_set(mIoPriority))
    xcastor_err("failed to set IO priority for path=%s, errno=%i",
                mPfnCastor.c_str(), errno);
}
