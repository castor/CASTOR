/*******************************************************************************
 *                      XrdxCastorIoPrio.hh
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2012  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *
 ******************************************************************************/

#include <sys/syscall.h>

#ifdef SYS_ioprio_set
#ifndef IOPRIO_BITS
const int IOPRIO_BITS = 16;
const int IOPRIO_CLASS_SHIFT = 13;
const int IOPRIO_PRIO_MASK = (1UL << IOPRIO_CLASS_SHIFT) - 1;

int IOPRIO_PRIO_CLASS(int mask)
{
  return ((mask) >> IOPRIO_CLASS_SHIFT);
}

int IOPRIO_PRIO_DATA(int mask)
{
  return ((mask) & IOPRIO_PRIO_MASK);
}

int IOPRIO_PRIO_VALUE(int cls, int data)
{
  return (((cls) << IOPRIO_CLASS_SHIFT) | data);
}
#endif

namespace {
#ifndef IOPRIO_CLASS_IDLE
  enum {
    IOPRIO_CLASS_NONE,
    IOPRIO_CLASS_RT,
    IOPRIO_CLASS_BE,
    IOPRIO_CLASS_IDLE
  };
#endif

#ifndef IOPRIO_WHO_PROCESS
  enum {
    IOPRIO_WHO_PROCESS = 1,
    IOPRIO_WHO_PGRP,
    IOPRIO_WHO_USER
  };
#endif
}
#endif

//------------------------------------------------------------------------------
//! Get ioprio function
//!
//! @param which determines how who is interpreted, and has one of the following
//!        values:
//!
//!        IOPRIO_WHO_PROCESS - 'who' is a process ID or thread ID identifying a
//!        single process or thread.  If 'who' is 0, then operate on the calling
//!        thread.
//!
//!        IOPRIO_WHO_PGRP - 'who' is a process group ID identifying all the
//!        members of a process group.  If 'who' is 0, then operate on the
//!        process group of which the caller is a member.
//!
//!        IOPRIO_WHO_USER - 'who' is a user ID identifying all of the processes
//!        that have a matching real UID.
//!
//! @param who can be 0, the process ID or the thread ID
//!
//! @return -1 on error and errno is set to indicate the error, otherwise the
//!        ioprio value. By default it return the ioprio of the current thread.
//------------------------------------------------------------------------------
static inline int ioprio_get(int which = IOPRIO_WHO_PROCESS, int who = 0)
{
  return syscall(SYS_ioprio_get, which, who);
}


//------------------------------------------------------------------------------
//! Get ioprio function
//!
//! @param which same as for ioprio_get
//! @param who same as for ioprio_get
//! @param ioprio a bit mask that specifies both the scheduling class and the
//!        priority to be assigned to the target process(es). The following
//!        macros are used for assembling and dissecting ioprio values:
//!
//!        IOPRIO_PRIO_VALUE(class, data) - given a scheduling class and priority
//!        (data), this macro combines the two values to produce an ioprio value,
//!        which is returned as the result of the macro.
//!
//!        IOPRIO_PRIO_CLASS(mask) - given mask (an ioprio value), this macro
//!        returns its I/O class component, that is, one of the values
//!        IOPRIO_CLASS_RT, IOPRIO_CLASS_BE, or IOPRIO_CLASS_IDLE.
//!
//!        IOPRIO_PRIO_DATA(mask) - given mask (an ioprio value), this macro
//!        returns its priority (data) component.
//!
//! @return -1 on error and errno is set to indicate the error, otherwise 0. By
//!        default set the ioprio of the current thread
//------------------------------------------------------------------------------
static inline int ioprio_set(int ioprio, int which=IOPRIO_WHO_PROCESS, int who=0)
{
 return syscall(SYS_ioprio_set, which, who, ioprio);
}
