/*******************************************************************************
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2012  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *
 ******************************************************************************/

#pragma once

#define XCASTOR2FS_MAXFILESYSTEMS    8192
#define XCASTOR2FS_MAXDISTINCTUSERS  8192

//! Timeout during which we received the response for an async request from
//! stager and the client need to show up to collect it
#define XCASTOR2FS_RESP_TIMEOUT      360

//! Maximum number of async requests in-fligth
#define XCASTOR2FS_MAX_REQUESTS      50000

//! Default value for the DiskManager MoverHandler Port on the diskserver
#define XCASTOR2OFS_MOVERHANDLER_PORT 15511

//! Default IO priority tag
#define XCASTOR2OFS_IOPRIO_UNKNOWN "unknown"
