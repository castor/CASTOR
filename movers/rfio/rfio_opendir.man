.\"
.\" $Id: rfio_opendir.man,v 1.7 2007/09/10 13:40:52 obarring Exp $
.\"
.\" Copyright (C) 1999-2001 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH RFIO_OPENDIR "3castor" "$Date: 2007/09/10 13:40:52 $" CASTOR "Rfio Library Functions"
.SH NAME
rfio_opendir \- open a directory
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "rfio_api.h"\fR
.sp
.BI "RDIR *rfio_opendir (const char *" path ");"
.SH DESCRIPTION
.B rfio_opendir
opens a directory to be used in subsequent
.B rfio_readdir
operations.
A
.B RDIR
structure and a buffer to cache the directory entries are allocated in the
client API.
.TP
.I path
specifies the logical pathname relative to the current directory or
the full pathname.
.SH NOTES
For CASTOR directories, a multi-threaded application will need to initialize itself the Cthread (CASTOR Thread Interface) library ; this is done by including "shift/Cthread_api.h" and calling the function Cthread_init() at the beginning. Otherwise accessing CASTOR directories will not be thread-safe. See \fBCthread\fP(3).
.SH RETURN VALUE
This routine returns a pointer to be used in the subsequent directory
function calls if the operation was successful or NULL if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOENT
A component of
.I path
prefix does not exist or
.I path
is a null pathname.
.TP
.B EACCES
Search permission is denied on a component of the
.I path
prefix or read permission is denied on
.IR path .
.TP
.B EFAULT
.I path
is a NULL pointer.
.TP
.B ENOTDIR
A component of
.I path
prefix is not a directory.
.TP
.B ENAMETOOLONG
The length of
.I path
exceeds
.B CA_MAXPATHLEN
or the length of a
.I path
component exceeds
.BR CA_MAXNAMELEN .
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.SH MULTI-THREAD APPLICATION USAGE
When calling RFIO from a multi-threaded application, it is first necessary to
initialize thread-specific storage and serialization objects by a call to
\fBCthread_init()\fP.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR rfio_closedir(3) ,
.BR rfio_readdir(3) ,
.BR rfio_rewinddir(3) ,
.BR Cthread(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
