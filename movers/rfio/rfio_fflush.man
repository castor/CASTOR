.\"
.\" $Id: rfio_fflush.man,v 1.4 2007/09/10 13:40:52 obarring Exp $
.\"
.\" Copyright (C) 1999-2001 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH RFIO_FFLUSH "3castor" "$Date: 2007/09/10 13:40:52 $" CASTOR "Rfio Library Functions"
.SH NAME
rfio_fflush \- flush a file
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "rfio_api.h"\fR
.sp
.BI "int rfio_fflush (FILE *" fp ");"
.SH DESCRIPTION
.B rfio_fflush
forces a write of all buffered data for the file whose descriptor \fBfp\fP is
the one returned by
.B rfio_fopen.
The file remains open.
.SH RETURN VALUE
This routine returns 0 if successful, -1 if the operation failed and
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B EBADF
.I fp
is not a valid file descriptor.
.TP
.B SEBADVERSION
Version ID mismatch.
.SH MULTI-THREAD APPLICATION USAGE
When calling RFIO from a multi-threaded application, it is first necessary to
initialize thread-specific storage and serialization objects by a call to
\fBCthread_init()\fP.
.SH SEE ALSO
.BR rfio_fopen(3) , Cthread(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
