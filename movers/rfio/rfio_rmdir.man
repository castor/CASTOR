.\"
.\" $Id: rfio_rmdir.man,v 1.6 2007/09/10 13:40:51 obarring Exp $
.\"
.\" Copyright (C) 1999-2001 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH RFIO_RMDIR "3castor" "$Date: 2007/09/10 13:40:51 $" CASTOR "Rfio Library Functions"
.SH NAME
rfio_rmdir \- remove a directory
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "rfio_api.h"\fR
.sp
.BI "int rfio_rmdir (const char *" path ");"
.SH DESCRIPTION
.B rfio_rmdir
removes a directory if it is empty.
.TP
.I path
specifies the logical pathname relative to the current directory or
the full pathname.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOENT
The named directory does not exist or is a null pathname.
.TP
.B EACCES
Search permission is denied on a component of the
.IR path
prefix or write permission is denied on the parent directory or
the parent has the sticky bit S_ISVTX set and
.RS 1.5i
.LP
the effective user ID of the requestor does not match the owner ID of the directory and
.LP
the effective user ID of the requestor does not match the owner ID of the
parent directory and
.LP
the requestor is not super-user.
.RE
.TP
.B EFAULT
.I path
is a NULL pointer.
.TP
.B EEXIST
The named directory is not empty.
.TP
.B ENOTDIR
A component of
.I path
prefix is not a directory.
.TP
.B EINVAL
.I path
is the current directory.
.TP
.B ENAMETOOLONG
The length of
.I path
exceeds
.B CA_MAXPATHLEN
or the length of a
.I path
component exceeds
.BR CA_MAXNAMELEN .
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.SH MULTI-THREAD APPLICATION USAGE
When calling RFIO from a multi-threaded application, it is first necessary to
initialize thread-specific storage and serialization objects by a call to
\fBCthread_init()\fP.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR rfio_mkdir(3) ,
.BR Cthread(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
