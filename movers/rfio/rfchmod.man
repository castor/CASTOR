.\"
.\" $Id: rfchmod.man,v 1.3 2009/01/14 17:39:21 sponcec3 Exp $
.\"
.\" Copyright (C) 1998-2002 by IN2P3 CC
.\" All rights reserved
.\"
.TH RFCHMOD "1castor" "$Date: 2009/01/14 17:39:21 $" CASTOR "Rfio User Commands"
.SH NAME
rfchmod \- change file access permission
.SH SYNOPSIS
.B rfchmod
.IR mode
.IR file...
.SH DESCRIPTION
.IX "\fLrfchmod\fR"
.B rfchmod
provides an interface to the
.B shift
remote file I/O daemon (rfiod) for changing the file access permission, so called
file mode.
.TP
.I mode
argument allows users to specify the file access permission applied to the file.
Only the absolute octal representation of the file access permission is supported.
For values of mode, see
.BR chmod(1) .
.TP
.I file
argument is either a remote file name of the form:
.RS
.RS
.HP
.IB hostname : path
.RE
.LP
or a local file name (not containing the :/ character combination).
.RE
.LP
Modification of the file access permission require authorisation.
.LP
For castor files we can use as remote file the new Turl sintax, there are two way to specify it:
.LP
 'rfio://[stagehost][:port]/?[svcClass=MySvcClass&]path=/castor/cern.ch/user/n/nobody/Myfile' 
.LP
.B or
.LP
 'rfio://[stagehost][:port]/[/castor/cern.ch/user/n/nobody/file][?[svcClass=MySvcClass]]'
.LP
If there is the pamameter "path", it must be the last one. The order of the other option is not taken into account.
.LP
The path must be specified as path in the parameters or before the parameters.
Of course the old Rfio sintax (rfio://[diskserver][:port]/path) is still valid.
.LP

.SH "SEE ALSO"
.BR chmod(1),
.BR rfio_chmod(3).
.SH "AUTHOR"
\fBCASTOR\fP Team <castor.support@cern.ch>
