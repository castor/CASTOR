.\"
.\" $Id: rfmkdir.man,v 1.8 2009/01/14 17:39:21 sponcec3 Exp $
.\"
.\" @(#)rfmkdir.man	1.1 09/07/98     CERN IT-PDP/DM Olof Barring
.\" Copyright (C) 1998-2002 by CERN/IT/PDP
.\" All rights reserved
.\"
.TH RFMKDIR "1castor" "$Date: 2009/01/14 17:39:21 $" CASTOR "Rfio User Commands"
.SH NAME
rfmkdir \- make remote directories
.SH SYNOPSIS
.B rfmkdir
[
.BI -m
.IR mode
]
[
.IB -p
]
.IR directory...
.SH DESCRIPTION
.IX "\fLrfmkdir\fR"
.B rfmkdir
provides an interface to the
.B shift
remote file I/O daemon (rfiod) for creating remote directories in mode 777
(possibly altered by
.BR umask(1) ).
The
.IR directory
argument is either a remote directory name of the form:
.IP
.IB hostname : path
.LP
or a local directory name (not containing the :/ character combination). Creation of a directory
requires write permission in the parent directory.
.LP

For castor files we can use as remote file the new Turl sintax, there are two way to specify it:
.LP
 'rfio://[stagehost][:port]/?[svcClass=MySvcClass&]path=/castor/cern.ch/user/n/nobody/Myfile' 
.LP
.B or
.LP
 'rfio://[stagehost][:port]/[/castor/cern.ch/user/n/nobody/file][?[svcClass=MySvcClass]]'
.LP
If there is the pamameter "path", it must be the last one. The order of the other option is not taken into account.
.LP
The path must be specified as path in the parameters or before the parameters.
Of course the old Rfio sintax (rfio://[diskserver][:port]/path) is still valid.
.LP
.SH "OPTIONS"
The following options apply to
.B rfmkdir:
.TP
.BI \-m " mode"
This option allows users to specify the mode to be used for the new directories.
Choices for modes can be found in
.BI chmod(1) .
.TP
.BI \-p
With this option,
.B rfmkdir
creates
.B dirname
by creating all the non-existing parent directories first. If the
.B dirname
already exists no error will be reported.
.SH "SEE ALSO"
.BR mkdir(1),
.BR umask(1),
.BR mkdir(2),
.BR rfiod(1)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
