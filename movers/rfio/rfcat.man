.\"
.\" $Id: rfcat.man,v 1.3 2009/01/14 17:39:21 sponcec3 Exp $
.\"
.\" Copyright (C) 2001-2002 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH RFCAT "1castor" "$Date: 2009/01/14 17:39:21 $" CASTOR "Rfio User Commands"
.SH NAME
rfcat \- Remote file concatenation to standard output
.SH SYNOPSIS
.B rfcat
.IR filename1
.IR [filename2[...]]
.SH DESCRIPTION
.IX "\fLrfcat\fR"
The remote file I/O concatenation program provides an interface to the
.B CASTOR
remote file I/O daemon (rfiod) for concatenating files from remote and/or
local hosts to standard output. Each
.IR filename
argument is either a remote file name of the form:
.IP
.IB hostname : path
.LP
or a local file name (not containing the :/ character combination)
.LP
or an HSM filename /castor/...
.LP
With no filename, or when
.I filename
is -, read standard input.

.LP
For castor files we can use as remote file the new Turl syntax, there are two way to specify it:
.LP
 'rfio://[stagehost][:port]/?[svcClass=MySvcClass&]path=/castor/cern.ch/user/n/nobody/Myfile' 
.LP
.B or
.LP
 'rfio://[stagehost][:port]/[/castor/cern.ch/user/n/nobody/file][?[svcClass=MySvcClass]]'
.LP
If there is the pamameter "path", it must be the last one. The order of the other option is not taken into account.
.LP
The path must be specified as path in the parameters or before the parameters.
Of course the old Rfio sintax (rfio://[diskserver][:port]/path) is still valid.
.LP

.SH RETURN CODES
\
.br
0	Ok.
.br
1	Bad parameter.
.br
2	System error.
.br
16	Device or resource busy.
.br
28	No space left on device.
.br
196	Request killed.
.SH SEE ALSO
.BR cat(1), 
.BR rfiod(1)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
