.\"
.\" $Id: rfio_statfs.man,v 1.3 2007/09/10 13:40:51 obarring Exp $
.\"
.\" Copyright (C) 1999-2001 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH RFIO_STATFS "3castor" "$Date: 2007/09/10 13:40:51 $" CASTOR "Rfio Library Functions"
.SH NAME
rfio_statfs \- get information about a mounted filesystem
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "rfio_api.h"\fR
.sp
.BI "int rfio_statfs (const char *" path ", struct rfstatfs *" statfsbuf ");"
.SH DESCRIPTION
.B rfio_statfs
gets information about a mounted filesystem.
.TP 0.8i
.I path
specifies the logical pathname relative to the current directory or
the full pathname of any file in this filesystem.
.TP
.I statfsbuf
is a pointer to a rfstatfs structure:
.nf
.ft CW
struct rfstatfs {
        long totblks  ;      /* Total number of blocks       */
        long freeblks ;      /* Number of free blocks        */
        long bsize    ;      /* Block size                   */
        long totnods  ;      /* Total number of inodes       */
        long freenods ;      /* Number of free inodes        */
};
.ft
.fi
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOENT
The named file/directory does not exist or is a null pathname.
.TP
.B EACCES
Search permission is denied on a component of the
.I path
prefix.
.TP
.B EFAULT
.I path
or
.I statfsbuf
is a NULL pointer.
.TP
.B ENOTDIR
A component of
.I path
prefix is not a directory.
.TP
.B ENAMETOOLONG
The length of
.I path
exceeds
.B CA_MAXPATHLEN
or the length of a
.I path
component exceeds
.BR CA_MAXNAMELEN .
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.SH MULTI-THREAD APPLICATION USAGE
When calling RFIO from a multi-threaded application, it is first necessary to
initialize thread-specific storage and serialization objects by a call to
\fBCthread_init()\fP.
.SH SEE ALSO
.BR Castor_limits(4) , Cthread(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
