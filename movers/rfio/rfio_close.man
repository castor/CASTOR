.\"
.\" $Id: rfio_close.man,v 1.7 2007/09/10 13:40:52 obarring Exp $
.\"
.\" Copyright (C) 1999-2001 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH RFIO_CLOSE "3castor" "$Date: 2007/09/10 13:40:52 $" CASTOR "Rfio Library Functions"
.SH NAME
rfio_close \- closes a file
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "rfio_api.h"\fR
.sp
.BI "int rfio_close (int " s ");"
.SH DESCRIPTION
.B rfio_close
closes the file whose descriptor \fBs\fP is the one returned by
.B rfio_open.
.SH RETURN VALUE
This routine returns 0 if successful, -1 if the operation failed and
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B EBADF
.I s
is not a valid file descriptor.
.TP
.B ENOSPC
File system full when writing last blocks (V3 only).
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SETIMEDOUT
Timed out.
.TP
.B SEBADVERSION
Version ID mismatch.
.TP
.B SEINTERNAL
Internal error.
.TP
.B SECONNDROP
Connection closed by remote end.
.TP
.B SECOMERR
Communication error.
.SH MULTI-THREAD APPLICATION USAGE
When calling RFIO from a multi-threaded application, it is first necessary to
initialize thread-specific storage and serialization objects by a call to
\fBCthread_init()\fP.
.SH SEE ALSO
.BR rfio_open(3) , Cthread(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
