/*
 * $Id: statfs.c,v 1.11 2008/07/31 07:09:14 sponcec3 Exp $
 */

/*
 * Copyright (C) 1990-1999 by CERN/IT/PDP/DM
 * All rights reserved
 */

/* statfs.c       Remote File I/O - get file system status     */

#define RFIO_KERNEL 1
#include "rfio.h"               /* Remote File I/O general definitions  */
#include <string.h>

int rfio_statfs(char    *path,               /* remote file path                     */
                struct rfstatfs *statfsbuf)     /* status buffer (subset of local used) */
{
  char     buf[BUFSIZ];       /* General input/output buffer          */
  register int    s;      /* socket descriptor            */
  int      status;
  int      len;
  char     *host, *filename;
  char     *p=buf;
  int   rt ;
  int  rcode, parserc ;

  INIT_TRACE("RFIO_TRACE");
  TRACE(1, "rfio", "rfio_statfs(%s, %x)", path, statfsbuf);

  if (!(parserc = rfio_parse(path,&host,&filename))) {
    /* if not a remote file, must be local  */
    TRACE(1, "rfio", "rfio_statfs:  using local statfs(%s, %x)",
          filename, statfsbuf);

    END_TRACE();
    rfio_errno = 0;
    return(rfstatfs(filename , statfsbuf));
  }
  if (parserc < 0) {
    END_TRACE();
    return(-1);
  }

  len = strlen(path)+1;
  if ( RQSTSIZE+len > BUFSIZ ) {
    TRACE(2,"rfio","rfio_statfs: request too long %d (max %d)",
          RQSTSIZE+len,BUFSIZ);
    END_TRACE();
    serrno = E2BIG;
    return(-1);
  }

  s = rfio_connect(host,&rt);
  if (s < 0)      {
    END_TRACE();
    return(-1);
  }

  marshall_WORD(p, RFIO_MAGIC);
  marshall_WORD(p, RQST_STATFS);
  marshall_LONG(p, len);
  p= buf + RQSTSIZE;
  marshall_STRING(p, filename);
  TRACE(2,"rfio","rfio_statfs: sending %d bytes",RQSTSIZE+len) ;
  if (netwrite_timeout(s,buf,RQSTSIZE+len,RFIO_CTRL_TIMEOUT) != (RQSTSIZE+len)) {
    TRACE(2, "rfio", "rfio_statfs: write(): ERROR occured (errno=%d)", errno);
    (void) close(s);
    END_TRACE();
    return(-1);
  }
  p = buf;
  TRACE(2, "rfio", "rfio_statfs: reading %d bytes", 7*LONGSIZE);
  if (netread_timeout(s, buf, 7*LONGSIZE, RFIO_CTRL_TIMEOUT) != (7*LONGSIZE))  {
    TRACE(2, "rfio", "rfio_statfs: read(): ERROR occured (errno=%d)", errno);
    (void) close(s);
    END_TRACE();
    return(-1);
  }
  unmarshall_LONG( p, statfsbuf->bsize ) ;
  unmarshall_LONG( p, statfsbuf->totblks ) ;
  unmarshall_LONG( p, statfsbuf->freeblks ) ;
  unmarshall_LONG( p, statfsbuf->totnods ) ;
  unmarshall_LONG( p, statfsbuf->freenods ) ;
  unmarshall_LONG( p, status ) ;
  unmarshall_LONG( p, rcode ) ;

  TRACE(1, "rfio", "rfio_statfs: return %d",status);
  rfio_errno = rcode ;
  (void) close(s);
  END_TRACE();
  return (status);
}
