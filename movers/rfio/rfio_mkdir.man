.\"
.\" $Id: rfio_mkdir.man,v 1.7 2007/09/10 13:40:52 obarring Exp $
.\"
.\" Copyright (C) 1999-2001 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH RFIO_MKDIR "3castor" "$Date: 2007/09/10 13:40:52 $" CASTOR "Rfio Library Functions"
.SH NAME
rfio_mkdir \- create a new directory
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "rfio_api.h"\fR
.sp
.BI "int rfio_mkdir (const char *" path ", mode_t " mode ");"
.SH DESCRIPTION
.B rfio_mkdir
creates a new directory.
.LP
An entry is created and the directory's owner ID
is set to the effective user ID of the requestor.
The group ID of the directory is set to the effective group ID of the requestor.
.TP
.I path
specifies the logical pathname relative to the current directory or
the full pathname.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOENT
A component of
.I path
prefix does not exist or
.I path
is a null pathname.
.TP
.B EACCES
Search permission is denied on a component of the
.I path
prefix or write permission on the parent directory is denied.
.TP
.B EFAULT
.I path
is a NULL pointer.
.TP
.B EEXIST
.I path
exists already.
.TP
.B ENOTDIR
A component of
.I path
prefix is not a directory.
.TP
.B ENOSPC
No space to store the new directory.
.TP
.B ENAMETOOLONG
The length of
.I path
exceeds
.B CA_MAXPATHLEN
or the length of a
.I path
component exceeds
.BR CA_MAXNAMELEN .
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.SH MULTI-THREAD APPLICATION USAGE
When calling RFIO from a multi-threaded application, it is first necessary to
initialize thread-specific storage and serialization objects by a call to
\fBCthread_init()\fP.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR rfio_chmod(3) ,
.BR Cthread(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
