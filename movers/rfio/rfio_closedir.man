.\"
.\" $Id: rfio_closedir.man,v 1.6 2007/09/10 13:40:52 obarring Exp $
.\"
.\" Copyright (C) 1999-2001 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH RFIO_CLOSEDIR "3castor" "$Date: 2007/09/10 13:40:52 $" CASTOR "Rfio Library Functions"
.SH NAME
rfio_closedir \- close directory opened by
.B rfio_opendir
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "rfio_api.h"\fR
.sp
.BI "int rfio_closedir (RDIR *" dirp ");"
.SH DESCRIPTION
.B rfio_closedir
closes the directory opened by
.B rfio_opendir
and associated with the
.B RDIR
structure pointed by
.IR dirp .
.TP
.I dirp
specifies the pointer value returned by
.BR rfio_opendir .
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B EBADF
File descriptor in DIR structure is invalid.
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SETIMEDOUT
Timed out.
.TP
.B SEBADVERSION
Version ID mismatch.
.TP
.B SECOMERR
Communication error.
.SH MULTI-THREAD APPLICATION USAGE
When calling RFIO from a multi-threaded application, it is first necessary to
initialize thread-specific storage and serialization objects by a call to
\fBCthread_init()\fP.
.SH SEE ALSO
.BR rfio_opendir(3) ,
.BR rfio_readdir(3) ,
.BR rfio_rewinddir(3) ,
.BR Cthread(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
