.\"
.\" $Id: rfio_fileno.man,v 1.2 2007/09/10 13:40:52 obarring Exp $
.\"
.\" Copyright (C) 2002 by CERN/IT/DS/HSM
.\" All rights reserved
.\"
.TH RFIO_FILENO "3castor" "$Date: 2007/09/10 13:40:52 $" CASTOR "Rfio Library Functions"
.SH NAME
rfio_fileno \- maps stream pointer to file descriptor
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "rfio_api.h"\fR
.sp
.BI "int rfio_fileno (FILE *" fp ");"
.SH DESCRIPTION
.B rfio_fileno
returns the integer file descriptor associated with the stream pointed to by 
.IR fp .
.SH RETURN VALUE
Upon successful completion,
.B rfio_fileno()
returns the integer value of the file descriptor associated with the stream
parameter.  Otherwise, the value -1 is returned and
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B EBADF
.I fp
is not a valid file descriptor.
.SH MULTI-THREAD APPLICATION USAGE
When calling RFIO from a multi-threaded application, it is first necessary to
initialize thread-specific storage and serialization objects by a call to
\fBCthread_init()\fP.
.SH SEE ALSO
.BR rfio_fopen(3) , Cthread(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
