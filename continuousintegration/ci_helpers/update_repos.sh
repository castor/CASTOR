#!/bin/bash
#
# This script is launched by CASTOR CI `publish_rpm` job. Its
# running edition is located at:
#
# /eos/project/c/castor/www/castor-repo/update_repos.sh
#

BASEREPOSURL="https://castorrepo.web.cern.ch/castorrepo/castor-repo"
BASEDIR=$(dirname $0)

cd ${BASEDIR}

# the artifacts were copied in subdirs because of xrdcp --recursive,
# move them to the right places
mv qa/cc-7/RPMS/x86_64/*rpm qa/cc-7/x86_64/
mv nightlies/cc-7/RPMS/x86_64/*rpm nightlies/cc-7/x86_64/
mv ops/cc-7/build_srpm/*src.rpm ops/cc-7/
mv ops/cc-7/noarch/build_rpm/*rpm ops/cc-7/noarch/

# some house keeping for the (weekly) "nightlies": keep only the last 3 weeks
find nightlies -type f -mtime +22 -exec rm -rf {} \;

# now update all repos metadata
for repository in qa/*/x86_64 nightlies/*/x86_64 ops/*/noarch; do
  echo Updating ${repository}...
  cd $repository
  # remove dirs that can completely block createrepo...
  rm -fr .repodata .olddata
  createrepo .
  cd -
done
