# From https://github.com/nherbaut/cmake-gsoap-simple-example.git
# with adaptations for CASTOR - look for cgsi_plugin and VOMS libraries.
#
# @author Castor Dev team, castor-dev@cern.ch
#########################################################################
#
# This module detects if gsoap is installed and determines where the
# include files and libraries are.
#
# This code sets the following variables:
#
# GSOAP_IMPORT_DIR      = full path to the gsoap import directory (for soapcpp2)
# GSOAP_CXX_LIBRARIES   = full path to the gsoap libraries
# GSOAP_CGSI_PLUGIN_LIB = full path to the cgsi_gSOAP plugin library
# GSOAP_VOMS_LIB        = full path to the VOMS library
# GSOAP_INCLUDE_DIR     = include dir to be used when using the gsoap library
# GSOAP_WSDL2H          = wsdl2h binary
# GSOAP_SOAPCPP2        = soapcpp2 binary
# GSOAP_FOUND           = set to true if gsoap was found successfully
#

# Be silent if GSOAP_INCLUDE_DIR is already cached
if (GSOAP_INCLUDE_DIR)
  set(GSOAP_FIND_QUIETLY TRUE)
endif (GSOAP_INCLUDE_DIR)

# -----------------------------------------------------
# GSOAP Import Directories
# -----------------------------------------------------
find_path(GSOAP_IMPORT_DIR
  NAMES wsa.h
  HINTS /usr/import /usr/share/gsoap/import
)
message (STATUS "GSOAP_IMPORT_DIR = ${GSOAP_IMPORT_DIR}")

# -----------------------------------------------------
# GSOAP and related libraries
# -----------------------------------------------------
find_library(GSOAP_CXX_LIBRARIES
	NAMES gsoap++
	HINTS /usr/lib /usr/lib64
	DOC "The main gsoap library"
)
message (STATUS "GSOAP_CXX_LIBRARIES = ${GSOAP_CXX_LIBRARIES}")

find_library(GSOAP_CGSI_PLUGIN_LIB
	NAMES cgsi_plugin_voms_cpp
	HINTS /usr/lib /usr/lib64
	DOC "The cGSI_gSOAP plugin library"
)
message (STATUS "GSOAP_CGSI_PLUGIN_LIB = ${GSOAP_CGSI_PLUGIN_LIB}")

find_library(GSOAP_VOMS_LIB
	NAMES vomsapi
	HINTS /usr/lib /usr/lib64
	DOC "The VOMS library"
)
message (STATUS "GSOAP_VOMS_LIB = ${GSOAP_VOMS_LIB}")

# -----------------------------------------------------
# GSOAP Include Directories
# -----------------------------------------------------
find_path(GSOAP_INCLUDE_DIR
	NAMES stdsoap2.h
	HINTS /usr /usr/include /usr/include/*
	DOC "The gsoap include directory"
)
message (STATUS "GSOAP_INCLUDE_DIR = ${GSOAP_INCLUDE_DIR}")

# -----------------------------------------------------
# GSOAP Binaries
# ----------------------------------------------------
if(NOT GSOAP_TOOL_DIR)
	set(GSOAP_TOOL_DIR GSOAP_ROOT)
endif()

find_program(GSOAP_WSDL2H
	NAMES wsdl2h
	HINTS ${GSOAP_TOOL_DIR}/bin
	DOC "The gsoap bin directory"
)
find_program(GSOAP_SOAPCPP2
	NAMES soapcpp2
	HINTS ${GSOAP_TOOL_DIR}/bin
	DOC "The gsoap bin directory"
)

# -----------------------------------------------------
# handle the QUIETLY and REQUIRED arguments and set GSOAP_FOUND to TRUE if
# all listed variables are TRUE
# -----------------------------------------------------
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(gsoap DEFAULT_MSG GSOAP_CXX_LIBRARIES GSOAP_CGSI_PLUGIN_LIB GSOAP_VOMS_LIB
	GSOAP_INCLUDE_DIR GSOAP_WSDL2H GSOAP_SOAPCPP2)
mark_as_advanced(GSOAP_INCLUDE_DIR GSOAP_LIBRARIES GSOAP_WSDL2H GSOAP_SOAPCPP2)
