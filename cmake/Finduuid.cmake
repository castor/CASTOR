#                      cmake/Finduuid.cmake
#
# This file is part of the Castor project.
# See http://castor.web.cern.ch/castor
#
# Copyright (C) 2003  CERN
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#
# castor-dev@cern.ch
#

# - Find uuid library and headers
# Finds the library that implements the uuid C functions such as uuid_generate()
#
# UUID_FOUND  - true if the uuid library has been found
# UUID_LIB    - location of the uuid library

find_header(uuid/uuid.h)

# Be silent if UUID_LIB is already cached
if (UUID_LIB)
  set(UUID_LIB_FIND_QUIETLY TRUE)
endif (UUID_LIB)

if(APPLE)
  find_library (UUID_LIB System)
else(APPLE)
  find_library (UUID_LIB uuid)
endif(APPLE)

include (FindPackageHandleStandardArgs)
find_package_handle_standard_args (uuid DEFAULT_MSG UUID_LIB)
