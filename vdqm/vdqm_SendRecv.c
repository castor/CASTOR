/*
 * Copyright (C) 1999-2003 by CERN
 * All rights reserved
 */

/*
 * vdqm_SendRecv.c - Send and receive VDQM request and acknowledge messages.
 */

#include <stdlib.h>
#include <string.h>
#include <sys/param.h>
#include <sys/types.h>                  /* Standard data types          */
#include <netdb.h>                      /* Network "data base"          */
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>                 /* Internet data types          */

#include <errno.h>

#include <Castor_limits.h>
#include <rtcp_constants.h>             /* Definition of RTCOPY_MAGIC   */
#include <net.h>
#include <log.h>
#include <osdep.h>
#include <Cnetdb.h>
#include <marshall.h>
#include <vdqm_constants.h>
#include <vdqm.h>
#include <serrno.h>
#include <common.h>
extern char *getconfent();

#define REQTYPE(Y,X) ( X == VDQM_##Y##_REQ || \
    X == VDQM_DEL_##Y##REQ || \
    X == VDQM_GET_##Y##QUEUE || (!strcmp(#Y,"VOL") && X == VDQM_PING) || \
    (!strcmp(#Y,"DRV") && X == VDQM_DEDICATE_DRV) )

typedef enum direction {SendTo,ReceiveFrom} direction_t;

#define DO_MARSHALL(X,Y,Z,W) { \
    if ( W == SendTo ) {marshall_##X(Y,Z);} \
    else {unmarshall_##X(Y,Z);} }

#define DO_MARSHALL_STRING(Y,Z,W,N) { \
    if ( W == SendTo ) {marshall_STRING(Y,Z);} \
    else { if(unmarshall_STRINGN(Y,Z,N)) { serrno=EINVAL; return -1; } } }



static int vdqm_Transfer(vdqmnw_t *nw,
                         vdqmHdr_t *hdr,
                         vdqmVolReq_t *volreq,
                         vdqmDrvReq_t *drvreq,
                         direction_t whereto) {
    
    char hdrbuf[VDQM_HDRBUFSIZ], buf[VDQM_MSGBUFSIZ];
    char servername[CA_MAXHOSTNAMELEN+1];
    char *p,*domain;
    struct sockaddr_in from;
    struct hostent *hp;
    socklen_t fromlen;
    int magic,reqtype,len,local_access; 
    int rc;
    int s;
    
    /*
     * Sanity checks
     */
    if ( nw == NULL                                   ||
        (nw->accept_socket == -1          &&
        nw->connect_socket == -1) ) {
        serrno = EINVAL;
        return(-1);
    }
    
    reqtype = -1;
    *servername = '\0';
    local_access = 0;
    magic = len = 0;
    if ( (s = nw->accept_socket) == -1 ) {
        rc = gethostname(servername,CA_MAXHOSTNAMELEN);
        s = nw->connect_socket;
    }
    
    if ( whereto == ReceiveFrom ) {
        rc = netread_timeout(s,hdrbuf,VDQM_HDRBUFSIZ,VDQM_TIMEOUT);
        switch (rc) {
        case -1: 
            serrno = SECOMERR;
            return(-1);
        case 0:
            serrno = SECONNDROP;
            return(-1);
        }
        p = hdrbuf;
        DO_MARSHALL(LONG,p,magic,whereto);
        DO_MARSHALL(LONG,p,reqtype,whereto);
        DO_MARSHALL(LONG,p,len,whereto);
        if ( hdr != NULL ) {
            hdr->magic = magic;
            hdr->reqtype = reqtype;
            hdr->len = len;
        }
        if ( VALID_VDQM_MSGLEN(len) ) {
            rc = netread_timeout(s,buf,len,VDQM_TIMEOUT);
            switch (rc) {
            case -1:
                serrno = SECOMERR;
                return(-1);
            case 0:
                serrno = SECONNDROP;
                return(-1);
            }
        } else if ( len > 0 ) {
            serrno = SEUMSG2LONG;
            return(-1);
        }
        
        fromlen = sizeof(from);
        if ( (rc = getpeername(s,(struct sockaddr *)&from,&fromlen)) == -1 ) {
          return(-1);
        } 
        if ( (hp = Cgethostbyaddr((void *)&(from.sin_addr),sizeof(struct in_addr),from.sin_family)) == NULL ) {
          return(-1);
        }
        if ( (REQTYPE(VOL,reqtype) && volreq == NULL) ||
             (REQTYPE(DRV,reqtype) && drvreq == NULL) ) {
            serrno = EINVAL;
            return(-1);
        } else if ( REQTYPE(DRV,reqtype) ) {
            /* 
             * We need to authorize request host if not same as server name.
             */
            strcpy(drvreq->reqhost,hp->h_name);
            if ( isremote(from.sin_addr,drvreq->reqhost) == 1 &&
                 getconfent("VDQM","REMOTE_ACCESS",1) == NULL ) {
            } else {
               local_access = 1;
               if ( (domain = strstr(drvreq->reqhost,".")) != NULL ) 
                    *domain = '\0';
            }
        }
    }
    if ( whereto == SendTo ) {
        if ( hdr != NULL && VDQM_VALID_REQTYPE(hdr->reqtype) ) reqtype = hdr->reqtype;
        else if ( volreq != NULL ) reqtype = VDQM_VOL_REQ;
        else if ( drvreq != NULL ) reqtype = VDQM_DRV_REQ;
        else {
            (*logfunc)(LOG_ERR,"vdqm_Transfer(): cannot determine request type to send\n");
            return(-1);
        }
        if ( *servername != '\0' ) {
            if ( reqtype == VDQM_VOL_REQ && *volreq->client_host == '\0' ) 
                strcpy(volreq->client_host,servername);
            else if ( reqtype == VDQM_DRV_REQ && *drvreq->server == '\0' )
                strcpy(drvreq->server,servername);
        }
    }
    
    p = buf;
    if ( REQTYPE(VOL,reqtype) && volreq != NULL ) {
        DO_MARSHALL(LONG,p,volreq->VolReqID,whereto);
        DO_MARSHALL(LONG,p,volreq->DrvReqID,whereto);
        DO_MARSHALL(LONG,p,volreq->priority,whereto);
        DO_MARSHALL(LONG,p,volreq->client_port,whereto);
        DO_MARSHALL(LONG,p,volreq->clientUID,whereto);
        DO_MARSHALL(LONG,p,volreq->clientGID,whereto);
        DO_MARSHALL(LONG,p,volreq->mode,whereto);
        DO_MARSHALL(LONG,p,volreq->recvtime,whereto);
        DO_MARSHALL_STRING(p,volreq->client_host,whereto, sizeof(volreq->client_host));
        DO_MARSHALL_STRING(p,volreq->volid,whereto, sizeof(volreq->volid));
        DO_MARSHALL_STRING(p,volreq->server,whereto, sizeof(volreq->server));
        DO_MARSHALL_STRING(p,volreq->drive,whereto, sizeof(volreq->drive));
        DO_MARSHALL_STRING(p,volreq->dgn,whereto, sizeof(volreq->dgn));
        DO_MARSHALL_STRING(p,volreq->client_name,whereto, sizeof(volreq->client_name));
    }
    if ( REQTYPE(DRV,reqtype) && drvreq != NULL ) {
        DO_MARSHALL(LONG,p,drvreq->status,whereto);
        DO_MARSHALL(LONG,p,drvreq->DrvReqID,whereto);
        DO_MARSHALL(LONG,p,drvreq->VolReqID,whereto);
        DO_MARSHALL(LONG,p,drvreq->jobID,whereto);
        DO_MARSHALL(LONG,p,drvreq->recvtime,whereto);
        DO_MARSHALL(LONG,p,drvreq->resettime,whereto);
        DO_MARSHALL(LONG,p,drvreq->usecount,whereto);
        DO_MARSHALL(LONG,p,drvreq->errcount,whereto);
        DO_MARSHALL(LONG,p,drvreq->MBtransf,whereto);
        DO_MARSHALL(LONG,p,drvreq->mode,whereto);
        DO_MARSHALL(HYPER,p,drvreq->TotalMB,whereto);
        DO_MARSHALL_STRING(p,drvreq->volid,whereto, sizeof(drvreq->volid));
        DO_MARSHALL_STRING(p,drvreq->server,whereto, sizeof(drvreq->server));
        DO_MARSHALL_STRING(p,drvreq->drive,whereto, sizeof(drvreq->drive));
        DO_MARSHALL_STRING(p,drvreq->dgn,whereto, sizeof(drvreq->dgn));
        DO_MARSHALL_STRING(p,drvreq->dedicate,whereto, sizeof(drvreq->dedicate));
        if ( (whereto == ReceiveFrom) && (local_access == 1) &&
             (domain = strstr(drvreq->server,".")) != NULL ) *domain = '\0';
    }
 
    if ( whereto == SendTo ) {
        /*
         * reqtype has already been determined above
         */
        if ( hdr != NULL && hdr->magic != 0 ) magic = hdr->magic;
        else magic = VDQM_MAGIC;
        len = 0;
        if ( REQTYPE(VOL,reqtype)) 
             len = VDQM_VOLREQLEN(volreq);
        else if ( REQTYPE(DRV,reqtype) ) 
               len = VDQM_DRVREQLEN(drvreq);
        else if ( hdr != NULL ) len = hdr->len;
        p = hdrbuf;
        DO_MARSHALL(LONG,p,magic,whereto);
        DO_MARSHALL(LONG,p,reqtype,whereto);
        DO_MARSHALL(LONG,p,len,whereto);
        rc = netwrite_timeout(s,hdrbuf,VDQM_HDRBUFSIZ,VDQM_TIMEOUT);
        switch (rc) {
        case -1:
            serrno = SECOMERR;
            return(-1);
        case 0:
            serrno = SECONNDROP;
            return(-1);
        }
        if ( len > 0 ) {
            rc = netwrite_timeout(s,buf,len,VDQM_TIMEOUT);
            switch (rc) {
            case -1:
                serrno = SECOMERR;
                return(-1);
            case 0:
                serrno = SECONNDROP;
                return(-1);
            }
        }
    } else if ( whereto == ReceiveFrom ) {
      if ( REQTYPE(DRV,reqtype) && (reqtype != VDQM_GET_DRVQUEUE) ) {
        if ( (strcmp(drvreq->reqhost,drvreq->server) != 0) &&
             (isadminhost(s,drvreq->reqhost) != 0) ) {
          serrno = EPERM;
          return(-1);
        }
      }
    }
    return(reqtype);
}

int vdqm_SendReq(vdqmnw_t *nw, 
                 vdqmHdr_t *hdr, 
                 vdqmVolReq_t *volreq,
                 vdqmDrvReq_t *drvreq) {
    direction_t whereto = SendTo;
    return(vdqm_Transfer(nw,hdr,volreq,drvreq,whereto));
}

int vdqm_RecvReq(vdqmnw_t *nw, 
                 vdqmHdr_t *hdr, 
                 vdqmVolReq_t *volreq,
                 vdqmDrvReq_t *drvreq) {
    direction_t whereto = ReceiveFrom;
    return(vdqm_Transfer(nw,hdr,volreq,drvreq,whereto));
}

int vdqm_SendPing(vdqmnw_t *nw,
                  vdqmHdr_t *hdr,
                  vdqmVolReq_t *volreq) {
    direction_t whereto = SendTo;
    vdqmHdr_t tmphdr, *tmphdr_p;
    if ( hdr == NULL ) {
        tmphdr.magic = VDQM_MAGIC; 
        tmphdr.reqtype = VDQM_PING;
        tmphdr.len = -1;
        tmphdr_p = &tmphdr;
    } else {
        hdr->reqtype = VDQM_PING;
        tmphdr_p = hdr;
    }
    return(vdqm_Transfer(nw,tmphdr_p,volreq,NULL,whereto));
}

static int vdqm_TransAckn(vdqmnw_t *nw, int reqtype, int *data, 
                          direction_t whereto) {
    char hdrbuf[VDQM_HDRBUFSIZ];
    int magic, recvreqtype, len, rc;
    char *p;
    int s;
    
    if ( nw == NULL                                   ||
        (nw->accept_socket == -1          &&
        nw->connect_socket == -1) ) {
        serrno = EINVAL;
        return(-1);
    }
    if ( (s = nw->accept_socket) == -1 )
        s = nw->connect_socket;
    
    magic = VDQM_MAGIC;
    len = 0;
    recvreqtype = reqtype;
    if ( data != NULL ) len = *data;
    
    if ( whereto == ReceiveFrom ) {
        rc = netread_timeout(s,hdrbuf,VDQM_HDRBUFSIZ,VDQM_TIMEOUT);
        switch (rc) {
        case -1: 
            serrno = SECOMERR;
            return(-1);
        case 0:
            serrno = SECONNDROP;
            return(-1);
        }
    }
    p = hdrbuf;
    DO_MARSHALL(LONG,p,magic,whereto);
    DO_MARSHALL(LONG,p,recvreqtype,whereto);
    DO_MARSHALL(LONG,p,len,whereto);
    
    if ( whereto == SendTo ) {
        magic = VDQM_MAGIC;
        len = 0;
        p = hdrbuf;
        rc = netwrite_timeout(s,hdrbuf,VDQM_HDRBUFSIZ,VDQM_TIMEOUT);
        switch (rc) {
        case -1: 
            serrno = SECOMERR;
            return(-1);
        case 0:
            serrno = SECONNDROP;
            return(-1);
        }
    }
    
    if ( data != NULL ) *data = len;
    return(recvreqtype);
}

int vdqm_AcknCommit(vdqmnw_t *nw) {
    direction_t whereto = SendTo;
    int reqtype = VDQM_COMMIT;
    
    return(vdqm_TransAckn(nw,reqtype,NULL,whereto));
}

int vdqm_RecvAckn(vdqmnw_t *nw) {
    direction_t whereto = ReceiveFrom;
    int reqtype = 0;
    
    return(vdqm_TransAckn(nw,reqtype,NULL,whereto));
}

int vdqm_RecvPingAckn(vdqmnw_t *nw) {
    direction_t whereto = ReceiveFrom;
    int reqtype = 0;
    int data = 0;
    
    reqtype = vdqm_TransAckn(nw,reqtype,&data,whereto);
    
    if ( reqtype != VDQM_PING || data < 0 )  {
        if ( data < 0 ) serrno = -data;
        else serrno = SEINTERNAL;
        return(-1);
    } else return(data);
}

static int vdqm_MarshallRTCPReq(char *buf,
                                vdqmVolReq_t *VolReq,
                                vdqmDrvReq_t *DrvReq,
                                direction_t whereto) {

    char *p;
    int reqtype,magic,len;

    if ( buf == NULL || VolReq == NULL ) return(-1);
    p = buf;
    magic = RTCOPY_MAGIC_OLD0;
    reqtype = VDQM_CLIENTINFO;
    len = 4*LONGSIZE + strlen(VolReq->client_name) + 
        strlen(VolReq->client_host) + strlen(DrvReq->dgn) + 
        strlen(DrvReq->drive) + 4;
    DO_MARSHALL(LONG,p,magic,whereto);
    DO_MARSHALL(LONG,p,reqtype,whereto);
    DO_MARSHALL(LONG,p,len,whereto);
    if ( (magic != RTCOPY_MAGIC && magic != RTCOPY_MAGIC_OLD0) || reqtype != VDQM_CLIENTINFO ) return(-1);
    DO_MARSHALL(LONG,p,VolReq->VolReqID,whereto);
    DO_MARSHALL(LONG,p,VolReq->client_port,whereto);
    DO_MARSHALL(LONG,p,VolReq->clientUID,whereto);
    DO_MARSHALL(LONG,p,VolReq->clientGID,whereto);
    DO_MARSHALL_STRING(p,VolReq->client_host,whereto, sizeof(VolReq->client_host));
    DO_MARSHALL_STRING(p,DrvReq->dgn,whereto, sizeof(VolReq->dgn));
    DO_MARSHALL_STRING(p,DrvReq->drive,whereto, sizeof(VolReq->drive));
    DO_MARSHALL_STRING(p,VolReq->client_name,whereto, sizeof(VolReq->client_name));
    return(len+3*LONGSIZE);
}

static int vdqm_MarshallRTCPAckn(char *buf,
                                 int *status,
                                 int *errmsglen,
                                 char *errmsg,
                                 direction_t whereto) {
    char *p;
    int reqtype,magic,len;
    int rc,msglen;
    if ( buf == NULL ) return(-1);

    rc = (status == NULL ? 0 : *status);
    msglen = (errmsglen == NULL ? 0 : *errmsglen);
    p = buf;
    magic = RTCOPY_MAGIC_OLD0;
    reqtype = VDQM_CLIENTINFO;
    len = LONGSIZE + msglen +1;
    DO_MARSHALL(LONG,p,magic,whereto);
    DO_MARSHALL(LONG,p,reqtype,whereto);
    DO_MARSHALL(LONG,p,len,whereto);
    if ( (magic != RTCOPY_MAGIC && magic != RTCOPY_MAGIC_OLD0) || reqtype != VDQM_CLIENTINFO ) return(-1);

    DO_MARSHALL(LONG,p,rc,whereto);
    if ( status != NULL ) *status = rc;
    if ( errmsglen != NULL && errmsg != NULL) {
        if ( whereto == ReceiveFrom ) {
            msglen = len - LONGSIZE -1;
            msglen = msglen < *errmsglen-1 ? msglen : *errmsglen-1;
            strncpy(errmsg,p,msglen);
            errmsg[msglen] = '\0';
            *errmsglen = msglen;
        } else {
            DO_MARSHALL(STRING,p,errmsg,SendTo);
        }
    }
    return(len+3*LONGSIZE);
}

int vdqm_GetRTCPReq(char *buf,
                    vdqmVolReq_t *VolReq,
                    vdqmDrvReq_t *DrvReq) {
    direction_t whereto = ReceiveFrom;
    return(vdqm_MarshallRTCPReq(buf,VolReq,DrvReq,whereto));
}

int vdqm_SendRTCPAckn(int connect_socket,
                      int *status,
                      int *errmsglen,
                      char *errmsg) {
    char buf[VDQM_MSGBUFSIZ];
    direction_t whereto = SendTo;
    int rc,len,msglen;

    len = msglen = 0;
    if ( errmsglen != NULL && *errmsglen > 0 && errmsg != NULL ) {
        msglen = (*errmsglen>VDQM_MSGBUFSIZ-4*LONGSIZE-1 ?
            VDQM_MSGBUFSIZ-4*LONGSIZE-1: *errmsglen);
    }

    len = vdqm_MarshallRTCPAckn(buf,status,&msglen,
            errmsg,whereto);

    rc = netwrite_timeout(connect_socket,buf,len,VDQM_TIMEOUT);
    return(rc);
}

int vdqm_SendVolPriority_Transfer(vdqmnw_t *nw, vdqmVolPriority_t *volpriority)
    {
    
    char hdrbuf[VDQM_HDRBUFSIZ], buf[VDQM_MSGBUFSIZ];
    char servername[CA_MAXHOSTNAMELEN+1];
    char *p;
    int magic,reqtype,len; 
    int rc;
    int s;
    
    /*
     * Sanity checks
     */
    if ( nw == NULL                                   ||
        (nw->accept_socket == -1          &&
        nw->connect_socket == -1) ) {
        serrno = EINVAL;
        return(-1);
    }
    
    *servername = '\0';
    magic = len = 0;
    if ( (s = nw->accept_socket) == -1 ) {
        rc = gethostname(servername,CA_MAXHOSTNAMELEN);
        s = nw->connect_socket;
    }
    
    if ( *servername != '\0' ) {
        strncpy(volpriority->clientHost, servername,
            sizeof(volpriority->clientHost));
        volpriority->clientHost[sizeof(volpriority->clientHost)-1] = '\0';
    }

    p = buf;
    DO_MARSHALL(LONG,p,volpriority->priority,SendTo);
    DO_MARSHALL(LONG,p,volpriority->clientUID,SendTo);
    DO_MARSHALL(LONG,p,volpriority->clientGID,SendTo);
    DO_MARSHALL_STRING(p,volpriority->clientHost,SendTo,
        sizeof(volpriority->clientHost));
    DO_MARSHALL_STRING(p,volpriority->vid,SendTo, sizeof(volpriority->vid));
    DO_MARSHALL(LONG,p,volpriority->tpMode,SendTo);
    DO_MARSHALL(LONG,p,volpriority->lifespanType,SendTo);
 
    magic   = VDQM_MAGIC2;
    reqtype = VDQM2_SET_VOL_PRIORITY;
    len     = VDQM_VOLPRIORITYLEN(volpriority);
    p = hdrbuf;
    DO_MARSHALL(LONG,p,magic,SendTo);
    DO_MARSHALL(LONG,p,reqtype,SendTo);
    DO_MARSHALL(LONG,p,len,SendTo);
    rc = netwrite_timeout(s,hdrbuf,VDQM_HDRBUFSIZ,VDQM_TIMEOUT);
    switch (rc) {
    case -1:
        serrno = SECOMERR;
        return(-1);
    case 0:
        serrno = SECONNDROP;
        return(-1);
    }
    if ( len > 0 ) {
        rc = netwrite_timeout(s,buf,len,VDQM_TIMEOUT);
        switch (rc) {
        case -1:
            serrno = SECOMERR;
            return(-1);
        case 0:
            serrno = SECONNDROP;
            return(-1);
        }
    }

    return(reqtype);
}

int vdqm_RecvVolPriority_Transfer(vdqmnw_t *nw, vdqmVolPriority_t *volpriority)
    {
    
    char hdrbuf[VDQM_HDRBUFSIZ], buf[VDQM_MSGBUFSIZ];
    char servername[CA_MAXHOSTNAMELEN+1];
    char *p;
    struct sockaddr_in from;
    socklen_t fromlen;
    int magic,reqtype,len; 
    int rc;
    int s;
    
    /*
     * Sanity checks
     */
    if ( nw == NULL                                   ||
        (nw->accept_socket == -1          &&
        nw->connect_socket == -1) ) {
        serrno = EINVAL;
        return(-1);
    }
    
    reqtype = -1;
    *servername = '\0';
    magic = len = 0;
    if ( (s = nw->accept_socket) == -1 ) {
        rc = gethostname(servername,CA_MAXHOSTNAMELEN);
        s = nw->connect_socket;
    }
    
    rc = netread_timeout(s,hdrbuf,VDQM_HDRBUFSIZ,VDQM_TIMEOUT);
    switch (rc) {
    case -1: 
        serrno = SECOMERR;
        return(-1);
    case 0:
        serrno = SECONNDROP;
        return(-1);
    }
    p = hdrbuf;
    DO_MARSHALL(LONG,p,magic,ReceiveFrom);
    DO_MARSHALL(LONG,p,reqtype,ReceiveFrom);
    DO_MARSHALL(LONG,p,len,ReceiveFrom);
    if ( VALID_VDQM_MSGLEN(len) ) {
        rc = netread_timeout(s,buf,len,VDQM_TIMEOUT);
        switch (rc) {
        case -1:
            serrno = SECOMERR;
            return(-1);
        case 0:
            serrno = SECONNDROP;
            return(-1);
        }
    } else if ( len > 0 ) {
        serrno = SEUMSG2LONG;
        return(-1);
    }
        
    fromlen = sizeof(from);
    if ( (rc = getpeername(s,(struct sockaddr *)&from,&fromlen)) ==
        -1 ) {
        return(-1);
    } 
    if ( Cgethostbyaddr((void *)&(from.sin_addr),sizeof(struct in_addr),
        from.sin_family) == NULL ) {
        return(-1);
    }

    p = buf;
    DO_MARSHALL(LONG,p,volpriority->priority,ReceiveFrom);
    DO_MARSHALL(LONG,p,volpriority->clientUID,ReceiveFrom);
    DO_MARSHALL(LONG,p,volpriority->clientGID,ReceiveFrom);
    DO_MARSHALL_STRING(p,volpriority->clientHost,ReceiveFrom,
        sizeof(volpriority->clientHost));
    DO_MARSHALL_STRING(p,volpriority->vid,ReceiveFrom,
        sizeof(volpriority->vid));
    DO_MARSHALL(LONG,p,volpriority->lifespanType,ReceiveFrom);
 
    return(reqtype);
}


int vdqm_SendDelDrv_Transfer(vdqmnw_t *nw, vdqmDelDrv_t *msg)
    {
    char hdrbuf[VDQM_HDRBUFSIZ], buf[VDQM_MSGBUFSIZ];
    char servername[CA_MAXHOSTNAMELEN+1];
    char *p;
    int magic,reqtype,len;
    int rc;
    int s;

    /*
     * Sanity checks
     */
    if ( nw == NULL                                   ||
        (nw->accept_socket == -1          &&
        nw->connect_socket == -1) ) {
        serrno = EINVAL;
        return(-1);
    }

    *servername = '\0';
    magic = len = 0;
    if ( (s = nw->accept_socket) == -1 ) {
        rc = gethostname(servername,CA_MAXHOSTNAMELEN);
        s = nw->connect_socket;
    }

    if ( *servername != '\0' ) {
        strncpy(msg->clientHost, servername, sizeof(msg->clientHost));
        msg->clientHost[sizeof(msg->clientHost)-1] = '\0';
    }

    p = buf;
    DO_MARSHALL(LONG,p,msg->clientUID,SendTo);
    DO_MARSHALL(LONG,p,msg->clientGID,SendTo);
    DO_MARSHALL_STRING(p,msg->clientHost,SendTo, sizeof(msg->clientHost));
    DO_MARSHALL_STRING(p,msg->server,SendTo, sizeof(msg->server));
    DO_MARSHALL_STRING(p,msg->drive,SendTo, sizeof(msg->drive));
    DO_MARSHALL_STRING(p,msg->dgn,SendTo, sizeof(msg->dgn));

    magic   = VDQM_MAGIC3;
    reqtype = VDQM3_DEL_DRV;
    len     = VDQM_DELDRVLEN(msg);
    p = hdrbuf;
    DO_MARSHALL(LONG,p,magic,SendTo);
    DO_MARSHALL(LONG,p,reqtype,SendTo);
    DO_MARSHALL(LONG,p,len,SendTo);
    rc = netwrite_timeout(s,hdrbuf,VDQM_HDRBUFSIZ,VDQM_TIMEOUT);
    switch (rc) {
    case -1:
        serrno = SECOMERR;
        return(-1);
    case 0:
        serrno = SECONNDROP;
        return(-1);
    }
    if ( len > 0 ) {
        rc = netwrite_timeout(s,buf,len,VDQM_TIMEOUT);
        switch (rc) {
        case -1:
            serrno = SECOMERR;
            return(-1);
        case 0:
            serrno = SECONNDROP;
            return(-1);
        }
    }

    return(reqtype);
}

int vdqm_SendDedicate_Transfer(vdqmnw_t *nw, vdqmDedicate_t *msg)
    {

    char hdrbuf[VDQM_HDRBUFSIZ], buf[VDQM_MSGBUFSIZ];
    char servername[CA_MAXHOSTNAMELEN+1];
    char *p;
    int magic,reqtype,len;
    int rc;
    int s;

    /*
     * Sanity checks
     */
    if ( nw == NULL                                   ||
        (nw->accept_socket == -1          &&
        nw->connect_socket == -1) ) {
        serrno = EINVAL;
        return(-1);
    }

    *servername = '\0';
    magic = len = 0;
    if ( (s = nw->accept_socket) == -1 ) {
        rc = gethostname(servername,CA_MAXHOSTNAMELEN);
        s = nw->connect_socket;
    }

    if ( *servername != '\0' ) {
        strncpy(msg->clientHost, servername, sizeof(msg->clientHost));
        msg->clientHost[sizeof(msg->clientHost)-1] = '\0';
    }

    p = buf;
    DO_MARSHALL(LONG,p,msg->clientUID,SendTo);
    DO_MARSHALL(LONG,p,msg->clientGID,SendTo);
    DO_MARSHALL_STRING(p,msg->clientHost,SendTo, sizeof(msg->clientHost));
    DO_MARSHALL_STRING(p,msg->server,SendTo, sizeof(msg->server));
    DO_MARSHALL_STRING(p,msg->drive,SendTo, sizeof(msg->drive));
    DO_MARSHALL_STRING(p,msg->dgn,SendTo, sizeof(msg->dgn));
    DO_MARSHALL_STRING(p,msg->dedicate,SendTo, sizeof(msg->dedicate));

    magic   = VDQM_MAGIC3;
    reqtype = VDQM3_DEDICATE;
    len     = VDQM_DEDICATELEN(msg);
    p = hdrbuf;
    DO_MARSHALL(LONG,p,magic,SendTo);
    DO_MARSHALL(LONG,p,reqtype,SendTo);
    DO_MARSHALL(LONG,p,len,SendTo);
    rc = netwrite_timeout(s,hdrbuf,VDQM_HDRBUFSIZ,VDQM_TIMEOUT);
    switch (rc) {
    case -1:
        serrno = SECOMERR;
        return(-1);
    case 0:
        serrno = SECONNDROP;
        return(-1);
    }
    if ( len > 0 ) {
        rc = netwrite_timeout(s,buf,len,VDQM_TIMEOUT);
        switch (rc) {
        case -1:
            serrno = SECOMERR;
            return(-1);
        case 0:
            serrno = SECONNDROP;
            return(-1);
        }
    }

    return(reqtype);
}

static int vdqm_AggregatorVolReq_Transfer(vdqmnw_t *nw, vdqmHdr_t *hdr,
    vdqmVolReq_t *volreq, direction_t whereto) {
    
    char hdrbuf[VDQM_HDRBUFSIZ], buf[VDQM_MSGBUFSIZ];
    char servername[CA_MAXHOSTNAMELEN+1];
    char *p;
    struct sockaddr_in from;
    socklen_t fromlen;
    int magic,reqtype,len; 
    int rc;
    int s;
    
    /*
     * Sanity checks
     */
    if ( nw == NULL                                   ||
        (nw->accept_socket == -1          &&
        nw->connect_socket == -1) ) {
        serrno = EINVAL;
        return(-1);
    }
    if ( volreq == NULL ) {
        serrno = EINVAL;
        return(-1);
    }
    
    reqtype = -1;
    *servername = '\0';
    magic = len = 0;
    if ( (s = nw->accept_socket) == -1 ) {
        rc = gethostname(servername,CA_MAXHOSTNAMELEN);
        s = nw->connect_socket;
    }
    
    if ( whereto == ReceiveFrom ) {
        rc = netread_timeout(s,hdrbuf,VDQM_HDRBUFSIZ,VDQM_TIMEOUT);
        switch (rc) {
        case -1: 
            serrno = SECOMERR;
            return(-1);
        case 0:
            serrno = SECONNDROP;
            return(-1);
        }
        p = hdrbuf;
        DO_MARSHALL(LONG,p,magic,whereto);
        DO_MARSHALL(LONG,p,reqtype,whereto);
        DO_MARSHALL(LONG,p,len,whereto);
        if ( hdr != NULL ) {
            hdr->magic = magic;
            hdr->reqtype = reqtype;
            hdr->len = len;
        }
        if ( VALID_VDQM_MSGLEN(len) ) {
            rc = netread_timeout(s,buf,len,VDQM_TIMEOUT);
            switch (rc) {
            case -1:
                serrno = SECOMERR;
                return(-1);
            case 0:
                serrno = SECONNDROP;
                return(-1);
            }
        } else if ( len > 0 ) {
            serrno = SEUMSG2LONG;
            return(-1);
        }
        
        fromlen = sizeof(from);
        if ( (rc = getpeername(s,(struct sockaddr *)&from,&fromlen)) == -1 ) {
          return(-1);
        } 
        if ( Cgethostbyaddr((void *)&(from.sin_addr),sizeof(struct in_addr),from.sin_family) == NULL ) {
          return(-1);
        }
    }
    if ( whereto == SendTo ) {
        reqtype = VDQM4_TAPEBRIDGE_VOL_REQ;
        if ( *servername != '\0' && *volreq->client_host == '\0' ) {
            strcpy(volreq->client_host,servername);
        }
    }
    
    p = buf;

    DO_MARSHALL(LONG,p,volreq->VolReqID,whereto);
    DO_MARSHALL(LONG,p,volreq->DrvReqID,whereto);
    DO_MARSHALL(LONG,p,volreq->priority,whereto);
    DO_MARSHALL(LONG,p,volreq->client_port,whereto);
    DO_MARSHALL(LONG,p,volreq->clientUID,whereto);
    DO_MARSHALL(LONG,p,volreq->clientGID,whereto);
    DO_MARSHALL(LONG,p,volreq->mode,whereto);
    DO_MARSHALL(LONG,p,volreq->recvtime,whereto);
    DO_MARSHALL_STRING(p,volreq->client_host,whereto, sizeof(volreq->client_host));
    DO_MARSHALL_STRING(p,volreq->volid,whereto, sizeof(volreq->volid));
    DO_MARSHALL_STRING(p,volreq->server,whereto, sizeof(volreq->server));
    DO_MARSHALL_STRING(p,volreq->drive,whereto, sizeof(volreq->drive));
    DO_MARSHALL_STRING(p,volreq->dgn,whereto, sizeof(volreq->dgn));
    DO_MARSHALL_STRING(p,volreq->client_name,whereto, sizeof(volreq->client_name));

    if ( whereto == SendTo ) {
        /*
         * reqtype has already been determined above
         */
        magic = VDQM_MAGIC4;
        len = 0;
        len = VDQM_VOLREQLEN(volreq);
        p = hdrbuf;
        DO_MARSHALL(LONG,p,magic,whereto);
        DO_MARSHALL(LONG,p,reqtype,whereto);
        DO_MARSHALL(LONG,p,len,whereto);
        rc = netwrite_timeout(s,hdrbuf,VDQM_HDRBUFSIZ,VDQM_TIMEOUT);
        switch (rc) {
        case -1:
            serrno = SECOMERR;
            return(-1);
        case 0:
            serrno = SECONNDROP;
            return(-1);
        }
        if ( len > 0 ) {
            rc = netwrite_timeout(s,buf,len,VDQM_TIMEOUT);
            switch (rc) {
            case -1:
                serrno = SECOMERR;
                return(-1);
            case 0:
                serrno = SECONNDROP;
                return(-1);
            }
        }
    }
    return(reqtype);
}

int vdqm_AggregatorVolReq_Send(vdqmnw_t *nw, vdqmHdr_t *hdr, 
    vdqmVolReq_t *volreq) {
    direction_t whereto = SendTo;
    return(vdqm_AggregatorVolReq_Transfer(nw,hdr,volreq,whereto));
}

int vdqm_AggregatorVolReq_Recv(vdqmnw_t *nw, vdqmHdr_t *hdr, vdqmVolReq_t *volreq) {
    direction_t whereto = ReceiveFrom;
    return(vdqm_AggregatorVolReq_Transfer(nw,hdr,volreq,whereto));
}
