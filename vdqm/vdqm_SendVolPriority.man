.\"
.\"
.\" Copyright (C) 1999-2000 by CERN/IT/PDP/DM
.\"
.TH VDQM_SENDVOLPRIORITY l "$Date: 2008/05/29 08:41:54 $" "CASTOR" "VDQM Library Functions"
.SH NAME
.PP
vdqm_SendVolPriority \- send the priority of a volume to the VDQM
.SH SYNOPSIS
.br
\fB#include "Castor_limits.h"\fR
.br
\fB#include "osdep.h"\fR
.br
\fB#include "net.h"\fR
.br
\fB#include "vdqm_api.h"\fR
.sp
.BI "int vdqm_SendVolPriority ("
.br
.BI "                char *" vid ,
.br
.BI "                int " tpMode ,
.br
.BI "                int " priority ,
.br
.BI "                int " lifespanType );
.SH DESCRIPTION
.B vdqm_SendVolPriority
sends to the VDQM the specified volume access priority.
.SH PARAMETERS
.I vid
\- character string specifying the volume visual identifier.
.PP
.I tpMode
\- integer value specifying the tape access mode.  Valid values are either 0
meaning write-disabled or 1 meaning write-enabled.
.PP
.I priority
\- integer value specifying the priority. 0 is the lowest priority and INT_MAX
is the highest.
.PP
.I lifespanType
\- integer value specifying the type of lifespan to be assigned to the priority
setting.  Valid values are either 0 meaning single-mount or 1 meaning unlimited.
.PP

.SH RETURN VALUES
.PP
On successful completion, the
.B vdqm_SendVolPriority
function returns 0. Otherwise, a value of \-1 is returned and
.B serrno
is set to indicate the error.

.SH ERRORS
.PP
If the
.B vdqm_SendVolPriority
function fails,
.B serrno
may be set to one of the following values:
.TP
.B SEINTERNAL
Unexpected internal error 
.TP
.B SENOSSERV
VDQM service unknown.
.TP
.B SENOSHOST
VDQM host unknown.
.TP
.B SECOMERR
A network library call failed when trying to establish the connection
with the VDQM server.
.TP
.B EINVAL
A parameter or combination of parameters is invalid. For instance if
.I VID
is a NULL pointer.
.TP
.B EVQSYERR
A fatal system call (e.g. calloc()) failure in VDQM server.
.TP
.B EVQHOLD
The server is in HOLD status. This is normally a temporary error due
to VDQM server maintenance. Client should retry after a short delay.

.SH EXAMPLE
Here follows an example showing how a client sends the priority of a volume to
the VDQM server.
.P
.nf
#include <errno.h>
#include <serrno.h>
#include <stdio.h>
#include "Castor_limits.h"
#include "osdep.h"
#include "net.h"
#include "vdqm_api.h"


int main(int argc, char **argv) {
  int tpMode, priority, lifespanType, nbRqsts, rc, len, sock, port,
    VolReqID;
  char *vid;

  if ( argc != 5 ) {
    fprintf(stderr,"Usage: %s vid tpMode priority lifespanType\\n",
      argv[0]);
    exit(2);
  }

  vid          = argv[1];
  tpMode       = atoi(argv[2]);
  priority     = atoi(argv[3]);
  lifespanType = atoi(argv[4]);

  rc = vdqm_SendVolPriority(vid, tpMode, priority, lifespanType);

  if ( rc == -1 ) {
    fprintf(stderr,"vdqm_SendVolPriority(): %s\\n", sstrerror(serrno));
    return -1;
  }

  return 0;
}
.fi

.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
