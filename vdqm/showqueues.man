.\" Copyright (C) 2000 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH SHOWQUEUES "1castor" "$Date: 2007/09/11 19:35:31 $" CASTOR "Show tape queues"
.SH NAME
showqueues \- show tape queues and running requests
.SH SYNOPSIS
.BI "showqueues [ -j ] [ -g <" device\ group "> ] [ -S < " tape\ server ">] [ -x  ] [ -D ] "

.SH DESCRIPTION
.B showqueues
shows information on the tape queues and running requests for a device group
or a specific tape server.
.PP
.BI "-j"
\- toggle output to show VDQM volume request ID instead of job ID for
running request. For queued request the volume request ID is always shown.
.PP
.BI "-g <" device\ group ">"
\- show the queues and running request for the give device group.
.PP
.BI "-S <" tape\ server ">"
\- show the queues and running requests on the given tape server.
.PP
.BI "-x"
\- Extended output.
.PP
.BI "-D"
\- only show the drives and their status

.SH EXAMPLE
.nf
.ft CW
% showqueues -g SD3R
drv1@test (1220 MB) status FREE vid:  last update Jun 15 17:22:09
drv2@test (14525 MB) status FREE vid:  last update Jun 15 17:22:09
% showqueues -S test
drv1@test (1220 MB) status FREE vid:  last update Jun 15 17:22:09
drv2@test (14525 MB) status FREE vid:  last update Jun 15 17:22:09
.ft
.fi
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
