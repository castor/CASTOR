.\"
.\"
.\" Copyright (C) 2000 by CERN/IT/PDP/DM
.\"
.TH VDQM_CONNECT "3castor" "$Date: 2009/07/23 12:22:06 $" "CASTOR" "VDQM Library Functions"
.SH NAME
.PP
vdqm_Connect \- connect to the VDQM server
.SH SYNOPSIS
.br
\fB#include "Castor_limits.h"\fR
.br
\fB#include "osdep.h"\fR
.br
\fB#include "net.h"\fR
.br
\fB#include "vdqm_api.h"\fR
.sp
.BI "int vdqm_Connect ("
.br
.BI "                   vdqmnw_t **" nw );
.SH PARAMETERS
.I nw
\- points to a pointer to a structure containing internal VDQM API network information. The pointer should initialised to NULL before the call.

.PP
.SH DESCRIPTION
.B vdqm_Connect
looks up the port number and host name for the VDQM server and attempts to
connect to it. The port number can be defined (in order of precedence) as
an environment variable 
.B VDQM_PORT
, as a CASTOR configuration parameter (VDQM PORT) in the
.B castor.conf 
file or as the vdqm service (tcp protocol) in 
.B services
file. In the same way the VDQM server host name can be defined as
environment variable
.B VDQM_HOST
or as a CASTOR configuration parameter (VDQM HOST) in the
.B castor.conf
file. 

The VDQM host configuration is allowed to contain two host names. The
first host name identifies the VDQM primary host and the second name
identifies the VDQM replication host. If both are defined
.B vdqm_Connect
will transparently attempt to connect to the replication host in case the
connect to the primary host failed. 

Default values for the VDQM port and host names are normally defined through 
compilation constants when building the vdqm library.

All VDQM API library functions will call
.B vdqm_Connect
and
.B vdqm_Disconnect(3)
internally in case the connection is not yet established. It is therefore
not necessary to directly call
.B vdqm_Connect
from an application. This is also the preferred usage since in its current
implementation the VDQM server does not allow multiple API calls to re-use
the same connection.

.SH EXAMPLE
Here follows an example showing how to open a connection to the VDQM server,
issue an API call and thereafter close the connection.
.P
.nf
#include <stdio.h>
#include <stdlib.h>
#include <Castor_limits.h>
#include <serrno.h>
#include <osdep.h>
#include <net.h>
#include <vdqm_api.h>

int main(int argc, char *argv) {
    int rc;
    vdqmnw_t **nw = NULL;

    rc = \fBvdqm_Connect\fP(nw);
    if ( rc == -1 ) {
        fprintf(stderr,"vdqm_Connect(): %s\\n",sstrerror(serrno));
        exit(1);
    }
    /* ...                                  */
    /* ... Call a VDQM API library function */
    /* ...                                  */

    /* Close the connection */
    rc = \fBvdqm_Disconnect\fP(nw); 
    if ( rc == -1 ) {
        fprintf(stderr,"vdqm_Disconnect(): %s\\n",sstrerror(serrno));
        exit(1);
    }
    exit(0);
}
.fi

.SH RETURN VALUES
.PP
On successful completion, the
.B vdqm_Connect
function returns 0. Otherwise, a value of \-1 is returned and
.B serrno
is set to indicate the error.
.SH ERRORS
.PP
If the
.B vdqm_Connect
function fails,
.B serrno
may be set to one of the following values:
.TP
.B SEINTERNAL
Unexpected internal error 
.TP
.B SENOSSERV
VDQM service unknown.
.TP
.B SENOSHOST
VDQM host unknown.
.TP
.B SECOMERR
A network library call failed when trying to establish the connection
with the VDQM server.
.TP
.B EINVAL
The nw parameter is not correctly initialised.
.TP
.B SEWOULDBLOCK
Failed to allocate memory to store the connection.

.SH SEE ALSO
.BR vdqm_Disconnect(3) , 
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
