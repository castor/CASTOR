.\"
.\"
.\" Copyright (C) 2000 by CERN/IT/PDP/DM
.\"
.TH VDQM_NEXTDRIVE l "$Date: 2001/09/26 09:13:56 $" "CASTOR" "VDQM Library Functions"
.SH NAME
.PP
vdqm_NextDrive \- get information about next drive in a running VDQM query
.SH SYNOPSIS
.br
\fB#include "Castor_limits.h"\fR
.br
\fB#include "osdep.h"\fR
.br
\fB#include "net.h"\fR
.br
\fB#include "Ctape_constants.h"\fR
.br
\fB#include "vdqm_api.h"\fR
.sp
.BI "int vdqm_NextDrive ("
.br
.BI "                const vdqmnw_t *" nw ,
.br
.BI "                vdqmDrvReq_t *" drvreq );
.SH PARAMETERS
.I nw
\- points to a structure containing internal VDQM API network information 
returned by a prior call to
.B vdqm_Connect(3).
If this parameter is NULL, which is the recommended usage,
.B vdqm_NextDrive
will internally call 
.B vdqm_Connect(3)
and
.B vdqm_Disconnect(3)
to open and close the connection with the VDQM server.
.PP
.I drvreq
\- VDQM structure (described below) containing all available information
about the drive.
.PP
.SH DESCRIPTION
.B vdqm_NextDrive
returns information about the next drive in a running query of the VDQM
database. The first call to 
.B vdqm_NextDrive
initiates the query and returns information about the first drive matching
the specified device group (if any). The connection to the VDQM server stays
open between two calls to
.B vdqm_NextDrive
in a running query. In order to guarantee consistent information the lock
on the queue is also maintained between two calls to
.B vdqm_NextDrive.
This means that the calls to
.B vdqm_NextDrive
should be put in a critical section, where there are no other blocking calls
(the query will eventually timeout). 

.B vdqm_NextDrive
returns -1 and sets 
.B serrno
to 
.B EVQEOQREACHED
when there are no more drives matching the query.

The drive record returned by 
.B vdqm_NextDrive
is defined in the
.B vdqm.h
file as follows:
.sp
.BI "typedef struct vdqmDrvReq " {
.br
.BI "                  int " status ;
.br
.BI "                  int " DrvReqID ;
.br
.BI "                  int " VolReqID ;
.br
.BI "                  int " jobID ;
.br
.BI "                  int " recvtime ;
.br
.BI "                  int " resettime ;
.br
.BI "                  int " usecount ;
.br
.BI "                  int " errcount ;
.br
.BI "                  int " MBtransf ;
.br
.BI "                  int " mode ;
.br
.BI "                  u_signed64 " TotalMB ;
.br
.BI "                  char " reqhost[CA_MAXHOSTNAMELEN+1] ;
.br
.BI "                  char " volid[CA_MAXVIDLEN+1] ;
.br
.BI "                  char " server[CA_MAXHOSTNAMELEN+1] ;
.br
.BI "                  char " drive[CA_MAXUNMLEN+1] ;
.br
.BI "                  char " dgn[CA_MAXDGNLEN+1] ;
.br
.BI "                  char " dedicate[CA_MAXLINELEN+1] ;
.br
.BI "} vdqmDrvReq_t" ;

Where the elements are:
.PP
.I status
\- Current drive status. The drive status is formed by the 
bitwise-inclusive-or of predefined status constants described in
.B vdqm_UnitStatus(3).
.PP
.I DrvReqID
\- Internal drive ID assigned by the VDQM server. Each drive has a unique
drive ID in the VDQM database.
.PP
.I VolReqID
\- ID of the volume request currently assigned to the drive (if any).
.PP
.I jobID
\- RTCOPY job ID of current request served by the drive (if any).
.PP
.I recvtime
\- Time of last status update
.PP
.I resettime
\- Time of last reset of the counter elements defined below.
.PP 
.I usecount
\- Usage counter (total number of volume requests servered so far).
.PP
.I errcount
\- Drive error counter.
.PP
.I MBtransf
\- MBytes transfered in last request.
.PP
.I mode
\- Tape access mode. Either of WRITE_ENABLE/WRITE_DISABLE defined in the
.B Ctape_constants.h
include file.
.PP
.I TotalMB
\- Total MBytes transfered (since last counter reset).
.PP
.I reqhost
\- Client request host name.
.PP
.I volid
\- Volume ID (VID) of mounted volume (if any).
.PP
.I server
\- Server host name.
.PP
.I drive
\- Drive name.
.PP
.I dgn
\- Device group name.
.PP
.I dedicate
\- Regular expression specifying drive dedication.

.SH EXAMPLE
Here follows an example showing how call
.B vdqm_NextDrive
to VDQM database for all drives in the specified device group.
.P
.nf
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <Castor_limits.h>
#include <serrno.h>
#include <osdep.h>
#include <net.h>
#include <Ctape_constants.h>
#include <Ctape_api.h>
#include <vdqm_api.h>

int main(int argc, char *argv) {
    int rc, last_id;
    vdqmnw_t **nw = NULL;
    char *dgn = NULL;
    vdqmDrvReq_t drvreq;

    if ( argc > 1 ) dgn = argv[1];

    /*
     * Initialise the structure
     */
    memset(&drvreq,'\0',sizeof(vdqmDrvReq_t));
    if ( dgn != NULL ) strcpy(drvreq.dgn,dgn);
    /*
     * Loop on query until no more info. is returned.
     */
    while ( (rc = \fBvdqm_NextDrive\fP(nw,&drvreq)) != -1 ) {
        /*
         * The VDQM server can sometimes return empty records or
         * repeate previous record.
         */
        if ( *drvreq.server != '\0' && *drvreq.drive != '\0' &&
             drvreq.DrvReqID != last_id ) {
            last_id = drvreq.DrvReqID;
            printf("%s@%s VID: %s, job ID: %d\\n",
                   drvreq.drive,drvreq.server,drvreq.volid,drvreq.jobID);
        }
    }
    exit(0);
}
.fi

.SH RETURN VALUES
.PP
On successful completion, the
.B vdqm_NextDrive
function returns 0. Otherwise, a value of \-1 is returned and
.B serrno
is set to indicate the error.
.SH ERRORS
.PP
If the
.B vdqm_NextDrive
function fails,
.B serrno
may be set to one of the following values:
.TP
.B SEINTERNAL
Unexpected internal error 
.TP
.B SENOSSERV
VDQM service unknown.
.TP
.B SENOSHOST
VDQM host unknown.
.TP
.B SECOMERR
A network library call failed when trying to establish the connection
with the VDQM server.
.TP
.B EINVAL
A parameter or combination of parameters is invalid. For instance if
any of the required parameters device group name (dgn) or drive name (unit)
is a NULL pointer.
.TP
.B EVQSYERR
A fatal system call (e.g. calloc()) failure in VDQM server.
.TP
.B EVQHOLD
The server is in HOLD status. This is normally a temporary error due
to VDQM server maintenance. Client should retry after a short delay.
.TP
.B EVQEOQREACHED
Running query reached its end.

.SH SEE ALSO
.BR vdqm_Connect(3), 
.BR vdqm_Disconnect(3),
.BR vdqm_UnitStatus(3),
.BR vdqm_NextVol(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
