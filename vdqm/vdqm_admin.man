.\" Copyright (C) 2000 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH VDQM_ADMIN "1castor" "$Date: 2002/11/05 16:41:50 $" CASTOR "VDQM Administrator Commands"
.SH NAME
vdqm_admin \- administrate VDQM server
.SH SYNOPSIS
.B vdqm_admin
.RB [ -ping ]
.RB [ -dedicate ]
.RB [ -deldrv ]
.RB [ -delvol ]
.RB [ option value ... ]

.SH DESCRIPTION
.B vdqm_admin
permits various administrative operation on the VDQM server itself as well as
some actions on drive and volume requests managed by the VDQM server. The
command syntax consist of a switch describing the action (e.g. ping)
followed by a
.I option value ...
pairs that depend on the requested action. Possible options-value pairs are
.sp
.RS
.BI "-dgn " device_group
.sp
.BI "-server " server_name
.sp
.BI "-drive " drive_name
.sp
.BI "-vid " VID
.sp
.BI "-match " dedicate_string
.sp
.BI "-reqid " request_ID
.RE
.sp
Here follows a description of the individual actions and the associated
option-value pair.
.PP
.BI "-ping"
\- ping the VDQM server to detect whether it is ALIVE or DEAD. The
following option-value pairs can be specified: 
.sp
.BI "-dgn " device_group
.sp
.BI "-vid " VID
.sp
.BI "-reqid " request_ID
.RS
In this case the queue position for that volume request is given.
.RE
.sp
.BI "-dedicate"
\- dedicate the specified drive to clients matching the dedication expression.
The following option-value pairs must be provided:
.sp
.RS
.BI "-dgn " device_group
.sp
.BI "-server " server_name
.sp
.BI "-drive " drive_name
.sp
.BI "-match " dedication_string
.RS
If the
.BI "-match"
option is omitted any previous dedication of that drive will be revoked.
The patter matching string must be of the form:
.I uid=.*,gid=.*,name=.*,host=.*,vid=.*,mode=.*,datestr=.*,timestr=.*,age=.*
(only the fields to be changed need to be specified, see example below).
.RE
.RE
.sp
.BI "-deldrv"
\- delete the specified drive from the VDQM data base.
The following option-value pairs must be provided:
.sp
.RS
.BI "-dgn " device_group
.sp
.BI "-server " server_name
.sp
.BI "-drive " drive_name
.RE
.sp
.BI "-delvol"
\- delete the specified volume request from the VDQM data base.
The following option-value pairs must be provided:
.sp
.RS
.BI "-dgn " device_group
.sp
.BI "-reqid " request_ID
.RE

.SH EXAMPLE
.nf
.ft CW
% vdqm_admin -ping
ALIVE
% showqueues -g SD3R
drv1@test (1220 MB) status FREE vid:  last update Jun 15 17:22:09
drv2@test (14525 MB) status FREE vid:  last update Jun 15 17:22:09
% vdqm_admin -dedicate -dgn SD3R -server test -drive drv1 -match 'gid=1160,host=test[0-5]'
% vdqm_admin -deldrv -dgn SD3R -server test -drive drv2
% showqueues -g SD3R
drv1@test (1220 MB) status FREE vid:  last update Jun 15 17:22:09
.ft
.fi
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH SEE ALSO
.B showqueues(1)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
