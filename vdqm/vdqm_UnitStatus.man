.\"
.\"
.\" Copyright (C) 1999-2000 by CERN/IT/PDP/DM
.\"
.TH VDQM_UNITSTATUS "3castor" "$Date: 2001/09/26 09:13:56 $" "CASTOR" "VDQM Library Functions"
.SH NAME
.PP
vdqm_UnitStatus \- update volume drive unit status in VDQM
.SH SYNOPSIS
.br
\fB#include "Castor_limits.h"\fR
.br
\fB#include "osdep.h"\fR
.br
\fB#include "net.h"\fR
.br
\fB#include "vdqm_api.h"\fR
.sp
.BI "int vdqm_UnitStatus ("
.br
.BI "                const vdqmnw_t *" nw ,
.br
.BI "                char *" VID ,
.br
.BI "                const char *" dgn ,
.br
.BI "                const char *" server,
.br
.BI "                const char *" unit ,
.br
.BI "                int *" status ,
.br
.BI "                int *" value ,
.br
.BI "                int " jobID );
.SH PARAMETERS
.I nw
\- points to a structure containing internal VDQM API network information returned
by a prior call to 
.B vdqm_Connect(3).
If this parameter is NULL, which is the recommended usage,
.B vdqm_UnitStatus
will internally call 
.B vdqm_Connect(3)
and
.B vdqm_Disconnect(3)
to open and close the connection with the VDQM server.
.PP
.I VID
\- character string specifying the volume identifier. This parameter is ignored for
all status values except VDQM_VOL_MOUNT and VDQM_VOL_UNMOUNT.
.PP
.I dgn
\- character string specifying the device group name associated with the unit.
.PP
.I server
\- character string specifying the hostname of the tape server. This parameter 
may be left NULL if the server is the same as the local host.
.PP
.I unit
\- character string specifying the drive unit name.
.PP
.I status
\- points to an integer which is constructed by a bitwise-inclusive-OR of unit
status flags. These flags are defined in the "vdqm_constants.h" header file and are
described below. This parameter may be changed on output to specify some action
recommended by the VDQM server (currently only VDQM_VOL_UNMOUNT).
.PP
.I value
\- points to an integer which contains a value associated with the status parameter.
.PP
.I jobID
\- integer containing the assigned job ID. If this parameter is less or equal
to zero the job ID is taken to be the PID of the calling process.
.PP
.SH DESCRIPTION
.B vdqm_UnitStatus
updates the status of a server/unit pair in the Volume and Drive Queue
Manager (VDQM) server database. If the server/unit pair was not known in
the VDQM server database prior to calling this routine, it will be automatically
declared. Thus,
.B vdqm_UnitStatus
is the only VDQM API call needed by the process responsible for the server/unit.
.SS "Unit status flags"
The unit status flags are defined as follows:
.PP
.B VDQM_UNIT_UP
\- the unit is configured up. Note that the VDQM_UNIT_FREE flag must also 
be set before the VDQM server will dispatch work to this unit.
.PP
.B VDQM_UNIT_DOWN
\- the unit is configured down. If there was a running job present on the
unit, it will be replaced on top of the volume request queue. This status
can only be set by tape server. Requests from other hosts will be rejected.
.PP
.B VDQM_UNIT_WAITDOWN
\- the unit is configuring down. Either the tape server waits for volume
to unload and dismount or the operator has requested unit to be down and
VDQM is waiting for confirmation from the tape server.
.PP
.B VDQM_UNIT_FREE
\- the unit is free and ready to receive a new request.
.PP
.B VDQM_UNIT_ASSIGN
\- the unit is assigned. The
.I value
parameter specifies the VDQM client volume request identifier on input and the local 
job identifier (either the one assigned with the jobID parameter or process ID 
of calling process if jobID <= 0) on output. If the jobID parameter
is greater than zero the assigned jobID is taken from it. Otherwise the jobID
is taken to be the PID of the calling process.
.PP
.B VDQM_UNIT_RELEASE
\- the unit is released and de-assigned from previous job ID. On input the
.I jobID
parameter specifies the local job ID of the job currently associated with the unit.
On output the
.I status
parameter may have changed to have the VDQM_VOL_UNMOUNT flag set. This is a
recommendation from the VDQM server to unmount the volume from unit.
.PP
.B VDQM_VOL_MOUNT
\- the volume idenfier specified with the
.I VID
parameter has been mounted on the unit. On input the
.I jobID 
parameter specifies the local job ID of the job currently associated with the unit.
.PP
.B VDQM_VOL_UNMOUNT
\- the volume previously mounted on unit has been unmounted. Optionally the
.I VID
parameter may contain the volume identifier of the unmounted volume. VDQM will then
cross-check this value with its internal table and return an error if the volume
identifiers do not correspond.
.PP
.B VDQM_FORCE_UNMOUNT
\- the client forces an unmount. This status value overrides a potential
deferred dismount in case there was another job for that volume. This status
must be followed by a VDQM_VOL_UNMOUNT when the volume has been physically
unmounted, so that VDQM can assign the volume to another job.
.PP
.B VDQM_TPD_STARTED
\- tells VDQM that the tape daemon has restarted. All drives in all device
groups supported by the calling server are removed and any assigned volume
request is deleted. This status can only be set from a client on the tape
server itself. The server parameter is ignored. Any attempt to set this
status for another server results in an error.
.PP
.B VDQM_UNIT_QUERY
\- query the status of specified unit. For each of the input parameters that 
are not NULL pointers the corresponding value is returned (if available). The
.I value
parameter will contain either the running jobID (if status VDQM_UNIT_ASSIGN is
set) or the VolReqID (if status is VDQM_UNIT_BUSY but not VDQM_UNIT_ASSIGN).
Note that the VDQM_UNIT_QUERY should not be combined with any other status. If
it is, the VDQM_UNIT_QUERY is ignored.
.PP
.B VDQM_UNIT_ERROR
\- increment error counter for this unit in VDQM. This status should be
set after a medium error or a drive error. This status should always be
followed by a VDQM_VOL_UNMOUNT when the volume has been physically unmounted.
.PP
.B VDQM_UNIT_MBCOUNT
\- increment the MBytes transferred counter for this unit in VDQM. The
.I value
parameter specifies the number of MBytes transferred. 
.PP
In addition to the listed status flags the VDQM server maintains the following
two internal unit status flags. Attempt to set these flags with
.B vdqm_UnitStatus
will result in an error.
.PP
.B VDQM_UNIT_BUSY
\- the unit has been assigned a volume request but the VDQM_UNIT_ASSIGN status has
not yet been received.
.PP
.B VDQM_UNIT_UNKNOWN
\- an attempt failed to send a volume request to the unit. This status will be
reset after a configurable time limit or upon reception of a unit status request.

.SH EXAMPLE
Here follows an example showing how a job is assigned, volume is mounted,
job released and volume unmounted.
.P
.nf
#include <stdio.h>
#include <stdlib.h>
#include <Castor_limits.h>
#include <serrno.h>
#include <osdep.h>
#include <net.h>
#include <vdqm_api.h>

int main(int argc, char *argv) {
    int VolReqID, jobID, value, status, rc;
    char *unit, *dgn, *VID;

    if ( argc < 5 ) {
        fprintf(stderr,"Usage: %s VolReqID unit dgn VID\\n",argv[0]);
        exit(2);
    }
    VolReqID = atoi(argv[1]);
    unit = argv[2];
    dgn = argv[3];
    VID = argv[4];

    /*
     * Assign the job. This call associates the VDQM specific VolReqID with
     * the actual jobID which is to be used in all subsequent calls.
     * The VolReqID is the key that identifies the volume request in
     * the VDQM volume queue. It is given to the client that requested
     * the volume to be mounted (see vdqm_SendVolReq()).
     */
    status = VDQM_UNIT_ASSIGN;
    jobID = getpid();
    rc = \fBvdqm_UnitStatus\fP(NULL,NULL,dgn,NULL,unit,&status,&VolReqID,jobID);
    if ( rc == -1 ) {
        fprintf(stderr,"vdqm_UnitStatus(): %s\\n",sstrerror(serrno));
        exit(1);
    }
    /*
     * Do the physical mount....
     */
    ...
    status = VDQM_VOL_MOUNT;
    rc = \fBvdqm_UnitStatus\fP(NULL,VID,dgn,NULL,unit,&status,&value,jobID);
    if ( rc == -1 ) {
        fprintf(stderr,"vdqm_UnitStatus(): %s\\n",sstrerror(serrno));
        exit(1);
    }
    /*
     * Do the tape IO...
     */
   ...
    /*
     * Job is finished. Release the reservation.
     */
    status = VDQM_UNIT_RELEASE;
    rc = \fBvdqm_UnitStatus\fP(NULL,VID,dgn,NULL,unit,&status,&value,jobID);
    if ( rc == -1 ) {
        fprintf(stderr,"vdqm_UnitStatus(): %s\\n",sstrerror(serrno));
        exit(1);
    }
    /*
     * Check if VDQM wants us to keep volume mounted for another job.
     */
    if ( (status & VDQM_VOL_UNMOUNT) ) {
        /*
         * No other job. We physically unload and unmount the volume....
         */
        ...
        status = VDQM_VOL_UNMOUNT;
        rc = \fBvdqm_UnitStatus\fP(NULL,VID,dgn,NULL,unit,&status,&value,jobID); 
        if ( rc == -1 ) {
            fprintf(stderr,"vdqm_UnitStatus(): %s\\n",sstrerror(serrno));
            exit(1);
        }
    }
    exit(0);
}
.fi

.SH RETURN VALUES
.PP
On successful completion, the
.B vdqm_UnitStatus
function returns 0. Otherwise, a value of \-1 is returned and
.B serrno
is set to indicate the error.
.SH ERRORS
.PP
If the
.B vdqm_UnitStatus
function fails,
.B serrno
may be set to one of the following values:
.TP
.B SEINTERNAL
Unexpected internal error 
.TP
.B SENOSSERV
VDQM service unknown.
.TP
.B SENOSHOST
VDQM host unknown.
.TP
.B SECOMERR
A network library call failed when trying to establish the connection
with the VDQM server.
.TP
.B EINVAL
A parameter or combination of parameters is invalid. For instance if
any of the required parameters unit name, device group name and status
is a NULL pointer. There are also some invalid combinations of the status
and value/jobID parameters:
either a VDQM_UNIT_ASSIGN request with a NULL value field or an invalid
jobID (less or equal to zero), or a VDQM_VOL_MOUNT, VDQM_UNIT_RELEASE
or VDQM_UNIT_MBCOUNT request with an invalid jobID. 
.TP
.B EVQNOSDRV
An update or a query has been issued for a drive that is not known to VDQM.
.TP
.B EPERM
Either a VDQM_UNIT_DOWN or VDQM_TPD_STARTED request from host different from 
the tape server or a VDQM_VOL_MOUNT for a request without associated volume 
request and the volume is busy.
.TP
.B EVQSYERR
A fatal system call (e.g. calloc())failure in VDQM server.
.TP
.B EVQUNNOTUP 
A unit status request (different from VDQM_UNIT_UP) on unit which
is not up.
.TP
.B EVQBADSTAT
Requested unit status is inconsistent with current status. The specific
cases are:
.br
 1) VDQM_UNIT_FREE attempted on unit with assigned job. 
.br
 2) VDQM_UNIT_FREE attempted on unit with mounted volume. 
.TP
.B EVQBADID
A unit status request was issued with either a wrong VolReqID (if
VDQM_UNIT_ASSIGN) or jobID (any other request).
.TP
.B EVQNOTASS
A VDQM_UNIT_MOUNT unit status to mount a volume was requested on a unit
which has not yet been assigned.
.TP
.B EVQBADVOLID
A VDQM_UNIT_MOUNT unit status to mount a volume was requested with a
different VOLID than specified in assinged volume request.
.TP
.B EVQHOLD
The server is in HOLD status. This is normally a temporary error due
to VDQM server maintenance. Client should retry after a short delay.

.SH SEE ALSO
.BR vdqm_Connect(3) ,
.BR vdqm_Disconnect(3) , 
.BR vdqm_SendVolReq(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
