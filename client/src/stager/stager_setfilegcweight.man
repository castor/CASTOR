.\" Copyright (C) 2005 by CERN/IT
.\" All rights reserved
.\"
.TH STAGER_SETFILEGCWEIGHT "1castor"castor "$Date: 2008/04/18 08:12:26 $" CASTOR "STAGER Commands"
.SH NAME
stager_setfilegcweight \- Updates the weight of a file for the garbage collection 
.SH SYNOPSIS
.B stager_setfilegcweight
.BI -M
.BI hsmfile
[
.BI -M
.BI ...
]
[
.BI -S
.BI service_class
]
[
.BI -h
]
.BI <weight>
.SH DESCRIPTION
The
.B stager_setfilegcweight
command updates, for one or many castor file(s) in the stager, the weight used to rank
the file(s) at garbage collection time. The passed weight is added to the current one
of the correspondent disk copies, and it can be thought as a time interval in seconds
the disk copies would gain before being garbage collected.

The
.B stager_setfilegcweight
command is restricted to group administrators.

.SH OPTIONS

.TP
.BI \-M,\ \-\-filename " hsmfile"
specifies the CASTOR HSM file(s) for which the weight should be changed.
.TP
.BI \-S,\ \-\-service_class " service_class"
specifies the service class where the file resides. If no service class
is given a default will be assigned by the stager.
.TP
.BI \-h,\ \-\-help
display command usage help
.TP
.BI <weight>
the new weight of the file(s)
.TP

.SH EXAMPLE
.fi
$ stager_setfilegcweight -M /castor/cern.ch/user/t/test/file1 86400
.fi
Received 1 responses
.fi
/castor/cern.ch/user/t/test/file1 SUBRQUEST_READY
.fi
# This file gained 1 day with respect to another file of the same size
.fi
# and last accessed at the same time.
.fi

.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.

.SH SEE ALSO
.BR stager_qry(1castor)

.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
