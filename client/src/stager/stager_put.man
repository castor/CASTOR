.\" Copyright (C) 2005 by CERN/IT
.\" All rights reserved
.\"
.TH STAGER_PUT "1castor"castor "$Date: 2008/06/02 16:27:29 $" CASTOR "STAGER Commands"
.SH NAME
stager_put \- request for writing a file into CASTOR
.SH SYNOPSIS
.B stager_put
[
.BI -M
.BI hsmfile
[
{
.BI -M
}
.BI ...
]]
[
.BI -f
.BI hsmFileList
]
[
.BI -S
.BI service_class
]
[
.BI -U
.BI usertag
]
[
.BI -r
]
[
.BI -h
]
.SH DESCRIPTION
The
.B stager_put
command starts a put/update session in CASTOR. It is primarily intended
for applications that need to open the same set of files several times (e.g. for updates)
before they are migrated to tape. The command should be used in conjunction with the
.B stager_putdone(1castor)
command, which marks the files ready for migration. For most purposes the
.B stager_put
command does not provide any advantage compared to using RFIO or other CASTOR
aware protocols (e.g ROOT) directly. Since the command locks files in STAGEOUT
status on the disk pools the client
.I must
take care of ending the 'transaction' using the
.B stager_putdone(1castor)
command. Site-policies may enforce timeout for non-terminated
.B stager_put
sessions allowing the CASTOR administrators to cleanup the files in STAGEOUT status.

.SH OPTIONS

.TP
.BI \-M,\ \-\-filename " hsmfile"
specifies the CASTOR HSM file(s) to be created.
.TP
.BI \-f,\ \-\-filelist " hsmFileList"
specifies a file containing a list of CASTOR HSM file(s) to be created, one per line.
This can be used to avoid the command line restrictions when many files should be created at once.
.TP
.BI \-S,\ \-\-service_class " service_class"
specifies the service class to be used when creating the files. If no service class
is given a default will be assigned by the stager.
.TP
.BI \-U,\ \-\-usertag " usertag"
allows for associating a usertag that can be used for identifying the request in a subsequent
.B stager_qry(1castor)
command.
.TP
.BI \-r,\ \-\-display_reqid
if specified, the request identifier asssigned by the stager will be printed to standard
output. The request identifier can be used instead of the castor filenames when closing
the put session with
.BI stager_putdone(1castor)
.TP
.BI \-h,\ \-\-help
display command usage help
.TP

.SH EXAMPLE
.fi
$ stager_put -M /castor/cern.ch/user/t/test/file1 -U test1 -r
.fi
42b002d7-0000-1000-9db8-cf9fcba30800
.fi
/castor/cern.ch/user/t/test/file1 SUBRQUEST_READY
.fi
$ rfcp /etc/group /castor/cern.ch/user/t/test/file1
.fi
2317 bytes in 0 seconds through local (in) and eth0 (out)
.fi
$ stager_qry -U test1
.fi
Received 1 responses
.fi
59316604@cnsuser STAGEOUT
.fi
$ rfcp /etc/passwd /castor/cern.ch/user/t/test/file1
.fi
1240856 bytes in 0 seconds through local (in) and eth0 (out)
.fi
$ stager_putdone -M /castor/cern.ch/user/t/test/file1
.fi
/castor/cern.ch/user/t/test/file1 SUBRQUEST_READY
.fi
$ stager_qry -M /castor/cern.ch/user/t/test/file1
.fi
Received 1 responses
.fi
59316604@cnsuser CANBEMIGR
.fi

.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.

.SH SEE ALSO
.BR stager_qry(1castor) ,
.BR stager_get(1castor) ,
.BR stager_update(1castor) ,
.BR stager_putdone(1castor),
.BR rfcp(1castor)

.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
