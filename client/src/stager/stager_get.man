.\" Copyright (C) 2005 by CERN/IT
.\" All rights reserved
.\"
.TH STAGER_GET "1castor"castor "$Date: 2008/06/02 16:27:29 $" CASTOR "STAGER Commands"
.SH NAME
stager_get \- pre-stage existing CASTOR files to disk
.SH SYNOPSIS
.B stager_get
[
.BI -M
.BI hsmfile
[
{
.BI -M
}
.BI ...
]]
[
.BI -f
.BI hsmFileList
]
[
.BI -S
.BI service_class
]
[
.BI -U
.BI usertag
]
[
.BI -r
]
[
.BI -h
]
.SH DESCRIPTION
.B stager_get
pre-stages a list of CASTOR HSM files from tape to disk. The command
only supports asychronous stage-in operation meaning that it returns immediately
after the stager has stored and acknowledged the request. The command does not wait for the
files to be staged in and it is supposed to be used in conjunction with the

Note that this command will also trigger the remigration/repair of missing segments for the involved
files be repacked. A typical case is when the second copy of a two copies file has been lost and
stager_get recalls the first copy. Also note that segments on EXPORTED tapes will be considered as
lost and thus remigrated/repaired. This is different behavior repack for which they will not be.

.B stager_qry(1castor)
command to check the progress of the request.

.SH OPTIONS

.TP
.BI \-M,\ \-\-filename " hsmfile"
specifies the CASTOR HSM file(s) to be staged in. The
.B \-M
option can be repeated, allowing to stage in several files with a single command.
.TP
.BI \-f,\ \-\-filelist " hsmFileList"
specifies a file containing a list of CASTOR HSM file(s) to be staged in, one per line.
This can be used to avoid the command line restrictions when many files should be staged in at once.
.TP
.BI \-S,\ \-\-service_class " service_class"
specifies the service class to be used when staging in the file. If no service class
is given a default will be assigned by the stager.
.TP
.BI \-U,\ \-\-usertag " usertag"
allows for associating a usertag that can be used for identifying the request in a subsequent
.B stager_qry(1castor)
command.
.TP
.BI \-r,\ \-\-display_reqid
if specified, the request identifier asssigned by the stager will be printed to standard
output. The request identifier can be used instead of the castor filenames when closing
the put-session with
.BI stager_putdone(1castor)
.TP
.BI \-h,\ \-\-help
display command usage help
.TP

.SH EXAMPLE
.fi
$ stager_get -M /castor/cern.ch/user/t/test/file1 -M /castor/cern.ch/user/t/test/file2 -U test1
.fi
Received 2 responses
.fi
/castor/cern.ch/user/t/test/file1 SUBREQUEST_READY
.fi
/castor/cern.ch/user/t/test/file2 SUBREQUEST_READY
.fi
$ stager_qry -U test1
.fi
Received 2 responses
.fi
59316604@cnsuser STAGEIN
.fi
59316617@cnsuser STAGED
.fi

.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.

.SH SEE ALSO
.BR stager_qry(1castor) ,
.BR stager_update(1castor)
.BR repack(1castor)

.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
