.\" Copyright (C) 2005 by CERN/IT
.\" All rights reserved
.\"
.TH STAGER_RM "1castor"castor "$Date: 2009/03/25 13:23:36 $" CASTOR "STAGER Commands"
.SH NAME
stager_rm \- request for removing a file from the CASTOR stager
.SH SYNOPSIS
.B stager_rm
[
.BI -S
.BI svcClass
|
.BI -a
]
[
.BI -M
.BI hsmfile
[
.BI -M
.BI ...
]]
[
.BI -f
.BI hsmFileList
]
[
.BI -h
]
.SH DESCRIPTION
The
.B stager_rm
command removes one or several files from the CASTOR stager catalog. 
This includes stopping all running requests and marking all replicas 
of the files on the given service class for garbage collection.
Note that this does not mean that the file is no more on disk,
since other replicas may exist in other service classes, and a
running recall of the file will be untouched leading to a new
copy of the file on disk just after the stager_rm has been
performed.

stager_rm operations can only be performed if the user has proper
permissions. At the stager level, user needs StageRmRequest permissions
on the disk pool considered, or on the '*' disk pool if \-a/\-\-all/\-S\ '*'
is used. At the namespace level, user needs write privileges on the
parent directory of the file being removed. 
.B stager_listprivileges
, 
.B nsls\ \-l
and 
.B nsgetacl
can be used to check permissions.

.SH OPTIONS

.TP
.BI \-S,\ \-\-svcClass " svcClass"
specifies the service class where to remove the file replicas. If \-S is not specified, the command will be ran against the 'default' service classes. Note that in such a case, the STAGE_SVCCLASS environment variable and the stagemap file will be taken into account. In order to run the command accross all service classes, the special value '*' can be used.
.TP
.BI \-a,\ \-\-all
explicitly specifies to remove all available replicas of the files
from all service classes.
.TP
.BI \-M,\ \-\-filename " hsmfile"
specifies the CASTOR HSM file(s) to be removed.
.TP
.BI \-f,\ \-\-filelist " hsmFileList"
specifies a file containing a list of CASTOR HSM file(s) to be removed, one per line.
This can be used to avoid the command line restrictions when many files should be removed at once.
.TP
.BI \-h,\ \-\-help
display command usage help
.TP

.SH EXAMPLE
.fi
$ stager_rm -M /castor/cern.ch/user/t/test/file1
.fi
42cd469e-0000-1000-81ea-8514e17e0800 Received 1 responses
.fi
/castor/cern.ch/user/t/test/file1 SUBRQUEST_READY
.fi

.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed. Note that busy or non-existing files are not considered as
failures.

.SH SEE ALSO
.BR stager_listprivileges(1castor)
.BR nsls(1castor)
.BR nsgetacl(1castor)

.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
