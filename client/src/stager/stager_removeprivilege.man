.\"
.\"              stager_add/removeprivilege.man
.\"
.\" This file is part of the Castor project.
.\" See http://castor.web.cern.ch/castor
.\"
.\" Copyright (C) 2003  CERN
.\" This program is free software; you can redistribute it and/or
.\" modify it under the terms of the GNU General Public License
.\" as published by the Free Software Foundation; either version 2
.\" of the License, or (at your option) any later version.
.\" This program is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\" You should have received a copy of the GNU General Public License
.\" along with this program; if not, write to the Free Software
.\" Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
.\"
.\"
.\" man page for stager_add/removeprivilege
.\"
.\" @author Castor Dev team, castor-dev@cern.ch
.\"****************************************************************************"
.TH STAGER_ADDPRIVILEGE "1castor"castor "$Date: 2008/11/25 16:45:01 $" CASTOR "STAGER Commands"
.SH NAME
stager_addprivilege, stager_removeprivilege \- add/remove privileges to/from the B/W list of CASTOR
.SH SYNOPSIS
.B stager_addprivilege
.BI -U
.BI [user][:group][,[user][:group]]*
[
.BI -R
.BI type[,type]*
]
[
.BI -S
.BI serviceClass
]
[
.BI -h
]

.B stager_removeprivilege
.BI -U
.BI [user][:group][,[user][:group]]*
[
.BI -R
.BI type[,type]*
]
[
.BI -S
.BI serviceClass
]
[
.BI -h
]

.SH DESCRIPTION
The
.B stager_addprivilege
command allows to add privileges into the CASTOR Black and White list.

The
.B stager_removeprivilege
command allows to remove privileges from the CASTOR Black and White list.

In both cases, the list of privileges is optimized and reduced to the minimum set.
Results may thus be surprising. As an example, with the following privileges :

ServiceClass    User     Group    RequestType           Status
.fi
*               *        st       *                    Granted
.fi
*               stage    *        *                    Denied

Removing all privileges for stage:st will not change anything.


.SH OPTIONS

.TP
.BI \-U,\ \-\-users " [user][:group][,[user][:group]]*"
specifies the list of users concerned by the new privileges. The list is
a comma separated set of users, given as "[username][:[groupName]]". Note
that an empty list is however not allowed. To specify that any user and any
group is concerned, use ":". In case a user is given with no group, the
group will be filled with the primary group of the user.
.TP
.BI \-R,\ \-\-reqTypes " type[,type]"*
specifies the list of request types concerned by the new privileges. The list is
comma separated and should use predefined request types. See 
.BR stager_listprivileges(1castor)
for a complete list. Examples are StageGetRequest, StageRmRequest or ChangePrivilege.
To specify all request types, this parameter must be ommited.
.TP
.BI \-S,\ \-\-service_class " service_class"
specifies the service class concerned by the new privileges.
This parameter can be ommited, defaulting then to all service classes. Note
that ommiting the parameter is the only way to address all service classes.
Giving the value '*' will only use the string '*' as the service class name. This
has to be used in case you handle some particular privileges (e.g. querying, privilege listing)
where there is a notion of cross service class action. You can thus allow
querying on a given service class, but not cross service class. See the example below.
.fi
Note finally that is the \-S parameter is not specified and the environment variable
.B STAGE_SVCCLASS
is set, its value will be used.
.TP
.BI \-h,\ \-\-help
display command usage help
.TP

.SH EXAMPLE
# start with an empty list
.fi
$ stager_listprivileges

.fi
# add privileges to anybody on service class default
.fi
$ stager_addprivilege -S default -U ':'
.fi
Change done successfully

.fi
# add all privileges to user stage, group st
.fi
$ stager_addprivilege -U 'stage:st' 
.fi
Change done successfully

.fi
# but remove the privilege to change privileges to stage/st
.fi
$ stager_removeprivilege -U 'stage:st' -R ChangePrivilege
.fi
Change done successfully

.fi
# and remove the privilege to do cross service class queries
.fi
$ stager_removeprivilege -U ':' -R StageFileQueryRequest -S '*'
.fi
Change done successfully

.fi
# and here is the result
.fi
$ stager_listprivileges 
.fi
ServiceClass    User     Group    RequestType           Status
.fi
*               stage    st       ChangePrivilege       Denied
.fi
*               stage    st       *                     Granted
.fi
default         *        *        *                     Granted
.fi
'*'             *        *        StageFileQueryRequest Denied
.fi

.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.

.SH SEE ALSO
.BR stager_listprivilege(1castor)

.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
