.\" Copyright (C) 2000-2002 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH NSDELETECLASS "1castor" "$Date: 2008/11/03 10:37:05 $" CASTOR "Cns Administrator Commands"
.SH NAME
nsdeleteclass \- delete a fileclass definition
.SH SYNOPSIS
.B nsdeleteclass
<\f--id=CLASSID|--name=CLASSNAME\fR> [\fIOPTION\fR]...
.SH DESCRIPTION
.B nsdeleteclass
deletes a fileclass definition.
The fileclass can be identified by CLASSID or by CLASSNAME.
If both are specified, they must point at the same class.
.LP
This command requires ADMIN privileges in the Cupv database.
.SH OPTIONS
.TP
.BI -h,\ \-\-host=HOSTNAME
specify the name server hostname. This can also be set through the
CNS_HOST environment variable.
.TP
.BI --id=CLASSID
The class number of the class to be deleted.
.TP
.BI --name=CLASSNAME
The name of the class to be deleted.
.TP
.B \-\-help
Display this help and exit
.SH EXAMPLES
.nf
.ft CW
nsdeleteclass --id 2 --name user
.ft
.fi
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR Cns_deleteclass(3) ,
.B Cupvlist(1)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
