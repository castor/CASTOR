.\" Copyright (C) 2004-2005 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH CNS_READDIRG "3castor" "$Date: 2008/09/22 13:41:38 $" CASTOR "Cns Library Functions"
.SH NAME
Cns_readdirg \- read CASTOR directory opened by
.B Cns_opendir
in the name server
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "Cns_api.h"\fR
.sp
.BI "struct Cns_direnstatg *Cns_readdirg (Cns_DIR *" dirp )
.SH DESCRIPTION
.B Cns_readdirg
reads the CASTOR directory opened by
.B Cns_opendir
in the name server.
This routine returns a pointer to a structure containing the current directory
entry including the GUID associated.
.PP
.nf
.ft CW
struct Cns_direnstatg {
	u_signed64	fileid;
	char		guid[CA_MAXGUIDLEN+1];
	mode_t		filemode;
	int		nlink;		/* number of files in a directory */
	uid_t		uid;
	gid_t		gid;
	u_signed64	filesize;
	time_t		atime;		/* last access to file */
	time_t		mtime;		/* last file modification */
	time_t		ctime;		/* last metadata modification */
	short		fileclass;	/* not used */
	char		status;		/* ' ' --> online, 'm' --> migrated */
	char		csumtype[3];	/* "CS", "AD" or "MD" */
	char		csumvalue[CA_MAXCKSUMLEN+1];
	unsigned short	d_reclen;	/* length of this entry */
	char		d_name[1];	/* basename in variable length */
};
.ft
.fi
.PP
.B Cns_readdirg
caches a variable number of such entries, depending on the filename size, to
minimize the number of requests to the name server.
.TP
.I dirp
specifies the pointer value returned by
.BR Cns_opendir .
.SH RETURN VALUE
This routine returns a pointer to a structure containing the current directory
entry if the operation was successful or NULL if the end of the directory was
reached or if the operation failed. When the end of the directory is encountered,
serrno is not changed. If the operation failed,
.B serrno
is set appropriately.

As Cns_readdirg returns a null pointer
both at the end of the directory and on error, an application wishing to check
for error situations should set
.B serrno
to 0, then call Cns_readdirg, then check
.B serrno
and if it is non-zero, assume an error has occurred.
.SH ERRORS
.TP 1.3i
.B EBADF
File descriptor in DIR structure is invalid.
.TP
.B EFAULT
.I dirp
is a NULL pointer.
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.TP
.B ENSNACT
Name server is not running or is being shutdown.
.SH SEE ALSO
.BR Cns_closedir(3) ,
.BR Cns_opendir(3) ,
.BR Cns_rewinddir(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
