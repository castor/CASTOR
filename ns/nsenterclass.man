.\" Copyright (C) 2000-2002 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH NSENTERCLASS "1castor" "$Date: 2008/11/03 10:37:05 $" CASTOR "Cns Administrator Commands"
.SH NAME
nsenterclass \- define a new file class
.SH SYNOPSIS
.B nsenterclass
--name=\fICLASSNAME\fR --id=\fIID\fR [\fIOPTION\fR]...
.SH DESCRIPTION
.B nsenterclass
defines a new file class with the specified name and id.
.LP
This command requires ADMIN privileges in the Cupv database.
.SH OPTIONS
.TP
.BI -h,\ \-\-host=HOSTNAME
specify the name server hostname. This can also be set through the
CNS_HOST environment variable.
.TP
.BI --id=CLASSID
The class number must be strictly positive.
.TP
.BI --name=CLASSNAME
The class name must be at most CA_MAXCLASNAMELEN characters long.
.TP
.BI --nbcopies=NUM
Specifies the number of copies for a file. Each copy is written to a different
tape pool. Default is 1.
.TP
.B \-\-help
Display this help and exit
.SH EXAMPLES
.nf
.ft CW
nsenterclass --id 2 --name
.ft
.fi
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR Cns_enterclass(3) ,
.B Cupvlist(1)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
