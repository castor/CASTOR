.\" Copyright (C) 1999-2000 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH NSMKDIR "1castor" "$Date: 2008/11/03 10:37:05 $" CASTOR "Cns User Commands"
.SH NAME
nsmkdir \- make CASTOR directory in the name server
.SH SYNOPSIS
.B nsmkdir
[\fIOPTION\fR]... \fIDIRECTORY\fR...
.SH DESCRIPTION
.B nsmkdir
creates the specified CASTOR directories in the name server.
.LP
This requires write permission in the parent directory.
The owner ID and group ID of the new directories are set to the requestor's
real user ID and group ID, respectively.
.TP
.I DIRECTORY
specifies the CASTOR pathname.
If DIRECTORY does not start with
.BR / ,
it is prefixed by the content of the
.B CASTOR_HOME
environment variable.
.LP
The
.B nsmkdir
command has the following options:
.TP
.B -m,\ \-\-mode=MODE
specifies the mode to be used. Default mode is 777, minus the user umask.
.TP
.B -p,\ \-\-parents
creates all the non-existing parent directories first.
The mode set for the created intermediate directories is the logical difference
between 0777 and the user umask but at least 0300.
.B \-\-help
display this help and exit
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR Cns_chmod(3) ,
.BR Cns_mkdir(3) ,
.BR Cns_umask(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
