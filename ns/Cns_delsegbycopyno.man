.\" Copyright (C) 2003 by CERN
.\" All rights reserved
.\"
.TH CNS_delsegbycopyno "3castor" "$Date: 2009/07/09 12:43:40 $" CASTOR "Cns Library Functions"
.SH NAME
Cns_delsegbycopyno \- delete file segment by copyno
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "Cns_api.h"\fR
.sp
.BI "int Cns_delsegbycopyno (const char *" path ,
.BI "struct Cns_fileid *" file_uniqueid ,
.BI "int " copyno )
.SH DESCRIPTION
.B Cns_delsegbycopyno
deletes file segments from the name server by path and copy number. The file
can be identified by
.I path
name or by
.IR file_uniqueid .
If both are specified,
.I file_uniqueid
is used.
.TP
.I path
specifies the logical pathname relative to the current CASTOR directory or
the full CASTOR pathname.
.TP
.I copyno
is the copy number. If specified as zero all segments belonging to the file will
be deleted
.LP
This function requires ADMIN privileges in the Cupv database.
.RE
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the
operation failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOENT
The referenced file does not exist anymore.
.TP
.B EFAULT
.I path
and
.I file_uniqueid
are NULL pointers.
.TP
.B EISDIR
The file is not a regular file.
.TP
.B ENAMETOOLONG
The length of
.I path
exceeds
.B CA_MAXPATHLEN
or the length of a
.I path
component exceeds
.BR CA_MAXNAMELEN .
.TP
.B EINVAL
The copy number is negative.
.TP
.B SEENTRYNFND
No segment found for file and copy number.
.TP
.B SEINTERNAL
An internal error has occurred.
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.TP
.B ENSNACT
Name server is not running or is being shutdown.
.SH SEE ALSO
.BR Castor_limits(4)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
