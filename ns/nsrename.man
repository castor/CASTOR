.\" Copyright (C) 1999-2000 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH NSRENAME "1castor" "$Date: 2008/11/03 10:37:05 $" CASTOR "Cns User Commands"
.SH NAME
nsrename \- rename a CASTOR file or directory in the name server
.SH SYNOPSIS
.B nsrename
[\fIOPTION\fR]... \fISOURCE\f  \fIDEST\f
.SH DESCRIPTION
.B nsrename
renames a CASTOR file or directory in the name server.
.LP
This requires write permission in the parent directories.
.LP
If SOURCE or DEST
does not start with
.BR / ,
it is prefixed by the content of the
.B CASTOR_HOME
environment variable.
.SH OPTIONS
The following options are supported:
.TP
.B -v,\ \-\-verbose
Explain what is being done
.TP
.B -f,\ \-\-force
Do not prompt before overwriting the destination path if it already exists.
.TP
.B \-\-help
Display this help and exit
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR Cns_rename(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
