.\" Copyright (C) 1999-2005 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH NSD "8castor" "$Date: 2009/08/18 09:43:00 $" CASTOR "Cns Administrator Commands"
.SH NAME
nsd \- start the name server
.SH SYNOPSIS
.B nsd
[
.BI -f
] [
.BI -c " config_file"
] [
.BI -l " log_file"
] [
.B -r
] [
.BI -t " nbthreads"
] [
.B -s
] [
.B -n " nshostname"
] [
.B -L
]
.SH DESCRIPTION
.LP
The
.B nsd
command starts the name server.
This command is usually executed at system startup time
.RB ( /etc/rc.local ).
This will read the name server configuration file,
create the "/" entry in the database if it does not exist yet,
create a pool of threads and look for requests.
Each of them is processed in a thread which opens a connection to the
database server if necessary.
When a request has been completed, the thread becomes idle until it is allocated
to another request.
The connection to the database server is kept open between 2 requests.
If the name server is being shutdown or not active, the requests are
automatically retried by the client API.
.LP
All error messages and statistical information are kept in a log.
.LP
The name server listen port number can be defined on client hosts and
on the name server itself in either of the following ways:
.RS
.LP
setting an environment variable CNS_PORT and CNS_SEC_PORT for the secure port
.RS
.PP
.nf
setenv CNS_PORT 5010
setenv CNS_SEC_PORT 5510 (For secure server)
.fi
.RE
.LP
an entry in
.B /etc/castor/castor.conf
like:
.RS
.PP
.nf
CNS	PORT     5010
CNS	SEC_PORT 5510 (For secure server)
.fi
.RE
.LP
an entry in
.B /etc/services
like:
.RS
.PP
.nf
cns           5010/tcp # CASTOR name server
cns_sec       5510/tcp (For secure server)
.fi
.RE
.RE
.LP
If none of these methods is used, the default port number is taken from the
definition of CNS_PORT and CNS_SEC_PORT in Cns_constants.h.
.LP
The name server host name can be defined on client hosts
in either of the following ways:
.RS
.LP
setting an environment variable CNS_HOST, for example:
.RS
.HP
setenv CNS_HOST castor5
.RE
.LP
an entry in
.B /etc/castor/castor.conf
for example:
.RS
.HP
CNS	HOST	castor5
.RE
.RE
.LP
If none of these methods is used, the default host is "castorns".
.LP
The name server database keeps the metadata information: filename, file size,
access time, permissions and migration status.
.LP
The name server configuration file contains password information for the
database and must be readable/writable only by root.
It contains a single line in the format:
.HP
.RS
username/password@server
.RE
or
.RS
username/password@server/dbname
.RE
.sp
where 'username' and 'password' are the credentials to login to the database
instance identified by 'server'. If 'dbname' is not specified, "cns_db" is used.
.SH OPTIONS
.TP
.BI -f
Remain in the foreground.
.TP
.BI -c " config_file"
Specifies a different path for the Name Server configuration file.
.TP
.BI -l " log_file"
Specifies a different path for the Name Server log file.
.TP
.B -r
Read-only mode. Only query operations are allowed.
.TP
.BI -t " nbthreads"
Specifies the number of threads. Default is 20.
.TP
.B -s
Enables strong authentication mode.
.TP
.BI -n " nshostname"
Specifies the value of the NSHOSTNAME attribute used in Name Server log files.
By default the value is castorns.
.TP
.B -L
Forces nsd to listen on the loopback interface only.
.SH FILES
.TP 1.5i
.B /etc/NSCONFIG
configuration file
.TP
.B /var/log/castor/nsd.log
.fi
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
