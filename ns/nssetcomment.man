.\" Copyright (C) 2000-2002 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH NSSETCOMMENT "1castor" "$Date: 2008/11/03 10:37:05 $" CASTOR "Cns User Commands"
.SH NAME
nssetcomment \- add/replace a comment associated with a file/directory
.SH SYNOPSIS
.B nssetcomment
.I PATH
.I COMMENT
.SH DESCRIPTION
.B nssetcomment
adds/replaces a comment associated with a file/directory in the CASTOR
name server.
.LP
The effective user ID of the process must match the owner of the file or
the caller must have GRP_ADMIN or ADMIN privileges in the Cupv database.
.TP
.I PATH
specifies the CASTOR pathname.
If PATH does not start with
.BR / ,
it is prefixed by the content of the
.B CASTOR_HOME
environment variable.
.SH OPTIONS
.TP
.B \-\-help
display this help and exit
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR Cns_setcomment(3) ,
.B Cupvlist(1)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
