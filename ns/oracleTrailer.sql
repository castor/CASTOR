/*****************************************************************************
 *              oracleTrailer.sql
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

/* SQL statement to populate the intial schema version */
UPDATE UpgradeLog SET schemaVersion = '2_1_15_0'
 WHERE startDate = (SELECT max(startDate) FROM UpgradeLog);

/* Package holding type declarations for the NameServer PL/SQL API */
CREATE OR REPLACE PACKAGE castorns AS
  TYPE cnumList IS TABLE OF INTEGER INDEX BY BINARY_INTEGER;
  TYPE Segment_Rec IS RECORD (
    fileId NUMBER,
    lastOpenTime NUMBER,
    copyNo INTEGER,
    segSize INTEGER,
    comprSize INTEGER,
    vid VARCHAR2(6),
    fseq INTEGER,
    blockId RAW(4),
    checksum_name VARCHAR2(16),
    checksum INTEGER,
    gid INTEGER,
    creationTime NUMBER,
    lastModificationTime NUMBER
  );
  TYPE Segment_Cur IS REF CURSOR RETURN Segment_Rec;
  TYPE Stats_Rec IS RECORD (
    timeStamp NUMBER,
    timeInterval NUMBER,
    gid INTEGER,
    maxFileId INTEGER,
    fileCount INTEGER,
    fileSize INTEGER,
    segCount INTEGER,
    segSize INTEGER,
    segCompressedSize INTEGER,
    seg2Count INTEGER,
    seg2Size INTEGER,
    seg2CompressedSize INTEGER,
    fileIdDelta INTEGER,
    fileCountDelta INTEGER,
    fileSizeDelta INTEGER,
    segCountDelta INTEGER,
    segSizeDelta INTEGER,
    segCompressedSizeDelta INTEGER,
    seg2CountDelta INTEGER,
    seg2SizeDelta INTEGER,
    seg2CompressedSizeDelta INTEGER
  );
  TYPE Stats IS TABLE OF Stats_Rec;
END castorns;
/

/* Useful types */
CREATE OR REPLACE TYPE strList IS TABLE OF VARCHAR2(2048);
/
CREATE OR REPLACE TYPE numList IS TABLE OF INTEGER;
/

/**
 * Package containing the definition of some relevant (s)errno values and messages.
 */
CREATE OR REPLACE PACKAGE serrno AS
  /* (s)errno values */
  ENOENT          CONSTANT PLS_INTEGER := 2;    /* No such file or directory */
  EACCES          CONSTANT PLS_INTEGER := 13;   /* Permission denied */
  EBUSY           CONSTANT PLS_INTEGER := 16;   /* Device or resource busy */
  EEXIST          CONSTANT PLS_INTEGER := 17;   /* File exists */
  EISDIR          CONSTANT PLS_INTEGER := 21;   /* Is a directory */
  EINVAL          CONSTANT PLS_INTEGER := 22;   /* Invalid argument */
  
  SEINTERNAL      CONSTANT PLS_INTEGER := 1015; /* Internal error */
  SECHECKSUM      CONSTANT PLS_INTEGER := 1037; /* Bad checksum */
  ENSFILECHG      CONSTANT PLS_INTEGER := 1402; /* File has been overwritten, request ignored */
  ENSNOSEG        CONSTANT PLS_INTEGER := 1403; /* Segment had been deleted */
  ENSTOOMANYSEGS  CONSTANT PLS_INTEGER := 1406; /* Too many copies on tape */
  ENSOVERWHENREP  CONSTANT PLS_INTEGER := 1407; /* Cannot overwrite valid segment when replacing */
  ERTWRONGSIZE    CONSTANT PLS_INTEGER := 1613; /* (Recalled) file size incorrect */
  ESTNOSEGFOUND   CONSTANT PLS_INTEGER := 1723; /* File has no copy on tape or no diskcopies are accessible */
  
  /* messages */
  ENOENT_MSG          CONSTANT VARCHAR2(2048) := 'No such file or directory';
  EACCES_MSG          CONSTANT VARCHAR2(2048) := 'Permission denied';
  EBUSY_MSG           CONSTANT VARCHAR2(2048) := 'Device or resource busy';
  EEXIST_MSG          CONSTANT VARCHAR2(2048) := 'File exists';
  EISDIR_MSG          CONSTANT VARCHAR2(2048) := 'Is a directory';
  EINVAL_MSG          CONSTANT VARCHAR2(2048) := 'Invalid argument';
  
  SEINTERNAL_MSG      CONSTANT VARCHAR2(2048) := 'Internal error';
  SECHECKSUM_MSG      CONSTANT VARCHAR2(2048) := 'Checksum mismatch between segment and file';
  ENSFILECHG_MSG      CONSTANT VARCHAR2(2048) := 'File has been overwritten, request ignored';
  ENSNOSEG_MSG        CONSTANT VARCHAR2(2048) := 'Segment had been deleted';
  ENSTOOMANYSEGS_MSG  CONSTANT VARCHAR2(2048) := 'Too many copies on tape';
  ENSOVERWHENREP_MSG  CONSTANT VARCHAR2(2048) := 'Cannot overwrite valid segment when replacing';
  ERTWRONGSIZE_MSG    CONSTANT VARCHAR2(2048) := 'Incorrect file size';
  ESTNOSEGFOUND_MSG   CONSTANT VARCHAR2(2048) := 'File has no copy on tape or no diskcopies are accessible';
END serrno;
/


/* Get current time as a time_t. Not that easy in ORACLE */
CREATE OR REPLACE FUNCTION getTime RETURN NUMBER IS
  epoch            TIMESTAMP WITH TIME ZONE;
  now              TIMESTAMP WITH TIME ZONE;
  interval         INTERVAL DAY(9) TO SECOND;
  interval_days    NUMBER;
  interval_hours   NUMBER;
  interval_minutes NUMBER;
  interval_seconds NUMBER;
BEGIN
  epoch := TO_TIMESTAMP_TZ('01-JAN-1970 00:00:00 00:00',
    'DD-MON-YYYY HH24:MI:SS TZH:TZM');
  now := SYSTIMESTAMP AT TIME ZONE '00:00';
  interval         := now - epoch;
  interval_days    := EXTRACT(DAY    FROM (interval));
  interval_hours   := EXTRACT(HOUR   FROM (interval));
  interval_minutes := EXTRACT(MINUTE FROM (interval));
  interval_seconds := EXTRACT(SECOND FROM (interval));

  RETURN interval_days * 24 * 60 * 60 + interval_hours * 60 * 60 +
    interval_minutes * 60 + interval_seconds;
END;
/

/* Returns a time interval in seconds */
CREATE OR REPLACE FUNCTION getSecs(startTime IN TIMESTAMP, endTime IN TIMESTAMP) RETURN NUMBER IS
BEGIN
  RETURN TRUNC(EXTRACT(SECOND FROM (endTime - startTime)), 6);
END;
/

/* Function to convert seconds into a time string using the format:
 * DD-MON-YYYY HH24:MI:SS. If seconds is not defined then the current time
 * will be returned.
 */
CREATE OR REPLACE FUNCTION getTimeString
(seconds IN NUMBER DEFAULT NULL,
 format  IN VARCHAR2 DEFAULT 'DD-MON-YYYY HH24:MI:SS')
RETURN VARCHAR2 AS
BEGIN
  RETURN (to_char(to_date('01-JAN-1970', 'DD-MON-YYYY') +
          nvl(seconds, getTime()) / (60 * 60 * 24), format));
END;
/

/* Generate a universally unique id (UUID) */
CREATE OR REPLACE FUNCTION uuidGen RETURN VARCHAR2 IS
  ret VARCHAR2(36);
BEGIN
  -- Note: the guid generator provided by ORACLE produces sequential uuid's, not
  -- random ones. The reason for this is because random uuid's are not good for
  -- indexing!
  RETURN lower(regexp_replace(sys_guid(), '(.{8})(.{4})(.{4})(.{4})(.{12})', '\1-\2-\3-\4-\5'));
END;
/

/* Function to extract a configuration option from the castor config table.
 */
CREATE OR REPLACE FUNCTION getConfigOption(className VARCHAR2, optionName VARCHAR2, defaultValue VARCHAR2)
RETURN VARCHAR2 IS
  returnValue VARCHAR2(2048) := defaultValue;
BEGIN
  SELECT value INTO returnValue
    FROM CastorConfig
   WHERE class = className
     AND key = optionName
     AND value != 'undefined';
  RETURN returnValue;
EXCEPTION WHEN NO_DATA_FOUND THEN
  RETURN returnValue;
END;
/

/* A small procedure to add a line to the temporary SetSegsForFilesResultsHelper table.
 * The table is ultimately NOT a temporary table because Oracle does not support temporary tables
 * with distributed transactions, but it is used as such: see castor/db/oracleTapeGateway.sql.
 */
CREATE OR REPLACE PROCEDURE addSegResult(inIsOnlyLog IN INTEGER, inReqId IN VARCHAR2, inErrorCode IN INTEGER,
                                         inMsg IN VARCHAR2, inFileId IN NUMBER, inParams IN VARCHAR2) AS
BEGIN
  INSERT INTO SetSegsForFilesResultsHelper (isOnlyLog, reqId, timeinfo, errorCode, msg, fileId, params)
    VALUES (inIsOnlyLog, inReqId, getTime(), inErrorCode, inMsg, inFileId, inParams);
END;
/

/* A function to extract the full path of a file in one go */
CREATE OR REPLACE FUNCTION getPathForFileid(inFid IN NUMBER) RETURN VARCHAR2 IS
  CURSOR c IS
    SELECT /*+ NO_CONNECT_BY_COST_BASED */ name
      FROM Cns_file_metadata
    START WITH fileid = inFid
    CONNECT BY fileid = PRIOR parent_fileid
    ORDER BY level DESC;
  p VARCHAR2(2048) := '';
BEGIN
   FOR i in c LOOP
     p := p ||  '/' || i.name;
   END LOOP;
   -- remove first '/'
   p := replace(p, '///', '/');
   IF length(p) > 1024 THEN
     -- the caller will return SENAMETOOLONG
     raise_application_error(-20001, '');
   END IF;
   RETURN p;
END;
/

/* A function to compute the depth of the full path of a file in one go */
CREATE OR REPLACE FUNCTION getPathDepthForFileid(inFid IN NUMBER) RETURN INTEGER IS
  CURSOR c IS
    SELECT /*+ NO_CONNECT_BY_COST_BASED */ level
      FROM Cns_file_metadata
    START WITH fileid = inFid
    CONNECT BY fileid = PRIOR parent_fileid
    ORDER BY level DESC;
BEGIN
   FOR i in c LOOP
     RETURN i.level-1;
   END LOOP;
   RETURN -1;
END;
/

/**
 * This function drops a potentially existing segment with given copyNo, and then
 * checks for another copy of the same file on the same tape
 */
CREATE OR REPLACE FUNCTION checkAndDropSameCopynbSeg(inReqId IN VARCHAR2,
                                                     inFid IN INTEGER,
                                                     inVid IN VARCHAR2,
                                                     inCopyNo IN INTEGER) RETURN INTEGER IS
  varNb INTEGER;
  varRepSeg castorns.Segment_Rec;
  varBlockId VARCHAR2(8);
  varParams VARCHAR2(2048);
BEGIN
  -- First try and delete a previous segment with the same copyNo: this may exist
  -- even on fresh migrations when the stager fails to commit a previous migration attempt
  DELETE FROM Cns_seg_metadata
   WHERE s_fileid = inFid AND copyNo = inCopyNo
  RETURNING copyNo, segSize, compression, vid, fseq, blockId, checksum_name, checksum,
            gid, creationTime, lastModificationTime
       INTO varRepSeg.copyNo, varRepSeg.segSize, varRepSeg.comprSize, varRepSeg.vid,
            varRepSeg.fseq, varRepSeg.blockId, varRepSeg.checksum_name, varRepSeg.checksum,
            varRepSeg.gid, varRepSeg.creationTime, varRepSeg.lastModificationTime;
  IF varRepSeg.vid IS NOT NULL THEN
    -- we have some data, log
    SELECT varRepSeg.blockId INTO varBlockId FROM Dual;  -- to_char() of a RAW type does not work. This does the trick...
    varParams := 'copyNb=' || varRepSeg.copyNo ||' gid=' || varRepSeg.gid ||' SegmentSize=' || varRepSeg.segSize
      ||' Compression=' || varRepSeg.comprSize ||' TPVID=' || varRepSeg.vid
      ||' fseq=' || varRepSeg.fseq ||' BlockId="' || varBlockId
      ||'" ChecksumType="' || varRepSeg.checksum_name ||'" ChecksumValue=' || to_char(varRepSeg.checksum, 'XXXXXXXX')
      ||' creationTime=' || trunc(varRepSeg.creationTime, 6)
      ||' lastModificationTime=' || trunc(varRepSeg.lastModificationTime, 6);
    addSegResult(1, inReqId, 0, 'Unlinking segment (replaced)', inFid, varParams);
  END IF;
  -- Now that no previous segment exists with the same copyNo, prevent 2nd copy in the same tape
  SELECT count(*) INTO varNb
    FROM Cns_seg_metadata
   WHERE s_fileid = inFid AND s_status = '-' AND vid = inVid;
  IF varNb > 0 THEN
    RETURN serrno.EEXIST;
  ELSE
    RETURN 0;
  END IF;
END;
/


/**
 * This procedure creates a segment for a given file.
 * Return code:
 * 0  in case of success
 * ENOENT         if the file does not exist (e.g. it has been dropped meanwhile).
 * EISDIR         if the file is a directory.
 * EEXIST         if the file already has a valid segment on the same tape.
 * ENSFILECHG     if the file has been modified meanwhile.
 * SEINTERNAL     if another segment exists on the given tape at the given fseq location.
 * ENSTOOMANYSEGS if this copy exceeds the number of copies allowed by the file's fileclass.
 *                This also applies if inSegEntry refers to a file which fileclass allows 0 copies.
 */
CREATE OR REPLACE PROCEDURE setSegmentForFile(inSegEntry IN castorns.Segment_Rec,
                                              inReqId IN VARCHAR2,
                                              rc OUT INTEGER, msg OUT NOCOPY VARCHAR2) AS
  varFid NUMBER;
  varFmode NUMBER(6);
  varFLastMTime NUMBER;
  varFSize NUMBER;
  varFGid INTEGER;
  varFClassId NUMBER;
  varFCNbCopies NUMBER;
  varFCksumName VARCHAR2(2);
  varFCksum VARCHAR2(32);
  varNb INTEGER;
  varBlockId VARCHAR2(8);
  varParams VARCHAR2(2048);
  varLastOpenTimeFromClient NUMBER;
  varSegCreationTime NUMBER;
  -- Trap `ORA-00001: unique constraint violated` errors
  CONSTRAINT_VIOLATED EXCEPTION;
  PRAGMA EXCEPTION_INIT(CONSTRAINT_VIOLATED, -00001);
BEGIN
  rc := 0;
  msg := '';
  -- We truncate stagertime to 5 decimal digits, which is the precision of the lastOpenTime we get from the stager.
  -- Note this is just fitting the mantissa precision of a double, and it is due to the fact
  -- that those numbers go through OCI as double.
  SELECT fileId, filemode, TRUNC(stagertime, 5), fileClass, fileSize, csumType, csumValue, gid
    INTO varFid, varFmode, varFLastMTime, varFClassId, varFSize, varFCksumName, varFCksum, varFGid
    FROM Cns_file_metadata
   WHERE fileId = inSegEntry.fileId FOR UPDATE;
  varLastOpenTimeFromClient := inSegEntry.lastOpenTime;
  -- Is it a directory?
  IF bitand(varFmode, 4*8*8*8*8) > 0 THEN  -- 040000 == S_IFDIR
    rc := serrno.EISDIR;
    msg := serrno.EISDIR_MSG;
    ROLLBACK;
    RETURN;
  END IF;
  -- Has the file been changed meanwhile?
  IF varFLastMTime > varLastOpenTimeFromClient THEN
    rc := serrno.ENSFILECHG;
    msg := serrno.ENSFILECHG_MSG ||' : NSLastOpenTime='|| varFLastMTime
      ||', StagerLastOpenTime='|| varLastOpenTimeFromClient;
    ROLLBACK;
    RETURN;
  END IF;
  -- Cross check file and segment checksums when adler32 (AD in the file entry):
  -- unfortunately we have to play with different representations...
  IF (varFCksumName = 'AD' OR varFCksumName = 'PA') AND inSegEntry.checksum_name = 'adler32' AND
     inSegEntry.checksum != to_number(varFCksum, 'XXXXXXXX') THEN
    rc := serrno.SECHECKSUM;
    msg := serrno.SECHECKSUM_MSG ||' : '
      || upper(varFCksum) ||' vs '|| to_char(inSegEntry.checksum, 'XXXXXXXX');
    ROLLBACK;
    RETURN;
  END IF;
  -- Cross check segment (transfered) size and file size
  IF varFSize != inSegEntry.segSize THEN
    rc := serrno.ERTWRONGSIZE;
    msg := serrno.ERTWRONGSIZE_MSG ||' : expected '
      || to_char(varFSize) ||', got '|| to_char(inSegEntry.segSize);
    ROLLBACK;
    RETURN;
  END IF;
  -- Prevent to write a second copy of this file on a tape that already holds a valid copy,
  -- whilst allowing the overwrite of a same copyNo segment: this makes the NS operation
  -- fully idempotent and allows to compensate from possible errors in the tapegateway
  -- after a segment had been committed in the namespace.
  rc := checkAndDropSameCopynbSeg(inReqId, varFid, inSegEntry.vid, inSegEntry.copyNo);
  IF rc = serrno.EEXIST THEN
    msg := 'File already has a copy on VID '|| inSegEntry.vid;
    ROLLBACK;
    RETURN;
  END IF;
  
  -- We're done with the pre-checks, try and insert the segment metadata
  -- and deal with the possible unique (vid,fseq) constraint violation exception
  BEGIN
    varSegCreationTime := getTime();
    INSERT INTO Cns_seg_metadata (s_fileId, copyNo, fsec, segSize, s_status, vid,
      fseq, blockId, compression, side, checksum_name, checksum, gid, creationTime, lastModificationTime)
    VALUES (varFid, inSegEntry.copyNo, 1, inSegEntry.segSize, '-', inSegEntry.vid,
      inSegEntry.fseq, inSegEntry.blockId,
      CASE inSegEntry.comprSize WHEN 0 THEN 100 ELSE trunc(inSegEntry.segSize*100/inSegEntry.comprSize) END,
      0, inSegEntry.checksum_name, inSegEntry.checksum, varFGid, varSegCreationTime, varSegCreationTime);
  EXCEPTION WHEN CONSTRAINT_VIOLATED THEN
    -- This can be due to a PK violation or to the unique (vid,fseq) violation. The first
    -- is excluded because of checkAndDropSameCopynbSeg(), thus the second is the case:
    -- we have an existing segment at that fseq position. This is forbidden!
    rc := serrno.SEINTERNAL;
    msg := 'A file already exists at fseq '|| inSegEntry.fseq ||' on VID '|| inSegEntry.vid;
    ROLLBACK;
    RETURN;
  END;
  -- Finally check for too many segments for this file
  SELECT nbCopies INTO varFCNbCopies
    FROM Cns_class_metadata
   WHERE classid = varFClassId;
  SELECT count(*) INTO varNb
    FROM Cns_seg_metadata
   WHERE s_fileid = varFid AND s_status = '-';
  IF varNb > varFCNbCopies THEN
    rc := serrno.ENSTOOMANYSEGS;
    msg := serrno.ENSTOOMANYSEGS_MSG ||' : expected '|| to_char(varFCNbCopies) ||', got '
      || to_char(varNb) ||' attempting to write a new segment on VID '|| inSegEntry.vid;
    ROLLBACK;
    RETURN;
  END IF;
  -- Update file status
  UPDATE Cns_file_metadata SET status = 'm' WHERE fileid = varFid;
  -- Commit and log
  SELECT inSegEntry.blockId INTO varBlockId FROM Dual;  -- to_char() of a RAW type does not work. This does the trick...
  varParams := 'copyNb='|| inSegEntry.copyNo ||' SegmentSize='|| inSegEntry.segSize
    ||' Compression='|| CASE inSegEntry.comprSize WHEN 0 THEN 'inf' ELSE trunc(inSegEntry.segSize*100/inSegEntry.comprSize) END
    ||' TPVID='|| inSegEntry.vid ||' fseq='|| inSegEntry.fseq ||' BlockId="' || varBlockId
    ||'" gid=' || varFGid ||' ChecksumType="'|| inSegEntry.checksum_name ||'" ChecksumValue=' || inSegEntry.checksum
    ||' creationTime=' || trunc(varSegCreationTime, 6);
  addSegResult(0, inReqId, 0, 'New segment information', varFid, varParams);
  COMMIT;
EXCEPTION WHEN NO_DATA_FOUND THEN
  -- The file entry was not found, just give up
  rc := serrno.ENOENT;
  msg := serrno.ENOENT_MSG;
END;
/

/**
 * This procedure replaces one (or more) segments for a given file when repacked.
 * This can be a one to one replacement (same copy number) or a move, if the
 * original copy had a different copy number than the new one. In this last case,
 * the new copy may actually replace 2 old copies as the new copy number
 * may correspond to a second copy that will be replaced. Note that this
 * last case is only accepted if the second replaced copy was invalid.
 * Return code:
 * 0  in case of success
 * ENOENT         if the file does not exist (e.g. it has been dropped meanwhile).
 * EISDIR         if the file is a directory.
 * EEXIST         if the file already has a valid segment on the same tape.
 * ENSFILECHG     if the file has been modified meanwhile.
 * SEINTERNAL     if another segment exists on the given tape at the given fseq location.
 * ENSNOSEG       if the to-be-replaced segment was not found.
 * ENSOVERWHENREP if inOldCopyNo refers to a valid segment and the new inSegEntry has a different copyNo.
 */
CREATE OR REPLACE PROCEDURE replaceSegmentForFile(inOldCopyNo IN INTEGER, inSegEntry IN castorns.Segment_Rec,
                                                  inReqId IN VARCHAR2,
                                                  rc OUT INTEGER, msg OUT NOCOPY VARCHAR2) AS
  varFid NUMBER;
  varFmode NUMBER(6);
  varFLastMTime NUMBER;
  varFSize NUMBER;
  varFGid INTEGER;
  varFClassId NUMBER;
  varFCNbCopies NUMBER;
  varFCksumName VARCHAR2(2);
  varFCksum VARCHAR2(32);
  varNb INTEGER;
  varBlockId VARCHAR2(8);
  varOwSeg castorns.Segment_Rec;
  varRepSeg castorns.Segment_Rec;
  varStatus CHAR(1);
  varParams VARCHAR2(2048);
  varLastOpenTimeFromClient NUMBER;
  varSegCreationTime NUMBER;
  -- Trap `ORA-00001: unique constraint violated` errors
  CONSTRAINT_VIOLATED EXCEPTION;
  PRAGMA EXCEPTION_INIT(CONSTRAINT_VIOLATED, -00001);
BEGIN
  rc := 0;
  msg := '';
  -- We truncate stagertime to 5 decimal digits, which is the precision of the lastOpenTime we get from the stager.
  -- Note this is just fitting the mantissa precision of a double, and it is due to the fact
  -- that those numbers go through OCI as double.
  SELECT fileId, filemode, TRUNC(stagertime, 5), fileClass, fileSize, csumType, csumValue, gid
    INTO varFid, varFmode, varFLastMTime, varFClassId, varFSize, varFCksumName, varFCksum, varFGid
    FROM Cns_file_metadata
   WHERE fileId = inSegEntry.fileId FOR UPDATE;
  varLastOpenTimeFromClient := inSegEntry.lastOpenTime;
  -- Is it a directory?
  IF bitand(varFmode, 4*8*8*8*8) > 0 THEN  -- 040000 == S_IFDIR
    rc := serrno.EISDIR;
    msg := serrno.EISDIR_MSG;
    ROLLBACK;
    RETURN;
  END IF;
  -- Has the file been changed meanwhile?
  IF varFLastMTime > varLastOpenTimeFromClient THEN
    rc := serrno.ENSFILECHG;
    msg := serrno.ENSFILECHG_MSG ||' : NSLastOpenTime='|| varFLastMTime
      ||', StagerLastOpenTime='|| varLastOpenTimeFromClient;
    ROLLBACK;
    RETURN;
  END IF;
  -- Cross check file and segment checksums when adler32 (AD in the file entry):
  -- unfortunately we have to play with different representations...
  IF (varFCksumName = 'AD' OR varFCksumName = 'PA') AND inSegEntry.checksum_name = 'adler32' AND
     inSegEntry.checksum != to_number(varFCksum, 'XXXXXXXX') THEN
    rc := serrno.SECHECKSUM;
    msg := serrno.SECHECKSUM_MSG ||' : '
      || varFCksum ||' vs '|| to_char(inSegEntry.checksum, 'XXXXXXXX');
    ROLLBACK;
    RETURN;
  END IF;
  -- Cross check segment (transfered) size and file size
  IF varFSize != inSegEntry.segSize THEN
    rc := serrno.ERTWRONGSIZE;
    msg := serrno.ERTWRONGSIZE_MSG ||' : expected '
      || to_char(varFSize) ||', got '|| to_char(inSegEntry.segSize);
    ROLLBACK;
    RETURN;
  END IF;
  -- Repack specific: --
  -- Make sure the segment for the given oldCopyNo still exists
  BEGIN
    SELECT s_status, creationTime INTO varStatus, varSegCreationTime
      FROM Cns_seg_metadata
     WHERE s_fileid = varFid AND copyNo = inOldCopyNo;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    -- Previous segment not found, give up
    rc := serrno.ENSNOSEG;
    msg := serrno.ENSNOSEG_MSG;
    ROLLBACK;
    RETURN;
  END;
  -- Check if a segment exists with the target copyNo, in case this is different from the
  -- old copyNo: such a segment is about to be overwritten
  IF inSegEntry.copyNo != inOldCopyNo THEN
    BEGIN
      SELECT s_status INTO varStatus
        FROM Cns_seg_metadata
       WHERE s_fileid = varFid AND copyNo = inSegEntry.copyNo;
      IF varStatus = '-' THEN
        -- We are asked to overwrite a valid segment and replace another one.
        -- This is forbidden.
        rc := serrno.ENSOVERWHENREP;
        msg := serrno.ENSOVERWHENREP_MSG;
        ROLLBACK;
        RETURN;
      END IF;
      -- OK, the segment being overwritten is invalid, we can drop it
      DELETE FROM Cns_seg_metadata
       WHERE s_fileid = varFid AND copyNo = inSegEntry.copyNo
      RETURNING copyNo, segSize, compression, vid, fseq, blockId, checksum_name, checksum,
                gid, creationTime, lastModificationTime
           INTO varOwSeg.copyNo, varOwSeg.segSize, varOwSeg.comprSize, varOwSeg.vid,
                varOwSeg.fseq, varOwSeg.blockId, varOwSeg.checksum_name, varOwSeg.checksum,
                varOwSeg.gid, varOwSeg.creationTime, varOwSeg.lastModificationTime;
      -- Log overwritten segment metadata
      SELECT varOwSeg.blockId INTO varBlockId FROM Dual;
      varParams := 'copyNb='|| varOwSeg.copyNo ||' SegmentSize='|| varOwSeg.segSize
        ||' Compression='|| varOwSeg.comprSize ||' TPVID='|| varOwSeg.vid
        ||' fseq='|| varOwSeg.fseq ||' BlockId="' || varBlockId ||'" gid=' || varOwSeg.gid
        ||' ChecksumType="'|| varOwSeg.checksum_name ||'" ChecksumValue=' || to_char(varOwSeg.checksum, 'XXXXXXXX')
        ||' creationTime=' || trunc(varOwSeg.creationTime, 6)
        ||' lastModificationTime=' || trunc(varOwSeg.lastModificationTime, 6);
      addSegResult(1, inReqId, 0, 'Unlinking segment (overwritten)', varFid, varParams);
    EXCEPTION WHEN NO_DATA_FOUND THEN
      -- Nothing found with the target copyNo: this means the existing segment we're repacking
      -- actually had an incorrect copyNo (e.g. it was 2 but no copyNo=1 existed). In such a case
      -- we want to fix the existing segment so that it gets properly replaced.
      -- Note that we won't throw CONSTRAINT_VIOLATED because we know inSegEntry.copyNo
      -- does not exist, and we know the segment currently has copyNo=inOldCopyNo.
      UPDATE Cns_seg_metadata
         SET copyNo = inSegEntry.copyNo
       WHERE s_fileid = varFid
         AND copyNo = inOldCopyNo;
      addSegResult(1, inReqId, 0, 'Updating copy number', varFid, 'oldCopyNb='|| inOldCopyNo
                   ||' newCopyNb='|| inSegEntry.copyNo);
    END;
  END IF;
  -- Prevent to write a second copy of this file on a tape that already holds a valid copy,
  -- whilst allowing the overwrite of a same copyNo segment: this makes the NS operation
  -- fully idempotent and allows to compensate from possible errors in the tapegateway
  -- after a segment had been committed in the namespace.
  rc := checkAndDropSameCopynbSeg(inReqId, varFid, inSegEntry.vid, inSegEntry.copyNo);
  IF rc = serrno.EEXIST THEN
    msg := 'File already has a copy on VID '|| inSegEntry.vid;
    ROLLBACK;
    RETURN;
  END IF;

  -- We're done with the pre-checks, try and insert the segment metadata
  -- and deal with the possible unique (vid,fseq) constraint violation exception
  BEGIN
    INSERT INTO Cns_seg_metadata (s_fileId, copyNo, fsec, segSize, s_status, vid,
      fseq, blockId, compression, side, checksum_name, checksum, gid, creationTime, lastModificationTime)
    VALUES (varFid, inSegEntry.copyNo, 1, inSegEntry.segSize, '-', inSegEntry.vid,
      inSegEntry.fseq, inSegEntry.blockId,
      CASE inSegEntry.comprSize WHEN 0 THEN 100 ELSE trunc(inSegEntry.segSize*100/inSegEntry.comprSize) END,
      0, inSegEntry.checksum_name, inSegEntry.checksum, varFGid, varSegCreationTime, getTime());
  EXCEPTION WHEN CONSTRAINT_VIOLATED THEN
    -- There must already be an existing segment at that fseq position for a different file.
    -- This is forbidden! Abort the entire operation.
    rc := serrno.SEINTERNAL;
    msg := 'A file already exists at fseq '|| inSegEntry.fseq ||' on VID '|| inSegEntry.vid;
    ROLLBACK;
    RETURN;
  END;
  -- Finally check for too many segments for this file: this can only happen if manual operations
  -- have created a previous inconsistency. In such a case we rollback and make repack fail,
  -- without attempting to repair the inconsistency (i.e. drop a segment).
  SELECT nbCopies INTO varFCNbCopies
    FROM Cns_class_metadata
   WHERE classid = varFClassId;
  SELECT count(*) INTO varNb
    FROM Cns_seg_metadata
   WHERE s_fileid = varFid AND s_status = '-';
  IF varNb > varFCNbCopies THEN
    rc := serrno.ENSTOOMANYSEGS;
    msg := serrno.ENSTOOMANYSEGS_MSG ||' : expected '|| to_char(varFCNbCopies) ||', got '
      || to_char(varNb) ||' attempting to write a new segment on VID '|| inSegEntry.vid;
    ROLLBACK;
    RETURN;
  END IF;
  -- All right, commit and log
  SELECT inSegEntry.blockId INTO varBlockId FROM Dual;  -- to_char() of a RAW type does not work. This does the trick...
  varParams := 'copyNb='|| inSegEntry.copyNo ||' SegmentSize='|| inSegEntry.segSize
    ||' Compression='|| CASE inSegEntry.comprSize WHEN 0 THEN 'inf' ELSE trunc(inSegEntry.segSize*100/inSegEntry.comprSize) END
    ||' TPVID='|| inSegEntry.vid
    ||' fseq='|| inSegEntry.fseq ||' blockId="' || varBlockId ||'" gid=' || varFGid
    ||' ChecksumType="'|| inSegEntry.checksum_name ||'" ChecksumValue=' || inSegEntry.checksum
    ||' creationTime=' || trunc(varSegCreationTime, 6) ||' Repack=True';
  addSegResult(0, inReqId, 0, 'New segment information', varFid, varParams);
  COMMIT;
EXCEPTION WHEN NO_DATA_FOUND THEN
  -- The file entry was not found, just give up
  rc := serrno.ENOENT;
  msg := serrno.ENOENT_MSG;
END;
/

/**
 * This procedure sets or replaces segments for multiple files by calling either setSegmentForFile
 * or replaceSegmentForFile in a loop, the choice depending on the oldCopyNo values:
 * when 0, a normal migration is assumed and setSegmentForFile is called.
 * The input is fetched from SetSegsForFilesInputHelper and deleted,
 * the output is stored into SetSegsForFilesResultsHelper. In both cases the
 * inReqId UUID is a unique key identifying one invocation of this procedure.
 */
CREATE OR REPLACE PROCEDURE setOrReplaceSegmentsForFiles(inReqId IN VARCHAR2) AS
  varRC INTEGER;
  varParams VARCHAR2(1000);
  varStartTime TIMESTAMP;
  varSeg castorns.Segment_Rec;
  varCount INTEGER := 0;
  varErrCount INTEGER := 0;
BEGIN
  varStartTime := SYSTIMESTAMP;
  -- Loop over all files. Each call commits or rollbacks each file.
  FOR s IN (SELECT fileId, lastModTime, copyNo, oldCopyNo, transfSize, comprSize,
                   vid, fseq, blockId, checksumType, checksum
              FROM SetSegsForFilesInputHelper
             WHERE reqId = inReqId) LOOP
    varSeg.fileId := s.fileId;
    varSeg.lastOpenTime := s.lastModTime;
    varSeg.copyNo := s.copyNo;
    varSeg.segSize := s.transfSize;
    varSeg.comprSize := s.comprSize;
    varSeg.vid := s.vid;
    varSeg.fseq := s.fseq;
    varSeg.blockId := s.blockId;
    varSeg.checksum_name := s.checksumType;
    varSeg.checksum := s.checksum;
    IF s.oldCopyNo = 0 THEN
      setSegmentForFile(varSeg, inReqId, varRC, varParams);
    ELSE
      replaceSegmentForFile(s.oldCopyNo, varSeg, inReqId, varRC, varParams);
    END IF;
    IF varRC != 0 THEN
      varParams := 'ErrorCode='|| to_char(varRC) ||' ErrorMessage="'|| varParams ||'"';
      addSegResult(0, inReqId, varRC, 'Error creating/replacing segment', s.fileId, varParams);
      varErrCount := varErrCount + 1;
      COMMIT;
    END IF;
    varCount := varCount + 1;
  END LOOP;
  IF varCount > 0 THEN
    -- Final logging
    varParams := 'Function="setOrReplaceSegmentsForFiles" NbFiles='|| varCount
      || ' NbErrors=' || varErrCount
      ||' ElapsedTime='|| getSecs(varStartTime, SYSTIMESTAMP)
      ||' AvgProcessingTime='|| trunc(getSecs(varStartTime, SYSTIMESTAMP)/varCount, 6);
    addSegResult(1, inReqId, 0, 'Bulk processing complete', 0, varParams);
  END IF;
  -- Clean input data
  DELETE FROM SetSegsForFilesInputHelper
   WHERE reqId = inReqId;
  COMMIT;
  -- Return results and logs to the stager. Unfortunately we can't OPEN CURSOR FOR ...
  -- because we would get ORA-24338: 'statement handle not executed' at run time.
  -- Moreover, temporary tables are not supported with distributed transactions,
  -- so the stager will remotely open the SetSegsForFilesResultsHelper table, and
  -- we clean the tables by hand using the reqId key.
EXCEPTION WHEN OTHERS THEN
  -- In case of an uncaught exception, log it and preserve the SetSegsForFilesResultsHelper
  -- content for the stager as other files may have already been committed. Any other
  -- remaining file from the input will have to be migrated again.
  varParams := 'Function="setOrReplaceSegmentsForFiles" errorMessage="' || SQLERRM
        ||'" stackTrace="' || dbms_utility.format_error_backtrace ||'" fileId=' || varSeg.fileId
        || ' copyNo=' || varSeg.copyNo || ' VID=' || varSeg.vid
        || ' fSeq=' || varSeg.fseq;
  addSegResult(0, inReqId, SQLCODE, 'Uncaught Oracle exception', varSeg.fileId, varParams);
  DELETE FROM SetSegsForFilesInputHelper
   WHERE reqId = inReqId;
  COMMIT;
END;
/

/* This procedure implements the Cns_closex API.
 * inCksumType can be either 'AD' or 'NO' for no checksum.
 */
CREATE OR REPLACE PROCEDURE closex(inFid IN INTEGER,
                                   inFileSize IN INTEGER,
                                   inCksumType IN VARCHAR2,
                                   inCksumValue IN VARCHAR2,
                                   inMTime IN INTEGER,
                                   inLastOpenTime IN NUMBER,
                                   outRC OUT INTEGER,
                                   outMsg OUT VARCHAR2) AS
  -- the autonomous transaction is needed because we commit AND we have OUT parameters,
  -- otherwise we would get an ORA-02064 distributed operation not supported error.
  PRAGMA AUTONOMOUS_TRANSACTION;
  varFmode NUMBER(6);
  varFLastOpenTime NUMBER;
  varFCksumName VARCHAR2(2);
  varFCksum VARCHAR2(32);
  varUnused INTEGER;
BEGIN
  outRC := 0;
  outMsg := '';
  -- We truncate to 5 decimal digits, which is the precision of the lastOpenTime we get from the stager.
  -- Note this is just fitting the mantissa precision of a double, and it is due to the fact
  -- that those numbers go through OCI as double.
  SELECT filemode, TRUNC(stagertime, 5), csumType, csumValue
    INTO varFmode, varFLastOpenTime, varFCksumName, varFCksum
    FROM Cns_file_metadata
   WHERE fileId = inFid FOR UPDATE;
  -- Is it a directory?
  IF bitand(varFmode, 4*8*8*8*8) > 0 THEN  -- 040000 == S_IFDIR
    outRC := serrno.EISDIR;
    outMsg := serrno.EISDIR_MSG;
    ROLLBACK;
    RETURN;
  END IF;
  -- Has the file been changed meanwhile?
  IF varFLastOpenTime > inLastOpenTime THEN
    outRC := serrno.ENSFILECHG;
    outMsg := serrno.ENSFILECHG_MSG ||' : NSLastOpenTime='|| varFLastOpenTime
      ||', StagerLastOpenTime='|| inLastOpenTime;
    ROLLBACK;
    RETURN;
  END IF;
  -- Validate checksum type
  IF inCksumType != 'AD' AND inCksumType != 'NO' THEN
    outRC := serrno.EINVAL;
    outMsg := serrno.EINVAL_MSG ||' : incorrect checksum type detected '|| inCksumType;
    ROLLBACK;
    RETURN;
  END IF;
  -- Validate checksum value
  BEGIN
    SELECT to_number(inCksumValue, 'XXXXXXXX') INTO varUnused FROM Dual;
  EXCEPTION WHEN INVALID_NUMBER THEN
    outRC := serrno.EINVAL;
    outMsg := serrno.EINVAL_MSG ||' : incorrect checksum value detected '|| inCksumValue;
    ROLLBACK;
    RETURN;
  END;
  -- Cross check file checksums when preset-adler32 (PA in the file entry):
  IF varFCksumName = 'PA' AND
     to_number(inCksumValue, 'XXXXXXXX') != to_number(varFCksum, 'XXXXXXXX') THEN
    outRC := serrno.SECHECKSUM;
    outMsg := 'Predefined file checksum mismatch : preset='|| varFCksum ||', actual='|| inCksumValue;
    ROLLBACK;
    RETURN;
  END IF;
  -- All right, update file size and other metadata
  UPDATE Cns_file_metadata
     SET fileSize = inFileSize,
         csumType = CASE WHEN inCksumType != 'NO' THEN inCksumType ELSE NULL END,
         csumValue = CASE WHEN inCksumType != 'NO' THEN inCksumValue ELSE NULL END,
         ctime = inMTime,
         mtime = inMTime
   WHERE fileId = inFid;
  outMsg := 'Function="closex" FileSize=' || inFileSize
    ||' ChecksumType="'|| inCksumType ||'" ChecksumValue="'|| inCksumValue
    ||'" NewModTime=' || inMTime ||' StagerLastOpenTime='|| inLastOpenTime ||' RtnCode=0';
  COMMIT;
EXCEPTION WHEN NO_DATA_FOUND THEN
  -- The file entry was not found, just give up
  outRC := serrno.ENOENT;
  outMsg := serrno.ENOENT_MSG;
  ROLLBACK;  -- this is a no-op, but it's needed to close the autonomous transaction and avoid ORA-06519 errors
END;
/

/* Helper function to recover a deleted file from CASTOR. It is assumed that the tape segment is still reachable */
CREATE OR REPLACE PROCEDURE undeleteFile(inFileId IN INTEGER, inParent IN INTEGER, inGuid IN CHAR, inName IN VARCHAR2,
                                         inFileMode IN INTEGER, inNLink IN INTEGER, inOwnerUid IN INTEGER, inGid IN INTEGER,
                                         inFileSize IN INTEGER, inATime IN NUMBER, inCTime IN NUMBER, inFileClass IN INTEGER,
                                         inCksumType IN VARCHAR2, inCksumValue IN VARCHAR2,
                                         inCopyNo IN INTEGER, inVid IN VARCHAR2, inFseq IN INTEGER, inBlockId IN VARCHAR2,
                                         inCompression IN INTEGER, inOverwrite IN INTEGER) AS
  CONSTRAINT_VIOLATED EXCEPTION;
  PRAGMA EXCEPTION_INIT(CONSTRAINT_VIOLATED, -1);
  varRecreationTime NUMBER := getTime();
  varParams VARCHAR2(2048);
  varAcl VARCHAR2(3900);
  varGid INTEGER;
BEGIN
  -- Attempt to reinsert the file and the segment metadata
  BEGIN
    UPDATE Cns_file_metadata SET nlink = nlink + 1
     WHERE fileId = inParent
    RETURNING acl INTO varAcl;   -- inherit parent's ACLs
    INSERT INTO Cns_file_metadata (fileid, parent_fileid, guid, name, fileMode, nLink, owner_uid, gid, fileSize,
      atime, mtime, ctime, stagertime, fileclass, status, csumtype, csumvalue, acl)
    VALUES (inFileId, inParent, inGuid, inName, inFileMode, inNLink, inOwnerUid, inGid, inFileSize,
      inATime, varRecreationTime, inCTime, varRecreationTime, inFileClass, 'm', inCksumType, inCksumValue, varAcl);
    varGid := inGid;
  EXCEPTION
  WHEN CONSTRAINT_VIOLATED THEN
    -- The file actually exists: overwrite only if explicitly requested
    IF inOverwrite = 1 THEN
      UPDATE Cns_file_metadata
        SET fileSize = inFileSize,
            status = 'm',
            atime = varRecreationTime,
            mtime = varRecreationTime
      WHERE fileId = inFileId
      RETURNING gid INTO varGid;
    ELSE
      raise_application_error(-20000, 'A file already exists with fileid '|| inFileId);
    END IF;
  WHEN NO_DATA_FOUND THEN
    raise_application_error(-20000, 'Parent fileid '|| inParent ||' does not exist any longer');
  END;
  BEGIN
    INSERT INTO Cns_seg_metadata (s_fileId, copyNo, fsec, segSize, s_status, vid, fseq, blockId,
      compression, side, checksum_name, checksum, gid, creationTime, lastModificationTime)
    VALUES (inFileId, inCopyNo, 1, inFileSize, '-', inVid, inFseq, HEXTORAW(inBlockId), inCompression, 0,
      CASE inCksumType WHEN 'AD' THEN 'adler32' ELSE inCksumType END, to_number(inCksumValue, 'XXXXXXXX'),
      varGid, varRecreationTime, varRecreationTime);
  EXCEPTION WHEN CONSTRAINT_VIOLATED THEN
    raise_application_error(-20000, 'A file already exists at fseq '|| inFseq ||' on VID '|| inVid);
  END;
  -- Commit and log
  varParams := 'copyNb='|| inCopyNo ||' FileSize='|| inFileSize ||' TPVID='|| inVid
    ||' gid=' || inGid ||' ChecksumType="'|| inCksumType ||'" ChecksumValue=' || inCksumValue
    ||' creationTime=' || trunc(varRecreationTime, 6);
  addSegResult(1, NULL, 0, 'undeleteFile: file and segment information restored', inFileId, varParams);
  COMMIT;
END;
/


/*** Namespace statistics gathering ***/

CREATE OR REPLACE PROCEDURE insertNSStats(inGid IN INTEGER, inTimestamp IN NUMBER,
                                          inMaxFileId IN INTEGER, inFileCount IN INTEGER, inFileSize IN INTEGER,
                                          inSegCount IN INTEGER, inSegSize IN INTEGER, inSegCompressedSize IN INTEGER,
                                          inSeg2Count IN INTEGER, inSeg2Size IN INTEGER, inSeg2CompressedSize IN INTEGER) AS
  CONSTRAINT_VIOLATED EXCEPTION;
  PRAGMA EXCEPTION_INIT(CONSTRAINT_VIOLATED, -1);
BEGIN
  INSERT INTO UsageStats (gid, timestamp, maxFileId, fileCount, fileSize, segCount, segSize,
                          segCompressedSize, seg2Count, seg2Size, seg2CompressedSize)
    VALUES (inGid, inTimestamp, inMaxFileId, inFileCount, inFileSize, inSegCount, inSegSize,
            inSegCompressedSize, inSeg2Count, inSeg2Size, inSeg2CompressedSize);
EXCEPTION WHEN CONSTRAINT_VIOLATED THEN
  UPDATE UsageStats SET
    maxFileId = CASE WHEN inMaxFileId > maxFileId THEN inMaxFileId ELSE maxFileId END,
    fileCount = fileCount + inFileCount,
    fileSize = fileSize + inFileSize,
    segCount = segCount + inSegCount,
    segSize = segSize + inSegSize,
    segCompressedSize = segCompressedSize + inSegCompressedSize,
    seg2Count = seg2Count + inSeg2Count,
    seg2Size = seg2Size + inSeg2Size,
    seg2CompressedSize = seg2CompressedSize + inSeg2CompressedSize
  WHERE gid = inGid AND timestamp = inTimestamp;
END;
/

/* This procedure is run as a database job to generate statistics from the namespace */
CREATE OR REPLACE PROCEDURE gatherNSStats AS
  varTimestamp NUMBER := trunc(getTime());
BEGIN
  -- File-level statistics
  FOR g IN (SELECT gid, MAX(fileid) maxId, COUNT(*) fileCount, SUM(filesize) fileSize
              FROM Cns_file_metadata
             WHERE stagerTime < varTimestamp
               AND onCta IS NULL
             GROUP BY gid) LOOP
    insertNSStats(g.gid, varTimestamp, g.maxId, g.fileCount, g.fileSize, 0, 0, 0, 0, 0, 0);
  END LOOP;
  COMMIT;
  -- Tape-level statistics
  FOR g IN (SELECT gid, copyNo, SUM(segSize * 100 / decode(compression,0,100,compression)) segComprSize,
                   SUM(segSize) segSize, COUNT(*) segCount
              FROM Cns_seg_metadata
             WHERE creationTime < varTimestamp
               AND onCta IS NULL
             GROUP BY gid, copyNo) LOOP
    IF g.copyNo = 1 THEN
      insertNSStats(g.gid, varTimestamp, 0, 0, 0, g.segCount, g.segSize, g.segComprSize, 0, 0, 0);
    ELSE
      insertNSStats(g.gid, varTimestamp, 0, 0, 0, 0, 0, 0, g.segCount, g.segSize, g.segComprSize);
    END IF;
  END LOOP;
  COMMIT;
  -- Also compute totals
  INSERT INTO UsageStats (gid, timestamp, maxFileId, fileCount, fileSize, segCount, segSize,
                          segCompressedSize, seg2Count, seg2Size, seg2CompressedSize)
    (SELECT -1, varTimestamp, MAX(maxFileId), SUM(fileCount), SUM(fileSize),
            SUM(segCount), SUM(segSize), SUM(segCompressedSize),
            SUM(seg2Count), SUM(seg2Size), SUM(seg2CompressedSize)
       FROM UsageStats
      WHERE timestamp = varTimestamp);
  COMMIT;
END;
/

/* This pipelined function returns a report of namespace statistics over the given number of days */
CREATE OR REPLACE FUNCTION NSStatsReport(inDays INTEGER) RETURN castorns.Stats PIPELINED IS
BEGIN
  FOR l IN (
        SELECT NewStats.timestamp timeStamp, NewStats.timestamp-OldStats.timeStamp timeInterval, NewStats.gid,
               NewStats.maxFileId, NewStats.fileCount, NewStats.fileSize,
               NewStats.segCount, NewStats.segSize, NewStats.segCompressedSize,
               NewStats.seg2Count, NewStats.seg2Size, NewStats.seg2CompressedSize,
               NewStats.maxFileId-OldStats.maxFileId fileIdDelta, NewStats.fileCount-OldStats.fileCount fileCountDelta,
               NewStats.fileSize-OldStats.fileSize fileSizeDelta,
               NewStats.segCount-OldStats.segCount segCountDelta, NewStats.segSize-OldStats.segSize segSizeDelta,
               NewStats.segCompressedSize-OldStats.segCompressedSize segCompressedSizeDelta,
               NewStats.seg2Count-OldStats.seg2Count seg2CountDelta, NewStats.seg2Size-OldStats.seg2Size seg2SizeDelta,
               NewStats.seg2CompressedSize-OldStats.seg2CompressedSize seg2CompressedSizeDelta
          FROM
            (SELECT gid, timestamp, maxFileId, fileCount, fileSize, segCount, segSize, segCompressedSize,
                    seg2Count, seg2Size, seg2CompressedSize
               FROM UsageStats
              WHERE timestamp = (SELECT MAX(timestamp) FROM UsageStats
                                  WHERE timestamp < getTime() - 86400*inDays)) OldStats,
            (SELECT gid, timestamp, maxFileId, fileCount, fileSize, segCount, segSize, segCompressedSize,
                    seg2Count, seg2Size, seg2CompressedSize
               FROM UsageStats
              WHERE timestamp = (SELECT MAX(timestamp) FROM UsageStats)) NewStats
         WHERE OldStats.gid = NewStats.gid
         ORDER BY NewStats.gid DESC) LOOP    -- gid = -1, i.e. the totals line, comes last
    PIPE ROW(l);
  END LOOP;
END;
/

/* A convenience view to get weekly statistics */
CREATE OR REPLACE VIEW WeeklyReportView AS
  SELECT * FROM TABLE(NSStatsReport(7));


/* useful procedure to recompile all invalid items in the DB
   as many times as needed, until nothing can be improved anymore.
   Also reports the list of invalid items if any */
CREATE OR REPLACE PROCEDURE recompileAll AS
  varNbInvalids INTEGER;
  varNbInvalidsLastRun INTEGER := -1;
BEGIN
  WHILE varNbInvalidsLastRun != 0 LOOP
    varNbInvalids := 0;
    FOR a IN (SELECT object_name, object_type
                FROM user_objects
               WHERE object_type IN ('PROCEDURE', 'TRIGGER', 'FUNCTION', 'VIEW', 'PACKAGE BODY')
                 AND status = 'INVALID')
    LOOP
      IF a.object_type = 'PACKAGE BODY' THEN a.object_type := 'PACKAGE'; END IF;
      BEGIN
        EXECUTE IMMEDIATE 'ALTER ' ||a.object_type||' '||a.object_name||' COMPILE';
      EXCEPTION WHEN OTHERS THEN
        -- ignore, so that we continue compiling the other invalid items
        NULL;
      END;
    END LOOP;
    -- check how many invalids are still around
    SELECT count(*) INTO varNbInvalids FROM user_objects
     WHERE object_type IN ('PROCEDURE', 'TRIGGER', 'FUNCTION', 'VIEW', 'PACKAGE BODY') AND status = 'INVALID';
    -- should we give up ?
    IF varNbInvalids = varNbInvalidsLastRun THEN
      DECLARE
        varInvalidItems VARCHAR(2048);
      BEGIN
        -- yes, as we did not move forward on this run
        SELECT LISTAGG(object_name, ', ') WITHIN GROUP (ORDER BY object_name) INTO varInvalidItems
          FROM user_objects
         WHERE object_type IN ('PROCEDURE', 'TRIGGER', 'FUNCTION', 'VIEW', 'PACKAGE BODY') AND status = 'INVALID';
        raise_application_error(-20000, 'Revalidation of PL/SQL code failed. Still ' ||
                                        varNbInvalids || ' invalid items : ' || varInvalidItems);
      END;
    END IF;
    -- prepare for next loop
    varNbInvalidsLastRun := varNbInvalids;
    varNbInvalids := 0;
  END LOOP;
END;
/


/*
 * Database jobs
 */
BEGIN
  -- Remove database jobs before recreating them
  FOR j IN (SELECT job_name FROM user_scheduler_jobs
             WHERE job_name = 'NSSTATSJOB')
  LOOP
    DBMS_SCHEDULER.DROP_JOB(j.job_name, TRUE);
  END LOOP;

  -- Create a db job to be run every day executing the gatherNSStats procedure
  DBMS_SCHEDULER.CREATE_JOB(
      JOB_NAME        => 'NSStatsJob',
      JOB_TYPE        => 'PLSQL_BLOCK',
      JOB_ACTION      => 'BEGIN gatherNSStats(); END;',
      JOB_CLASS       => 'CASTOR_JOB_CLASS',
      START_DATE      => SYSDATE + 60/1440,
      REPEAT_INTERVAL => 'FREQ=DAILY; INTERVAL=1',
      ENABLED         => TRUE,
      COMMENTS        => 'Gathering of Nameserver usage statistics');
END;
/
