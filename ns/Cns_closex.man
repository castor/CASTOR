.\" Copyright (C) 1999-2005 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH CNS_CLOSEX "3castor" "$Date: 2009/06/30 12:54:06 $" CASTOR "Cns Library Functions"
.SH NAME
Cns_closex \- close a file, setting its size and checksum attributes
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "Cns_api.h"\fR
.sp
.BI "int Cns_closex (const struct Cns_fileid * " file_uniqueid,
.BI "const u_signed64 " filesize,
.BI "const char * " csumtype,
.BI "const char * " csumvalue,
.BI "const time_t " new_mod_time,
.BI "const u_signed64 " last_stagertime_usec,
.BI "struct Cns_filestatcs * " statbuf)
.SH DESCRIPTION
.B ---- FOR INTERNAL USE ONLY! ----
.LP
.B Cns_closex
given a
.IR file_uniqueid
structure containing the name server hostname and file id sets the
.IR filesize
and file checksum information (
.IR csumtype
and
.IR csumvalue
). If the
.I filesize
is 0, any segments associated to the file will be deleted and the file's
migration flag
.I 'm'
cleared.
.LP
.I new_mod_time
specifies the new modification time of the file.
.I last_stagertime_usec
specifies the last open time as known by the requester stager. This is used to
verify that no concurrent modifications have occurred to the file while being
modified.
Note that this value is given as an integer representing the number of microsecond since the Epoch.
.LP
The structure pointed to by
.IR statbuf
contains the following members:
.LP
.RS
u_signed64  fileid;      /* entry unique identifier */
.br
mode_t      filemode;    /* see below */
.br
int         nlink;       /* number of files in a directory */
.br
uid_t       uid;
.br
gid_t       gid;
.br
u_signed64  filesize;
.br
time_t      atime;       /* last access to file */
.br
time_t      mtime;       /* last file modification */
.br
time_t      ctime;       /* last metadata modification */
.br
short       fileclass;
.br
char        status;      /* '-' --> online, 'm' --> migrated */
.br
char        csumtype[3];
.br
char        csumvalue[CA_MAXCKSUMLEN + 1];
.RE
.LP
Execution of this command requires the requester to have ADMIN privileges.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOENT
The file id specified in the
.I file_uniqueid
structure does not exist.
.TP
.B ENSFILECHG
Concurrent modification detected on the file, a newer version exists elsewhere.
.TP
.B EISDIR
The file is not a regular file.
.TP
.B EFAULT
.I file_uniqueid
is a NULL pointer.
.TP
.B EINVAL
The
.I csumtype
or
.I csumvalue
are invalid. Valid values for
.I csumtype
are
.B AD
(adler32) and
.B PA
(Pre-adler32).
.TP
.B EROFS
The name server is in read only mode.
.TP
.B SECHECKSUM
Bad checksum detected.
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.TP
.B ENSNACT
Name server is not running or is being shutdown.
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
