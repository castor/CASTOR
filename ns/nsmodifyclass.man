.\" Copyright (C) 2000-2002 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH NSMODIFYCLASS "1castor" "$Date: 2008/11/03 10:37:05 $" CASTOR "Cns Administrator Commands"
.SH NAME
nsmodifyclass \- modify an existing file class
.SH SYNOPSIS
.B nsmodifyclass
<\f--id=CLASSID|--name=CLASSNAME\fR> [\fIOPTION\fR]...
.SH DESCRIPTION
.B nsmodifyclass
modifies an existing file class.
The fileclass can be identified by CLASSID or by CLASSNAME.
If both are specified, they must point at the same class.
.LP
This command requires ADMIN privileges in the Cupv database.
.SH OPTIONS
.TP
.BI -h,\ \-\-host=HOSTNAME
specify the name server hostname. This can also be set through the
CNS_HOST environment variable.
.TP
.BI --flags=FLAGS
.TP
.BI --id=CLASSID
The class number of the class to be modified.
.TP
.BI --name=CLASSNAME
The name of the class to be modified.
.TP
.BI --nbcopies=NUM
Specifies the number of copies for a file. Each copy is written to a different
tape pool.
.TP
.BI --newname=CLASSNAME
The class name must be at most CA_MAXCLASNAMELEN characters long.
.TP
.B \-\-help
Display this help and exit
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR Cns_modifyclass(3) ,
.B Cupvlist(1)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
