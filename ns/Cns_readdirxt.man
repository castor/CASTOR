.\" Copyright (C) 2000 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH CNS_READDIRXT "3castor" "$Date: 2006/01/26 15:36:20 $" CASTOR "Cns Library Functions"
.SH NAME
Cns_readdirxt \- read CASTOR directory opened by
.B Cns_opendir
in the name server
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "Cns_api.h"\fR
.sp
.BI "struct Cns_direntape *Cns_readdirxt (Cns_DIR *" dirp )
.SH DESCRIPTION
.B Cns_readdirxt
reads the CASTOR directory opened by
.B Cns_opendir
in the name server.
This routine returns a pointer to a structure containing the current directory
entry name and the tape segment information.
.B Cns_readdirxt
caches a variable number of such entries, depending on the filename size, to
minimize the number of requests to the name server.
.TP
.I dirp
specifies the pointer value returned by
.BR Cns_opendir .
.SH RETURN VALUE
This routine returns a pointer to a structure containing the current directory
entry if the operation was successful or NULL if the end of the directory was
reached or if the operation failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B EBADF
File descriptor in DIR structure is invalid.
.TP
.B EFAULT
.I dirp
is a NULL pointer.
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.TP
.B ENSNACT
Name server is not running or is being shutdown.
.SH SEE ALSO
.BR Cns_closedir(3) ,
.BR Cns_getsegattrs(3) ,
.BR Cns_opendir(3) ,
.B Cns_rewinddir(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
