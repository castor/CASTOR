.\" Copyright (C) 2004 by CERN/IT/GD/CT
.\" All rights reserved
.\"
.TH CNS_ABORTTRANS "3castor" "$Date: 2006/01/26 15:36:16 $" CASTOR "Cns Library Functions"
.SH NAME
Cns_aborttrans \- abort a transaction
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "Cns_api.h"\fR
.sp
.B int Cns_aborttrans (void)
.SH DESCRIPTION
.B Cns_aborttrans
aborts a transaction.
A ROLLBACK is executed for all updates done since
.BR Cns_starttrans .
The connection to the Name Server is closed.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.TP
.B ENSNACT
Name server is not running or is being shutdown.
.SH SEE ALSO
.BR Cns_endtrans(3) ,
.BR Cns_starttrans(3)
