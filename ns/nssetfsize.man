.\" Copyright (C) 2003 by CERN/IT/DM
.\" All rights reserved
.\"
.TH NSSETFSIZE "1castor" "$Date: 2009/03/26 15:00:27 $" CASTOR "Cns User Commands"
.SH NAME
nssetfsize \- update the size of a file
.SH SYNOPSIS
.B nssetfsize
[\fIOPTION\fR]... \fIPATH\fR...
.SH DESCRIPTION
.B nssetfsize
is a command line utility that allows users with ADMIN privileges to update the size of a file in the CASTOR name server.
.TP
.I PATH
specifies the CASTOR pathname or directory.
If PATH does not start with
.BR /
it is prefixed by the content of the
.B CASTOR_HOME
environment variable.
.SH OPTIONS
The following options are supported:
.TP
.B -x,\ \-\-size=SIZE
The size of the file in bytes.
.TP
.B -r,\ \-\-reset
Reset the file size to 0.
.TP
.B \-\-help
display this help and exit
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
