.\" Copyright (C) 2003 by CERN/IT/DM
.\" All rights reserved
.\"
.TH NSGETPATH "1castor" "$Date: 2009/06/19 09:28:58 $" CASTOR "Cns User Commands"
.SH NAME
nsgetpath \- get file path from fileid
.SH SYNOPSIS
.B nsgetpath
[\fROPTION\fR]
[\f--h HOSTNAME\fR] [FILEID|-x HEXID\fR]
.SH DESCRIPTION
.B nsgetpath
gets the name of the castorfile given by its fileid and the name
server where it resides. If the residing name server is not specified the
application will use the CNS_HOST option as defined in the users environment
or as specified in castor.conf.
.SH OPTIONS
.TP
.B -x,\ \-\-hex=HEXID
Fileid expressed in hexadecimal form.
.TP
.BI -h,\ \-\-host=HOSTNAME
specify the name server hostname.
.TP
.B \-\-help
display this help and exit
.SH EXAMPLES
.BI #\ nsgetpath\ cnsuser\ 183591842
.fi
/castor/cern.ch/c3/itdc/notape/47eb6a9a-7cd5-439f-b156-0a953098b1ca

.BI #\ nsgetpath\ -h\ cnsuser\ -x\ AF163A2
.fi
/castor/cern.ch/c3/itdc/notape/47eb6a9a-7cd5-439f-b156-0a953098b1ca

.BI #\ nsgetpath\ -x\ AF163A2
.fi
/castor/cern.ch/c3/itdc/notape/47eb6a9a-7cd5-439f-b156-0a953098b1ca


.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation failed.
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>

