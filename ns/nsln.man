.\" Copyright (C) 2004 by CERN/IT/GD/CT
.\" All rights reserved
.\"
.TH NSLN "1castor" "$Date: 2008/11/03 10:37:05 $" CASTOR "Cns User Commands"
.SH NAME
nsln \- make a symbolic link to a file or a directory in the CASTOR Name Server
.SH SYNOPSIS
.B nsln -s
\fITARGET\fR [\fILINK_NAME\fR]

.B nsln -s
\fITARGET\fR... \fIDIRECTORY\fR
.SH DESCRIPTION
.B nsln
makes a symbolic link to a file or a directory in the CASTOR Name Server.
.LP
This requires write permission in LINK_NAME parent directory. If LINK_NAME does not start with
.BR / ,
it is prefixed by the content of the
.B CASTOR_HOME
environment variable. If LINK_NAME is not specified, the link is created in the current directory with the same basename as the target. In the second form, it creates in DIRECTORY a link for each target.
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR Cns_symlink(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
