.\" Copyright (C) 1999-2002 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH NSRM "1castor" "$Date: 2009/06/24 09:03:38 $" CASTOR "Cns User Commands"
.SH NAME
nsrm \- remove CASTOR files or directories in the name server
.SH SYNOPSIS
.B nsrm
[\fIOPTION\fR]... \fIFILE\fR...
.SH DESCRIPTION
.B nsrm
removes CASTOR files or directories in the name server.
For directories either
.B -r
or
.B -R
must be present.
For regular files, it calls
.B Cns_unlink
while for directories, it calls
.BR Cns_rmdir .
.LP
This requires write permission in the parent directory and the file itself.
If write permission on an entry is denied, the standard input is a terminal and
the
.B -f
option is not given, the prompt "override write protection" appears and if the
response is not
.BR y ,
the entry is not deleted.
Entries directly under a protected directory are never deleted.
.TP
.I FILE
specifies the CASTOR pathname.
If FILE does not start with
.BR / ,
it is prefixed by the content of the
.B CASTOR_HOME
environment variable.
.SH OPTIONS
The following options are supported:
.TP
.B -f,\ \-\-force
ignore nonexistent files or directories, never prompt
.TP
.B -i,\ \-\-interactive
prompt before any removal
.TP
.B -r, -R,\ \-\-recursive
remove the contents of directories recursively
.TP
.B \-\-help
display this help and exit
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR Cns_rmdir(3) ,
.B Cns_unlink(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
