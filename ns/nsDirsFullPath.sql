/*****************************************************************************
 *                             nsDirsFullPath.sql
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This script creates and populates a table with all full name paths
 * from a Castor Nameserver schema
 * Cf. also the scripts in castor/db/NSanalysis
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

-- Table to hold directory info
CREATE TABLE Dirs_Full_Path (fileid NUMBER NOT NULL,
                             parent NUMBER NOT NULL,
                             name VARCHAR2 (255),
                             path VARCHAR2 (2048),
                             depth INTEGER);

ALTER TABLE Dirs_Full_Path PARALLEL;

-- create some useful indexes
CREATE UNIQUE INDEX I_dirs_fileid ON Dirs_Full_Path (fileid);
CREATE INDEX I_dirs_parent ON Dirs_Full_Path (parent);
CREATE INDEX I_dirs_depth ON Dirs_Full_Path (depth, fileid, parent);


-- Enter all directories into new dirs table (~30min -> 2min in parallel)
INSERT /*+ APPEND */ INTO Dirs_Full_Path
 (SELECT fileid, parent_fileid, name, '', -1
    FROM Cns_file_metadata WHERE BITAND(filemode,16384) = 16384);
COMMIT;

-- compute hierarchy (~2h40min -> ~1h30min in parallel)
UPDATE Dirs_Full_Path SET depth = 0, path = '/' WHERE parent = 0;
DECLARE
  maxdepth NUMBER;
  d NUMBER;
  n NUMBER := 0;
BEGIN
  LOOP
    UPDATE Dirs_Full_Path SET depth = d+1 WHERE parent IN
      (SELECT fileid FROM Dirs_Full_Path WHERE depth = d);
    EXIT WHEN SQL%ROWCOUNT = 0;
    COMMIT;
    d := d + 1;
  END LOOP;
  COMMIT;
  -- first level independently since parent in the root
  UPDATE Dirs_Full_Path SET path = '/'||name WHERE depth = 1;
  SELECT MAX(depth) INTO maxdepth FROM Dirs_Full_Path;
  -- start at top level and go down the hierarchy
  FOR d IN 1..maxdepth LOOP
    FOR parent IN (SELECT fileid, path FROM Dirs_Full_Path WHERE depth = d) LOOP
      UPDATE Dirs_Full_Path SET path = parent.path||'/'||name WHERE parent = parent.fileid;
      n := n + 1;
      IF n = 2000 THEN
        COMMIT;
        n := 0;
      END IF;
    END LOOP;
  END LOOP;
  COMMIT;
END;
/
