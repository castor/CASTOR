.\" Copyright (C) 1999-2002 by CERN/IT/DM
.\" All rights reserved
.\"
.TH NSRMDIR "1castor" "$Date: 2008/11/03 10:10:24 $" CASTOR "Cns User Commands"
.SH NAME
nsrmdir \- remove empty directories in the name server
.SH SYNOPSIS
.B nsrmdir
[\fIOPTION\fR]... \fIDIRECTORY\fR...
.SH DESCRIPTION
.B nsrmdir
removes directories from the CASTOR name server, if they are empty.
.TP
.I DIRECTORY
specifies the CASTOR directory.
If DIRECTORY does not start with
.BR / ,
it is prefixed by the content of the
.B CASTOR_HOME
environment variable.
.SH OPTIONS
 The following options are supported:
.TP
.B -v,\ \-\-verbose
output a diagnostic for every directory processed
.TP
.B -i,\ \-\-ignore-fail-on-non-empty
ignore each failure that is solely because a directory is non-empty
.TP
.B -p,\ \-\-parents
remove each component of the path name pointing to the directory in reverse order. E.g. nsrmdir -p a/b/c is similar to rmdir a/b/c a/b a
.TP
.B \-\-help
display this help and exit
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR Cns_rmdir(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
Q
