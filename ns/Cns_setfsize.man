.\" Copyright (C) 1999-2000 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH CNS_SETFSIZE "3castor" "$Date: 2009/03/26 15:31:48 $" CASTOR "Cns Library Functions"
.SH NAME
Cns_setfsize \- set filesize for a regular file; set also last modification time to the current time
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "Cns_api.h"\fR
.sp
.BI "int Cns_setfsize (const char *" path ,
.BI "struct Cns_fileid *" file_uniqueid ,
.BI "u_signed64 " filesize,
.BI "time_t " new_mod_time,
.BI "time_t " last_mod_time )
.SH DESCRIPTION
.B Cns_setfsize
sets the filesize for a regular file; set also the last modification time to the
current time. Takes care that the file was not modified by somebody else in the mean time.
If it was, ignores the request and fail with ENSFILECHG.
This function should only be called by the stager after the last write
operation has been performed on the file. The caller must have ADMIN privileges in the
Cupv database to execute this call.
The file can be identified by
.I path
name or by
.IR file_uniqueid .
If both are specified,
.I file_uniqueid
is used.
.TP
.I path
specifies the logical pathname relative to the current CASTOR directory or
the full CASTOR pathname.
.TP
.I new_mod_time
specifies the new modification time of the file.
.TP
.I last_mod_time
specifies the last modification time of the file, as known by the caller. This is used to double check that the file was not modified concurrently by somebody else.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOENT
A component of
.I path
prefix does not exist or
.I path
is a null pathname.
.TP
.B EACCES
Search permission is denied on a component of the
.I path
prefix or the caller effective user ID does not match the owner ID of the file
or write permission on the file itself is denied or the caller does not have
ADMIN privileges in the Cupv database.
.TP
.B ENSFILECHG
The file has been changed/overwritten by somebody else since the last_mod_time given. Request was thus ignored.
.TP
.B EFAULT
.I path
and
.I file_uniqueid
are NULL pointers.
.TP
.B ENOTDIR
A component of
.I path
prefix is not a directory.
.TP
.B EISDIR
The file is not a regular file.
.TP
.B ENAMETOOLONG
The length of
.I path
exceeds
.B CA_MAXPATHLEN
or the length of a
.I path
component exceeds
.BR CA_MAXNAMELEN .
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.TP
.B ENSNACT
Name server is not running or is being shutdown.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR Cns_chdir(3) ,
.BR Cns_stat(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
