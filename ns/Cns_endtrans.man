.\" Copyright (C) 2004 by CERN/IT/GD/CT
.\" All rights reserved
.\"
.TH CNS_ENDTRANS "3castor" "$Date: 2006/01/26 15:36:17 $" CASTOR "Cns Library Functions"
.SH NAME
Cns_endtrans \- end transaction mode
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "Cns_api.h"\fR
.sp
.B int Cns_endtrans (void)
.SH DESCRIPTION
.B Cns_endtrans
ends transaction mode.
If all the DB update operations between
.B Cns_starttrans
and
.B Cns_endtrans
are successful, a COMMIT is done. The connection to the Name Server is closed.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.TP
.B ENSNACT
Name server is not running or is being shutdown.
.SH SEE ALSO
.BR Cns_aborttrans(3) ,
.BR Cns_starttrans(3)
