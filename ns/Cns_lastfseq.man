.\" Copyright (C) 2003 by CERN
.\" All rights reserved
.\"
.TH CNS_LASTFSEQ "3castor" "$Date: 2008/02/26 18:20:59 $" CASTOR "Cns Library Functions"
.SH NAME
Cns_lastfseq \- get the file segment attributes for the last file written to a volume
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "Cns_api.h"\fR
.sp
.BI "int *Cns_lastfseq (char *" vid ,
.BI "int " side ,
.BI "struct Cns_seqattrs *" segment )
.SH DESCRIPTION
.B Cns_lastfseq
returns the file segment attributes for the last file written to a volume.
.TP
.I vid
is the visual identifier of the volume.
.TP
.I side
the media side number (Only useful for multi-sided media).
.TP
.I segment
is a pointer to a Cns_segattrs structure to be populated by the API.
.RE
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the
operation failed. In the latter case, 
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOENT
The referenced volume does not exist anymore.
.TP
.B ENOMEM
Memory could not be allocated for the output buffer.
.TP
.B EFAULT
.I vid 
is a null pointer.
.TP
.B EINVAL
The length of
.I vid
exceeds
.BR CA_MAXVIDLEN .
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.TP
.B ENSNACT
Name server is not running or is being shutdown.
.SH SEE ALSO
.BR Castor_limits(4)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
