.\" Copyright (C) 2000-2001 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH NSLISTCLASS "1castor" "$Date: 2008/11/03 10:37:05 $" CASTOR "Cns User Commands"
.SH NAME
nslistclass \- query the CASTOR Name Server about a given class or list all existing classes
.SH SYNOPSIS
.B nslistclass
<\f--id=CLASSID|--name=CLASSNAME\fR> [\fIOPTION\fR]...
.SH DESCRIPTION
.B nslistclass
queries the CASTOR Name Server about a given class or lists all existing classes:
The fileclass can be identified by CLASSID or by CLASSNAME.
If both are specified, they must point at the same class.
.SH OPTIONS
.TP
.BI --id=CLASSID
The class number of the class to be displayed.
.TP
.BI --name=CLASSNAME
The name of the class to be displayed.
.TP
.BI --nohdr
Display each class on one line, removing key value pair listing.
.TP
.BI -h,\ \-\-host=HOSTNAME
specify the name server hostname. This can also be set through the
CNS_HOST environment variable.
.TP
.B \-\-help
display this help and exit
.SH EXAMPLES
.nf
.ft CW
nslistclass --id 2

CLASS_ID        2
CLASS_NAME      user
NBCOPIES        1


nslistclass --id 2 -nohdr

2 user 1
.ft
.fi
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR Cns_listclass(3) ,
.B Cns_queryclass(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
