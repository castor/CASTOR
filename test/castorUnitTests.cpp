/******************************************************************************
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * 
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

#include "castor/BaseObject.hpp"
#include "castor/log/log.hpp"
#include "castor/log/DummyLogger.hpp"
#include "h/Cthread_api.h"

#include <gtest/gtest.h>
#include <gmock/gmock.h>

int main(int argc, char** argv) {
  // The call to Cthread_init is necessary at the beginning of any program
  // relying on the old "Cthread" collection. Calling should not have side
  // effects.
  Cthread_init();

  // By default unit tests do not want to log
  castor::log::init(new castor::log::DummyLogger("castorUnitTests"));

  // The following line must be executed to initialize Google Mock
  // (and Google Test) before running the tests.
  ::testing::InitGoogleMock(&argc, argv);
  int ret = RUN_ALL_TESTS();
  castor::BaseObject::resetServices();

  castor::log::shutdown();

  // Close standard in, out and error so that valgrind can be used with the
  // following command-line to track open file-descriptors:
  //
  //     valgrind --track-fds=yes
  close(0);
  close(1);
  close(2);

  return ret;
}
