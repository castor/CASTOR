/******************************************************************************
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * 
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

#pragma once

#include "castor/common/CastorConfiguration.hpp"
#include "castor/log/log.hpp"

namespace castor {
  namespace server {

    /**
     * Wrapper arount a CastorConfiguration object that logs each configuration
     * entry read.
     */
    class LoggedCastorConfiguration {
    public:

      /**
       * Constructor.
       *
       * @param conf CastorConfiguration object to be wrapped.
       */
      LoggedCastorConfiguration(castor::common::CastorConfiguration &conf);

      /**
       * Retrieves and logs a configuration entry as an integer.
       *
       * @param category category of the configuration parameter
       * @param name category of the configuration parameter
       * @param defaultValue the value to be returned if the configuration entry
       * is not in the configuration file
       * @return the integer value
       */
      template<typename T> T getConfEntInt(const std::string &category,
        const std::string &key, const T defaultValue) {
        std::string source;
        const T value = m_conf.getConfEntInt(category, key, defaultValue,
          &source);
        std::list<log::Param> params = {
          log::Param("category", category),
          log::Param("key", key),
          log::Param("value", value),
          log::Param("source", source)};
        log::write(LOG_INFO, "Configuration entry", params);
        return value;
      }

      /**
       * Retrieves and logs a configuration entry as an integer.
       *
       * Besides other possible exceptions, this method throws a
       * castor::exception::NoEntry exception if the specified configuration
       * entry is not in the configuration file.
       *
       * @param category category of the configuration parameter
       * @param name category of the configuration parameter
       * @return the integer value
       */
      template<typename T> T getConfEntInt(const std::string &category,
        const std::string &key) {
        std::string source;
        const T value = m_conf.getConfEntInt<T>(category, key, &source);
        std::list<log::Param> params = {
          log::Param("category", category),
          log::Param("key", key),
          log::Param("value", value),
          log::Param("source", source)};
        log::write(LOG_INFO, "Configuration entry", params);
        return value;
      }

      /**
       * Retrieves a configuration entry.
       *
       * @param category the category of the entry
       * @param key the key of the entry
       * @param defaultValue the value to be returned if the configuration entry
       * is not in the configuration file
       */
      const std::string& getConfEntString(const std::string &category,
        const std::string &key, const std::string &defaultValue);

      /**
       * Retrieves a configuration entry.
       *
       * Besides other possible exceptions, this method throws a
       * castor::exception::NoEntry exception if the specified configuration
       * entry is not in the configuration file.
       *
       * @param category the category of the entry
       * @param key the key of the entry
       */
      const std::string& getConfEntString(const std::string &category,
        const std::string &key);

    private:

      /**
       * CastorConfiguration object to be wrapped.
       */
      castor::common::CastorConfiguration &m_conf;

    }; // class LoggedCastorConfiguration

  } // namespace server
} // namespace castor
