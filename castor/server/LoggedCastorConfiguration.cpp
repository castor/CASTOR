/******************************************************************************
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * 
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

#include "castor/server/LoggedCastorConfiguration.hpp"

//------------------------------------------------------------------------------
// constructor
//------------------------------------------------------------------------------
castor::server::LoggedCastorConfiguration::LoggedCastorConfiguration(
  castor::common::CastorConfiguration &conf): m_conf(conf) {
}

//------------------------------------------------------------------------------
// getConfEntString
//------------------------------------------------------------------------------
const std::string& castor::server::LoggedCastorConfiguration::getConfEntString(
  const std::string &category, const std::string &key,
  const std::string &defaultValue) {
  std::string source;
  const std::string &value = m_conf.getConfEntString(category, key,
    defaultValue, &source);
  std::list<log::Param> params = {
    log::Param("category", category),
    log::Param("key", key),
    log::Param("value", value),
    log::Param("source", source)};
  log::write(LOG_INFO, "Configuration entry", params);
  return value;
}
  
//------------------------------------------------------------------------------
// getConfEntString
//------------------------------------------------------------------------------
const std::string& castor::server::LoggedCastorConfiguration::getConfEntString(
  const std::string &category, const std::string &key) {
  std::string source;
  const std::string &value = m_conf.getConfEntString(category, key, &source);
  std::list<log::Param> params = {
    log::Param("category", category),
    log::Param("key", key),
    log::Param("value", value),
    log::Param("source", source)};
  log::write(LOG_INFO, "Configuration entry", params);
  return value;
}
