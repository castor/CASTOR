/******************************************************************************
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Helper class for handling file-oriented user requests
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/


#pragma once

#include <vector>
#include <iostream>
#include <string>
#include <string.h>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "Cuuid.h"
#include "stager_constants.h"
#include "Cns_api.h"
#include "Cpwd.h"
#include "Cgrp.h"
#include "u64subr.h"
#include "serrno.h"
#include <errno.h>

#include "castor/IObject.hpp"
#include "castor/ObjectSet.hpp"
#include "castor/Constants.hpp"
#include "castor/stager/IStagerSvc.hpp"
#include "castor/db/DbCnvSvc.hpp"
#include "castor/BaseAddress.hpp"
#include "castor/IClient.hpp"
#include "castor/stager/SubRequest.hpp"
#include "castor/stager/FileRequest.hpp"
#include "castor/stager/SvcClass.hpp"
#include "castor/stager/CastorFile.hpp"
#include "castor/stager/FileClass.hpp"
#include "castor/stager/SubRequestStatusCodes.hpp"
#include "castor/stager/SubRequestGetNextStatusCodes.hpp"
#include "castor/exception/Exception.hpp"

namespace castor {

  namespace stager {

    namespace daemon {

      class RequestHelper {

      public:

        // services needed: database and stager services
        castor::stager::IStagerSvc* stagerService;
        castor::db::DbCnvSvc* dbSvc;
        castor::BaseAddress* baseAddr;

        // NameServer data
        ::Cns_fileid cnsFileid;
        ::Cns_filestatcs cnsFilestat;
        u_signed64 m_stagerOpenTimeInUsec;
      
        // request & co
        castor::stager::SubRequest* subrequest;
        castor::stager::FileRequest* fileRequest;
        castor::stager::SvcClass* svcClass;
        castor::stager::CastorFile* castorFile;

        // user and group id
        uid_t euid;
        gid_t egid;
      
        // for logging purposes
        Cuuid_t subrequestUuid;
        Cuuid_t requestUuid;
      
        timeval tvStart;

        RequestHelper(castor::stager::SubRequest* subRequestToProcess, int &typeRequest)
          ;

        ~RequestHelper() throw();

        /**
         * Resolves the svcClass if not resolved yet
         * @throw exception in case of any database error
         */
        void resolveSvcClass() ;

        /**
         * Checks the existence of the requested file in the NameServer, and creates it if the request allows for
         * creation. Returns the fileId and nsHost for the file, its size and its stagerOpenTime in microseconds
         * @throw exception in case of permission denied or non extisting file and no right to create it
         */
        static void openNameServerFile(const Cuuid_t &requestUuid, const uid_t euid, const gid_t egid,
                                       const int reqType, const std::string &fileName,
                                       const u_signed64 fileClassIfForced,
                                       const int modebits, const int flags,
                                       struct Cns_fileid &cnsFileid, u_signed64 &fileClass,
                                       u_signed64 &fileSize, u_signed64 &stagerOpenTimeInUsec)
        ;

        /**
         * Stats the requested file in the NameServer.
         * @throw exception in case of any NS error except ENOENT. serrno is set accordingly.
         */
        void statNameServerFile()
          ;

        /**
         * Gets the castorFile from the db, calling selectCastorFile
         * @throw exception in case of any database error
         */
        void getCastorFile() ;

        /**
         * Logs a standard message to DLF including all needed info (e.g. filename, svcClass, etc.)
         * @param level the DLF logging level
         * @param message the message to be logged
         * @param fid the fileId structure if needed
         */
        void logToDlf(int level, const char* message, u_signed64 fid = 0) throw();

      }; //end RequestHelper class
      
    }//end namespace daemon
    
  }//end namespace stager
  
}//end namespace castor

