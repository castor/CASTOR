/******************************************************************************
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Thread for dumping database logs to DLF
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

#include <vector>

#include "castor/Constants.hpp"
#include "castor/IService.hpp"
#include "castor/log/log.hpp"
#include "castor/stager/IStagerSvc.hpp"
#include "castor/exception/Exception.hpp"
#include "castor/db/DbCnvSvc.hpp"
#include "castor/stager/daemon/LoggingThread.hpp"

//-----------------------------------------------------------------------------
// run
//-----------------------------------------------------------------------------
void castor::stager::daemon::LoggingThread::run(void*) throw() {
  try {
    // get the StagerSvc
    castor::Services* svcs = castor::BaseObject::services();
    castor::IService* svc = svcs->service("DbStagerSvc", castor::SVC_DBSTAGERSVC);
    castor::stager::IStagerSvc *stgSvc = dynamic_cast<castor::stager::IStagerSvc*>(svc);

    // as dlopen is not reentrant (i.e., symbols might be still loading now due to the dlopen
    // of another thread), it may happen that the service is not yet valid or dynamic_cast fails.
    // In such a case we simply give up for this round.
    if(stgSvc == 0) return;

    // actual work: dump the DB logs to DLF
    stgSvc->dumpDBLogs();
    
    // log we're done
    castor::log::write(LOG_DEBUG, "Dump of the DB logs completed");

  } catch (castor::exception::Exception& e) {
    // "Unexpected exception caught"
    std::list<castor::log::Param> params =
      {castor::log::Param("Function", "LoggingThread::run"),
       castor::log::Param("Message", e.getMessage().str()),
       castor::log::Param("Code", e.code())};
    castor::log::write(LOG_ERR, "Unexpected exception caught", params);
  }
}
