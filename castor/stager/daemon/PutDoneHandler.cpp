
/************************************************************************************************/
/* PutDoneHandler: Constructor and implementation of the PutDone request's handle        */
/******************************************************************************************** */

#include "castor/stager/daemon/RequestHelper.hpp"
#include "castor/stager/daemon/ReplyHelper.hpp"
#include "castor/stager/daemon/PutDoneHandler.hpp"

#include "castor/Services.hpp"
#include "castor/BaseObject.hpp"
#include "castor/IService.hpp"
#include "stager_constants.h"
#include "castor/Constants.hpp"
#include "Cns_api.h"
#include "Cpwd.h"
#include "Cgrp.h"

#include "castor/stager/DiskCopyForRecall.hpp"
#include "castor/exception/Exception.hpp"
#include "castor/log/log.hpp"

#include "serrno.h"
#include <errno.h>

#include <iostream>
#include <string>


namespace castor{
  namespace stager{
    namespace daemon{
      
      void PutDoneHandler::handle() 
      {
        RequestHandler::handle();

        ReplyHelper* stgReplyHelper = NULL;
        
        // check if file exists         
        reqHelper->statNameServerFile();
        if(serrno == ENOENT) {
          reqHelper->logToDlf(LOG_USER_ERROR, "User asking for a non existing file", 0);
          castor::exception::Exception e(ENOENT);
          throw e;
        }

        // check write permissions
        if(0 != Cns_accessUser(reqHelper->subrequest->fileName().c_str(), W_OK,
                               reqHelper->fileRequest->euid(), reqHelper->fileRequest->egid())) {
          castor::exception::Exception ex(serrno);
          reqHelper->logToDlf(LOG_USER_ERROR, "User doesn't have the right permission", reqHelper->cnsFileid.fileid);
          throw ex;
        }
        
        // get/create CastorFile
        reqHelper->getCastorFile();
          
        try{
          reqHelper->logToDlf(LOG_DEBUG, "PutDone Request", reqHelper->cnsFileid.fileid);
          
          switch(reqHelper->stagerService->processPutDoneRequest(reqHelper->subrequest)) {
            
            case -1:  // request put in wait
            reqHelper->logToDlf(LOG_INFO, "Request moved to Wait", reqHelper->cnsFileid.fileid);
            break;
            
            case 0:   // error
            reqHelper->logToDlf(LOG_USER_ERROR, "Unable to perform request notifying user", reqHelper->cnsFileid.fileid);
            break;
            
            case 1:   // ok
            reqHelper->subrequest->setStatus(SUBREQUEST_FINISHED);
            
            stgReplyHelper = new ReplyHelper();
            stgReplyHelper->setAndSendIoResponse(reqHelper,&(reqHelper->cnsFileid), 0, "No error");
            stgReplyHelper->endReplyToClient(reqHelper);
            delete stgReplyHelper;
            stgReplyHelper = 0;
            break;
          }
          
        }
        catch(castor::exception::Exception& e){
          if(stgReplyHelper != NULL) delete stgReplyHelper;
          
          std::list<castor::log::Param> params = {
            castor::log::Param("NSFILEID", reqHelper->cnsFileid.fileid),
            castor::log::Param("REQID", reqHelper->requestUuid),
            castor::log::Param("Error Code",sstrerror(e.code())),
            castor::log::Param("Error Message",e.getMessage().str())
          };
          
          castor::log::write(LOG_ERR, "PutDone Request", params);
          throw e;
        }
      }
      
    }//end namespace daemon
  }//end namespace stager
}//end namespace castor
