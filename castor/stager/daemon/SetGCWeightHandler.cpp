/*******************************************************************************************************/
/* handler for the SetFileGCWeight request, simply call to the stagerService->setFileGCWeight()       */
/* since it isn't job oriented, it inherits from the RequestHandler                            */
/* it always needs to reply to the client                                                           */
/***************************************************************************************************/

#include "serrno.h"
#include <errno.h>
#include <iostream>
#include <string>

#include "Cupv_api.h"
#include "stager_constants.h"
#include "castor/IObject.hpp"
#include "castor/ObjectSet.hpp"
#include "castor/System.hpp"
#include "castor/stager/IStagerSvc.hpp"
#include "castor/stager/SubRequest.hpp"
#include "castor/stager/SetFileGCWeight.hpp"
#include "castor/exception/Exception.hpp"
#include "castor/log/log.hpp"

#include "castor/stager/daemon/SetGCWeightHandler.hpp"
#include "castor/stager/daemon/RequestHelper.hpp"
#include "castor/stager/daemon/ReplyHelper.hpp"


namespace castor{
  namespace stager{
    namespace daemon{
            
      void SetGCWeightHandler::handle() 
      {
        RequestHandler::handle();
        
        ReplyHelper* stgReplyHelper = new ReplyHelper();
        
        reqHelper->statNameServerFile(); 
        if(serrno == ENOENT) {
          // user error, file does not exist
          reqHelper->logToDlf(LOG_USER_ERROR, "User asking for a non existing file", 0);
          castor::exception::Exception ex(serrno);
          throw ex;
        }

        castor::stager::SetFileGCWeight* setGCWeightReq =
          dynamic_cast<castor::stager::SetFileGCWeight*>(reqHelper->fileRequest);
        std::string localHost;
        uid_t euid = setGCWeightReq->euid();
        uid_t egid = setGCWeightReq->egid();
        // Get the name of the localhost to pass into the Cupv interface.
        try {
          localHost = castor::System::getHostName();
        } catch (castor::exception::Exception& e) {
          castor::exception::Exception ex(SEINTERNAL);
          ex.getMessage() << "Failed to determine the name of the localhost";
          throw ex;
        }
        // Check if the user has GRP_ADMIN or ADMIN privileges.
        int rc = Cupv_check(euid, egid, localHost.c_str(), localHost.c_str(),
          (reqHelper->cnsFilestat.gid == egid ? P_GRP_ADMIN : P_ADMIN));
        if ((rc < 0) && (serrno != EACCES)) {
          castor::exception::Exception ex(serrno);
          ex.getMessage() << "Failed Cupv_check call for "
                          << euid << ":" << egid;
          throw ex;
        }
        else if (serrno == EACCES) {
          reqHelper->logToDlf(LOG_USER_ERROR,
                              "User doesn't have the right permission",
                              reqHelper->cnsFileid.fileid);
          castor::exception::Exception ex(serrno);
          throw ex;
        }

        try {
          // log the request
          std::list<castor::log::Param> params = {
            castor::log::Param("NSFILEID", reqHelper->cnsFileid.fileid),
            castor::log::Param("REQID", reqHelper->requestUuid),
            castor::log::Param("SUBREQID", reqHelper->subrequestUuid),
            castor::log::Param("Type",
                               ((unsigned)reqHelper->fileRequest->type() < castor::ObjectsIdsNb ?
                                castor::ObjectsIdStrings[reqHelper->fileRequest->type()] : "Unknown")),
            castor::log::Param("Filename", reqHelper->subrequest->fileName()),
            castor::log::Param("uid", reqHelper->euid),
            castor::log::Param("gid", reqHelper->egid),
            castor::log::Param("SvcClass", reqHelper->svcClass->name()),
            castor::log::Param("Weight", setGCWeightReq->weight())
          };
          castor::log::write(LOG_INFO, "SetGCWeight Request", params);
          // execute it
          int rc = reqHelper->stagerService->setFileGCWeight(reqHelper->cnsFileid.fileid, reqHelper->cnsFileid.server,
                                                                    reqHelper->svcClass->id(), setGCWeightReq->weight());
          // this method fails only if no diskCopies were found on the given service class. In such a case we answer the client
          // without involving the Error service.
          reqHelper->subrequest->setStatus(rc ? SUBREQUEST_FINISHED : SUBREQUEST_FAILED_FINISHED);
          
          stgReplyHelper->setAndSendIoResponse(reqHelper, &(reqHelper->cnsFileid),
                                               (rc ? 0 : ENOENT), (rc ? "" : "File not found on this service class"));
          stgReplyHelper->endReplyToClient(reqHelper);
          delete stgReplyHelper;
        }
        catch(castor::exception::Exception& e) {          
          if(stgReplyHelper != NULL) delete stgReplyHelper;
          std::list<castor::log::Param> params={
            castor::log::Param("NSFILEID", reqHelper->cnsFileid.fileid),
            castor::log::Param("REQID", reqHelper->requestUuid),
            castor::log::Param("Error Code", sstrerror(e.code())),
            castor::log::Param("Error Message", e.getMessage().str())
          };
          
          castor::log::write(LOG_ERR, "SetGCWeight Request", params);
          throw e; 
        }
      }
      
    }//end namespace daemon
  }//end namespace stager
}//end namespace castor
