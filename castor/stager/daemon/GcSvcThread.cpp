/******************************************************************************
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Service thread for garbage collection related requests
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

#include <vector>

#include "castor/Services.hpp"
#include "castor/Constants.hpp"
#include "castor/BaseAddress.hpp"
#include "castor/IClient.hpp"
#include "castor/IService.hpp"
#include "castor/log/log.hpp"
#include "castor/stager/IGCSvc.hpp"
#include "castor/exception/Exception.hpp"
#include "castor/BaseObject.hpp"
#include "castor/stager/Request.hpp"
#include "castor/stager/GCFileList.hpp"
#include "castor/stager/GCFile.hpp"
#include "castor/stager/Files2Delete.hpp"
#include "castor/stager/NsFilesDeleted.hpp"
#include "castor/stager/NsFilesDeletedResponse.hpp"
#include "castor/stager/StgFilesDeleted.hpp"
#include "castor/stager/StgFilesDeletedResponse.hpp"
#include "castor/stager/GCLocalFile.hpp"
#include "castor/rh/GCFilesResponse.hpp"
#include "castor/rh/BasicResponse.hpp"
#include "castor/replier/RequestReplier.hpp"
#include "castor/stager/daemon/GcSvcThread.hpp"

//-----------------------------------------------------------------------------
// constructor
//-----------------------------------------------------------------------------
castor::stager::daemon::GcSvcThread::GcSvcThread() throw() :
  BaseRequestSvcThread("GCSvc", "DbGCSvc", castor::SVC_DBGCSVC) {}


//-----------------------------------------------------------------------------
// handleFilesDeletedOrFailed
//-----------------------------------------------------------------------------
void castor::stager::daemon::GcSvcThread::handleFilesDeletedOrFailed
(castor::stager::Request* req,
 castor::IClient *client,
 castor::Services* svcs,
 castor::stager::IGCSvc* gcSvc,
 castor::BaseAddress &ad,
 Cuuid_t uuid) throw() {
  castor::rh::BasicResponse res;
  try {
    // cannot return 0 since we check the type before calling this method
    castor::stager::GCFileList *uReq =
      dynamic_cast<castor::stager::GCFileList*> (req);
    // Fills it with files to be deleted
    svcs->fillObj(&ad, req, castor::OBJ_GCFile);
    /* Invoking the method                */
    /* ---------------------------------- */
    std::vector<u_signed64*> arg;
    u_signed64* argArray = new u_signed64[uReq->files().size()];
    int i = 0;
    for (std::vector<castor::stager::GCFile*>::iterator it =
           uReq->files().begin();
         it != uReq->files().end();
         it++) {
      argArray[i] = (*it)->diskCopyId();
      arg.push_back(&(argArray[i]));
      i++;
    }
    std::list<castor::log::Param> params = {castor::log::Param("REQID", uuid)};
    if (castor::OBJ_FilesDeleted == req->type()) {
      // "Invoking filesDeleted"
      castor::log::write(LOG_INFO, "Invoking filesDeleted", params);
      gcSvc->filesDeleted(arg);
    } else {
      // "Invoking filesDeletionFailed"
      castor::log::write(LOG_INFO, "Invoking filesDeletionFailed", params);
      gcSvc->filesDeletionFailed(arg);
    }
    delete[] argArray;
  } catch (castor::exception::Exception& e) {
    // "Unexpected exception caught"
    std::list<castor::log::Param> params =
      {castor::log::Param("REQID", uuid),
       castor::log::Param("Function", "GcSvcThread::handleFilesDeletedOrFailed"),
       castor::log::Param("Message", e.getMessage().str()),
       castor::log::Param("Code", e.code())};
    castor::log::write(LOG_ERR, "Unexpected exception caught", params);
    res.setErrorCode(e.code());
    res.setErrorMessage(e.getMessage().str());
  }
  // Reply To Client
  try {
    castor::replier::RequestReplier *rr =
      castor::replier::RequestReplier::getInstance();
    res.setReqAssociated(req->reqId());
    rr->sendResponse(client, &res, true);
  } catch (castor::exception::Exception& e) {
    // "Unexpected exception caught"
    std::list<castor::log::Param> params =
      {castor::log::Param("REQID", uuid),
       castor::log::Param("Function", "GcSvcThread::handleFilesDeletedOrFailed.reply"),
       castor::log::Param("Message", e.getMessage().str()),
       castor::log::Param("Code", e.code())};
    castor::log::write(LOG_ERR, "Unexpected exception caught", params);
  }
}

//-----------------------------------------------------------------------------
// handleFiles2Delete
//-----------------------------------------------------------------------------
void castor::stager::daemon::GcSvcThread::handleFiles2Delete
(castor::stager::Request* req,
 castor::IClient *client,
 castor::Services*,
 castor::stager::IGCSvc* gcSvc,
 castor::BaseAddress&,
 Cuuid_t uuid) throw() {
  // Useful Variables
  castor::stager::Files2Delete *uReq;
  std::vector<castor::stager::GCLocalFile*>* result = 0;
  castor::rh::GCFilesResponse res;
  try {
    // get the Files2Delete
    // cannot return 0 since we check the type before calling this method
    uReq = dynamic_cast<castor::stager::Files2Delete*> (req);
    // "Invoking selectFiles2Delete"
    std::list<castor::log::Param> params =
      {castor::log::Param("REQID", uuid),
       castor::log::Param("DiskServer", uReq->diskServer())};
    castor::log::write(LOG_INFO, "Invoking selectFiles2Delete", params);
    result = gcSvc->selectFiles2Delete(uReq->diskServer());
  } catch (castor::exception::Exception& e) {
    // "Unexpected exception caught"
    std::list<castor::log::Param> params =
      {castor::log::Param("REQID", uuid),
       castor::log::Param("Function", "GcSvcThread::handleFiles2Delete"),
       castor::log::Param("Message", e.getMessage().str()),
       castor::log::Param("Code", e.code())};
    castor::log::write(LOG_ERR, "Unexpected exception caught", params);
    res.setErrorCode(e.code());
    res.setErrorMessage(e.getMessage().str());
  }
  if (0 != result) {
    for(std::vector<castor::stager::GCLocalFile *>::iterator it =
          result->begin();
        it != result->end();
        it++) {
      // Here we transfer the ownership of the GCLocalFiles
      // to res. Result can thus be deleted with no risk
      // of memory leak
      res.addFiles(*it);
      // "File selected for deletion"
      std::list<castor::log::Param> params =
        {castor::log::Param("NSFILEID", (*it)->fileId()),
         castor::log::Param("REQID", uuid),
         castor::log::Param("DiskServer", uReq->diskServer()),
         castor::log::Param("Filename", (*it)->fileName())};
      castor::log::write(LOG_INFO, "File selected for deletion", params);
    }
  }
  // Reply To Client
  try {
    castor::replier::RequestReplier *rr =
      castor::replier::RequestReplier::getInstance();
    res.setReqAssociated(req->reqId());
    rr->sendResponse(client, &res, true);
  } catch (castor::exception::Exception& e) {
    // "Unexpected exception caught"
    std::list<castor::log::Param> params =
      {castor::log::Param("REQID", uuid),
       castor::log::Param("Function", "GcSvcThread::handleFiles2Delete.reply"),
       castor::log::Param("Message", e.getMessage().str()),
       castor::log::Param("Code", e.code())};
    castor::log::write(LOG_ERR, "Unexpected exception caught", params);
  }
  // Cleanup
  if (result) delete result;
}

//-----------------------------------------------------------------------------
// handleNsFilesDeleted
//-----------------------------------------------------------------------------
void castor::stager::daemon::GcSvcThread::handleNsFilesDeleted
(castor::stager::Request* req,
 castor::IClient *client,
 castor::Services* svcs,
 castor::stager::IGCSvc* gcSvc,
 castor::BaseAddress &ad,
 Cuuid_t uuid) throw() {
  // Useful Variables
  castor::stager::NsFilesDeleted *uReq;
  std::vector<u_signed64> result;
  std::vector<u_signed64> delFileIds;
  castor::stager::NsFilesDeletedResponse res;
  try {
    // get the NsFilesDeleted request
    // cannot return 0 since we check the type before calling this method
    uReq = dynamic_cast<castor::stager::NsFilesDeleted*> (req);
    // Fills it with files to be deleted
    svcs->fillObj(&ad, req, castor::OBJ_GCFile);
    // collect input fileids
    u_signed64 *input = (u_signed64 *) calloc(uReq->files().size(), sizeof(u_signed64));
    int i = 0;
    for (std::vector<castor::stager::GCFile*>::const_iterator it =
           uReq->files().begin();
         it != uReq->files().end();
         it++, i++) {
      // despite the syntax, this IS a fileId.
      // Some bad reuse of exiting object, I apologize...
      input[i] = (*it)->diskCopyId();
    }

    // Call the nameserver to determine which files have been deleted
    int nbFids = uReq->files().size();
    int rc = Cns_bulkexist(uReq->nsHost().c_str(), input, &nbFids);
    if (rc != 0) {
        free(input);
        castor::exception::Exception e(serrno);
        e.getMessage() << strerror(serrno);
        throw e;
    }

    // Put the returned deleted files into a vector
    for (i = 0; i < nbFids; i++) {
        delFileIds.push_back(input[i]);
    }
    free(input);

    // only if some files are to be deleted
    if (nbFids > 0) {
      // "Invoking nsFilesDeleted"
      std::list<castor::log::Param> params =
        {castor::log::Param("REQID", uuid),
         castor::log::Param("NbFiles", uReq->files().size())};
      castor::log::write(LOG_INFO, "Invoking nsFilesDeleted", params);
      result = gcSvc->nsFilesDeleted(delFileIds, uReq->nsHost());
    }
  } catch (castor::exception::Exception& e) {
    // "Unexpected exception caught"
    std::list<castor::log::Param> params =
      {castor::log::Param("REQID", uuid),
       castor::log::Param("Function", "GcSvcThread::handleNsFilesDeleted"),
       castor::log::Param("Message", e.getMessage().str()),
       castor::log::Param("Code", e.code())};
    castor::log::write(LOG_ERR, "Unexpected exception caught", params);
    res.setErrorCode(e.code());
    res.setErrorMessage(e.getMessage().str());
  }

  // Prepare reply to gcd and log
  for(std::vector<u_signed64>::iterator it =
        result.begin();
      it != result.end();
      it++) {
    // Here we transfer the ownership of the GCFiles
    // to res. Thus we will not reallocate them
    castor::stager::GCFile *gf = new castor::stager::GCFile();
    gf->setDiskCopyId(*it);
    res.addOrphanFileIds(gf);
    // "File deleted since it disappeared from nameserver"
    std::list<castor::log::Param> params = {
      castor::log::Param("NSFILEID", *it),
      castor::log::Param("REQID", uuid)
    };
    castor::log::write(LOG_INFO, "File deleted since it disappeared from nameserver", params);
  }
  // Reply To Client
  try {
    castor::replier::RequestReplier *rr =
      castor::replier::RequestReplier::getInstance();
    res.setReqAssociated(req->reqId());
    rr->sendResponse(client, &res, true);
  } catch (castor::exception::Exception& e) {
    // "Unexpected exception caught"
    std::list<castor::log::Param> params =
      {castor::log::Param("REQID", uuid),
       castor::log::Param("Function", "GcSvcThread::handleNsFilesDeleted.reply"),
       castor::log::Param("Message", e.getMessage().str()),
       castor::log::Param("Code", e.code())};
    castor::log::write(LOG_ERR, "Unexpected exception caught", params);
  }
}

//-----------------------------------------------------------------------------
// handleStgFilesDeleted
//-----------------------------------------------------------------------------
void castor::stager::daemon::GcSvcThread::handleStgFilesDeleted
(castor::stager::Request* req,
 castor::IClient *client,
 castor::Services* svcs,
 castor::stager::IGCSvc* gcSvc,
 castor::BaseAddress &ad,
 Cuuid_t uuid) throw() {
  // Useful Variables
  castor::stager::StgFilesDeleted *uReq;
  std::vector<u_signed64> orphanDiskCopies;
  castor::stager::StgFilesDeletedResponse res;
  try {
    // get the StgFilesDeleted request
    // cannot return 0 since we check the type before calling this method
    uReq = dynamic_cast<castor::stager::StgFilesDeleted*> (req);
    // Fills it with files to be deleted
    svcs->fillObj(&ad, req, castor::OBJ_GCFile);
    // collect input copyIds
    std::vector<u_signed64> diskCopies;
    for (std::vector<castor::stager::GCFile*>::const_iterator it =
           uReq->files().begin();
         it != uReq->files().end();
         it++) {
      diskCopies.push_back((*it)->diskCopyId());
    }
    // "Invoking stgFilesDeleted"
    std::list<castor::log::Param> params =
      {castor::log::Param("REQID", uuid),
       castor::log::Param("NbFiles", uReq->files().size())};
    castor::log::write(LOG_INFO, "Invoking stgFilesDeleted", params);
    orphanDiskCopies = gcSvc->stgFilesDeleted(diskCopies, uReq->nsHost());
  } catch (castor::exception::Exception& e) {
    // "Unexpected exception caught"
    std::list<castor::log::Param> params =
      {castor::log::Param("REQID", uuid),
       castor::log::Param("Function", "GcSvcThread::handleStgFilesDeleted"),
       castor::log::Param("Message", e.getMessage().str()),
       castor::log::Param("Code", e.code())};
    castor::log::write(LOG_ERR, "Unexpected exception caught", params);
    res.setErrorCode(e.code());
    res.setErrorMessage(e.getMessage().str());
  }
  for(std::vector<u_signed64>::iterator it =
        orphanDiskCopies.begin();
      it != orphanDiskCopies.end();
      it++) {
    // Here we transfer the ownership of the GCFiles
    // to res. Thus we will not reallocate them
    castor::stager::GCFile *gf = new castor::stager::GCFile();
    gf->setDiskCopyId(*it);
    res.addOrphanFileIds(gf);
    // "File to be unlinked since it disappeared from stager"
    std::list<castor::log::Param> params =
      {castor::log::Param("REQID", uuid),
       castor::log::Param("DiskCopyId", *it)};
    castor::log::write(LOG_INFO, "File to be unlinked since it disappeared from the stager", params);
  }
  // Reply To Client
  try {
    castor::replier::RequestReplier *rr =
      castor::replier::RequestReplier::getInstance();
    res.setReqAssociated(req->reqId());
    rr->sendResponse(client, &res, true);
  } catch (castor::exception::Exception& e) {
    // "Unexpected exception caught"
    std::list<castor::log::Param> params =
      {castor::log::Param("REQID", uuid),
       castor::log::Param("Function", "GcSvcThread::handleStgFilesDeleted.reply"),
       castor::log::Param("Message", e.getMessage().str()),
       castor::log::Param("Code", e.code())};
    castor::log::write(LOG_ERR, "Unexpected exception caught", params);
  }
}

//-----------------------------------------------------------------------------
// process
//-----------------------------------------------------------------------------
void castor::stager::daemon::GcSvcThread::process
(castor::IObject *param) throw() {
  // Useful variables
  castor::stager::Request* req = 0;
  castor::Services *svcs = 0;
  castor::stager::IGCSvc *gcSvc = 0;
  Cuuid_t uuid = nullCuuid;
  castor::IClient *client = 0;
  // address to access db
  castor::BaseAddress ad;
  ad.setCnvSvcName("DbCnvSvc");
  ad.setCnvSvcType(castor::SVC_DBCNV);
  try {
    // get the GCSvc. Note that we cannot cache it since we
    // would not be thread safe
    svcs = castor::BaseObject::services();
    castor::IService* svc = svcs->service("DbGCSvc", castor::SVC_DBGCSVC);
    gcSvc = dynamic_cast<castor::stager::IGCSvc*>(svc);
    if (0 == gcSvc) {
      // "Could not get GCSvc"
      std::list<castor::log::Param> params =
        {castor::log::Param("REQID", uuid),
         castor::log::Param("Function", "GcSvcThread::process")};
      castor::log::write(LOG_ERR, "Could not get GCSvc", params);
      return;
    }
    // Retrieving request and client from the database
    // Note that casting the request will never give 0
    // since select does return a request for sure
    req = dynamic_cast<castor::stager::Request*>(param);
    string2Cuuid(&uuid, (char*)req->reqId().c_str());
    // Getting the client
    svcs->fillObj(&ad, req, castor::OBJ_IClient);
    client = req->client();
    if (0 == client) {
      // "No client associated with request ! Cannot answer !"
      std::list<castor::log::Param> params = {castor::log::Param("REQID", uuid)};
      castor::log::write(LOG_ERR, "No client associated with request ! Cannot answer !", params);
      delete req;
      return;
    }
  } catch (castor::exception::Exception& e) {
    // If we fail here, we do NOT have enough information
    // to reply to the client ! So we only log something.
    // "Unexpected exception caught"
    std::list<castor::log::Param> params =
      {castor::log::Param("REQID", uuid),
       castor::log::Param("Function", "GcSvcThread::process.1"),
       castor::log::Param("Message", e.getMessage().str()),
       castor::log::Param("Code", e.code())};
    castor::log::write(LOG_ERR, "Unexpected exception caught", params);
    if (req) delete req;
    return;
  }

  // At this point we are able to reply to the client
  switch (req->type()) {
  case castor::OBJ_FilesDeleted:
  case castor::OBJ_FilesDeletionFailed:
    castor::stager::daemon::GcSvcThread::handleFilesDeletedOrFailed
      (req, client, svcs, gcSvc, ad, uuid);
    break;
  case castor::OBJ_Files2Delete:
    castor::stager::daemon::GcSvcThread::handleFiles2Delete
      (req, client, svcs, gcSvc, ad, uuid);
    break;
  case castor::OBJ_NsFilesDeleted:
    castor::stager::daemon::GcSvcThread::handleNsFilesDeleted
      (req, client, svcs, gcSvc, ad, uuid);
    break;
  case castor::OBJ_StgFilesDeleted:
    castor::stager::daemon::GcSvcThread::handleStgFilesDeleted
      (req, client, svcs, gcSvc, ad, uuid);
    break;
  default:
    // "Unknown Request type"
    std::list<castor::log::Param> params =
      {castor::log::Param("REQID", uuid),
       castor::log::Param("Type", req->type())};
    castor::log::write(LOG_ERR, "Unknown Request type", params);
    delete req;
    req=0;
    return;
  }
  try {
    // Delete Request From the database
    svcs->deleteRep(&ad, req, true);
  } catch (castor::exception::Exception& e) {
    // "Unexpected exception caught"
    std::list<castor::log::Param> params =
      {castor::log::Param("REQID", uuid),
       castor::log::Param("Function", "GcSvcThread::process.2"),
       castor::log::Param("Message", e.getMessage().str()),
       castor::log::Param("Code", e.code())};
    castor::log::write(LOG_ERR, "Unexpected exception caught", params);
  }
  // Final cleanup
  delete req;
  req=0;
  return;
}
