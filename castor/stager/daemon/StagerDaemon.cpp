/******************************************************************************
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Main stager daemon
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

#include <iostream>
#include <string>
#include <errno.h>
#include <serrno.h>
#include <getconfent.h>

#include "castor/Constants.hpp"
#include "castor/BaseObject.hpp"
#include "castor/log/log.hpp"
#include "castor/exception/Exception.hpp"
#include "castor/exception/InvalidArgument.hpp"
#include "castor/log/SyslogLogger.hpp"
#include "castor/PortNumbers.hpp"
#include "castor/System.hpp"
#include "castor/server/SignalThreadPool.hpp"
#include "castor/server/DbAlertedThreadPool.hpp"
#include "castor/replier/RequestReplier.hpp"
#include "castor/db/DbCnvSvc.hpp"

#include "castor/stager/daemon/StagerDaemon.hpp"
#include "castor/stager/daemon/JobRequestSvcThread.hpp"
#include "castor/stager/daemon/StageRequestSvcThread.hpp"
#include "castor/stager/daemon/BulkStageReqSvcThread.hpp"
#include "castor/stager/daemon/QueryRequestSvcThread.hpp"
#include "castor/stager/daemon/ErrorSvcThread.hpp"
#include "castor/stager/daemon/GcSvcThread.hpp"
#include "castor/stager/daemon/LoggingThread.hpp"
#include "castor/stager/daemon/NsOverride.hpp"


int main(int argc, char* argv[]){
  try{
    castor::log::init(new castor::log::SyslogLogger("stagerd"));

    castor::stager::daemon::StagerDaemon
      stagerDaemon(std::cout, std::cerr);

    castor::stager::IStagerSvc* stgService =
      dynamic_cast<castor::stager::IStagerSvc*>
      (castor::BaseObject::services()->service
       ("DbStagerSvc", castor::SVC_DBSTAGERSVC));
    if(stgService == 0) {
      castor::exception::Exception e(EINVAL);
      e.getMessage() << "Failed to load DbStagerSvc, check for shared libraries configuration" << std::endl;
      throw e;
    }

    /*******************************/
    /* thread pools for the stager */
    /*******************************/
    stagerDaemon.addThreadPool
      (new castor::server::DbAlertedThreadPool
       ("JobRequestSvcThread",
	new castor::stager::daemon::JobRequestSvcThread()));

    stagerDaemon.addThreadPool
      (new castor::server::DbAlertedThreadPool
       ("StageRequestSvcThread",
	new castor::stager::daemon::StageRequestSvcThread()));

    stagerDaemon.addThreadPool
      (new castor::server::DbAlertedThreadPool
       ("BulkStageReqSvcThread",
	new castor::stager::daemon::BulkStageReqSvcThread()));

    stagerDaemon.addThreadPool
      (new castor::server::DbAlertedThreadPool
       ("QueryRequestSvcThread",
	new castor::stager::daemon::QueryRequestSvcThread()));

    // These threads poll the database every 2 seconds.
    stagerDaemon.addThreadPool
      (new castor::server::DbAlertedThreadPool
       ("ErrorSvcThread",
	new castor::stager::daemon::ErrorSvcThread(), 2));

    stagerDaemon.addThreadPool
      (new castor::server::DbAlertedThreadPool
       ("GcSvcThread",
	new castor::stager::daemon::GcSvcThread()));

    // This thread dumps the logs from the DB
    stagerDaemon.addThreadPool
      (new castor::server::SignalThreadPool
       ("LoggingThread",
	new castor::stager::daemon::LoggingThread(), 5));

    stagerDaemon.getThreadPool('J')->setNbThreads(10);
    stagerDaemon.getThreadPool('S')->setNbThreads(4);
    stagerDaemon.getThreadPool('B')->setNbThreads(3);
    stagerDaemon.getThreadPool('Q')->setNbThreads(6);
    stagerDaemon.getThreadPool('E')->setNbThreads(2);
    stagerDaemon.getThreadPool('G')->setNbThreads(4);
    stagerDaemon.getThreadPool('L')->setNbThreads(1);

    stagerDaemon.parseCommandLine(argc, argv);

    const bool runAsStagerSuperuser = true;
    stagerDaemon.start(runAsStagerSuperuser);

  } catch (castor::exception::Exception& e) {
    std::cerr << "Caught exception: "
	      << sstrerror(e.code()) << std::endl
	      << e.getMessage().str() << std::endl;

    // "Exception caught when starting "
    std::list<castor::log::Param> params =
      {castor::log::Param("Code", sstrerror(e.code())),
       castor::log::Param("Message", e.getMessage().str())};
    castor::log::write(LOG_ERR, "Exception caught when starting", params);
  } catch (...) {
    std::cerr << "Caught general exception!" << std::endl;
  }

  return 0;
}// end main


/*****************************************************************************************/
/* constructor: initiallizes the DLF logging and set the default value to its attributes */
/*****************************************************************************************/
castor::stager::daemon::StagerDaemon::StagerDaemon(std::ostream &stdOut,
  std::ostream &stdErr) : castor::server::MultiThreadedDaemon(stdOut, stdErr) {
  castor::log::write(LOG_INFO, "StagerDaemon started");
}

/*************************************************************/
/* help method for the configuration (from the command line) */
/*************************************************************/
void castor::stager::daemon::StagerDaemon::help(std::string programName)
{
  std::cout << "Usage: " << programName << " [options]\n"
    "\n"
    "where options can be:\n"
    "\n"
    "\t--Jthreads    or -J {integer >= 0}  \tNumber of threads for the Job requests service\n"
    "\t--Sthreads    or -S {integer >= 0}  \tNumber of threads for the Stage requests service\n"
    "\t--Qthreads    or -Q {integer >= 0}  \tNumber of threads for the Query requests service\n"
    "\t--Ethreads    or -E {integer >= 0}  \tNumber of threads for the Error service\n"
    "\t--Gthreads    or -G {integer >= 0}  \tNumber of threads for the GC service\n"
    "\n"
    "Comments to: Castor.Support@cern.ch\n";
}

void castor::stager::daemon::StagerDaemon::waitAllThreads() throw()
{
  castor::server::MultiThreadedDaemon::waitAllThreads();
  castor::replier::RequestReplier::getInstance()->terminate();
}
