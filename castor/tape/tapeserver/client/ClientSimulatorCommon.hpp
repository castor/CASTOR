/******************************************************************************
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * 
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

#pragma once

#include "castor/io/ServerSocket.hpp"

namespace castor {
namespace tape {
namespace tapeserver {
namespace client {
  /**
   * This class contains common code shared by the ClientSimulator and
   * ClientSimSingleReply subclasses.
   *
   * This ia class which simulates the tape server part of the client
   * communication with the server. The constructor will hence setup a client
   * callback socket and wait. All the tape mounting logic is hence skipped.
   * We will then in parallel start a tape session.
   */
  class ClientSimulatorCommon {
  public:
    /**
     * Constrcutor.
     *
     * @param volReqId The volume request ID returned by the VDQM as a result of
     *                 requesting a drive.
     */
    ClientSimulatorCommon(const uint32_t volReqId);

    /**
     * Creates, binds and sets to listening the callback socket to be used for
     * callbacks from the tape-bridge daemon.
     */
    void setupCallbackSock();

    /**
     * Desctructor.
     */
    virtual ~ClientSimulatorCommon() throw() = 0;

  protected:
    
    /**
     * TCP/IP tape-bridge callback socket.
     */
    castor::io::ServerSocket m_callbackSock;

    /**
     * The volume request ID returned by the VDQM as a result of requesting a
     * drive.
     */
    int32_t m_volReqId;
  };

} // namespace client
} // namespace tapeserver
} // namespace tape
} // namesapce castor
