/******************************************************************************
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * 
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

#pragma once

#include <memory>
#include "castor/tape/tapeserver/client/ClientSimulatorCommon.hpp"
#include "castor/tape/tapegateway/FileToMigrateStruct.hpp"
#include "castor/tape/tapegateway/FilesToMigrateList.hpp"
#include "castor/tape/tapegateway/RAOFilesToRecallList.hpp"
#include "castor/tape/tapegateway/RAOFileToRecallStruct.hpp"
#include "castor/tape/tapegateway/VolumeRequest.hpp"

namespace castor {
namespace tape {
namespace tapeserver {
namespace client {
  /**
   * A templated class which simulates the tapegateway part of the client
   * communication with the server. The constructor will hence setup a client
   * callback socket and wait. The method processRequest will wait for a
   * message and reply with the type chosen on template implementation.
   */
  template<class ReplyType>
  class ClientSimSingleReply: public ClientSimulatorCommon {
  public:
    ClientSimSingleReply(uint32_t volReqId, const std::string & vid, 
            const std::string & density, bool breakTransaction = false): 
    ClientSimulatorCommon(volReqId),
    m_vid(vid), 
    m_density(density), m_breakTransaction(breakTransaction)
    {
    }

    virtual ~ClientSimSingleReply() throw() {}
    
    struct ipPort {
      ipPort(uint32_t i, uint16_t p): ip(i), port(p) {}
      union {
        uint32_t ip;
        struct {
          uint8_t a;
          uint8_t b;
          uint8_t c;
          uint8_t d;
        } parts;
      };
      uint16_t port;
    };
    struct ipPort getCallbackAddress() {
      unsigned short port = 0;
      /* This is a workaround for the usage of unsigned long for ips in castor
       * (it's not fine anymore on 64 bits systems).
       */
      unsigned long ip = 0;
      m_callbackSock.getPortIp(port, ip);
      return ipPort(ip,port);
    }
    void sessionLoop() {
      processFirstRequest();
      m_callbackSock.close();
    }

  private:
    // Process the first request which should be getVolume
    void processFirstRequest()  {
      // Accept the next connection
      std::unique_ptr<castor::io::ServerSocket> clientConnection(m_callbackSock.accept());
      // Read in the message sent by the tapebridge
      std::unique_ptr<castor::IObject> obj(clientConnection->readObject());
      // Convert to a gateway message (for transactionId)
      tapegateway::GatewayMessage & gm = 
              dynamic_cast<tapegateway::GatewayMessage &>(*obj);
      // Reply with our own type and transaction id
      ReplyType repl;
      repl.setAggregatorTransactionId(gm.aggregatorTransactionId() ^ 
        (m_breakTransaction?666:0));
      repl.setMountTransactionId(m_volReqId);
      clientConnection->sendObject(repl);
    }
    // Notify the client
    void sendEndNotificationErrorReport(
    const uint64_t             tapebridgeTransactionId,
    const int                  errorCode,
    const std::string          &errorMessage,
    castor::io::AbstractSocket &sock)
    throw();
    std::string m_vid;
    std::string m_volLabel;
    std::string m_density;
    bool m_breakTransaction;
  };
  
  /**
   * Specific version for the FilesToMigrateList: we want
   * to pass a non empty list, so it has to be a little bit less trivial.
   */
  template<>
  void ClientSimSingleReply<castor::tape::tapegateway::FilesToMigrateList>::processFirstRequest() 
           {
    using namespace castor::tape::tapegateway;
      // Accept the next connection
      std::unique_ptr<castor::io::ServerSocket> clientConnection(m_callbackSock.accept());
      // Read in the message sent by the tapebridge
      std::unique_ptr<castor::IObject> obj(clientConnection->readObject());
      // Convert to a gateway message (for transactionId)
      GatewayMessage & gm = 
              dynamic_cast<tapegateway::GatewayMessage &>(*obj);
      // Reply with our own type and transaction id
      FilesToMigrateList repl;
      repl.setAggregatorTransactionId(gm.aggregatorTransactionId() ^ 
        (m_breakTransaction?666:0));
      repl.setMountTransactionId(m_volReqId);
      repl.filesToMigrate().push_back(new FileToMigrateStruct());
      clientConnection->sendObject(repl);
  }
  
    /**
   * Specific version for the FilesToRecallList: we want
   * to pass a non empty list, so it has to be a little bit less trivial.
   */
  template<>
  void ClientSimSingleReply<castor::tape::tapegateway::RAOFilesToRecallList>::processFirstRequest() 
           {
    using namespace castor::tape::tapegateway;
      // Accept the next connection
      std::unique_ptr<castor::io::ServerSocket> clientConnection(m_callbackSock.accept());
      // Read in the message sent by the tapebridge
      std::unique_ptr<castor::IObject> obj(clientConnection->readObject());
      // Convert to a gateway message (for transactionId)
      GatewayMessage & gm = 
              dynamic_cast<tapegateway::GatewayMessage &>(*obj);
      // Reply with our own type and transaction id
      RAOFilesToRecallList repl;
      repl.setAggregatorTransactionId(gm.aggregatorTransactionId() ^ 
        (m_breakTransaction?666:0));
      repl.setMountTransactionId(m_volReqId);
      repl.filesToRecall().push_back(new RAOFileToRecallStruct());
      clientConnection->sendObject(repl);
  }
}
}
}
}
