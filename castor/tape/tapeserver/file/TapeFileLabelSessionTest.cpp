/******************************************************************************
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * 
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

#include "castor/exception/UserError.hpp"
#include "castor/tape/tapeserver/drive/FakeDrive.hpp"
#include "castor/tape/tapeserver/file/File.hpp"

#include <gtest/gtest.h>
#include <memory>

namespace UnitTests {
  
class TapeFileLabelSessionTest : public ::testing::Test {
protected:
  virtual void SetUp() {
  }
  virtual void TearDown() {
  }   
};
  
TEST_F(TapeFileLabelSessionTest, labelEmptyTapeWithoutForce) {    
  using namespace castor::tape;

  tapeserver::drive::FakeDrive drive;
  const bool tapeServerConfiguredToUseLbp = true;
  const std::string vid = "123456";
  const bool lbp = true;
  const bool forceLabellingOfNonEmptyTapes = false;

  tapeFile::LabelSession(drive, tapeServerConfiguredToUseLbp, vid, lbp, forceLabellingOfNonEmptyTapes);
}
  
TEST_F(TapeFileLabelSessionTest, labelNonEmptyExpectedTapeWithForce) {    
  using namespace castor::tape;

  tapeserver::drive::FakeDrive drive;

  // Label an empty tape so that it is no longer empty
  {
    const bool tapeServerConfiguredToUseLbp = true;
    const std::string vid = "123456";
    const bool lbp = true;
    const bool forceLabellingOfNonEmptyTapes = false;

    tapeFile::LabelSession(drive, tapeServerConfiguredToUseLbp, vid, lbp, forceLabellingOfNonEmptyTapes);
  }

  drive.rewind();

  // Label the non-empty tape with force
  {
    const bool tapeServerConfiguredToUseLbp = true;
    const std::string vid = "123456";
    const bool lbp = true;
    const bool forceLabellingOfNonEmptyTapes = true;

    tapeFile::LabelSession(drive, tapeServerConfiguredToUseLbp, vid, lbp, forceLabellingOfNonEmptyTapes);
  }
}
  
TEST_F(TapeFileLabelSessionTest, labelNonEmptyTapeWithoutForce) {    
  using namespace castor::tape;

  tapeserver::drive::FakeDrive drive;

  // Label an empty tape so that it is no longer empty
  {
    const bool tapeServerConfiguredToUseLbp = true;
    const std::string vid = "123456";
    const bool lbp = true;
    const bool forceLabellingOfNonEmptyTapes = false;

    tapeFile::LabelSession(drive, tapeServerConfiguredToUseLbp, vid, lbp, forceLabellingOfNonEmptyTapes);
  }

  drive.rewind();

  // Try to label the non-empty tape without force
  {
    const bool tapeServerConfiguredToUseLbp = true;
    const std::string vid = "123456";
    const bool lbp = true;
    const bool forceLabellingOfNonEmptyTapes = false;

    ASSERT_THROW(tapeFile::LabelSession(drive, tapeServerConfiguredToUseLbp, vid, lbp, forceLabellingOfNonEmptyTapes),
      castor::exception::UserError);
  }
}

TEST_F(TapeFileLabelSessionTest, labelNonEmptyUnexpectedTapeWithForce) {
  using namespace castor::tape;

  tapeserver::drive::FakeDrive drive;

  // Label an empty tape with an incorrect VID
  {
    const char labelWithWrongVid[] = "VOL1VWRONG                           CASTOR                                    3";
    ASSERT_EQ(81, sizeof(labelWithWrongVid));
    drive.writeBlock(labelWithWrongVid, 80);
  }

  drive.rewind();

  // try to label the non-empty unexpected tape with force
  {
    const bool tapeServerConfiguredToUseLbp = true;
    const std::string vid = "123456";
    const bool lbp = true;
    const bool forceLabellingOfNonEmptyTapes = true;

    ASSERT_THROW(tapeFile::LabelSession(drive, tapeServerConfiguredToUseLbp, vid, lbp, forceLabellingOfNonEmptyTapes),
      castor::exception::Exception);
  }
}

TEST_F(TapeFileLabelSessionTest, labelGarbageVol1TapeWithForce) {
  using namespace castor::tape;

  tapeserver::drive::FakeDrive drive;

  // Label an empty tape with an incorrect VID
  {
    const char labelWithWrongVid[] = "GARBAGEGARBAGEGARBAGEGARBAGEGARBAGEGARBAGEGARBAGEGARBAGEGARBAGEGARBAGEGARBAGEGAR";
    ASSERT_EQ(81, sizeof(labelWithWrongVid));
    drive.writeBlock(labelWithWrongVid, 80);
  }

  drive.rewind();

  // Try to label the non-empty garbage filled tape with force
  {
    const bool tapeServerConfiguredToUseLbp = true;
    const std::string vid = "123456";
    const bool lbp = true;
    const bool forceLabellingOfNonEmptyTapes = true;

    ASSERT_THROW(tapeFile::LabelSession(drive, tapeServerConfiguredToUseLbp, vid, lbp, forceLabellingOfNonEmptyTapes),
      tapeFile::TapeFormatError);
  }
}

TEST_F(TapeFileLabelSessionTest, labelTooSmallVol1TapeWithForce) {
  using namespace castor::tape;

  tapeserver::drive::FakeDrive drive;

  // Label an empty tape with an incorrect VID
  {
    const char tooSmallVol1[] = "TOOSMALL";
    ASSERT_EQ(9, sizeof(tooSmallVol1));
    drive.writeBlock(tooSmallVol1, 8);
  }

  drive.rewind();

  // Try to label the non-empty garbage filled tape with force
  {
    const bool tapeServerConfiguredToUseLbp = true;
    const std::string vid = "123456";
    const bool lbp = true;
    const bool forceLabellingOfNonEmptyTapes = true;

    ASSERT_THROW(tapeFile::LabelSession(drive, tapeServerConfiguredToUseLbp, vid, lbp, forceLabellingOfNonEmptyTapes),
      castor::exception::Exception);
  }
}

TEST_F(TapeFileLabelSessionTest, labelTooLargeVol1TapeWithForce) {
  using namespace castor::tape;

  tapeserver::drive::FakeDrive drive;

  // Label an empty tape with an incorrect VID
  {
    const char labelWithWrongVid[] = "TOOSMALL";
    ASSERT_EQ(9, sizeof(labelWithWrongVid));
    drive.writeBlock(labelWithWrongVid, 8);
  }

  drive.rewind();

  // Try to label the non-empty garbage filled tape with force
  {
    const bool tapeServerConfiguredToUseLbp = true;
    const std::string vid = "123456";
    const bool lbp = true;
    const bool forceLabellingOfNonEmptyTapes = true;

    ASSERT_THROW(tapeFile::LabelSession(drive, tapeServerConfiguredToUseLbp, vid, lbp, forceLabellingOfNonEmptyTapes),
      castor::exception::Exception);
  }
}

} // namespace UnitTests
