/******************************************************************************
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * 
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

#include "castor/server/LoggedCastorConfiguration.hpp"
#include "castor/tape/tapeserver/daemon/Constants.hpp"
#include "castor/tape/tapeserver/daemon/CatalogueConfig.hpp"
#include "castor/tape/tapeserver/TapeBridgeConstants.hpp"

//------------------------------------------------------------------------------
// constructor
//------------------------------------------------------------------------------
castor::tape::tapeserver::daemon::CatalogueConfig::CatalogueConfig() throw():
  waitJobTimeoutInSecs(TAPESERVER_WAITJOBTIMEOUT),
  waitJobTimeoutSignal(TAPESERVER_WAITJOBTIMEOUTSIGNAL),    
  mountTimeoutInSecs(TAPESERVER_MOUNTTIMEOUT),
  mountTimeoutSignal(TAPESERVER_MOUNTTIMEOUTSIGNAL),     
  blockMoveTimeoutInSecs(TAPESERVER_BLKMOVETIMEOUT),
  blockMoveTimeoutSignal(TAPESERVER_BLKMOVETIMEOUTSIGNAL),     
  vdqmDriveSyncIntervalSecs(TAPESERVER_VDQMDRIVESYNCINTERVAL) {
}

//------------------------------------------------------------------------------
// createFromCastorConf
//------------------------------------------------------------------------------
castor::tape::tapeserver::daemon::CatalogueConfig
  castor::tape::tapeserver::daemon::CatalogueConfig::createFromCastorConf(
    log::Logger *const log) {
  server::LoggedCastorConfiguration
    castorConf(common::CastorConfiguration::getConfig());

  CatalogueConfig config;
  config.waitJobTimeoutInSecs = castorConf.getConfEntInt("TapeServer",
    "WaitJobTimeout", TAPESERVER_WAITJOBTIMEOUT);
  config.waitJobTimeoutSignal = castorConf.getConfEntInt("TapeServer",
    "WaitJobTimeoutSignal", TAPESERVER_WAITJOBTIMEOUTSIGNAL);
  config.mountTimeoutInSecs = castorConf.getConfEntInt("TapeServer",
    "MountTimeout", TAPESERVER_MOUNTTIMEOUT);
  config.mountTimeoutSignal = castorConf.getConfEntInt("TapeServer",
    "MountTimeoutSignal", TAPESERVER_MOUNTTIMEOUTSIGNAL);
  config.blockMoveTimeoutInSecs = castorConf.getConfEntInt("TapeServer",
    "BlkMoveTimeout", TAPESERVER_BLKMOVETIMEOUT);
  config.blockMoveTimeoutSignal = castorConf.getConfEntInt("TapeServer",
    "BlkMoveTimeoutSignal", TAPESERVER_BLKMOVETIMEOUTSIGNAL);
  config.vdqmDriveSyncIntervalSecs = castorConf.getConfEntInt("TapeServer",
    "VdqmDriveSyncInterval", TAPESERVER_VDQMDRIVESYNCINTERVAL);

  return config;
}
