/****************************************************************************** 
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * 
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/
#include "castor/tape/tapeserver/daemon/RecallTaskInjector.hpp"
#include "castor/tape/tapeserver/client/ClientInterface.hpp"
#include "castor/log/LogContext.hpp"
#include "castor/tape/tapegateway/RAOFilesToRecallList.hpp"
#include "castor/tape/tapegateway/RAOFileToRecallStruct.hpp"
#include "castor/tape/tapeserver/utils/suppressUnusedVariable.hpp"
#include "castor/tape/tapeserver/daemon/DiskWriteThreadPool.hpp"
#include "castor/tape/tapeserver/daemon/TapeReadTask.hpp"
#include "castor/tape/tapeserver/client/ClientProxy.hpp"
#include "castor/tape/tapeserver/client/ClientInterface.hpp"
#include "castor/tape/tapeserver/file/File.hpp"
#include "log.h"
#include "castor/tape/tapeserver/daemon/TapeReadSingleThread.hpp"
#include <stdint.h>

using castor::log::LogContext;
using castor::log::Param;

namespace {
  
  // TODO: merge with same function in File.hpp and move to tape/utils
  uint32_t blockID(const castor::tape::tapegateway::RAOFileToRecallStruct& ftr)
  {
    return (ftr.blockId0() << 24) | (ftr.blockId1() << 16) |  (ftr.blockId2() << 8) | ftr.blockId3();
  }
 
}

namespace{
  /**
   *  function to set a NULL the owning FilesToMigrateList  of a FileToMigrateStruct
   *   Indeed, a clone of a structure will only do a shallow copy (sic).
   *   Otherwise at the second destruction the object will try to remove itself 
   *   from the owning list and then boom !
   * @param ptr a pointer to an object to change
   * @return the parameter ptr 
   */
  castor::tape::tapegateway::RAOFileToRecallStruct* removeOwningList(castor::tape::tapegateway::RAOFileToRecallStruct* ptr){
    ptr->setFilesToRecallList(NULL);
    return ptr;
  }
}

namespace castor{
namespace tape{
namespace tapeserver{
namespace daemon {
  
RecallTaskInjector::RecallTaskInjector(RecallMemoryManager & mm, 
        TapeSingleThreadInterface<TapeReadTask> & tapeReader,
        DiskWriteThreadPool & diskWriter,
        client::ClientInterface& client,
        uint64_t maxFiles, uint64_t byteSizeThreshold,castor::log::LogContext lc) : 
        m_thread(*this),m_memManager(mm),
        m_tapeReader(tapeReader),m_diskWriter(diskWriter),
        m_client(client),m_lc(lc),m_maxFiles(maxFiles),m_maxBytes(byteSizeThreshold),
        m_clientType(tapegateway::READ_TP)
{}
//------------------------------------------------------------------------------
//destructor
//------------------------------------------------------------------------------
RecallTaskInjector::~RecallTaskInjector(){
}
//------------------------------------------------------------------------------
//finish
//------------------------------------------------------------------------------

void RecallTaskInjector::finish(){
  castor::server::MutexLocker ml(&m_producerProtection);
  m_queue.push(Request());
}
//------------------------------------------------------------------------------
//requestInjection
//------------------------------------------------------------------------------
void RecallTaskInjector::requestInjection(bool lastCall) {
  //@TODO where shall we  acquire the lock ? There of just before the push ?
  castor::server::MutexLocker ml(&m_producerProtection);
  m_queue.push(Request(m_maxFiles, m_maxBytes, lastCall));
}
//------------------------------------------------------------------------------
//waitThreads
//------------------------------------------------------------------------------
void RecallTaskInjector::waitThreads() {
  m_thread.wait();
}
//------------------------------------------------------------------------------
//startThreads
//------------------------------------------------------------------------------
void RecallTaskInjector::startThreads() {
  m_thread.start();
}
//------------------------------------------------------------------------------
//setDriveInterface
//------------------------------------------------------------------------------
void RecallTaskInjector::setDriveInterface(castor::tape::tapeserver::drive::DriveInterface *di) {
  m_drive = di;
}
//------------------------------------------------------------------------------
//initRAO
//------------------------------------------------------------------------------
void RecallTaskInjector::initRAO() {
  m_useRAO = true;
  m_raoFuture = m_raoPromise.get_future();
}
//------------------------------------------------------------------------------
//waitForPromise
//------------------------------------------------------------------------------
void RecallTaskInjector::waitForPromise() {
  m_raoFuture.wait();
}
//------------------------------------------------------------------------------
//setPromise
//------------------------------------------------------------------------------
void RecallTaskInjector::setPromise() {
  try {
    m_raoPromise.set_value();
  } catch (const std::exception &exc) {}
}
//------------------------------------------------------------------------------
//injectBulkRecalls
//------------------------------------------------------------------------------
void RecallTaskInjector::injectBulkRecalls() {
  uint32_t block_size = 262144;
  uint32_t njobs = m_jobs.size();
  std::vector<uint32_t> raoOrder;

  if (m_useRAO) {
    std::list<castor::tape::SCSI::Structures::RAO::blockLims> files;

    for (uint32_t i = 0; i < njobs; i++) {
      tapegateway::RAOFileToRecallStruct *job = m_jobs.at(i);
      if (! job->hasFileSize()) {
        m_lc.log(LOG_INFO, "RAO supported by the drive but not by the stager");
        m_useRAO = false;
        break;
      }

      castor::tape::SCSI::Structures::RAO::blockLims lims;
      strncpy((char*)lims.fseq, std::to_string(i).c_str(), sizeof(i));
      lims.begin = castor::tape::tapeFile::BlockId::extract(*job);
      lims.end = lims.begin + 8 +
                /* ceiling the number of blocks */
                ((job->fileSize() + block_size - 1) / block_size);

      files.push_back(lims);

      if (files.size() == m_raoLimits.maxSupported ||
              ((i == njobs - 1) && (files.size() > 1))) {
        /* We do a RAO query if:
         *  1. the maximum number of files supported by the drive
         *     for RAO query has been reached
         *  2. the end of the jobs list has been reached and there are at least
         *     2 unordered files
         */
        m_drive->queryRAO(files, m_raoLimits.maxSupported);

        /* Add the RAO sorted files to the new list*/
        for (auto fit = files.begin(); fit != files.end(); fit++) {
          uint64_t id = atoi((char*)fit->fseq);
          raoOrder.push_back(id);
        }
        files.clear();
      }
    }
    
    /* Copy the rest of the files in the new ordered list */
    for (auto fit = files.begin(); fit != files.end(); fit++) {
      if (! m_useRAO) break;
      uint64_t id = atoi((char*)fit->fseq);
      raoOrder.push_back(id);
    }
    files.clear();
  }

  std::ostringstream recallOrderLog;
  if(m_useRAO) {
    recallOrderLog << "Recall order of FSEQs using RAO:";
  } else {
    recallOrderLog << "Recall order of FSEQs:";
  }

  for (uint32_t i = 0; i < njobs; i++) {
    uint32_t index = m_useRAO ? raoOrder.at(i) : i;

    tapegateway::RAOFileToRecallStruct *job = m_jobs.at(index);

    recallOrderLog << " " << job->fseq();

    // Workaround for bug CASTOR-4829: tapegateway: should request positioning by blockid for recalls instead of fseq
    // When the client is a tape gateway, we should *always* position by block id.
    if (tapegateway::TAPE_GATEWAY == m_clientType) {
      job->setPositionCommandCode(tapegateway::TPPOSIT_BLKID);
    }

    log::ScopedParamContainer params(m_lc);
    params.add("NSHOSTNAME", job->nshost())
          .add("NSFILEID", job->fileid())
          .add("fSeq", job->fseq())
          .add("blockID", blockID(*job))
          .add("path", job->path())
          .add("hasFileSize", job->hasFileSize() ? "true" : "false");
    if(job->hasFileSize()) {
      params.add("fileSize", job->fileSize());
    }

    m_lc.log(LOG_INFO, "Recall task created");
    
    DiskWriteTask * dwt = 
      new DiskWriteTask(
        removeOwningList(dynamic_cast<tape::tapegateway::RAOFileToRecallStruct*>(job->clone())),
        m_memManager);
    TapeReadTask * trt = 
      new TapeReadTask(
        removeOwningList(
          dynamic_cast<tape::tapegateway::RAOFileToRecallStruct*>(job->clone())),
          *dwt,
          m_memManager);
    
    m_diskWriter.push(dwt);
    m_tapeReader.push(trt);
    m_lc.log(LOG_INFO, "Created tasks for recalling a file");
    delete job;
  }
  m_jobs.clear();
  m_lc.log(LOG_INFO, recallOrderLog.str());
  LogContext::ScopedParam sp03(m_lc, Param("nbFile", m_jobs.size()));
  m_lc.log(LOG_INFO, "Finished processing batch of recall tasks from client");
}
//------------------------------------------------------------------------------
//synchronousFetch
//------------------------------------------------------------------------------
bool RecallTaskInjector::synchronousFetch()
{
  client::ClientProxy::RequestReport reqReport;  

  std::unique_ptr<castor::tape::tapegateway::RAOFilesToRecallList>
    filesToRecallList;
  uint64_t reqFiles = (m_useRAO && m_hasUDS) ? m_raoLimits.maxSupported - m_fetched : m_maxFiles;
  /* If RAO is enabled, we must ask for files up to 1PB.
   * We are limiting to 1PB because the size will be passed as
   * oracle::occi::Number which is limited to ~56 bits precision
   */
  uint64_t reqSize = (m_useRAO && m_hasUDS) ? 1024L * 1024 * 1024 * 1024 * 1024 : m_maxBytes;
  try {
    filesToRecallList.reset(m_client.getFilesToRecall(reqFiles,reqSize,reqReport));
  } catch (castor::exception::Exception & ex) {
    castor::log::ScopedParamContainer scoped(m_lc);
    scoped.add("transactionId", reqReport.transactionId)
          .add("byteSizeThreshold",reqSize)
          .add("requestedFiles", reqFiles)
          .add("message", ex.getMessageValue());
    m_lc.log(LOG_ERR, "Failed to getFilesToRecall");
    return false;
  }
  castor::log::ScopedParamContainer scoped(m_lc); 
  scoped.add("sendRecvDuration", reqReport.sendRecvDuration)
        .add("byteSizeThreshold",reqSize)
        .add("transactionId", reqReport.transactionId)
        .add("requestedFiles", reqFiles);
  if(NULL==filesToRecallList.get()) { 
    m_lc.log(LOG_ERR, "No files to recall: empty mount");
    return false;
  }
  else {
    auto aux = filesToRecallList->filesToRecall();
    for (auto it = aux.begin(); it != aux.end(); it++) {
      m_jobs.emplace_back((*it)->clone());
    }
    if (! m_useRAO) {
      injectBulkRecalls();
    }
    else {
      m_fetched = filesToRecallList->filesToRecall().size();
      LogContext::ScopedParam sp(m_lc, Param("fetchedFiles", m_fetched));
      m_lc.log(LOG_INFO,"Fetched files to recall");
    }
    return true;
  }
}
//------------------------------------------------------------------------------
//signalEndDataMovement
//------------------------------------------------------------------------------
  void RecallTaskInjector::signalEndDataMovement(){        
    //first send the end signal to the threads
    m_tapeReader.finish();
    m_diskWriter.finish();
  }
//------------------------------------------------------------------------------
//deleteAllTasks
//------------------------------------------------------------------------------
  void RecallTaskInjector::deleteAllTasks(){
    //discard all the tasks !!
    while(m_queue.size()>0){
      m_queue.pop();
    }
  }
  
//------------------------------------------------------------------------------
//WorkerThread::run
//------------------------------------------------------------------------------
void RecallTaskInjector::WorkerThread::run()
{
  using castor::log::LogContext;
  m_parent.m_lc.pushOrReplace(Param("thread", "RecallTaskInjector"));
  m_parent.m_lc.log(LOG_DEBUG, "Starting RecallTaskInjector thread");

  if (m_parent.m_useRAO) {
    /* RecallTaskInjector is waiting to have access to the drive in order
     * to perform the RAO query;
     */
    m_parent.waitForPromise();
    try {
      m_parent.m_raoLimits = m_parent.m_drive->getLimitUDS();
      m_parent.m_hasUDS = true;
      LogContext::ScopedParam sp(m_parent.m_lc, Param("maxSupportedUDS", m_parent.m_raoLimits.maxSupported));
      m_parent.m_lc.log(LOG_INFO,"Query getLimitUDS for RAO completed");
      if (m_parent.m_fetched < m_parent.m_raoLimits.maxSupported) {
        /* Fetching until we reach maxSupported for the tape drive RAO
         */
        m_parent.synchronousFetch();
      }
      m_parent.injectBulkRecalls();
      /* Disabling RAO after reordering of the first batch of files */
      m_parent.m_useRAO = false;
    }
    catch (castor::tape::SCSI::Exception& e) {
      m_parent.m_lc.log(LOG_WARNING, "The drive does not support RAO: disabled");
      m_parent.m_useRAO = false;
      m_parent.injectBulkRecalls();
    }
  }
  try{
    while (1) {
      Request req = m_parent.m_queue.pop();
      if (req.end) {
        m_parent.m_lc.log(LOG_INFO,"Received a end notification from tape thread: triggering the end of session.");
        m_parent.signalEndDataMovement();
        break;
      }
      m_parent.m_lc.log(LOG_DEBUG,"RecallJobInjector:run: about to call client interface");
      client::ClientProxy::RequestReport reqReport;
      std::unique_ptr<tapegateway::RAOFilesToRecallList> filesToRecallList(m_parent.m_client.getFilesToRecall(req.filesRequested, req.bytesRequested,reqReport));
      
      LogContext::ScopedParam sp01(m_parent.m_lc, Param("transactionId", reqReport.transactionId));
      LogContext::ScopedParam sp02(m_parent.m_lc, Param("connectDuration", reqReport.connectDuration));
      LogContext::ScopedParam sp03(m_parent.m_lc, Param("sendRecvDuration", reqReport.sendRecvDuration));
      
      if (NULL == filesToRecallList.get()) {
        if (req.lastCall) {
          m_parent.m_lc.log(LOG_INFO,"No more file to recall: triggering the end of session.");
          m_parent.signalEndDataMovement();
          break;
        } else {
          m_parent.m_lc.log(LOG_DEBUG,"In RecallJobInjector::WorkerThread::run(): got empty list, but not last call. NoOp.");
        }
      } else {
        auto aux = filesToRecallList->filesToRecall();
        for (auto it = aux.begin(); it != aux.end(); it++) {
          m_parent.m_jobs.emplace_back((*it)->clone());
        }
        m_parent.injectBulkRecalls();
      }
    } // end of while(1)
  } //end of try
  catch(const castor::exception::Exception& ex){
      //we end up there because we could not talk to the client
      log::ScopedParamContainer container( m_parent.m_lc);
      container.add("exception code",ex.code())
               .add("exception message",ex.getMessageValue());
      m_parent.m_lc.logBacktrace(LOG_ERR,ex.backtrace());
      m_parent.m_lc.log(LOG_ERR,"In RecallJobInjector::WorkerThread::run(): "
      "could not retrieve a list of file to recall. End of session");
      
      m_parent.signalEndDataMovement();
      m_parent.deleteAllTasks();
    }
  //-------------
  m_parent.m_lc.log(LOG_DEBUG, "Finishing RecallTaskInjector thread");
  /* We want to finish at the first lastCall we encounter.
   * But even after sending finish() to m_diskWriter and to m_tapeReader,
   * m_diskWriter might still want some more task (the threshold could be crossed),
   * so we discard everything that might still be in the queue
   */
  if(m_parent.m_queue.size()>0) {
    bool stillReading =true;
    while(stillReading) {
      Request req = m_parent.m_queue.pop();
      if (req.end){
        stillReading = false;
      }
      LogContext::ScopedParam sp(m_parent.m_lc, Param("lastCall", req.lastCall));
      m_parent.m_lc.log(LOG_DEBUG,"In RecallJobInjector::WorkerThread::run(): popping extra request");
    }
  }
}

} //end namespace daemon
} //end namespace tapeserver
} //end namespace tape
} //end namespace castor
