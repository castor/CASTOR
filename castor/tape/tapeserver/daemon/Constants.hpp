/******************************************************************************
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

#pragma once

#include <stdint.h>
#include <stdlib.h>
#include <time.h>
#include <signal.h>

namespace castor     {
namespace tape       {
namespace tapeserver {
namespace daemon     {

/**
 * The TCP/IP port on which the tape server daemon listens for incoming
 * connections and jobs from the VDQM server.
 */
const unsigned short TAPESERVER_JOB_PORT = 5070;

/**
 * The TCP/IP port on which the tape server daemon listens for incoming
 * connections from the tpconfig admin command.
 */
const unsigned short TAPESERVER_ADMIN_PORT = 5011;

/**
 * The TCP/IP port on which the tape server daemon listens for incoming
 * connections from the tape labeling command.
 */
const unsigned short TAPESERVER_LABEL_PORT = 54321;

/**
 * The TCP/IP port on which ZMQ sockets will bind for internal communication
 * between forked sessions and the parent tapeserverd process.
 */
const unsigned short TAPESERVER_INTERNAL_PORT = 54322;

/**
 * The compile-time default value for the maximum time in seconds that the
 * data-transfer session can take to get the transfer job from the client.
 */
const time_t TAPESERVER_WAITJOBTIMEOUT = 60; // 1 minute

/**
 * The compile-time default value for the signal to be used to kill 
 * the data-transfer session  that takes more than the maximum time in seconds
 * to get the transfer job from the client.
 */
const uint32_t TAPESERVER_WAITJOBTIMEOUTSIGNAL = SIGKILL;  

/** 
 * The compile-time default value for the maximum time in seconds that the
 * data-transfer session can take to mount a tape.
 */
const time_t TAPESERVER_MOUNTTIMEOUT = 900; // 15 minutes

/**
 * The compile-time default value for the signal to be used to kill 
 * the data-transfer session that takes more than the maximum time in seconds
 * to mount a tape.
 */
const uint32_t TAPESERVER_MOUNTTIMEOUTSIGNAL = SIGKILL; 

/**
 * The compile-time default value for the maximum time in seconds the
 * data-transfer session of tapeserverd can cease to move data blocks.
 */
const time_t TAPESERVER_BLKMOVETIMEOUT = 1800; // 30 minutes

/**
 * The compile-time default value for the signal to be used to kill 
 * the data-transfer session of tapeserverd that ceases more than the maximum 
 * time in seconds to move data blocks.
 */
const uint32_t TAPESERVER_BLKMOVETIMEOUTSIGNAL = SIGKILL; 

/**
 * The time interval in seconds to wait between attempts to keep the vdqmd
 * daemon synchronized with the state of a tape drive within the catalogue of
 * the tapeserverd daemon.
 */
const time_t TAPESERVER_VDQMDRIVESYNCINTERVAL = 600; // 10 minutes

/**
 * The compile-time default value for the number of disk threads in 
 * the thread pool serving disk accesses.
 */
const uint32_t TAPESERVER_NB_DISK_THREAD = 10;

/**
 * The compile-time default value for the memory buffers exchanged between
 * tape and disk threads.
 */
const size_t TAPESERVER_BUFSZ = 5 * 1024 * 1024;

/**
 * The compile time timeout value for the potentially DB based calls to the client.
 * As those can take time on a contended and for bulk communications, we go above 
 * the default 20 seconds.
 * This value is not configurable.
 */
const int TAPESERVER_DB_TIMEOUT = 60 * 5; // 5 minutes

/**
 * The timeout in seconds to use for Xroot functions.  Please note that a
 * value of 0 means never timeout and is therefore not recommended.
 */
const uint16_t TAPESERVER_XROOTTIMEOUT = 300;

/**
 * The default choice on xroot streaming flag
 */
const bool TAPESERVER_XROOTSTREAMING = true;

} // namespace daemon
} // namespace tapeserver
} // namespace tape
} // namespace castor
