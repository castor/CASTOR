.\" Copyright (C) 2003  CERN
.\" This program is free software; you can redistribute it and/or
.\" modify it under the terms of the GNU General Public License
.\" as published by the Free Software Foundation; either version 2
.\" of the License, or (at your option) any later version.
.\" This program is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\" You should have received a copy of the GNU General Public License
.\" along with this program; if not, write to the Free Software
.\" Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
.TH CASTORTAPELABEL "8castor" "$Date: 2014/03/24 14:44:00 $" CASTOR "CASTOR"
.SH NAME
castor-tape-label \- Pre-label a CASTOR tape
.SH SYNOPSIS
.BI "castor-tape-label -u unitname -g dgn -V vid [-h] [-f] [-d]"

.SH DESCRIPTION
\fBcastor-tape-label\fP is a command-line tool for pre-labelling a CASTOR tape.
.P

.SH OPTIONS

.TP
\fB\-u, \-\-unitname <unitname>
Explicit name of drive to be used.  This option is mandatory.
.TP
\fB\-g, \-\-dgni <dgn>
Device group name of the tape.  This option is mandatory.
.TP
\fB\-V, \-\-vid <vid>
Volume ID of the tape.  This option is mandatory.
.TP
\fB\-h, \-\-help
Print help message and exit.
.TP
\fB\-f, \-\-force
Use this option to label a non-blank tape
.TP
\fB\-l, \-\-lbp
Enable logical block protection (default disabled).
.TP
\fB\-d, \-\-debug
Debug mode on (default off).

.SH EXAMPLE

\fBcastor-tape-label\fP -u T10D6515 -g T10KD6 -V T54321 -f

.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
