/******************************************************************************
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

#pragma once

#include "castor/tape/tapegateway/FileMigratedNotificationStruct.hpp"
#include "castor/tape/tapegateway/FilesToMigrateList.hpp"
#include "castor/tape/tapegateway/FilesToMigrateListRequest.hpp"
#include "castor/tape/tapegateway/FileToMigrateStruct.hpp"
#include "castor/tape/tpcp/TpcpCommand.hpp"

#include <memory>

namespace castor {
namespace tape   {
namespace tpcp   {

/**
 * The class implementing the writetp command.
 */
class WriteTpCommand : public TpcpCommand {
public:

  /**
   * Constructor.
   */
  WriteTpCommand() throw();

  /**
   * Destructor.
   */
  virtual ~WriteTpCommand() throw();


protected:

  /**
   * Writes the command-line usage message of to the specified output stream.
   *
   * @param os Output stream to be written to.
   */
  void usage(std::ostream &os) const throw();

  /**
   * Parses the specified command-line arguments.
   *
   * @param argc Argument count from the executable's entry function: main().
   * @param argv Argument vector from the executable's entry function: main().
   */
  void parseCommandLine(const int argc, char **argv);

  /**
   * Checks the tape can be accessed.
   *
   * @throw A castor::exception::Exception exception if the tape cannot be
   *        accessed.
   */
  void checkAccessToTape() const;

  /**
   * Request a drive connected to the specified tape-server from the VDQM.
   *
   * @param tapeServer If not NULL then this parameter specifies the tape
   *                   server to be used, therefore overriding the drive
   *                   scheduling of the VDQM.
   */
  void requestDriveFromVdqm(char *const tapeServer);

  /**
   * Sends the volume message to the tape-server daemon.
   *
   * @param volumeRequest The volume rerquest message received from the
   *                      tapegatewayd daemon.
   * @param connection    The already open connection to the tape-server daemon
   *                      over which the volume message should be sent.
   */
  void sendVolumeToTapeServer(const tapegateway::VolumeRequest &volumeRequest,
    castor::io::AbstractTCPSocket &connection) const;

  /**
   * Performs the tape copy whether it be DUMP, READ or WRITE.
   */
  void performTransfer();

  /**
   * Dispatches the appropriate handler for the specified tape-gateway message.
   *
   * @param obj  The tape-gateway message for which a handler should be
   *             dispatched.
   * @param sock The socket on which to reply to the message.
   * @return     True if there is more work to be done, else false.
   */
  bool dispatchMsgHandler(castor::IObject *const obj,
    castor::io::AbstractSocket &sock);

private:

  /**
   * Data type for a map of file transaction IDs to the disk file-names of
   * files currently being transfered.
   */
  typedef std::map<uint64_t, std::string> FileTransferMap;

  /**
   * Map of file transaction IDs to the disk file-names of files currently
   * being transfered.
   */
  FileTransferMap m_pendingFileTransfers;

  /**
   * The next tape file sequence number to be used when migrating using the
   * TPPOSIT_FSEQ positioning method.  This member variable is not used with
   * the TPPOSIT_EOI positioning method.
   */
  int32_t m_nextTapeFseq;

  /**
   * The number of successfully transfered files.
   */
  uint64_t m_nbMigratedFiles;

  /**
   * FilesToMigrateListRequest message handler.
   *
   * @param obj  The tape-server message to be processed.
   * @param sock The socket on which to reply to the tape server.
   * @return     True if there is more work to be done else false.
   */
  bool handleFilesToMigrateListRequest(IObject *const obj,
    io::AbstractSocket &sock);

  /**
   * Creates a FilesToMigrateList based on the remaining work to be done and on
   * how much can be put into the current batch.
   *
   * @param rqst The request for more work from the tape server.
   * @param sock The socket on which to reply to the tape server.
   * @return     The FilesToMigrateList mesaage.
   */
  tapegateway::FilesToMigrateList createFilesToMigrateList(
    tapegateway::FilesToMigrateListRequest &rqst, io::AbstractSocket &sock);


  /**
   * Creates a FileToMigrateStruct for the specified file to be migrated,
   *
   * @param rqst     The request for more work from the tape server.
   * @param sock     The socket on which to reply to the tape server.
   * @param filename The name of the file to be migrated.
   * @return         The FileToMigrateStruct.
   */
  std::unique_ptr<tapegateway::FileToMigrateStruct> createFileToMigrateStruct(
    tapegateway::FilesToMigrateListRequest &rqst, io::AbstractSocket &sock,
    const std::string &filename);

  /**
   * FileMigrationReportList message handler.
   *
   * @param obj  The tape-server message to be processed.
   * @param sock The socket on which to reply to the tape server.
   * @return     True if there is more work to be done else false.
   */
  bool handleFileMigrationReportList(castor::IObject *const obj,
    castor::io::AbstractSocket &sock);

  /**
   * Handles the specified successful migration of files to tape.
   *
   * @param transactionId The transaction Id of the enclosing
   *                      FileMigrationReportList message.
   * @param files         The sucessful migrations to tape.
   * @param sock          The socket on which to reply to the tape server.
   */
  void handleSuccessfulMigrations(
    const uint64_t transactionId,
    const std::vector<tapegateway::FileMigratedNotificationStruct*> &files,
    castor::io::AbstractSocket &sock);

  /**
   * Handles the successfull migration of a file to tape.
   *
   * @param transactionId The transaction Id of the enclosing
   *                      FileMigrationReportList message.
   * @param file          The file that was successfully migrated to tape.
   * @param sock          The socket on which to reply to the tape server.
   */
  void handleSuccessfulMigration(
    const uint64_t transactionId,
    const tapegateway::FileMigratedNotificationStruct &file,
    castor::io::AbstractSocket &sock);

  /**
   * EndNotification message handler.
   *
   * @param obj  The tape-server message to be processed.
   * @param sock The socket on which to reply to the tape server.
   * @return     True if there is more work to be done else false.
   */
  bool handleEndNotification(castor::IObject *const obj,
    castor::io::AbstractSocket &sock);

  /**
   * EndNotificationErrorReport message handler.
   *
   * @param obj  The tape-server message to be processed.
   * @param sock The socket on which to reply to the tape server.
   * @return     True if there is more work to be done else false.
   */
  bool handleEndNotificationErrorReport(castor::IObject *const obj,
    castor::io::AbstractSocket &sock);

  /**
   * PingNotification message handler.
   *
   * @param obj  The tape-server message to be processed.
   * @param sock The socket on which to reply to the tape server.
   * @return     True if there is more work to be done else false.
   */
  bool handlePingNotification(castor::IObject *const obj,
    castor::io::AbstractSocket &sock);

}; // class WriteTpCommand

} // namespace tpcp
} // namespace tape
} // namespace castor
