/******************************************************************************
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

#pragma once

#include "castor/BaseObject.hpp"
#include "castor/exception/InvalidArgument.hpp"
#include "castor/exception/PermissionDenied.hpp"
#include "castor/io/ServerSocket.hpp"
#include "castor/tape/Constants.hpp"
#include "castor/tape/tapegateway/FileErrorReportStruct.hpp"
#include "castor/tape/tapegateway/FileRecalledNotificationStruct.hpp"
#include "castor/tape/tapegateway/VolumeRequest.hpp"
#include "castor/tape/tpcp/FilenameList.hpp"
#include "castor/tape/tpcp/Helper.hpp"
#include "castor/tape/tpcp/ReadTpBlkIdCommandLine.hpp"
#include "Castor_limits.h"
#include "serrno.h"
#include "vmgr_api.h"

#include <iostream>
#include <list>
#include <map>
#include <signal.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

namespace castor {
namespace tape   {
namespace tpcp   {

/**
 * The class implementing the readtpblkid command.
 */
class ReadTpBlkIdCommand : public castor::BaseObject {
public:

  /**
   * Constructor.
   *
   * @param programName The program name.
   */
  ReadTpBlkIdCommand(const char *const programName) throw();

  /**
   * Destructor.
   */
  ~ReadTpBlkIdCommand() throw();

  /**
   * The entry function of this command.
   *
   * @param argc The number of command-line arguments.
   * @param argv The command-line arguments.
   */
  int main(const int argc, char **argv) throw();


protected:

  /**
   * The exception throwing version of the entry function of the this command.
   *
   * @param argc The number of command-line arguments.
   * @param argv The command-line arguments.
   */
  int exceptionThrowingMain(const int argc, char **argv);

  /**
   * Parses the specified command-line arguments.
   *
   * @param argc Argument count from the executable's main().
   * @param argv Argument vector from the executable's main().
   */
  void parseCommandLine(const int argc, char **argv);

  /**
   * Writes the command-line usage message of tpcp onto the specified output
   * stream.
   *
   * @param os Output stream to be written to.
   */
  void usage(std::ostream &os) const;

  /**
   * Writes the parsed comand-line onto the specified output stream.
   *
   * @param os Output stream to be written to.
   */
  void displayCommandLine(std::ostream &os) const;

  /**
   * Performs the transfer.
   */
  void performTransfer();

  /**
   * Retrieves information about the specified tape from the VMGR.
   *
   * This method is basically a C++ wrapper around the C VMGR function
   * vmgr_querytape.  This method converts the return value of -1 and the
   * serrno to an exception in the case of an error.
   */
  void vmgrQueryTape();

  /**
   * Checks the tape can be accessed.
   *
   * @throw A castor::exception::Exception exception if the tape cannot be
   *        accessed.
   */
  void checkAccessToTape() const;

  /**
   * Creates, binds and sets to listening the callback socket to be used for
   * callbacks from the tape-bridge daemon.
   */
  void setupCallbackSock();

  /**
   * Request a drive from the VDQM to mount the specified tape for the
   * specified access mode (read or write).
   *
   * @param accessMode The tape access mode, either WRITE_DISABLE or
   *                   WRITE_ENABLE.
   * @param tapeServer If not NULL then this parameter specifies the tape
   *                   server to be used, therefore overriding the drive
   *                   scheduling of the VDQM.
   */
  void requestDriveFromVdqm(const int accessMode, char *const tapeServer);

  /**
   * Checks existence of a given request in VDQM. Throws CASTOR exception if not
   * successful.
   * 
   * @param mountTransactionId  The id of the request to check.
   */
  void pingVdqmServer(const int mountTransactionId) const;

  /**
   * Sends the volume message to the tape-server daemon.
   *
   * @param volumeRequest The volume request message received from the
   *                      tapegatewayd daemon.
   * @param connection    The already open connection to the daemon
   *                      over which the volume message should be sent.
   */
  void sendVolumeToTapeServer(
    const tapegateway::VolumeRequest &volumeRequest,
    castor::io::AbstractTCPSocket    &connection) const;

  /**
   * Waits for and accepts an incoming tape-bridge connection, reads in the
   * tape-bridge message and then dispatches it to appropriate message handler
   * member function.
   *
   * @return True if there is more work to be done, else false.
   */
  bool waitForMsgAndDispatchHandler();

  /**
   * Dispatches the appropriate handler for the specified tape-gateway message.
   *
   * @param obj  The tape-gateway message for which a handler should be
   *             dispatched.
   * @param sock The socket on which to reply to the message.
   * @return     True if there is more work to be done, else false.
   */
  bool dispatchMsgHandler(castor::IObject *const obj,
    castor::io::AbstractSocket &sock);

  /**
   * FilesToRecallListRequest message handler.
   *
   * @param obj  The tape-server message to be processed.
   * @param sock The socket on which to reply to the tape server.
   * @return     True if there is more work to be done else false.
   */
  bool handleFilesToRecallListRequest(castor::IObject *const obj,
    castor::io::AbstractSocket &sock);

  /**
   * FilesToRecallListRequest message handler that replies to the client with a
   * RASOFilesToRecallList.
   *
   * This handler effectively uses the tape-gateway protocol that targets
   * Recommended Access Order (RAO).
   *
   * @param obj  The tape-server message to be processed.
   * @param sock The socket on which to reply to the tape server.
   * @return     True if there is more work to be done else false.
   */
  bool handleFilesToRecallListRequestRAO(castor::IObject *const obj,
    castor::io::AbstractSocket &sock);

  /**
   * FilesToRecallListRequest message handler that replies to the client with a
   * FilesToRecallList.
   *
   * This handler effectively uses the tape-gateway protocol that existed before
   * the introduction of Recommended Access Order (RAO).
   *
   * @param obj  The tape-server message to be processed.
   * @param sock The socket on which to reply to the tape server.
   * @return     True if there is more work to be done else false.
   */
  bool handleFilesToRecallListRequestBeforeRAO(castor::IObject *const obj,
    castor::io::AbstractSocket &sock);

  /**
   * FileRecallReportList message handler.
   *
   * @param obj  The tape-server message to be processed.
   * @param sock The socket on which to reply to the tape server.
   * @return     True if there is more work to be done else false.
   */
  bool handleFileRecallReportList(castor::IObject *const obj,
    castor::io::AbstractSocket &sock);

  /**
   * Handles the specified successful recalls of files from tape.
   *
   * @param transactionId The transaction Id of the enclosing
   *                      FileRecallReportList message.
   * @param files         The sucessful recalls from tape.
   * @param sock          The socket on which to reply to the tape server.
   */
  void handleSuccessfulRecalls(
    const uint64_t transactionId,
    const std::vector<tapegateway::FileRecalledNotificationStruct*> &files,
    castor::io::AbstractSocket &sock);

  /**
   * Handles the successfull recall of a file from tape.
   *
   * @param transactionId The transaction Id of the enclosing
   *                      FileRecallReportList message.
   * @param file          The file that was successfully recalled from tape.
   * @param sock          The socket on which to reply to the tape server.
   */
  void handleSuccessfulRecall(
    const uint64_t transactionId,
    const tapegateway::FileRecalledNotificationStruct &file,
    castor::io::AbstractSocket &sock);

  /**
   * EndNotification message handler.
   *
   * @param obj  The tape-server message to be processed.
   * @param sock The socket on which to reply to the tape server.
   * @return     True if there is more work to be done else false.
   */
  bool handleEndNotification(castor::IObject *const obj,
    castor::io::AbstractSocket &sock);

  /**
   * EndNotificationErrorReport message handler.
   *
   * @param obj  The tape-server message to be processed.
   * @param sock The socket on which to reply to the tape server.
   * @return     True if there is more work to be done else false.
   */
  bool handleEndNotificationErrorReport(castor::IObject *const obj,
    castor::io::AbstractSocket &sock);

  /**
   * PingNotification message handler.
   *
   * @param obj  The tape-bridge message to be processed.
   * @param sock The socket on which to reply to the tape-bridge.
   * @return     True if there is more work to be done else false.
   */
  bool handlePingNotification(castor::IObject *const obj,
    castor::io::AbstractSocket &sock);

  /**
   * Handles the specified failed file-transfers.
   *
   * @param files The failed file transfers.
   */
  void handleFailedTransfers(
    const std::vector<tapegateway::FileErrorReportStruct*> &files);

  /**
   * Handles the specified failed file-transfer.
   *
   * @param file The failed file-transfer.
   */
  void handleFailedTransfer(const tapegateway::FileErrorReportStruct &file);

  /**
   * Acknowledges the end of the session to the tape-bridge.
   */
  void acknowledgeEndOfSession();

  /**
   * Convenience method for sending EndNotificationErrorReport messages to the
   * tape-bridge.
   *
   * Note that this method intentionally does not throw any exceptions.  The
   * method tries to send an EndNotificationErrorReport messages to the
   * tape-bridge and fails silently in the case it cannot.
   *
   * @param transactionId The transaction ID.
   * @param errorCode     The error code.
   * @param errorMessage  The error message.
   * @param sock          The socket on which to reply to the tape server.
   */
  void sendEndNotificationErrorReport(
    const uint64_t             transactionId,
    const int                  errorCode,
    const std::string          &errorMessage,
    castor::io::AbstractSocket &sock)
    throw();

  /**
   * Convenience method that casts the specified CASTOR framework object into
   * the specified pointer to Gateway message.
   *
   * If the cast fails then an EndNotificationErrorReport is sent to the
   * Gateway and an appropriate exception is thrown.
   *
   * @param obj  The CASTOR framework object.
   * @param msg  Out parameter. The pointer to the Gateway massage that will be
   *             set by this method.
   * @param sock The socket on which to reply to the tape-bridge with an
   *             EndNotificationErrorReport message if the cast fails.
   */
  template<class T> void castMessage(castor::IObject *const obj, T *&msg,
    castor::io::AbstractSocket &sock) throw() {
    msg = dynamic_cast<T*>(obj);

    if(msg == NULL) {
      std::stringstream oss;

      oss <<
        "Unexpected object type" <<
        ": Actual=" << Helper::objectTypeToString(obj->type()) <<
        " Expected=" << Helper::objectTypeToString(T().type());

      const uint64_t transactionId = 0; // Transaction Id unknown
      sendEndNotificationErrorReport(transactionId, SEINTERNAL,
        oss.str(), sock);

      castor::exception::Exception ex;
      ex.getMessage() <<  oss.str();
      throw ex;
    }
  }

  /**
   * Helper method that wraps the C function stat() and converts any
   * error it reports into an exception.
   *
   * @param path    The path to be passed to stat().
   * @param statBuf The stat buffer to be passed to stat().
   */
  void localStat(const char *path, struct stat &statBuf) const;


private:

  /**
   * The umask of the process.
   */
  const mode_t m_umask;

  /**
   * The program name.
   */
  const char *const m_programName;

  /**
   * Initially set to false, but set to true if a SIGNINT interrupt is received
   * (control-C).
   */
  static bool s_receivedSigint;

  /**
   * The ID of the user running the tpcp command.
   */
  const uid_t m_userId;

  /**
   * The ID of default group of the user running the tpcp command.
   */
  const gid_t m_groupId;

  /**
   * Vmgr error buffer.
   */
  static char vmgr_error_buffer[VMGRERRORBUFLEN];

  /**
   * The parsed command-line.
   */
  ReadTpBlkIdCommandLine m_cmdLine;

  /**
   * Tape information retrieved from the VMGR about the tape to be used.
   */
  vmgr_tape_info_byte_u64 m_vmgrTapeInfo;

  /**
   * The DGN of the tape to be used.
   */
  char m_dgn[CA_MAXDGNLEN + 1];

  /**
   * TCP/IP tape-bridge callback socket.
   */
  castor::io::ServerSocket m_callbackSock;

  /**
   * True if the tpcp command has got a volume request ID from the VDQM.
   */
  bool m_gotVolReqId;

  /**
   * The volume request ID returned by the VDQM as a result of requesting a
   * drive.
   */
  int32_t m_volReqId;

  /**
   * The next file transaction ID.
   */
  uint64_t m_fileTransactionId;

  /**
   * The hostname of the machine.
   */
  char m_hostname[CA_MAXHOSTNAMELEN+1];


  /**
   * The SIGINT signal handler.
   */
  static void sigintHandler(int signal);

  /**
   * The SIGINT action handler structure to be used with sigaction.
   */
  struct sigaction m_sigintAction;

  /**
   * The current working directory where tpcp command is run.
   */
  char m_cwd[CA_MAXPATHLEN+1];

  /**
   * Structure used to describe a tape session error.
   */
  struct TapeSessionError {
    int         errorCode;
    std::string errorCodeString;
    std::string errorMessage;

    TapeSessionError(): errorCode(0), errorCodeString(""),
      errorMessage("Success") {
    }
  };

  /**
   * If the tape server reported a tape session error then this member variable
   * shall contain a decription of that error.
   */
  TapeSessionError m_tapeSessionErrorReportedByTapeServer;

  /**
   * True if the tape server reported a tape session error.
   */
  bool m_tapeServerReportedATapeSessionError;

  /**
   * Iterator used to keep track of which recall should be executed next.
   */
  std::list<BlkIdRecall>::const_iterator m_recallItor;

  /**
   * The number of successfully transferred files.
   */
  uint64_t m_nbRecalledFiles;

  /**
   * Data type for a map of file transaction IDs to files currently being
   * transfered.
   */
  typedef std::map<uint64_t, BlkIdRecall> FileTransferMap;

  /**
   * Map of file transaction IDs to files currently being transferred.
   */
  FileTransferMap m_pendingFileTransfers;

  /**
   * This method prefixes the local hostname onto the beginning of local
   * file-names.
   *
   * This method prefixes the current working directory onto the
   * beginning of relative-path file-names.
   */
  void translateFilenamesIntoRemoteFilenames();

  /**
   * Displays the specified error message, cleans up (at least deletes the
   * volume-request id if there is one) and then calls exit(1) indicating an
   * error.
   */
  void displayErrorMsgCleanUpAndExit(const std::string &msg) throw();

  /**
   * Executes the main code of the command.
   *
   * The specification of this method intentionally does not have a throw()
   * clause so that any type of exception can be thrown.
   */
  void executeCommand();

  /**
   * Determines and returns the numeric code that should be returned by this
   * command-line tool.
   */
  int determineCommandLineReturnCode() const throw();

  /**
   * Deletes the specified VDQM volume request.
   */
  void deleteVdqmVolumeRequest();

}; // class ReadTpBlkIdCommand

} // namespace tpcp
} // namespace tape
} // namespace castor
