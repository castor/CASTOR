/******************************************************************************
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

#pragma once

#include "castor/tape/tpcp/BlkIdRecall.hpp"
#include "castor/utils/utils.hpp"
#include "Castor_limits.h"

#include <list>
#include <string>

namespace castor {
namespace tape   {
namespace tpcp   {

/**
 * Data type used to store the results of parsing the command-line of the
 * readtpblkid command.
 */
struct ReadTpBlkIdCommandLine {

  /**
   * True if the debug option has been set.
   */
  bool debugSet;

  /**
   * True if the help option has been set.
   */
  bool helpSet;

  /**
   * True if the tape server option has been set.
   */
  bool serverSet;

  /**
   * The tape server to be used therefore overriding the drive scheduling of
   * the VDQM.
   */
  char server[CA_MAXHOSTNAMELEN+1];

  /**
   * True if the tape-gateway protocol targeted at Recommended Access Order
   * (RAO) should be used.
   */
  bool raoSet;

  /**
   * True if the "recall list" filename option has been set.
   */
  bool recallListSet;

  /**
   * The filename of the "recall list" file.
   */
  std::string recallListFileName;

  /**
   * The VID of the tape to be mounted.
   */
  char vid[CA_MAXVIDLEN+1];

  /**
   * The list of block-id based recalls to be executed.
   */
  std::list<BlkIdRecall> recalls;

  /**
   * Constructor.
   */
  ReadTpBlkIdCommandLine() :
    debugSet(false),
    helpSet(false),
    serverSet(false),
    raoSet(false),
    recallListSet(false) {
    castor::utils::setBytes(server, '\0');
    castor::utils::setBytes(vid   , '\0');
  }
}; // struct ReadTpBlkIdCommandLine

} // namespace tpcp
} // namespace tape
} // namespace castor


