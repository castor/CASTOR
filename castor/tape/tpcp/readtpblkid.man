.\" Copyright (C) 2003  CERN
.\" This program is free software; you can redistribute it and/or
.\" modify it under the terms of the GNU General Public License
.\" as published by the Free Software Foundation; either version 2
.\" of the License, or (at your option) any later version.
.\" This program is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\" You should have received a copy of the GNU General Public License
.\" along with this program; if not, write to the Free Software
.\" Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
.TH READTP "1castor" "$Date: 2009/08/07 15:56:38 $" CASTOR "CASTOR"
.SH NAME
readtpblkid \- read one or more files from tape using block-id positioning
.SH SYNOPSIS
.BI "readtpblkid [OPTIONS] VID [fSeq fileStartBlkId fileSize destFile]..."

.SH DESCRIPTION
.B readtpblkid
reads one or more files from a tape and writes them to disk.  Tape positioning
is block-id based as opposed to using tape file sequence numbers.
.P
This command gives privileged users direct access to the tape system.
.P
The tape to be read is specified by the \fBVID\fP argument.  Each tape file
recall file is specified as a quadruple of \fB[fSeq fileStartBlkId fileSize
destFile]\fP.
.P
\fBfSeq\fP is the tape file sequence number of the file to be recalled.
\fBfSeq\fP is not used to position the tape.  It is however compared with
the tape file sequence number stored in the header file of the file to be
recalled.  If the \fBfSeq\fP specified on the command-line does not match
the value within the header file then the recall will be aborted.
.P
\fBfileStartBlkId\fP is the block ID of the first tape block of the header file
that precedes the file to be retrieved from tape.
.P
\fBfileSize\fP is the size of the file to be recalled in bytes.
.P
\fBdestFile\fP is the path of the destination disk file in the form
[host:]local_path.

.SH OPTIONS
.TP
\fB\-d, \-\-debug
Turns on the printing of debug information.
.TP
\fB\-h, \-\-help
Prints the usage message.
.TP
\fB\-s, \-\-server server
Specifies the tape server to be used, therefore overriding the drive scheduling
of the VDQM.  The name of the server must match exactly the name given by the
VDQM showqueues command.  For example if showqueues displays the tape server
name 'tpsrv123' then specifiying '-s tpsrv123.cern.ch' will not work and the
VDQM request will remain in the VDQM until it is deleted (this command will
delete the request when it receives Ctrl-C).  If this option is not set then
the VDQM will allocate the first free compatible drive.
.TP
\fB\-r, \-\-rao
Use the tape-gateway protocol targeted at Recommended Access Order (RAO).
.TP
\fB-l, \-\-recallList file
A comma separated value file containing a list of recalls.  Each line of the
file represents one recall and is of the form:
\fBfSeq,fileStartBlkId,fileSize,destFile

.SH "RETURN CODES"
.TP
\fB 0
Ok.
.TP
\fB 1
Command failed.

.SH CASTOR CONFIGURATION PARAMETERS
The \fBreadtpblkid\fP command reads and uses the following CASTOR configuration
parameters which are specified using environment variables or within the CASTOR
configuration file (the default location is /etc/castor/castor.conf).
.TP
\fBTAPESERVERCLIENT LOWPORT
The inclusive low port of the tapeserver client callback port number range.
The default is 30201.
.TP
\fBTAPESERVERCLIENT HIGHPORT
The inclusive high port of the tapeserver client callback port number range.
The default is 30300.
.TP
\fBVDQM HOST
The name of the host on which the VDQM daemon is running.
.TP
\fBVMGR HOST
The name of the host on which the VMGR daemon is running.

.SH EXAMPLE
The following two examples do the same thing.  They both read the first file of
a tape and write it to the local filesystem of the tape server.  They are
therefore useful when a tape operator or developer wishes to read a tape file
without using a remote transfer protocol.

.P
.B runuser --shell='/bin/bash' --session-command='readtpblkid -s tpsrvXXX V12345 1 1 1225 file:///tmp/1 ' stage

.P
.B runuser --shell='/bin/bash' --session-command='readtpblkid -s tpsrvXXX V12345 1 1 1225 rfio://localhost:////tmp/1 ' stage
.P
The following 3 line example shows how to create a file of comma separated
values representing file start positions and file sizes from \fBnslisttape\fP
and then pass that file on to \fBreadtpblkid\fP:
.P
.B nslisttape -V I57530 | head | sort -R > nslisttape_I57530.txt
.br
.B awk '{$6=strtonum("0x" $6); print $5 "," $6 "," $7 ",file:///dev/null"}' nslisttape_I57530.txt > readtpblkid_I57530.csv
.br
.B runuser --shell='/bin/bash' --session-command='readtpblkid --rao -s tpsrv100 -l ./readtpblkid_I57530.csv I57530’ stage

.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
