/******************************************************************************
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

#pragma once

#include <stdint.h>
#include <string>

namespace castor {
namespace tape   {
namespace tpcp   {

/**
 * Data type used to describe a block-id based recall.
 */
struct BlkIdRecall {

  /**
   * The tape file sewunce number.
   */
  int fseq;

  /**
   * The block ID of the first tape block of the header file that precedes the
   * file to be retrieved from tape.
   */
  uint64_t startBlkId;

  /**
   * The size of the file in bytes.
   */
  uint64_t fileSize;

  /**
   * The path of the destination disk file.
   */
  std::string destDiskFile;

  /**
   * Constructor.
   */
  BlkIdRecall() :
    fseq(0),
    startBlkId(0),
    fileSize(0) {
  }
}; // struct BlkIdRecall

} // namespace tpcp
} // namespace tape
} // namespace castor


