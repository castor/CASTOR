/******************************************************************************
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/
 
#include "castor/common/CastorConfiguration.hpp"
#include "castor/Constants.hpp"
#include "castor/exception/InvalidArgument.hpp"
#include "castor/io/io.hpp"
#include "castor/PortNumbers.hpp"
#include "castor/System.hpp"
#include "castor/tape/tapegateway/EndNotification.hpp"
#include "castor/tape/tapegateway/EndNotificationErrorReport.hpp"
#include "castor/tape/tapegateway/FileRecallReportList.hpp"
#include "castor/tape/tapegateway/FilesToRecallList.hpp"
#include "castor/tape/tapegateway/FilesToRecallListRequest.hpp"
#include "castor/tape/tapegateway/FileToRecallStruct.hpp"
#include "castor/tape/tapegateway/NoMoreFiles.hpp"
#include "castor/tape/tapegateway/NotificationAcknowledge.hpp"
#include "castor/tape/tapegateway/PingNotification.hpp"
#include "castor/tape/tapegateway/RAOFilesToRecallList.hpp"
#include "castor/tape/tapegateway/RAOFileToRecallStruct.hpp"
#include "castor/tape/tapegateway/Volume.hpp"
#include "castor/tape/tapegateway/VolumeMode.hpp"
#include "castor/tape/tapegateway/VolumeRequest.hpp"
#include "castor/tape/tpcp/Constants.hpp"
#include "castor/tape/tpcp/Helper.hpp"
#include "castor/tape/tpcp/ReadTpBlkIdCommand.hpp"
#include "castor/tape/tpcp/BlkIdRecallsFileParser.hpp"
#include "castor/utils/utils.hpp"
#include "Cgetopt.h"
#include "common.h"
#include "Ctape_constants.h"
#include "Cupv_api.h"
#include "Cupv_constants.h"
#include "serrno.h"
#include "vdqm_api.h"
#include "vmgr_api.h"

#include <ctype.h>
#include <exception>
#include <fstream>
#include <getopt.h>
#include <iostream>
#include <list>
#include <stdint.h>
#include <sstream>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/unistd.h>
#include <time.h>
#include <unistd.h>
#include <poll.h>
 
#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <memory>

//------------------------------------------------------------------------------
// vmgr_error_buffer
//------------------------------------------------------------------------------
char castor::tape::tpcp::ReadTpBlkIdCommand::vmgr_error_buffer[VMGRERRORBUFLEN];


//------------------------------------------------------------------------------
// s_receivedSigint
//------------------------------------------------------------------------------
bool castor::tape::tpcp::ReadTpBlkIdCommand::s_receivedSigint = false;


//------------------------------------------------------------------------------
// ostream << operator for vmgr_tape_info_byte_u64 
//------------------------------------------------------------------------------
static std::ostream &operator<<(std::ostream &os,
  const vmgr_tape_info_byte_u64 &value) {

  os << "{"
        "vid=\""                        << value.vid                 << "\","
        "vsn=\""                        << value.vsn                 << "\","
        "library=\""                    << value.library             << "\","
        "density=\""                    << value.density             << "\","
        "lbltype=\""                    << value.lbltype             << "\","
        "model=\""                      << value.model               << "\","
        "media_letter=\""               << value.media_letter        << "\","
        "manufacturer=\""               << value.manufacturer        << "\","
        "sn= \""                        << value.sn                  << "\","
        "nbsides="                      << value.nbsides             <<   ","
        "etime="                        << value.etime               <<   ","
        "rcount="                       << value.rcount              <<   ","
        "wcount="                       << value.wcount              <<   ","
        "rhost=\""                      << value.rhost               << "\","
        "whost=\""                      << value.whost               << "\","
        "rjid="                         << value.rjid                <<   ","
        "wjid="                         << value.wjid                <<   ","
        "rtime="                        << value.rtime               <<   ","
        "wtime="                        << value.wtime               <<   ","
        "side="                         << value.side                <<   ","
        "poolname=\""                   << value.poolname            << "\","
        "status="                       << value.status              <<   ","
        "estimated_free_space_byte_u64="<< value.estimated_free_space_byte_u64
          <<   ","
        "nbfiles="                      << value.nbfiles
     << "}";

  return os;
}


//------------------------------------------------------------------------------
// constructor
//------------------------------------------------------------------------------
castor::tape::tpcp::ReadTpBlkIdCommand::ReadTpBlkIdCommand(const char *const programName)
  throw() :
  m_umask(umask(0)),
  m_programName(programName),
  m_userId(getuid()),
  m_groupId(getgid()),
  m_callbackSock(false),
  m_gotVolReqId(false),
  m_volReqId(0),
  m_fileTransactionId(1),
  m_tapeServerReportedATapeSessionError(false),
  m_nbRecalledFiles(0) {

  // Restore original umask
  umask(m_umask);

  memset(m_hostname, 0, sizeof(m_hostname));
  memset(m_cwd, 0, sizeof(m_cwd));

  // Prepare the SIGINT action handler structure ready for sigaction()
  memset(&m_sigintAction, 0, sizeof(m_sigintAction));   // No flags
  m_sigintAction.sa_handler = &sigintHandler;
  if(sigfillset(&m_sigintAction.sa_mask) < 0) { // Mask all signals
    const int savedErrno = errno;
    castor::exception::Exception ex;
    ex.getMessage() <<
      "Failed to initialize signal mask using sigfillset"
      ": " << sstrerror(savedErrno);
    throw ex;
  }

  // Set the SIGINT signal handler
  if(sigaction(SIGINT, &m_sigintAction, 0) < 0) {
    const int savedErrno = errno;
    castor::exception::Exception ex;
    ex.getMessage() <<
      "Failed to set the SIGINT signal handler using sigaction"
      ": " << sstrerror(savedErrno);
    throw ex;
  }

  castor::utils::setBytes(m_vmgrTapeInfo, '\0');
  castor::utils::setBytes(m_dgn, '\0');
}


//------------------------------------------------------------------------------
// destructor
//------------------------------------------------------------------------------
castor::tape::tpcp::ReadTpBlkIdCommand::~ReadTpBlkIdCommand() throw() {
  // Do nothing
}

//------------------------------------------------------------------------------
// main
//------------------------------------------------------------------------------
int castor::tape::tpcp::ReadTpBlkIdCommand::main(const int argc, char **argv) throw() {
  try {
    return exceptionThrowingMain(argc, argv);
  } catch(castor::exception::Exception &ex) {
    displayErrorMsgCleanUpAndExit(ex.getMessage().str());
  } catch(std::exception &se) {
    displayErrorMsgCleanUpAndExit(se.what());
  } catch(...) {
    displayErrorMsgCleanUpAndExit("Caught unknown exception");
  }

  // Reaching this statement means an error occurred
  return 1;
}


//------------------------------------------------------------------------------
// main
//------------------------------------------------------------------------------
int castor::tape::tpcp::ReadTpBlkIdCommand::exceptionThrowingMain(const int argc, char **argv) {

  // Parse the command line
  try {
    parseCommandLine(argc, argv);
  } catch (castor::exception::InvalidArgument &ex) {
    std::cerr
      << std::endl
      << "Failed to parse the command-line:\n\n"
      << ex.getMessage().str() << std::endl
      << std::endl;
    usage(std::cerr);
    std::cerr << std::endl;
    return 1;
  }

  // If debug, then display parsed command-line arguments
  if(m_cmdLine.debugSet) {
    displayCommandLine(std::cout);
  }

  // Display usage message and exit if help option found on command-line
  if(m_cmdLine.helpSet) {
    std::cout << std::endl;
    usage(std::cout);
    std::cout << std::endl;
    return 0;
  }

  // Execute the command
  executeCommand();

  return determineCommandLineReturnCode();
}


//------------------------------------------------------------------------------
// parseCommandLine
//------------------------------------------------------------------------------
void castor::tape::tpcp::ReadTpBlkIdCommand::parseCommandLine(const int argc,
  char **argv)  {

  static struct option longopts[] = {
    {"debug"     ,       no_argument, NULL, 'd'},
    {"help"      ,       no_argument, NULL, 'h'},
    {"server"    , required_argument, NULL, 's'},
    {"rao"       ,       no_argument, NULL, 'r'},
    {"recallList", required_argument, NULL, 'l'},
    {NULL        ,                 0, NULL,  0 }
  };

  optind = 1;
  opterr = 0;

  char c;

  while((c = getopt_long(argc, argv, ":dhs:rl:", longopts, NULL)) != -1) {

    switch (c) {
    case 'd':
      m_cmdLine.debugSet = true;
      break;

    case 'h':
      m_cmdLine.helpSet = true;
      break;

    case 's':
      m_cmdLine.serverSet = true;
      try {
        castor::utils::copyString(m_cmdLine.server, optarg);
      } catch(castor::exception::Exception &ne) {
        castor::exception::Exception ex;
        ex.getMessage() <<
          "Failed to copy the argument of the server command-line option"
          " into the internal data structures"
          ": " << ne.getMessage().str();
        throw ex;
      }
      break;

    case 'r':
      m_cmdLine.raoSet = true;
      break;

    case 'l':
      m_cmdLine.recallListSet = true;
      if(NULL == optarg) {
        castor::exception::Exception ex;
        ex.getMessage() <<
          "Failed to obtain the argument of the recallList command-line option"
            ": optarg is NULL";
        throw ex;
      }
      m_cmdLine.recallListFileName = optarg;
      break;

    case ':':
      {
        castor::exception::InvalidArgument ex;
        ex.getMessage() << "\tThe -" << (char)optopt
          << " option requires a parameter";
        throw ex;
      }
      break;

    case '?':
      {
        castor::exception::InvalidArgument ex;

        if(optopt == 0) {
          ex.getMessage() << "\tUnknown command-line option";
        } else {
          ex.getMessage() << "\tUnknown command-line option: -" << (char)optopt;
        }
        throw ex;
      }
      break;

    default:
      {
        castor::exception::Exception ex;
        ex.getMessage() <<
          "\tgetopt_long returned the following unknown value: 0x" <<
          std::hex << (int)c;
        throw ex;
      }
    } // switch (c)
  } // while ((c = getopt_long(argc, argv, "h", longopts, NULL)) != -1)

  // There is no need to continue parsing when the help option is set
  if( m_cmdLine.helpSet) {
    return;
  }

  // Calculate the number of non-option ARGV-elements
  const int nbArgs = argc-optind;

  // Check the VID has been specified
  if(nbArgs < 1) {
    castor::exception::InvalidArgument ex;

    ex.getMessage() <<
      "\tThe VID has not been specified";

    throw ex;
  }

  // Parse the VID command-line argument
  try {
    castor::utils::copyString(m_cmdLine.vid, argv[optind]);
  } catch(castor::exception::Exception &ne) {
    castor::exception::Exception ex;
    ex.getMessage() <<
      "Failed to copy VID comand-line argument into the internal data"
      " structures"
      ": " << ne.getMessage().str();
    throw ex;
  }

  const int nbRawRecallArgs = nbArgs - 1;

  if(m_cmdLine.recallListSet && 0 != nbRawRecallArgs) {
    castor::exception::InvalidArgument ex;

    ex.getMessage() <<
      "\t[fSeq fileStartBlkId fileSize destFile].. command-line arguments\n"
      "\tand the -l/--recallList option are mutually exclusive";
    throw ex;
  }

  if(m_cmdLine.recallListSet) {
    m_cmdLine.recalls =
      BlkIdRecallsFileParser::parseFile(m_cmdLine.recallListFileName);
  }

  if(!m_cmdLine.recallListSet) {
    if(0 != nbRawRecallArgs % 4) {
      castor::exception::InvalidArgument ex;

      ex.getMessage() <<
        "\tThe arguments composing the list of recalls to be executed must be\n"
        "\ta multiple of four because each recall is described by:\n"
        "\t\t1. The tape file sequence number.\n"
        "\t\t2. The start of the tape file as a block ID\n"
        "\t\t3. The size of the file in bytes.\n"
        "\t\t4. The path of the destination disk file.";

      throw ex;
    }

    // Move on to the next command-line argument
    optind++;

    const int nbFilesToBeRecalled = nbRawRecallArgs / 4;
    for(int recallFileIndex = 0; recallFileIndex < nbFilesToBeRecalled;
      recallFileIndex++) {
      BlkIdRecall recall;

      {
        std::stringstream fseq;
        fseq << argv[optind++];
        if (!utils::isValidUInt(fseq.str())) {
          castor::exception::InvalidArgument ex;
          ex.getMessage() << "\tFseq " << fseq.str() <<
            " is not a valid unsigned integer";
          throw ex;
        }
        fseq >> recall.fseq;
      }

      {
        std::stringstream startBlkId;
        startBlkId << argv[optind++];
        if (!utils::isValidUInt(startBlkId.str())) {
          castor::exception::InvalidArgument ex;
          ex.getMessage() << "\tStart block ID " << startBlkId.str() <<
            " is not a valid unsigned integer";
          throw ex;
        }
        startBlkId >> recall.startBlkId;
      }

      {
        std::stringstream fileSize;
        fileSize << argv[optind++];
        if (!utils::isValidUInt(fileSize.str())) {
          castor::exception::InvalidArgument ex;
          ex.getMessage() << "\tFile size " << fileSize.str() <<
            " is not a valid unsigned integer";
          throw ex;
        }
        fileSize >> recall.fileSize;
      }

      recall.destDiskFile = argv[optind++];

      m_cmdLine.recalls.push_back(recall);
    }
  } // if(!m_cmdLine.recallListSet)

  if(m_cmdLine.recalls.empty()) {
    castor::exception::InvalidArgument ex;

    ex.getMessage() << "\tThere must be at least one file to be recalled";

    throw ex;
  }
}


//------------------------------------------------------------------------------
// usage
//------------------------------------------------------------------------------
void castor::tape::tpcp::ReadTpBlkIdCommand::usage(std::ostream &os) const {
  os <<
    "Usage:\n"
    "\t" << m_programName <<
      " [OPTIONS] VID [fSeq fileStartBlkId fileSize destFile]...\n"
    "\n"
    "Where:\n"
    "\n"
    "\tVID            The VID of the tape to be read from.\n"
    "\tfSeq           The tape file sequence number of the file to be\n"
    "\t               recalled.  fSeq is not used to position the tape.  It\n"
    "\t               is however compared with the tape file sequence number\n"
    "\t               stored in the header file of the file to be recalled.\n"
    "\t               If the fSeq specified on the command-line does not\n"
    "\t               match the value within the header file then the recall\n"
    "\t               will be aborted.\n"
    "\tfileStartBlkId The block ID of the first tape block of the header file\n"
    "\t               that precedes the file to be retrieved from tape.\n"
    "\tfileSize       The size of the file to be recalled in bytes.\n"
    "\tdestFile       The path of the destination disk file in the form\n"
    "\t               [host:]local_path.\n"
    "\n"
    "Options:\n"
    "\n"
    "\t-d, --debug           Turn on the printing of debug information.\n"
    "\t-h, --help            Print this help messgae and exit.\n"
    "\t-s, --server server   Specifies the tape server to be used, therefore\n"
    "\t                      overriding the drive scheduling of the VDQM.\n"
    "\t-r, --rao             Use the tape-gateway protocol targeted at\n"
    "\t                      Recommended Access Order (RAO).\n"
    "\t-l, --recallList file A comma separated value file containing a list\n"
    "\t                      of recalls.  Each line of the file represents\n"
    "\t                      one recall and is of the form:\n"
    "\t                          fSeq,fileStartBlkId,fileSize,destFile\n"
    "\n"
    "Comments to: Castor.Support@cern.ch" << std::endl;
}


//------------------------------------------------------------------------------
// displayCommandLine
//------------------------------------------------------------------------------
void castor::tape::tpcp::ReadTpBlkIdCommand::displayCommandLine(std::ostream &os)
  const {
  os << "Parsed command-line:" << std::endl <<
    "\tdebugSet=" << (m_cmdLine.debugSet ? "true" : "false") << std::endl <<
    "\thelpSet=" << (m_cmdLine.helpSet ? "true" : "false") << std::endl <<
    "\tserver=" << m_cmdLine.server << std::endl <<
    "\tVID=" << m_cmdLine.vid << std::endl;

  for(std::list<BlkIdRecall>::const_iterator itor = m_cmdLine.recalls.begin();
    itor != m_cmdLine.recalls.end(); itor++) {
    os << "\trecall(fseq=" << itor->fseq << ",startBlkId=" << itor->startBlkId
      << ",fileSize=" << itor->fileSize << ",destDiskFile=" <<
      itor->destDiskFile << ")" << std::endl;
  }
}


//------------------------------------------------------------------------------
// determineCommandlineReturnCode
//------------------------------------------------------------------------------
int castor::tape::tpcp::ReadTpBlkIdCommand::determineCommandLineReturnCode()
  const throw() {
  if(m_tapeServerReportedATapeSessionError) {
    if(ENOSPC == m_tapeSessionErrorReportedByTapeServer.errorCode) {
      return ENOSPC; // Reached the physical end of tape
    } else {
      return 1; // Error other than reaching the physical end of tape
    }
  } else {
    return 0; // Success
  }
}


//------------------------------------------------------------------------------
// displayErrorMsgCleanUpAndExit
//------------------------------------------------------------------------------
void castor::tape::tpcp::ReadTpBlkIdCommand::displayErrorMsgCleanUpAndExit(
  const std::string &msg) throw() {

  // Display error message
  {
    const time_t now = time(NULL);
    castor::utils::writeTime(std::cerr, now, TIMEFORMAT);
    std::cerr <<
      " " << m_programName << " failed"
      ": " << msg << std::endl;
  }

  // Clean up
  if(m_gotVolReqId) {
    const time_t now = time(NULL);
    castor::utils::writeTime(std::cerr, now, TIMEFORMAT);
    std::cerr <<
      " Deleting volume request with ID = " << m_volReqId << std::endl;

    try {
      deleteVdqmVolumeRequest();
    } catch(castor::exception::Exception &ex) {

      const time_t now = time(NULL);
  
      castor::utils::writeTime(std::cerr, now, TIMEFORMAT);
      std::cerr << " Failed to delete VDQM volume request: "
         << ex.getMessage().str() << std::endl;
    }
  }

  exit(1); // Error
}


//------------------------------------------------------------------------------
// executeCommand
//------------------------------------------------------------------------------
void castor::tape::tpcp::ReadTpBlkIdCommand::executeCommand() {

  // Get the local hostname
  gethostname(m_hostname, sizeof(m_hostname));    

  // Check if the hostname of the machine is set to "localhost"
  if(strcmp(m_hostname, "localhost") == 0) {
    castor::exception::Exception ex(ECANCELED);
    ex.getMessage() <<
      m_programName << " cannot be ran on a machine where hostname is set to "
      "\"localhost\"";
    throw ex;
  }
    
  // Get the Current Working Directory
  if(NULL == getcwd(m_cwd, sizeof(m_cwd))) {
    castor::exception::Exception ex(ECANCELED);
    ex.getMessage() << "Failed to determine the current working directory";
    throw ex;
  }

  // This command cannot be ran as root
  if(m_userId == 0 && m_groupId == 0) {
    castor::exception::Exception ex(ECANCELED);
    ex.getMessage() <<
      m_programName << " cannot be ran as root";
    throw ex;
  }

  // Set the VMGR error buffer so that the VMGR does not write errors to
  // stderr
  vmgr_error_buffer[0] = '\0';
  if(vmgr_seterrbuf(vmgr_error_buffer,sizeof(vmgr_error_buffer)) != 0) {
    castor::exception::Exception ex(ECANCELED);
    ex.getMessage() <<
      "Failed to set VMGR error buffer";
    throw ex;
  }

  translateFilenamesIntoRemoteFilenames();

  // Get information about the tape to be used from the VMGR
  vmgrQueryTape();

  // If debug, then display the tape information retrieved from the VMGR
  if(m_cmdLine.debugSet) {
    std::ostream &os = std::cout;

    os << "vmgr_tape_info_byte_u64 from the VMGR = " <<
      m_vmgrTapeInfo << std::endl;

    os << "DGN from the VMGR =\"" << m_dgn << "\"" << std::endl;
  }

  // Check that the VID returned in the VMGR tape information matches that of
  // the requested tape
  if(strcmp(m_cmdLine.vid, m_vmgrTapeInfo.vid) != 0) {
     castor::exception::Exception ex(ECANCELED);
     std::ostream &os = ex.getMessage();

     os <<
       "VID in tape information retrieved from VMGR does not match that of "
       "the requested tape"
       ": Request VID = " << m_cmdLine.vid <<
       " VID returned from VMGR = " << m_vmgrTapeInfo.vid;

     throw ex;
  }

  checkAccessToTape();

  // Setup the tape-server callback-socket
  setupCallbackSock();

  // If debug, then display a textual description of the callback socket
  if(m_cmdLine.debugSet) {
    std::ostream &os = std::cout;

    os << "Tape-server callback socket-details = ";
    io::writeSockDescription(os, m_callbackSock.socket());
    os << std::endl;
  }

  // Send the request for a drive to the VDQM
  {
    char *const tapeServer = m_cmdLine.serverSet ? m_cmdLine.server : NULL;
    requestDriveFromVdqm(WRITE_DISABLE, tapeServer);
  }

  // Command-line user feedback
  {
    std::ostream &os = std::cout;
    time_t       now = time(NULL);

    castor::utils::writeTime(os, now, TIMEFORMAT);
    os << " Waiting for a drive: Volume request ID = " << m_volReqId
       << std::endl;
  }

  // Socket file descriptor for a callback connection from the tape server
  int connectionSockFd = 0;

  // Wait for a callback connection from the tape server
  {
    bool   waitForCallback = true;
    time_t timeout         = WAITCALLBACKTIMEOUT;
    while(waitForCallback) {
      try {
        pingVdqmServer(m_volReqId);
      } catch (castor::exception::Exception& e) {
        
        castor::exception::Exception ex(ECANCELED);
        ex.getMessage() << e.getMessage().str() << ": " << sstrerror(e.code());
        throw ex;
      }
      try {
        connectionSockFd = io::acceptConnection(m_callbackSock.socket(),
                                                 timeout);

        waitForCallback = false;
      } catch(castor::exception::TimeOut &tx) {

        // Command-line user feedback
        std::ostream &os = std::cout;
        time_t       now = time(NULL);

        castor::utils::writeTime(os, now, TIMEFORMAT);
        os <<
          " Waited " << WAITCALLBACKTIMEOUT << " seconds.  "
          "Continuing to wait." <<  std::endl;

        // Wait again for the default timeout
        timeout = WAITCALLBACKTIMEOUT;
        
      } catch(castor::exception::AcceptConnectionInterrupted &ix) {

        // If a SIGINT signal was received (control-c)
        if(s_receivedSigint) {

          castor::exception::Exception ex(ECANCELED);
          ex.getMessage() << "Received SIGNINT";

          throw ex;

        // Else received a signal other than SIGINT
        } else {

          // Wait again for the remaining amount of time
          timeout = ix.remainingTime();
        }
      }
    }
  }

  // Command-line user feedback
  {
    std::ostream &os = std::cout;
    time_t       now = time(NULL);

    castor::utils::writeTime(os, now, TIMEFORMAT);
    os << " Selected tape server is ";

    const std::string hostName = io::getPeerHostName(connectionSockFd);

    os << hostName << std::endl;
  }

  // Wrap the connection socket descriptor in a CASTOR framework socket in
  // order to get access to the framework marshalling and un-marshalling
  // methods
  castor::io::AbstractTCPSocket callbackConnectionSock(connectionSockFd);

  // Read in the object sent by the tape server
  std::unique_ptr<castor::IObject> firstObj(callbackConnectionSock.readObject());

  switch (firstObj->type()) {
  case OBJ_VolumeRequest:
    // Do nothing as this is the type of message we expect in the
    // good-day scenario
    break;

  case OBJ_EndNotificationErrorReport:
    {
      // Cast the object to its type
      tapegateway::EndNotificationErrorReport &endNotificationErrorReport =
        dynamic_cast<tapegateway::EndNotificationErrorReport&>(
          *(firstObj.get()));

      castor::exception::Exception ex(ECANCELED);
      ex.getMessage() <<
        "Tape-bridge reported an error"
        ": " << endNotificationErrorReport.errorMessage();
      throw ex;
    }
  default:
    {
      castor::exception::Exception ex(ECANCELED);
      ex.getMessage() <<
        "Received the wrong type of object from the tape server" <<
        ": Actual=" << Helper::objectTypeToString(firstObj->type()) <<
        " Expected=VolumeRequest or EndNotificationErrorReport";
      throw ex;
    }
  } //switch (firstObj->type())

  // Cast the object to its type
  tapegateway::VolumeRequest &volumeRequest =
    dynamic_cast<tapegateway::VolumeRequest&>(*(firstObj.get()));

  Helper::displayRcvdMsgIfDebug(volumeRequest, m_cmdLine.debugSet);

  // Check the volume request ID of the VolumeRequest object matches that of
  // the reply from the VDQM when the drive was requested
  if(volumeRequest.mountTransactionId() != (uint64_t)m_volReqId) {
    castor::exception::InvalidArgument ex;

    ex.getMessage()
      << "Received the wrong mount transaction ID from the tape server"
      << ": Actual=" << volumeRequest.mountTransactionId()
      << " Expected=" <<  m_volReqId;

    throw ex;
  }

  sendVolumeToTapeServer(volumeRequest, callbackConnectionSock);

  // Close the connection to the tape server
  callbackConnectionSock.close();

  performTransfer();
}


//------------------------------------------------------------------------------
// vmgrQueryTape
//------------------------------------------------------------------------------
void castor::tape::tpcp::ReadTpBlkIdCommand::vmgrQueryTape() {
  const int side = 0;
  serrno=0;
  const int rc = vmgr_querytape_byte_u64(m_cmdLine.vid, side, &m_vmgrTapeInfo,
    m_dgn);
  const int savedSerrno = serrno;

  if(0 != rc) {
    castor::exception::Exception ex;
    ex.getMessage() <<
      "Failed call to vmgr_querytape()"
      ": VID = " << m_cmdLine.vid <<
      ": " << sstrerror(savedSerrno);
    throw ex;
  }
}

//------------------------------------------------------------------------------
// checkAccessToTape
//------------------------------------------------------------------------------
void castor::tape::tpcp::ReadTpBlkIdCommand::checkAccessToTape()
  const  {
  const bool userIsTapeOperator =
    Cupv_check(m_userId, m_groupId, m_hostname, "TAPE_SERVERS",
      P_TAPE_OPERATOR) == 0 ||
    Cupv_check(m_userId, m_groupId, m_hostname, NULL          ,
      P_TAPE_OPERATOR) == 0;

  // Only tape-operators can read disabled tapes
  if(m_vmgrTapeInfo.status & DISABLED) {
    if(!userIsTapeOperator) {
      castor::exception::Exception ex(ECANCELED);
      std::ostream &os = ex.getMessage();
      os << "Tape is not available for reading"
        ": Tape is DISABLED and user is not a tape-operator";
      throw ex;
    }
  }

  if(m_vmgrTapeInfo.status & EXPORTED ||
    m_vmgrTapeInfo.status & ARCHIVED) {
    castor::exception::Exception ex(ECANCELED);
    std::ostream &os = ex.getMessage();
    os << "Tape is not available for reading"
      ": Tape is";
    if(m_vmgrTapeInfo.status & EXPORTED) os << " EXPORTED";
    if(m_vmgrTapeInfo.status & ARCHIVED) os << " ARCHIVED";
    throw ex;
  }
}


//------------------------------------------------------------------------------
// setupCallbackSock
//------------------------------------------------------------------------------
void castor::tape::tpcp::ReadTpBlkIdCommand::setupCallbackSock() {
  common::CastorConfiguration &castorConf =
    common::CastorConfiguration::getConfig();

  const unsigned short lowPort = castorConf.getConfEntInt(
    "TAPESERVERCLIENT", "LOWPORT", (unsigned short)TAPESERVERCLIENT_LOWPORT);
  const unsigned short highPort = castorConf.getConfEntInt(
    "TAPESERVERCLIENT", "HIGHPORT", (unsigned short)TAPESERVERCLIENT_HIGHPORT);

  // Bind the tape-server callback-socket
  m_callbackSock.bind(lowPort, highPort);
  m_callbackSock.listen();
}

//------------------------------------------------------------------------------
// requestDriveFromVdqm 
//------------------------------------------------------------------------------
void castor::tape::tpcp::ReadTpBlkIdCommand::requestDriveFromVdqm(
  const int accessMode, char *const tapeServer) {

  // Get the port number and IP of the callback port
  unsigned short port = 0;
  unsigned long  ip   = 0;
  m_callbackSock.getPortIp(port, ip);

  int      rc          = 0;
  int      savedSerrno  = 0;
  vdqmnw_t *nw         = NULL;
  int      *reqID      = &m_volReqId;
  char     *VID        = m_cmdLine.vid;
  char     *dgn        = m_dgn;
  char     *server     = tapeServer;
  char     *unit       = NULL;
  int      mode        = accessMode;
  int      client_port = port;

  // Connect to the VDQM
  rc = vdqm_Connect(&nw);
  savedSerrno = serrno;

  // If successfully connected
  if(rc != -1) {

    // Ask the VDQM to create a request
    rc = vdqm_CreateRequest(nw, reqID, VID, dgn, server, unit, mode,
      client_port);
    savedSerrno = serrno;

    // If the request was sucessfully created
    if(rc != -1) {

      // Ask the VDQM to queue the newly created request
      rc = vdqm_QueueRequest(nw);
      savedSerrno = serrno;
    }

    // If the request was successfully queued
    if(rc != -1) {

      // Record the fact
      m_gotVolReqId = true;
    }

    // Disconnect from the VDQM
    rc = vdqm_Disconnect(&nw);
    savedSerrno = serrno;
  }

  // Throw an exception if there was an error
  if(rc == -1) {
    castor::exception::Exception ex(savedSerrno);

    ex.getMessage() << sstrerror(savedSerrno);

    throw ex;
  }
}

//------------------------------------------------------------------------------
// pingVdqmServer
//------------------------------------------------------------------------------
void castor::tape::tpcp::ReadTpBlkIdCommand::pingVdqmServer (
  const int mountTransactionId) const {
  serrno=0;
  const int rc = vdqm_PingServer(NULL,NULL,mountTransactionId); // dgn is not given
  if (rc < 0) {
    if (EPERM == serrno) {
      // converts EPERM replay to the correct one which we should recieve from
      // the VDQM server - EVQNOSVOL "Specified vol. req. not found"
      castor::exception::Exception ex(EVQNOSVOL);
      ex.getMessage()
        << "Ping VDQM server for volume request with ID = "
        << mountTransactionId << " failed";
      throw ex;
    } else {
      castor::exception::Exception ex(serrno);
      ex.getMessage()
        << "Ping VDQM server for volume request with ID = "
        << mountTransactionId << " failed";
      throw ex;
    }
  }
}


//------------------------------------------------------------------------------
// sendVolumeToTapeServer
//------------------------------------------------------------------------------
void castor::tape::tpcp::ReadTpBlkIdCommand::sendVolumeToTapeServer(
  const tapegateway::VolumeRequest &volumeRequest,
  castor::io::AbstractTCPSocket &connection) const {
  castor::tape::tapegateway::Volume volumeMsg;
  volumeMsg.setVid(m_vmgrTapeInfo.vid);
  volumeMsg.setClientType(castor::tape::tapegateway::READ_TP);
  volumeMsg.setMode(castor::tape::tapegateway::READ);
  volumeMsg.setLabel(m_vmgrTapeInfo.lbltype);
  volumeMsg.setMountTransactionId(m_volReqId);
  volumeMsg.setAggregatorTransactionId(volumeRequest.aggregatorTransactionId());
  volumeMsg.setDensity(m_vmgrTapeInfo.density);

  // Send the volume message to the tape server
  connection.sendObject(volumeMsg);

  Helper::displaySentMsgIfDebug(volumeMsg, m_cmdLine.debugSet);
}


//------------------------------------------------------------------------------
// performTransfer
//------------------------------------------------------------------------------
void castor::tape::tpcp::ReadTpBlkIdCommand::performTransfer() {
  m_recallItor = m_cmdLine.recalls.begin();

  // Spin in the wait for and dispatch message loop until there is no more work
  while(waitForMsgAndDispatchHandler()) {
    // Do nothing
  }

  const uint64_t nbIncompleteTransfers = m_pendingFileTransfers.size();

  std::ostream &os = std::cout;

  time_t now = time(NULL);
  castor::utils::writeTime(os, now, TIMEFORMAT);
  os << " Finished reading from tape " << m_cmdLine.vid << std::endl
    << std::endl
    << "Number of files to be recalled = "
      << m_cmdLine.recalls.size() << std::endl
    << "Number of successfull recalls  = " << m_nbRecalledFiles << std::endl
    << "Number of incomplete transfers = " << nbIncompleteTransfers
    << std::endl;

  if(m_pendingFileTransfers.size() > 0) {
    os << std::endl;
  }

  for(FileTransferMap::iterator itor=m_pendingFileTransfers.begin();
    itor!=m_pendingFileTransfers.end(); itor++) {

    uint64_t    fileTransactionId = itor->first;
    BlkIdRecall &fileTransfer     = itor->second;

    os << "Incomplete transfer: fileTransactionId=" << fileTransactionId
      << " fseq=" << fileTransfer.fseq
      << " startBlkId=" << fileTransfer.startBlkId
      << " fileSize=" << fileTransfer.fileSize
      << " destDiskFile" << fileTransfer.destDiskFile
      << std::endl;
  }

  os << std::endl;
}


//------------------------------------------------------------------------------
// waitForMsgAndDispatchHandler
//------------------------------------------------------------------------------
bool castor::tape::tpcp::ReadTpBlkIdCommand::waitForMsgAndDispatchHandler() {

  // Socket file descriptor for a callback connection from the tape server
  int connectionSockFd = 0;

  // Wait for a callback connection from the tape server
  {
    bool   waitForCallback = true;
    time_t timeout         = WAITCALLBACKTIMEOUT;
    while(waitForCallback) {
      try {
        pingVdqmServer(m_volReqId);
      } catch (castor::exception::Exception& e) {
        
        castor::exception::Exception ex(ECANCELED);
        ex.getMessage() << e.getMessage().str() << ": " << sstrerror(e.code());
        throw ex;
      }
      try {
        connectionSockFd = io::acceptConnection(m_callbackSock.socket(),
          timeout);

        waitForCallback = false;
      } catch(castor::exception::TimeOut &tx) {

        // Command-line user feedback
        std::ostream &os = std::cout;
        time_t       now = time(NULL);

        castor::utils::writeTime(os, now, TIMEFORMAT);
        os <<
          " Waited " << WAITCALLBACKTIMEOUT << " seconds.  "
          "Continuing to wait." <<  std::endl;

        // Wait again for the default timeout
        timeout = WAITCALLBACKTIMEOUT;

      } catch(castor::exception::AcceptConnectionInterrupted &ix) {

         // If a SIGINT signal was received (control-c)
         if(s_receivedSigint) {
          castor::exception::Exception ex(ECANCELED);

          ex.getMessage() << "Received SIGNINT";
          throw ex;

        // Else received a signal other than SIGINT
        } else {

          // Wait again for the remaining amount of time
          timeout = ix.remainingTime();
        }
      }
    }
  }

  // If debug, then display a textual description of the tape server
  // callback connection
  if(m_cmdLine.debugSet) {
    std::ostream &os = std::cout;

    os << "Tape-server connection = ";
    io::writeSockDescription(os, connectionSockFd);
    os << std::endl;
  }

  // Wrap the connection socket descriptor in a CASTOR framework socket in
  // order to get access to the framework marshalling and un-marshalling
  // methods
  castor::io::AbstractTCPSocket sock(connectionSockFd);

  // Read in the message sent by the tape server
  std::unique_ptr<castor::IObject> obj(sock.readObject());

  // If debug, then display the type of message received from the tape server
  if(m_cmdLine.debugSet) {
    std::ostream &os = std::cout;

    os << "Received tape-server message of type = "
       << Helper::objectTypeToString(obj->type()) << std::endl;
  }

  {
    // Cast the message to a GatewayMessage so that the mount transaction ID
    // can be checked
    tapegateway::GatewayMessage *msg =
      dynamic_cast<tapegateway::GatewayMessage *>(obj.get());
    if(msg == NULL) {
      std::stringstream oss;

      oss <<
        "Unexpected object type" <<
        ": actual=" << Helper::objectTypeToString(obj->type()) <<
        " expected=Subclass of GatewayMessage";

      const uint64_t transactionId = 0; // Unknown transaction ID
      sendEndNotificationErrorReport(transactionId, SEINTERNAL,
        oss.str(), sock);

      castor::exception::Exception ex;
      ex.getMessage() << oss.str();
      throw ex;
    }

    // Check the mount transaction ID
    if(msg->mountTransactionId() != (uint64_t)m_volReqId) {
      std::stringstream oss;

      oss <<
        "Mount transaction ID mismatch" <<
        ": actual=" << msg->mountTransactionId() <<
        " expected=" << m_volReqId;

      sendEndNotificationErrorReport(msg->aggregatorTransactionId(), EBADMSG,
        oss.str(), sock);

      castor::exception::Exception ex(EBADMSG);
      ex.getMessage() << oss.str();
      throw ex;
    }
  }

  const bool moreWork = dispatchMsgHandler(obj.get(), sock);

  // Close the tape-server callback-connection
  sock.close();

  return moreWork;
}

//------------------------------------------------------------------------------
// dispatchMsgHandler
//------------------------------------------------------------------------------
bool castor::tape::tpcp::ReadTpBlkIdCommand::dispatchMsgHandler(
  castor::IObject *const obj, castor::io::AbstractSocket &sock) {
  switch(obj->type()) {
  case OBJ_FilesToRecallListRequest:
    return handleFilesToRecallListRequest(obj, sock);
  case OBJ_FileRecallReportList:
    return handleFileRecallReportList(obj, sock);
  case OBJ_EndNotification:
    return handleEndNotification(obj, sock);
  case OBJ_EndNotificationErrorReport:
    return handleEndNotificationErrorReport(obj, sock);
  case OBJ_PingNotification:
    return handlePingNotification(obj, sock);
  default:
  {
    std::stringstream oss;

    oss <<
      "Received unexpected tape-server message"
        ": Message type = " << Helper::objectTypeToString(obj->type());

    const uint64_t transactionId = 0; // Unknown transaction ID
    sendEndNotificationErrorReport(transactionId, EBADMSG,
      oss.str(), sock);

    castor::exception::Exception ex;
    ex.getMessage() << oss.str();
    throw ex;
  }
  }
}


//------------------------------------------------------------------------------
// handleFilesToRecallListRequest
//------------------------------------------------------------------------------
bool castor::tape::tpcp::ReadTpBlkIdCommand::handleFilesToRecallListRequest(
  castor::IObject *const obj, castor::io::AbstractSocket &sock) {
  if(m_cmdLine.raoSet) {
    return handleFilesToRecallListRequestRAO(obj, sock);
  } else {
    return handleFilesToRecallListRequestBeforeRAO(obj, sock);
  }
}


//------------------------------------------------------------------------------
// handleFilesToRecallListRequestRAO
//------------------------------------------------------------------------------
bool castor::tape::tpcp::ReadTpBlkIdCommand::handleFilesToRecallListRequestRAO(castor::IObject *const obj,
  castor::io::AbstractSocket &sock) {

  const tapegateway::FilesToRecallListRequest *msg = NULL;

  castMessage(obj, msg, sock);
  Helper::displayRcvdMsgIfDebug(*msg, m_cmdLine.debugSet);

  // Note that in case of -n/--nodata, m_filenames would by empty.
  const bool anotherFile = m_cmdLine.recalls.end() != m_recallItor;

  if(anotherFile) {
    // Create a FilesToRecallList message for the tape server
    tapegateway::RAOFilesToRecallList fileList;
    fileList.setMountTransactionId(m_volReqId);
    fileList.setAggregatorTransactionId(msg->aggregatorTransactionId());

    u_signed64 nbFilesInList = 0;
    u_signed64 nbFileBytesInList = 0;

    while(m_cmdLine.recalls.end() != m_recallItor &&
      nbFilesInList < msg->maxFiles() &&
      nbFileBytesInList < msg->maxBytes()) {

      std::unique_ptr<tapegateway::RAOFileToRecallStruct> file(
        new tapegateway::RAOFileToRecallStruct());
      file->setFileTransactionId(m_fileTransactionId);
      file->setNshost("tpcp\0");
      file->setFileid(0);
      file->setFseq(m_recallItor->fseq);
      file->setPositionCommandCode(tapegateway::TPPOSIT_BLKID);
      file->setPath(m_recallItor->destDiskFile);
      file->setUmask(m_umask);
      file->setChecksumName("NONE");
      file->setChecksum(0);
      file->setFileSize(m_recallItor->fileSize);
      file->setBlockId0((m_recallItor->startBlkId >> 24) & 0xFF);
      file->setBlockId1((m_recallItor->startBlkId >> 16) & 0xFF);
      file->setBlockId2((m_recallItor->startBlkId >> 8) & 0xFF);
      file->setBlockId3(m_recallItor->startBlkId & 0xFF);

      fileList.addFilesToRecall(file.release());

      // Update the map of current file transfers and increment the file
      // transaction ID
      m_pendingFileTransfers[m_fileTransactionId] = *m_recallItor;
      m_fileTransactionId++;

      nbFilesInList++;
      nbFileBytesInList += m_recallItor->fileSize;

      {
        // Command-line user feedback
        std::ostream &os = std::cout;

        time_t now = time(NULL);
        castor::utils::writeTime(os, now, TIMEFORMAT);
        os
          << " Recalling fseq=" << m_recallItor->fseq << ",startBlkId=" <<
          m_recallItor->startBlkId << ",fileSize=" << m_recallItor->fileSize <<
          ",destDiskFile=" << m_recallItor->destDiskFile << std::endl;
      }

      // Move onto the next file to be recalled
      m_recallItor++;
    }

    // Send the FilesToRecallList message to the tape server
    sock.sendObject(fileList);

    Helper::displaySentMsgIfDebug(fileList, m_cmdLine.debugSet);

    // Else no more files
  } else {

    // Create the NoMoreFiles message for the tape server
    castor::tape::tapegateway::NoMoreFiles noMore;
    noMore.setMountTransactionId(m_volReqId);
    noMore.setAggregatorTransactionId(msg->aggregatorTransactionId());

    // Send the NoMoreFiles message to the tape server
    sock.sendObject(noMore);

    Helper::displaySentMsgIfDebug(noMore, m_cmdLine.debugSet);
  }

  return true;
}


//------------------------------------------------------------------------------
// handleFilesToRecallListRequestBeforeRAO
//------------------------------------------------------------------------------
bool castor::tape::tpcp::ReadTpBlkIdCommand::handleFilesToRecallListRequestBeforeRAO(castor::IObject *const obj,
  castor::io::AbstractSocket &sock) {

  const tapegateway::FilesToRecallListRequest *msg = NULL;

  castMessage(obj, msg, sock);
  Helper::displayRcvdMsgIfDebug(*msg, m_cmdLine.debugSet);

  // Note that in case of -n/--nodata, m_filenames would by empty.
  const bool anotherFile = m_cmdLine.recalls.end() != m_recallItor;

  if(anotherFile) {
    // Create a FilesToRecallList message for the tape server
    tapegateway::FilesToRecallList fileList;
    fileList.setMountTransactionId(m_volReqId);
    fileList.setAggregatorTransactionId(msg->aggregatorTransactionId());

    u_signed64 nbFilesInList = 0;
    u_signed64 nbFileBytesInList = 0;

    while(m_cmdLine.recalls.end() != m_recallItor &&
      nbFilesInList < msg->maxFiles() &&
      nbFileBytesInList < msg->maxBytes()) {

      std::unique_ptr<tapegateway::FileToRecallStruct> file(
        new tapegateway::FileToRecallStruct());
      file->setFileTransactionId(m_fileTransactionId);
      file->setNshost("tpcp\0");
      file->setFileid(0);
      file->setFseq(m_recallItor->fseq);
      file->setPositionCommandCode(tapegateway::TPPOSIT_BLKID);
      file->setPath(m_recallItor->destDiskFile);
      file->setUmask(m_umask);
      file->setBlockId0((m_recallItor->startBlkId >> 24) & 0xFF);
      file->setBlockId1((m_recallItor->startBlkId >> 16) & 0xFF);
      file->setBlockId2((m_recallItor->startBlkId >> 8) & 0xFF);
      file->setBlockId3(m_recallItor->startBlkId & 0xFF);
      fileList.addFilesToRecall(file.release());

      // Update the map of current file transfers and increment the file
      // transaction ID
      m_pendingFileTransfers[m_fileTransactionId] = *m_recallItor;
      m_fileTransactionId++;

      nbFilesInList++;
      nbFileBytesInList += m_recallItor->fileSize;

      {
        // Command-line user feedback
        std::ostream &os = std::cout;

        time_t now = time(NULL);
        castor::utils::writeTime(os, now, TIMEFORMAT);
        os
          << " Recalling fseq=" << m_recallItor->fseq << ",startBlkId=" <<
          m_recallItor->startBlkId << ",fileSize=" << m_recallItor->fileSize <<
          ",destDiskFile=" << m_recallItor->destDiskFile << std::endl;
      }

      // Move onto the next file to be recalled
      m_recallItor++;
    }

    // Send the FilesToRecallList message to the tape server
    sock.sendObject(fileList);

    Helper::displaySentMsgIfDebug(fileList, m_cmdLine.debugSet);

    // Else no more files
  } else {

    // Create the NoMoreFiles message for the tape server
    castor::tape::tapegateway::NoMoreFiles noMore;
    noMore.setMountTransactionId(m_volReqId);
    noMore.setAggregatorTransactionId(msg->aggregatorTransactionId());

    // Send the NoMoreFiles message to the tape server
    sock.sendObject(noMore);

    Helper::displaySentMsgIfDebug(noMore, m_cmdLine.debugSet);
  }

  return true;
}


//------------------------------------------------------------------------------
// handleFileRecallReportList
//------------------------------------------------------------------------------
bool castor::tape::tpcp::ReadTpBlkIdCommand::handleFileRecallReportList(
  castor::IObject *const obj, castor::io::AbstractSocket &sock) {

  tapegateway::FileRecallReportList *msg = NULL;

  castMessage(obj, msg, sock);
  Helper::displayRcvdMsgIfDebug(*msg, m_cmdLine.debugSet);

  handleSuccessfulRecalls(msg->aggregatorTransactionId(),
    msg->successfulRecalls(), sock);

  handleFailedTransfers(msg->failedRecalls());

  // Create the NotificationAcknowledge message for the tape server
  castor::tape::tapegateway::NotificationAcknowledge acknowledge;
  acknowledge.setMountTransactionId(m_volReqId);
  acknowledge.setAggregatorTransactionId(msg->aggregatorTransactionId());

  // Send the NotificationAcknowledge message to the tape server
  sock.sendObject(acknowledge);

  Helper::displaySentMsgIfDebug(acknowledge, m_cmdLine.debugSet);

  return true;
}


//------------------------------------------------------------------------------
// handleSuccessfulRecalls
//------------------------------------------------------------------------------
void castor::tape::tpcp::ReadTpBlkIdCommand::handleSuccessfulRecalls(
  const uint64_t transactionId,
  const std::vector<tapegateway::FileRecalledNotificationStruct*> &files,
  castor::io::AbstractSocket &sock) {
  for(std::vector<tapegateway::FileRecalledNotificationStruct*>::const_iterator
    itor = files.begin(); itor != files.end(); itor++) {

    if(NULL == *itor) {
      castor::exception::Exception ex;
      ex.getMessage() <<
        "Pointer to FileRecalledNotificationStruct is NULL";
      throw ex;
    }

    handleSuccessfulRecall(transactionId, *(*itor), sock);
  }
}


//------------------------------------------------------------------------------
// handleSuccessfulRecall
//------------------------------------------------------------------------------
void castor::tape::tpcp::ReadTpBlkIdCommand::handleSuccessfulRecall(
  const uint64_t transactionId,
  const tapegateway::FileRecalledNotificationStruct &file,
  castor::io::AbstractSocket &sock) {
  // Check the file transaction ID
  {
    FileTransferMap::iterator itor =
      m_pendingFileTransfers.find(file.fileTransactionId());

    // If the fileTransactionId is unknown
    if(itor == m_pendingFileTransfers.end()) {
      std::stringstream oss;

      oss <<
        "Received unknown file transaction ID from the tape server"
          ": fileTransactionId=" << file.fileTransactionId();

      sendEndNotificationErrorReport(transactionId, EBADMSG,
        oss.str(), sock);

      castor::exception::Exception ex(ECANCELED);

      ex.getMessage() << oss.str();
      throw ex;
    }

    // Command-line user feedback
    {
      std::ostream &os = std::cout;
      BlkIdRecall &fileTransfer = itor->second;

      time_t now = time(NULL);
      castor::utils::writeTime(os, now, TIMEFORMAT);
      os <<
        " Recalled fseq=" << fileTransfer.fseq << ",startBlkId=" <<
        fileTransfer.startBlkId << ",fileSize=" << fileTransfer.fileSize <<
        ",destDiskFile=" << fileTransfer.destDiskFile << " checksumType=\"" <<
        file.checksumName() << "\"" << " checksum=0x" << std::hex <<
        file.checksum() << std::dec << std::endl;
    }

    // The file has been transfer so remove it from the map of pending
    // transfers
    m_pendingFileTransfers.erase(itor);
  }

  // Update the count of successfull recalls
  m_nbRecalledFiles++;
}


//------------------------------------------------------------------------------
// handlePingNotification
//------------------------------------------------------------------------------
bool castor::tape::tpcp::ReadTpBlkIdCommand::handlePingNotification(
  castor::IObject *const obj, castor::io::AbstractSocket &sock) {

  tapegateway::PingNotification *msg = NULL;

  castMessage(obj, msg, sock);
  Helper::displayRcvdMsgIfDebug(*msg, m_cmdLine.debugSet);

  // Create the NotificationAcknowledge message for the tape server
  castor::tape::tapegateway::NotificationAcknowledge acknowledge;
  acknowledge.setMountTransactionId(m_volReqId);
  acknowledge.setAggregatorTransactionId(msg->aggregatorTransactionId());

  // Send the NotificationAcknowledge message to the tape server
  sock.sendObject(acknowledge);

  Helper::displaySentMsgIfDebug(acknowledge, m_cmdLine.debugSet);

  return true;
}


//------------------------------------------------------------------------------
// handleEndNotification
//------------------------------------------------------------------------------
bool castor::tape::tpcp::ReadTpBlkIdCommand::handleEndNotification(
  castor::IObject *const obj, castor::io::AbstractSocket &sock)
   {

  tapegateway::EndNotification *msg = NULL;

  castMessage(obj, msg, sock);
  Helper::displayRcvdMsgIfDebug(*msg, m_cmdLine.debugSet);

  // Create the NotificationAcknowledge message for the tape server
  castor::tape::tapegateway::NotificationAcknowledge acknowledge;
  acknowledge.setMountTransactionId(m_volReqId);
  acknowledge.setAggregatorTransactionId(msg->aggregatorTransactionId());

  // Send the NotificationAcknowledge message to the tape server
  sock.sendObject(acknowledge);

  Helper::displaySentMsgIfDebug(acknowledge, m_cmdLine.debugSet);

  return false;
}


//------------------------------------------------------------------------------
// handleFailedTransfers
//------------------------------------------------------------------------------
void castor::tape::tpcp::ReadTpBlkIdCommand::handleFailedTransfers(
  const std::vector<tapegateway::FileErrorReportStruct*> &files) {
  for(std::vector<tapegateway::FileErrorReportStruct*>::const_iterator
    itor = files.begin(); itor != files.end(); itor++) {

    if(NULL == *itor) {
      castor::exception::Exception ex;
      ex.getMessage() << "Pointer to FileErrorReportStruct is NULL";
      throw ex;
    }

    handleFailedTransfer(*(*itor));
  }
}


//------------------------------------------------------------------------------
// handleFailedTransfer
//------------------------------------------------------------------------------
void castor::tape::tpcp::ReadTpBlkIdCommand::handleFailedTransfer(
  const tapegateway::FileErrorReportStruct &file) {

  // Command-line user feedback
  std::ostream &os = std::cout;
  const time_t now = time(NULL);

  castor::utils::writeTime(os, now, TIMEFORMAT);
  os <<
    " Tape server encountered the following error concerning a specific file:"
    "\n\n"
    "Error code        = "   << file.errorCode()            <<   "\n"
    "Error code string = \"" << sstrerror(file.errorCode()) << "\"\n"
    "Error message     = \"" << file.errorMessage()         << "\"\n"
    "\n"
    "File fileTransactionId = "   << file.fileTransactionId() <<   "\n"
    "File nsHost            = \"" << file.nshost()            << "\"\n"
    "File nsFileId          = "   << file.fileid()            <<   "\n"
    "File tapeFSeq          = "   << file.fseq()              <<   "\n" <<
    std::endl;
}


//------------------------------------------------------------------------------
// handleEndNotificationErrorReport
//------------------------------------------------------------------------------
bool castor::tape::tpcp::ReadTpBlkIdCommand::handleEndNotificationErrorReport(
  castor::IObject *const obj, castor::io::AbstractSocket &sock)
   {

  tapegateway::EndNotificationErrorReport *msg = NULL;

  castMessage(obj, msg, sock);
  Helper::displayRcvdMsgIfDebug(*msg, m_cmdLine.debugSet);

  // Store a description of the tape session error and note that it was
  // reported by the tape server.
  m_tapeSessionErrorReportedByTapeServer.errorCode = msg->errorCode();
  m_tapeSessionErrorReportedByTapeServer.errorCodeString =
    sstrerror(msg->errorCode());
  m_tapeSessionErrorReportedByTapeServer.errorMessage = msg->errorMessage();
  m_tapeServerReportedATapeSessionError = true;

  // Command-line user feedback
  {
    std::ostream &os = std::cout;
    const time_t now = time(NULL);

    castor::utils::writeTime(os, now, TIMEFORMAT);
    os <<
      " Tape server encountered the following error:" << std::endl <<
      std::endl <<
      "Error code        = " <<
      m_tapeSessionErrorReportedByTapeServer.errorCode  << std::endl <<
      "Error code string = \"" <<
      m_tapeSessionErrorReportedByTapeServer.errorCodeString << "\"" <<
      std::endl <<
      "Error message     = \"" <<
      m_tapeSessionErrorReportedByTapeServer.errorMessage << "\"" <<
      std::endl << std::endl;
  }

  // Create the NotificationAcknowledge message for the tape server
  castor::tape::tapegateway::NotificationAcknowledge acknowledge;
  acknowledge.setMountTransactionId(m_volReqId);
  acknowledge.setAggregatorTransactionId(msg->aggregatorTransactionId());

  // Send the NotificationAcknowledge message to the tape server
  sock.sendObject(acknowledge);

  Helper::displaySentMsgIfDebug(acknowledge, m_cmdLine.debugSet);
  
  return false;
}


//------------------------------------------------------------------------------
// acknowledgeEndOfSession
//------------------------------------------------------------------------------
void castor::tape::tpcp::ReadTpBlkIdCommand::acknowledgeEndOfSession()
   {

  // Socket file descriptor for a callback connection from the tape server
  int connectionSockFd = 0;

  // Wait for a callback connection from the tape server
  {
    bool   waitForCallback = true;
    time_t timeout         = WAITCALLBACKTIMEOUT;
    while(waitForCallback) {
      try {
        pingVdqmServer(m_volReqId);
      } catch (castor::exception::Exception& e) {
        
        castor::exception::Exception ex(ECANCELED);
        ex.getMessage() << e.getMessage().str() << ": " << sstrerror(e.code());
        throw ex;
      }
      try {
        connectionSockFd = io::acceptConnection(m_callbackSock.socket(),
          timeout);

        waitForCallback = false;
      } catch(castor::exception::TimeOut &tx) {

        // Command-line user feedback
        std::ostream &os = std::cout;
        time_t       now = time(NULL);

        castor::utils::writeTime(os, now, TIMEFORMAT);
        os <<
          " Waited " << WAITCALLBACKTIMEOUT << " seconds.  "
          "Continuing to wait." <<  std::endl;

        // Wait again for the default timeout
        timeout = WAITCALLBACKTIMEOUT;

      } catch(castor::exception::AcceptConnectionInterrupted &ix) {

        // If a SIGINT signal was received (control-c)
        if(s_receivedSigint) {
          castor::exception::Exception ex(ECANCELED);

          ex.getMessage() <<
           ": Received SIGNINT";

          throw ex;

        // Else received a signal other than SIGINT
        } else {

          // Wait again for the remaining amount of time
          timeout = ix.remainingTime();
        }
      }
    }
  }

  // If debug, then display a textual description of the tape server
  // callback connection
  if(m_cmdLine.debugSet) {
    std::ostream &os = std::cout;

    os << "Tape-server connection = ";
    io::writeSockDescription(os, connectionSockFd);
    os << std::endl;
  }

  // Wrap the connection socket descriptor in a CASTOR framework socket in
  // order to get access to the framework marshalling and un-marshalling
  // methods
  castor::io::AbstractTCPSocket sock(connectionSockFd);

  // Read in the object sent by the tape server
  std::unique_ptr<castor::IObject> obj(sock.readObject());

  // Pointer to the received object with the object's type
  tapegateway::EndNotification *endNotification = NULL;

  castMessage(obj.get(), endNotification, sock);
  Helper::displayRcvdMsgIfDebug(*endNotification, m_cmdLine.debugSet);

  // Create the NotificationAcknowledge message for the tape server
  castor::tape::tapegateway::NotificationAcknowledge acknowledge;
  acknowledge.setMountTransactionId(m_volReqId);
  acknowledge.setAggregatorTransactionId(
    endNotification->aggregatorTransactionId());

  // Send the volume message to the tape server
  sock.sendObject(acknowledge);

  // Close the connection to the tape server
  sock.close();

  Helper::displaySentMsgIfDebug(acknowledge, m_cmdLine.debugSet);
}


//------------------------------------------------------------------------------
// sendEndNotificationErrorReport
//------------------------------------------------------------------------------
void castor::tape::tpcp::ReadTpBlkIdCommand::sendEndNotificationErrorReport(
  const uint64_t             transactionId,
  const int                  errorCode,
  const std::string          &errorMessage,
  castor::io::AbstractSocket &sock)
  throw() {

  try {
    // Create the message
    tapegateway::EndNotificationErrorReport errorReport;
    errorReport.setAggregatorTransactionId(transactionId);
    errorReport.setErrorCode(errorCode);
    errorReport.setErrorMessage(errorMessage);

    // Send the message to the tape server
    sock.sendObject(errorReport);

    Helper::displaySentMsgIfDebug(errorReport, m_cmdLine.debugSet);
  } catch(...) {
    // Do nothing
  }
}


//------------------------------------------------------------------------------
// sigintHandler
//------------------------------------------------------------------------------
void castor::tape::tpcp::ReadTpBlkIdCommand::sigintHandler(int) {
  s_receivedSigint = true;
}


//------------------------------------------------------------------------------
// deleteVdqmVolumeRequest
//------------------------------------------------------------------------------
void castor::tape::tpcp::ReadTpBlkIdCommand::deleteVdqmVolumeRequest()
   {

  const int rc = vdqm_DelVolumeReq(NULL, m_volReqId, m_vmgrTapeInfo.vid, m_dgn,
    NULL, NULL, 0);
  const int savedSerrno = serrno;

  if(rc < 0) {
    castor::exception::Exception ex;
    ex.getMessage() <<
      "Failed call to vdqm_DelVolumeReq()"
      ": " << sstrerror(savedSerrno);
    throw ex;
  }
}


//------------------------------------------------------------------------------
// translateFilenamesIntoRemoteFilenames
//------------------------------------------------------------------------------
void castor::tape::tpcp::ReadTpBlkIdCommand::translateFilenamesIntoRemoteFilenames() {

  std::list<BlkIdRecall>::iterator itor = m_cmdLine.recalls.begin();
  // local string containing the hostname + ":"
  std::string hostname(m_hostname);
  hostname.append(":");

  while(itor!=m_cmdLine.recalls.end()) {

   std::string &filename = itor->destDiskFile;

   // check if the file name ends with '.' or '/'
   const char *characters = "/.";
   std::string::size_type end = filename.find_last_not_of(characters);

   if(end == std::string::npos || end != filename.length()-1) {

    castor::exception::Exception ex(ECANCELED);
    ex.getMessage() <<
           ": Invalid file-name syntax"
           ": File name must identify a regular file"
           ": file name=\"" << itor->destDiskFile <<"\"";

      throw ex;
   }

   const size_t firstPos = filename.find(":/");

   // if there are 0 ":/" --> (is a local file) 
   if(firstPos == std::string::npos){
     if(filename.find_first_of("/") != 0){
       // Prefix it with the CWD
       filename.insert(0, "/");
       filename.insert(0, m_cwd);
     }
     // Prefix it with the Hostname
     filename.insert(0, hostname);

   } else {
     // if there is at least 1 ":/" -->check the "hostname" entered by the
     // user (note :/ can be part of the filepath)
     std::string str = filename.substr(0, firstPos);
     if(str.empty()){
       castor::exception::Exception ex(ECANCELED);
       ex.getMessage() <<
         ": Invalid file-name syntax"
         ": Missing hostname before ':/'"
         ": file name=\"" << filename <<"\"";

       throw ex;
     }
     // if file hostname == "localhost" or "127.0.0.1"
     // --> replace it with hostname
     if (str == "localhost" || str == "127.0.0.1"){

       filename.replace(0, firstPos+1, hostname);
     }
   }

    ++itor;
  }
}


//------------------------------------------------------------------------------
// localStat
//------------------------------------------------------------------------------
void castor::tape::tpcp::ReadTpBlkIdCommand::localStat(const char *const path,
  struct stat &statBuf) const  {
  if(stat(path, &statBuf)) {
    const char *const errMsg = sstrerror(errno);
    castor::exception::Exception ex;
    ex.getMessage() << "Failed to stat \"" << path << "\""
      ": " << errMsg;
    throw ex;
  }
}
