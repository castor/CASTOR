/******************************************************************************
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

#pragma once

#include "castor/tape/tpcp/BlkIdRecall.hpp"

#include <list>
#include <string>

namespace castor {
namespace tape   {
namespace tpcp   {

/**
 * Class responsible for parsing a file of block-ID based recall jobs.
 */
class BlkIdRecallsFileParser {
public:

  /**
   * Parses the specified file containing block-ID based recall jobs.
   *
   * This method:
   * <ul>
   * <li>Trims leading and trailing white space from each line
   * <li>Ignores blank lines with or without white space.
   * <li>Ignores comment lines, i.e. those starting with a '#' after their
   * leading and trailing white space has been trimmed.
   * </ul>
   *
   * @param filename The name of the file to be parsed.
   * @return The list of parsed block-ID based recall jobs.
   */
  static std::list<BlkIdRecall> parseFile(const std::string &filename);

private:

  /**
   * Appends each line of the specified file to the specified list of lines.
   * The new-line characters are extracted from the file, but they are not
   * stored in the lines appended to the list.
   *
   * An empty line, with or without a delimiting '\n' character will be appended
   * to the list of lines as an empty string.
   *
   * @param filename The filename of the file to be read.
   * @return The lists file lines.
   */
  static std::list<std::string> readFileIntoList(const std::string &filename);

  /**
   * Parses the specified line into a BlkIdRecall structure.
   *
   * The format of the line to be parsed should be:
   *
   *   fSeq,fileStartBlkId,fileSize,destFile
   *
   * @param line The line tobe parsed.
   * @return The BlkIdRecall structure.
   */
  static BlkIdRecall parseLineIntoBlkIdRecall(const std::string &line);

}; // class TpcpCommand

} // namespace tpcp
} // namespace tape
} // namespace castor
