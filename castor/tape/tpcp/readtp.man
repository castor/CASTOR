.\" Copyright (C) 2003  CERN
.\" This program is free software; you can redistribute it and/or
.\" modify it under the terms of the GNU General Public License
.\" as published by the Free Software Foundation; either version 2
.\" of the License, or (at your option) any later version.
.\" This program is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\" You should have received a copy of the GNU General Public License
.\" along with this program; if not, write to the Free Software
.\" Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
.TH READTP "1castor" "$Date: 2009/08/07 15:56:38 $" CASTOR "CASTOR"
.SH NAME
readtp \- read from a tape
.SH SYNOPSIS
.BI "readtp VID SEQUENCE [OPTIONS] [FILE]..."

.SH DESCRIPTION
.B readtp
reads one or files from a tape and writes them to disk.
.P
This command gives privileged users direct access to the tape system.
.P
The tape to be read is specified by the \fBVID\fP argument.  The tape files to
be read are specified as a \fBSEQUENCE\fP of tape file sequence numbers.  The
syntax used to specify the sequence is as follows:
.RS
.TP 1.5i
.I f1\-f2
Files
.I f1
to
.I f2
inclusive.
.TP
.I f1\-
Files
.I f1
to the last file on the tape.
.TP
.I f1\-f2,\|f4,\|f6-
A series of non\-consecutive ranges of files.
.RE
.P
A destination disk-file should be given in the form [host:]local_path.
The file names of the destination disk-files can be specified using three
mutually-exclusive methods.  They can be entered on the command-line
(\fB[FILE]\fP), in a "file-list" file using the
.B -f, --filelist
option or they may not be specified at all using the
.B -n, \-\-nodata
option.

.SH OPTIONS
.TP
\fB\-d, \-\-debug
Turns on the printing of debug information.
.TP
\fB\-h, \-\-help
Prints the usage message.
.TP
\fB\-s, \-\-server server
Specifies the tape server to be used, therefore overriding the drive scheduling
of the VDQM.  The name of the server must match exactly the name given by the
VDQM showqueues command.  For example if showqueues displays the tape server
name 'tpsrv123' then specifiying '-s tpsrv123.cern.ch' will not work and the
VDQM request will remain in the VDQM until it is deleted (this command will
delete the request when it receives Ctrl-C).  If this option is not set then
the VDQM will allocate the first free compatible drive.
.TP
.B -f, \-\-filelist file
File containing the list of destination file-names.
There should be one filename per line. Whitespace is
trimmed from the left and right of each filename.  Blank lines are ignored.
Lines starting with a '#' character are considered comments and are therefore
ignored.  Whitespace is allowed between the beginning of the line and a '#'
character.  Please note that this option is mutually exclusive with
providing filenames on the command-line and with the
.B -n, \-\-nodata
option.
.TP
\fB\-n, \-\-nodata
Send all data read from tape to /dev/null on the tape server.  Please note
that this option is mutually exclusive with providing filenames on the
command-line and with the
.B -f, \-\-filelist
option.

.SH "RETURN CODES"
.TP
\fB 0
Ok.
.TP
\fB 1
Command failed.

.SH CASTOR CONFIGURATION PARAMETERS
The \fBreadtp\fP command reads and uses the following CASTOR configuration
parameters which are specified using environment variables or within the CASTOR
configuration file (the default location is /etc/castor/castor.conf).
.TP
\fBTAPESERVERCLIENT LOWPORT
The inclusive low port of the tapeserver client callback port number range.
The default is 30201.
.TP
\fBTAPESERVERCLIENT HIGHPORT
The inclusive high port of the tapeserver client callback port number range.
The default is 30300.
.TP
\fBVDQM HOST
The name of the host on which the VDQM daemon is running.
.TP
\fBVMGR HOST
The name of the host on which the VMGR daemon is running.

.SH EXAMPLE
The follwing example reads the first and fourth files from the tape with the
VID \fBI10553\fP and writes them to the files \fBlxc2disk07:/tmp/test_a\fP and
\fBlxc2disk07:/tmp/test_b\fP respectively.

.P
.B readtp I10553 1,4 lxc2disk07:/tmp/test_a lxc2disk07:/tmp/test_b

The following two examples do the same thing.  They both read the first file of
a tape and write it to the local filesystem of the tape server.  They are
therefore useful when a tape operator or developer wishes to read a tape file
without using a remote transfer protocol.

.P
.B runuser --shell='/bin/bash' --session-command='readtp V12345 1 -s tpsrvXXX file:///tmp/1 ' stage

.P
.B runuser --shell='/bin/bash' --session-command='readtp V12345 1 -s tpsrvXXX rfio://localhost:////tmp/1 ' stage

.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
