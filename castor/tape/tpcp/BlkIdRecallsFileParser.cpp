/******************************************************************************
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/
 
#include "castor/exception/Exception.hpp"
#include "castor/tape/tpcp/BlkIdRecallsFileParser.hpp"
#include "castor/utils/utils.hpp"

#include <iostream>
#include <fstream>
#include <stdint.h>

//------------------------------------------------------------------------------
// parseFileList
//------------------------------------------------------------------------------
std::list<castor::tape::tpcp::BlkIdRecall>
  castor::tape::tpcp::BlkIdRecallsFileParser::parseFile(
    const std::string &filename) {

  std::list<castor::tape::tpcp::BlkIdRecall> recalls;
  const std::list<std::string> lines = readFileIntoList(filename);

  uint64_t lineNb = 0;
  for(std::list<std::string>::const_iterator itor = lines.begin();
    itor != lines.end(); itor++) {
    lineNb++;
    std::string line = *itor;

    // Left and right trim the line
    line = castor::utils::trimString(line);

    // If the line is not empty string and it is not a comment
    if(!line.empty() && !(line.size() > 0 && line[0] == '#')) {
      BlkIdRecall recall;
      try {
        recall = parseLineIntoBlkIdRecall(line);
      } catch(castor::exception::Exception &ne) {
        castor::exception::Exception ex;
        ex.getMessage() << "Failed to parse line " << lineNb << " of file " <<
          filename << ": " << ne.getMessage().str();
        throw ex;
      }
      recalls.push_back(recall);
    }
  }

  return recalls;
}

//------------------------------------------------------------------------------
// readFileIntoList
//------------------------------------------------------------------------------
std::list<std::string>
  castor::tape::tpcp::BlkIdRecallsFileParser::readFileIntoList(
  const std::string &filename) {

  std::ifstream file(filename.c_str());

  if(!file) {
    castor::exception::Exception ex(ECANCELED);

    ex.getMessage() << "Failed to open file: Filename='" << filename << "'";

    throw ex;
  } 

  std::list<std::string> lines;
  while(!file.eof()) {
    std::string line;

    std::getline(file, line, '\n');

    if(!line.empty() || !file.eof()) {
      lines.push_back(line);
    }
  }

  return lines;
}

//------------------------------------------------------------------------------
// parseLineIntoBlkIdRecall
//------------------------------------------------------------------------------
castor::tape::tpcp::BlkIdRecall
  castor::tape::tpcp::BlkIdRecallsFileParser::parseLineIntoBlkIdRecall(
    const std::string &line) {
  // The format of the line to be parsed should be:
  //    fSeq,fileStartBlkId,fileSize,destFile
  std::vector<std::string> columns;

  // Column values are separated by commas
  castor::utils::splitString(line, ',', columns);

  const int expectedNbColumns = 4;
  if(expectedNbColumns != columns.size()) {
    exception::Exception ex;
    ex.getMessage() << "BlkIdRecall line should contain exactly 4 comma"
      " separated columns: expected=" << expectedNbColumns << ",actual=" <<
      columns.size();
    throw ex;
  }

  // Left and right trim each column
  columns[0] = castor::utils::trimString(columns[0]);
  columns[1] = castor::utils::trimString(columns[1]);
  columns[2] = castor::utils::trimString(columns[2]);
  columns[3] = castor::utils::trimString(columns[3]);

  BlkIdRecall recall;

  {
    std::stringstream fseq;
    fseq << columns[0];
    if (!utils::isValidUInt(fseq.str())) {
      castor::exception::InvalidArgument ex;
      ex.getMessage() << "\tFseq " << fseq.str() <<
        " is not a valid unsigned integer";
      throw ex;
    }
    fseq >> recall.fseq;
  }

  {
    std::stringstream startBlkId;
    startBlkId << columns[1];
    if (!utils::isValidUInt(startBlkId.str())) {
      castor::exception::InvalidArgument ex;
      ex.getMessage() << "\tStart block ID " << startBlkId.str() <<
        " is not a valid unsigned integer";
      throw ex;
    }
    startBlkId >> recall.startBlkId;
  }

  {
    std::stringstream fileSize;
    fileSize << columns[2];
    if (!utils::isValidUInt(fileSize.str())) {
      castor::exception::InvalidArgument ex;
      ex.getMessage() << "\tFile size " << fileSize.str() <<
        " is not a valid unsigned integer";
      throw ex;
    }
    fileSize >> recall.fileSize;
  }

  recall.destDiskFile = columns[3];

  return recall;
}
