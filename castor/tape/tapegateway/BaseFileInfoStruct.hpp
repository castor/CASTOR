/**** This file has been autogenerated by gencastor from Umbrello UML model ***/

/******************************************************************************
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

#pragma once

// Include Files
#include "castor/IObject.hpp"
#include "castor/tape/tapegateway/PositionCommandCode.hpp"
#include "osdep.h"
#include <iostream>
#include <string>

namespace castor {

  // Forward declarations
  class ObjectSet;

  namespace tape {

    namespace tapegateway {

      /**
       * class BaseFileInfoStruct
       */
      class BaseFileInfoStruct : public virtual castor::IObject {

      public:

        /**
         * Empty Constructor
         */
        BaseFileInfoStruct() throw();

        /**
         * Empty Destructor
         */
        virtual ~BaseFileInfoStruct() throw();

        /**
         * Outputs this object in a human readable format
         * @param stream The stream where to print this object
         * @param indent The indentation to use
         * @param alreadyPrinted The set of objects already printed.
         * This is to avoid looping when printing circular dependencies
         */
        virtual void print(std::ostream& stream,
                           std::string indent,
                           castor::ObjectSet& alreadyPrinted) const;

        /**
         * Outputs this object in a human readable format
         */
        virtual void print() const;

        /**
         * Get the value of m_fileTransactionId
         * @return the value of m_fileTransactionId
         */
        u_signed64 fileTransactionId() const {
          return m_fileTransactionId;
        }

        /**
         * Set the value of m_fileTransactionId
         * @param new_var the new value of m_fileTransactionId
         */
        void setFileTransactionId(u_signed64 new_var) {
          m_fileTransactionId = new_var;
        }

        /**
         * Get the value of m_nshost
         * @return the value of m_nshost
         */
        std::string nshost() const {
          return m_nshost;
        }

        /**
         * Set the value of m_nshost
         * @param new_var the new value of m_nshost
         */
        void setNshost(std::string new_var) {
          m_nshost = new_var;
        }

        /**
         * Get the value of m_fileid
         * @return the value of m_fileid
         */
        u_signed64 fileid() const {
          return m_fileid;
        }

        /**
         * Set the value of m_fileid
         * @param new_var the new value of m_fileid
         */
        void setFileid(u_signed64 new_var) {
          m_fileid = new_var;
        }

        /**
         * Get the value of m_fseq
         * @return the value of m_fseq
         */
        int fseq() const {
          return m_fseq;
        }

        /**
         * Set the value of m_fseq
         * @param new_var the new value of m_fseq
         */
        void setFseq(int new_var) {
          m_fseq = new_var;
        }

        /**
         * Get the value of m_positionCommandCode
         * @return the value of m_positionCommandCode
         */
        PositionCommandCode positionCommandCode() const {
          return m_positionCommandCode;
        }

        /**
         * Set the value of m_positionCommandCode
         * @param new_var the new value of m_positionCommandCode
         */
        void setPositionCommandCode(PositionCommandCode new_var) {
          m_positionCommandCode = new_var;
        }

      private:

        u_signed64 m_fileTransactionId;

        std::string m_nshost;

        u_signed64 m_fileid;

        int m_fseq;

        PositionCommandCode m_positionCommandCode;

      }; /* end of class BaseFileInfoStruct */

    } /* end of namespace tapegateway */

  } /* end of namespace tape */

} /* end of namespace castor */

