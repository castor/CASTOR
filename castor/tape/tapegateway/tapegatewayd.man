.TH TAPEGATEWAYD "$Date: 2009/08/18 09:42:55 $" CASTOR "TapeGateway"
.SH NAME
tapegatewayd \- tape gateway daemon
.SH SYNOPSIS
.B tapegateway
[
.BI -f
]
.SH DESCRIPTION
.B tapegatewayd
is the CASTOR component which handles the interface between the disk cache and the tape backend, providing support for the new generation tape format.
This component is a multi-threaded application, database centric, which uses the CASTOR framework.
It submits drive requests to VDQM when migrations and/or recalls are required.
The migrations/recalls are triggered thanks to the interaction of different daemons ( stager, mighunter, rechandler), each of them is in charge of different db transactions.
The tapegateway interacts with the VDQM using messages over TCP/IP.
The communication between tapegateway and the several Tapebridge, which are running on the tape servers, is a based on TCP/IP messages too.
The tapegatewayd relies on the DLF logging interface. Logs can be accessed by web interface or locally on the machine using the /var/log/castor/tapegatewayd.log log file.
 
.SH OPTIONS
.BI \-f
To run in foreground

.SH CASTOR CONFIGURATION PARAMETERS
The tapegateway daemon reads and uses the following CASTOR configuration
parameters which are specified within the CASTOR configuration file (the
default location is /etc/castor/castor.conf).
.TP
\fBTAPEGATEWAY MINWORKERTHREADS
Specifies the minimum number of threads in the Worker thread pool.
.TP
\fBTAPEGATEWAY MAXWORKERTHREADS
Specifies the maximum number of threads that can be in the Worker thread pool.
.TP
\fBTAPEGATEWAY PORT
Specifies the TCP/IP port used by the tapegateway to receive messages from the
tape server.  The default is 62801 and should NOT normally be modified.

.SH EXAMPLE
.fi
# tapegatewayd
.fi
# tapegatewayd  -f

.SH EXIT STATUS
This program returns 0 if the operation was successful or > 0 if the operation
failed.

.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
