/******************************************************************************
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2004  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 *
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

#include "castor/Constants.hpp"
#include "castor/exception/Exception.hpp"
#include "castor/IService.hpp"
#include "castor/Services.hpp"
#include "castor/tape/tapegateway/daemon/ITapeGatewaySvc.hpp"
#include "castor/tape/tapegateway/daemon/VdqmRequestsProducerThread.hpp"
#include "castor/tape/tapegateway/daemon/VdqmTapeGatewayHelper.hpp"
#include "castor/tape/tapegateway/daemon/VmgrTapeGatewayHelper.hpp"
#include "castor/tape/tapegateway/EndNotificationErrorReport.hpp"
#include "castor/tape/tapegateway/ScopedTransaction.hpp"
#include "castor/utils/Timer.hpp"
#include "castor/log/log.hpp"
#include "Ctape_constants.h"
#include "serrno.h"
#include "u64subr.h"

#include <errno.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <memory>

//------------------------------------------------------------------------------
// constructor
//------------------------------------------------------------------------------
castor::tape::tapegateway::VdqmRequestsProducerThread::VdqmRequestsProducerThread(int port){
  m_port=port;
}

//------------------------------------------------------------------------------
// run 
//------------------------------------------------------------------------------
void castor::tape::tapegateway::VdqmRequestsProducerThread::run(void* /*param*/) throw() {
  // connect to the db
  castor::IService* dbSvc = 
    castor::BaseObject::services()->service("OraTapeGatewaySvc", castor::SVC_ORATAPEGATEWAYSVC);
  castor::tape::tapegateway::ITapeGatewaySvc* oraSvc =
    dynamic_cast<castor::tape::tapegateway::ITapeGatewaySvc*>(dbSvc);
  if (0 == oraSvc) {
    castor::log::write(LOG_ERR, "Fatal error");
    return;
  }

  // loop over tapes that need to be handled
  std::string vid;
  int vdqmPriority;
  int mode;
  while (getTapeToHandle(oraSvc, vid, vdqmPriority, mode)) {
    // check tape in VMGR (and fail migration if not ok)
    struct TapeInfo info = checkInVMGR(oraSvc, vid, mode);
    if (0 != strcmp(info.dgnBuffer,"")) {
      // Create a VDQM request and commit to stager DB
      sendToVDQM(oraSvc, vid, info.dgnBuffer, mode, info.vmgrTapeInfo.lbltype,
                 info.vmgrTapeInfo.density, vdqmPriority);
    }
  }
}

//------------------------------------------------------------------------------
// getTapeToHandle 
//------------------------------------------------------------------------------
bool castor::tape::tapegateway::VdqmRequestsProducerThread::getTapeToHandle
(castor::tape::tapegateway::ITapeGatewaySvc* oraSvc, 
 std::string &vid,
 int &vdqmPriority,
 int &mode)
  throw() {
  castor::utils::Timer timer;
  try {
    // get a tape from the db
    castor::log::write(LOG_DEBUG, "VdqmRequestsProducer: getting a tape to submit");
    // This PL/SQL takes locks
    oraSvc->getTapeWithoutDriveReq(vid, vdqmPriority, mode);
  } catch (castor::exception::Exception& e){
    // error in getting new tape to submit
    std::list<castor::log::Param> params = {
      castor::log::Param("errorCode",sstrerror(e.code())),
      castor::log::Param("errorMessage",e.getMessage().str())
    };
    castor::log::write(LOG_ERR, "VdqmRequestsProducer: no tape retrieved", params);
    return 0;
  }
  // check whether we have something to do
  if (vid.empty()) {
    castor::log::write(LOG_DEBUG, "VdqmRequestsProducer: no tape retrieved");
    return false;
  } else {
    // We have something to do, log "found tape to submit"
    std::list<castor::log::Param> params = {
      castor::log::Param("TPVID", vid),
      castor::log::Param("vdqmPriority", vdqmPriority),
      castor::log::Param("mode", mode==WRITE_ENABLE?"WRITE_ENABLE":"WRITE_DISABLE"),
      castor::log::Param("ProcessingTime", timer.secs())
    };
    castor::log::write(LOG_DEBUG, "VdqmRequestsProducer: no tape to submit", params);
  }
  return true;
}

//------------------------------------------------------------------------------
// checkInVMGR 
//------------------------------------------------------------------------------
castor::tape::tapegateway::TapeInfo
castor::tape::tapegateway::VdqmRequestsProducerThread::checkInVMGR
(castor::tape::tapegateway::ITapeGatewaySvc* oraSvc, 
 const std::string &vid,
 const int mode)
  throw() {
  castor::utils::Timer timer;
  try {
    TapeInfo info = VmgrTapeGatewayHelper::getTapeInfoAssertAvailable(vid, m_shuttingDown);
    std::list<castor::log::Param> paramsVmgr = {
      castor::log::Param("TPVID", vid),
      castor::log::Param("dgn", info.dgnBuffer),
      castor::log::Param("label", info.vmgrTapeInfo.lbltype),
      castor::log::Param("density", info.vmgrTapeInfo.density),
      castor::log::Param("ProcessingTime", timer.secs())
    };
    // tape is fine
    castor::log::write(LOG_DEBUG, "VdqmRequestsProducer: querying vmgr",paramsVmgr);
    return info;
  } catch (castor::exception::Exception& e) {
    std::list<castor::log::Param> params = {
      castor::log::Param("Standard Message", sstrerror(e.code())),
      castor::log::Param("Precise Message", e.getMessage().str()),
      castor::log::Param("TPVID", vid),
      castor::log::Param("ProcessingTime", timer.secs())
    };
    castor::log::write(LOG_ERR, "VdqmRequestsProducer: vmgr error", params);
    if (e.code() == ENOENT || e.code() == EFAULT || e.code() == EINVAL ||
        e.code() == ETHELD || e.code() == ETABSENT || e.code() == ETARCH ) {
      try {
        // cancel the operation in the db for these error. Note that logging is done from the DB
        oraSvc->cancelMigrationOrRecall(mode, vid, e.code(), e.getMessage().str());
      } catch (castor::exception::Exception& e ){
        // impossible to update the information of submitted tape
        std::list<castor::log::Param> params = {
          castor::log::Param("errorCode", sstrerror(e.code())),
          castor::log::Param("errorMessage", e.getMessage().str()),
          castor::log::Param("context","tape was unavailable, wanted to cancel recall/migration"),
          castor::log::Param("TPVID", vid)
        };
        castor::log::write(LOG_ERR, "VdqmRequestsProducer: cannot update db", params);
      }
    }
    // For other errors, we leave the request untouched so that we will try again later
  }
  // case of error only
  return TapeInfo();
}

//------------------------------------------------------------------------------
// sendToVDQM 
//------------------------------------------------------------------------------
void castor::tape::tapegateway::VdqmRequestsProducerThread::sendToVDQM
(castor::tape::tapegateway::ITapeGatewaySvc* oraSvc, 
 const std::string &vid,
 const char* dgn,
 const int mode,
 const char* label,
 const char* density,
 const int vdqmPriority)
  throw() {
  VdqmTapeGatewayHelper vdqmHelper;
  castor::utils::Timer timer;
  int mountTransactionId = 0;
  try {
    // connect to vdqm and submit the request
    vdqmHelper.connectToVdqm();
  } catch (castor::exception::Exception& e) {
    std::list<castor::log::Param> params = {
      castor::log::Param("errorCode", sstrerror(e.code())),
      castor::log::Param("errorMessage", e.getMessage().str()),
      castor::log::Param("TPVID", vid),
      castor::log::Param("mountTransactionId", mountTransactionId),
      castor::log::Param("ProcessingTime", timer.secs())
    };
    castor::log::write(LOG_ERR, "VdqmRequestsProducer: vdqm error", params);
    return;
  }
  // we are now connected to VDQM
  try {
    mountTransactionId = vdqmHelper.createRequest(vid, dgn, mode, m_port, vdqmPriority);
    // set mountTransactionId in the RecallMount and commit
    oraSvc->attachDriveReq(vid, mountTransactionId, mode, label, density);
    // confirm to vdqm
    vdqmHelper.confirmRequestToVdqm();
    // log "VdqmRequestsProducer: request submitted to vdqm successfully"
    std::list<castor::log::Param> paramsDb = {
      castor::log::Param("TPVID", vid),
      castor::log::Param("mountTransactionId", mountTransactionId),
      castor::log::Param("ProcessingTime", timer.secs())
    };
    castor::log::write(LOG_INFO, "VdqmRequestsProducer: request submitted to vdqm successfully", paramsDb);
  } catch (castor::exception::Exception& e) {
    std::list<castor::log::Param> params = {
      castor::log::Param("errorCode", sstrerror(e.code())),
      castor::log::Param("errorMessage", e.getMessage().str()),
      castor::log::Param("TPVID", vid),
      castor::log::Param("mountTransactionId", mountTransactionId),
      castor::log::Param("ProcessingTime", timer.secs())
    };
    castor::log::write(LOG_ERR, "VdqmRequestsProducer: vdqm error", params);
  }
  // in all cases, we (try to) disconnect from VDQM
  try {
    vdqmHelper.disconnectFromVdqm();
  } catch (castor::exception::Exception& e) {
    // impossible to disconnect from vdqm
    std::list<castor::log::Param> params = {
      castor::log::Param("errorCode", sstrerror(e.code())),
      castor::log::Param("errorMessage", e.getMessage().str()),
      castor::log::Param("TPVID", vid),
      castor::log::Param("mountTransactionId", mountTransactionId),
      castor::log::Param("ProcessingTime", timer.secs())
    };
    castor::log::write(LOG_ERR, "VdqmRequestsProducer: vdqm error",params);
  }
}
