/******************************************************************************
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2004  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 *
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

#include "Cns_api.h" // for log only
#include "Cuuid.h"
#include "castor/Constants.hpp"
#include "castor/io/io.hpp"
#include "castor/IService.hpp"
#include "castor/Services.hpp"
#include "castor/stager/TapeTpModeCodes.hpp"
#include "castor/tape/tapegateway/EndNotification.hpp"
#include "castor/tape/tapegateway/EndNotificationErrorReport.hpp"
#include "castor/tape/tapegateway/GatewayMessage.hpp"
#include "castor/tape/tapegateway/NoMoreFiles.hpp"
#include "castor/tape/tapegateway/NotificationAcknowledge.hpp"
#include "castor/tape/tapegateway/VolumeRequest.hpp"
#include "castor/tape/tapegateway/Volume.hpp"
#include "castor/tape/tapegateway/BaseFileInfoStruct.hpp"
#include "castor/tape/tapegateway/FileMigratedNotificationStruct.hpp"
#include "castor/tape/tapegateway/FileErrorReportStruct.hpp"
#include "castor/tape/tapegateway/FileRecalledNotificationStruct.hpp"
#include "castor/tape/tapegateway/FilesListRequest.hpp"
#include "castor/tape/tapegateway/FileMigrationReportList.hpp"
#include "castor/tape/tapegateway/FilesToMigrateList.hpp"
#include "castor/tape/tapegateway/FileToMigrateStruct.hpp"
#include "castor/tape/tapegateway/FileRecallReportList.hpp"
#include "castor/tape/tapegateway/FilesToMigrateListRequest.hpp"
#include "castor/tape/tapegateway/FilesToRecallListRequest.hpp"
#include "castor/tape/tapegateway/daemon/NsTapeGatewayHelper.hpp"
#include "castor/tape/tapegateway/daemon/VmgrTapeGatewayHelper.hpp"
#include "castor/tape/tapegateway/daemon/WorkerThread.hpp"
#include "castor/tape/tapegateway/RAOFilesToRecallList.hpp"
#include "castor/tape/tapegateway/RAOFileToRecallStruct.hpp"
#include "castor/utils/Timer.hpp"
#include "castor/log/log.hpp"
#include "Ctape_constants.h" // for log only
#include "getconfent.h"
#include "serrno.h"
#include "u64subr.h"

#include <errno.h>
#include <sys/time.h>
#include <sys/types.h>
#include <limits.h>
#include <inttypes.h>
#include <stdlib.h>
#include <unistd.h>
#include <memory>
#include <typeinfo>
#include <algorithm>
#include <queue>
#include <cstring>
#include <typeinfo>

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
castor::tape::tapegateway::WorkerThread::WorkerThread():BaseObject(){}

//------------------------------------------------------------------------------
// runs the thread
//------------------------------------------------------------------------------
void castor::tape::tapegateway::WorkerThread::run(void* arg)
{
  // to comunicate with the tape Tapebridge
  // service to access the database
  castor::IService* dbSvc = castor::BaseObject::services()->service("OraTapeGatewaySvc", castor::SVC_ORATAPEGATEWAYSVC);
  castor::tape::tapegateway::ITapeGatewaySvc* oraSvc = dynamic_cast<castor::tape::tapegateway::ITapeGatewaySvc*>(dbSvc);
  if (0 == oraSvc) {
    castor::log::write(LOG_ERR, "Fatal error");
    return;
  }
  try{
    // Gather information about the request to allow detailed and compact logging
    // in request handlers.
    requesterInfo requester; // This will be initialised by constructor.
    
    // create a socket
    std::unique_ptr<castor::io::ServerSocket> sock((castor::io::ServerSocket*)arg); 
    try {
      sock->getPeerIp(requester.port, requester.ip);
      requester.hostName = io::getPeerHostName(sock->socket());
    } catch(castor::exception::Exception& e) {
      // "Exception caught : ignored" message
      std::list<castor::log::Param> params =
	{castor::log::Param("Standard Message", sstrerror(e.code())),
	 castor::log::Param("Precise Message", e.getMessage().str())};
      castor::log::write(LOG_ERR, "Worker: unknown client", params);
      return;
    }
    // get the incoming request
    try {
      std::unique_ptr<castor::IObject>  obj(sock->readObject());
      if (obj.get() == NULL){
        std::list<castor::log::Param> params = {
            castor::log::Param("IP",  castor::log::IPAddress(requester.ip)),
            castor::log::Param("Port",requester.port),
            castor::log::Param("HostName",requester.hostName)
        };
	castor::log::write(LOG_ERR, "Worker: invalid request", params);
	return;
      }
      // first let's just extract for logging the GatewayMessage info
      GatewayMessage& message = dynamic_cast<GatewayMessage&>(*obj);
      std::list<castor::log::Param> params = {
          castor::log::Param("IP",  castor::log::IPAddress(requester.ip)),
          castor::log::Param("Port",requester.port),
          castor::log::Param("HostName",requester.hostName),
          castor::log::Param("mountTransactionId",message.mountTransactionId()),
          castor::log::Param("tapebridgeTransId",message.aggregatorTransactionId()),
      };
      castor::log::write(LOG_DEBUG, "Worker: received a message", params);

      //let's call the handler
      castor::IObject* handler_response = NULL;
      castor::log::write(LOG_DEBUG, "Worker: dispatching request", params);
      switch (obj->type()) {
        case OBJ_VolumeRequest:
          handler_response = handleStartWorker(*obj,*oraSvc,requester);
          break;
        case OBJ_EndNotification:
          handler_response = handleEndWorker(*obj,*oraSvc,requester);
          break;
        case OBJ_EndNotificationErrorReport:
          handler_response = handleFailWorker(*obj,*oraSvc,requester);
          break;
        case OBJ_FileMigrationReportList:
          handler_response = handleFileMigrationReportList(*obj,*oraSvc,requester);
          break;
        case OBJ_FileRecallReportList:
          handler_response = handleFileRecallReportList(*obj,*oraSvc,requester);
          break;
        case OBJ_FilesToMigrateListRequest:
          handler_response = handleFilesToMigrateListRequest(*obj,*oraSvc,requester);
          break;
        case OBJ_FilesToRecallListRequest:
          handler_response = handleFilesToRecallListRequest(*obj,*oraSvc,requester);
          break;
        default:
          //object not valid
          delete handler_response;
          castor::log::write(LOG_ERR, "Worker: invalid request", params);
          return;
      }
      // send back the response
      std::unique_ptr<castor::IObject> response(handler_response);
      try {

	castor::log::write(LOG_DEBUG,"Worker: responding to the Tapebridge", params);

	sock->sendObject(*response);

      }catch (castor::exception::Exception& e){
        std::list<castor::log::Param> params = {
            castor::log::Param("IP",  castor::log::IPAddress(requester.ip)),
            castor::log::Param("Port",requester.port),
            castor::log::Param("HostName",requester.hostName),
            castor::log::Param("errorCode",sstrerror(e.code())),
            castor::log::Param("errorMessage",e.getMessage().str())
        };
	castor::log::write(LOG_ERR,"Worker: cannot respond to the Tapebridge", params);
      }

    } catch (castor::exception::Exception& e) {
      
      // "Unable to read Request from socket" message
      
      std::list<castor::log::Param> params = {
	  castor::log::Param("IP",  castor::log::IPAddress(requester.ip)),
	  castor::log::Param("Port",requester.port),
	  castor::log::Param("HostName",requester.hostName),
	  castor::log::Param("errorCode",sstrerror(e.code())),
	  castor::log::Param("errorMessage",e.getMessage().str())
	};
	
      castor::log::write(LOG_ERR,"Worker: cannot receive message", params);
      return;
    }
    catch( std::bad_cast &ex){
      // "Invalid Request" message
      std::list<castor::log::Param> params = {
          castor::log::Param("IP",  castor::log::IPAddress(requester.ip)),
          castor::log::Param("Port",requester.port),
          castor::log::Param("HostName",requester.hostName)
      };
      castor::log::write(LOG_ERR, "Worker: invalid request", params);
	return;
    }
  } catch(...) {
    //castor one are trapped before 
    castor::log::write(LOG_ERR, "Worker: received volume request");
    
  }

}

std::string castor::tape::tapegateway::WorkerThread::requesterInfo::str ()
{
  // Pretty print the requester info into a string
  std::stringstream ctxt;
  ctxt << "HostName=" << hostName << " Port=" << port
      << " IP=" << std::dec;
  // Chopping of the ulong (u32) IP representation into traditional
  // decimal bytes and dots.
  for (int i=0; i<4; i++) {
    // If i is non-zero, add separator.
    if (i) ctxt << ".";
    ctxt << ( ( ip >> (3-i)*8) & 0xFF);
  }
  return ctxt.str();
}

castor::IObject* castor::tape::tapegateway::WorkerThread::handleStartWorker(
    castor::IObject&  obj, castor::tape::tapegateway::ITapeGatewaySvc& oraSvc,
    requesterInfo& requester  ) {

  // I received a start worker request
  Volume* response=new Volume();

  castor::log::write(LOG_DEBUG, "Worker: getting volume from db");

  try{
    VolumeRequest &startRequest = dynamic_cast<VolumeRequest&>(obj);
    std::list<castor::log::Param> params = {
        castor::log::Param("IP",  castor::log::IPAddress(requester.ip)),
        castor::log::Param("Port",requester.port),
        castor::log::Param("HostName",requester.hostName),
        castor::log::Param("mountTransactionId", startRequest.mountTransactionId()),
        castor::log::Param("tapebridgeTransId", startRequest.aggregatorTransactionId()),
    };
    
    castor::log::write(LOG_DEBUG, "Worker: getting volume from db", params);

    castor::utils::Timer timer;
    try {
    
      // GET FROM DB
      // The wrapper function has no hidden effect
      // The SQL commits in all cases.
      oraSvc.startTapeSession(startRequest, *response); 
    
    } catch (castor::exception::Exception& e){
      if (response) delete response;

      EndNotificationErrorReport* errorReport=new EndNotificationErrorReport();
      errorReport->setErrorCode(e.code());
      errorReport->setErrorMessage(e.getMessage().str());
      errorReport->setMountTransactionId(startRequest.mountTransactionId());
      errorReport->setAggregatorTransactionId(startRequest.aggregatorTransactionId());
    
      std::list<castor::log::Param> params ={
          castor::log::Param("IP",  castor::log::IPAddress(requester.ip)),
          castor::log::Param("Port",requester.port),
          castor::log::Param("HostName",requester.hostName),
          castor::log::Param("mountTransactionId", startRequest.mountTransactionId()),
          castor::log::Param("tapebridgeTransId", startRequest.aggregatorTransactionId()),
          castor::log::Param("errorCode",sstrerror(e.code())),
          castor::log::Param("errorMessage",e.getMessage().str())
	};

      castor::log::write(LOG_ERR, "Worker: no volume found", params);
      return errorReport;
    }
     
    if (response->mountTransactionId()==0 ) {
      // I don't have anything to recall I send a NoMoreFiles
      castor::log::write(LOG_INFO,"Worker: no file found for such volume", params);
      
      delete response;

      NoMoreFiles* noMore= new NoMoreFiles();
      noMore->setMountTransactionId(startRequest.mountTransactionId());
      noMore->setAggregatorTransactionId(startRequest.aggregatorTransactionId());
      return noMore;
    }
    
    response->setAggregatorTransactionId(startRequest.aggregatorTransactionId());
   
    std::list<castor::log::Param> params0 ={
        castor::log::Param("IP",  castor::log::IPAddress(requester.ip)),
        castor::log::Param("Port",requester.port),
        castor::log::Param("HostName",requester.hostName),
        castor::log::Param("mountTransactionId", startRequest.mountTransactionId()),
        castor::log::Param("tapebridgeTransId", startRequest.aggregatorTransactionId()),
        castor::log::Param("TPVID",response->vid()),
        castor::log::Param("mode",response->mode()),
        castor::log::Param("label",response->label()),
        castor::log::Param("density",response->density()),
        castor::log::Param("ProcessingTime", timer.secs())
      };
    
    castor::log::write(LOG_INFO, "Worker: volume found", params0);

  } catch( std::bad_cast &ex) {

    if (response) delete response;
    // "Invalid Request" message
    castor::log::write(LOG_ALERT, "Worker: invalid cast");
    EndNotificationErrorReport* errorReport=new EndNotificationErrorReport();
    errorReport->setErrorCode(EINVAL);
    errorReport->setErrorMessage("invalid object");
    return errorReport;
  }

  return response;

}

castor::IObject*  castor::tape::tapegateway::WorkerThread::handleEndWorker(
    castor::IObject&  obj, castor::tape::tapegateway::ITapeGatewaySvc&  oraSvc,
    requesterInfo& requester) {
  // I received an EndTransferRequest, I send back an EndTransferResponse
  std::unique_ptr <NotificationAcknowledge> response(new NotificationAcknowledge());
  EndNotification * pEndRep =  dynamic_cast<EndNotification *>(&obj);
  if (!pEndRep) {
    // "Invalid Request" message
    castor::log::write(LOG_INFO, "Worker: invalid cast");
    std::unique_ptr<EndNotificationErrorReport> errorReport(new EndNotificationErrorReport());
    errorReport->setErrorCode(EINVAL);
    errorReport->setErrorMessage("invalid object");
    return errorReport.release();
  }
  EndNotification& endRequest = *pEndRep;
  pEndRep = NULL;

  // Log the end notification.
  {
    std::list<castor::log::Param> params ={
        castor::log::Param("IP",  castor::log::IPAddress(requester.ip)),
        castor::log::Param("Port",requester.port),
        castor::log::Param("HostName",requester.hostName),
        castor::log::Param("mountTransactionId", endRequest.mountTransactionId()),
        castor::log::Param("tapebridgeTransId", endRequest.aggregatorTransactionId())
    };
    castor::log::write(LOG_INFO, "Worker: received end transaction notification", params);
  }
  response->setMountTransactionId(endRequest.mountTransactionId());
  response->setAggregatorTransactionId(endRequest.aggregatorTransactionId());
  castor::utils::Timer timer;
  try {
    // ACCESS DB to get tape to release
    castor::tape::tapegateway::ITapeGatewaySvc::TapeToReleaseInfo tape;
    // Straightforward wrapper, read-only sql.
    oraSvc.getTapeToRelease(endRequest.mountTransactionId(),tape);
    std::list<castor::log::Param> paramsComplete ={
        castor::log::Param("IP",  castor::log::IPAddress(requester.ip)),
        castor::log::Param("Port",requester.port),
        castor::log::Param("HostName",requester.hostName),
        castor::log::Param("mountTransactionId", endRequest.mountTransactionId()),
        castor::log::Param("tapebridgeTransId", endRequest.aggregatorTransactionId()),
        castor::log::Param("TPVID",tape.vid),
        castor::log::Param("mode",tape.mode),
        castor::log::Param("ProcessingTime", timer.secs())
    };
    castor::log::write(LOG_DEBUG, "Worker: getting tape used", paramsComplete);
    try {
      // UPDATE VMGR
      if (tape.mode == WRITE_ENABLE) { // just for write case
        if (tape.full) {
          // The tape has been determined as full during a file operation (in error report)
          // Set it as FULL in the VMGR.
          try {
            timer.reset();
            VmgrTapeGatewayHelper::setTapeAsFull(tape.vid, m_shuttingDown);
            std::list<castor::log::Param> paramsVmgr ={
                castor::log::Param("IP",  castor::log::IPAddress(requester.ip)),
                castor::log::Param("Port",requester.port),
                castor::log::Param("HostName",requester.hostName),
                castor::log::Param("mountTransactionId", endRequest.mountTransactionId()),
                castor::log::Param("tapebridgeTransId", endRequest.aggregatorTransactionId()),
                castor::log::Param("TPVID",tape.vid),
                castor::log::Param("mode",tape.mode),
                castor::log::Param("ProcessingTime", timer.secs())
            };
            castor::log::write(LOG_INFO, "Worker: set tape as FULL", paramsVmgr);
          } catch (castor::exception::Exception& e) {
            std::list<castor::log::Param> params ={
                castor::log::Param("IP",  castor::log::IPAddress(requester.ip)),
                castor::log::Param("Port",requester.port),
                castor::log::Param("HostName",requester.hostName),
                castor::log::Param("mountTransactionId", endRequest.mountTransactionId()),
                castor::log::Param("tapebridgeTransId", endRequest.aggregatorTransactionId()),
                castor::log::Param("TPVID", tape.vid),
                castor::log::Param("errorCode",sstrerror(e.code())),
                castor::log::Param("errorMessage",e.getMessage().str())
            };
            castor::log::write(LOG_ERR, "Worker: cannot set the tape as FULL", params);
          }
        }
        timer.reset();
        VmgrTapeGatewayHelper::resetBusyTape(tape.vid, m_shuttingDown);
        std::list<castor::log::Param> paramsVmgr ={
            castor::log::Param("IP",  castor::log::IPAddress(requester.ip)),
            castor::log::Param("Port",requester.port),
            castor::log::Param("HostName",requester.hostName),
            castor::log::Param("mountTransactionId", endRequest.mountTransactionId()),
            castor::log::Param("tapebridgeTransId", endRequest.aggregatorTransactionId()),
            castor::log::Param("TPVID",tape.vid),
            castor::log::Param("mode",tape.mode),
            castor::log::Param("ProcessingTime", timer.secs())
        };
        castor::log::write(LOG_INFO, "Worker: releasing BUSY tape after end transaction", paramsVmgr);
      }
    } catch (castor::exception::Exception& e) {
      std::list<castor::log::Param> params ={
          castor::log::Param("IP",  castor::log::IPAddress(requester.ip)),
          castor::log::Param("Port",requester.port),
          castor::log::Param("HostName",requester.hostName),
          castor::log::Param("mountTransactionId", endRequest.mountTransactionId()),
          castor::log::Param("tapebridgeTransId", endRequest.aggregatorTransactionId()),
          castor::log::Param("TPVID", tape.vid),
          castor::log::Param("errorCode",sstrerror(e.code())),
          castor::log::Param("errorMessage",e.getMessage().str())
      };
      castor::log::write(LOG_ERR, "Worker: cannot release BUSY tape after end transaction", params);
    }
    // ACCESS DB now we mark it as done
    timer.reset();
    // Wrapper function is straightforward. No trap.
    // SQL commits in the end but could fail in many selects.
    // Rollback required for this one.
    oraSvc.endTapeSession(endRequest.mountTransactionId());
    std::list<castor::log::Param> paramsDb ={
        castor::log::Param("IP",  castor::log::IPAddress(requester.ip)),
        castor::log::Param("Port",requester.port),
        castor::log::Param("HostName",requester.hostName),
        castor::log::Param("mountTransactionId", endRequest.mountTransactionId()),
        castor::log::Param("tapebridgeTransId", endRequest.aggregatorTransactionId()),
        castor::log::Param("TPVID",tape.vid),
        castor::log::Param("mode",tape.mode),
        castor::log::Param("ProcessingTime", timer.secs())
    };
    castor::log::write(LOG_INFO,"Worker: updating db after end transaction", paramsDb);
  } catch (castor::exception::Exception& e){
    std::list<castor::log::Param> params ={
        castor::log::Param("IP",  castor::log::IPAddress(requester.ip)),
        castor::log::Param("Port",requester.port),
        castor::log::Param("HostName",requester.hostName),
        castor::log::Param("mountTransactionId", endRequest.mountTransactionId()),
        castor::log::Param("tapebridgeTransId", endRequest.aggregatorTransactionId()),
        castor::log::Param("errorCode",sstrerror(e.code())),
        castor::log::Param("errorMessage",e.getMessage().str())
    };
    castor::log::write(LOG_ERR, "Worker: db error while updating for end transaction", params);
    return response.release();
  }
  return response.release();
}

castor::IObject*  castor::tape::tapegateway::WorkerThread::handleFailWorker(
    castor::IObject&  obj, castor::tape::tapegateway::ITapeGatewaySvc&  oraSvc,
    requesterInfo& requester ) {
  // We received an EndNotificationErrorReport
  EndNotificationErrorReport *pEndErrRep =  dynamic_cast<EndNotificationErrorReport *>(&obj);
  if (!pEndErrRep) {
    // "Invalid Request" message
    castor::log::write(LOG_INFO, "Worker: invalid cast");
    std::unique_ptr<EndNotificationErrorReport> errorReport(new EndNotificationErrorReport());
    errorReport->setErrorCode(EINVAL);
    errorReport->setErrorMessage("invalid object");
    return errorReport.release();
  }
  EndNotificationErrorReport& endRequest = *pEndErrRep;
  pEndErrRep = NULL;
  std::unique_ptr<NotificationAcknowledge> response(new NotificationAcknowledge());
  std::list<castor::log::Param> params ={
      castor::log::Param("IP",  castor::log::IPAddress(requester.ip)),
      castor::log::Param("Port",requester.port),
      castor::log::Param("HostName",requester.hostName),
      castor::log::Param("mountTransactionId", endRequest.mountTransactionId()),
      castor::log::Param("tapebridgeTransId", endRequest.aggregatorTransactionId()),
      castor::log::Param("errorcode", endRequest.errorCode()),
      castor::log::Param("errorMessage", endRequest.errorMessage())
  };
  // Depending on the error we get, this can be an innocent end of tape or something else.
  // The ENOSPC case is business as usual. Rest remains an error.
  castor::log::write((endRequest.errorCode() == ENOSPC)?LOG_INFO:LOG_ERR,
                     "Worker: received end notification error report", params);
  response->setMountTransactionId(endRequest.mountTransactionId());
  response->setAggregatorTransactionId(endRequest.aggregatorTransactionId());
  castor::utils::Timer timer;
  try {
    // ACCESS DB to get tape to release
    castor::tape::tapegateway::ITapeGatewaySvc::TapeToReleaseInfo tape;
    // Safe, read only SQL
    oraSvc.getTapeToRelease(endRequest.mountTransactionId(), tape);
    std::list<castor::log::Param> paramsComplete ={
        castor::log::Param("IP",  castor::log::IPAddress(requester.ip)),
        castor::log::Param("Port",requester.port),
        castor::log::Param("HostName",requester.hostName),
        castor::log::Param("mountTransactionId", endRequest.mountTransactionId()),
        castor::log::Param("tapebridgeTransId", endRequest.aggregatorTransactionId()),
        castor::log::Param("TPVID",tape.vid),
        castor::log::Param("mode",tape.mode),
        castor::log::Param("ProcessingTime", timer.secs())
    };
    castor::log::write(LOG_DEBUG, "Worker: getting tape used during failure", paramsComplete);
    // Update VMGR just for write case, we release/mark the tape.
    if (tape.mode == WRITE_ENABLE) {
      // CHECK IF THE ERROR WAS DUE TO A FULL TAPE, or if the tape was previously marked as full
      if (endRequest.errorCode() == ENOSPC || tape.full) {
        try {
          timer.reset();
          VmgrTapeGatewayHelper::setTapeAsFull(tape.vid, m_shuttingDown);
          std::list<castor::log::Param> paramsVmgr ={
              castor::log::Param("IP",  castor::log::IPAddress(requester.ip)),
              castor::log::Param("Port",requester.port),
              castor::log::Param("HostName",requester.hostName),
              castor::log::Param("mountTransactionId", endRequest.mountTransactionId()),
              castor::log::Param("tapebridgeTransId", endRequest.aggregatorTransactionId()),
              castor::log::Param("TPVID",tape.vid),
              castor::log::Param("mode",tape.mode),
              castor::log::Param("ProcessingTime", timer.secs())
          };
          castor::log::write(LOG_INFO, "Worker: set tape as FULL", paramsVmgr);
        } catch (castor::exception::Exception& e) {
          std::list<castor::log::Param> params ={
              castor::log::Param("IP",  castor::log::IPAddress(requester.ip)),
              castor::log::Param("Port",requester.port),
              castor::log::Param("HostName",requester.hostName),
              castor::log::Param("mountTransactionId", endRequest.mountTransactionId()),
              castor::log::Param("tapebridgeTransId", endRequest.aggregatorTransactionId()),
              castor::log::Param("TPVID", tape.vid),
              castor::log::Param("errorCode",sstrerror(e.code())),
              castor::log::Param("errorMessage",e.getMessage().str())
          };
          castor::log::write(LOG_ERR, "Worker: cannot set the tape as FULL", params);
        }
      } else {
        try {
          // We just release the tape
          timer.reset();
          VmgrTapeGatewayHelper::resetBusyTape(tape.vid, m_shuttingDown);
          std::list<castor::log::Param> paramsVmgr ={
              castor::log::Param("IP",  castor::log::IPAddress(requester.ip)),
              castor::log::Param("Port",requester.port),
              castor::log::Param("HostName",requester.hostName),
              castor::log::Param("mountTransactionId", endRequest.mountTransactionId()),
              castor::log::Param("tapebridgeTransId", endRequest.aggregatorTransactionId()),
              castor::log::Param("TPVID",tape.vid),
              castor::log::Param("mode",tape.mode),
              castor::log::Param("ProcessingTime", timer.secs())
          };
          castor::log::write(LOG_INFO, "Worker: releasing BUSY tape after end notification error report", paramsVmgr);
        } catch (castor::exception::Exception& e) {
          std::list<castor::log::Param> params ={
              castor::log::Param("IP",  castor::log::IPAddress(requester.ip)),
              castor::log::Param("Port",requester.port),
              castor::log::Param("HostName",requester.hostName),
              castor::log::Param("mountTransactionId", endRequest.mountTransactionId()),
              castor::log::Param("tapebridgeTransId", endRequest.aggregatorTransactionId()),
              castor::log::Param("TPVID", tape.vid),
              castor::log::Param("errorCode",sstrerror(e.code())),
              castor::log::Param("errorMessage",e.getMessage().str())
          };
          castor::log::write(LOG_ERR, "Worker: cannot release BUSY tape after end transaction", params);
        }
      }
    }
    // ACCESS db now we fail it
    timer.reset();
    // Direct wrapper, committing SQL
    oraSvc.endTapeSession(endRequest.mountTransactionId(), endRequest.errorCode());
    std::list<castor::log::Param> paramsDb ={
        castor::log::Param("IP",  castor::log::IPAddress(requester.ip)),
        castor::log::Param("Port",requester.port),
        castor::log::Param("HostName",requester.hostName),
        castor::log::Param("mountTransactionId", endRequest.mountTransactionId()),
        castor::log::Param("tapebridgeTransId", endRequest.aggregatorTransactionId()),
        castor::log::Param("TPVID",tape.vid),
        castor::log::Param("mode",tape.mode),
        castor::log::Param("ProcessingTime", timer.secs())
    };
    castor::log::write(LOG_INFO,"Worker: updating db after end notification error report", paramsDb);
  } catch (castor::exception::Exception& e){
    std::list<castor::log::Param> params ={
        castor::log::Param("IP",  castor::log::IPAddress(requester.ip)),
        castor::log::Param("Port",requester.port),
        castor::log::Param("HostName",requester.hostName),
        castor::log::Param("mountTransactionId", endRequest.mountTransactionId()),
        castor::log::Param("tapebridgeTransId", endRequest.aggregatorTransactionId()),
        castor::log::Param("errorCode",sstrerror(e.code())),
        castor::log::Param("errorMessage",e.getMessage().str())
    };
    castor::log::write(LOG_ERR, "Worker: db error while updating for end notification error report", params);
    return response.release();
  }
  return  response.release();
}

castor::IObject*  castor::tape::tapegateway::WorkerThread::handleFileMigrationReportList(
    castor::IObject&  obj, castor::tape::tapegateway::ITapeGatewaySvc&  oraSvc,
    requesterInfo& requester ) {
  // first check we are called with the proper class
  FileMigrationReportList* rep = dynamic_cast<FileMigrationReportList *>(&obj);
  if (!rep) {
    // "Invalid Request" message
    castor::log::write(LOG_ERR, "Worker: invalid cast");
    EndNotificationErrorReport* errorReport=new EndNotificationErrorReport();
    errorReport->setErrorCode(EINVAL);
    errorReport->setErrorMessage("invalid object");
    return errorReport;
  }
  FileMigrationReportList & fileMigrationReportList = *rep;
  rep = NULL;

  // Log a summary for the report
  {
    std::list<castor::log::Param> params ={
        castor::log::Param("IP",  castor::log::IPAddress(requester.ip)),
        castor::log::Param("Port",requester.port),
        castor::log::Param("HostName",requester.hostName),
        castor::log::Param("mountTransactionId", fileMigrationReportList.mountTransactionId()),
        castor::log::Param("tapebridgeTransId", fileMigrationReportList.aggregatorTransactionId()),
        castor::log::Param("successes", fileMigrationReportList.successfulMigrations().size()),
        castor::log::Param("failures", fileMigrationReportList.failedMigrations().size()),
    };
    castor::log::write(LOG_INFO, "Worker: received a migration report list", params);
  }

  // We have 2 lists of both successes and errors.
  // The database side of the application will take care of updating both the
  // name server and the stager database.
  // The VMGR is still the responsibility of the gateway.

  // Check we are not confronted too early with a future feature of the tape bridge (update of fseq in
  // VMGR from tape server side instead of from gateway side).
  if (fileMigrationReportList.fseqSet()) {
    // fseq should not have been set by tape server. Error and end tape session.
    // We got an unexpected message type. Internal error. Better stop here.
    std::string report = "fseqSet flag set by tape bridge.";
    report += "This is not supported yet in this version of the tape gateway. ";
    report += "Please check deployment release notes. ";
    report += "In handleFileMigrationReportList: aborting the session.";
    std::list<castor::log::Param> params ={
        castor::log::Param("IP",  castor::log::IPAddress(requester.ip)),
        castor::log::Param("Port",requester.port),
        castor::log::Param("HostName",requester.hostName),
        castor::log::Param("mountTransactionId", fileMigrationReportList.mountTransactionId()),
        castor::log::Param("tapebridgeTransId", fileMigrationReportList.aggregatorTransactionId()),
        castor::log::Param("errorCode",SEINTERNAL),
        castor::log::Param("errorMessage",report)
    };
    castor::log::write(LOG_ERR, "Internal error", params);
    // Abort all the rest.
    std::unique_ptr<EndNotificationErrorReport> errorReport (new EndNotificationErrorReport());
    errorReport->setErrorCode(SEINTERNAL);
    errorReport->setErrorMessage(report);
    return errorReport.release();
  }

  // Step 1: get the VID from the DB (once for all files)
  // Get the VID from the DB (and the tape pool)
  std::string vid;
  std::string tapePool;
  try {
    oraSvc.getMigrationMountVid(fileMigrationReportList.mountTransactionId(), vid, tapePool);
  } catch (castor::exception::Exception e) {
    // No Vid, not much we can do... The DB is failing on us. We can try to
    // mark the session as closing, be better warn the tape server of a problem
    // so we have a chance to not loop.
    logMigrationCannotFindVid (nullCuuid, requester, fileMigrationReportList, e);
    // This helper procedure will log its own failures.
    setSessionToClosing(oraSvc, requester, fileMigrationReportList.mountTransactionId());
    EndNotificationErrorReport* errorReport=new EndNotificationErrorReport();
    errorReport->setErrorCode(ENOENT);
    errorReport->setErrorMessage("Session not found in DB.");
    errorReport->setMountTransactionId(fileMigrationReportList.mountTransactionId());
    errorReport->setAggregatorTransactionId(fileMigrationReportList.aggregatorTransactionId());
    return errorReport;
  }

  // Step 2: update the VMGR, so that the written tape copies are safe from overwrite.
  // Failing here is fatal.
  // We can update the vmgr in one go here, reporting for several files at a time.
  // XXX Limitation: today, any migration error is fatal and terminates the migration
  // session. This means only record the successes in the VMGR.
  // When the tape server will handle failed migrations gracefully (leave a "hole"
  // on the tape and carry on), we will have
  // the record the empty hole left by the failure in the middle of the successes.
  // Today we just bump up the tape's statistics for the successes.
  if (!fileMigrationReportList.successfulMigrations().empty()) {
    // 2.1 Build the statistics of the successes. Also log the reception of them.
    u_signed64 totalBytes = 0, totalCompressedBytes = 0;
    int highestFseq = -1;
    int lowestFseq = INT_MAX;

    for (std::vector<FileMigratedNotificationStruct *>::iterator sm =
           fileMigrationReportList.successfulMigrations().begin();
           sm < fileMigrationReportList.successfulMigrations().end(); sm++) {
      totalBytes += (*sm)->fileSize();
      // We break our space usage estimates if the compressed file size is less
      // than a byte. So we do not count for a compressed size of less than 1.
      u_signed64 cfs = (*sm)->compressedFileSize();
      if (!cfs) cfs = 1;
      totalCompressedBytes += cfs;
      highestFseq = std::max((*sm)->fseq(), highestFseq);
      lowestFseq =  std::min((*sm)->fseq(), lowestFseq);
      // Log the file notification while we're at it.
      logMigrationNotified(nullCuuid, fileMigrationReportList.mountTransactionId(),
          fileMigrationReportList.aggregatorTransactionId(), (*sm)->fileid(), requester, **sm);
    }

    // 2.2 As mentioned above, the tape server does not carry on on errors.
    // That means the successes should be a continuous streak of fseqs, and all
    // failures should have a fseq higher than any of the successes.
    // 2.2.1 Check continuity
    if (highestFseq - lowestFseq + 1 !=
        (int) fileMigrationReportList.successfulMigrations().size()) {
      castor::exception::Exception e;
      e.getMessage() << "In handleFileMigrationReportList, mismatching "
          << "fSeqs and number of successes: lowestFseq=" << lowestFseq
          << " highestFseq=" << highestFseq << " expectedNumber="
          << highestFseq - lowestFseq + 1 <<  " actualNumber="
          << fileMigrationReportList.successfulMigrations().size();
      logInternalError(e, requester, fileMigrationReportList);
      std::unique_ptr<EndNotificationErrorReport> errorReport (new EndNotificationErrorReport());
      errorReport->setErrorCode(EIO);
      errorReport->setErrorMessage("Wrong successes number/fseq range mismatch");
      errorReport->setMountTransactionId(fileMigrationReportList.mountTransactionId());
      errorReport->setAggregatorTransactionId(fileMigrationReportList.aggregatorTransactionId());
      return errorReport.release();
    }

    // 2.2.2 Check that any failure fseq is above the highest success fSeq
    for (std::vector<FileErrorReportStruct*>::iterator f =
        fileMigrationReportList.failedMigrations().begin();
        f < fileMigrationReportList.failedMigrations().end();
        f++) {
      if ( (*f)->fseq() <= highestFseq) {
        castor::exception::Exception e;
        e.getMessage() << "In handleFileMigrationReportList, mismatching "
            << "fSeqs for failure, lower than highest success fSeq: failureFSeq="
            << (*f)->fseq() << " failureNsFileId=" << (*f)->fileid()
            << " failureFileTransactionId=" << (*f)->fileTransactionId()
            << " failureErrorCode=" << (*f)->errorCode()
            << " failureErrorMessage=" << (*f)->errorMessage()
            << " highestSuccessFseq=" << highestFseq;
        logInternalError(e, requester, fileMigrationReportList);
        std::unique_ptr<EndNotificationErrorReport> errorReport (new EndNotificationErrorReport());
        errorReport->setErrorCode(EIO);
        errorReport->setErrorMessage("Error report interleaved in successes. This is not supported (nor expected)");
        errorReport->setMountTransactionId(fileMigrationReportList.mountTransactionId());
        errorReport->setAggregatorTransactionId(fileMigrationReportList.aggregatorTransactionId());
        return errorReport.release();
      }
    }

    // 2.3 Update the VMGR for fseq. Stop here in case of failure.
    castor::utils::Timer timer;
    try {
      VmgrTapeGatewayHelper::bulkUpdateTapeInVmgr(
          fileMigrationReportList.successfulMigrations().size(),
          highestFseq, totalBytes, totalCompressedBytes,
          vid, m_shuttingDown);
      logMigrationBulkVmgrUpdate(nullCuuid, requester, fileMigrationReportList,
        fileMigrationReportList.successfulMigrations().size(),
        highestFseq, totalBytes, totalCompressedBytes,
        tapePool, vid, timer.usecs());
    } catch (castor::exception::Exception& e){
      logMigrationVmgrFailure(nullCuuid, requester, fileMigrationReportList, vid,
        tapePool, e);
      // We stop processing here. Unfortunately, the data we just wrote is not
      // protected by the VMGR's fseq record, so we cannot reference it in the 
      // name server. The older data is protected on the next mount by both
      // previously updated VMGR's fseq and ns information, which are crossed
      // in the tape gateway before mounting for write.
      
      // We could not update the VMGR for this tape: better leave it alone and stop the session
      EndNotificationErrorReport* errorReport=new EndNotificationErrorReport();
      errorReport->setErrorCode(EIO);
      errorReport->setErrorMessage("Failed to update VMGR");
      errorReport->setMountTransactionId(fileMigrationReportList.mountTransactionId());
      errorReport->setAggregatorTransactionId(fileMigrationReportList.aggregatorTransactionId());
      return errorReport;
    }
  }

  {
    // 2.4 Scan all errors to find tape full condition for more update of the
    // VMGR (at end session). Right now, the full status is just recorded in the
    // stager DB, to be used at end of tape session.
    // This is tape full conditions currently.
    bool tapeFull = false;
    for (std::vector<FileErrorReportStruct *>::iterator fm =
        fileMigrationReportList.failedMigrations().begin();
        fm < fileMigrationReportList.failedMigrations().end(); fm++){
      fileErrorClassification cl = classifyBridgeMigrationFileError((*fm)->errorCode());
      tapeFull |= cl.tapeIsFull;
    }
    if (tapeFull) {
      try {
        oraSvc.flagTapeFullForMigrationSession(fileMigrationReportList.mountTransactionId());
      } catch (castor::exception::Exception e){
        logInternalError (e, requester, fileMigrationReportList);
      }
    }
  }

  // Step 3: update NS and stager DB.
  // Once we passed that point, whatever is on tape is safe. We are free to update
  // the name server, and the record "job done" in stager DB. Both steps are
  // handled by the stager DB as is has a DB link to the name server's.
  try {
    oraSvc.setBulkFileMigrationResult(
        requester.str(),
        fileMigrationReportList.mountTransactionId(),
        fileMigrationReportList.successfulMigrations(),
        fileMigrationReportList.failedMigrations());
  } catch (castor::exception::Exception e) {
    logInternalError (e, requester, fileMigrationReportList);
  }

  // Log completion of the processing.
  {
    std::list<castor::log::Param> params ={
        castor::log::Param("IP",  castor::log::IPAddress(requester.ip)),
        castor::log::Param("Port",requester.port),
        castor::log::Param("HostName",requester.hostName),
        castor::log::Param("mountTransactionId", fileMigrationReportList.mountTransactionId()),
        castor::log::Param("tapebridgeTransId", fileMigrationReportList.aggregatorTransactionId())
    };
    castor::log::write(LOG_INFO, "Worker: finished processing a migration report list", params);
  }
  std::unique_ptr <NotificationAcknowledge> ack (new NotificationAcknowledge());
  ack->setAggregatorTransactionId(fileMigrationReportList.aggregatorTransactionId());
  ack->setMountTransactionId(fileMigrationReportList.mountTransactionId());
  return ack.release();
}

castor::IObject*  castor::tape::tapegateway::WorkerThread::handleFileRecallReportList(
    castor::IObject&  obj, castor::tape::tapegateway::ITapeGatewaySvc&  oraSvc,
    requesterInfo& requester ) {
  // Unlike the migrations, which are linked by the sequential writing, the recalls
  // Can be considered as independent entities.
  // We can then push the data to the DB blindly in both success and failure cases.

  // first check we are called with the proper class
  FileRecallReportList * recRep = dynamic_cast <FileRecallReportList *>(&obj);
  if (!recRep) {
    // We did not get the expected class
    // "Invalid Request" message
    castor::log::write(LOG_ERR, "Worker: invalid cast");
    EndNotificationErrorReport* errorReport=new EndNotificationErrorReport();
    errorReport->setErrorCode(EINVAL);
    errorReport->setErrorMessage("invalid object");
    return errorReport;
  }
  FileRecallReportList &fileRecallReportList = *recRep;
  recRep = NULL;

  // Log a summary for the report
  {
    std::list<castor::log::Param> params ={
        castor::log::Param("IP",  castor::log::IPAddress(requester.ip)),
        castor::log::Param("Port",requester.port),
        castor::log::Param("HostName",requester.hostName),
        castor::log::Param("mountTransactionId", fileRecallReportList.mountTransactionId()),
        castor::log::Param("tapebridgeTransId", fileRecallReportList.aggregatorTransactionId()),
        castor::log::Param("successes", fileRecallReportList.successfulRecalls().size()),
        castor::log::Param("failures", fileRecallReportList.failedRecalls().size()),
    };
    // "Worker: received a recall report list"
    castor::log::write(LOG_INFO, "Worker: received a recall report list", params);
  }

  // We have 2 lists of both successes and errors.
  // The database side of the application will take care of updating the stager database.
  // As opposed to migrations, the VMGR needs no update here.
  // results are also logged by the DB procedures.
  try {
    oraSvc.setBulkFileRecallResult(
        requester.str(),
        fileRecallReportList.mountTransactionId(),
        fileRecallReportList.successfulRecalls(),
        fileRecallReportList.failedRecalls());
  } catch (castor::exception::Exception e) {
    logInternalError (e, requester, fileRecallReportList);
  } catch (std::bad_cast bc) {
    castor::exception::Exception e;
    e.getMessage() << "Got a bad cast exception in handleFileRecallReportList: "
        << bc.what();
    logInternalError (e, requester, fileRecallReportList);
  }

  // Log completion of the processing.
  {
    std::list<castor::log::Param> params ={
        castor::log::Param("IP",  castor::log::IPAddress(requester.ip)),
        castor::log::Param("Port",requester.port),
        castor::log::Param("HostName",requester.hostName),
        castor::log::Param("mountTransactionId", fileRecallReportList.mountTransactionId()),
        castor::log::Param("tapebridgeTransId", fileRecallReportList.aggregatorTransactionId())
    };
    // "Worker: finished processing a recall report list"
    castor::log::write(LOG_INFO, "Worker: finished processing a recall report list", params);
  }
  std::unique_ptr <NotificationAcknowledge> ack (new NotificationAcknowledge());
  ack->setAggregatorTransactionId(fileRecallReportList.aggregatorTransactionId());
  ack->setMountTransactionId(fileRecallReportList.mountTransactionId());
  return ack.release();
}

castor::IObject*  castor::tape::tapegateway::WorkerThread::handleFilesToMigrateListRequest(
    castor::IObject&  obj, castor::tape::tapegateway::ITapeGatewaySvc&  oraSvc,
    requesterInfo& requester ) {
  // first check we are called with the proper class
  FilesToMigrateListRequest* req_p = dynamic_cast <FilesToMigrateListRequest *>(&obj);
  if (!req_p) {
    // We did not get the expected class
    // "Invalid Request" message
    castor::log::write(LOG_ERR, "Worker: invalid cast");
    EndNotificationErrorReport* errorReport=new EndNotificationErrorReport();
    errorReport->setErrorCode(EINVAL);
    errorReport->setErrorMessage("invalid object");
    return errorReport;
  }
  FilesToMigrateListRequest &ftmlr = *req_p;
  req_p = NULL;

  // Log the request
  std::list<castor::log::Param> params_outloop = {
      castor::log::Param("IP",  castor::log::IPAddress(requester.ip)),
      castor::log::Param("Port",requester.port),
      castor::log::Param("HostName",requester.hostName),
      castor::log::Param("mountTransactionId",ftmlr.mountTransactionId()),
      castor::log::Param("tapebridgeTransId",ftmlr.aggregatorTransactionId()),
      castor::log::Param("maxFiles",ftmlr.maxFiles()),
      castor::log::Param("maxBytes",ftmlr.maxBytes())
  };
  castor::log::write(LOG_DEBUG, "Worker: file to migrate requested",params_outloop);

  // Prepare the file list container
  std::queue <FileToMigrateStruct> files_list;
  bool dbFailure = false;
  double dbServingTime;
  castor::utils::Timer timer;

  // Transmit the request to the DB directly
  try {
    oraSvc.getBulkFilesToMigrate(requester.str(), ftmlr.mountTransactionId(),
        ftmlr.maxFiles(), ftmlr.maxBytes(),
        files_list);
  } catch (castor::exception::Exception& e) {
    std::list<castor::log::Param> params ={
        castor::log::Param("IP",  castor::log::IPAddress(requester.ip)),
        castor::log::Param("Port",requester.port),
        castor::log::Param("HostName",requester.hostName),
        castor::log::Param("mountTransactionId",ftmlr.mountTransactionId()),
        castor::log::Param("tapebridgeTransId",ftmlr.aggregatorTransactionId()),
        castor::log::Param("maxBytes",ftmlr.maxBytes()),
        castor::log::Param("maxFiles",ftmlr.maxFiles()),
        castor::log::Param("errorCode",sstrerror(e.code())),
        castor::log::Param("errorMessage",e.getMessage().str())
    };
    castor::log::write(LOG_ERR, "Worker: db error while retrieving file to migrate",params);
    // The tape server does not need to know about our messing up.
    // We just report no more files to it.
    dbFailure = true;
  }
  dbServingTime = timer.secs();
  // If it was a simple no more work, log it as such
  if (files_list.empty())
    castor::log::write(LOG_INFO, "Worker: no more file to migrate", params_outloop);

  // If the list turned up empty, or the DB blew up, reply with a no more files.
  if (dbFailure || files_list.empty()) {
    std::unique_ptr<NoMoreFiles> noMore(new NoMoreFiles);
    noMore->setMountTransactionId(ftmlr.mountTransactionId());
    noMore->setAggregatorTransactionId(ftmlr.aggregatorTransactionId());
    return noMore.release();
  }

  //The list is non-empty, we build the response and gather a few stats
  u_signed64 filesCount = 0, bytesCount = 0;
  std::unique_ptr<FilesToMigrateList> files_response(new FilesToMigrateList);
  files_response->setAggregatorTransactionId(ftmlr.aggregatorTransactionId());
  files_response->setMountTransactionId(ftmlr.mountTransactionId());
  while (!files_list.empty()) {
    std::unique_ptr<FileToMigrateStruct> ftm (new FileToMigrateStruct);
    *ftm = files_list.front();
    filesCount++;
    bytesCount+= ftm->fileSize();
    // Log the per-file information
    std::list<castor::log::Param> paramsComplete ={
        castor::log::Param("NSFILEID", ftm->fileid()),
        castor::log::Param("IP",  castor::log::IPAddress(requester.ip)),
        castor::log::Param("Port",requester.port),
        castor::log::Param("HostName",requester.hostName),
        castor::log::Param("mountTransactionId",ftmlr.mountTransactionId()),
        castor::log::Param("tapebridgeTransId",ftmlr.aggregatorTransactionId()),
        castor::log::Param("fseq",ftm->fseq()),
        castor::log::Param("path",ftm->path()),
        castor::log::Param("fileSize", ftm->fileSize()),
        castor::log::Param("lastKnownFileName", ftm->lastKnownFilename()),
        castor::log::Param("lastModificationTime", ftm->lastModificationTime()),
        castor::log::Param("fileTransactionId", ftm->fileTransactionId())
      };
    castor::log::write(LOG_INFO, "Worker: file to migrate retrieved from db", paramsComplete);
    // ... and store in the response.
    files_response->filesToMigrate().push_back(ftm.release());
    files_list.pop();
  }

  // Log the summary of the recall request
  {
    std::list<castor::log::Param> params = {
        castor::log::Param("IP",  castor::log::IPAddress(requester.ip)),
        castor::log::Param("Port",requester.port),
        castor::log::Param("HostName",requester.hostName),
        castor::log::Param("mountTransactionId",ftmlr.mountTransactionId()),
        castor::log::Param("tapebridgeTransId",ftmlr.aggregatorTransactionId()),
        castor::log::Param("maxFiles",ftmlr.maxFiles()),
        castor::log::Param("maxBytes",ftmlr.maxBytes()),
        castor::log::Param("filesCount",filesCount),
        castor::log::Param("totalSize",bytesCount),
        castor::log::Param("DbProcessingTime", dbServingTime),
        castor::log::Param("DbProcessingTimePerFile", dbServingTime/filesCount),
        castor::log::Param("ProcessingTime", timer.secs()),
        castor::log::Param("ProcessingTimePerFile", timer.secs()/filesCount)
    };
    castor::log::write(LOG_INFO, "Worker: files to migrate retrieved in bulk from db",params);
  }
  // and reply to requester.
  return files_response.release();
}

castor::IObject*  castor::tape::tapegateway::WorkerThread::handleFilesToRecallListRequest(
    castor::IObject&  obj, castor::tape::tapegateway::ITapeGatewaySvc&  oraSvc,
    requesterInfo& requester ) {
  // first check we are called with the proper class
  FilesToRecallListRequest * req = dynamic_cast <FilesToRecallListRequest *>(&obj);
  if (!req) {
    // We did not get the expected class
    // "Invalid Request" message
    castor::log::write(LOG_ERR, "Worker: invalid cast");
    EndNotificationErrorReport* errorReport=new EndNotificationErrorReport();
    errorReport->setErrorCode(EINVAL);
    errorReport->setErrorMessage("invalid object");
    return errorReport;
  }
  FilesToRecallListRequest &ftrlr = *req;
  req = NULL;

  // Log the request
  std::list<castor::log::Param> params_outloop = {
      castor::log::Param("IP",  castor::log::IPAddress(requester.ip)),
      castor::log::Param("Port",requester.port),
      castor::log::Param("HostName",requester.hostName),
      castor::log::Param("mountTransactionId",ftrlr.mountTransactionId()),
      castor::log::Param("tapebridgeTransId",ftrlr.aggregatorTransactionId()),
      castor::log::Param("maxFiles",ftrlr.maxFiles()),
      castor::log::Param("maxBytes",ftrlr.maxBytes())
  };
  castor::log::write(LOG_DEBUG, "Worker: file to recall requested", params_outloop);

  // Prepare the file list container
  std::queue <castor::tape::tapegateway::ITapeGatewaySvc::RAOFileToRecallStructWithContext> files_list;
  bool dbFailure = false;
  double dbServingTime;
  castor::utils::Timer timer;

  // Transmit the request to the DB directly
  try {
    oraSvc.getBulkFilesToRecall(requester.str(), ftrlr.mountTransactionId(),
        ftrlr.maxFiles(), ftrlr.maxBytes(),
        files_list);
  } catch (castor::exception::Exception& e) {
    std::list<castor::log::Param> params ={
        castor::log::Param("IP",  castor::log::IPAddress(requester.ip)),
        castor::log::Param("Port",requester.port),
        castor::log::Param("HostName",requester.hostName),
        castor::log::Param("mountTransactionId",ftrlr.mountTransactionId()),
        castor::log::Param("tapebridgeTransId",ftrlr.aggregatorTransactionId()),
        castor::log::Param("maxBytes",ftrlr.maxBytes()),
        castor::log::Param("maxFiles",ftrlr.maxFiles()),
        castor::log::Param("errorCode",sstrerror(e.code())),
        castor::log::Param("errorMessage",e.getMessage().str())
    };
    castor::log::write(LOG_ERR, "Worker: db error while retrieving file to recall", params);
    // The tape server does not need to know about our messing up.
    // We just report no more files to it.
    dbFailure = true;
  }

  dbServingTime = timer.secs();
  // If it was a simple no more work, log it as such
  if (files_list.empty())
    castor::log::write(LOG_INFO, "Worker: no more file to recall", params_outloop);

  // If the list turned up empty, or the DB blew up, reply with a no more files.
  if (dbFailure || files_list.empty()) {
    std::unique_ptr<NoMoreFiles> noMore(new NoMoreFiles);
    noMore->setMountTransactionId(ftrlr.mountTransactionId());
    noMore->setAggregatorTransactionId(ftrlr.aggregatorTransactionId());
    return noMore.release();
  }

  //The list is non-empty, we build the response and gather a few stats
  u_signed64 filesCount = 0;
  std::unique_ptr<RAOFilesToRecallList> files_response(new RAOFilesToRecallList);
  files_response->setAggregatorTransactionId(ftrlr.aggregatorTransactionId());
  files_response->setMountTransactionId(ftrlr.mountTransactionId());
  while (!files_list.empty()) {
    std::unique_ptr<RAOFileToRecallStruct> ftr (new RAOFileToRecallStruct);
    castor::tape::tapegateway::ITapeGatewaySvc::RAOFileToRecallStructWithContext & ftrwc = files_list.front();
    *ftr = ftrwc;
    filesCount++;
    // Log the per-file information
    std::stringstream blockid;
    blockid << std::hex << std::uppercase << std::noshowbase << std::setfill('0')
        << std::setw(2) << (int)ftr->blockId0()
        << std::setw(2) << (int)ftr->blockId1()
        << std::setw(2) << (int)ftr->blockId2()
        << std::setw(2) << (int)ftr->blockId3();
    std::list<castor::log::Param> paramsComplete ={
        castor::log::Param("NSFILEID", ftr->fileid()),
        castor::log::Param("IP",  castor::log::IPAddress(requester.ip)),
        castor::log::Param("Port",requester.port),
        castor::log::Param("HostName",requester.hostName),
        castor::log::Param("mountTransactionId",ftrlr.mountTransactionId()),
        castor::log::Param("tapebridgeTransId",ftrlr.aggregatorTransactionId()),
        castor::log::Param("fseq",ftr->fseq()),
        castor::log::Param("path",ftr->path()),
        castor::log::Param("blockId",blockid.str()),
        castor::log::Param("fileTransactionId", ftr->fileTransactionId()),
        castor::log::Param("copyNb", ftrwc.copyNb),
        castor::log::Param("eUid", ftrwc.eUid),
        castor::log::Param("eGid", ftrwc.eGid),
        castor::log::Param("TPVID", ftrwc.VID),
        castor::log::Param("fileSize", ftrwc.fileSize()),
        castor::log::Param("creationTime", ftrwc.creationTime),
        castor::log::Param("nbRetriesWithinMount", ftrwc.nbRetriesInMount),
        castor::log::Param("nbMounts", ftrwc.nbMounts)
    };
    // Handle the special case when blockId = 0: we should position by fseq
    // Said to be for fseq=1 in older code, unconfirmed.
    if (!(ftr->blockId0()|ftr->blockId1()|ftr->blockId2()|ftr->blockId3()))
      ftr->setPositionCommandCode(TPPOSIT_FSEQ);
    castor::log::write(LOG_INFO, "Worker: file to recall retrieved from db", paramsComplete);
    // ... and store in the response.
    files_response->filesToRecall().push_back(ftr.release());
    files_list.pop();
  }

  // Log the summary of the recall request
  {
    std::list<castor::log::Param> params = {
        castor::log::Param("IP",  castor::log::IPAddress(requester.ip)),
        castor::log::Param("Port",requester.port),
        castor::log::Param("HostName",requester.hostName),
        castor::log::Param("mountTransactionId",ftrlr.mountTransactionId()),
        castor::log::Param("tapebridgeTransId",ftrlr.aggregatorTransactionId()),
        castor::log::Param("maxFiles",ftrlr.maxFiles()),
        castor::log::Param("maxBytes",ftrlr.maxBytes()),
        castor::log::Param("filesCount",filesCount),
        castor::log::Param("DbProcessingTime", dbServingTime),
        castor::log::Param("DbProcessingTimePerFile", dbServingTime/filesCount),
        castor::log::Param("ProcessingTime", timer.secs()),
        castor::log::Param("ProcessingTimePerFile", timer.secs()/filesCount)
    };
    castor::log::write(LOG_INFO, "Worker: files to recall retrieved in bulk from db",params);
  }
  // and reply to requester.
  return files_response.release();
}

// Helper function for setting a session to closing in the DB.
// This is usually in reaction to a problem, which is logged in the caller.
// Comes with logging of its own problems as internal error (context in a
// string).
// As it's a last ditch action, there is no need to send further exceptions.
void castor::tape::tapegateway::WorkerThread::setSessionToClosing (
    castor::tape::tapegateway::ITapeGatewaySvc&  oraSvc,
    castor::tape::tapegateway::WorkerThread::requesterInfo& requester,
    u_signed64 mountTransactionId)
throw()
{
  try {
    oraSvc.endTapeSession(mountTransactionId);
  } catch (castor::exception::Exception e) {
    // If things still go wrong, log an internal error.
    std::stringstream report;
    report <<  "Failed to endTapeSession in the DB "
      "in: castor::tape::tapegateway::WorkerThread::setSessionToClosing: "
      "error=" << e.code() << " errorMessage=" << e.getMessage().str();
    std::list<castor::log::Param> params ={
        castor::log::Param("IP",  castor::log::IPAddress(requester.ip)),
        castor::log::Param("Port",requester.port),
        castor::log::Param("HostName",requester.hostName),
        castor::log::Param("mountTransactionId", mountTransactionId),
        castor::log::Param("errorCode",SEINTERNAL),
        castor::log::Param("errorMessage",report.str())
    };
    castor::log::write(LOG_ERR, "Internal error", params);
  }
}

castor::tape::tapegateway::WorkerThread::fileErrorClassification
castor::tape::tapegateway::WorkerThread::classifyBridgeMigrationFileError(int errorCode) throw() {
  // We rely on the default for the return values:
  //fileInvolved(true),
  //fileRetryable(true),
  //tapeIsFull(false)
  fileErrorClassification ret;
  if (errorCode == ENOSPC) {
    ret.tapeIsFull = true;
    ret.fileInvolved = false;
  }
  return ret;
}

castor::tape::tapegateway::WorkerThread::fileErrorClassification
castor::tape::tapegateway::WorkerThread::classifyBridgeRecallFileError(int /*errorCode*/) throw() {
  // We rely on the default for the return values:
  //fileInvolved(true),
  //fileRetryable(true),
  //tapeIsFull(false)
  fileErrorClassification ret;
  return ret;
}

void castor::tape::tapegateway::WorkerThread::logInternalError (
    castor::exception::Exception e, requesterInfo& requester,
    FileMigrationReportList & fileMigrationReportList) throw() {
  std::list<castor::log::Param> params ={
      castor::log::Param("IP",  castor::log::IPAddress(requester.ip)),
      castor::log::Param("Port",requester.port),
      castor::log::Param("HostName",requester.hostName),
      castor::log::Param("mountTransactionId", fileMigrationReportList.mountTransactionId()),
      castor::log::Param("tapebridgeTransId", fileMigrationReportList.aggregatorTransactionId()),
      castor::log::Param("errorCode",SEINTERNAL),
      castor::log::Param("errorMessage",e.getMessage().str())
  };
  castor::log::write(LOG_ERR, "Internal error", params);
}

void castor::tape::tapegateway::WorkerThread::logInternalError (
    castor::exception::Exception e, requesterInfo& requester,
    FileRecallReportList &fileRecallReportList) throw() {
  std::list<castor::log::Param> params ={
      castor::log::Param("IP",  castor::log::IPAddress(requester.ip)),
      castor::log::Param("Port",requester.port),
      castor::log::Param("HostName",requester.hostName),
      castor::log::Param("mountTransactionId", fileRecallReportList.mountTransactionId()),
      castor::log::Param("tapebridgeTransId", fileRecallReportList.aggregatorTransactionId()),
      castor::log::Param("errorCode",SEINTERNAL),
      castor::log::Param("errorMessage",e.getMessage().str())
  };
  castor::log::write(LOG_ERR, "Internal error", params);
}

void castor::tape::tapegateway::WorkerThread::logMigrationNotified (Cuuid_t uuid,
    u_signed64 mountTransactionId, u_signed64 aggregatorTransactionId,
    u_signed64 castorFileId,
    const requesterInfo& requester, const FileMigratedNotificationStruct & fileMigrated)
{
  std::stringstream checkSum;
  checkSum << std::hex << std::showbase << std::uppercase << fileMigrated.checksum();
  std::list<castor::log::Param> paramsComplete = {
      castor::log::Param("NSFILEID", castorFileId),
      castor::log::Param("REQID", uuid),
      castor::log::Param("IP",  castor::log::IPAddress(requester.ip)),
      castor::log::Param("Port",requester.port),
      castor::log::Param("HostName",requester.hostName),
      castor::log::Param("mountTransactionId",mountTransactionId),
      castor::log::Param("tapebridgeTransId",aggregatorTransactionId),
      castor::log::Param("fseq",fileMigrated.fseq()),
      castor::log::Param("checksum name",fileMigrated.checksumName()),
      castor::log::Param("checksum",checkSum.str()),
      castor::log::Param("fileSize",fileMigrated.fileSize()),
      castor::log::Param("compressedFileSize",fileMigrated.compressedFileSize()),
      castor::log::Param("blockid", tapeBlockIdToString(fileMigrated.blockId0(),
          fileMigrated.blockId1(), fileMigrated.blockId2(),
          fileMigrated.blockId3())),
      castor::log::Param("fileTransactionId",fileMigrated.fileTransactionId())
  };
  castor::log::write(LOG_DEBUG, "Worker: received migration notification", paramsComplete);
}

std::string castor::tape::tapegateway::WorkerThread::tapeBlockIdToString(
  const unsigned char blockId0,
  const unsigned char blockId1,
  const unsigned char blockId2,
  const unsigned char blockId3) throw() {
  std::ostringstream oss;

  oss << std::hex << std::setfill('0') <<
    std::setw(2) << (int)blockId0 <<
    std::setw(2) << (int)blockId1 <<
    std::setw(2) << (int)blockId2 <<
    std::setw(2) << (int)blockId3;

  return oss.str();
}

void castor::tape::tapegateway::WorkerThread::logMigrationBulkVmgrUpdate (Cuuid_t uuid,
    const requesterInfo& requester,  const FileMigrationReportList & fileMigrationReportList,
    int filesCount, u_signed64 highestFseq, u_signed64 totalBytes,
    u_signed64 totalCompressedBytes, const std::string & tapePool,
    const std::string & vid, signed64 procTime)
{
  std::list<castor::log::Param> paramsVmgr ={
      castor::log::Param("REQID", uuid),
      castor::log::Param("IP",  castor::log::IPAddress(requester.ip)),
      castor::log::Param("Port",requester.port),
      castor::log::Param("HostName",requester.hostName),
      castor::log::Param("mountTransactionId",fileMigrationReportList.mountTransactionId()),
      castor::log::Param("tapebridgeTransId",fileMigrationReportList.aggregatorTransactionId()),
      castor::log::Param("tapePool",tapePool),
      castor::log::Param("filesCount",filesCount),
      castor::log::Param("highestFseq",highestFseq),
      castor::log::Param("TPVID",vid),
      castor::log::Param("totalSize",totalBytes),
      castor::log::Param("compressedTotalSize",totalCompressedBytes),
      castor::log::Param("ProcessingTime", procTime * 0.000001)
  };
  castor::log::write(LOG_INFO, "Worker: updating vmgr for migrated file", paramsVmgr);
}

void castor::tape::tapegateway::WorkerThread::logMigrationVmgrFailure (
    Cuuid_t uuid, const requesterInfo& requester,
    const FileMigrationReportList &fileMigrationReportList,
    const std::string & vid, const std::string & tapePool,
    castor::exception::Exception & e)
{
  std::list<castor::log::Param> params ={
      castor::log::Param("REQID", uuid),
      castor::log::Param("IP",  castor::log::IPAddress(requester.ip)),
      castor::log::Param("Port",requester.port),
      castor::log::Param("HostName",requester.hostName),
      castor::log::Param("mountTransactionId",fileMigrationReportList.mountTransactionId()),
      castor::log::Param("tapebridgeTransId",fileMigrationReportList.aggregatorTransactionId()),
      castor::log::Param("tapePool",tapePool),
      castor::log::Param("TPVID",vid),
      castor::log::Param("errorCode",sstrerror(e.code())),
      castor::log::Param("errorMessage",e.getMessage().str())
  };
  castor::log::write(LOG_ERR,"Worker: vmgr error for migrated/repacked file", params);
}

void castor::tape::tapegateway::WorkerThread::logMigrationCannotFindVid (Cuuid_t uuid,
    const requesterInfo& requester, const FileMigrationReportList & migrationReport,
    castor::exception::Exception & e)
{
  std::list<castor::log::Param> params ={
      castor::log::Param("REQID", uuid),
      castor::log::Param("IP",  castor::log::IPAddress(requester.ip)),
      castor::log::Param("Port",requester.port),
      castor::log::Param("HostName",requester.hostName),
      castor::log::Param("mountTransactionId",migrationReport.mountTransactionId()),
      castor::log::Param("tapebridgeTransId",migrationReport.aggregatorTransactionId()),
      castor::log::Param("errorCode",sstrerror(e.code())),
      castor::log::Param("errorMessage",e.getMessage().str())
  };
  castor::log::write(LOG_ERR, "Worker: cannot find VID for migration mount", params);
}

void castor::tape::tapegateway::WorkerThread::logTapeReadOnly (
    Cuuid_t uuid, const requesterInfo& requester,
    const FileMigrationReportList & fileMigrationReportList,
    const std::string & tapePool, const std::string & vid)
{
  std::list<castor::log::Param> params ={
      castor::log::Param("REQID", uuid),
      castor::log::Param("IP",  castor::log::IPAddress(requester.ip)),
      castor::log::Param("Port",requester.port),
      castor::log::Param("HostName",requester.hostName),
      castor::log::Param("mountTransactionId",fileMigrationReportList.mountTransactionId()),
      castor::log::Param("tapebridgeTransId",fileMigrationReportList.aggregatorTransactionId()),
      castor::log::Param("tapePool",tapePool),
      castor::log::Param("TPVID",vid)
  };
  castor::log::write(LOG_INFO, "Worker: tape set to readonly after failed fSeq update", params);
}

void castor::tape::tapegateway::WorkerThread::logCannotReadOnly (
    Cuuid_t uuid, const requesterInfo& requester,
    const FileMigrationReportList & fileMigrationReportList,
    const std::string & tapePool, const std::string & vid,
    castor::exception::Exception & e)
{
  std::list<castor::log::Param> params ={
      castor::log::Param("REQID", uuid),
      castor::log::Param("IP",  castor::log::IPAddress(requester.ip)),
      castor::log::Param("Port",requester.port),
      castor::log::Param("HostName",requester.hostName),
      castor::log::Param("mountTransactionId",fileMigrationReportList.mountTransactionId()),
      castor::log::Param("tapebridgeTransId",fileMigrationReportList.aggregatorTransactionId()),
      castor::log::Param("TPVID",vid),
      castor::log::Param("tapePool",tapePool),
      castor::log::Param("errorCode",sstrerror(e.code())),
      castor::log::Param("errorMessage",e.getMessage().str())
  };
  castor::log::write(LOG_ERR, "Worker: failed to set tape to readonly after failed fSeq update", params);
}

