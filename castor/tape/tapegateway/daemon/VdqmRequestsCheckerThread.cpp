/******************************************************************************
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2004  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Regularly checks that ongoing recalls and migrations have an associated
 * request in VDQM. Cleans up when it's not the case
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

#include "castor/Constants.hpp"
#include "castor/exception/Exception.hpp"
#include "castor/IService.hpp"
#include "castor/Services.hpp"
#include "castor/stager/TapeTpModeCodes.hpp"
#include "castor/tape/tapegateway/daemon/ITapeGatewaySvc.hpp"
#include "castor/tape/tapegateway/daemon/VdqmRequestsCheckerThread.hpp"
#include "castor/tape/tapegateway/daemon/VdqmTapeGatewayHelper.hpp"
#include "castor/tape/tapegateway/daemon/VmgrTapeGatewayHelper.hpp"
#include "castor/utils/Timer.hpp"
#include "castor/log/log.hpp"
#include "Ctape_constants.h"
#include "serrno.h"

#include <errno.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

//------------------------------------------------------------------------------
// constructor
//------------------------------------------------------------------------------
castor::tape::tapegateway::VdqmRequestsCheckerThread::VdqmRequestsCheckerThread
(u_signed64 timeOut) : m_timeOut(timeOut) {}

//------------------------------------------------------------------------------
// run
//------------------------------------------------------------------------------
void castor::tape::tapegateway::VdqmRequestsCheckerThread::run(void*)
{
  // service to access the database
  castor::IService* dbSvc =
    castor::BaseObject::services()->service("OraTapeGatewaySvc", castor::SVC_ORATAPEGATEWAYSVC);
  castor::tape::tapegateway::ITapeGatewaySvc* oraSvc =
    dynamic_cast<castor::tape::tapegateway::ITapeGatewaySvc*>(dbSvc);
  if (0 == oraSvc) {
    castor::log::write(LOG_ERR, "Fatal error");
    return;
  }

  // get list of ongoing recall and migration requests
  std::list<castor::tape::tapegateway::ITapeGatewaySvc::TapeRequest> requests;
  castor::utils::Timer timer;
  try {
    // get tapes to check from the db
    castor::log::write(LOG_DEBUG,"VdqmRequestsChecker: getting tapes to check");
    // This SQL take lock on a tape gateway requests and updates them without commit.
    oraSvc->getTapesWithDriveReqs(requests, m_timeOut);
  } catch (castor::exception::Exception& e) {
    // log "VdqmRequestsChecker: no tape to check"
    std::list<castor::log::Param> params = {
      castor::log::Param("errorCode",sstrerror(e.code())),
      castor::log::Param("errorMessage",e.getMessage().str()),
      castor::log::Param("context", "was getting VDQM requests to be checked from the stager DB")
    };
    castor::log::write(LOG_ERR, "VdqmRequestsChecker: no tape to check", params);
    return;
  }
  if (requests.empty()){
    // log "VdqmRequestsChecker: no tape to check"
    std::list<castor::log::Param> params = { castor::log::Param("ProcessingTime", timer.secs()) };
    castor::log::write(LOG_DEBUG, "VdqmRequestsChecker: no tape to check", params);
    return;
  } else {
    // log "VdqmRequestsChecker: tapes to check found"
    std::list<castor::log::Param> params = {
      castor::log::Param("ProcessingTime", timer.secs()),
      castor::log::Param("NbVDQMRequestsToCheck", requests.size()),
    };
    castor::log::write(LOG_DEBUG, "VdqmRequestsChecker: tapes to check found", params);
  }

  // Loop through requests and call VDQM
  std::list<int> tapesToRetry;
  std::list<std::string> tapesToReset;
  for (std::list<castor::tape::tapegateway::ITapeGatewaySvc::TapeRequest>::const_iterator
         request = requests.begin();
       request != requests.end();
       request++){
    std::list<castor::log::Param> params = {
      castor::log::Param("mode", request->mode==WRITE_DISABLE?"Recall":"Migration"),
      castor::log::Param("mountTransactionId", request->mountTransactionId),
      castor::log::Param("TPVID", request->vid) };
    castor::log::write(LOG_DEBUG, "VdqmRequestsChecker: querying vdqm", params);
    try {
      VdqmTapeGatewayHelper::checkVdqmForRequest(request->mountTransactionId);
    } catch (castor::exception::Exception& e) {
      // log "VdqmRequestsChecker: request was lost or out of date"
      std::list<castor::log::Param> params = {
        castor::log::Param("errorCode",sstrerror(e.code())),
        castor::log::Param("errorMessage",e.getMessage().str()),
        castor::log::Param("mode", request->mode==WRITE_DISABLE?"Recall":"Migration"),
        castor::log::Param("mountTransactionId", request->mountTransactionId),
        castor::log::Param("TPVID", request->vid) };
      castor::log::write(LOG_WARNING, "VdqmRequestsChecker: request was lost or out of date", params);
      if (e.code() == EPERM) {
        tapesToRetry.push_back(request->mountTransactionId);
        if (request->mode == WRITE_ENABLE ){
          tapesToReset.push_back(request->vid);
        }
      }
    }
  }

  // Update VMGR, releasing the busy tapes
  for (std::list<std::string>::iterator tapeToReset = tapesToReset.begin();
       tapeToReset != tapesToReset.end();
       tapeToReset++){
    try {
      VmgrTapeGatewayHelper::resetBusyTape(*tapeToReset, m_shuttingDown);
      std::list<castor::log::Param> params =
        {castor::log::Param("TPVID", *tapeToReset)};
      castor::log::write(LOG_INFO, "VdqmRequestsChecker: releasing BUSY tape", params);
    } catch (castor::exception::Exception& e){
      std::list<castor::log::Param> params = {
        castor::log::Param("TPVID", *tapeToReset),
        castor::log::Param("errorCode",sstrerror(e.code())),
        castor::log::Param("errorMessage",e.getMessage().str())};
      castor::log::write(LOG_ERR, "VdqmRequestsChecker: cannot release BUSY tape after end transaction", params);
    }
  }

  // update the db to eventually send again some requests
  try {
    timer.reset();
    oraSvc->restartLostReqs(tapesToRetry); // autocommited
    std::list<castor::log::Param> paramsReset = { castor::log::Param("ProcessingTime", timer.secs()) };
    castor::log::write(LOG_DEBUG, "VdqmRequestsChecker: tapes resurrected", paramsReset);
  } catch (castor::exception::Exception& e){
    // impossible to update the information of checked tape
    std::list<castor::log::Param> params = {
      castor::log::Param("errorCode",sstrerror(e.code())),
      castor::log::Param("errorMessage",e.getMessage().str())
    };
    castor::log::write(LOG_ERR, "VdqmRequestsChecker: cannot update db", params);
  }
}




