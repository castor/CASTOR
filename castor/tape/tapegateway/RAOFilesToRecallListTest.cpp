/******************************************************************************
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * 
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

#include "castor/BaseObject.hpp"
#include "castor/exception/Exception.hpp"
#include "castor/io/biniostream.h"
#include "castor/io/StreamAddress.hpp"
#include "castor/Services.hpp"
#include "castor/tape/tapegateway/RAOFilesToRecallList.hpp"
#include "castor/tape/tapegateway/RAOFileToRecallStruct.hpp"

#include <gtest/gtest.h>
#include <memory>

namespace unitTests {

class castor_tape_gatewayprotocol_RAOFilesToRecallListTest : public ::testing::Test {
protected:

  virtual void SetUp() {
  }

  virtual void TearDown() {
  }
};

TEST_F(castor_tape_gatewayprotocol_RAOFilesToRecallListTest, createRepCreatObj) {
  using namespace castor;
  using namespace castor::tape;

  tapegateway::RAOFilesToRecallList fileListToBeSerialized;
  fileListToBeSerialized.setMountTransactionId(9);
  fileListToBeSerialized.setAggregatorTransactionId(10);

  {
    std::unique_ptr<tapegateway::RAOFileToRecallStruct> fileToBeSerialized(new tapegateway::RAOFileToRecallStruct());

    fileToBeSerialized->setFileTransactionId(1);
    fileToBeSerialized->setNshost("nshost");
    fileToBeSerialized->setFileid(2);
    fileToBeSerialized->setFseq(3);
    fileToBeSerialized->setPositionCommandCode(tapegateway::TPPOSIT_BLKID);
    fileToBeSerialized->setPath("path");
    fileToBeSerialized->setBlockId0(4);
    fileToBeSerialized->setBlockId1(5);
    fileToBeSerialized->setBlockId2(6);
    fileToBeSerialized->setBlockId3(7);
    fileToBeSerialized->setUmask(8);
    ASSERT_FALSE(fileToBeSerialized->hasChecksumName());
    fileToBeSerialized->setChecksumName("checksumName");
    ASSERT_TRUE(fileToBeSerialized->hasChecksumName());
    fileToBeSerialized->setChecksum(9);
    ASSERT_FALSE(fileToBeSerialized->hasFileSize());
    fileToBeSerialized->setFileSize(10);
    ASSERT_TRUE(fileToBeSerialized->hasFileSize());
    fileListToBeSerialized.addFilesToRecall(fileToBeSerialized.release());
  }

  io::biniostream buffer;
  io::StreamAddress address(buffer, "StreamCnvSvc", castor::SVC_STREAMCNV);
  Services *const services = BaseObject::sharedServices();
  ASSERT_TRUE(nullptr != services);
  services->createRep(&address, &fileListToBeSerialized);

  std::unique_ptr<IObject> parsedObj(services->createObj(&address));

  tapegateway::RAOFilesToRecallList &parsedFileList =
    dynamic_cast<tapegateway::RAOFilesToRecallList &>(*parsedObj);

  ASSERT_EQ(fileListToBeSerialized.mountTransactionId(), parsedFileList.mountTransactionId());
  ASSERT_EQ(fileListToBeSerialized.aggregatorTransactionId(), parsedFileList.aggregatorTransactionId());
  ASSERT_EQ(1, parsedFileList.filesToRecall().size());

  std::vector<tapegateway::RAOFileToRecallStruct*> &parsedFiles = parsedFileList.filesToRecall();
  tapegateway::RAOFileToRecallStruct * const parsedFile = parsedFiles[0];
  ASSERT_NE(nullptr, parsedFile);

  ASSERT_EQ(1, parsedFile->fileTransactionId());
  ASSERT_EQ("nshost", parsedFile->nshost());
  ASSERT_EQ(2, parsedFile->fileid());
  ASSERT_EQ(3, parsedFile->fseq());
  ASSERT_EQ(tapegateway::TPPOSIT_BLKID, parsedFile->positionCommandCode());
  ASSERT_EQ("path", parsedFile->path());
  ASSERT_EQ(4, parsedFile->blockId0());
  ASSERT_EQ(5, parsedFile->blockId1());
  ASSERT_EQ(6, parsedFile->blockId2());
  ASSERT_EQ(7, parsedFile->blockId3());
  ASSERT_EQ(8, parsedFile->umask());
  ASSERT_TRUE(parsedFile->hasChecksumName());
  ASSERT_EQ("checksumName", parsedFile->checksumName());
  ASSERT_TRUE(parsedFile->hasChecksum());
  ASSERT_EQ(9, parsedFile->checksum());
  ASSERT_TRUE(parsedFile->hasFileSize());
  ASSERT_EQ(10, parsedFile->fileSize());
}

} // namespace unitTests
