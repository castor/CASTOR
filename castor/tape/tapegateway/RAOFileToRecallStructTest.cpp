/******************************************************************************
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * 
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

#include "castor/BaseObject.hpp"
#include "castor/exception/Exception.hpp"
#include "castor/io/biniostream.h"
#include "castor/io/StreamAddress.hpp"
#include "castor/Services.hpp"
#include "castor/tape/tapegateway/RAOFilesToRecallList.hpp"
#include "castor/tape/tapegateway/RAOFileToRecallStruct.hpp"

#include <gtest/gtest.h>
#include <memory>

namespace unitTests {

class castor_tape_gatewayprotocol_RAOFileToRecallStructTest : public ::testing::Test {
protected:

  virtual void SetUp() {
  }

  virtual void TearDown() {
  }
};

TEST_F(castor_tape_gatewayprotocol_RAOFileToRecallStructTest, setChecksumName) {
  using namespace castor::tape;

  tapegateway::RAOFileToRecallStruct fileToRecall;

  ASSERT_THROW(fileToRecall.checksumName(), castor::exception::Exception);

  const std::string checksumName = "Checksum name";
  fileToRecall.setChecksumName(checksumName);
  ASSERT_EQ(checksumName, fileToRecall.checksumName());
}

TEST_F(castor_tape_gatewayprotocol_RAOFileToRecallStructTest, setChecksum) {
  using namespace castor::tape;

  tapegateway::RAOFileToRecallStruct fileToRecall;

  ASSERT_THROW(fileToRecall.checksum(), castor::exception::Exception);

  const u_signed64 checksum = 12345;
  fileToRecall.setChecksum(checksum);
  ASSERT_EQ(checksum, fileToRecall.checksum());
}

TEST_F(castor_tape_gatewayprotocol_RAOFileToRecallStructTest, setFileSize) {
  using namespace castor::tape;

  tapegateway::RAOFileToRecallStruct fileToRecall;

  ASSERT_THROW(fileToRecall.fileSize(), castor::exception::Exception);

  const u_signed64 fileSize = 12345;
  fileToRecall.setFileSize(fileSize);
  ASSERT_EQ(fileSize, fileToRecall.fileSize());
}

} // namespace unitTests
