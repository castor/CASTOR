/**** This file has been autogenerated by gencastor from Umbrello UML model ***/

/******************************************************************************
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

#pragma once

// Include Files
#include "castor/tape/tapegateway/GatewayMessage.hpp"
#include "osdep.h"
#include <iostream>
#include <string>
#include <vector>

namespace castor {

  // Forward declarations
  class ObjectSet;
  class IObject;

  namespace tape {

    namespace tapegateway {

      // Forward declarations
      class FileToMigrateStruct;

      /**
       * class FilesToMigrateList
       */
      class FilesToMigrateList : public virtual GatewayMessage {

      public:

        /**
         * Empty Constructor
         */
        FilesToMigrateList() throw();

        /**
         * Empty Destructor
         */
        virtual ~FilesToMigrateList() throw();

        /**
         * Outputs this object in a human readable format
         * @param stream The stream where to print this object
         * @param indent The indentation to use
         * @param alreadyPrinted The set of objects already printed.
         * This is to avoid looping when printing circular dependencies
         */
        virtual void print(std::ostream& stream,
                           std::string indent,
                           castor::ObjectSet& alreadyPrinted) const;

        /**
         * Outputs this object in a human readable format
         */
        virtual void print() const;

        /**
         * Gets the type of this kind of objects
         */
        static int TYPE();

        /********************************************/
        /* Implementation of IObject abstract class */
        /********************************************/
        /**
         * Gets the type of the object
         */
        virtual int type() const;

        /**
         * virtual method to clone any object
         */
        virtual castor::IObject* clone();

        /*********************************/
        /* End of IObject abstract class */
        /*********************************/
        /**
         * Get the value of m_id
         * The id of this object
         * @return the value of m_id
         */
        u_signed64 id() const {
          return m_id;
        }

        /**
         * Set the value of m_id
         * The id of this object
         * @param new_var the new value of m_id
         */
        void setId(u_signed64 new_var) {
          m_id = new_var;
        }

        /**
         * Add a FileToMigrateStruct* object to the m_filesToMigrateVector list
         */
        void addFilesToMigrate(FileToMigrateStruct* add_object) {
          m_filesToMigrateVector.push_back(add_object);
        }

        /**
         * Remove a FileToMigrateStruct* object from m_filesToMigrateVector
         */
        void removeFilesToMigrate(FileToMigrateStruct* remove_object) {
          for (unsigned int i = 0; i < m_filesToMigrateVector.size(); i++) {
            FileToMigrateStruct* item = m_filesToMigrateVector[i];
            if (item == remove_object) {
              std::vector<FileToMigrateStruct*>::iterator it = m_filesToMigrateVector.begin() + i;
              m_filesToMigrateVector.erase(it);
              return;
            }
          }
        }

        /**
         * Get the list of FileToMigrateStruct* objects held by m_filesToMigrateVector
         * @return list of FileToMigrateStruct* objects held by m_filesToMigrateVector
         */
        std::vector<FileToMigrateStruct*>& filesToMigrate() {
          return m_filesToMigrateVector;
        }

      private:

        /// The id of this object
        u_signed64 m_id;

        std::vector<FileToMigrateStruct*> m_filesToMigrateVector;

      }; /* end of class FilesToMigrateList */

    } /* end of namespace tapegateway */

  } /* end of namespace tape */

} /* end of namespace castor */

