/**** This file has been autogenerated by gencastor from Umbrello UML model ***/

/******************************************************************************
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

// Include Files
#include "StreamTapeDriveCnv.hpp"
#include "castor/CnvFactory.hpp"
#include "castor/Constants.hpp"
#include "castor/IAddress.hpp"
#include "castor/ICnvSvc.hpp"
#include "castor/IObject.hpp"
#include "castor/ObjectCatalog.hpp"
#include "castor/ObjectSet.hpp"
#include "castor/exception/Exception.hpp"
#include "castor/io/StreamAddress.hpp"
#include "castor/io/StreamBaseCnv.hpp"
#include "castor/io/StreamCnvSvc.hpp"
#include "castor/vdqm/DeviceGroupName.hpp"
#include "castor/vdqm/TapeDrive.hpp"
#include "castor/vdqm/TapeDriveCompatibility.hpp"
#include "castor/vdqm/TapeDriveDedication.hpp"
#include "castor/vdqm/TapeDriveStatusCodes.hpp"
#include "castor/vdqm/TapeRequest.hpp"
#include "castor/vdqm/TapeServer.hpp"
#include "castor/vdqm/VdqmTape.hpp"
#include "osdep.h"
#include <string>
#include <vector>

//------------------------------------------------------------------------------
// Instantiation of a static factory class - should never be used
//------------------------------------------------------------------------------
static castor::CnvFactory<castor::io::StreamTapeDriveCnv>
  s_factoryStreamTapeDriveCnv;

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
castor::io::StreamTapeDriveCnv::StreamTapeDriveCnv(castor::ICnvSvc* cnvSvc) :
 StreamBaseCnv(cnvSvc) {}

//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
castor::io::StreamTapeDriveCnv::~StreamTapeDriveCnv() throw() {
}

//------------------------------------------------------------------------------
// ObjType
//------------------------------------------------------------------------------
unsigned int castor::io::StreamTapeDriveCnv::ObjType() {
  return castor::vdqm::TapeDrive::TYPE();
}

//------------------------------------------------------------------------------
// objType
//------------------------------------------------------------------------------
unsigned int castor::io::StreamTapeDriveCnv::objType() const {
  return ObjType();
}

//------------------------------------------------------------------------------
// createRep
//------------------------------------------------------------------------------
void castor::io::StreamTapeDriveCnv::createRep(castor::IAddress* address,
                                               castor::IObject* object,
                                               bool,
                                               unsigned int)
   {
  castor::vdqm::TapeDrive* obj = 
    dynamic_cast<castor::vdqm::TapeDrive*>(object);
  StreamAddress* ad = 
    dynamic_cast<StreamAddress*>(address);
  ad->stream() << obj->type();
  ad->stream() << obj->jobID();
  ad->stream() << obj->modificationTime();
  ad->stream() << obj->resettime();
  ad->stream() << obj->usecount();
  ad->stream() << obj->errcount();
  ad->stream() << obj->transferredMB();
  ad->stream() << obj->totalMB();
  ad->stream() << obj->driveName();
  ad->stream() << obj->id();
  ad->stream() << obj->status();
}

//------------------------------------------------------------------------------
// createObj
//------------------------------------------------------------------------------
castor::IObject* castor::io::StreamTapeDriveCnv::createObj(castor::IAddress* address)
   {
  StreamAddress* ad = 
    dynamic_cast<StreamAddress*>(address);
  // create the new Object
  castor::vdqm::TapeDrive* object = new castor::vdqm::TapeDrive();
  // Now retrieve and set members
  int jobID;
  ad->stream() >> jobID;
  object->setJobID(jobID);
  u_signed64 modificationTime;
  ad->stream() >> modificationTime;
  object->setModificationTime(modificationTime);
  u_signed64 resettime;
  ad->stream() >> resettime;
  object->setResettime(resettime);
  int usecount;
  ad->stream() >> usecount;
  object->setUsecount(usecount);
  int errcount;
  ad->stream() >> errcount;
  object->setErrcount(errcount);
  int transferredMB;
  ad->stream() >> transferredMB;
  object->setTransferredMB(transferredMB);
  u_signed64 totalMB;
  ad->stream() >> totalMB;
  object->setTotalMB(totalMB);
  std::string driveName;
  ad->stream() >> driveName;
  object->setDriveName(driveName);
  u_signed64 id;
  ad->stream() >> id;
  object->setId(id);
  int status;
  ad->stream() >> status;
  object->setStatus((castor::vdqm::TapeDriveStatusCodes)status);
  return object;
}

//------------------------------------------------------------------------------
// marshalObject
//------------------------------------------------------------------------------
void castor::io::StreamTapeDriveCnv::marshalObject(castor::IObject* object,
                                                   castor::io::StreamAddress* address,
                                                   castor::ObjectSet& alreadyDone)
   {
  castor::vdqm::TapeDrive* obj = 
    dynamic_cast<castor::vdqm::TapeDrive*>(object);
  if (0 == obj) {
    // Case of a null pointer
    address->stream() << castor::OBJ_Ptr << ((unsigned int)0);
  } else if (alreadyDone.find(obj) == alreadyDone.end()) {
    // Case of a pointer to a non streamed object
    createRep(address, obj, true);
    // Mark object as done
    alreadyDone.insert(obj);
    cnvSvc()->marshalObject(obj->tape(), address, alreadyDone);
    cnvSvc()->marshalObject(obj->runningTapeReq(), address, alreadyDone);
    address->stream() << obj->tapeDriveDedication().size();
    for (std::vector<castor::vdqm::TapeDriveDedication*>::iterator it = obj->tapeDriveDedication().begin();
         it != obj->tapeDriveDedication().end();
         it++) {
      cnvSvc()->marshalObject(*it, address, alreadyDone);
    }
    address->stream() << obj->tapeDriveCompatibilities().size();
    for (std::vector<castor::vdqm::TapeDriveCompatibility*>::iterator it = obj->tapeDriveCompatibilities().begin();
         it != obj->tapeDriveCompatibilities().end();
         it++) {
      cnvSvc()->marshalObject(*it, address, alreadyDone);
    }
    cnvSvc()->marshalObject(obj->deviceGroupName(), address, alreadyDone);
    cnvSvc()->marshalObject(obj->tapeServer(), address, alreadyDone);
  } else {
    // case of a pointer to an already streamed object
    address->stream() << castor::OBJ_Ptr << alreadyDone[obj];
  }
}

//------------------------------------------------------------------------------
// unmarshalObject
//------------------------------------------------------------------------------
castor::IObject* castor::io::StreamTapeDriveCnv::unmarshalObject(castor::io::biniostream& stream,
                                                                 castor::ObjectCatalog& newlyCreated)
   {
  castor::io::StreamAddress ad(stream, "StreamCnvSvc", castor::SVC_STREAMCNV);
  castor::IObject* object = createObj(&ad);
  // Mark object as created
  newlyCreated.insert(object);
  // Fill object with associations
  castor::vdqm::TapeDrive* obj = 
    dynamic_cast<castor::vdqm::TapeDrive*>(object);
  ad.setObjType(castor::OBJ_INVALID);
  castor::IObject* objTape = cnvSvc()->unmarshalObject(ad, newlyCreated);
  obj->setTape(dynamic_cast<castor::vdqm::VdqmTape*>(objTape));
  ad.setObjType(castor::OBJ_INVALID);
  castor::IObject* objRunningTapeReq = cnvSvc()->unmarshalObject(ad, newlyCreated);
  obj->setRunningTapeReq(dynamic_cast<castor::vdqm::TapeRequest*>(objRunningTapeReq));
  unsigned int tapeDriveDedicationNb;
  ad.stream() >> tapeDriveDedicationNb;
  for (unsigned int i = 0; i < tapeDriveDedicationNb; i++) {
    ad.setObjType(castor::OBJ_INVALID);
    castor::IObject* objTapeDriveDedication = cnvSvc()->unmarshalObject(ad, newlyCreated);
    obj->addTapeDriveDedication(dynamic_cast<castor::vdqm::TapeDriveDedication*>(objTapeDriveDedication));
  }
  unsigned int tapeDriveCompatibilitiesNb;
  ad.stream() >> tapeDriveCompatibilitiesNb;
  for (unsigned int i = 0; i < tapeDriveCompatibilitiesNb; i++) {
    ad.setObjType(castor::OBJ_INVALID);
    castor::IObject* objTapeDriveCompatibilities = cnvSvc()->unmarshalObject(ad, newlyCreated);
    obj->addTapeDriveCompatibilities(dynamic_cast<castor::vdqm::TapeDriveCompatibility*>(objTapeDriveCompatibilities));
  }
  ad.setObjType(castor::OBJ_INVALID);
  castor::IObject* objDeviceGroupName = cnvSvc()->unmarshalObject(ad, newlyCreated);
  obj->setDeviceGroupName(dynamic_cast<castor::vdqm::DeviceGroupName*>(objDeviceGroupName));
  ad.setObjType(castor::OBJ_INVALID);
  castor::IObject* objTapeServer = cnvSvc()->unmarshalObject(ad, newlyCreated);
  obj->setTapeServer(dynamic_cast<castor::vdqm::TapeServer*>(objTapeServer));
  return object;
}

