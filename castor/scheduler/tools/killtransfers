#!/usr/bin/python
#/******************************************************************************
# *                   killtransfers.py
# *
# * This file is part of the Castor project.
# * See http://castor.web.cern.ch/castor
# *
# * Copyright (C) 2003  CERN
# * This program is free software; you can redistribute it and/or
# * modify it under the terms of the GNU General Public License
# * as published by the Free Software Foundation; either version 2
# * of the License, or (at your option) any later version.
# * This program is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU General Public License for more details.
# * You should have received a copy of the GNU General Public License
# * along with this program; if not, write to the Free Software
# * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# *
# *
# * command line to unqueue pending transfers
# *
# * @author Castor Dev team, castor-dev@cern.ch
# *****************************************************************************/

'''command line of the CASTOR transfer manager suite allowing to unqueue pending transfers and fail them'''

import sys
import getopt
import traceback
import rpyc
import castor_tools

# usage function
def usage(exitCode):
  '''prints usage'''
  print 'Usage : ' + sys.argv[0] + ' [-h|--help] transferid[...]'
  print '        ' + sys.argv[0] + ' [-h|--help] -a|--all [-u|--user <user>]'
  sys.exit(exitCode)

# first parse the options
user = None
killall = False
verbose = False
try:
  options, args = getopt.getopt(sys.argv[1:], 'hau:v', ['help', 'all', 'user', 'verbose'])
except Exception, e:
  print e
  usage(1)
for f, v in options:
  if f == '-h' or f == '--help':
    usage(0)
  elif f == '-a' or f == '--all':
    killall = True
  elif f == '-u' or f == '--user':
    if user:
      print 'Cannot use -u|--user twice'
      usage(1)
    user = v
  elif f == '-v' or f == '--verbose':
    verbose = True
  else:
    print "unknown option : " + f
    usage(1)

if not killall and user:
  print "-u|--user can only be used in conjunction with -a|--all"
  usage(1)
if killall and len(args) > 0:
  print "No arguments expected when -a|--all used"
  usage(1)
if not killall and len(args) == 0:
  print "Missing argument"
  usage(1)

# connect to server and gather numbers
try:
  conf = castor_tools.castorConf()
  try:
    rpcconn = rpyc.connect(conf.getValue('DiskManager', 'ServerHosts').split()[0],
                           conf.getValue('TransferManager', 'Port', 15011, int))
    if killall:
      rpcconn.root.killalltransfers(user)
    else:
      rpcconn.root.killtransfers(args)
    print 'transfer(s) killed'
  except Exception, e:
    print 'Caught exception : (' + str(e.__class__) + ') : ' + str(e)
    if verbose:
      if hasattr(e, "_remote_tb"):
          print 'Remote trace :'
          print e._remote_tb[0]
      traceback.print_exc()
except ValueError, e:
  print e
  sys.exit(1)
