.lf 8 transfermgrd.man
.TH transfermgrd "8castor" "2011/03/21" CASTOR "transfermgrd"
.SH NAME
transfermgrd \- starts the CASTOR's transfer manager daemon
.SH SYNOPSIS
.B transfermgrd
[
.BI -f
]
[
.BI -h
]
.SH DESCRIPTION
.LP
The
.B transfermgrd
command starts the CASTOR scheduling daemon.
.LP

.SH OPTIONS

.TP
.BI \-f,\ \-\-foreground
runs in foreground mode (no daemonization).
.TP
.BI \-h,\ \-\-help
displays command usage help.

.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>





