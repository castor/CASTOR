.lf 8 diskmanagerd.man
.TH diskmanagerd "8castor" "2011/03/21" CASTOR "diskmanagerd"
.SH NAME
diskmanagerd \- starts the CASTOR DiskServer Manager Daemon
.SH SYNOPSIS
.B diskmanagerd
[
.BI -f
]
[
.BI -h
]
[
.BI --fakemode
]
.SH DESCRIPTION
.LP
The
.B diskmanagerd
command starts the CASTOR DiskServer Manager daemon.
.LP

.SH OPTIONS

.TP
.BI \-f,\ \-\-foreground
runs in foreground mode (no daemonization).
.TP
.BI \-h,\ \-\-help
displays command usage help.
.TP
.BI \-\-fakemode
fakes processing by not starting jobs, but still handling queues.
To be only used for testing purposes.

.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>





