#!/usr/bin/python
#/******************************************************************************
# *                   manager.py
# *
# * This file is part of the Castor project.
# * See http://castor.web.cern.ch/castor
# *
# * Copyright (C) 2003  CERN
# * This program is free software; you can redistribute it and/or
# * modify it under the terms of the GNU General Public License
# * as published by the Free Software Foundation; either version 2
# * of the License, or (at your option) any later version.
# * This program is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU General Public License for more details.
# * You should have received a copy of the GNU General Public License
# * along with this program; if not, write to the Free Software
# * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# *
# * the manager thread of the disk server manager daemon of CASTOR
# *
# * @author Castor Dev team, castor-dev@cern.ch
# *****************************************************************************/

"""manager thread of the disk server manager daemon of CASTOR."""

import threading
import time
import dlf
import connectionpool

# small helper function to add a suffix to all protocol identifiers for the logging
def _addKeySuffix(d, suff):
  for k in d.keys():
    d[k + suff] = d.pop(k)
  return d

class ManagerThread(threading.Thread):
  '''Management thread of the disk server manager, dealing with transfer completion control and associated cleanup,
  with transfer cancelation in case of no space, timeouts, etc., with restart of disk to disk transfers
  that are waiting on the sources when these are ready, and finally with activity reporting in the log.'''

  def __init__(self, runningTransfers, transferQueue):
    '''constructor'''
    super(ManagerThread, self).__init__(name='ManagementThread')
    self.alive = True
    self.runningTransfers = runningTransfers
    self.transferQueue = transferQueue
    self.lastActivityStats = time.time() - 30
    # start the thread
    self.setDaemon(True)
    self.start()


  def gatherActivityStats(self):
    '''gather and log the current activity: cf. diskmanagerd::exposed_summarizeTransfers()'''
    if time.time() - self.lastActivityStats >= 29:
      # do this twice per minute
      self.lastActivityStats = time.time()
      activityData = self.transferQueue.nbTransfers(None, True) + self.runningTransfers.nbTransfers(None, True)
      nbtPend, nbtPendPerProto, nbsPend, _unused_sPendPerProto, nbtRun, nbtRunPerProto, nbsRun, _unused_sRunPerProto = activityData
      # flatten the per-proto tuples into their own keys
      perProto = _addKeySuffix(dict(nbtPendPerProto), 'Pend')
      perProto.update(_addKeySuffix(dict(nbtRunPerProto), 'Run'))
      # log the current counts
      dlf.writeinfo('Running and pending transfers summary', nbtPend=nbtPend, nbsPend=nbsPend, nbtRun=nbtRun, nbsRun=nbsRun, **perProto)

  def run(self):
    '''main method, containing the infinite loop'''
    while self.alive:
      try:
        # check for running transfers that have completed
        self.runningTransfers.poll()
        # regather xrootd transfers from xrootd: this is required every now and then
        # because xrootd may crash and forget about transfers that we know
        self.runningTransfers.gatherXrootdTransfers()
        # check for d2d destination transfers that could now start
        self.transferQueue.pollD2dDest()
        # check for transfer to be canceled for various reasons (timeout, missing resources, ...)
        self.transferQueue.checkForTransfersCancelation()
        # check for d2d src that were canceled
        self.runningTransfers.checkForCanceledRunningD2dSrc()
        # regularly log the current activity (count of running and pending transfers)
        self.gatherActivityStats()
      except connectionpool.Timeout, e:
        dlf.writenotice('Connection timeout in Management thread', Type=str(e.__class__), Message=str(e))
      except Exception, e:
        dlf.writeerr('Caught exception in Management thread', Type=str(e.__class__), Message=str(e))
      # do not loop too fast
      time.sleep(10)

  def stop(self):
    '''stops processing in this thread'''
    self.alive = False

