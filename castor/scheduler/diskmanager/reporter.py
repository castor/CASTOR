#!/usr/bin/python
#/******************************************************************************
# *                   reporter.py
# *
# * This file is part of the Castor project.
# * See http://castor.web.cern.ch/castor
# *
# * Copyright (C) 2003  CERN
# * This program is free software; you can redistribute it and/or
# * modify it under the terms of the GNU General Public License
# * as published by the Free Software Foundation; either version 2
# * of the License, or (at your option) any later version.
# * This program is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU General Public License for more details.
# * You should have received a copy of the GNU General Public License
# * along with this program; if not, write to the Free Software
# * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# *
# * the reporter thread of the disk server manager daemon of CASTOR
# *
# * @author Castor Dev team, castor-dev@cern.ch
# *****************************************************************************/

"""reporter thread of the disk server manager daemon of CASTOR."""

import threading
import socket
import os
import time
import subprocess
import json
import connectionpool
import dlf

class StreamCount(object):
  '''a small object handling count of streams'''
  def __init__(self):
    '''constructor'''
    self.nbReads = 0
    self.nbWrites = 0
    self.nbRecalls = 0
    self.nbMigrations = 0

class ReporterThread(threading.Thread):
  '''reporter thread.
  This thread is responsible for regularly sending the resource monitoring and status
   information concerning the diskserver where it runs to the transfermanager'''

  def __init__(self, runningTransfers, configuration):
    '''constructor'''
    super(ReporterThread, self).__init__(name='Reporter')
    self.alive = True
    self.runningTransfers = runningTransfers
    self.configuration = configuration
    # start the thread
    self.setDaemon(True)
    self.start()

  def buildReport(self):
    '''builds a report concerning all filesystems from this diskserver'''
    # the report will have the following form :
    # ((diskServerName, mountPoint, maxFreeSpace, minAllowedFreeSpace, totalSpace,
    #   freeSpace, nbReadStreams, nbWriteStreams, nbRecalls, nbMigrations), ...)
    # with one tuple per filesystem
    diskServerName = socket.getfqdn()
    maxFreeSpace = self.configuration.getValue('DiskManager', 'FSMaxFreeSpace', 0.05, float)
    minAllowedFreeSpace = self.configuration.getValue('DiskManager', 'FSMinAllowedFreeSpace', 0.02, float)
    mountPoints = set(self.configuration.getValue('DiskManager', 'MountPoints', '').split())
    dataPools = dict((extpool, poolColUser.split(':')) for poolColUser, extpool in
                     [entry.split('@') for entry in
                      self.configuration.getValue('DiskManager', 'DataPools', '').split()])
    streamCount = self.runningTransfers.getStreamCount()
    reports = []
    for mountPoint in mountPoints:
      # check we really have a mount point
      if os.path.ismount(mountPoint):
        # get total and free space
        stat = os.statvfs(mountPoint)
        totalSpace = stat.f_blocks*stat.f_bsize
        freeSpace = stat.f_bavail*stat.f_bsize
      else:
        # it's not: report it with 0 space and log a warning, it may have been temporarily unmounted
        # because of an hardware failure
        dlf.writenotice('Path is not a mount point, reporting 0 space', diskServer=diskServerName, mountPoint=mountPoint)
        totalSpace = freeSpace = 0
      # get number of streams of each kind
      try:
        sc = streamCount[mountPoint]
      except KeyError:
        # no stream at all on this filesystem
        sc = StreamCount()
      # fill report
      reports.append((diskServerName, mountPoint, 'NoExtUser', 'NoExtPool', maxFreeSpace, minAllowedFreeSpace,
                      totalSpace, freeSpace, sc.nbReads, sc.nbWrites, sc.nbRecalls, sc.nbMigrations))
      # check whether we should create subdirectories in the mountPoints, that is
      # whether this is a brand new mountPoints that we should populate
      if os.path.ismount(mountPoint):
        try:
          os.stat(os.sep.join([mountPoint, '01']))
        except OSError:
          # no, then let's create the missing directories
          for i in range(100):
            os.mkdir(os.sep.join([mountPoint, '%02d' % i]))
    # now handle dataPools if any
    if dataPools:
      try:
        for dataPool in dataPools:
            try:
                # get numbers from ceph
                jsonoutput = subprocess.Popen(['ceph', 'df', '--id', dataPools[dataPool][1], '-f', 'json'],
                                              stdout=subprocess.PIPE).communicate()[0]
                pyoutput = json.loads(jsonoutput)
                usedSpace = dict([(entry['name'], entry['stats']['stored'])
                                  for entry in pyoutput['pools']
                                  if entry['name'] in dataPools])
                jsonoutput = subprocess.Popen(['ceph', 'osd', 'pool', 'get-quota', dataPool, '--id', dataPools[dataPool][1], '-f', 'json'],
                                              stdout=subprocess.PIPE).communicate()[0]
                pyoutput = json.loads(jsonoutput)
                totalSpace = pyoutput['quota_max_bytes']
                if totalSpace == 0:
                    raise Exception("Quota missing in external pool %s for datapool %s (set to 0)"
                                    % (dataPool, dataPools[dataPool][0]))
                freeSpace = totalSpace - usedSpace[dataPool]
                # fill report
                reports.append((diskServerName, dataPools[dataPool][0], # castor datapool name
                                dataPools[dataPool][1], # external user
                                dataPool,               # external pool
                                maxFreeSpace, minAllowedFreeSpace,
                                totalSpace, freeSpace, 0, 0, 0, 0))
            except KeyError:
                  raise Exception("No data found for datapool %s - May be missing quota" % dataPool)
      except ValueError, e:
        raise Exception("Error caught in trying to build report for Datapools : %s" % e)
    if len(reports) == 0:
      # no mount points nor data pool configured, log a warning
      dlf.writenotice('Reporter found no mount points nor data pools to report, leaving diskserver offline')
    return tuple(reports)

  def run(self):
    '''main method, containing the infinite loop'''
    # remember time of the last error log
    lastErrorLogTime = 0.0
    while self.alive:
      try:
        # build and send report
        report = self.buildReport()
        schedulers = self.configuration.getValue('DiskManager', 'ServerHosts').split()
        if len(schedulers) == 0:
          # "No scheduler given in configfile, please fix" message
          dlf.writeerr('No scheduler given in configfile, please fix')
        else:
          notSent = True
          i = 0
          while notSent and i < len(schedulers):
             try:
               connectionpool.connections.stateReport(schedulers[i], report)
               notSent = False
             except Exception, e:
               # "Caught exception when sending report to transfermanager,
               # will try other transfermanagers" message
               dlf.writedebug('Caught exception when sending report to transfermanager, will try other transfermanagers', transferManager=schedulers[i], error=str(e))
             i += 1
          if notSent:
             # no more than one error log every HeartbeatNotSentLogInterval to not flood the logs
             if time.time() > lastErrorLogTime + \
                self.configuration.getValue('DiskManager', 'HeartbeatNotSentLogInterval', 300.0, float):
                # "Could not send report to any transfermanager, giving up" message
                dlf.writeerr('Could not send report to any transfermanager, giving up', schedulers=str(schedulers), error=str(e))
                # remember last error time
                lastErrorLogTime = time.time()
             else:
                # "Could not send report to any transfermanager, giving up" message
                dlf.writedebug('Could not send report to any transfermanager, giving up', schedulers=str(schedulers), error=str(e))
      except Exception, e:
        # "Caught exception in Reporter thread" message
        dlf.writeerr('Caught exception in Reporter thread. Giving up for this round', error=str(e))
      # do not loop too fast, send only one report every second
      time.sleep(self.configuration.getValue('DiskManager', 'HeartbeatInterval', 1.0, float))

  def stop(self):
    '''stops processing in this thread'''
    self.alive = False
