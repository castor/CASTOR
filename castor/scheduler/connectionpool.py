#/******************************************************************************
# *                   connectionpool.py
# *
# * This file is part of the Castor project.
# * See http://castor.web.cern.ch/castor
# *
# * Copyright (C) 2003  CERN
# * This program is free software; you can redistribute it and/or
# * modify it under the terms of the GNU General Public License
# * as published by the Free Software Foundation; either version 2
# * of the License, or (at your option) any later version.
# * This program is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU General Public License for more details.
# * You should have received a copy of the GNU General Public License
# * along with this program; if not, write to the Free Software
# * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# *
# *
# * Code handling pools of connections to identical remote services
# *
# * @author Castor Dev team, castor-dev@cern.ch
# *****************************************************************************/

'''connection pool module of the CASTOR transfer manager.
Handles a pool of cached rpyc connections to different nodes'''

import exceptions
import rpyc
import rpyc.core.netref
import castor_tools
import dlf
from commonexceptions import CastorException

class Timeout(rpyc.core.async.AsyncResultTimeout):
  '''Our name for the timeout exception on asynchronous calls'''
  pass

def asyncreq(proxy, handler, *args):
    """Overwrite of the rpyc.core.netref.asyncreq helper function to fix
    the case where the connection has been dropped in the mean time"""
    conn = object.__getattribute__(proxy, "____conn__")
    connection = conn()
    if not connection:
      raise exceptions.ReferenceError('weakly-referenced object no longer exists')
    oid = object.__getattribute__(proxy, "____oid__")
    return connection.async_request(handler, oid, *args)

class ConnectionPool(object):
  '''Object handling a pool of connections to identical remote services'''

  def __init__(self):
    '''constructor'''
    # the configuration of CASTOR
    self.config = castor_tools.castorConf()
    # cached connections
    self.connections = {}

  def getConnection(self, machine):
    '''returns an open connection to the specified machine'''
    try:
      # get the cached connection
      conn = self.connections[machine]
      # return it, as we found one
      return conn.root
    except KeyError:
      # no cached connection, create a new one
      # find out the port to be used
      if machine not in self.config.getValue('DiskManager', 'ServerHosts').split():
        # we have a diskserver
        port = self.config.getValue('DiskManager', 'Port', 15011, int)
      else:
        # we have a master node
        port = self.config.getValue('TransferManager', 'Port', 15011, int)
      try:
        # create the connection
        rpcconn = rpyc.connect(machine, port, config={'instantiate_custom_exceptions':True})
        # cache it
        self.connections[machine] = rpcconn
        # Make the setting up of the connection able to timeout
        # This is in principle done by the rpyc framework on first use of the root attribute
        # but as rpyc does it in a blocking manner, we had to do it "by hand" with
        # proper timeout functionality
        remote_root_ref = rpcconn.async_request(rpyc.core.consts.HANDLE_GETROOT)
        remote_root_ref.set_expiry(self.config.getValue('Scheduler', 'AdminTimeout', 5, float))
        rpcconn._remote_root = remote_root_ref.value
      except Exception, e:
        dlf.writenotice('Exception when establishing the first connection', machine=machine, Type=str(e.__class__), Message=str(e))
        raise
      # and return
      dlf.writedebug('Connection established', machine=machine)
      return rpcconn.root

  def close(self, machine):
    '''Close the connection to a given machine'''
    if machine in self.connections:
      conn = self.connections[machine]
      del self.connections[machine]
      try:
        conn.close()
      except Exception:
        pass

  def closeall(self):
    '''Close all connections'''
    conns = self.connections.values()
    self.connections = {}
    for conn in conns:
      try:
        conn.close()
      except Exception:
        pass

  def __getattr__(self, name):
    '''we implement a proxying facility here, where any method will be forwarded
    to the connection of the machine given as first argument.
    So calling connectionPool.foo('machine1', 1,2,3) will be forwarded to a call
    to foo(1,2,3) on the connection associated to machine 'machine1'
    A keyword argument timeout can also be passed to set a dedicated timeout on
    the remote call and overwrite the default value given by the
    Scheduler/ConnectionTimeout entry of the config file. Note that None
    can be passed, meaning there is no timeout.
    As an example, one can call connectionPool.foo('machine1', 1,2,3, timeout=3.5)'''
    def f(machine, *args, **kwargs):
      '''wrapped method doing the actual call'''
      try:
        # first see whether a timeout has been explicitely given. Note that this is the
        # timeout for the actual call to the function. The timeout for internal calls
        # is not touched and still given by the Scheduler/ConnectionTimeout entry
        # in the config file
        try:
          timeout = kwargs['timeout']
          del kwargs['timeout']
        except KeyError:
          timeout = self.config.getValue('Scheduler', 'ConnectionTimeout', 1, float)
        if len(kwargs) > 0:
          raise TypeError("got unexpected keyword argument %r" % (kwargs.keys()[0],))
        # we may want to retry in case we get an exception
        gotException = False
        while True:
          try:
            # try existing connection
            conn = self.getConnection(machine)
            remote_attr = asyncreq(conn, rpyc.core.consts.HANDLE_GETATTR, name)
            remote_attr.set_expiry(self.config.getValue('Scheduler', 'ConnectionTimeout', 1, float))
            result = asyncreq(remote_attr.value, rpyc.core.consts.HANDLE_CALL, args)
            result.set_expiry(timeout)
            return result.value
          except (EOFError, IOError, exceptions.ReferenceError), e:
            # log the connection loss
            dlf.writenotice('Connection lost when executing remote method', methodName=name, machine=machine,
                            Type=str(e.__class__), Message=str(e))
            # as the connection was lost, drop it
            self.close(machine)
            # and try again with a brand new connection, if it was our first attempt, or raise the exception otherwise
            if gotException:
              # replace custom exception with our own Timeout
              if isinstance(e, exceptions.ReferenceError):
                raise Timeout(e.args)
              raise
            else:
              gotException = True
      except Exception, e:
        if isinstance(e, CastorException):
          # this is our own exception, just propagate it without extra logging
          raise
        dlf.writenotice('Exception when executing remote method', methodName=name, machine=machine,
                       Type=str(e.__class__), Message=str(e))
        # amend errors with the name of the machine that we were connecting to
        e.args = e.args + ('connected to ' + machine,)
        # replace custom exception with our own Timeout
        if isinstance(e, rpyc.core.async.AsyncResultTimeout):
          # don't propagate an RPyC-specific exception
          raise Timeout(e.args)
        raise
    return f

# Global connection pool for all needed connections
connections = ConnectionPool()
