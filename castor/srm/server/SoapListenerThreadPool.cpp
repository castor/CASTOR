/******************************************************************************
 *                      SoapListenerThreadPool.cpp
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2004  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * A ListenerThreadPool able to handle SOAP connections as used by the SRM server
 *
 * @author Giuseppe Lo Presti
 *****************************************************************************/

// Include Files
#include <sstream>
#include <iomanip>
#include "castor/Services.hpp"
#include "castor/System.hpp"
#include "castor/log/log.hpp"
#include "castor/exception/Exception.hpp"
#include "Cuuid.h"

#include <cgsi_plugin.h>
#include "srmNsmap.h"
#include "soapH.h"
#include "castor/srm/SrmConstants.hpp"
#include "castor/srm/server/SoapListenerThreadPool.hpp"


//------------------------------------------------------------------------------
// bind
//------------------------------------------------------------------------------
void castor::srm::server::SoapListenerThreadPool::bind() throw (castor::exception::Exception)
{
  m_soap = soap_new();
  soap_init(m_soap);
  soap_cgsi_init(m_soap, CGSI_OPT_SERVER|CGSI_OPT_DISABLE_VOMS_CHECK|CGSI_OPT_DISABLE_NAME_CHECK);
  soap_set_namespaces(m_soap, namespaces);
  m_soap->bind_flags |= SO_REUSEADDR;

  /* Set up timeouts on receive and send in an effort to stop CGSI errors causing blockages */
  m_soap->send_timeout = castor::srm::SrmConstants::getInstance()->soapSendTimeout();
  m_soap->recv_timeout = castor::srm::SrmConstants::getInstance()->soapRecieveTimeout();
  m_soap->accept_timeout = 5;   // this does not seem to be taken into account... default is 30

  /* create a SOAP socket for the server and bind */
  m_port = castor::srm::SrmConstants::getInstance()->port();
  std::list<castor::log::Param> tmpP = {
      castor::log::Param("Port", m_port)
  };
  castor::log::write(LOG_DEBUG, "Attempting bind to port", tmpP);
  int masterSocket = soap_bind(m_soap, NULL, m_port, castor::srm::SrmConstants::getInstance()->soapBacklog());

  if ( !soap_valid_socket(masterSocket) ) {
    const char** faultCode = soap_faultcode(m_soap);
    std::string myFault;
    while (faultCode != NULL && *faultCode != NULL && *faultCode[0] != '\0' ) {
        myFault.append(*faultCode).append("; ");
        faultCode++;
    }
    const char** faultString = soap_faultstring(m_soap);
    std::string myFaultString;
    while (faultString != NULL && *faultString != NULL && *faultString[0] != '\0' ) {
        myFaultString.append(*faultString).append("; ");
        faultString++;
    }
    std::list<castor::log::Param> params = {
        castor::log::Param("Host", castor::System::getHostName()),
        castor::log::Param("Port", m_port),
        castor::log::Param("Message", "Failed to bind to port"),
        castor::log::Param("SOAPFaultCode", myFault),
        castor::log::Param("SOAPFaultString", myFaultString),
    };
    //"SOAP bind error" message
    castor::log::write(LOG_ERR, "Soap Bind error", params);
    //soap_print_fault(m_soap, stderr);

    //TODO - When the Communication exceptions grows a default ctor, we should
    // use this rather than a generic exception.
    //castor::exception::Communication x;
    castor::exception::Exception x;
    x.getMessage() << "SOAP bind failed: " << myFaultString << std::endl;
    throw x;      // this will make the server exit
  }
}

//------------------------------------------------------------------------------
// shutdown
//------------------------------------------------------------------------------
bool castor::srm::server::SoapListenerThreadPool::shutdown() throw()
{
  soap_closesock(m_soap);
  soap_done(m_soap);
  soap_end(m_soap);
  soap_free(m_soap);
  return castor::server::BaseThreadPool::shutdown();
}

//------------------------------------------------------------------------------
// listenLoop
//------------------------------------------------------------------------------
void castor::srm::server::SoapListenerThreadPool::listenLoop()
{
  for (;;) {
    try {
      int slaveSocket = soap_accept(m_soap);
      if(m_stopped) {
        // we just got stopped, don't bother processing as we're terminating
        break;
      }
      if (slaveSocket < 0) {
        char errstr[1024];
        soap_sprint_fault(m_soap, errstr, sizeof(errstr));
        // here we should really use m_soap->error, but we get 0...
        if(strstr(errstr, "Timeout")) continue;
        castor::exception::Exception x;
        x.getMessage() << "SOAP accept failed, " << errstr;
        throw x;
      }
      // Handle the request. The thread will deallocate the copied structure
      threadAssign(soap_copy(m_soap));
    }
    catch(castor::exception::Exception any) {
      std::list<castor::log::Param> p = {
        castor::log::Param("ErrorMessage", any.getMessage().str())};
      // "Exception caught", system level error at this point
      castor::log::write(LOG_ERR, "Exception caught", p);
    }
  }
}

//------------------------------------------------------------------------------
// terminate
//------------------------------------------------------------------------------
void castor::srm::server::SoapListenerThreadPool::terminate(void* param) throw()
{
  struct soap* soap = (struct soap*)param;
  // whenever we'll have proper OO abstraction in the SOAP interface, we could
  // give here a standard reply as follows:
  //soap->retStatus->statusCode =  srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
  //soap->retStatus->explanation->assign("The SRM server is too busy at the moment, please try again later");
  soap_destroy(soap);
  soap_end(soap);
  soap_done(soap);
  soap_free(soap);
}
