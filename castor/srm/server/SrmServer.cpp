/******************************************************************************
 *                      SrmServer.cpp
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2004  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Server part of the SRM software suite.
 * Handles all requests and pushes the asynchronous ones to the DB
 * for further processing by the Daemon part.
 *
 * @author castor-dev team
 *****************************************************************************/

// Include Files
#include <iostream>
#include "globus/globus_thread.h"

#include "castor/Constants.hpp"
#include "castor/ICnvSvc.hpp"
#include "castor/Services.hpp"
#include "castor/log/log.hpp"
#include "castor/log/SyslogLogger.hpp"
#include "castor/db/DbParamsSvc.hpp"
#include "castor/server/metrics/MetricsCollector.hpp"
#include "castor/exception/Exception.hpp"
#include <dlfcn.h>

#include "castor/srm/server/SrmThread.hpp"
#include "castor/srm/server/SoapListenerThreadPool.hpp"
#include "castor/srm/SrmConstants.hpp"
#include "castor/srm/server/SrmServer.hpp"


//------------------------------------------------------------------------------
// main method
//------------------------------------------------------------------------------
int main(int argc, char *argv[]) {
  try {
    castor::log::init(new castor::log::SyslogLogger("srmfed"));
    castor::srm::server::SrmServer server(std::cout, std::cerr);

    // Parse arguments and reads config file
    server.parseCommandLine(argc, argv);

    // Check availability of db service
    if(castor::BaseObject::services()->service("DbCnvSvc", castor::SVC_DBCNV) == 0) {
      castor::exception::Exception nosvc;
      nosvc.getMessage() << "No DB service available in config file";
      throw nosvc;
    }

    // Create main thread pool
    int nThreads = castor::srm::SrmConstants::getInstance()->nbServerThreads();
    int maxThreads = castor::srm::SrmConstants::getInstance()->nbMaxServerThreads();
    server.addThreadPool(
      new castor::srm::server::SoapListenerThreadPool("SRMThread",
        new castor::srm::server::SrmThread(), nThreads, maxThreads));

    // Create a db parameters service and fill with appropriate defaults
    castor::IService* s = castor::BaseObject::sharedServices()->service("DbParamsSvc", castor::SVC_DBPARAMSSVC);
    castor::db::DbParamsSvc* dbparams = dynamic_cast<castor::db::DbParamsSvc*>(s);
    if(dbparams == 0) {
      castor::exception::Exception e;
      e.getMessage() << "Could not instantiate the parameters service";
      throw e;
    }
    dbparams->setSchemaVersion(SRMSCHEMAVERSION);
    dbparams->setDbAccessConfFile(ORASRMCONFIGFILE);

    // enable multi-threading in the Globus runtime
    globus_thread_set_model("pthread");

    // SRM server started
    std::list<castor::log::Param> params =
     {castor::log::Param("Port", castor::srm::SrmConstants::getInstance()->port())};  
    castor::log::write(LOG_INFO, "SRM Server started", params);

    const bool runAsStagerSuperuser = false;
    server.start(runAsStagerSuperuser);
    return 0;
  } catch (castor::exception::Exception e) {
    std::cerr << "Caught castor exception : "
              << sstrerror(e.code()) << std::endl
              << e.getMessage().str() << std::endl;
  } catch (...) {
    std::cerr << "Caught general exception!" << std::endl;
  }
  return 1;
}


//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
castor::srm::server::SrmServer::SrmServer(std::ostream &stdOut, std::ostream &stdErr) :
  castor::server::MultiThreadedDaemon(stdOut, stdErr) {
  castor::log::write(LOG_INFO, "SrmServer started");
}

//------------------------------------------------------------------------------
// parseCommandLine
//------------------------------------------------------------------------------
void castor::srm::server::SrmServer::parseCommandLine(int argc, char* argv[]) {
  
  int c = 1;
  bool foreground = false;
  castor::srm::SrmConstants *srmConstants = castor::srm::SrmConstants::getInstance();
  srmConstants->readConfigFile();
  while ( c < argc ) {
    if (strcmp(argv[c], "-p") == 0 || strcmp(argv[c], "--port") == 0 ) {
        srmConstants->setPort(atoi(argv[++c]));
        c++;
    }
    else if ( strcmp(argv[c], "-b") == 0 || strcmp(argv[c], "--back-log") == 0 ) {
        srmConstants->setSoapBacklog(atoi(argv[++c]));
        c++;
    }
    else if ( strcmp(argv[c], "-S") == 0 || strcmp(argv[c], "--Sthreads") == 0 ) {
        srmConstants->setNbServerThreads(atoi(argv[++c]));
        c++;
    }
    else if ( strcmp(argv[c], "-C") == 0 || strcmp(argv[c], "--Castor-threads") == 0 ) {
        srmConstants->setMaxCastorThreads(atoi(argv[++c]));
        c++;
    }
    else if ( strcmp(argv[c], "-f") == 0 || strcmp(argv[c], "--foreground") == 0 ) {
        foreground = true;
        c++;
    }
    else if ( strcmp(argv[c], "-m") == 0 || strcmp(argv[c], "--metrics") == 0 ) {
        // initialize the metrics collector thread, custom metrics to come later on
        castor::server::metrics::MetricsCollector::getInstance(this);
        c++;
    }
    else {
        help(argv[0]);
        exit(1);
    }
  }
  setCommandLineHasBeenParsed(foreground);
}

//------------------------------------------------------------------------------
// help
//------------------------------------------------------------------------------
void castor::srm::server::SrmServer::help(std::string programName)
{
  std::cout << "Usage: " << programName << " [options]\n"
    "\n"
    "where options can be:\n"
    "\n"
    "\t--foreground      or -f            \tForeground\n"
    "\t--help            or -h            \tPrint this help and exit\n"
    "\t--Sthreads        or -S #threads   \tNumber of SRM server threads\n"
    "\t--Castor-threads  or -C #threads   \tNumber of concurrent threads allowed to be busy in Castor calls\n"
    "\t--port            or -p port       \tPort to listen on\n"
    "\t--config-file     or -c configFile \tPath to configuration file\n"
    "\t--back-log        or -b backlog    \tNumber of SOAP messages allowed to be queued\n"
    "\t--metrics         or -m            \tEnable metrics collection\n"
    "\n"
    "Comments to: Castor.Support@cern.ch\n";
}
