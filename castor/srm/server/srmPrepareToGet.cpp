#include <cgsi_plugin.h>

#include <osdep.h>
#include <castor/BaseAddress.hpp>
#include <castor/BaseObject.hpp>
#include <castor/Constants.hpp>
#include <castor/Services.hpp>
#include <castor/log/log.hpp>
#include <castor/server/Daemon.hpp>
#include <getconfent.h>

#include <castor/exception/Exception.hpp>

#include "soapH.h"

#include "castor/srm/SURL.hpp"
#include "castor/srm/server/SrmUtils.hpp"
#include "castor/srm/server/SrmLogger.hpp"
#include "castor/srm/server/RequestValidator.hpp"
#include "castor/srm/SrmConstants.hpp"
#include "castor/srm/Constants.hpp"
#include "castor/srm/ISrmSvc.hpp"
#include "castor/srm/SrmUser.hpp"
#include "castor/srm/StageRequest.hpp"
#include "castor/srm/StorageArea.hpp"
#include "castor/srm/SubRequest.hpp"
#include "castor/srm/SubRequestStatus.hpp"
#include "castor/srm/UserFile.hpp"

#include "ResponseWriter.hpp"

int srm__srmPrepareToGet(
        struct soap *soap,
        srm__srmPrepareToGetRequest*        srmPrepareToGetRequest,
        srm__srmPrepareToGetResponse_& rep) //< response parameter
{
    castor::srm::server::SrmLogger *myLogger = new castor::srm::server::SrmLogger();
    myLogger->LOGENTRY(soap);
    srm__srmPrepareToGetResponse *repp;
    srm__srmPrepareToGetRequest *req;
    uid_t uid;
    gid_t gid;

    req = srmPrepareToGetRequest;
    castor::srm::server::SrmUtils *utils = new castor::srm::server::SrmUtils();
    utils->map_user(soap, &uid, &gid);

    // Prepare the reply structure 
    try {
        repp = soap_new_srm__srmPrepareToGetResponse(soap, -1);
        repp->returnStatus = soap_new_srm__TReturnStatus(soap, -1);
        repp->returnStatus->explanation = soap_new_std__string(soap, -1);
        repp->requestToken = NULL;
        repp->arrayOfFileStatuses = NULL;
        repp->remainingTotalRequestTime = NULL;

        repp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREREQUEST_USCOREQUEUED;
        repp->returnStatus->explanation->assign("");
    }
    catch (...) {
        std::list<castor::log::Param> params = {
            castor::log::Param("REQID", myLogger->getId()),
            castor::log::Param("Message", "Out of memory")
        };
        castor::log::write(LOG_ALERT, "Exception caught", params);
        myLogger->LOGEXIT(srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR, "Out of memory");
        delete utils;
        delete myLogger;
        return SOAP_EOM;
    }
    rep.srmPrepareToGetResponse = repp;

    castor::srm::server::ResponseWriter   myWriter;
    castor::srm::server::RequestValidator validate;

    // set up database services
    castor::Services* svcs;
    castor::srm::ISrmSvc* srmSvc;
    castor::BaseAddress ad;
    try {
        svcs = castor::BaseObject::services();
        ad.setCnvSvcName("DbCnvSvc");
        ad.setCnvSvcType(castor::SVC_DBCNV);
        castor::IService *svc = castor::BaseObject::services()->service("DbSrmSvc", castor::SVC_DBSRMSVC);
        srmSvc = dynamic_cast<castor::srm::ISrmSvc*>(svc);
        if ( srmSvc == 0 ) {
            repp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
            repp->returnStatus->explanation->assign("Unable to establish database service");
            std::list<castor::log::Param> params = {castor::log::Param("REQID", myLogger->getId())};
            castor::log::write(LOG_ERR, "Could not get Conversion Service for Database", params);
            myLogger->LOGEXIT(repp->returnStatus->statusCode, repp->returnStatus->explanation->c_str());
            delete utils;
            delete myLogger;
            myWriter.write(rep);
            return SOAP_OK;
        }
    }
    catch (castor::srm::SrmException& e) {
        repp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        repp->returnStatus->explanation->assign( e.getMessage().str());
        myLogger->LOGEXCEPTION(&e, &ad);
        myLogger->LOGEXIT(repp->returnStatus->statusCode, repp->returnStatus->explanation->c_str());
        delete utils;
        delete myLogger;
        myWriter.write(rep);
        return SOAP_OK;
    }

    // Get current user 'cos we will need it
    std::string dn, uname, vo;
    try {
        dn    = utils->getDn(soap);
        uname = utils->getUserName(soap);
        vo    = utils->getVoName(soap);
    } catch(castor::exception::Exception &e) {
        repp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREAUTHENTICATION_USCOREFAILURE;
        repp->returnStatus->explanation->assign ("Could not get user information");
        myLogger->LOGEXIT(repp->returnStatus->statusCode, repp->returnStatus->explanation->c_str());
        delete utils;   
        delete myLogger;  
        myWriter.write(rep);
        return SOAP_OK;
    }


    castor::srm::SrmConstants *srmConstants = castor::srm::SrmConstants::getInstance();
    castor::srm::StageRequest* dbReq = 0;
    castor::srm::SrmUser *thisUser = 0;
    std::string protocol(srmConstants->supportedProtocols()[0]);
    std::string stagerHost;
    std::string svcClass;
    try {
        stagerHost = utils->getStagerHost(soap);
        validate.notNull(srmPrepareToGetRequest->arrayOfFileRequests, "arrayOfFileRequests");
        validate.arrayOfGetFileRequests(srmPrepareToGetRequest->arrayOfFileRequests);
        if (srmPrepareToGetRequest->desiredFileStorageType)
            validate.storageType(*(srmPrepareToGetRequest->desiredFileStorageType));
        // Validate any space token supplied
        if (req->targetSpaceToken) {
            castor::srm::StorageArea *area = srmSvc->getStorageArea(*(req->targetSpaceToken));
            validate.storageArea(area);
            validate.svcClass(area);
            svcs->fillObj(&ad, area, castor::srm::OBJ_SrmUser);
            validate.permission(area, vo);
            svcClass = area->svcClass();
            delete area->srmUser();
            delete area;
        }
        // Validate any protocol supplied
        if ( req->transferParameters && 
             req->transferParameters->arrayOfTransferProtocols &&
             req->transferParameters->arrayOfTransferProtocols->stringArray.size() > 0) {
            validate.protocol(req->transferParameters->arrayOfTransferProtocols->stringArray,
                    &protocol);
        }
        validate.lifetime(srmPrepareToGetRequest->desiredPinLifeTime, true);
        validate.lifetime(srmPrepareToGetRequest->desiredTotalRequestTime, true);
    } catch (castor::exception::Exception &e) {
        castor::srm::SrmException* se = dynamic_cast<castor::srm::SrmException*>(&e);
        if(se) {
            repp->returnStatus->statusCode = (srm__TStatusCode)se->code();
        } else {
            repp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        }
        repp->returnStatus->explanation->assign(e.getMessage().str());
        myLogger->LOGEXIT(repp->returnStatus->statusCode, repp->returnStatus->explanation->c_str());
        delete utils;   
        delete myLogger;  
        myWriter.write(rep);
        return SOAP_OK;
    }

    // Other necesssary checks:
    // SRM section 5.1.2(e): Access latency must be ONLINE
    if (srmPrepareToGetRequest->targetFileRetentionPolicyInfo &&
        srmPrepareToGetRequest->targetFileRetentionPolicyInfo->accessLatency &&
        *(srmPrepareToGetRequest->targetFileRetentionPolicyInfo->accessLatency) != srm__TAccessLatency__ONLINE ) {
    }

    try {
        // Assign a storageArea pointer - we will need it later
        std::string mySpaceToken;
    
        // The only thing left to fill in in the response for this subrequest is the
        // the request expiration time, to be taken either from the request or from the default value.
        signed64 reqTime;
        if ( srmPrepareToGetRequest->desiredTotalRequestTime ) {
            reqTime = *(srmPrepareToGetRequest->desiredTotalRequestTime);
        }
        else {
            reqTime = castor::srm::SrmConstants::getInstance()->defaultLifetime();
        }
    
        // Get the user
        thisUser = srmSvc->getSrmUser(uname, vo, dn);
        // CastorRequestId is assigned by the daemon based on stage_PrepareToGet...
        dbReq = new castor::srm::StageRequest();
        if (req->userRequestDescription) dbReq->setRequestDesc(*(req->userRequestDescription));
        dbReq->setProtocol(protocol);
        dbReq->setEuid(uid);
        dbReq->setEgid(gid);
        dbReq->setStatus(castor::srm::REQUEST_PENDING);
        dbReq->setSrmUser(thisUser);
        dbReq->setRequestType(castor::srm::REQUESTTYPE_GET);
        dbReq->setHandler(1);
        dbReq->setExpirationInterval(reqTime);
        dbReq->setProxyCert(" ");
        dbReq->setSrmRequestId(myLogger->getIdAsString());
        dbReq->setRhHost( stagerHost );
        dbReq->setRhPort( STAGERPORT );
        dbReq->setSvcClass(svcClass);

        // Build up responses for each subrequest
        repp->arrayOfFileStatuses = soap_new_srm__ArrayOfTGetRequestFileStatus(soap, -1);
        unsigned  dirCount=0;
        bool validSubrequest = false;
        for (unsigned i = 0; 
             i < srmPrepareToGetRequest->arrayOfFileRequests->requestArray.size(); 
             i++) {
            // Get the subrequest from the request
            srm__TGetFileRequest *fileReq = req->arrayOfFileRequests->requestArray[i];

            // Allocate space for response
            srm__TGetRequestFileStatus* fileStatus;
            fileStatus = soap_new_srm__TGetRequestFileStatus(soap, -1);
            fileStatus->sourceSURL.assign( fileReq->sourceSURL );
            fileStatus->fileSize                 = NULL;
            fileStatus->status                   = soap_new_srm__TReturnStatus(soap, -1);
            fileStatus->status->explanation      = soap_new_std__string(soap, -1);
            fileStatus->status->statusCode       = srm__TStatusCode__SRM_USCOREREQUEST_USCOREQUEUED;
            fileStatus->status->explanation->assign("");
            fileStatus->estimatedWaitTime        = NULL;
            fileStatus->remainingPinTime         = NULL;
            fileStatus->transferURL              = NULL;
            fileStatus->transferProtocolInfo     = NULL;

            // Also create a subrequest object
            castor::srm::SubRequest *subrequest = new castor::srm::SubRequest();
            if (srmPrepareToGetRequest->targetSpaceToken) 
                subrequest->setSpaceToken(srmPrepareToGetRequest->targetSpaceToken->c_str());
            subrequest->setTurl("");
            subrequest->setCastorFileName("");
            subrequest->setReservedSize(0);
            subrequest->setReason("");
            subrequest->setErrorCode(0);
            subrequest->setSurl(fileReq->sourceSURL);
            subrequest->setCastorReqId("");
            subrequest->setSvcClass(svcClass);
            subrequest->setStatus(castor::srm::SUBREQUEST_PENDING);
            subrequest->setRequest(dbReq);
            dbReq->addSubRequests(subrequest);

            // Validate request object
            std::string cfn;
            try {
                castor::srm::SURL* surl = validate.surl(fileReq->sourceSURL);
                cfn = surl->getFileName();
                delete surl;
            } catch (castor::exception::Exception &e) {
                fileStatus->status->statusCode = (srm__TStatusCode)e.code();
                fileStatus->status->explanation->assign(e.getMessage().str());
                repp->arrayOfFileStatuses->statusArray.push_back(fileStatus);
                subrequest->setStatus(castor::srm::SUBREQUEST_FAILED);
                subrequest->setReason(e.getMessage().str());
                continue;
            }
            subrequest->setCastorFileName(cfn);

            // Get the size of the file and include in response.
            Cns_fileid nsId;
            nsId.fileid = 0;
            strncpy(nsId.server, castor::srm::SrmConstants::getInstance()->nsHost().c_str(), sizeof(nsId.server)-1);
            struct Cns_filestat buf;
            myLogger->logCnsCall("Cns_stat", cfn);
            int rc = Cns_stat(cfn.c_str(), &buf);
            myLogger->logCnsReturn("Cns_stat", rc);
            if ( rc == 0 ) {
                fileStatus->fileSize = (ULONG64*)soap_malloc(soap, sizeof(ULONG64));
                *(fileStatus->fileSize) = buf.filesize;
                nsId.fileid = buf.fileid;
                myLogger->LOGFILEREQ(&nsId, cfn);
                subrequest->setReservedSize(buf.filesize); // fileSize as given by Cns_stat()
            }
            else {
                switch (serrno) {
                    case ENOENT:
                    case EFAULT:
                    case ENOTDIR:
                    case ENAMETOOLONG:
                        fileStatus->status->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREPATH;
                        fileStatus->status->explanation->assign(sstrerror(serrno));
                        break;
                    case EACCES:
                        fileStatus->status->statusCode = srm__TStatusCode__SRM_USCOREAUTHORIZATION_USCOREFAILURE;
                        fileStatus->status->explanation->assign(strerror(serrno));
                        break;
                    case SENOSHOST:
                    case SENOSSERV:
                    case SECOMERR:
                    case ENSNACT:
                        fileStatus->status->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
                        fileStatus->status->explanation->assign(sstrerror(serrno));
                        break;
                    default:
                        fileStatus->status->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
                        fileStatus->status->explanation->assign("Unexpected error status");
                        break;
                }
                repp->arrayOfFileStatuses->statusArray.push_back(fileStatus);
                // We also need to insert this into the database since future StatusOf requests should
                // indicate that this request failed.
                subrequest->setStatus(castor::srm::SUBREQUEST_FAILED);
                subrequest->setErrorCode(serrno);
                subrequest->setReason(sstrerror(serrno));
                continue;
            }

            // According to SRM spec, we have to fail the whole request is any
            // SURL matches a directory (5.1.3-SRM_NOT_SUPPORTED).  
            if ( (buf.filemode & S_IFDIR) ) {
                repp->returnStatus->statusCode = srm__TStatusCode__SRM_USCORENOT_USCORESUPPORTED;
                repp->returnStatus->explanation->assign("Recursion of GET not supported");
                utils->unmap_user();
                myLogger->LOGEXIT(repp->returnStatus->statusCode, repp->returnStatus->explanation->c_str());
                myWriter.write(rep);
                for (std::vector<castor::srm::SubRequest*>::const_iterator it = dbReq->subRequests().begin();
                     it != dbReq->subRequests().end();
                     it++) {
                  if((*it)->userFile()) {
                    delete (*it)->userFile();
                  }
                  (*it)->setUserFile(0);
                  (*it)->setRequest(0);
                  delete (*it);
                }
                dbReq->subRequests().clear();
                delete dbReq;
                delete thisUser;
                delete utils;
                delete myLogger;
                return SOAP_OK;
            }
            /*
             * This is a better implementation whereby we report INVALID_PATH for the
             * case that the directory is embedded in a number of valid SURL's.
             * However, since clients do not expect this logic it is left for some 
             * future enlightened review of the spec - if ever
            if ( (buf.filemode & S_IFDIR) ) {
                fileStatus->status->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREPATH;
                fileStatus->status->explanation->assign("Directories not supported in a GET request");
                repp->arrayOfFileStatuses->statusArray.push_back(fileStatus);
                subrequest->setStatus(castor::srm::SUBREQUEST_FAILED);
                subrequest->setReason("Directories not supported in a GET request");
                dirCount++;
                continue;
            }
             */
            
            // After this, we should have a valid subrequest.
            validSubrequest = true;

            fileStatus->estimatedWaitTime = (int*)soap_malloc(soap, sizeof(int));
            *(fileStatus->estimatedWaitTime) = 3;
            fileStatus->remainingPinTime = (int*)soap_malloc(soap, sizeof(int));
            *(fileStatus->remainingPinTime) = reqTime;
            repp->arrayOfFileStatuses->statusArray.push_back(fileStatus);

            // create the subRequest in the database and attach the userFile
            svcs->createRep(&ad, subrequest, false);
            srmSvc->createUserFile(subrequest, nsId.fileid, castor::srm::SrmConstants::getInstance()->nsHost(), buf.filesize);
        }

        if ( !validSubrequest ) {
            // No valid subrequests found, or they all failed
            repp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
            repp->returnStatus->explanation->assign("No valid subrequests found");
        }
        if (dirCount == srmPrepareToGetRequest->arrayOfFileRequests->requestArray.size()) {
            repp->returnStatus->statusCode = srm__TStatusCode__SRM_USCORENOT_USCORESUPPORTED;
            repp->returnStatus->explanation->assign("Get requests on a directory are not supported");
        }
        thisUser->addRequests(dbReq);
        svcs->createRep(&ad, dbReq, false);
        svcs->fillRep(&ad, dbReq, castor::srm::OBJ_SubRequest, false);
        svcs->fillRep(&ad, dbReq, castor::srm::OBJ_SrmUser, true);
        std::list<castor::log::Param> params = {castor::log::Param("REQID", myLogger->getId())};
        castor::log::write(LOG_DEBUG, "Request stored in DB", params);


        // notify the daemon about the new request
        //try {
        //    castor::server::Daemon::sendNotification(castor::srm::SrmConstants::getInstance()->notifyHost(),
        //      castor::srm::SrmConstants::getInstance()->reqNotifyPort(), 'S');
        //} catch (castor::exception::Exception ignored) {}

        // if we arrive here everything has been fine    
        std::stringstream ss;
        ss << dbReq->id() << std::ends;
        repp->requestToken = soap_new_std__string(soap, -1);
        repp->requestToken->assign( ss.str());
        std::list<castor::log::Param> rtP = {
            castor::log::Param("REQID", myLogger->getId()),
            castor::log::Param("RequestToken", dbReq->id())
        };
        castor::log::write(LOG_INFO, "RequestToken assigned", rtP);
    }
    catch (castor::exception::Exception e) {
        repp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        repp->returnStatus->explanation->assign(e.getMessage().str());
        myLogger->LOGEXCEPTION(&e, &ad);
    }
    if(dbReq) {
      for (std::vector<castor::srm::SubRequest*>::const_iterator it = dbReq->subRequests().begin();
           it != dbReq->subRequests().end();
           it++) {
        if((*it)->userFile()) {
          delete (*it)->userFile();
        }
        (*it)->setUserFile(0);
        (*it)->setRequest(0);
        delete (*it);
      }
      dbReq->subRequests().clear();
      delete dbReq;
    }
    if (thisUser) delete thisUser;

    utils->unmap_user();
    myWriter.write(rep);
    myLogger->LOGEXIT(repp->returnStatus->statusCode, repp->returnStatus->explanation->c_str());
    delete utils;
    delete myLogger;
    return SOAP_OK;
}
