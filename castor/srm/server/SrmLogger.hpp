/******************************************************************************
 *                      srm/Srmlogger.hpp
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * 
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/
#ifndef SRM_SRMLOGGER_HPP
#define SRM_SRMLOGGER_HPP

// System Headers
#include <map>

// CASTOR headers
#include <h/stager_client_api.h>
#include <castor/exception/Exception.hpp>
#include <castor/BaseAddress.hpp>
#include <castor/log/log.hpp>
#include <h/Cuuid.h>
#include <h/Cns_api.h>
#include <serrno.h>

// SOAP headers
#include <soapH.h>

// Macros which will add function information
#define LOGENTRY(soap)  logEntry(soap, __func__)
#define LOGSTARTEDREQ(count)  logStartedReq(__func__, count)
#define LOGEXIT(status, message)  logExit(status, message, __func__)  
#define LOGFILEREQ(nsId, filename)  logFileRequest(nsId, filename, __func__)
#define LOGEXCEPTION(e, ad)  logException(e, ad, __func__)

namespace castor {
 namespace srm {
  namespace server {
    class SrmLogger {
        public:
            SrmLogger();
            SrmLogger(std::string targetReqId);
            void logEntry(struct soap *soap, std::string function);
            void logStartedReq(std::string function, unsigned count);
            void logExit(srm__TStatusCode status, std::string message, std::string function);
            void logFileRequest(Cns_fileid* nsId, std::string filename, std::string function);
            void logException(castor::exception::Exception* e, castor::BaseAddress* ad, std::string function);
            
            void logFileFailure(Cns_fileid* nsId, std::string filename, std::string message);
            void logCnsCall(std::string cnsCall, std::string file, Cns_fileid* nsId = 0, unsigned int logLevel = LOG_DEBUG);
            void logCnsReturn(std::string cnsCall, int status, Cns_fileid* nsId = 0);
            void logStagerCall(std::string stgCall, struct stage_options opts, Cns_fileid* nsId = 0, unsigned int logLevel = LOG_INFO);
            void logStagerReturn(std::string stgCall, int status, std::string cReqId, Cns_fileid* nsId = 0);

            Cuuid_t getId() { return _myReqId; }
            std::string getTargetId() { return _targetReqId; }
            std::string getIdAsString();

            void setId(std::string s_newTargetReqId);

        private:
            std::string  decodeStatus(srm__TStatusCode status);
            Cuuid_t _myReqId;
            std::string _targetReqId;
            timeval tvStart;
    };
  }
 }
}

#endif
