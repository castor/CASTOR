#include <fstream>
#include <sstream>
#include <unistd.h>
#include <algorithm>

#include <cgsi_plugin.h>

#include <stager_client_api_common.hpp>
#include <stager_client_api.h>
#include <stager_mapper.h>
#include <getconfent.h>
#include <Cpwd.h>
#include <Cgrp.h>
#include <Cns_api.h>
#include <castor/exception/Exception.hpp>
#include <castor/log/log.hpp>

#include "castor/srm/SrmUtils.hpp"
#include "castor/srm/SrmConstants.hpp"
#include "castor/srm/server/SrmUtils.hpp"

pthread_mutex_t castor::srm::server::SrmUtils::mutex = PTHREAD_MUTEX_INITIALIZER;
std::map<std::string, std::string> castor::srm::server::SrmUtils::s_castorVersions;

const int castor::srm::server::SrmUtils::STAGEPORT = 9002;

// XXX This declaration used to be in cgsi_plugin.h but it got dropped in version 1.3.5.
// XXX But the implementation is still available in the libcgsi_plugin library and we use it!
int retrieve_voms_credentials(struct soap *soap);

castor::srm::server::SrmUtils::SrmUtils() {
}

int castor::srm::server::SrmUtils::map_user(struct soap *soap, uid_t *uid, gid_t *gid) {
    if ( is_context_established(soap) <= 0 ) {
        // Context not established
        *uid = 0;
        *gid = 0;
        return -1;
    }

    // Get the real and effective user so we can unmap
    realUID = getuid();
    realGID = getgid();
    effectiveGID = getegid();
    effectiveUID = geteuid();

    std::string thisUser = getUserName(soap);

    struct passwd *pw = Cgetpwnam(thisUser.c_str());
    if ( 0 == pw ) {
        // Could not find user
        return -1;
    }
    uid_t localUID = pw->pw_uid;
    gid_t localGID = pw->pw_gid;

    // Modified to use stager_API
    stage_setid( localUID, localGID);
    Cns_setid( localUID, localGID);

    if ( 0 != uid ) {
        *uid = localUID;
    }
    if ( 0 != gid ) {
        *gid = localGID;
    }

    return 0;
}

void castor::srm::server::SrmUtils::unmap_user() {
    // Changed to user stager and nameserver APIs
    stage_resetid();
    Cns_unsetid();
}

void castor::srm::server::SrmUtils::getUserAndGroup( std::string userID, uid_t* uid, gid_t* gid) throw(castor::exception::Exception) {
    struct passwd *pw = Cgetpwnam(userID.c_str());
    if ( 0 == pw ) {
        castor::exception::Exception x;
        //cout << "Cgetpwnam failed for user " << userID.c_str() << endl;
        x.getMessage() << "User id not recognised" << std::ends;
        throw x;
        //cout << "x thrown and returned" << endl;
        return;
    }

    *uid = pw->pw_uid;
    *gid = pw->pw_gid;

    //free(pw);
    return;
}
    
const std::string* castor::srm::server::SrmUtils::set_delegated_credentials( struct soap *soap, uid_t uid, gid_t gid ) {
    char tmpFile[] = "/tmp/srmFileXXXXXX";

    if (!has_delegated_credentials(soap) ) {
        // Client did not provide delegated credentials
        return NULL;
    }

    int fd;
    if ( (fd = mkstemp(tmpFile)) == -1) {
        // Can not create temp file
        return NULL;
    }

    fchown(fd, uid, gid);

    if ( export_delegated_credentials(soap, tmpFile) < 0 ) {
        // Problem exporting delegated credentials
        close (fd);
        unlink (tmpFile);
        return NULL;
    }

    if ( set_default_proxy_file(soap, tmpFile) < 0 ) {
        //Problem while setting default proxy file
        close (fd);
        unlink (tmpFile);
        return NULL;
    }
    close (fd);
    return new std::string(tmpFile);
}

const std::string castor::srm::server::SrmUtils::toString (int val) {
    std::stringstream ss;
    ss << val;
    return ss.str();
}

bool castor::srm::server::SrmUtils::isGroupMapped( ) {
    // default is true for safetly
    bool rtn = true;

    static const std::string delim = " ";

    // Open grid map file
    std::ifstream myFile ("/etc/grid-security/grid-mapfile");
    if ( myFile.is_open() ) {
        std::string line;
        std::string lastword;
        getline(myFile, line);

        //Skip any delimiters at start...
        std::string::size_type lastPos = line.find_first_not_of(delim, 0);
        // Find first non-delimiter
        std::string::size_type     pos = line.find_first_of(delim, lastPos);

        // Loop over std::string
        while ( std::string::npos != pos || std::string::npos != lastPos ) {
            lastword = line.substr(lastPos, pos-lastPos);
            lastPos  = line.find_first_not_of(delim, pos);
            pos      = line.find_first_of(delim, lastPos);
        }
        if ( lastword[0] != '.' )  rtn = false;  // User mapped
    }
    return rtn;
}

std::string castor::srm::server::SrmUtils::getStagerHost (struct soap *soap) throw (castor::exception::Exception) {
    std::string voname = getVoName(soap);
    transform ( voname.begin(), voname.end(), voname.begin(), ( int(*)(int))toupper );

    return getStagerHost(voname);
}

std::string castor::srm::server::SrmUtils::getStagerHost( std::string voname ) throw (castor::exception::Exception) {
    // If no mapping found, then refuse the request
    std::string stageHost = "";

    // If STAGE_HOST is set, then we use this and ignore
    // everything else
    if ( getenv("STAGE_HOST") && strlen(getenv("STAGE_HOST")) > 0 ) {
        stageHost = getenv("STAGE_HOST");
    }
    else {
        // Get the VO name
        char *ptr;
        if ( voname.length() == 0 ) {
          // get the default mapping if available
            ptr = getconfent("SRM", "STAGE_HOST", 0);
        }
        else {
            ptr = getconfent("SRM", const_cast<char*> (voname.c_str()), 0);
        }
        if ( ptr ) {
            stageHost = ptr;
        }
    }
    if ( stageHost == "" ) {
        // this is not permitted
        castor::exception::Exception x((int)srm__TStatusCode__SRM_USCOREAUTHORIZATION_USCOREFAILURE);
        std::string message("No stager host configured for VO ");
        message.append(voname);
        x.getMessage() << "Access denied for users not belonging to any VO";
      throw x;
    }
    return stageHost;
}

std::vector<std::string> castor::srm::server::SrmUtils::getSvcClassList(std::string vo) {
    std::vector<std::string> rtnVector;
    std::string mapFile = castor::srm::SrmConstants::getInstance()->storageMappingFile();
    std::string myVO = vo;
    transform(myVO.begin(), myVO.end(), myVO.begin(), toupper);
    char** svcClassList;
    int nSvcClasses;
    int rc = getconfent_multi_fromfile
        (const_cast<char*>(mapFile.c_str()),
         const_cast<char*>(myVO.c_str()), "default", 10, &svcClassList, &nSvcClasses);
    if (rc != 0 || nSvcClasses == 0) return rtnVector;
    for (int i=0; i<nSvcClasses; i++)
        rtnVector.push_back(svcClassList[i]);
    return rtnVector;
}

std::string castor::srm::server::SrmUtils::getSvcClass(std::string &vo,
        srm__TRetentionPolicy policy,
        srm__TAccessLatency latency) 
throw (castor::exception::Exception) {
    std::string  mapFile = castor::srm::SrmConstants::getInstance()->storageMappingFile();
    std::string myVO = vo;
    transform(myVO.begin(), myVO.end(), myVO.begin(), toupper);
    std::string mapping;
    if ( policy == srm__TRetentionPolicy__CUSTODIAL && latency == srm__TAccessLatency__NEARLINE ) {
        mapping="TAPE1DISK0";
    }
    else if (  policy == srm__TRetentionPolicy__CUSTODIAL && latency == srm__TAccessLatency__ONLINE ) {
        mapping="TAPE1DISK1";
    }
    else if ( policy == srm__TRetentionPolicy__REPLICA && latency == srm__TAccessLatency__ONLINE) {
        mapping="TAPE0DISK1";
    }
    else {
        castor::exception::Exception e((int)srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST);
        e.getMessage() << "Unable to map accessLatency/RetentionPolicy to service class";
        throw e;
    }
        
    char *ptr = getconfent_fromfile(
            const_cast<char*>(mapFile.c_str()),
            const_cast<char*>(myVO.c_str()),
            const_cast<char*>(mapping.c_str()),
            0);
    if (!ptr) {
        castor::exception::Exception e((int)srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST);
        e.getMessage() << "No entry for " << mapping << " for VO " << myVO;
        throw e;
    }
    return ptr;
}

std::string castor::srm::server::SrmUtils::getClientAddress(struct soap *soap) {
    struct sockaddr_in ad;
    socklen_t ad_len = sizeof(ad);
    std::ostringstream ipout;
    if ( getpeername(soap->socket, (struct sockaddr*)&ad, &ad_len) == -1 ) {
        return strerror(errno);
    }
    else {
        ipout << castor::log::IPAddress(ntohl(ad.sin_addr.s_addr));
        return ipout.str();
    }
}

std::string castor::srm::server::SrmUtils::getDn(struct soap *soap) {
    char dn[1024];
    get_client_dn(soap, dn, sizeof(dn));
    return std::string(dn);
}

std::string castor::srm::server::SrmUtils::getVoName(struct soap *soap) {
    char *vc=0;
    if ( retrieve_voms_credentials(soap) == 0 ) {
        vc = get_client_voname(soap);
    }
    if (!vc) {
        // See if we can get the VO based on the user
        vc = const_cast<char*>(getVoName(getUserName(soap)).c_str());
    }
    return vc;
}

std::string castor::srm::server::SrmUtils::getVoName(std::string username) throw (castor::srm::SrmException) {
    std::string mapFile = castor::srm::SrmConstants::getInstance()->storageMappingFile();
    if ( mapFile.length() == 0 ) {
        std::list<castor::log::Param> dbgParam =  {
            castor::log::Param ("Message", "Configuration error; srm2_storagemap file not defined"),
        };
        // "Configuration error"
        castor::log::write(LOG_ERR, "Configuration error", dbgParam);
        castor::srm::SrmException x(srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR);
        x.getMessage() << "Configuration error; srm2_storagemap file not defined" << std::ends;
        throw x;
    }
    struct stat buf;
    if ( stat( mapFile.c_str(), &buf) != 0 ) {
        // "Configuration error"
        std::list<castor::log::Param> dbgParam =  {
            castor::log::Param ("Message", "Unable to stat storage map file"),
            castor::log::Param ("MapFile", mapFile.c_str()),
        };
        castor::log::write(LOG_ERR, "Configuration error", dbgParam);
        castor::srm::SrmException x(srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR);
        x.getMessage() << "Configuration error; srm2_storagemap(" << mapFile << ") does not exist" << std::ends;
        throw x;
    }

    char *ptr = getconfent_fromfile(const_cast<char*>(mapFile.c_str()),
                  "USTGMAP", const_cast<char*>(username.c_str()), 0);
    if (!ptr) {
        ptr = getconfent_fromfile(const_cast<char*>(mapFile.c_str()),
                "USTGMAP", "*", 0);
    }
    if (!ptr) {
        std::list<castor::log::Param> dbgParam =  {
            castor::log::Param ("Message", "Configuration Error: No mapping for User/VO"),
            castor::log::Param ("UserName", username.c_str()),
            castor::log::Param ("MapFile", mapFile.c_str()),
        };
        castor::log::write(LOG_ERR, "Configuration error", dbgParam);
        castor::srm::SrmException x(srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR);
        x.getMessage() << "Configuration error; srm2_storagemap(" << mapFile << ") does not exist" << std::ends;
        throw x;
    }
    return ptr;
}

std::string castor::srm::server::SrmUtils::getVoRole(struct soap *soap) {
    std::string role;
    char **roles;

    int nfqans = 0;
    if ( retrieve_voms_credentials(soap) == 0  && ( roles = get_client_roles(soap, &nfqans)) != NULL ) {
        if ( nfqans>0 ) role = roles[0];
    }
    return role;
}

std::string castor::srm::server::SrmUtils::getUserName(struct soap *soap) {
    std::string role = getVoRole(soap);
    std::string thisUser;
    if ( role.length() != 0 ) {
        // Strip any Capability field from the role
        if ( role.find("/Capability") != std::string::npos ) {
            role = role.substr(0, role.find("/Capability"));
        }
        // See if the entry is in the VOMS mapping file
        std::ifstream vomsReader;
        vomsReader.open("/etc/grid-security/voms-mapfile", std::ios::in);
        if ( vomsReader.is_open() ) {
            while ( vomsReader.good() ) {
                std::string line;
                getline(vomsReader, line);
                if (line.find(role) != std::string::npos) {
                    std::vector<std::string> tokens;
                    std::string buf;
                    std::stringstream ss(line);
                    while (ss >> buf)
                        tokens.push_back(buf);
                    if (tokens.size() > 0) {
                        thisUser = tokens[tokens.size()-1];
                    }
                    break;
                }
            }
            vomsReader.close();
        }
    }
    if (thisUser.length() == 0) {
        char userName[64] = "\0";
        if ( get_client_username(soap, userName, 64) < 0 || userName[0] == '\0') {
            // user not found or empty username returned
            castor::srm::SrmException x(srm__TStatusCode__SRM_USCOREAUTHENTICATION_USCOREFAILURE);
            x.getMessage() << "Unable to map current DN to a User" << std::ends;
            throw x;
        }
        thisUser.assign(userName);
    }
    return thisUser;
}

std::string castor::srm::server::SrmUtils::getCastorVersion (struct soap *soap) {
    std::string voname = getVoName(soap);
    return ( getCastorVersion(voname) );
}

std::string castor::srm::server::SrmUtils::getCastorVersion (std::string vo) {

    transform ( vo.begin(), vo.end(), vo.begin(), ( int(*)(int))toupper );

    std::string stagerHost;
    if ( vo.length() == 0 ) {
        stagerHost = getconfent("SRM", "STAGE_HOST", 0);
    }
    else {
        char* buf = getconfent("SRM", const_cast<char*> (vo.c_str()), 0);
        if(buf) stagerHost = buf;
    }
    if ( stagerHost.length() == 0 && getenv("STAGE_HOST") && strlen(getenv("STAGE_HOST")) > 0 ) {
        stagerHost = getenv("STAGE_HOST");
    }
    
    if ( stagerHost.length() == 0 ) {
        // this means we don't have a configured STAGE_HOST. Just return the
        // major version, anyway other requests are likely to fail.
        return "2";   
    }
    if(s_castorVersions.find(stagerHost) == s_castorVersions.end()) {
        // we don't have a cached value, query the stager
        struct stage_options opts;
        opts.stage_host = (char*)stagerHost.c_str();
        opts.stage_port = castor::srm::server::SrmUtils::STAGEPORT;
        opts.service_class = NULL;
        int Mv, mv, Mr, mr;
        
        stage_setid(castor::srm::SrmConstants::getInstance()->getStageUid(), castor::srm::SrmConstants::getInstance()->getStageGid());
        int rc = stage_version(&Mv, &mv, &Mr, &mr, &opts);
        if(rc == 0) {
           std::stringstream ss;
           ss << Mv << '.' << mv << '.' << Mr << '-' << mr;
           // we protect with a mutex only the insertion in the map, not
           // the (potentially long) call to the stager.
           pthread_mutex_lock(&mutex);
           s_castorVersions[stagerHost] = ss.str();
           pthread_mutex_unlock(&mutex);
        }
        else {
           // the call failed
           return "2";
        }
    }
    return s_castorVersions[stagerHost];
}

bool castor::srm::server::SrmUtils::supportedFeature (struct soap *soap, const std::string feature) {
    bool isSupported = false;

    // First make sure the feature is defined - if not assume it is not.  Note support for these
    // feature MUST be in castor.conf; they can not be overridden by environment variables.
    char *ptr = getconfent("SRMFEATURE", const_cast<char*>(feature.c_str()), 0);
    if (ptr) {
        std::string featureVersion = ptr;
        // So the feature is specified and ptr gives the castor version which supports it.  Now we need to also get
        // the castor version of the current stager instance
        std::string voname = getVoName(soap);
        std::string stagerVersion = getCastorVersion(voname);
        // Now we need to compare the two versions.  Easiest way is to split the std::string
        // into substrings and compare the result.  Here we will naughtily use non std::string function
        // sscanf - why? 'cos I can!
        int major0, minor0, version0;
        int major1, minor1, version1;
        sscanf(featureVersion.c_str(), "%d.%d.%d", &major0, &minor0, &version0);
        sscanf(stagerVersion.c_str() , "%d.%d.%d", &major1, &minor1, &version1);

        if ( major1 >= major0 &&
             minor1 >= minor0 &&
             version1 >= version0 )
            isSupported = true;
    }

    return isSupported;
}
