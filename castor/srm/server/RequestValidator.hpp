/******************************************************************************
 *                      Validator.hpp
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This class attempts to handle validation of incoming requests.
 * In the event of an invalid request, each method will throw an
 * error containing an SRM return code and a suitable message which
 * will be forwarded to the client
 *
 * @author Shaun de Witt
 *****************************************************************************/
#ifndef SRM_VALIDATOR_HPP
#define SRM_VALIDATOR_HPP

#include <string>
#include <vector>

#include <soapH.h>
#include <castor/srm/SURL.hpp>
#include <castor/srm/SrmException.hpp>
#include <castor/srm/StageRequest.hpp>
#include <castor/srm/StorageArea.hpp>
#include <castor/srm/SrmConstants.hpp>

namespace castor {
 namespace srm {
    namespace server {
        class RequestValidator {
            public:
                //
                // Check if the supplied request token exists.  If it does, this also ensures
                // the supplied value is an unsigned integer.
                //
                bool requestToken(std::string token) throw (srm::SrmException);

                //
                // Check if the supplied space toekn is not empty.
                //
                bool spaceToken(std::string token) throw (srm::SrmException);

                //
                // See if the array of SURLs are supplied.
                //
                bool arrayOfSurls(std::vector<std::string>) throw (srm::SrmException);

                //
                // Does the supplied request token represent a valid request
                //
                bool request(srm::StageRequest* request, int reqType=-1) throw (srm::SrmException);

                //
                // Is the user authrised to access this request?
                //
                bool permission(std::string requestorUname,
                        std::string requestorDN,
                        std::string ownerUname, 
                        std::string ownerDN) throw (srm::SrmException);

                //
                // Validate requestor has permission to access the space token
                //
                bool permission(srm::StorageArea *area, std::string vo) throw (srm::SrmException);

                //
                // Validate incoming storage type is valid
                //
                bool storageType(int type) throw (srm::SrmException);

                // 
                // Validate the space token is valid.  Essentially does a null pointer check and
                // by default checks whether the space is currently allocated or not.  If not, then
                // an error is thrown
                //
                bool storageArea(srm::StorageArea* area, bool checkAvailable=true) throw (srm::SrmException);

                //
                // Validate the requested protocol is supported
                //
                bool protocol(std::vector<std::string> requestedProtocols, std::string* agreedProtocol) throw (srm::SrmException);

                //
                // Check whether an array of space tokens is provided
                //
                bool spaceTokenList(std::vector<std::string> arrayOfSpaceTokens) throw (srm::SrmException);

                //
                // Validate an array of file requests.  Checks the array is not null and is not empty
                //
                bool arrayOfGetFileRequests(srm__ArrayOfTGetFileRequest* array) throw (srm::SrmException);

                //
                // Validate an array of file requests.  Checks the array is not null and is not empty
                //
                bool arrayOfPutFileRequests(srm__ArrayOfTPutFileRequest* array) throw (srm::SrmException);

                //
                // Validate that a service class is associated with a storage area.  If not this is an
                // internal error and implies to StorageArea table is misconfigured.  It does not validate
                // that the svcClass exists on the castor instance
                //
                bool svcClass(srm::StorageArea* area) throw (srm::SrmException);

                //
                // Validate an incoming SURL is parseable and return a pointer to the SURL object.
                //
                castor::srm::SURL* surl(std::string surl) throw (srm::SrmException);

                //
                // Validate lifetimes. Checks the value is not negative.  By default, null times
                // are not permitted, but this can be overridden by the client.
                //
                bool lifetime(int* lifetime, bool nullAllowed=false) throw (srm::SrmException);

                //
                // Validate any pointer is not null.
                //
                bool notNull(void *ptr, std::string param) throw (srm::SrmException);

                //
                // Validate if a pointer is null.  Sometimes supplying a value is not supported.
                //
                bool null(void *ptr, std::string param) throw (srm::SrmException);

                //
                // Generic function for validation of std::string std::vector
                //
                bool validateVectorOfStrings(std::vector<std::string> v) throw (srm::SrmException);

            private:

                void generateException(enum srm__TStatusCode errorCode, std::string message) throw (srm::SrmException);
        };
    }
} }  // end of namespace castor/srm

#endif
