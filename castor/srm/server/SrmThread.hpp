/******************************************************************************
 *                      SrmThread.hpp
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 *
 *
 * @author Giuseppe Lo Presti
 *****************************************************************************/

#ifndef SRM_SERVER_SRMTHREAD_HPP
#define SRM_SERVER_SRMTHREAD_HPP 1

#include "castor/server/IThread.hpp"
#include "castor/BaseObject.hpp"
#include "castor/exception/Exception.hpp"

namespace castor {
 namespace srm {

  namespace server {

    /**
     * The SRM server main thread.
     */
    class SrmThread : public castor::server::IThread,
                      public castor::BaseObject {

    public:

      /**
       * Method called once per request, where all the code resides
       * @param param the socket obtained from the calling thread pool
       */
      virtual void run(void *param);
      
      virtual void init() {};

      virtual void stop() {};

    }; // class SrmThread

  } // end of namespace server

} } /* end of namespace castor/srm */

#endif // SRM_SERVER_SRMTHREAD_HPP
