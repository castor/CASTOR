#include <cgsi_plugin.h>
#include <Cns_api.h>
#include <castor/log/log.hpp>

#include "castor/srm/SURL.hpp"
#include "castor/srm/server/SrmUtils.hpp"
#include "castor/srm/server/SrmLogger.hpp"
#include "castor/srm/SrmConstants.hpp"
#include "castor/srm/server/ResponseWriter.hpp"
#include "castor/srm/server/RequestValidator.hpp"
#include "soapH.h"

int srm__srmRmdir(
        struct soap *soap,
        srm__srmRmdirRequest*               srmRmdirRequest,
        srm__srmRmdirResponse_      &rep) //< response parameter
{
    castor::srm::server::SrmLogger *myLogger = new castor::srm::server::SrmLogger();
    myLogger->LOGENTRY(soap);

    castor::srm::server::SrmUtils *utils = new castor::srm::server::SrmUtils();

    srm__srmRmdirRequest *req;
    srm__srmRmdirResponse *resp;
    req = srmRmdirRequest;

    utils->map_user(soap, NULL, NULL);
    castor::srm::server::ResponseWriter myWriter;
    castor::srm::server::RequestValidator      validate;

    //Prepare the reply
    try {
        resp = soap_new_srm__srmRmdirResponse(soap, -1);
        resp->returnStatus = soap_new_srm__TReturnStatus(soap, -1);
        resp->returnStatus->explanation = soap_new_std__string(soap, -1);

        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
        resp->returnStatus->explanation->assign("");
    }
    catch (...) {
        std::list<castor::log::Param> params = {
            castor::log::Param("REQID", myLogger->getId()),
            castor::log::Param("Message", "Out of memory")
        };
        castor::log::write(LOG_ALERT, "Exception caught", params);
        utils->unmap_user();
        delete utils;
        myLogger->LOGEXIT(srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR, "Out of memory");
        delete myLogger;
        myWriter.write(rep);
        return SOAP_EOM;
    }

    //Put resp inside rep struct container
    rep.srmRmdirResponse = resp;

    //Get castor filename
    std::string dirpath;
    try {
        castor::srm::SURL* surl = validate.surl(req->SURL);
        dirpath = surl->getFileName();
        delete surl;
    } catch(castor::exception::Exception &e) {
        resp->returnStatus->statusCode = (srm__TStatusCode)e.code();
        resp->returnStatus->explanation->assign(e.getMessage().str());
        utils->unmap_user();
        myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
        myWriter.write(rep);
        delete utils;
        delete myLogger;
        return SOAP_OK;
    }

    //Call the Castor function now to do the rmdir & return success/error

    //Mark recursive as not-supported for now (TPS 14.10.05)
    if (req->recursive != NULL && *(req->recursive) == true){
        resp->returnStatus->explanation->assign("Recursion not supported");
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCORENOT_USCORESUPPORTED;
        utils->unmap_user();
        myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
        myWriter.write(rep);
        delete utils;
        delete myLogger;
        return SOAP_OK;
    }

    Cns_filestat buf;
    Cns_fileid   nsId;
    nsId.fileid=0;
    strncpy(nsId.server, castor::srm::SrmConstants::getInstance()->nsHost().c_str(), sizeof(nsId.server)-1);
    if ( Cns_stat(dirpath.c_str(), &buf) == 0 )
        nsId.fileid = buf.fileid;

    myLogger->logCnsCall("Cns_rmdir", dirpath, &nsId, LOG_INFO);
    int rc = Cns_rmdir(dirpath.c_str());
    myLogger->logCnsReturn("Cns_rmdir", rc, &nsId);

    if (rc != 0) {
        switch ( serrno ) {
            case ENOENT:
            case EFAULT:
            case ENOTDIR:
            case EINVAL:
            case ENAMETOOLONG:
                resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREPATH;
                resp->returnStatus->explanation->assign(sstrerror(serrno));
                break;
            case EEXIST:
                resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCORENON_USCOREEMPTY_USCOREDIRECTORY;
                resp->returnStatus->explanation->assign(sstrerror(serrno));
                break;
            case EACCES:
                resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREAUTHORIZATION_USCOREFAILURE;
                resp->returnStatus->explanation->assign(sstrerror(serrno));
                break;
            case SENOSHOST:
            case SENOSSERV:
            case SECOMERR:
            case ENSNACT:
                resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
                resp->returnStatus->explanation->assign(sstrerror(serrno));
                break;
            default:
                resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
                resp->returnStatus->explanation->assign("Unexpected error status");
                break;
        }
        utils->unmap_user();
        delete utils;
        myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
        delete myLogger;
        myWriter.write(rep);
        return SOAP_OK;
    }
    utils->unmap_user();
    myWriter.write(rep);
    delete utils;
    myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
    delete myLogger;
    return SOAP_OK;

}


