#include <sys/types.h>
#include <sys/stat.h>
#include <pwd.h>
#include <dirent.h>
#include <sstream>
#include <algorithm>

#include <Cns_api.h>
#include <Cpwd.h>
#include <Cgrp.h>
#include <castor/log/log.hpp>
#include <stager_client_api.h>
#include <stager_client_api_common.hpp>
#include <vmgr_api.h>

#include <castor/BaseObject.hpp>
#include <castor/IService.hpp>
#include <castor/Services.hpp>
#include <castor/exception/Exception.hpp>

#include "soapH.h"

#include "castor/srm/Constants.hpp"
#include "castor/srm/ISrmSvc.hpp"
#include "castor/srm/SURL.hpp"
#include "castor/srm/SrmConstants.hpp"
#include "castor/srm/server/SrmUtils.hpp"
#include "castor/srm/server/SrmLogger.hpp"
#include "castor/srm/server/RequestValidator.hpp"
#include "castor/srm/server/StageIncrementer.hpp"

//#include "SrmFileTypes.hpp"
#include "ResponseWriter.hpp"

// Prototypes
enum srm__TStatusCode do__Ls ( std::string filename, 
        std::string* user, 
        int maxRcurse,
        int offset,
        int count,
        std::vector<srm__TMetaDataPathDetail*> *response, 
        bool detailedListing,
        bool verboseListing,
        bool recurse,
        int *currRecurse,
        unsigned *fileCount,
        std::map<std::string, std::string>& scToSpace,
        castor::srm::server::SrmLogger *logger,
        struct stage_options opts,
        struct soap *soap);

enum srm__TPermissionMode getOwnerPermission(mode_t mode);
enum srm__TPermissionMode getGroupPermission(mode_t mode);
enum srm__TPermissionMode getOtherPermission(mode_t mode);


int srm__srmLs(
        struct soap *soap,
        srm__srmLsRequest*                  srmLsRequest,
        srm__srmLsResponse_         &response) //< response parameter
{
    castor::srm::server::SrmLogger *myLogger = new castor::srm::server::SrmLogger();
    myLogger->LOGENTRY(soap);


    srm__srmLsResponse* resp;
    try {
        resp = soap_new_srm__srmLsResponse(soap, -1);
        resp->returnStatus = soap_new_srm__TReturnStatus(soap, -1);
        resp->returnStatus->explanation = soap_new_std__string(soap, -1);
        resp->requestToken = NULL;
        resp->details = soap_new_srm__ArrayOfTMetaDataPathDetail(soap, -1);

        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
        resp->returnStatus->explanation->assign("");
    }
    catch(...) {
        std::list<castor::log::Param> params = {
            castor::log::Param("REQID", myLogger->getId()),
            castor::log::Param("Message", "Out of memory")
        };
        castor::log::write(LOG_ALERT, "Exception caught", params);
        myLogger->LOGEXIT(srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR, "Out of memory");
        delete myLogger;
        return SOAP_EOM;
    }
    response.srmLsResponse = resp;

    castor::srm::server::ResponseWriter myWriter;
    castor::srm::server::RequestValidator      validate;

    // Validate request
    std::string stagerHost;
    castor::srm::server::SrmUtils *utils = new castor::srm::server::SrmUtils();
    try {
        validate.notNull(srmLsRequest->arrayOfSURLs, "arrayOfSURLs");
        validate.arrayOfSurls(srmLsRequest->arrayOfSURLs->urlArray);
        validate.null(srmLsRequest->fileStorageType, "fileStorageType");
        validate.notNull(utils, "");
        stagerHost = utils->getStagerHost(soap);
    } catch (castor::exception::Exception &e) {
        castor::srm::SrmException* se = dynamic_cast<castor::srm::SrmException*>(&e);
        if(se) {
            resp->returnStatus->statusCode = (srm__TStatusCode)se->code();
        } else {
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        }
        resp->returnStatus->explanation->assign(e.getMessage().str());
        myWriter.write(response);
        myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
        if (utils) delete utils;
        delete myLogger;
        return SOAP_OK;
    }

    // Idiot checking - no negatives, and numLevels/allRecurse mutually exclusive
    int offset = 0;
    int count = 0;
    int nRecurse = 1;  // Default numbr of recursions
    bool fullRecurse = false;
    if ( srmLsRequest->offset ) {
        offset = *(srmLsRequest->offset);
    }
    if ( srmLsRequest->count ) {
        count = *(srmLsRequest->count);
    }
    if ( srmLsRequest->numOfLevels ) {
        nRecurse = *(srmLsRequest->numOfLevels);
    }
    if ( srmLsRequest->allLevelRecursive ) {
        fullRecurse = *(srmLsRequest->allLevelRecursive);
    }

    // Check valid values for offset, count and recurse
    castor::srm::SrmConstants *srmConstants = castor::srm::SrmConstants::getInstance();
    if ( offset < 0 || nRecurse < 0 || count < 0 ) {
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST;
        resp->returnStatus->explanation->assign("Negative values for count, offset and numOfLevels not allowed");
    }
    if (fullRecurse && nRecurse > 0 ) {
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST;
        resp->returnStatus->explanation->assign("Full recursion and specifying numOfLevels are exclusive");
    }
    if ( (count-offset)>0 && (unsigned)(count - offset) > srmConstants->maxReturns() ) {
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCORETOO_USCOREMANY_USCORERESULTS;
        std::stringstream ss;
        ss << "(count - offset) must be less than " << srmConstants->maxReturns() << std::ends;
        resp->returnStatus->explanation->assign(ss.str());
    }
    if ( (unsigned)(offset + count) > srmConstants->maxCount() ) {
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCORETOO_USCOREMANY_USCORERESULTS;
        std::stringstream ss;
        ss << "(count + offset) must be less than " << srmConstants->maxCount() << std::ends;
        resp->returnStatus->explanation->assign(ss.str());
    }
    if ( resp->returnStatus->statusCode != srm__TStatusCode__SRM_USCORESUCCESS ) {
        std::list<castor::log::Param> params = {
            castor::log::Param("REQID", myLogger->getId()),
            castor::log::Param("Offset", offset),
            castor::log::Param("Count", count),
            castor::log::Param("NumOfLevels", nRecurse),
            castor::log::Param("Message", resp->returnStatus->explanation->c_str())
        };
        castor::log::write(LOG_USER_ERROR, "Invalid Request", params);
        myWriter.write(response);
        myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
        delete utils;
        delete myLogger;
        return SOAP_OK;
    }

    if ( fullRecurse) {
        nRecurse = 1024; //Hard code a maximum level of recurrsion - should never see this
    }

    bool fullDetailedList = false;
    if ( srmLsRequest->fullDetailedList ) fullDetailedList = *(srmLsRequest->fullDetailedList);
    
    // Populate the map svcClass -> space token description
    castor::IService *svc = castor::BaseObject::services()->service("DbSrmSvc", castor::SVC_DBSRMSVC);
    castor::srm::ISrmSvc* srmSvc = dynamic_cast<castor::srm::ISrmSvc*>(svc);
    if ( srmSvc == 0 ) {
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        resp->returnStatus->explanation->assign("Unable to establish database service");
        myWriter.write(response);
        utils->unmap_user();
        delete utils;
        myLogger->LOGEXIT(resp->returnStatus->statusCode,  resp->returnStatus->explanation->c_str());
        delete myLogger;
        return SOAP_OK;
    }
    std::map<std::string, std::string> scToSpace = srmSvc->getSpaceTokensList(utils->getVoName(soap));

    std::vector<xsd__anyURI> surls = srmLsRequest->arrayOfSURLs->urlArray;
    std::vector<srm__TMetaDataPathDetail*> responseList;

    struct stage_options opts;
    opts.stage_host    = strdup (stagerHost.c_str());
    opts.stage_port    = STAGERPORT;
    opts.service_class = NULL;

    bool recurse = ( fullRecurse || nRecurse > 0 );
    enum srm__TStatusCode myStatus = srm__TStatusCode__SRM_USCOREFAILURE;
    try {
        // Need to increment stager thread count
        castor::srm::server::StageIncrementer inc;
        inc.increment();
        
        unsigned fileCount = 0;
        // Map to the correct user to stop permission problems.
        uid_t uid;
        gid_t gid;
        utils->map_user(soap, &uid, &gid);
        for ( unsigned i=0; i<surls.size(); i++ ) {
            int currRecurse = 0;
            std::string cfn;
            try {
                castor::srm::SURL* surl = validate.surl(surls[i]);
                cfn = surl->getFileName();
                delete surl;
            } catch (castor::exception::Exception e) {
                // Some kind of error parsing the SURL - just go onto the next one
                std::list<castor::log::Param> surlP = {
                    castor::log::Param("REQID", myLogger->getId()),
                    castor::log::Param("Message", "Unable to parse SURL"),
                    castor::log::Param("SURL", surls[i]),
                    castor::log::Param("Details", e.getMessage().str() )
                };
                castor::log::write(LOG_USER_ERROR, "Exception caught", surlP);
                continue;
            }

            myStatus = do__Ls( cfn, 
                    srmLsRequest->authorizationID,
                    nRecurse,
                    offset,
                    count,
                    &resp->details->pathDetailArray,
                    fullDetailedList,
                    true,
                    recurse,
                    &currRecurse,
                    &fileCount,
                    scToSpace,
                    myLogger, 
                    opts,
                    soap);
            if ( myStatus == srm__TStatusCode__SRM_USCORETOO_USCOREMANY_USCORERESULTS ) break;
        }
    }
    catch (castor::exception::Exception &e) {
        free (opts.stage_host);
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        resp->returnStatus->explanation->assign(e.getMessage().str());
        myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
        utils->unmap_user();
        delete utils;
        delete myLogger;
        return SOAP_OK;
    }
    free (opts.stage_host);
    if ( resp->details->pathDetailArray.size() == 0 ) {
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
        resp->returnStatus->explanation->assign("Failed for all paths");
    }
    else {
        bool foundGood = false;
        for (unsigned count=0; count < resp->details->pathDetailArray.size(); count++ ) {
            if ( resp->details->pathDetailArray[count]->status->statusCode == srm__TStatusCode__SRM_USCORESUCCESS ||
                    resp->details->pathDetailArray[count]->status->statusCode == srm__TStatusCode__SRM_USCOREFILE_USCOREBUSY ||
                    resp->details->pathDetailArray[count]->status->statusCode == srm__TStatusCode__SRM_USCOREDONE ) {
                foundGood = true;
                break;
            }
        }
        if ( !foundGood ) {
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
            resp->returnStatus->explanation->assign("Failed for all paths");
        }
        else {
            resp->returnStatus->statusCode = myStatus;
            resp->returnStatus->explanation->assign("");
        }
    }
    myWriter.write(response);
    myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
    utils->unmap_user();
    delete utils;
    delete myLogger;
    return SOAP_OK;
}

enum srm__TStatusCode  do__Ls( std::string name, 
        std::string  *user,
        int maxRecurse,
        int offset,
        int count,
        std::vector<srm__TMetaDataPathDetail*> *responses,
        bool detailed, 
        bool verbose,
        bool recurse, 
        int *currRecurse,
        unsigned *fileCount,
        std::map<std::string, std::string>& scToSpace,
        castor::srm::server::SrmLogger *myLogger,
        struct stage_options opts,
        struct soap *soap) {

    if ( count > 0 && (*fileCount > unsigned (offset + count)) ) {
        return srm__TStatusCode__SRM_USCORESUCCESS;
    }
    else if ( (signed)(*fileCount - offset) > (signed)castor::srm::SrmConstants::getInstance()->maxReturns() ) {
        return srm__TStatusCode__SRM_USCORETOO_USCOREMANY_USCORERESULTS;
    }
    std::string cfn = name;

    // Allocate space for details and assign some default values to keep SOAP happy
    srm__TMetaDataPathDetail *details = 0;
    bool takingData  = false;
    if ( *fileCount >= (unsigned)offset ) {
        takingData = true;
        details = soap_new_srm__TMetaDataPathDetail(soap, -1);
        details->path.assign(cfn);
        details->status               = soap_new_srm__TReturnStatus(soap, -1);
        details->status->explanation  = soap_new_std__string(soap, -1);
        details->status->explanation->assign("");
        details->status->statusCode   = srm__TStatusCode__SRM_USCORESUCCESS;
        details->size                 = NULL;
        details->createdAtTime        = NULL;
        details->lastModificationTime = NULL;
        details->fileStorageType      = NULL;
        details->retentionPolicyInfo  = NULL;
        details->fileLocality         = NULL;
        details->arrayOfSpaceTokens   = NULL;
        details->type                 = NULL;
        details->lifetimeAssigned     = NULL;
        details->lifetimeLeft         = NULL;
        details->ownerPermission      = NULL;
        details->groupPermission      = NULL;
        details->otherPermission      = NULL;
        details->checkSumType         = NULL;
        details->checkSumValue        = NULL;
        // Initialise subpaths to NULL - needed for recursive directory listing
        details->arrayOfSubPaths      = NULL;
    }
    (*fileCount)++;
    if ( *fileCount > castor::srm::SrmConstants::getInstance()->maxCount() ) {
        return srm__TStatusCode__SRM_USCORETOO_USCOREMANY_USCORERESULTS;
    }

    int pathElements=0;
    for ( unsigned letter=0; letter < cfn.length(); letter++ ) {
        if (cfn[letter] == '/' && cfn[letter+1] != '/' ) pathElements++;
        if (pathElements > 3) break;
    }
    if ( pathElements < 3 ) {
        // Even if we are not taking data, trying to do an Ls at this level is not supported.
        if (!details) {
            details = soap_new_srm__TMetaDataPathDetail(soap, -1);
            details->path.assign(cfn);
            details->status               = soap_new_srm__TReturnStatus(soap, -1);
            details->status->explanation  = soap_new_std__string(soap, -1);
            details->status->explanation->assign("");
            details->size                 = NULL;
            details->createdAtTime        = NULL;
            details->lastModificationTime = NULL;
            details->fileStorageType      = NULL;
            details->retentionPolicyInfo  = NULL;
            details->fileLocality         = NULL;
            details->arrayOfSpaceTokens   = NULL;
            details->type                 = NULL;
            details->lifetimeAssigned     = NULL;
            details->lifetimeLeft         = NULL;
            details->ownerPermission      = NULL;
            details->groupPermission      = NULL;
            details->otherPermission      = NULL;
            details->checkSumType         = NULL;
            details->checkSumValue        = NULL;
            details->arrayOfSubPaths      = NULL;
        }
        details->status->statusCode = srm__TStatusCode__SRM_USCORENOT_USCORESUPPORTED;
        details->status->explanation->assign("User can not access directory at this level");
        responses->push_back(details);
        return srm__TStatusCode__SRM_USCORENOT_USCORESUPPORTED;
    }

    if (details) details->type = (enum srm__TFileType*)soap_malloc(soap, sizeof(enum srm__TFileType));
    struct Cns_filestatcs buf;
    Cns_fileid nsId;
    nsId.fileid = 0;
    strncpy(nsId.server, castor::srm::SrmConstants::getInstance()->nsHost().c_str(), sizeof(nsId.server)-1);
    int statRc = 0;
    myLogger->logCnsCall("Cns_statcs", cfn, 0, LOG_DEBUG);
    // We use the statcs version to also retrieve checksum data
    if ( (statRc = Cns_statcs(cfn.c_str(), &buf)) == 0 ) {
        nsId.fileid = buf.fileid;
        myLogger->logCnsReturn("Cns_statcs", statRc, &nsId);
        if ( (buf.filemode & S_IFDIR) ) {
            if ( takingData ) {
                myLogger->LOGFILEREQ(&nsId, cfn);
                // For the purposes of RFE 34735, we make the type a return parameter for all returns
                *(details->type) = srm__TFileType__DIRECTORY;
                if ( detailed ) {
                    // Need to fill in user permission
                    details->size                 = (ULONG64*)soap_malloc(soap, sizeof(ULONG64));
                    details->createdAtTime        = (time_t*)soap_malloc(soap, sizeof(time_t));
                    details->lastModificationTime = (time_t*)soap_malloc(soap, sizeof(time_t));
                    details->fileLocality         = (enum srm__TFileLocality*)soap_malloc(soap, sizeof(enum srm__TFileLocality));
                    details->lifetimeLeft         = (int*)soap_malloc(soap, sizeof(int));
                    details->ownerPermission      = soap_new_srm__TUserPermission(soap, -1);
                    details->groupPermission      = soap_new_srm__TGroupPermission(soap, -1);
                    details->otherPermission      = (enum srm__TPermissionMode*)soap_malloc(soap, sizeof(enum srm__TPermissionMode));

                    *(details->size)                 = buf.filesize;
                    *(details->createdAtTime)        = buf.ctime;
                    *(details->lastModificationTime) = buf.mtime;
                    *(details->fileLocality)         = srm__TFileLocality__ONLINE_USCOREAND_USCORENEARLINE;
                    *(details->type)                 = srm__TFileType__DIRECTORY;
                    *(details->lifetimeLeft)         = -1;

                    struct passwd *pw = Cgetpwuid(buf.uid);
                    if ( pw ) {
                        details->ownerPermission->userID.assign(pw->pw_name);
                    }
                    else {
                        std::stringstream ss;
                        ss << buf.uid;
                        details->ownerPermission->userID.assign(ss.str());
                    }
                    details->ownerPermission->mode = getOwnerPermission(buf.filemode);
                    struct group  *gr = Cgetgrgid(buf.gid);
                    if ( gr ) {
                        details->groupPermission->groupID.assign(gr->gr_name);
                    }
                    else {
                        std::stringstream ss;
                        ss << buf.gid;
                        details->groupPermission->groupID.assign(ss.str());
                    }
                    details->groupPermission->mode = getGroupPermission(buf.filemode);
                    *(details->otherPermission) = getOtherPermission(buf.filemode);
                }
                responses->push_back(details);
            }
            if ( recurse && ((*currRecurse)++ < maxRecurse) ) {
                std::vector<srm__TMetaDataPathDetail*> *toFill;
                if ( takingData ) {
                    details->arrayOfSubPaths = soap_new_srm__ArrayOfTMetaDataPathDetail(soap, -1);
                    toFill = &details->arrayOfSubPaths->pathDetailArray;
                }
                else {
                    toFill = responses;
                }
                myLogger->logCnsCall("Cns_opendir", cfn, &nsId, LOG_INFO);
                Cns_DIR *thisDir = Cns_opendir(cfn.c_str());
                myLogger->logCnsReturn("Cns_opendir", serrno, &nsId);
                struct Cns_direnstat *entry;
                while ( (entry = Cns_readdirx(thisDir)) != NULL ) {
                    std::string fName = cfn + "/" + entry->d_name;
                    enum srm__TStatusCode myRC = do__Ls(fName, user, maxRecurse,
                            offset, count, toFill, detailed, false, recurse, 
                            currRecurse, fileCount, scToSpace, myLogger, opts, soap);
                    if ( myRC == srm__TStatusCode__SRM_USCORETOO_USCOREMANY_USCORERESULTS) {
                        myLogger->logCnsCall("Cns_closedir", cfn, &nsId);
                        Cns_closedir (thisDir);
                        myLogger->logCnsReturn("Cns_closedir", serrno, &nsId);
                        return myRC;
                    }
                    //if ( *fileCount >= (unsigned)(offset+count) ) break;
                }
                myLogger->logCnsCall("Cns_closedir", cfn, &nsId, LOG_INFO);
                Cns_closedir (thisDir);
                myLogger->logCnsReturn("Cns_closedir", serrno, &nsId);
                (*currRecurse)--;
            }
        }
        else {
            if ( takingData ) {
                myLogger->LOGFILEREQ(&nsId, cfn);
                details->status->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
                details->status->explanation->assign("");
                details->size = (ULONG64*)soap_malloc(soap, sizeof(ULONG64));
                *(details->size) = buf.filesize;
                *(details->type) = srm__TFileType__FILE_;
                if ( detailed ) {
                    details->createdAtTime        = (time_t*)soap_malloc(soap, sizeof(time_t));
                    details->lastModificationTime = (time_t*)soap_malloc(soap, sizeof(time_t));
                    details->fileLocality         = NULL;
                    details->lifetimeLeft         = (int*)soap_malloc(soap, sizeof(int));
                    details->ownerPermission      = soap_new_srm__TUserPermission(soap, -1);
                    details->groupPermission      = soap_new_srm__TGroupPermission(soap, -1);
                    details->otherPermission      = (enum srm__TPermissionMode*)soap_malloc(soap, sizeof(enum srm__TPermissionMode));
                    *(details->createdAtTime)        = buf.ctime;
                    *(details->lastModificationTime) = buf.mtime;
                    *(details->lifetimeLeft)         = -1;

                    struct passwd *pw = Cgetpwuid(buf.uid);
                    if ( pw ) {
                        details->ownerPermission->userID.assign(pw->pw_name);
                    }
                    else {
                        std::stringstream ss;
                        ss << buf.uid;
                        details->ownerPermission->userID.assign(ss.str());
                    }
                    details->ownerPermission->mode = getOwnerPermission(buf.filemode);
                    struct group  *gr = Cgetgrgid(buf.gid);
                    if ( gr ) {
                        details->groupPermission->groupID.assign(gr->gr_name);
                    }
                    else {
                        std::stringstream ss;
                        ss << buf.gid;
                        details->groupPermission->groupID.assign(ss.str());
                    }
                    details->groupPermission->mode = getGroupPermission(buf.filemode);
                    *(details->otherPermission) = getOtherPermission(buf.filemode);

                    // For a file, getting the file locality is a little tricky.  We need to do
                    // a file query to see if the file is online, and we use the status of the
                    // Cns_stat buffer to determine is the file is migrated or not.
                    if (verbose) {
                        details->fileLocality = (enum srm__TFileLocality*)soap_malloc(soap, sizeof(enum srm__TFileLocality));

                        struct stage_query_req stageRequest;
                        stageRequest.type = ::BY_FILENAME_ALLSC;
                        std::string filename = cfn;
                        stageRequest.param = (char*)filename.c_str();

                        struct stage_filequery_resp *stageResponse = 0;
                        int nResp, sfqrc;
                        myLogger->logStagerCall("stage_filequery", opts, &nsId);
                        sfqrc = stage_filequery(&stageRequest, 1, &stageResponse, &nResp, &opts);
                        myLogger->logStagerReturn("stage_filequery", sfqrc, "", &nsId);

                        // Initialize as if nothing was found on disk
                        if ( buf.status == 'm' ) {
                            *(details->fileLocality) = srm__TFileLocality__NEARLINE;
                        }
                        else {
                            *(details->fileLocality) = srm__TFileLocality__UNAVAILABLE;
                        }
                        if ( sfqrc == 0 && stageResponse[0].errorCode == 0 ) {
                            // No error, decode the status and eventually fill the space tokens
                            details->arrayOfSpaceTokens = soap_new_srm__ArrayOfString(soap, -1);
                            for(int i = 0; i < nResp; i++) {
                                switch ( stageResponse[i].status ) {
                                    case ::FILE_STAGEOUT:
                                        // Awaiting putDone. By definition this is the only available copy and it can't be accessed
                                        details->status->statusCode = srm__TStatusCode__SRM_USCOREFILE_USCOREBUSY;
                                        *(details->fileLocality) = srm__TFileLocality__UNAVAILABLE;
                                        break;
                                    case ::FILE_STAGED:
                                    case ::FILE_CANBEMIGR:
                                        // On disk - check migration status to see if also on tape
                                        if ( buf.status == 'm' ) {
                                            *(details->fileLocality) = srm__TFileLocality__ONLINE_USCOREAND_USCORENEARLINE;
                                        }
                                        else {
                                            *(details->fileLocality) = srm__TFileLocality__ONLINE;
                                        }
                                        // and fill space token if the service class is exposed as an SRM space
                                        if(scToSpace.find(stageResponse[i].poolname) == scToSpace.end()) {
                                            // "" means 'the file is ONLINE somewhere but a bringOnline (with disk-to-disk copy)
                                            // is needed prior to getting a TURL from a valid SRM space'
                                            std::vector<std::string>::iterator it = find(
                                              details->arrayOfSpaceTokens->stringArray.begin(),
                                              details->arrayOfSpaceTokens->stringArray.end(), "");
                                            if(it == details->arrayOfSpaceTokens->stringArray.end()) {
                                                details->arrayOfSpaceTokens->stringArray.push_back("");
                                            }
                                        }
                                        else {
                                            details->arrayOfSpaceTokens->stringArray.push_back(scToSpace[stageResponse[i].poolname]);
                                        }
                                        break;
                                    case ::FILE_STAGEIN:
                                    case ::FILE_STAGEABLE:
                                        // File is either being staged (disk-to-disk copied or recalled from tape)
                                        // or online on a draining hardware (thus needing a disk-to-disk copy before being accessed).
                                        // In either case, we say it's NEARLINE unless another status has been set
                                        // (e.g. another copy is already ONLINE).
                                        if (*(details->fileLocality) == srm__TFileLocality__UNAVAILABLE ) {
                                            *(details->fileLocality) = srm__TFileLocality__NEARLINE;
                                        }
                                        break;
                                    default:
                                        // This copy of the file is not in a good state, ignore it and go to next one if present
                                        break;
                                }
                            }  // for
                            if(details->arrayOfSpaceTokens->stringArray.size() == 1 && details->arrayOfSpaceTokens->stringArray[0] == "") {
                                // only copy(ies) from non-SRM space tokens available, thus effectively NEARLINE
                                *(details->fileLocality) = srm__TFileLocality__NEARLINE;
                            }
                            if(details->arrayOfSpaceTokens->stringArray.size() == 0) {
                                // No ONLINE copy found, drop the array of space tokens
                                soap_delete(soap, details->arrayOfSpaceTokens);
                                details->arrayOfSpaceTokens = NULL;
                            }
                        }                        
                        else {
                            // No copy found or the stager returned an error. First deal with auth errors
                            if (stageResponse && stageResponse[0].errorCode == EACCES) {
                              details->status->statusCode = srm__TStatusCode__SRM_USCOREAUTHORIZATION_USCOREFAILURE;
                              details->status->explanation->assign(sstrerror(EACCES));
                              soap_delete(soap, details->fileLocality);
                              details->fileLocality = NULL;
                              responses->push_back(details);
                              return srm__TStatusCode__SRM_USCORESUCCESS;
                            }
                        }
                        if(stageResponse) {
                            free_filequery_resp(stageResponse, nResp);
                        }
                        if ( buf.filesize == 0 )
                            *(details->fileLocality) = srm__TFileLocality__NONE;
                        
                        // Checksum information
                        details->checkSumType  = soap_new_std__string(soap, -1);
                        details->checkSumValue = soap_new_std__string(soap, -1);
                        if ( strcmp(buf.csumtype, "AD") == 0 ) {
                            details->checkSumType->assign("ADLER32");
                        }
                        else {
                            details->checkSumType->assign(buf.csumtype);
                            transform( details->checkSumType->begin(), details->checkSumType->end(), details->checkSumType->begin(), (int(*)(int))toupper);
                        }
                        std::string myValue = buf.csumvalue;
                        if(myValue.length() > 0 && myValue.length() < 8) {
                          // pad value to 8 digits
                          myValue = std::string(8-myValue.length(), '0') + myValue;
                        }
                        details->checkSumValue->assign(myValue);
                        
                        if ( buf.status == 'm' ) {
                            // For NEARLINE files on tape, do an extra call to VMGR to check the tape
                            struct Cns_segattrs *segAttrs;
                            int nSegments;
                            int gsarc = 0;
                            myLogger->logCnsCall("Cns_getsegattrs", cfn, &nsId, LOG_INFO);
                            gsarc = Cns_getsegattrs(const_cast<char*>(cfn.c_str()), NULL, &nSegments, &segAttrs);
                            myLogger->logCnsReturn("Cns_getsegattrs", gsarc, &nsId);
                            if ( gsarc == 0 ) {
                                if ( myValue.length() == 0 ) {
                                    // try to use the checksum from the tape if we didn't have one on the file
                                    details->checkSumType->assign(segAttrs->checksum_name);
                                    transform( details->checkSumType->begin(), details->checkSumType->end(), details->checkSumType->begin(), (int(*)(int))toupper);

                                    std::stringstream ss;
                                    ss << std::hex << segAttrs->checksum;
                                    myValue = ss.str();
                                    if(myValue.length() < 8) {
                                      myValue = std::string(8-myValue.length(), '0') + myValue;
                                    }
                                    details->checkSumValue->assign(myValue);
                                }
                                // Only one segment is supported here, multi-segmented files are gone since long now...
                                if (nSegments == 1) {
                                    struct vmgr_tape_info_byte_u64 tapeInfo;
                                    if ( vmgr_querytape_byte_u64( segAttrs[0].vid, segAttrs[0].side, &tapeInfo, NULL) == 0 ) {
                                        if ( ((tapeInfo.status & ARCHIVED) == ARCHIVED) ||
                                             ((tapeInfo.status & DISABLED) == DISABLED) ||
                                             ((tapeInfo.status & EXPORTED) == EXPORTED) ) {
                                            // Tape copy currently not available, remove the NEARLINE locality
                                            *(details->fileLocality) = *(details->fileLocality) == srm__TFileLocality__NEARLINE 
                                                ? srm__TFileLocality__UNAVAILABLE : srm__TFileLocality__ONLINE;
                                        }
                                    }
                                    else {
                                        // Cant determine state of this segment - log it and continue
                                        // "Exception caught : ignored"
                                        std::list<castor::log::Param> x = {
                                            castor::log::Param("NSFILEID", nsId.fileid),
                                            castor::log::Param("REQID", myLogger->getId()),
                                            castor::log::Param ("Call", "vmgr_querytape"),
                                            castor::log::Param ("ErrorMessage", sstrerror(serrno) ),
                                            castor::log::Param ("VID", segAttrs[0].vid)
                                        };
                                        castor::log::write(LOG_WARNING, "Exception caught : ignored", x);
                                    }
                                }
                                free (segAttrs);
                            }
                        } // End if nearline
                    } // End if verbose
                }  // End if detailed
                responses->push_back(details);
            }
        }
    }
    else {
        if ( takingData ) {
            myLogger->logCnsReturn("Cns_statcs", statRc, &nsId);
            details->path.assign(cfn);
            *(details->type) = srm__TFileType__FILE_;  // not strictly correct but we don't have any other suitable value
            switch (serrno) {
                case ENOENT:
                case EFAULT:
                case ENOTDIR:
                case ENAMETOOLONG:
                    details->status->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREPATH;
                    details->status->explanation->assign(sstrerror(serrno));
                    break;
                case EACCES:
                    details->status->statusCode = srm__TStatusCode__SRM_USCOREAUTHORIZATION_USCOREFAILURE;
                    details->status->explanation->assign(sstrerror(serrno));
                    break;
                case SENOSHOST:
                case SENOSSERV:
                case SECOMERR:
                case ENSNACT:
                    details->status->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
                    details->status->explanation->assign(sstrerror(serrno));
                    break;
                default:
                    details->status->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
                    details->status->explanation->assign("Unknown error while stating file");
                    break;
            }
            responses->push_back(details);
        }
    }
    return srm__TStatusCode__SRM_USCORESUCCESS;
}

enum srm__TPermissionMode getOwnerPermission(mode_t filemode) {
    enum srm__TPermissionMode myMode;
    if ( (filemode & ( S_IRUSR | S_IWUSR | S_IXUSR )) == ( S_IRUSR | S_IWUSR | S_IXUSR ) ) {
        myMode = srm__TPermissionMode__RWX;
    }
    else if ( (filemode & ( S_IRUSR | S_IWUSR )) == ( S_IRUSR | S_IWUSR ) ) {
        myMode = srm__TPermissionMode__RW;
    }
    else if ( (filemode & ( S_IRUSR | S_IXUSR )) == ( S_IRUSR | S_IXUSR ) ) {
        myMode = srm__TPermissionMode__RX;
    }
    else if ( (filemode & ( S_IWUSR | S_IXUSR )) == ( S_IWUSR | S_IXUSR ) ) {
        myMode = srm__TPermissionMode__WX;
    }
    else if ( (filemode & S_IRUSR) == S_IRUSR ) {
        myMode = srm__TPermissionMode__R;
    }
    else if ( (filemode & S_IWUSR) == S_IWUSR ) {
        myMode = srm__TPermissionMode__W;
    }
    else if ( (filemode & S_IXUSR) == S_IXUSR ) {
        myMode = srm__TPermissionMode__X;
    }
    else {
        myMode = srm__TPermissionMode__NONE;
    }
    return myMode;
}



enum srm__TPermissionMode getGroupPermission(mode_t filemode) {
    enum srm__TPermissionMode myMode;
    if ( (filemode & ( S_IRGRP | S_IWGRP | S_IXGRP )) == ( S_IRGRP | S_IWGRP | S_IXGRP ) ) {
        myMode = srm__TPermissionMode__RWX;
    }
    else if ( (filemode & ( S_IRGRP | S_IWGRP )) == ( S_IRGRP | S_IWGRP ) ) {
        myMode = srm__TPermissionMode__RW;
    }
    else if ( (filemode & ( S_IRGRP | S_IXGRP )) == ( S_IRGRP | S_IXGRP ) ) {
        myMode = srm__TPermissionMode__RX;
    }
    else if ( (filemode & ( S_IWGRP | S_IXGRP )) == ( S_IWGRP | S_IXGRP ) ) {
        myMode = srm__TPermissionMode__WX;
    }
    else if ( (filemode & S_IRGRP) == S_IRGRP ) {
        myMode = srm__TPermissionMode__R;
    }
    else if ( (filemode & S_IWGRP) == S_IWGRP ) {
        myMode = srm__TPermissionMode__W;
    }
    else if ( (filemode & S_IXGRP) == S_IXGRP ) {
        myMode = srm__TPermissionMode__X;
    }
    else {
        myMode = srm__TPermissionMode__NONE;
    }
    return myMode;
}

enum srm__TPermissionMode getOtherPermission(mode_t filemode) {
    enum srm__TPermissionMode myMode;
    if ( (filemode & ( S_IROTH | S_IWOTH | S_IXOTH )) == ( S_IROTH | S_IWOTH | S_IXOTH ) ) {
        myMode = srm__TPermissionMode__RWX;
    }
    else if ( (filemode & ( S_IROTH | S_IWOTH )) == ( S_IROTH | S_IWOTH ) ) {
        myMode = srm__TPermissionMode__RW;
    }
    else if ( (filemode & ( S_IROTH | S_IXOTH )) == ( S_IROTH | S_IXOTH ) ) {
        myMode = srm__TPermissionMode__RX;
    }
    else if ( (filemode & ( S_IWOTH | S_IXOTH )) == ( S_IWOTH | S_IXOTH ) ) {
        myMode = srm__TPermissionMode__WX;
    }
    else if ( (filemode & S_IROTH) == S_IROTH ) {
        myMode = srm__TPermissionMode__R;
    }
    else if ( (filemode & S_IWOTH) == S_IWOTH ) {
        myMode = srm__TPermissionMode__W;
    }
    else if ( (filemode & S_IXOTH) == S_IXOTH ) {
        myMode = srm__TPermissionMode__X;
    }
    else {
        myMode = srm__TPermissionMode__NONE;
    }
    return myMode;
}
