#include <cgsi_plugin.h>

#include <castor/BaseAddress.hpp>
#include <castor/BaseObject.hpp>
#include <castor/Constants.hpp>
#include <castor/Services.hpp>
#include <castor/log/log.hpp>

#include <stager_client_api.h>
#include <stager_client_api_common.hpp>

#include <castor/exception/Exception.hpp>
#include <castor/exception/NoEntry.hpp>

#include "soapH.h"

#include "castor/srm/SURL.hpp"
#include "castor/srm/server/SrmLogger.hpp"
#include "castor/srm/Constants.hpp"
#include "castor/srm/RequestType.hpp"
#include "castor/srm/ISrmSvc.hpp"
#include "castor/srm/SrmUser.hpp"
#include "castor/srm/SrmConstants.hpp"
#include "castor/srm/StageRequest.hpp"
#include "castor/srm/SubRequest.hpp"
#include "castor/srm/SubRequestStatus.hpp"
#include "castor/srm/UserFile.hpp"
#include "castor/srm/SrmUtils.hpp"
#include "castor/srm/server/SrmLogger.hpp"
#include "castor/srm/server/ResponseWriter.hpp"
#include "castor/srm/server/SrmUtils.hpp"
#include "castor/srm/server/RequestValidator.hpp"

int srm__srmStatusOfGetRequest(
        struct soap *soap,
        srm__srmStatusOfGetRequestRequest*  srmStatusOfGetRequestRequest,
        srm__srmStatusOfGetRequestResponse_& out) //< response parameter
{
    castor::srm::server::SrmLogger *myLogger = new castor::srm::server::SrmLogger();
    myLogger->LOGENTRY(soap);

    castor::srm::server::ResponseWriter myWriter;
    castor::srm::server::SrmUtils *utils = new castor::srm::server::SrmUtils();
    utils->map_user(soap, NULL, NULL);

    srm__srmStatusOfGetRequestResponse *resp;
    try {
        resp =
            soap_new_srm__srmStatusOfGetRequestResponse(soap,-1);
        out.srmStatusOfGetRequestResponse = resp;

        resp->remainingTotalRequestTime = NULL;
        resp->returnStatus = soap_new_srm__TReturnStatus(soap, -1);
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
        resp->returnStatus->explanation = soap_new_std__string(soap, -1);
        resp->returnStatus->explanation->assign("");
        resp->arrayOfFileStatuses = soap_new_srm__ArrayOfTGetRequestFileStatus(soap, -1);
    }
    catch (...) {
        std::list<castor::log::Param> params = {
            castor::log::Param("REQID", myLogger->getId()),
            castor::log::Param("Message", "Out of memory")
        };
        castor::log::write(LOG_ALERT, "Exception caught", params);
        utils->unmap_user();
        delete utils;
        myLogger->LOGEXIT(srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR, "Out of memory");
        myWriter.write(out);
        delete myLogger;
        return SOAP_EOM;
    }

    // Set up database...
    castor::Services* svcs;
    castor::srm::ISrmSvc* srmSvc;
    castor::BaseAddress ad;
    try {
        svcs = castor::BaseObject::services();
        ad.setCnvSvcName("DbCnvSvc");
        ad.setCnvSvcType(castor::SVC_DBCNV);
        castor::IService *svc = castor::BaseObject::services()->service("DbSrmSvc", castor::SVC_DBSRMSVC);
        srmSvc = dynamic_cast<castor::srm::ISrmSvc*>(svc);
        if ( srmSvc == 0 ) {
          std::list<castor::log::Param> params = {castor::log::Param("REQID", myLogger->getId())};
          castor::log::write(LOG_ERR, "Could not get Conversion Service for Database", params);
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
            resp->returnStatus->explanation->assign("Unable to establish database service");
            utils->unmap_user();
            delete utils;
            myLogger->LOGEXIT(resp->returnStatus->statusCode,  resp->returnStatus->explanation->c_str());
            myWriter.write(out);
            delete myLogger;
            return SOAP_OK;
        }
    }
    catch (castor::exception::Exception e) {
        myLogger->LOGEXCEPTION(&e, &ad);
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        resp->returnStatus->explanation->assign( e.getMessage().str());
        utils->unmap_user();
        delete utils;
        myLogger->LOGEXIT(resp->returnStatus->statusCode,  resp->returnStatus->explanation->c_str());
        myWriter.write(out);
        delete myLogger;
        return SOAP_OK;
    }

    std::string dn, uname, vo;
    std::string stagerHost;
    castor::srm::StageRequest* original = 0;
    castor::srm::server::RequestValidator validate;
    try {
        dn    = utils->getDn(soap);
        uname = utils->getUserName(soap);
        vo    = utils->getVoName(soap);
        stagerHost = utils->getStagerHost(soap);
        validate.requestToken(srmStatusOfGetRequestRequest->requestToken);
        original = srmSvc->getStageRequest(srmStatusOfGetRequestRequest->requestToken);
        validate.request(original, (int)castor::srm::REQUESTTYPE_GET);
        myLogger->setId(original->srmRequestId());
    } catch (castor::exception::Exception &e) {
        castor::srm::SrmException* se = dynamic_cast<castor::srm::SrmException*>(&e);
        if(se) {
            resp->returnStatus->statusCode = (srm__TStatusCode)se->code();
        } else {
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        }
        resp->returnStatus->explanation->assign(e.getMessage().str());
        utils->unmap_user();
        myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
        myWriter.write(out);
        if(original) delete original;
        delete utils;
        delete myLogger;
        return SOAP_OK;
    }

    // Create a map of filenames with associated boolean value.
    // The boolean is used to determine whether this requested
    // SURL has been processed in the following loop over subrequests.
    // Once subrequest processing is complete, any entries left
    // unprocessed could not have been in the original subrequest.
    // SURLs which can not be parsed are noted along with the error
    srm__ArrayOfAnyURI* surls = srmStatusOfGetRequestRequest->arrayOfSourceSURLs;
    bool explicit_surls = (surls != NULL && surls->urlArray.size()>0) ? true : false;
    if(explicit_surls) {
        for(unsigned int i = 0; i < surls->urlArray.size(); i++) {
            // Create a file response structure and initialize
            // its status to invalid.  When we loop over the
            // subrequests in the original request then we will
            // update this status.  In this way any SURL's not in
            // the original request will remain invalid.
            srm__TGetRequestFileStatus* requestStatus = 
                soap_new_srm__TGetRequestFileStatus(soap, -1);
            requestStatus->status = soap_new_srm__TReturnStatus(soap, -1);
            requestStatus->status->explanation  = soap_new_std__string(soap, -1);
            requestStatus->sourceSURL.assign(surls->urlArray[i]);
            requestStatus->status->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
            requestStatus->status->explanation->assign("SURL not in original request");
            requestStatus->fileSize=NULL;
            requestStatus->estimatedWaitTime=NULL;
            requestStatus->remainingPinTime=NULL;
            requestStatus->transferURL=NULL;
            requestStatus->transferProtocolInfo=NULL;
            resp->arrayOfFileStatuses->statusArray.push_back(requestStatus);
        }
    }

    castor::srm::SrmUser *thisUser = NULL;
    unsigned nSubRequests=0;
    try {
        // lock the original request
        srmSvc->lockStageRequest(original->id());

        svcs->fillObj(&ad, original, castor::srm::OBJ_SubRequest, false);
        unsigned failed     = 0;
        unsigned aborted    = 0;
        unsigned success    = 0;
        unsigned pending    = 0;
        unsigned inProgress = 0;
        unsigned timedout   = 0;
        castor::srm::SubRequest *thisSubRequest = 0;
        //nSubRequests = (explicit_surls) ? surls->urlArray.size():original->subRequests().size();
        nSubRequests = original->subRequests().size();


        // Just in case we need then...
        thisUser = srmSvc->getSrmUser(uname, vo, dn);

        Cns_fileid nsId;
        nsId.fileid = 0;
        strncpy(nsId.server, castor::srm::SrmConstants::getInstance()->nsHost().c_str(), sizeof(nsId.server)-1);
        // Loop over all subrequest
        for ( unsigned i=0; i<original->subRequests().size(); i++ ) {
            thisSubRequest = original->subRequests()[i];
            srm__TGetRequestFileStatus* fileStatus=0;
            // Record status here before generating SURL status since the overall
            // response code mist refer to the whole of the original request, not just
            // any subset of files passed in
            if (thisSubRequest->status() == castor::srm::SUBREQUEST_PENDING) 
                pending++;
            else if (thisSubRequest->status() == castor::srm::SUBREQUEST_INPROGRESS)
                inProgress++;
            else if (thisSubRequest->status() == castor::srm::SUBREQUEST_SUCCESS ||
                     thisSubRequest->status() == castor::srm::SUBREQUEST_RELEASED)
                success++;
            else if (thisSubRequest->status() == castor::srm::SUBREQUEST_ABORTED)
                aborted++;
            else  // All other cases assumed failure
                failed++;

            if ( explicit_surls ) {
                // Find the surl in the std::list
                for (unsigned c=0; c<resp->arrayOfFileStatuses->statusArray.size(); c++) {
                    if (thisSubRequest->surl() == resp->arrayOfFileStatuses->statusArray[c]->sourceSURL) {
                        fileStatus = resp->arrayOfFileStatuses->statusArray[c];
                        fileStatus->status->statusCode = srm__TStatusCode__SRM_USCOREFILE_USCOREIN_USCORECACHE;
                        fileStatus->status->explanation->assign("");
                        break;
                    }
                }
            }
            else { 
                // Create a new structure
                fileStatus = soap_new_srm__TGetRequestFileStatus(soap, -1);
                fileStatus->status = soap_new_srm__TReturnStatus(soap, -1);
                fileStatus->status->explanation  = soap_new_std__string(soap, -1);
                fileStatus->sourceSURL.assign(thisSubRequest->surl());
                fileStatus->status->statusCode = srm__TStatusCode__SRM_USCOREFILE_USCOREIN_USCORECACHE;
                fileStatus->status->explanation->assign("");
                fileStatus->fileSize=NULL;
                fileStatus->estimatedWaitTime=NULL;
                fileStatus->remainingPinTime=NULL;
                fileStatus->transferURL=NULL;
                fileStatus->transferProtocolInfo=NULL;
                resp->arrayOfFileStatuses->statusArray.push_back(fileStatus);
            }
            if (!fileStatus) 
                continue;

            // Now check status of subrequest....
            switch ( (int)thisSubRequest->status() ) {
                case castor::srm::SUBREQUEST_PENDING:
                case castor::srm::SUBREQUEST_INPROGRESS:
                  fileStatus->status->statusCode = (thisSubRequest->status() == castor::srm::SUBREQUEST_PENDING ?
                    srm__TStatusCode__SRM_USCOREREQUEST_USCOREQUEUED : srm__TStatusCode__SRM_USCOREREQUEST_USCOREINPROGRESS);
                    fileStatus->estimatedWaitTime = (int*)soap_malloc(soap, sizeof(int));
                    if ( fileStatus->estimatedWaitTime ) {
                        if ( original->nextCheck() == 0 ) {
                            // too early yet, please come back in at least the same time has already passed since request submission
                            *(fileStatus->estimatedWaitTime) = time(NULL) - original->creationTime();
                        }
                        else {
                            // polling is going on, no news before nextCheck, please come at least just after that
                            *(fileStatus->estimatedWaitTime) = original->nextCheck() - time(NULL) + 1;
                        }
                    }
                    break;
                case castor::srm::SUBREQUEST_SUCCESS:
                    fileStatus->fileSize         = (ULONG64*)soap_malloc(soap, sizeof(ULONG64));
                    fileStatus->remainingPinTime = (int*)soap_malloc(soap, sizeof(int));
                    fileStatus->transferURL      = soap_new_std__string(soap, -1);

                    *(fileStatus->fileSize) = thisSubRequest->reservedSize();
                    *(fileStatus->remainingPinTime) = 0;
                    fileStatus->status->statusCode = srm__TStatusCode__SRM_USCOREFILE_USCOREPINNED;
                    fileStatus->transferURL->assign( thisSubRequest->turl() );
                    break;
                case castor::srm::SUBREQUEST_FAILED:
                    switch ( thisSubRequest->errorCode() ) {
                        case EBUSY:
                            fileStatus->status->statusCode = srm__TStatusCode__SRM_USCOREFILE_USCOREBUSY;
                            break;
                        case EAGAIN:
                            fileStatus->status->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
                            break;
                        case EEXIST:
                            fileStatus->status->statusCode = srm__TStatusCode__SRM_USCOREDUPLICATION_USCOREERROR;
                            break;
                        case ENOENT:
                            fileStatus->status->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREPATH;
                            break;
                        case ETIMEDOUT:
                            fileStatus->status->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
                            timedout++;
                            break;
                        case ESTTAPEOFFLINE:
                        case ESTNOTAVAIL:
                        case ESTNOSEGFOUND:
                            fileStatus->status->statusCode = srm__TStatusCode__SRM_USCOREFILE_USCOREUNAVAILABLE;
                            break;
                        default:
                            fileStatus->status->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
                    }
                    fileStatus->status->explanation->assign(thisSubRequest->reason());
                    break;
                case castor::srm::SUBREQUEST_ABORTED:
                    fileStatus->status->statusCode = srm__TStatusCode__SRM_USCOREABORTED;
                    fileStatus->status->explanation->assign("This subreqeuest has been aborted by the user");
                    break;
                case castor::srm::SUBREQUEST_RELEASED:
                    // This is similar to success case, except that srmReleaseFiles had been called meanwhile.
                    // So it is success, and we explicitly return SRM_RELEASED
                    fileStatus->remainingPinTime = (int*)soap_malloc(soap, sizeof(int));
                    *(fileStatus->remainingPinTime) = 0;
                    fileStatus->status->statusCode = srm__TStatusCode__SRM_USCORERELEASED;
                    break;
                default:
                    failed++;
                    fileStatus->status->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
                    std::stringstream ss;
                    ss << "Unexpected internal status code for srmStatusOfGetRequest: " << thisSubRequest->status() << std::ends;
                    fileStatus->status->explanation->assign(ss.str());
                    break;
            }
        }
        svcs->commit(&ad);

        // Now see what to return at highest level....
        if ( failed == nSubRequests ) {
            // All subrequests failed
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
            resp->returnStatus->explanation->assign("No subrequests succeeded");
        }
        else if ( aborted == nSubRequests ) {
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREABORTED;
            resp->returnStatus->explanation->assign("All subrequests aborted");
        }
        else if ( pending == nSubRequests && original->status() == castor::srm::REQUEST_PENDING ) {
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREREQUEST_USCOREQUEUED;
            resp->returnStatus->explanation->assign("");
        }
        else if ( success == nSubRequests ) {
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
            resp->returnStatus->explanation->assign("");
        }
        else if ( pending > 0 || inProgress > 0 ) {
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREREQUEST_USCOREINPROGRESS;
            std::ostringstream ss;
            ss << (nSubRequests-pending-inProgress) << " out of " << nSubRequests << " subrequests completed";
            resp->returnStatus->explanation->assign(ss.str());
        }
        else if ( timedout > 0 ) {
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREREQUEST_USCORETIMED_USCOREOUT;
            std::ostringstream ss;
            ss << timedout << " out of " << nSubRequests << " subrequests timed out";
            resp->returnStatus->explanation->assign(ss.str());
        }
        else {
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREPARTIAL_USCORESUCCESS;
            std::ostringstream ss;
            ss << success << " out of " << nSubRequests << " subrequests succeeded";
            resp->returnStatus->explanation->assign(ss.str());
        }
    }
    catch (castor::exception::Exception e) {
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        resp->returnStatus->explanation->assign(e.getMessage().str());
        myLogger->LOGEXCEPTION(&e, &ad);
    }
    catch (...) {
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        resp->returnStatus->explanation->assign("Unknown exception in srmStatusOfGetRequest");
        myLogger->LOGEXCEPTION(0, &ad);
    }

    // Free up memory
    for ( unsigned i=0; i<original->subRequests().size();i++ ) {
        delete original->subRequests()[i];
    }
    original->subRequests().clear();
    delete original;
    if (thisUser) {
        delete thisUser;
    }
    utils->unmap_user();
    delete utils;
    myLogger->LOGEXIT(resp->returnStatus->statusCode,  resp->returnStatus->explanation->c_str());
    myWriter.write(out);
    delete myLogger;
    return SOAP_OK;
}
