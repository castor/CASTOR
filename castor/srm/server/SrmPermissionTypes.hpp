#ifndef SRM_SERVER_SRMPERMISSIONTYPES_HPP
#define SRM_SERVER_SRMPERMISSIONTYPES_HPP

namespace castor {
 namespace srm {
    namespace server {
        static enum srm__TPermissionMode ptypes[] = {
            srm__TPermissionMode__NONE,
            srm__TPermissionMode__X,
            srm__TPermissionMode__W,
            srm__TPermissionMode__WX,
            srm__TPermissionMode__R,
            srm__TPermissionMode__RX,
            srm__TPermissionMode__RW,
            srm__TPermissionMode__RWX
        };
    }
  }
}

#endif
