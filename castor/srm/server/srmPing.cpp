#include <cgsi_plugin.h>

#include <osdep.h>
#include <castor/BaseAddress.hpp>
#include <castor/BaseObject.hpp>
#include <castor/Constants.hpp>
#include <castor/Services.hpp>

#include "castor/srm/SrmConstants.hpp"
#include "castor/srm/server/SrmUtils.hpp"
#include "castor/srm/server/StageIncrementer.hpp"
#include "castor/srm/server/SrmLogger.hpp"
#include "castor/srm/server/ResponseWriter.hpp"
#include "soapH.h"

void srmPingAddExtraInfo (
        struct soap *soap,
        std::vector<srm__TExtraInfo*> *v,
        std::string key,
        std::string value) {
    if ( key.length() > 0 ) {
        srm__TExtraInfo* info = soap_new_srm__TExtraInfo(soap, -1);
        if ( info != NULL ) {
            info->value = soap_new_std__string (soap, -1);
            if ( info->value != NULL ) {
                info->key.assign(key);
                info->value->assign(value);
                v->push_back(info);
            }
        }
    }
}

std::string pingGetCastorVersion( struct soap *soap) {
    std::string version;
    try {
        castor::srm::server::SrmUtils utils;
        version = utils.getCastorVersion(soap);
    }
    catch (...) {
        return ("2");
    }
    return version;
}


int srm__srmPing (
        struct soap *soap,
        srm__srmPingRequest*,
        srm__srmPingResponse_  &myResponse)
{
    srm__srmPingResponse *resp;
    castor::srm::server::SrmLogger *myLogger = new castor::srm::server::SrmLogger();
    myLogger->LOGENTRY(soap);

    resp = soap_new_srm__srmPingResponse(soap, -1);

    resp->versionInfo.assign( "v2.2" );
    resp->otherInfo = NULL;

    // Add ExtraInfo block...
    resp->otherInfo = soap_new_srm__ArrayOfTExtraInfo(soap, -1);
    if ( resp->otherInfo ) {
        srmPingAddExtraInfo(soap, &resp->otherInfo->extraInfoArray, "backend_type", "CASTOR");
        srmPingAddExtraInfo(soap, &resp->otherInfo->extraInfoArray, "backend_version", pingGetCastorVersion(soap));
        srmPingAddExtraInfo(soap, &resp->otherInfo->extraInfoArray, "Nb of active CASTOR threads",
          castor::srm::server::SrmUtils::toString(castor::srm::server::StageIncrementer::getCastorCalls()));
    }

    myResponse.srmPingResponse = resp;
    castor::srm::server::ResponseWriter myWriter;
    myWriter.write(myResponse);
    myLogger->LOGEXIT( srm__TStatusCode__SRM_USCORESUCCESS, "");
    delete myLogger;
    return SOAP_OK;
}

