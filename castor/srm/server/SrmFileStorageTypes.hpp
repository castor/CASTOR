#ifndef SRM_SERVER_SRMFILESTORAGETYPES_HPP
#define SRM_SERVER_SRMFILESTORAGETYPES_HPP
namespace castor {
 namespace srm {
    namespace server {
        static enum srm__TFileStorageType fileStorageTypes[] = {
            srm__TFileStorageType__VOLATILE,
            srm__TFileStorageType__DURABLE,
            srm__TFileStorageType__PERMANENT
        };
    }
}
#endif
