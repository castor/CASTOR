#include <libgen.h>

#include "soapH.h"

#include <cgsi_plugin.h>
#include <Cns_api.h>
#include <castor/log/log.hpp>

#include "castor/srm/server/SrmUtils.hpp"
#include "castor/srm/server/SrmLogger.hpp"
#include "castor/srm/server/RequestValidator.hpp"
#include "castor/srm/SrmConstants.hpp"
#include "castor/srm/SURL.hpp"

#include "ResponseWriter.hpp"

int srm__srmMv(
        struct soap *soap,
        srm__srmMvRequest*                  srmMvRequest,
        srm__srmMvResponse_         &rep) //< response parameter
{
    castor::srm::server::SrmLogger *myLogger = new castor::srm::server::SrmLogger();
    myLogger->LOGENTRY(soap);
    srm__srmMvRequest *req;
    srm__srmMvResponse *resp;
    req = srmMvRequest;
    uid_t uid;
    gid_t gid;
    int rc = 0;


    //Prepare the reply
    try {
        resp = soap_new_srm__srmMvResponse(soap, -1);
        resp->returnStatus = soap_new_srm__TReturnStatus(soap, -1);
        resp->returnStatus->explanation = soap_new_std__string(soap, -1);

        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
        resp->returnStatus->explanation->assign("");
    }
    catch (...) {
        std::list<castor::log::Param> params = {
            castor::log::Param("REQID", myLogger->getId()),
            castor::log::Param("Message", "Out of memory")
        };
        castor::log::write(LOG_ALERT, "Exception caught", params);
        myLogger->LOGEXIT(srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR, "Out of memory");
        delete myLogger;
        return SOAP_EOM;
    }

    //Put resp inside rep struct container
    rep.srmMvResponse = resp;

    castor::srm::server::ResponseWriter myWriter;
    castor::srm::server::RequestValidator      validate;

    std::string sourcepath, targetpath;
    try {
        castor::srm::SURL* surl = validate.surl(req->fromSURL);
        sourcepath = surl->getFileName();
        delete surl;
        surl = validate.surl(req->toSURL);
        targetpath = surl->getFileName();
        delete surl;
    } catch (castor::exception::Exception e) {
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST;
        resp->returnStatus->explanation->assign("From/To SURL not specified");
        myWriter.write(rep);
        myLogger->LOGEXIT(resp->returnStatus->statusCode,  resp->returnStatus->explanation->c_str());
        delete myLogger;
        return SOAP_OK;
    }

    //Call the Castor function now to do the mv & return success/error
    std::list<castor::log::Param> fromParams = {
        castor::log::Param("REQID", myLogger->getId()),
        castor::log::Param("SourceFile", sourcepath),
        castor::log::Param("TargetFile", targetpath),
    };
    castor::log::write(LOG_INFO, "Argument Information", fromParams);

    // See if the target is a directory
    castor::srm::server::SrmUtils *utils = new castor::srm::server::SrmUtils();
    utils->map_user(soap, &uid, &gid);
    Cns_filestat tbuf;
    myLogger->logCnsCall("Cns_stat", targetpath.c_str());
    rc = Cns_stat(targetpath.c_str(), &tbuf);
    myLogger->logCnsReturn("Cns_stat", rc);
    if ( rc == 0 ) {
        if ( tbuf.filemode & S_IFDIR ) {
            // Append the basename of the source to the target  
            std::string basename = sourcepath.substr( sourcepath.find_last_of("/"), sourcepath.length());
            targetpath.append("/").append(basename);
        }
    }

    // Stat the source - and log the entry in the Cns call.  we will log the return in the tagret
    Cns_filestat sbuf;
    Cns_fileid   sid;
    sid.fileid=0;
    strncpy( sid.server, castor::srm::SrmConstants::getInstance()->nsHost().c_str(), sizeof(sid.server)-1);
    if ( Cns_stat(sourcepath.c_str(), &sbuf) == 0 ) {
        sid.fileid = sbuf.fileid;
    }
    myLogger->LOGFILEREQ(&sid, sourcepath);

    myLogger->logCnsCall("Cns_rename", sourcepath, &sid, LOG_INFO);
    rc = Cns_rename(sourcepath.c_str(), targetpath.c_str());
    sid.fileid=0;
    if (rc == 0) 
        if (Cns_stat(targetpath.c_str(), &tbuf) == 0)
            sid.fileid = tbuf.fileid;
    myLogger->logCnsReturn("Cns_rename", rc, &sid);

    if (rc != 0) {
        switch (serrno) {
            case ENOENT:
            case EFAULT:
            case ENOTDIR:
            case EISDIR:
            case ENAMETOOLONG:
                resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREPATH;
                resp->returnStatus->explanation->assign(sstrerror(serrno));
                break;
            case EACCES:
                resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREAUTHORIZATION_USCOREFAILURE;
                resp->returnStatus->explanation->assign(sstrerror(serrno));
                break;
            case EEXIST:
                resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREDUPLICATION_USCOREERROR;
                resp->returnStatus->explanation->assign(sstrerror(serrno));
                break;
            case EINVAL:
                resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST;
                resp->returnStatus->explanation->assign(sstrerror(serrno));
                break;
            case SENOSHOST:
            case SENOSSERV:
            case SECOMERR:
            case ENSNACT:
                resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
                resp->returnStatus->explanation->assign(sstrerror(serrno));
                break;
            default:
                resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
                resp->returnStatus->explanation->assign("Unexpected error status");
                break;
        }
    }
    utils->unmap_user();
    delete utils;
    myWriter.write(rep);
    myLogger->LOGEXIT(resp->returnStatus->statusCode,  resp->returnStatus->explanation->c_str());
    delete myLogger;
    return SOAP_OK;
}


