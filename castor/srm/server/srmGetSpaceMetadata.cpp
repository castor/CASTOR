#include <castor/BaseAddress.hpp>
#include <castor/BaseObject.hpp>
#include <castor/Constants.hpp>
#include <castor/Services.hpp>
#include <stager_client_api.h>
#include <client/src/stager/stager_client_api_query.hpp>
#include <castor/query/DiskPoolQueryType.hpp>
#include <castor/exception/Exception.hpp>

#include "soapH.h"
#include "castor/srm/ISrmSvc.hpp"
#include "castor/srm/server/SrmLogger.hpp"
#include "castor/srm/server/RequestValidator.hpp"
#include "castor/srm/StorageArea.hpp"
#include "castor/srm/StorageStatus.hpp"
#include "castor/srm/SrmConstants.hpp"
#include "castor/srm/server/SrmUtils.hpp"
#include "ResponseWriter.hpp"

int srm__srmGetSpaceMetaData(
        struct soap *soap,
        srm__srmGetSpaceMetaDataRequest*    srmGetSpaceMetaDataRequest,
        srm__srmGetSpaceMetaDataResponse_& response) //< response parameter
{
    castor::srm::server::SrmLogger *myLogger = new castor::srm::server::SrmLogger();
    myLogger->LOGENTRY(soap);

    srm__srmGetSpaceMetaDataResponse *resp;
    try {
        resp = soap_new_srm__srmGetSpaceMetaDataResponse( soap, -1 );
        resp->returnStatus = soap_new_srm__TReturnStatus( soap, -1 );
        resp->returnStatus->explanation = soap_new_std__string( soap, -1);
        resp->arrayOfSpaceDetails = NULL;

        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
        resp->returnStatus->explanation->assign("");
    }
    catch (...) {
        std::list<castor::log::Param> params = {
            castor::log::Param("REQID", myLogger->getId()),
            castor::log::Param("Message", "Out of memory")
        };
        castor::log::write(LOG_ALERT, "Exception caught", params);
        myLogger->LOGEXIT(srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR, "Out of memory");
        delete myLogger;
        return SOAP_EOM;
    }
    response.srmGetSpaceMetaDataResponse = resp;

    castor::srm::server::ResponseWriter   myWriter;
    castor::srm::server::RequestValidator        validate;

    // Set up database
    castor::srm::ISrmSvc* srmSvc;
    castor::BaseAddress ad;
    try {
        ad.setCnvSvcName("DbCnvSvc");
        ad.setCnvSvcType(castor::SVC_DBCNV);
        castor::IService *svc = castor::BaseObject::services()->service("DbSrmSvc", castor::SVC_DBSRMSVC);
        srmSvc = dynamic_cast<castor::srm::ISrmSvc*>(svc);
        if ( srmSvc == 0 ) {
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
            resp->returnStatus->explanation->assign("Unable to establish database service");
            std::list<castor::log::Param> params = {castor::log::Param("REQID", myLogger->getId())};
            castor::log::write(LOG_ERR, "Could not get Conversion Service for Database", params);
            myWriter.write(response);
            myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
            delete myLogger;
            return SOAP_OK;
        }
    }
    catch (castor::exception::Exception &e) {
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        resp->returnStatus->explanation->assign(e.getMessage().str());
        myLogger->LOGEXCEPTION(&e, &ad);
        myWriter.write(response);
        myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
        delete myLogger;
        return SOAP_OK;
    }

    std::string stagerHost;
    castor::srm::server::SrmUtils *utils=0;
    try {
        validate.notNull(srmGetSpaceMetaDataRequest->arrayOfSpaceTokens, "arrayOfSpaceTokens");
        validate.validateVectorOfStrings(srmGetSpaceMetaDataRequest->arrayOfSpaceTokens->stringArray);
        utils = new castor::srm::server::SrmUtils();
        stagerHost = utils->getStagerHost(soap);
        delete utils;
    } catch (castor::exception::Exception &e) {
        castor::srm::SrmException* se = dynamic_cast<castor::srm::SrmException*>(&e);
        if(se) {
            resp->returnStatus->statusCode = (srm__TStatusCode)se->code();
        } else {
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        }
        resp->returnStatus->explanation->assign(e.getMessage().str());
        myWriter.write(response);
        myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
        delete myLogger;
        if (utils) delete utils;
        return SOAP_OK;
    }

    struct stage_options opts;
    opts.stage_host = strdup(const_cast<char*> (stagerHost.c_str()));
    opts.stage_port = STAGERPORT;
    opts.service_class = NULL;

    int  nValid  = 0;
    resp->arrayOfSpaceDetails = soap_new_srm__ArrayOfTMetaDataSpace(soap, -1);
    std::vector<std::string> tokens = srmGetSpaceMetaDataRequest->arrayOfSpaceTokens->stringArray;
    for ( unsigned i=0; i<tokens.size(); i++ ) {
        // allocate memory
        srm__TMetaDataSpace* summary = soap_new_srm__TMetaDataSpace( soap, -1 );
        summary->spaceToken          = tokens[i];
        summary->status              = soap_new_srm__TReturnStatus(soap, -1);
        summary->status->explanation = soap_new_std__string(soap, -1);
        summary->retentionPolicyInfo = NULL;
        summary->owner               = NULL;
        summary->totalSize           = NULL;
        summary->guaranteedSize      = NULL;
        summary->unusedSize          = NULL;
        summary->lifetimeAssigned    = NULL;
        summary->lifetimeLeft        = NULL;

        summary->status->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
        summary->status->explanation->assign("");

        // get the storagearea
        castor::srm::StorageArea *thisArea = 0;
        try {
            validate.spaceToken(tokens[i]);
            thisArea = srmSvc->getStorageArea(tokens[i]);
            validate.storageArea(thisArea);
            validate.svcClass(thisArea);
        } catch (castor::srm::SrmException &e) {
            summary->status->statusCode = (srm__TStatusCode)e.code();
            summary->status->explanation->assign(e.getMessage().str());
            resp->arrayOfSpaceDetails->spaceDataArray.push_back(summary);
            if (thisArea) delete thisArea;
            continue;
        }catch (castor::exception::Exception &e) {
            summary->status->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
            summary->status->explanation->assign("Unknown exception: ");
            summary->status->explanation->append(e.getMessage().str());
            resp->arrayOfSpaceDetails->spaceDataArray.push_back(summary);
            if (thisArea) delete thisArea;
            continue;
        }

        // Allocate space for valid data
        summary->retentionPolicyInfo =  soap_new_srm__TRetentionPolicyInfo (soap, -1);
        summary->totalSize           = (ULONG64*)soap_malloc(soap, sizeof(ULONG64));
        summary->guaranteedSize      = (ULONG64*)soap_malloc(soap, sizeof(ULONG64));
        summary->unusedSize          = (ULONG64*)soap_malloc(soap, sizeof(ULONG64));
        summary->lifetimeLeft        = (int*)soap_malloc(soap, sizeof(int));

        summary->retentionPolicyInfo->retentionPolicy = srm__TRetentionPolicy__CUSTODIAL; // Most spaces in CASTOR are this
        summary->retentionPolicyInfo->accessLatency   = NULL;
        *(summary->totalSize)                         = 0;
        *(summary->guaranteedSize)                    = 0;
        *(summary->unusedSize)                        = 0;
        *(summary->lifetimeLeft)                      = -1;

        switch (thisArea->storageType()) {
            case 0:
            case 1:
            default:
                summary->retentionPolicyInfo->retentionPolicy = srm__TRetentionPolicy__CUSTODIAL;
                break;
            case 2:
                summary->retentionPolicyInfo->retentionPolicy = srm__TRetentionPolicy__REPLICA;
                break;
        }

        // Get the disk pool information
        struct stage_diskpoolquery_resp *responses;
        int rc, nResp;
        opts.service_class=strdup(const_cast<char*>(thisArea->svcClass().c_str()));

        myLogger->logStagerCall("stage_diskpoolsquery", opts);
        rc = stage_diskpoolsquery_internal(&responses, &nResp, &opts, castor::query::DISKPOOLQUERYTYPE_AVAILABLE);
        myLogger->logStagerReturn("stage_diskpoolsquery", rc, "");

        free (opts.service_class);

        if (rc != 0) {
            summary->status->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
            std::stringstream  message;
            message << "stage_diskpoolsquery for token " << tokens[i] 
                << "failed with error " << sstrerror(serrno);
            summary->status->explanation->assign(message.str());
            *(summary->lifetimeLeft)                      = 0;
            if (thisArea) delete thisArea;
            continue;
        }
        u_signed64 totSpace  = 0;
        u_signed64 freeSpace = 0;
        for (int i = 0; i < nResp; i++) {
            totSpace  += responses[i].totalSpace;
            freeSpace += responses[i].freeSpace;
            stage_delete_diskpoolquery_resp(&(responses[i]));
        }
        *(summary->totalSize)                         = totSpace;
        *(summary->guaranteedSize)                    = totSpace;
        *(summary->unusedSize)                        = freeSpace;
        *(summary->lifetimeLeft)                      = -1;
        summary->retentionPolicyInfo->retentionPolicy = (srm__TRetentionPolicy)(thisArea->storageType());
        resp->arrayOfSpaceDetails->spaceDataArray.push_back(summary);
        nValid++;
        delete thisArea;
    }

    if (nValid == 0) {
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
        resp->returnStatus->explanation->assign("No valid space tokens found matching query");
    }
    else if ((unsigned)nValid == tokens.size()) {
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
        resp->returnStatus->explanation->assign("");
    }
    else {
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREPARTIAL_USCORESUCCESS;
        resp->returnStatus->explanation->assign("");
    }

    myWriter.write(response);
    myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
    delete myLogger;
    return SOAP_OK;
}


