#include <cgsi_plugin.h>

#include <castor/BaseAddress.hpp>
#include <castor/BaseObject.hpp>
#include <castor/Constants.hpp>
#include <castor/Services.hpp>

#include <castor/exception/Exception.hpp>

#include "soapH.h"

#include "castor/srm/ISrmSvc.hpp"
#include "castor/srm/SrmUser.hpp"
#include "castor/srm/server/SrmUtils.hpp"
#include "castor/srm/server/SrmLogger.hpp"
#include "castor/srm/StorageArea.hpp"

#include "ResponseWriter.hpp"

int srm__srmGetSpaceTokens(
        struct soap *soap,
        srm__srmGetSpaceTokensRequest*       srmGetSpaceTokensRequest,
        srm__srmGetSpaceTokensResponse_&     response) //< response parameter
{
    castor::srm::server::SrmLogger *myLogger = new castor::srm::server::SrmLogger();
    myLogger->LOGENTRY(soap);

    srm__srmGetSpaceTokensResponse *resp;
    try {
        resp = soap_new_srm__srmGetSpaceTokensResponse(soap, -1);
        resp->returnStatus = soap_new_srm__TReturnStatus(soap, -1);
        resp->returnStatus->explanation = soap_new_std__string(soap, -1);
        resp->arrayOfSpaceTokens = NULL;
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
        resp->returnStatus->explanation->assign("");
    }
    catch (...) {
        std::list<castor::log::Param> params = {
            castor::log::Param("REQID", myLogger->getId()),
            castor::log::Param("Message", "Out of memory")
        };
        castor::log::write(LOG_ALERT, "Exception caught", params);
        myLogger->LOGEXIT(srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR, "Out of memory");
        delete myLogger;
        return SOAP_EOM;
    }
    response.srmGetSpaceTokensResponse = resp;

    castor::srm::server::ResponseWriter myWriter;

    /*
     * In principle this is an optional parameter.  If not supplied, we return all tokens
     * belonging to the VO
    // See if we have a valid space token...
    if ( srmGetSpaceTokensRequest->userSpaceTokenDescription == NULL ||
            srmGetSpaceTokensRequest->userSpaceTokenDescription->length() == 0 ) {
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST;
        resp->returnStatus->explanation->assign("No userSpaceTokenDescription supplied");
        myWriter.write(response);
        myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
        delete myLogger;
        return SOAP_OK;
    }
    */

    // Set up database services
    castor::srm::ISrmSvc* srmSvc;
    castor::BaseAddress ad;
    try {
        ad.setCnvSvcName("DbCnvSvc");
        ad.setCnvSvcType(castor::SVC_DBCNV);
        castor::IService *svc = castor::BaseObject::services()->service("DbSrmSvc", castor::SVC_DBSRMSVC);
        srmSvc = dynamic_cast<castor::srm::ISrmSvc*>(svc);
        if ( srmSvc == 0 ) {
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
            resp->returnStatus->explanation->assign("Unable to establish database service");
            std::list<castor::log::Param> params = {castor::log::Param("REQID", myLogger->getId())};
            castor::log::write(LOG_ERR, "Could not get Conversion Service for Database", params);
            myWriter.write(response);
            myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
            delete myLogger;
            return SOAP_OK;
        }
    }
    catch (castor::exception::Exception e) {
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        resp->returnStatus->explanation->assign( e.getMessage().str());
        myLogger->LOGEXCEPTION(&e, &ad);
        myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
        delete myLogger;
        return SOAP_OK;
    }

    try {
        // Get information about current user and check against request owner
        castor::srm::server::SrmUtils *utils = new castor::srm::server::SrmUtils();
        std::string dn, uname, vo;
        try {
            dn    = utils->getDn(soap);
            uname = utils->getUserName(soap);
            vo    = utils->getVoName(soap);
        } catch(castor::exception::Exception &e) {
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREAUTHENTICATION_USCOREFAILURE;
            resp->returnStatus->explanation->assign ("Could not get user information");
            std::list<castor::log::Param> params = {
                castor::log::Param("REQID", myLogger->getId()),
                castor::log::Param("Details", e.getMessage().str() )
            };
            castor::log::write(LOG_ERR, "Authentication error", params);
            *resp->returnStatus->explanation += ": " + e.getMessage().str();
            myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
            myWriter.write(response);
            delete utils;
            delete myLogger;  
            return SOAP_OK;
        }

        delete utils;

        // Get the space tokens
        std::vector<std::string> tokens = srmSvc->getSpaceTokens(&vo, srmGetSpaceTokensRequest->userSpaceTokenDescription);
        if ( srmGetSpaceTokensRequest->userSpaceTokenDescription ) {
            std::list<castor::log::Param> params = {
                castor::log::Param("REQID", myLogger->getId()),
                castor::log::Param("SpaceTokenDesc", srmGetSpaceTokensRequest->userSpaceTokenDescription->c_str()),
                castor::log::Param("VO", vo),
                castor::log::Param("NumSpaceTokens", tokens.size())
            };
            castor::log::write(LOG_DEBUG, "Got space tokens", params);
        }
        else {
            std::list<castor::log::Param> params = {
                castor::log::Param("REQID", myLogger->getId()),
                castor::log::Param("VO", vo),
                castor::log::Param("NumSpaceTokens", tokens.size())
            };
            castor::log::write(LOG_DEBUG, "Got space tokens", params);
        }

        // Now loop over all results (assuming there are any...)
        if ( tokens.size() > 0 ) {
            resp->arrayOfSpaceTokens = soap_new_srm__ArrayOfString(soap, -1);
            for ( unsigned i=0; i<tokens.size(); i++ ) {
                resp->arrayOfSpaceTokens->stringArray.push_back(tokens[i]);
            }
        }
    }
    catch (castor::exception::Exception x) {
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        resp->returnStatus->explanation->assign(x.getMessage().str());
        if ( resp->arrayOfSpaceTokens ) {
            soap_delete(soap, resp->arrayOfSpaceTokens);
            resp->arrayOfSpaceTokens = NULL;
        }
        myLogger->LOGEXCEPTION(&x, &ad);
        myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
        delete myLogger;
        return SOAP_OK;
    }
    if ( resp->arrayOfSpaceTokens == NULL ||
            resp->arrayOfSpaceTokens->stringArray.size() == 0 ) {
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST;
        resp->returnStatus->explanation->assign("No space tokens found matching description");
    }
    myWriter.write(response);
    myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
    delete myLogger;
    return SOAP_OK;
}


