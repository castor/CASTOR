#include <cgsi_plugin.h>

#include <castor/BaseAddress.hpp>
#include <castor/BaseObject.hpp>
#include <castor/Constants.hpp>
#include <castor/Services.hpp>
#include <castor/log/log.hpp>

#include <stager_client_api.h>

#include <castor/exception/Exception.hpp>

#include "soapH.h"
#include "castor/srm/Constants.hpp"
#include "castor/srm/ISrmSvc.hpp"
#include "castor/srm/SrmUser.hpp"
#include "castor/srm/StageRequest.hpp"
#include "castor/srm/SubRequest.hpp"
#include "castor/srm/SubRequestStatus.hpp"
#include "castor/srm/UserFile.hpp"
#include "castor/srm/SrmConstants.hpp"
#include "castor/srm/SURL.hpp"
#include "castor/srm/SrmUtils.hpp"
#include "castor/srm/server/SrmLogger.hpp"
#include "castor/srm/server/ResponseWriter.hpp"
#include "castor/srm/server/RequestValidator.hpp"
#include "castor/srm/server/SrmUtils.hpp"

int srm__srmStatusOfPutRequest(
        struct soap *soap,
        srm__srmStatusOfPutRequestRequest*  srmStatusOfPutRequestRequest,
        srm__srmStatusOfPutRequestResponse_& out) //< response parameter
{
    castor::srm::server::SrmLogger *myLogger = new castor::srm::server::SrmLogger();
    myLogger->LOGENTRY(soap);

    castor::srm::server::SrmUtils *utils = new castor::srm::server::SrmUtils();
    utils->map_user(soap, NULL, NULL);

    srm__srmStatusOfPutRequestResponse *resp;
    try {
        // Allocate basic space requirements
        resp = soap_new_srm__srmStatusOfPutRequestResponse(soap,-1);
        resp->returnStatus = soap_new_srm__TReturnStatus(soap, -1);
        resp->returnStatus->explanation = soap_new_std__string(soap, -1);
        resp->arrayOfFileStatuses = soap_new_srm__ArrayOfTPutRequestFileStatus(soap, -1);
        resp->remainingTotalRequestTime = NULL;

        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
        resp->returnStatus->explanation->assign("");
    }
    catch (...) {
        std::list<castor::log::Param> params = {
            castor::log::Param("REQID", myLogger->getId()),
            castor::log::Param("Message", "Out of memory")
        };
        castor::log::write(LOG_ALERT, "Exception caught", params);
        utils->unmap_user();
        delete utils;
        myLogger->LOGEXIT(srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR, "Out of memory");
        delete myLogger;
        return SOAP_EOM;
    }
    out.srmStatusOfPutRequestResponse = resp;

    // Set up database service
    castor::Services* svcs;
    castor::srm::ISrmSvc* srmSvc;
    castor::BaseAddress ad;
    try {
        svcs = castor::BaseObject::services();
        ad.setCnvSvcName("DbCnvSvc");
        ad.setCnvSvcType(castor::SVC_DBCNV);
        castor::IService *svc = castor::BaseObject::services()->service("DbSrmSvc", castor::SVC_DBSRMSVC);
        srmSvc = dynamic_cast<castor::srm::ISrmSvc*>(svc);
        if ( srmSvc == 0 ) {
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
            resp->returnStatus->explanation->assign("Unable to establish database service");
            std::list<castor::log::Param> params = {castor::log::Param("REQID", myLogger->getId())};
            castor::log::write(LOG_ERR, "Could not get Conversion Service for Database", params);
            utils->unmap_user();
            delete utils;
            myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
            delete myLogger;
            return SOAP_OK;
        }
    }
    catch (castor::exception::Exception e) {
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        resp->returnStatus->explanation->assign( e.getMessage().str());
        myLogger->LOGEXCEPTION(&e, &ad);
        utils->unmap_user();
        delete utils;
        myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
        delete myLogger;
        return SOAP_OK;
    }

    std::string dn, uname, vo;
    std::string stagerHost;
    castor::srm::StageRequest* original = 0;
    castor::srm::server::RequestValidator validate;
    try {
        dn = utils->getDn(soap);
        uname = utils->getUserName(soap);
        vo    = utils->getVoName(soap);
        stagerHost = utils->getStagerHost(soap);
        validate.requestToken(srmStatusOfPutRequestRequest->requestToken);
        original = srmSvc->getStageRequest(srmStatusOfPutRequestRequest->requestToken);
        validate.request(original, (int)castor::srm::REQUESTTYPE_PUT);
        svcs->fillObj(&ad, original, castor::srm::OBJ_SrmUser, true);
        validate.permission(uname, dn,
                original->srmUser()->userID(),
                original->srmUser()->dn());
        delete original->srmUser();
        original->setSrmUser(0);
    } catch (castor::exception::Exception &e) {
        castor::srm::SrmException* se = dynamic_cast<castor::srm::SrmException*>(&e);
        if(se) {
            resp->returnStatus->statusCode = (srm__TStatusCode)se->code();
        } else {
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        }
        resp->returnStatus->explanation->assign(e.getMessage().str());
        utils->unmap_user();
        myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
        delete utils;
        if (original) {
            if (original->srmUser()) {
                delete original->srmUser();
            }
            delete original;
        }
        delete myLogger;
        return SOAP_OK;

    }
    myLogger->setId(original->srmRequestId());

    // Check for request expiration...
    if ( original->creationTime() + original->expirationInterval() < (u_signed64)time(NULL) ) {
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREREQUEST_USCORETIMED_USCOREOUT;
        resp->returnStatus->explanation->assign("Request Timed out!");
        utils->unmap_user();
        myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
        delete original;
        delete myLogger;
        return SOAP_OK;
    }

    //
    // If we have a std::list of surls, create template return
    // statuses for each request
    //
    srm__ArrayOfAnyURI* surls = srmStatusOfPutRequestRequest->arrayOfTargetSURLs;
    bool explicit_surls = (surls != NULL && surls->urlArray.size()>0) ? true : false;
    if(explicit_surls) {
        for(unsigned int i = 0; i < surls->urlArray.size(); i++) {
            // Create a file response structure and initialize
            // its status to invalid.  When we loop over the
            // subrequests in the original request then we will
            // update this status.  In this way any SURL's not in
            // the original request will remain invalid.
            srm__TPutRequestFileStatus* fileStatus = 
                soap_new_srm__TPutRequestFileStatus(soap, -1);
            fileStatus->status = soap_new_srm__TReturnStatus(soap, -1);
            fileStatus->status->explanation  = soap_new_std__string(soap, -1);
            fileStatus->SURL.assign(surls->urlArray[i]);
            fileStatus->status->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
            fileStatus->status->explanation->assign("SURL not in original request");
            fileStatus->fileSize=NULL;
            fileStatus->estimatedWaitTime=NULL;
            fileStatus->remainingPinLifetime=NULL;
            fileStatus->remainingFileLifetime=NULL;
            fileStatus->transferURL=NULL;
            fileStatus->transferProtocolInfo=NULL;
            resp->arrayOfFileStatuses->statusArray.push_back(fileStatus);
        }
    }

    unsigned nSubRequests=0;

    try {
        try {
            srmSvc->lockStageRequest(original->id());
        }
        catch ( castor::exception::Exception x ) {
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREREQUEST_USCOREINPROGRESS;
            resp->returnStatus->explanation->assign("");
            utils->unmap_user();
            delete utils;
            delete original;
            myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
            delete myLogger;
            return SOAP_OK;
        }

        // Fill subrequest object
        svcs->fillObj(&ad, original, castor::srm::OBJ_SubRequest, false);
        nSubRequests = original->subRequests().size();

        // Otherwise fill in remaining request time in response
        resp->remainingTotalRequestTime = (int*)soap_malloc(soap, sizeof(int));
        *(resp->remainingTotalRequestTime) = original->creationTime() + original->expirationInterval() - time(NULL);
        if ( *(resp->remainingTotalRequestTime) < 0 ) {
            *(resp->remainingTotalRequestTime) = 0;
        }

        // Set up counters for subrequest status'
        unsigned failed  = 0;
        unsigned aborted = 0;
        unsigned success = 0;
        unsigned pending = 0;
        unsigned inProgress = 0;

        castor::srm::SubRequest *thisSubRequest = 0; // Holder for current subrequest

        Cns_fileid nsId;
        nsId.fileid = 0;
        strncpy(nsId.server, castor::srm::SrmConstants::getInstance()->nsHost().c_str(), sizeof(nsId.server)-1);

        // Start looping over subrequests
        for ( unsigned i=0; i<original->subRequests().size(); i++ ) {
            srm__TPutRequestFileStatus* fileStatus=0;
            thisSubRequest = original->subRequests()[i];
            // Record status here before generating SURL status since the overall
            // response code must refer to the whole of the original request, not just
            // any subset of files passed in
            if (thisSubRequest->status() == castor::srm::SUBREQUEST_PENDING) 
                pending++;
            else if (thisSubRequest->status() == castor::srm::SUBREQUEST_INPROGRESS)
                inProgress++;
            else if (thisSubRequest->status() == castor::srm::SUBREQUEST_SUCCESS ||
                     thisSubRequest->status() == castor::srm::SUBREQUEST_PUTDONE)
                success++;
            else if (thisSubRequest->status() == castor::srm::SUBREQUEST_ABORTED)
                aborted++;
            else  // All other cases assumed failure
                failed++;

            castor::srm::UserFile *uf = srmSvc->getUserFile( thisSubRequest->castorFileName() );
            nsId.fileid = uf->fileId();
            delete uf;

            if ( explicit_surls ) {
                // Find the surl in the std::list
                for (unsigned c=0; c<resp->arrayOfFileStatuses->statusArray.size(); c++) {
                    if (thisSubRequest->surl() == resp->arrayOfFileStatuses->statusArray[c]->SURL) {
                        fileStatus = resp->arrayOfFileStatuses->statusArray[c];
                        fileStatus->status->statusCode = srm__TStatusCode__SRM_USCOREFILE_USCOREIN_USCORECACHE;
                        fileStatus->status->explanation->assign("");
                        break;
                    }
                }
            }
            else {
                // Create a new one
                fileStatus = soap_new_srm__TPutRequestFileStatus(soap, -1);
                fileStatus->status = soap_new_srm__TReturnStatus(soap, -1);
                fileStatus->status->explanation  = soap_new_std__string(soap, -1);
                fileStatus->SURL.assign(thisSubRequest->surl());
                fileStatus->status->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
                fileStatus->status->explanation->assign("");
                fileStatus->fileSize=NULL;
                fileStatus->estimatedWaitTime=NULL;
                fileStatus->remainingPinLifetime=NULL;
                fileStatus->remainingFileLifetime=NULL;
                fileStatus->transferURL=NULL;
                fileStatus->transferProtocolInfo=NULL;
                resp->arrayOfFileStatuses->statusArray.push_back(fileStatus);
            }
            if (!fileStatus)
                continue;

            fileStatus->fileSize    = (ULONG64*)soap_malloc(soap, sizeof(ULONG64));
            *(fileStatus->fileSize) = thisSubRequest->reservedSize();

            // Now check status of subrequest...
            switch ( (int)thisSubRequest->status() ) {
                case castor::srm::SUBREQUEST_PENDING:
                    fileStatus->status->statusCode = srm__TStatusCode__SRM_USCOREREQUEST_USCOREQUEUED;
                    fileStatus->estimatedWaitTime  = (int*)soap_malloc(soap, sizeof(int));
                    *(fileStatus->estimatedWaitTime) = time(NULL) - original->creationTime();
                    break;
                    
                case castor::srm::SUBREQUEST_INPROGRESS:
                    fileStatus->status->statusCode = srm__TStatusCode__SRM_USCOREREQUEST_USCOREINPROGRESS;
                    fileStatus->estimatedWaitTime  = (int*)soap_malloc(soap, sizeof(int));
                    *(fileStatus->estimatedWaitTime) = time(NULL) - original->creationTime();
                    break;
                    
                case castor::srm::SUBREQUEST_SUCCESS:
                    fileStatus->status->statusCode = srm__TStatusCode__SRM_USCORESPACE_USCOREAVAILABLE;
                    fileStatus->transferURL        = soap_new_std__string(soap, -1);
                    fileStatus->transferURL->assign( thisSubRequest->turl() );
                    break;
                    
                case castor::srm::SUBREQUEST_PUTDONE:
                    fileStatus->status->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
                    {
                      castor::srm::UserFile *uf = srmSvc->getUserFile(thisSubRequest->castorFileName());
                      *(fileStatus->fileSize) = uf->fileSize();
                      delete uf;
                    }
                    fileStatus->remainingPinLifetime  = (int*)soap_malloc(soap, sizeof(int));;
                    fileStatus->remainingFileLifetime = (int*)soap_malloc(soap, sizeof(int));;
                    *(fileStatus->remainingPinLifetime)  = 0;
                    *(fileStatus->remainingFileLifetime) = -1;
                    break;
                    
                case castor::srm::SUBREQUEST_FAILED:
                    switch ( thisSubRequest->errorCode() ) {
                        case EBUSY:
                            fileStatus->status->statusCode = srm__TStatusCode__SRM_USCOREFILE_USCOREBUSY;
                            break;
                        case EAGAIN:
                            fileStatus->status->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
                            break;
                        case EEXIST:
                            fileStatus->status->statusCode = srm__TStatusCode__SRM_USCOREDUPLICATION_USCOREERROR;
                            break;
                        case ENOENT:
                            fileStatus->status->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREPATH;
                            break;
                        case EACCES:
                            fileStatus->status->statusCode = srm__TStatusCode__SRM_USCOREAUTHORIZATION_USCOREFAILURE;
                            break;
                        default:
                            fileStatus->status->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
                            break;
                    }
                    fileStatus->status->explanation->assign(thisSubRequest->reason());
                    if ( fileStatus->fileSize ) {
                        soap_dealloc(soap, fileStatus->fileSize);
                        fileStatus->fileSize = NULL;
                    }
                    break;
                    
                case castor::srm::SUBREQUEST_ABORTED:
                    fileStatus->status->statusCode = srm__TStatusCode__SRM_USCOREABORTED;
                    fileStatus->status->explanation->assign("This subreqeuest has been aborted by the user");
                    if (fileStatus->fileSize) {
                        soap_dealloc (soap, fileStatus->fileSize);
                        fileStatus->fileSize = NULL;
                    }
                    break;
                    
                default:
                    fileStatus->status->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
                    std::stringstream ss;
                    ss << "Unexpected internal status code for srmStatusOfPutRequest: " << thisSubRequest->status() << std::ends;
                    fileStatus->status->explanation->assign(ss.str());
                    break;
            } // End of switch
        } // End loop over subrequests
        svcs->commit(&ad);  // commit all and remove locks
        // Now see what to return at highest level....
        if ( failed == nSubRequests ) {
            // All subrequests failed
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
            resp->returnStatus->explanation->assign("No subrequests succeeded");
        }
        else if ( aborted == nSubRequests ) {
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREABORTED;
            resp->returnStatus->explanation->assign("All subrequests aborted");
            *(resp->remainingTotalRequestTime)=0;
        }
        else if ( failed+aborted == nSubRequests ) {
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
            resp->returnStatus->explanation->assign("No subrequests succeeded");
        }
        else if ( pending == nSubRequests ) {
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREREQUEST_USCOREQUEUED;
            resp->returnStatus->explanation->assign("");
        }
        else if ( success == nSubRequests ) {
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
            resp->returnStatus->explanation->assign("");
        }
        else if ( inProgress > 0 ) {
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREREQUEST_USCOREINPROGRESS;
            resp->returnStatus->explanation->assign("");
        }
        else if ( pending > 0 ) {
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREREQUEST_USCOREQUEUED;
            resp->returnStatus->explanation->assign("");
        }
        else {
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREPARTIAL_USCORESUCCESS;
            resp->returnStatus->explanation->assign("");
        }
    } // end of try block

    catch (castor::exception::Exception e) {
        svcs->rollback(&ad);  // release locks
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        resp->returnStatus->explanation->assign(e.getMessage().str());
        myLogger->LOGEXCEPTION(&e, &ad);
    }
    catch (std::string s) {
        svcs->rollback(&ad);  // release locks
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        resp->returnStatus->explanation->assign(s);
        std::list<castor::log::Param> params = {
            castor::log::Param("REQID", myLogger->getId()),
            castor::log::Param("Message", "Exception caught during processing"),
            castor::log::Param("Details", s )
        };
        castor::log::write(LOG_ERR, "Exception caught", params);
    }
    catch(...) {
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        resp->returnStatus->explanation->assign("Unexpected exception caught");
        std::list<castor::log::Param> params = {
          castor::log::Param("REQID", myLogger->getId())
        };
        castor::log::write(LOG_ALERT, "Exception caught", params);
    }
    
    utils->unmap_user();
    delete utils;
    myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
    for (unsigned i=0; i<original->subRequests().size(); i++) {
        delete original->subRequests()[i];
    }
    original->subRequests().clear();
    delete original;
    delete myLogger;
    castor::srm::server::ResponseWriter myWriter;
    myWriter.write(out);
    return SOAP_OK;
}
