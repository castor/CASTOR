/******************************************************************************
 *                      SoapListenerThreadPool.hpp
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * A ListenerThreadPool to handle SOAP connections as used by the SRM server
 *
 * @author Giuseppe Lo Presti
 *****************************************************************************/

#ifndef SRM_SERVER_SOAPLISTENERTHREADPOOL_HPP
#define SRM_SERVER_SOAPLISTENERTHREADPOOL_HPP 1

#include <iostream>
#include <string>
#include "soapH.h"
#include "castor/server/ListenerThreadPool.hpp"
#include "castor/exception/Exception.hpp"
#include "castor/io/ServerSocket.hpp"

namespace castor {
 namespace srm {

 namespace server {

  /**
   * A ListenerThreadPool able to handle SOAP connections
   * as used by the SRM server
   */
  class SoapListenerThreadPool : public castor::server::ListenerThreadPool {

  public:

    /**
     * Constructor. Note that the port is taken afterwards from the SrmConstants.
     * @param poolName, thread, initThreads, maxThreads as in ListenerThreadPool
     * See castor::server::ListenerThreadPool.
     */
    SoapListenerThreadPool(const std::string poolName, castor::server::IThread* thread,
      unsigned int initThreads, unsigned int maxThreads) throw() :
      castor::server::ListenerThreadPool::ListenerThreadPool
      (poolName, thread, 0, true, initThreads, maxThreads) {};

    /**
     * Destructor
     */
    virtual ~SoapListenerThreadPool() throw() {};

    /**
     * Stops the SOAP listening and performs a shutdown of the pool
     */
    virtual bool shutdown() throw();

  protected:
  
    /**
     * Performs the SOAP initialization and the bind here
     */
    virtual void bind() throw (castor::exception::Exception);
    
    /**
     * Listening loop implementation for this Listener, based on SOAP sockets
     */
    virtual void listenLoop();

    /**
     * Terminates the work to be done when the thread pool is exhausted,
     * by simply closing the connection to the client.
     * @param param user parameter that would have been passed to a thread
     */
    virtual void terminate(void* param) throw();
    
  private:

    /// SOAP structure to handle the SOAP socket
    struct soap* m_soap;

  };

 } // end of namespace server

} } /* end of namespace castor/srm */

#endif // SRM_SERVER_SOAPLISTENERTHREADPOOL_HPP
