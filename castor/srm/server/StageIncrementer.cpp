/******************************************************************************
 *                      StageIncrementer.cpp
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 *
 *
 * @author Shaun De Witt
 *****************************************************************************/

#include <castor/log/log.hpp>
#include <castor/srm/SrmConstants.hpp>
#include <castor/srm/server/StageIncrementer.hpp>

unsigned castor::srm::server::StageIncrementer::s_castorCalls = 0;
pthread_mutex_t castor::srm::server::StageIncrementer::s_mutex = PTHREAD_MUTEX_INITIALIZER;

// 
// dtor
//
castor::srm::server::StageIncrementer::~StageIncrementer() {
    if(m_doneIncr) {
        pthread_mutex_lock(&s_mutex);
        if(s_castorCalls > 0) {  // sanity check
          s_castorCalls--;
        }
        pthread_mutex_unlock(&s_mutex);
    }
}

//
// increment
//
void castor::srm::server::StageIncrementer::increment() throw (castor::srm::SrmException) {
    unsigned maxCastor = castor::srm::SrmConstants::getInstance()->maxCastorThreads();
    if(m_doneIncr) {
      // sanity check
      return;
    }
    if (s_castorCalls < maxCastor ) {
        pthread_mutex_lock(&s_mutex);
        if ( s_castorCalls < maxCastor ) {
            s_castorCalls++;
            m_doneIncr = true;
            std::list<castor::log::Param> p = {
                castor::log::Param("Castor Threads", s_castorCalls),
            };
            castor::log::write(LOG_DEBUG, "New Request Arrival", p);
        }
        pthread_mutex_unlock(&s_mutex);
    }
    if (!m_doneIncr) {
        castor::srm::SrmException x(srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR);
        x.getMessage() << "Too many threads busy with CASTOR" << std::ends;
        throw x;
    }
}
