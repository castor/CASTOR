#include <cgsi_plugin.h>

#include <castor/BaseAddress.hpp>
#include <castor/BaseObject.hpp>
#include <castor/Constants.hpp>
#include <castor/Services.hpp>

#include <stager_client_api.h>

#include <castor/exception/Exception.hpp>

#include "soapH.h"

#include "castor/srm/SURL.hpp"
#include "castor/srm/server/SrmUtils.hpp"
#include "castor/srm/server/SrmLogger.hpp"
#include "castor/srm/Constants.hpp"
#include "castor/srm/RequestType.hpp"
#include "castor/srm/ISrmSvc.hpp"
#include "castor/srm/SrmUser.hpp"
#include "castor/srm/SrmConstants.hpp"
#include "castor/srm/StageRequest.hpp"
#include "castor/srm/SubRequest.hpp"
#include "castor/srm/SubRequestStatus.hpp"
#include "castor/srm/UserFile.hpp"
#include "castor/srm/server/ResponseWriter.hpp"
#include "castor/srm/server/RequestValidator.hpp"

int srm__srmStatusOfBringOnlineRequest (
        struct soap *soap,
        srm__srmStatusOfBringOnlineRequestRequest   *myRequest,
        srm__srmStatusOfBringOnlineRequestResponse_ &myResponse)
{
    castor::srm::server::SrmLogger *myLogger = new castor::srm::server::SrmLogger();
    myLogger->LOGENTRY(soap);

    castor::srm::server::ResponseWriter myWriter;
    castor::srm::server::SrmUtils *utils = new castor::srm::server::SrmUtils();
    utils->map_user(soap, NULL, NULL);

    srm__srmStatusOfBringOnlineRequestResponse *resp;
    try {
        resp =
            soap_new_srm__srmStatusOfBringOnlineRequestResponse(soap,-1);
        myResponse.srmStatusOfBringOnlineRequestResponse = resp;

        resp->remainingTotalRequestTime  = NULL;
        resp->returnStatus               = soap_new_srm__TReturnStatus(soap, -1);
        resp->returnStatus->statusCode   = srm__TStatusCode__SRM_USCORESUCCESS;
        resp->returnStatus->explanation  = soap_new_std__string(soap, -1);
        resp->returnStatus->explanation->assign("");
        resp->arrayOfFileStatuses = soap_new_srm__ArrayOfTBringOnlineRequestFileStatus(soap, -1);
        resp->remainingDeferredStartTime = NULL;
    }
    catch (...) {
        std::list<castor::log::Param> params = {
            castor::log::Param("REQID", myLogger->getId()),
            castor::log::Param("Message", "Out of memory")
        };
        castor::log::write(LOG_ALERT, "Exception caught", params);
        utils->unmap_user();
        delete utils;
        myLogger->LOGEXIT(srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR, "Out of memory");
        myWriter.write(myResponse);
        delete myLogger;
        return SOAP_EOM;
    }

    // Set up database...
    castor::Services* svcs;
    castor::srm::ISrmSvc* srmSvc;
    castor::BaseAddress ad;
    try {
        svcs = castor::BaseObject::services();
        ad.setCnvSvcName("DbCnvSvc");
        ad.setCnvSvcType(castor::SVC_DBCNV);
        castor::IService *svc = castor::BaseObject::services()->service("DbSrmSvc", castor::SVC_DBSRMSVC);
        srmSvc = dynamic_cast<castor::srm::ISrmSvc*>(svc);
        if ( srmSvc == 0 ) {
          std::list<castor::log::Param> params = {castor::log::Param("REQID", myLogger->getId())};
          castor::log::write(LOG_ERR, "Could not get Conversion Service for Database", params);
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
            resp->returnStatus->explanation->assign("Unable to establish database service");
            utils->unmap_user();
            delete utils;
            myLogger->LOGEXIT(resp->returnStatus->statusCode,  resp->returnStatus->explanation->c_str());
            myWriter.write(myResponse);
            delete myLogger;
            return SOAP_OK;
        }
    }
    catch (castor::exception::Exception e) {
        myLogger->LOGEXCEPTION(&e, &ad);
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        resp->returnStatus->explanation->assign( e.getMessage().str());
        utils->unmap_user();
        delete utils;
        myLogger->LOGEXIT(resp->returnStatus->statusCode,  resp->returnStatus->explanation->c_str());
        myWriter.write(myResponse);
        delete myLogger;
        return SOAP_OK;
    }

    std::string dn, uname, vo;
    std::string stagerHost;
    castor::srm::StageRequest* original = 0;
    castor::srm::server::RequestValidator validate;
    try {
        dn    = utils->getDn(soap);
        uname = utils->getUserName(soap);
        vo    = utils->getVoName(soap);
        stagerHost = utils->getStagerHost(soap);
        validate.requestToken(myRequest->requestToken);
        original = srmSvc->getStageRequest(myRequest->requestToken);
        validate.request(original, (int)castor::srm::REQUESTTYPE_BOL);
        myLogger->setId(original->srmRequestId());
    } catch (castor::exception::Exception &e) {
        castor::srm::SrmException* se = dynamic_cast<castor::srm::SrmException*>(&e);
        if(se) {
            resp->returnStatus->statusCode = (srm__TStatusCode)se->code();
        } else {
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        }
        resp->returnStatus->explanation->assign(e.getMessage().str());
        utils->unmap_user();
        myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
        myWriter.write(myResponse);
        if(original) delete original;
        delete utils;
        delete myLogger;
        return SOAP_OK;
    }

    //
    // If we have a std::list of surls, create template return
    // statsses for each request
    //
    srm__ArrayOfAnyURI* surls = myRequest->arrayOfSourceSURLs;
    bool explicit_surls = (surls != NULL && surls->urlArray.size()>0) ? true : false;
    if(explicit_surls) {
        for(unsigned int i = 0; i < surls->urlArray.size(); i++) {
            // Create a file response structure and initialize
            // its status to invalid.  When we loop over the
            // subrequests in the original request then we will
            // update this status.  In this way any SURL's not in
            // the original request will remain invalid.
            srm__TBringOnlineRequestFileStatus* requestStatus = 
                soap_new_srm__TBringOnlineRequestFileStatus(soap, -1);
            requestStatus->status = soap_new_srm__TReturnStatus(soap, -1);
            requestStatus->status->explanation  = soap_new_std__string(soap, -1);
            requestStatus->sourceSURL.assign(surls->urlArray[i]);
            requestStatus->status->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
            requestStatus->status->explanation->assign("SURL not in original request");
            requestStatus->fileSize=NULL;
            requestStatus->estimatedWaitTime=NULL;
            requestStatus->remainingPinTime=NULL;
            resp->arrayOfFileStatuses->statusArray.push_back(requestStatus);
        }
    }

    try {

        // take a lock on the request before going on
        srmSvc->lockStageRequest(original->id());

        svcs->fillObj(&ad, original, castor::srm::OBJ_SrmUser, false);
        svcs->fillObj(&ad, original, castor::srm::OBJ_SubRequest, false);
        //unsigned loopCount = (explicit_surls) ? surls->urlArray.size():original->subRequests().size();
        unsigned loopCount  = original->subRequests().size();
        unsigned failed     = 0;
        unsigned aborted    = 0;
        unsigned success    = 0;
        unsigned pending    = 0;
        unsigned inProgress = 0;
        unsigned timedout   = 0;

        // Just in case we need then...
        castor::srm::SrmUser *thisUser       = srmSvc->getSrmUser(uname, vo, dn);

        Cns_fileid nsId;
        nsId.fileid = 0;
        strncpy(nsId.server, castor::srm::SrmConstants::getInstance()->nsHost().c_str(), sizeof(nsId.server)-1);
        castor::srm::SubRequest *thisSubRequest = 0;
        // Loop over all subrequests
        for ( unsigned i=0; i<original->subRequests().size(); i++ ) {
            thisSubRequest = original->subRequests()[i];
            srm__TBringOnlineRequestFileStatus* requestStatus=0;
            // Record status here before generating SURL status since the overall
            // response code mist refer to the whole of the original request, not just
            // any subset of files passed in
            if (thisSubRequest->status() == castor::srm::SUBREQUEST_PENDING) 
                pending++;
            else if (thisSubRequest->status() == castor::srm::SUBREQUEST_INPROGRESS)
                inProgress++;
            else if (thisSubRequest->status() == castor::srm::SUBREQUEST_SUCCESS ||
                     thisSubRequest->status() == castor::srm::SUBREQUEST_RELEASED)
                success++;
            else if (thisSubRequest->status() == castor::srm::SUBREQUEST_ABORTED)
                aborted++;
            else  // All other cases assumed failure
                failed++;

            if ( explicit_surls ) {
                // Find the surl in the std::list
                for (unsigned c=0; c<resp->arrayOfFileStatuses->statusArray.size(); c++) {
                    if (thisSubRequest->surl() == resp->arrayOfFileStatuses->statusArray[c]->sourceSURL) {
                        requestStatus = resp->arrayOfFileStatuses->statusArray[c];
                        requestStatus->status->statusCode = srm__TStatusCode__SRM_USCOREFILE_USCOREIN_USCORECACHE;
                        requestStatus->status->explanation->assign("");
                        break;
                    }
                }
            }
            else { 
                // Create a new structure
                requestStatus = soap_new_srm__TBringOnlineRequestFileStatus(soap, -1);
                requestStatus->status = soap_new_srm__TReturnStatus(soap, -1);
                requestStatus->status->explanation  = soap_new_std__string(soap, -1);
                requestStatus->sourceSURL.assign(thisSubRequest->surl());
                requestStatus->status->statusCode = srm__TStatusCode__SRM_USCOREFILE_USCOREIN_USCORECACHE;
                requestStatus->status->explanation->assign("");
                requestStatus->fileSize=NULL;
                requestStatus->estimatedWaitTime=NULL;
                requestStatus->remainingPinTime=NULL;
                resp->arrayOfFileStatuses->statusArray.push_back(requestStatus);
            }
            if (!requestStatus) 
                continue;

            // Now check status of subrequest....
            switch ( (int)thisSubRequest->status() ) {
                case castor::srm::SUBREQUEST_PENDING:
                case castor::srm::SUBREQUEST_INPROGRESS:
                    requestStatus->status->statusCode = (thisSubRequest->status() == castor::srm::SUBREQUEST_PENDING ?
                    srm__TStatusCode__SRM_USCOREREQUEST_USCOREQUEUED : srm__TStatusCode__SRM_USCOREREQUEST_USCOREINPROGRESS);
                    requestStatus->estimatedWaitTime = (int*)soap_malloc(soap, sizeof(int));
                    if ( requestStatus->estimatedWaitTime ) {
                        if ( original->nextCheck() == 0 ) {
                            // too early yet, please come back in at least the same time has already passed since request submission
                            *(requestStatus->estimatedWaitTime) = time(NULL) - original->creationTime();
                        }
                        else {
                            // polling is going on, no news before nextCheck, please come at least just after that
                            *(requestStatus->estimatedWaitTime) = original->nextCheck() - time(NULL) + 1;
                        }
                    }
                    break;
                case castor::srm::SUBREQUEST_SUCCESS:
                case castor::srm::SUBREQUEST_RELEASED:
                    // strictly speaking, RELEASED should not apply to bringOnline requests because
                    // no pin was involved; anyway we don't make any difference here and return SUCCESS.
                    requestStatus->fileSize = (ULONG64*)soap_malloc(soap, sizeof(ULONG64));
                    *(requestStatus->fileSize) = thisSubRequest->reservedSize();
                    requestStatus->remainingPinTime = (int*)soap_malloc(soap, sizeof(int));
                    *(requestStatus->remainingPinTime) = 0;
                    requestStatus->status->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
                    break;

                case castor::srm::SUBREQUEST_FAILED:
                    switch ( thisSubRequest->errorCode() ) {
                        case EBUSY:
                            requestStatus->status->statusCode = srm__TStatusCode__SRM_USCOREFILE_USCOREBUSY;
                            break;
                        case EAGAIN:
                            requestStatus->status->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
                            break;
                        case EEXIST:
                            requestStatus->status->statusCode = srm__TStatusCode__SRM_USCOREDUPLICATION_USCOREERROR;
                            break;
                        case ENOENT:
                            requestStatus->status->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREPATH;
                            break;
                        case ETIMEDOUT:
                            timedout++;
                            requestStatus->status->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
                            break;
                        case ESTTAPEOFFLINE:
                        case ESTNOTAVAIL:
                        case ESTNOSEGFOUND:
                            requestStatus->status->statusCode = srm__TStatusCode__SRM_USCOREFILE_USCOREUNAVAILABLE;
                            break;
                        default:
                            requestStatus->status->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
                    }
                    requestStatus->status->explanation->assign(thisSubRequest->reason());
                    break;

                case castor::srm::SUBREQUEST_ABORTED:
                    requestStatus->status->statusCode = srm__TStatusCode__SRM_USCOREABORTED;
                    requestStatus->status->explanation->assign("File request aborted");
                    break;

                default:
                    requestStatus->status->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
                    requestStatus->status->explanation->assign("Unknown status code in database - unable to ascertain status");
                    break;
            }
        }
        delete thisUser;
        svcs->commit(&ad);

        if ( failed == loopCount ) {
            // All subrequests failed
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
            resp->returnStatus->explanation->assign("No subrequests succeeded");
        }
        else if ( aborted == loopCount ) {
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREABORTED;
            resp->returnStatus->explanation->assign("All subrequests aborted");
        }
        else if ( pending == loopCount && original->status() == castor::srm::REQUEST_PENDING ) {
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREREQUEST_USCOREQUEUED;
            resp->returnStatus->explanation->assign("");
        }
        else if ( success == loopCount ) {
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
            resp->returnStatus->explanation->assign("");
        }
        else if ( pending > 0 || inProgress > 0 ) {
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREREQUEST_USCOREINPROGRESS;
            std::ostringstream ss;
            ss << (loopCount-pending-inProgress) << " out of " << loopCount << " subrequests completed";
            resp->returnStatus->explanation->assign(ss.str());
        }
        else if ( timedout > 0 ) {
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREREQUEST_USCORETIMED_USCOREOUT;
            std::ostringstream ss;
            ss << timedout << " out of " << loopCount << " subrequests timed out";
            resp->returnStatus->explanation->assign(ss.str());
        }
        else {
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREPARTIAL_USCORESUCCESS;
            std::ostringstream ss;
            ss << success << " out of " << loopCount << " subrequests succeeded";
            resp->returnStatus->explanation->assign(ss.str());
        }
    }
    catch (castor::exception::Exception e) {
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        resp->returnStatus->explanation->assign(e.getMessage().str());
        myLogger->LOGEXCEPTION(&e, &ad);
    }
    catch (...) {
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        resp->returnStatus->explanation->assign("Unknown exception in srmStatusOfBringOnlineRequest");
        myLogger->LOGEXCEPTION(0, &ad);
    }
    for (unsigned c=0;c<original->subRequests().size(); c++)
        delete original->subRequests()[c];
    delete original->srmUser();
    delete original;

    utils->unmap_user();
    delete utils;
    myLogger->LOGEXIT(resp->returnStatus->statusCode,  resp->returnStatus->explanation->c_str());
    myWriter.write(myResponse);
    delete myLogger;
    return SOAP_OK;
}
