#ifndef SRM_SERVER_SRMFILETYPES_HPP
#define SRM_SERVER_SRMFILETYPES_HPP

namespace castor {
 namespace srm {
    namespace server {
        static enum srm__TFileType ftypes[] = {
            srm__TFileType__FILE_,
            srm__TFileType__DIRECTORY,
            srm__TFileType__LINK
        };
    }
}
#endif
