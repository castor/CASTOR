#include <castor/log/log.hpp>

#include "soapH.h"

#include "castor/srm/SrmConstants.hpp"
#include "castor/srm/server/SrmLogger.hpp"

#include "ResponseWriter.hpp"

int srm__srmGetTransferProtocols (
        struct soap *soap,
        srm__srmGetTransferProtocolsRequest*,
        srm__srmGetTransferProtocolsResponse_  &myResponse)
{
    castor::srm::server::SrmLogger *myLogger = new castor::srm::server::SrmLogger();
    myLogger->LOGENTRY(soap);

    srm__srmGetTransferProtocolsResponse *resp;
    try {
        resp = soap_new_srm__srmGetTransferProtocolsResponse(soap, -1);

        resp->returnStatus = soap_new_srm__TReturnStatus(soap, -1);
        resp->returnStatus->explanation = soap_new_std__string(soap, -1);
        resp->protocolInfo = soap_new_srm__ArrayOfTSupportedTransferProtocol(soap, -1);

        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
        resp->returnStatus->explanation->assign("");
    }
    catch (...) {
        myLogger->LOGEXIT(srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR, "Out of memory");
        delete myLogger;
        return SOAP_EOM;
    }
    myResponse.srmGetTransferProtocolsResponse = resp;
    
    castor::srm::server::ResponseWriter myWriter;

    std::vector<std::string> protocols = castor::srm::SrmConstants::getInstance()->supportedProtocols();
    if (protocols.size() == 0 ) {
        std::list<castor::log::Param> foo = {
            castor::log::Param("REQID", myLogger->getId()),
            castor::log::Param("Message", "No protocols defined"),
        };
        castor::log::write(LOG_ERR, "Configuration error", foo);
    }

    try {
        for ( unsigned i=0; i<protocols.size(); i++ ) {
            srm__TSupportedTransferProtocol* myProtocol = soap_new_srm__TSupportedTransferProtocol(soap, -1);

            myProtocol->transferProtocol.assign(protocols[i]);
            myProtocol->attributes = NULL;
            resp->protocolInfo->protocolArray.push_back(myProtocol);
        }
    }
    catch (...) {
        myLogger->LOGEXIT(srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR, "Out of memory");
        delete myLogger;
        return SOAP_EOM;
    }
    myWriter.write(myResponse);
    myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
    delete myLogger;
    return SOAP_OK;
    
}

