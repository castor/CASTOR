#include <map>
#include <pwd.h>
#include <grp.h>
#include <sys/types.h>

#include <Cns_api.h>
#include <castor/log/log.hpp>
#include <castor/exception/Exception.hpp>

#include "soapH.h"
#include "castor/srm/SURL.hpp"
#include "castor/srm/server/SrmUtils.hpp"
#include "castor/srm/server/SrmLogger.hpp"
#include "castor/srm/server/RequestValidator.hpp"
#include "castor/srm/SrmConstants.hpp"

#include "SrmPermissionTypes.hpp"
#include "ResponseWriter.hpp"

int srm__srmGetPermission (
        struct soap *soap,
        srm__srmGetPermissionRequest    *myRequest,
        srm__srmGetPermissionResponse_  &myResponse)
{
    castor::srm::server::SrmLogger *myLogger = new castor::srm::server::SrmLogger();
    myLogger->LOGENTRY(soap);
    castor::srm::server::SrmUtils *utils = new castor::srm::server::SrmUtils();

    // Construct response
    srm__srmGetPermissionResponse *resp;
    try {
        resp = soap_new_srm__srmGetPermissionResponse(soap, -1);
        resp->returnStatus = soap_new_srm__TReturnStatus (soap, -1);
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
        resp->returnStatus->explanation = soap_new_std__string(soap, -1);
        resp->returnStatus->explanation->assign("");
        resp->arrayOfPermissionReturns = NULL;
    }
    catch (...){
        myLogger->LOGEXIT(srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR, "Out of memory");
        delete utils;
        delete myLogger;
        return SOAP_EOM;
    }
    myResponse.srmGetPermissionResponse = resp;

    castor::srm::server::ResponseWriter myWriter;
    castor::srm::server::RequestValidator      validate;

    try {
        validate.notNull(myRequest->arrayOfSURLs, "arrayOfSURLs");
        validate.arrayOfSurls(myRequest->arrayOfSURLs->urlArray);
    } catch (castor::exception::Exception &e) {
        castor::srm::SrmException* se = dynamic_cast<castor::srm::SrmException*>(&e);
        if(se) {
            resp->returnStatus->statusCode = (srm__TStatusCode)se->code();
        } else {
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        }
        resp->returnStatus->explanation->assign(e.getMessage().str());
        myWriter.write(myResponse);
        myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
        delete utils;
        delete myLogger;
        return SOAP_OK;
    }

    // Switch to the current user
    uid_t myUID;
    gid_t myGID;
    utils->map_user(soap, &myUID, &myGID);
    utils->unmap_user();

    unsigned nOK = 0;
    // If we make it here, we can allocate memory for return
    try {
        resp->arrayOfPermissionReturns = soap_new_srm__ArrayOfTPermissionReturn(soap, -1);
        std::vector<xsd__anyURI> surls = myRequest->arrayOfSURLs->urlArray;

        // Prepare Cns_filestat buffer
        Cns_filestat buf;
        Cns_fileid   nsId;
        nsId.fileid=0;
        strncpy(nsId.server, castor::srm::SrmConstants::getInstance()->nsHost().c_str(), sizeof(nsId.server)-1);

        for ( unsigned i=0; i<surls.size(); i++ ) {

            srm__TPermissionReturn* thisReturn = soap_new_srm__TPermissionReturn(soap, -1);

            thisReturn->surl.assign(surls[i]);
            thisReturn->status                  = soap_new_srm__TReturnStatus(soap, -1);
            thisReturn->status->explanation     = soap_new_std__string(soap, -1);
            thisReturn->owner                   = soap_new_std__string(soap, -1);
            thisReturn->ownerPermission         = NULL;
            thisReturn->arrayOfUserPermissions  = NULL;
            thisReturn->arrayOfGroupPermissions = NULL;
            thisReturn->otherPermission         = NULL;

            thisReturn->status->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
            thisReturn->status->explanation->assign("");

            // get castor file name
            std::string cfn;
            try {
                castor::srm::SURL* mySURL = validate.surl(surls[i]); 
                cfn = mySURL->getFileName();
                delete mySURL;
            } catch(castor::exception::Exception &e) {
                thisReturn->status->statusCode = (srm__TStatusCode)e.code();;
                thisReturn->status->explanation->assign(e.getMessage().str());
                resp->arrayOfPermissionReturns->permissionArray.push_back(thisReturn);
                continue;
            }

            // See if se can stat the current file
            myLogger->logCnsCall("Cns_stat", cfn);
            Cns_setid(myUID, myGID);
            int rtn = Cns_stat(cfn.c_str(), &buf );
            Cns_unsetid();
            myLogger->logCnsReturn("Cns_stat", rtn);
            if ( rtn != 0 ) {
                switch(serrno) {
                    case ENOENT:
                    case ENOTDIR:
                    case ENAMETOOLONG:
                        thisReturn->status->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREPATH;
                        break;
                    case EACCES:
                        thisReturn->status->statusCode = srm__TStatusCode__SRM_USCOREAUTHORIZATION_USCOREFAILURE;
                        break;
                    default:
                        thisReturn->status->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
                }
                thisReturn->status->explanation->assign(sstrerror(serrno));
                resp->arrayOfPermissionReturns->permissionArray.push_back(thisReturn);
                continue;
            }
            nsId.fileid = buf.fileid;
            myLogger->LOGFILEREQ(&nsId, cfn);
            // Get the owner and their permission
            thisReturn->owner->assign(getpwuid(buf.uid)->pw_name);
            bool readP = buf.filemode & S_IRUSR;
            bool writP = buf.filemode & S_IWUSR;
            bool execP = buf.filemode & S_IXUSR;
            if ( !readP && !writP && !execP ) {
                thisReturn->ownerPermission = &castor::srm::server::ptypes[0];
            }
            else if ( !readP && !writP && execP ) {
                thisReturn->ownerPermission = &castor::srm::server::ptypes[1];
            }
            else if ( !readP && writP && !execP ) {
                thisReturn->ownerPermission = &castor::srm::server::ptypes[2];
            }
            else if ( !readP && writP && execP ) {
                thisReturn->ownerPermission = &castor::srm::server::ptypes[3];
            }
            else if ( readP && !writP && !execP ) {
                thisReturn->ownerPermission = &castor::srm::server::ptypes[4];
            }
            else if ( readP && !writP && execP ) {
                thisReturn->ownerPermission = &castor::srm::server::ptypes[5];
            }
            else if ( readP && writP &&  !execP) {
                thisReturn->ownerPermission = &castor::srm::server::ptypes[6];
            } 
            else {
                thisReturn->ownerPermission = &castor::srm::server::ptypes[7];
            }

            // Other permission
            readP = buf.filemode & S_IROTH;
            writP = buf.filemode & S_IWOTH;
            execP = buf.filemode & S_IXOTH;
            if ( !readP && !writP && !execP ) {
                thisReturn->otherPermission = &castor::srm::server::ptypes[0];
            }
            else if ( !readP && !writP && execP ) {
                thisReturn->otherPermission = &castor::srm::server::ptypes[1];
            }
            else if ( !readP && writP && !execP ) {
                thisReturn->otherPermission = &castor::srm::server::ptypes[2];
            }
            else if ( !readP && writP && execP ) {
                thisReturn->otherPermission = &castor::srm::server::ptypes[3];
            }
            else if ( readP && !writP && !execP ) {
                thisReturn->otherPermission = &castor::srm::server::ptypes[4];
            }
            else if ( readP && !writP && execP ) {
                thisReturn->otherPermission = &castor::srm::server::ptypes[5];
            }
            else if ( readP && writP &&  !execP) {
                thisReturn->otherPermission = &castor::srm::server::ptypes[6];
            } 
            else {
                thisReturn->otherPermission = &castor::srm::server::ptypes[7];
            }

            // Prepare Cns_acl structures
            Cns_acl *currPermissions = NULL;
            int nCurrentPermissions = CA_MAXACLENTRIES;
            currPermissions = (Cns_acl*)malloc(nCurrentPermissions*sizeof(Cns_acl));

            myLogger->logCnsCall("Cns_getacl", cfn, &nsId, LOG_INFO);
            nCurrentPermissions = Cns_getacl(cfn.c_str(), nCurrentPermissions, currPermissions);
            myLogger->logCnsReturn("Cns_getacl", nCurrentPermissions, &nsId);
            if ( nCurrentPermissions == -1 ) {
                thisReturn->status->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
                if ( serrno > 0 ) {
                    thisReturn->status->explanation->assign(sstrerror(serrno));
                }
                else {
                    thisReturn->status->explanation->assign("Unexpected error from Cns_getacl");
                }
                resp->arrayOfPermissionReturns->permissionArray.push_back(thisReturn);
                continue;
            }
            std::map<int, Cns_acl> userIds;
            std::map<int, Cns_acl> groupIds;
            for ( int i=0; i<nCurrentPermissions; i++) {
                if ( currPermissions[i].a_type == (CNS_ACL_DEFAULT & CNS_ACL_MASK) &&
                     (currPermissions[i].a_type == /*CNS_ACL_DEFAULT &*/ CNS_ACL_USER ||
                      currPermissions[i].a_type == /*CNS_ACL_DEFAULT &*/ CNS_ACL_USER_OBJ) ) {
                    userIds[currPermissions[i].a_id] = currPermissions[i];
                }
                if ( currPermissions[i].a_type == /*CNS_ACL_DEFAULT &*/ CNS_ACL_GROUP ||
                     currPermissions[i].a_type == /*CNS_ACL_DEFAULT &*/ CNS_ACL_GROUP_OBJ ) {
                    groupIds[currPermissions[i].a_id] = currPermissions[i];
                }
            }
            free (currPermissions);
            if ( userIds.size() > 0 ) {
                thisReturn->arrayOfUserPermissions = soap_new_srm__ArrayOfTUserPermission(soap, -1);

                std::map<int, Cns_acl>::iterator iter = userIds.begin();
                while ( iter != userIds.end() ) {
                    srm__TUserPermission *thisPermission = soap_new_srm__TUserPermission(soap, -1);
                    struct passwd *pw_st = getpwuid( (*iter).first );
                    if ( pw_st == NULL ) {
                        std::list<castor::log::Param> p = {
                            castor::log::Param("NSFILEID", nsId.fileid),
                            castor::log::Param("REQID", myLogger->getId()),
                            castor::log::Param ("Message", "getpwuid failed"),
                            castor::log::Param ("uid", (*iter).first)
                        };
                        castor::log::write(LOG_WARNING, "Subrequest status", p);
                        continue;
                    }
                    thisPermission->userID.assign( pw_st->pw_name );

                    bool readP = (*iter).second.a_perm & S_IROTH;
                    bool writP = (*iter).second.a_perm & S_IWOTH;
                    bool execP = (*iter).second.a_perm & S_IXOTH;
                    if ( !readP && !writP && !execP ) {
                        thisPermission->mode = castor::srm::server::ptypes[0];
                    }
                    else if ( !readP && !writP && execP ) {
                        thisPermission->mode = castor::srm::server::ptypes[1];
                    }
                    else if ( !readP && writP && !execP ) {
                        thisPermission->mode = castor::srm::server::ptypes[2];
                    }
                    else if ( !readP && writP && execP ) {
                        thisPermission->mode = castor::srm::server::ptypes[3];
                    }
                    else if ( readP && !writP && !execP ) {
                        thisPermission->mode = castor::srm::server::ptypes[4];
                    }
                    else if ( readP && !writP && execP ) {
                        thisPermission->mode = castor::srm::server::ptypes[5];
                    }
                    else if ( readP && writP &&  !execP) {
                        thisPermission->mode = castor::srm::server::ptypes[6];
                    } 
                    else {
                        thisPermission->mode = castor::srm::server::ptypes[7];
                    }
                    iter++;
                    thisReturn->arrayOfUserPermissions->userPermissionArray.push_back(thisPermission);
                }
            }
            thisReturn->arrayOfGroupPermissions = soap_new_srm__ArrayOfTGroupPermission(soap, -1);
            if ( groupIds.size() > 0 ) {
                std::map<int, Cns_acl>::iterator iter = groupIds.begin();
                while ( iter != groupIds.end() ) {
                    srm__TGroupPermission *thisPermission = soap_new_srm__TGroupPermission(soap, -1);
                    thisPermission->groupID.assign( getgrgid( (*iter).first )->gr_name );

                    bool readP = (*iter).second.a_perm & S_IROTH;
                    bool writP = (*iter).second.a_perm & S_IWOTH;
                    bool execP = (*iter).second.a_perm & S_IXOTH;
                    if ( !readP && !writP && !execP ) {
                        thisPermission->mode = castor::srm::server::ptypes[0];
                    }
                    else if ( !readP && !writP && execP ) {
                        thisPermission->mode = castor::srm::server::ptypes[1];
                    }
                    else if ( !readP && writP && !execP ) {
                        thisPermission->mode = castor::srm::server::ptypes[2];
                    }
                    else if ( !readP && writP && execP ) {
                        thisPermission->mode = castor::srm::server::ptypes[3];
                    }
                    else if ( readP && !writP && !execP ) {
                        thisPermission->mode = castor::srm::server::ptypes[4];
                    }
                    else if ( readP && !writP && execP ) {
                        thisPermission->mode = castor::srm::server::ptypes[5];
                    }
                    else if ( readP && writP &&  !execP) {
                        thisPermission->mode = castor::srm::server::ptypes[6];
                    } 
                    else {
                        thisPermission->mode = castor::srm::server::ptypes[7];
                    }
                    iter++;
                    thisReturn->arrayOfGroupPermissions->groupPermissionArray.push_back(thisPermission);
                }
            }
            else {
                srm__TGroupPermission *thisPermission = soap_new_srm__TGroupPermission(soap, -1);
                thisPermission->groupID.assign( getgrgid(buf.gid)->gr_name );
                bool readP = buf.filemode & S_IRGRP;
                bool writP = buf.filemode & S_IWGRP;
                bool execP = buf.filemode & S_IXGRP;
                if ( !readP && !writP && !execP ) {
                    thisPermission->mode = castor::srm::server::ptypes[0];
                }
                else if ( !readP && !writP && execP ) {
                    thisPermission->mode = castor::srm::server::ptypes[1];
                }
                else if ( !readP && writP && !execP ) {
                    thisPermission->mode = castor::srm::server::ptypes[2];
                }
                else if ( !readP && writP && execP ) {
                    thisPermission->mode = castor::srm::server::ptypes[3];
                }
                else if ( readP && !writP && !execP ) {
                    thisPermission->mode = castor::srm::server::ptypes[4];
                }
                else if ( readP && !writP && execP ) {
                    thisPermission->mode = castor::srm::server::ptypes[5];
                }
                else if ( readP && writP &&  !execP) {
                    thisPermission->mode = castor::srm::server::ptypes[6];
                } 
                else {
                    thisPermission->mode = castor::srm::server::ptypes[7];
                }
                thisReturn->arrayOfGroupPermissions->groupPermissionArray.push_back(thisPermission);
            }
            resp->arrayOfPermissionReturns->permissionArray.push_back(thisReturn);
            nOK++;
        }
    }
    catch (...) {
        myLogger->LOGEXCEPTION(0, 0);
    }
    if (nOK == 0) {
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
        resp->returnStatus->explanation->assign("No subrequests succeeded.");
    }
    else if (nOK != myRequest->arrayOfSURLs->urlArray.size()) {
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREPARTIAL_USCORESUCCESS;
    }
    myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
    delete utils;
    delete myLogger;
    myWriter.write(myResponse);
    return SOAP_OK;
}

