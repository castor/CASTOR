/******************************************************************************
 *                      SrmServer.hpp
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2004  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Server part of the SRM software suite.
 * Handles all requests and pushes the asynchronous ones to the DB
 * for further processing by the Daemon part.
 *
 * @author castor-dev team
 *****************************************************************************/

#ifndef SRM_SRMSERVER_HPP
#define SRM_SRMSERVER_HPP

#include "soapH.h"
#include <iostream>
#include "castor/server/MultiThreadedDaemon.hpp"

namespace castor {
 namespace srm {

 namespace server {

  /**
   * CASTOR SRM2 frontend daemon.
   */
  class SrmServer : public castor::server::MultiThreadedDaemon {

  public:

    /**
     * Constructor
     * @param stdOut Stream representing standard out.
     * @param stdErr Stream representing standard error.
     */
    SrmServer(std::ostream &stdOut, std::ostream &stdErr);

    /**
     * Destructor
     */
    virtual ~SrmServer() throw () {};

    /**
     * Overrides parent argument parsing with specific SRM options
     */
    void parseCommandLine(int argc, char **argv);

  protected:

    virtual void help(std::string programName);

  };

 }

} }  // end of namespace castor/srm

#endif
