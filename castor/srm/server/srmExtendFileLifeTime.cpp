#include <cgsi_plugin.h>

#include <castor/BaseAddress.hpp>
#include <castor/BaseObject.hpp>
#include <castor/Constants.hpp>
#include <castor/Services.hpp>

#include <castor/exception/Exception.hpp>

#include "soapH.h"

#include "castor/srm/SURL.hpp"
#include "castor/srm/server/SrmLogger.hpp"
#include "castor/srm/server/RequestValidator.hpp"
#include "castor/srm/SrmConstants.hpp"
#include "castor/srm/Constants.hpp"
#include "castor/srm/ISrmSvc.hpp"
#include "castor/srm/StageRequest.hpp"
#include "castor/srm/SubRequest.hpp"
#include "castor/srm/SubRequestStatus.hpp"
#include "castor/srm/SrmUser.hpp"
#include "castor/srm/UserFile.hpp"

#include "ResponseWriter.hpp"

int srm__srmExtendFileLifeTime(
        struct soap *soap,
        srm__srmExtendFileLifeTimeRequest*  req,
        srm__srmExtendFileLifeTimeResponse_& response) //< response parameter
{

    castor::srm::server::SrmLogger *myLogger = new castor::srm::server::SrmLogger();
    myLogger->LOGENTRY(soap);
    castor::srm::server::ResponseWriter myWriter;
    castor::srm::server::RequestValidator      validate;

    // Allocate minimal response for now.
    srm__srmExtendFileLifeTimeResponse *out;
    try {
        out = soap_new_srm__srmExtendFileLifeTimeResponse(soap, -1);
        out->arrayOfFileStatuses = NULL;
        out->returnStatus = soap_new_srm__TReturnStatus(soap, -1);
        out->returnStatus->explanation = soap_new_std__string(soap, -1);

        out->returnStatus->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
        out->returnStatus->explanation->assign("");
    }
    catch (...) {
        std::list<castor::log::Param> params = {
            castor::log::Param("REQID", myLogger->getId()),
            castor::log::Param("Message", "Out of memory")
        };
        castor::log::write(LOG_ALERT, "Exception caught", params);
        myLogger->LOGEXIT(srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR, "Out of memory");
        delete myLogger;
        return SOAP_OK;
    }

    response.srmExtendFileLifeTimeResponse = out;

    // Set up database services.
    castor::Services* svcs;
    castor::IService* s;
    castor::BaseAddress ad;
    castor::srm::ISrmSvc *srmSvc;
    try {
        svcs = castor::BaseObject::services();
        s = svcs->service("DbSrmSvc", castor::SVC_DBSRMSVC);
        srmSvc = dynamic_cast<castor::srm::ISrmSvc*>(s);
        if ( srmSvc == 0 ) {
            out->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
            out->returnStatus->explanation->assign("Unable to establish database service");
            std::list<castor::log::Param> params = {castor::log::Param("REQID", myLogger->getId())};
            castor::log::write(LOG_ERR, "Could not get Conversion Service for Database", params);
            myWriter.write(response);
            myLogger->LOGEXIT(out->returnStatus->statusCode, out->returnStatus->explanation->c_str());
            delete myLogger;
            return SOAP_OK;
        }
        ad.setCnvSvcName("DbCnvSvc");
        ad.setCnvSvcType(castor::SVC_DBCNV);
    }
    catch (castor::exception::Exception e) {
        out->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        out->returnStatus->explanation->assign( e.getMessage().str());
        myLogger->LOGEXCEPTION(&e, 0);
        myLogger->LOGEXIT(out->returnStatus->statusCode, out->returnStatus->explanation->c_str());
        delete myLogger;
        myWriter.write(response);
        return SOAP_OK;
    }
    castor::srm::StageRequest *originalRequest = 0;
    try {
        bool nullTimeAllowed = true;
        validate.lifetime(req->newFileLifeTime, nullTimeAllowed);
        validate.lifetime(req->newPinLifeTime, nullTimeAllowed);
        validate.notNull(req->arrayOfSURLs, "arrayOfSURLs");
        validate.arrayOfSurls(req->arrayOfSURLs->urlArray);
        if (req->requestToken) {
            originalRequest = srmSvc->getStageRequest(req->requestToken->c_str());
            validate.request(originalRequest);
        }
    } catch (castor::exception::Exception &e) {
        castor::srm::SrmException* se = dynamic_cast<castor::srm::SrmException*>(&e);
        if(se) {
            out->returnStatus->statusCode = (srm__TStatusCode)se->code();
        } else {
            out->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        }
        out->returnStatus->explanation->assign(e.getMessage().str());
        myWriter.write(response);
        myLogger->LOGEXIT(out->returnStatus->statusCode, out->returnStatus->explanation->c_str());
        delete myLogger;
        return SOAP_OK;
    }

    // Special validations for this method:
    // Section 5.1.6.2(a): Can not have both pin and file lifetimes in request
    if ( req->newFileLifeTime != NULL && req->newPinLifeTime != NULL) {
        out->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST;
        out->returnStatus->explanation->assign("Pin and File lifetimes supplied");
    }
    // Section 5.1.6.2(c): If request token is supplied, only Pin lifetime is supported.
    if ( req->requestToken != NULL && req->newPinLifeTime == NULL ) {
        out->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST;
        out->returnStatus->explanation->assign("Request token supplied without a pin lifetime");
    }
    // Section 5.1.6.2(d): Invalid to supply both request token and file lifetime
    if (req->requestToken != NULL && req->newFileLifeTime != NULL) {
        out->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST;
        out->returnStatus->explanation->assign("Request token supplied with a file lifetime");
    }
    // If neither time supplied, this is also an error
    if ( req->newFileLifeTime == NULL && req->newPinLifeTime == NULL) {
        out->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST;
        out->returnStatus->explanation->assign("Neither Pin nor File lifetimes supplied");
    }
    if (out->returnStatus->statusCode != srm__TStatusCode__SRM_USCORESUCCESS) {
        myWriter.write(response);
        myLogger->LOGEXIT(out->returnStatus->statusCode, out->returnStatus->explanation->c_str());
        delete myLogger;
        if (originalRequest) delete(originalRequest);
        return SOAP_OK;
    }
    if (originalRequest) {
        myLogger->setId(originalRequest->srmRequestId());
        myLogger->LOGSTARTEDREQ(1);
    }
    try {
        // Allocate space for the return array
        out->arrayOfFileStatuses = soap_new_srm__ArrayOfTSURLLifetimeReturnStatus(soap, -1);

        if (originalRequest) {
            // lock the original request
            srmSvc->lockStageRequest(originalRequest->id());
            // Fill the subrequests 
            svcs->fillObj(&ad, originalRequest, castor::srm::OBJ_SubRequest, false);
        }

        unsigned nOK = 0;
        // Loop over all subrequests
        for ( unsigned i=0; i<req->arrayOfSURLs->urlArray.size(); i++ ) {
            // Allocate space for a return thingy
            srm__TSURLLifetimeReturnStatus* myReturn = soap_new_srm__TSURLLifetimeReturnStatus(soap, -1);
            myReturn->surl.assign ( req->arrayOfSURLs->urlArray[i] );
            myReturn->status = soap_new_srm__TReturnStatus(soap, -1);
            myReturn->status->explanation = soap_new_std__string(soap, -1);
            myReturn->status->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
            myReturn->status->explanation->assign("");
            // Only return pin and file lifetimes if valid request
            myReturn->fileLifetime = NULL;
            myReturn->pinLifetime  = NULL;


            if ( !originalRequest ) {
                // Extending surl - only support -1
                myReturn->status->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
                myReturn->status->explanation->assign("");
                myReturn->fileLifetime = (int*)soap_malloc(soap, sizeof(int));
                *(myReturn->fileLifetime) = -1;
                out->arrayOfFileStatuses->statusArray.push_back(myReturn);
                nOK++;
                continue;
            }
            
            // For pinning which is not supported in CASTOR, we check if
            // files are currently staged and if so, return a value of 0.
            // First we need to see if this file was in the subrequests
            castor::srm::SubRequest *thisSub = 0;
            for ( unsigned j=0; j<originalRequest->subRequests().size(); j++ ) {
                if ( myReturn->surl.find(originalRequest->subRequests()[j]->castorFileName()) != std::string::npos ) {
                    thisSub = originalRequest->subRequests()[j];
                    break;
                }
            }

            if ( thisSub == 0 ) {
                myReturn->status->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST;
                myReturn->status->explanation->assign("This SURL is not in original request");
                out->arrayOfFileStatuses->statusArray.push_back(myReturn);
                std::list<castor::log::Param> reqP = {
                    castor::log::Param("REQID", myLogger->getId()),
                    castor::log::Param("Message", myReturn->status->explanation->c_str()),
                    castor::log::Param("SURL", myReturn->surl)
                };
                castor::log::write(LOG_USER_ERROR, "Invalid Request", reqP);
                continue;
            }

            if ( originalRequest->requestType() == castor::srm::REQUESTTYPE_PUT ) {
                if (originalRequest->status() == castor::srm::REQUEST_DONE ) {
                    myReturn->status->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST;
                    myReturn->status->explanation->assign("putDone complete on this Put request");
                    out->arrayOfFileStatuses->statusArray.push_back(myReturn);
                    std::list<castor::log::Param> reqP = {
                        castor::log::Param("REQID", myLogger->getId()),
                        castor::log::Param("Message", myReturn->status->explanation->c_str()),
                        castor::log::Param("SURL", myReturn->surl),
                        castor::log::Param("Status", thisSub->status())
                    };
                    castor::log::write(LOG_USER_ERROR, "Invalid Request", reqP);
                }
                else {
                    // We extend the expiration interval on the subrequest - there is no
                    // real pinning here
                    time_t expiryTime = time(NULL) + *(req->newPinLifeTime);
                    try {
                        originalRequest->setExpirationInterval(expiryTime);
                        svcs->updateRep(&ad, originalRequest, true);
                        myReturn->pinLifetime = (int*)soap_malloc(soap, sizeof(int));
                        *(myReturn->pinLifetime) = *(req->newPinLifeTime);
                        nOK++;
                    }
                    catch (castor::exception::Exception x) {
                        myLogger->LOGEXCEPTION(&x, &ad);
                        myReturn->status->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
                        myReturn->status->explanation->assign(x.getMessage().str());
                    }
                    out->arrayOfFileStatuses->statusArray.push_back(myReturn);
                }
                continue;
            }
            // If this subrequest is in any way invalid we should also handle this
            if ( thisSub->status() != castor::srm::SUBREQUEST_SUCCESS &&
                 thisSub->status() != castor::srm::SUBREQUEST_RELEASED ) {
                myReturn->status->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST;
                myReturn->status->explanation->assign("This SURL is not in valid status");
                out->arrayOfFileStatuses->statusArray.push_back(myReturn);
                std::list<castor::log::Param> reqP = {
                    castor::log::Param("REQID", myLogger->getId()),
                    castor::log::Param("Message", myReturn->status->explanation->c_str()),
                    castor::log::Param("SURL", myReturn->surl),
                    castor::log::Param("Status", thisSub->status())
                };
                castor::log::write(LOG_USER_ERROR, "Invalid Request", reqP);
                continue;
            }
            // If the file is released....
            if (thisSub->status() == castor::srm::SUBREQUEST_RELEASED) {
                myReturn->status->statusCode = srm__TStatusCode__SRM_USCORERELEASED;
                myReturn->status->explanation->assign("SURL has already been released");
                out->arrayOfFileStatuses->statusArray.push_back(myReturn);
                continue;
            }
            // The standard case: because we don't support pinning, we default to LIFETIME_EXPIRED,
            // unless the corresponding (put) request was not completed, in which case we fail this request.
            if ( originalRequest->status() == castor::srm::REQUEST_DONE ) {
                myReturn->status->statusCode = srm__TStatusCode__SRM_USCOREFILE_USCORELIFETIME_USCOREEXPIRED;
                myReturn->status->explanation->assign("No pins left on this file");
            }
            else {
                myReturn->status->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
                myReturn->status->explanation->assign("putDone not complete");
            }
            out->arrayOfFileStatuses->statusArray.push_back(myReturn);
        }
        svcs->commit(&ad);
        if ( nOK == 0 ) {
            out->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
            out->returnStatus->explanation->assign("No lifetimes updated");
        }
        else if ( nOK == req->arrayOfSURLs->urlArray.size() ) {
            out->returnStatus->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
            out->returnStatus->explanation->assign("");
        }
        else {
            out->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREPARTIAL_USCORESUCCESS;
            out->returnStatus->explanation->assign("");
        }
        if ( originalRequest ) {
            for (unsigned c=0; c<originalRequest->subRequests().size(); c++)
                free (originalRequest->subRequests()[c]);
            free (originalRequest);
        }
    }
    catch (castor::exception::Exception e) {
        out->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        out->returnStatus->explanation->assign(e.getMessage().str());
        myLogger->LOGEXCEPTION(&e, &ad);
    }

    myWriter.write(response);
    myLogger->LOGEXIT(out->returnStatus->statusCode, out->returnStatus->explanation->c_str());
    delete myLogger;
    return SOAP_OK;
}

