#include <castor/log/log.hpp>

#include "soapH.h"
#include "castor/srm/server/SrmLogger.hpp"
#include "castor/srm/server/ResponseWriter.hpp"

int srm__srmReserveSpace(
        struct soap *soap,
        srm__srmReserveSpaceRequest*,
        struct srm__srmReserveSpaceResponse_& response)//< response parameter
{
    castor::srm::server::SrmLogger *myLogger = new castor::srm::server::SrmLogger();
    myLogger->LOGENTRY(soap);

    try {
        srm__srmReserveSpaceResponse *resp  = soap_new_srm__srmReserveSpaceResponse(soap, -1);
        resp->returnStatus                  = soap_new_srm__TReturnStatus(soap, -1);
        resp->returnStatus->explanation     = soap_new_std__string( soap, -1 );
        resp->requestToken                  = NULL;
        resp->estimatedProcessingTime       = NULL;
        resp->retentionPolicyInfo           = NULL;
        resp->sizeOfTotalReservedSpace      = NULL;
        resp->sizeOfGuaranteedReservedSpace = NULL;
        resp->lifetimeOfReservedSpace       = NULL;
        resp->spaceToken                    = NULL;
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCORENOT_USCORESUPPORTED;
        resp->returnStatus->explanation->assign("This SRM does not support dynamic space allocation");
        response.srmReserveSpaceResponse = resp;
    }
    catch (...) {
        std::list<castor::log::Param> params = {
            castor::log::Param("REQID", myLogger->getId()),
            castor::log::Param("Message", "Out of memory")
        };
        castor::log::write(LOG_ALERT, "Exception caught", params);
        myLogger->LOGEXIT(srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR, "Out of memory");
        delete myLogger;
        return SOAP_EOM;
    }

    myLogger->LOGEXIT(srm__TStatusCode__SRM_USCORENOT_USCORESUPPORTED, "Not supported");
    castor::srm::server::ResponseWriter myWriter;
    myWriter.write(response);
    delete myLogger;
    return SOAP_OK;
}
