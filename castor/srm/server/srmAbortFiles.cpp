#include <cgsi_plugin.h>

#include <castor/BaseAddress.hpp>
#include <castor/BaseObject.hpp>
#include <castor/Constants.hpp>
#include <castor/Services.hpp>
#include <castor/stager/StageAbortRequest.hpp>
#include <castor/stager/StageRmRequest.hpp>
#include <castor/stager/SubRequest.hpp>
#include <castor/stager/NsFileId.hpp>
#include <castor/rh/Response.hpp>

#include "soapH.h"
#include "castor/srm/CastorClient.hpp"
#include "castor/srm/Constants.hpp"
#include "castor/srm/SrmConstants.hpp"
#include "castor/srm/server/SrmLogger.hpp"
#include "castor/srm/ISrmSvc.hpp"
#include "castor/srm/StageRequest.hpp"
#include "castor/srm/SubRequest.hpp"
#include "castor/srm/SubRequestStatus.hpp"
#include "castor/srm/SrmUser.hpp"
#include "castor/srm/SURL.hpp"
#include "castor/srm/server/SrmUtils.hpp"
#include "castor/srm/server/ResponseWriter.hpp"
#include "castor/srm/server/RequestValidator.hpp"
#include <castor/srm/server/StageIncrementer.hpp>

std::string abortFiles( castor::srm::server::SrmLogger *myLogger, castor::srm::StageRequest* origReq, std::string filename, std::string reqId, Cns_fileid nsId ) {
    std::string ret;
    castor::stager::Request* req = 0;
    if(origReq->requestType() == castor::srm::REQUESTTYPE_PUT) {
      // Remove from nameserver in case of PUT
      myLogger->logCnsCall("Cns_unlink", filename, &nsId, LOG_INFO);
      int cns_rc = Cns_unlink(filename.c_str());
      myLogger->logCnsReturn("Cns_unlink", cns_rc, &nsId);
      
      // Plus send a StageRm request
      castor::stager::StageRmRequest* rmReq = new castor::stager::StageRmRequest();
      req = rmReq;
      rmReq->setSvcClassName("*");
      castor::stager::SubRequest *subreq = new castor::stager::SubRequest();
      subreq->setRequest(rmReq);
      subreq->setFileName(filename);
      rmReq->addSubRequests(subreq);
    } else {
      // Send an abort request in case of GET or BOL
      castor::stager::StageAbortRequest* aReq = new castor::stager::StageAbortRequest();
      req = aReq;
      castor::stager::NsFileId *nsfileid = new castor::stager::NsFileId();  // will be deallocated when aReq goes out of scope
      nsfileid->setFileid(nsId.fileid);
      nsfileid->setNsHost(nsId.server);
      nsfileid->setRequest(aReq);
      aReq->addFiles(nsfileid);
      aReq->setParentUuid(reqId);
    }
    try {
      castor::srm::server::StageIncrementer inc;
      inc.increment();
      castor::srm::FileIdMap fileIds;
      fileIds[filename] = nsId;
      std::vector<castor::rh::Response*> resps;
      resps = castor::srm::CastorClient::sendRequest(myLogger->getId(), *req, origReq->rhHost(), origReq->rhPort(), fileIds);
      if(resps[0]->errorCode()) {
        ret = resps[0]->errorMessage();
      }
      delete resps[0];
    } catch (castor::exception::Exception e) {
      ret = e.getMessage().str();      
      // "Request failed" message
      std::list<castor::log::Param> snafu = {
        castor::log::Param("REQID", myLogger->getId()),
        castor::log::Param("RequestToken", origReq->id()),
        castor::log::Param("FileName", filename),
        castor::log::Param("ErrorMessage", sstrerror(e.code())),
        castor::log::Param("Details",  e.getMessage().str())
      };
      castor::log::write(LOG_ERR, "Request Failed", snafu);
    }
    
    delete req;
    return ret;
}

int srm__srmAbortFiles(
        struct soap *soap,
        srm__srmAbortFilesRequest*          srmAbortFilesRequest,
        srm__srmAbortFilesResponse_ &out) //< response parameter
{
    castor::srm::server::SrmLogger *myLogger = new castor::srm::server::SrmLogger();
    myLogger->LOGENTRY(soap);


    // Allocate space for return structure
    srm__srmAbortFilesResponse *resp;
    try {
        resp = soap_new_srm__srmAbortFilesResponse(soap, -1);
        resp->returnStatus = soap_new_srm__TReturnStatus(soap, -1);
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
        resp->returnStatus->explanation = soap_new_std__string(soap, -1);
        resp->returnStatus->explanation->assign("");
        resp->arrayOfFileStatuses = soap_new_srm__ArrayOfTSURLReturnStatus(soap, -1);
    }
    catch (...) {
        std::list<castor::log::Param> params = {
            castor::log::Param("REQID", myLogger->getId()),
            castor::log::Param("Message", "Out of memory")
        };
        castor::log::write(LOG_ALERT, "Exception caught", params);
        myLogger->LOGEXIT(resp->returnStatus->statusCode, "Out of memory");
        delete myLogger;
        return SOAP_EOM;
    }
    out.srmAbortFilesResponse = resp;

    castor::srm::server::ResponseWriter myWriter;

    // Set up database service
    castor::Services* svcs;
    castor::srm::ISrmSvc* srmSvc;
    castor::BaseAddress ad;
    try {
        svcs = castor::BaseObject::services();
        ad.setCnvSvcName("DbCnvSvc");
        ad.setCnvSvcType(castor::SVC_DBCNV);
        castor::IService *svc = castor::BaseObject::services()->service("DbSrmSvc", castor::SVC_DBSRMSVC);
        srmSvc = dynamic_cast<castor::srm::ISrmSvc*>(svc);
        if ( srmSvc == 0 ) {
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
            resp->returnStatus->explanation->assign("Unable to establish database service");
            std::list<castor::log::Param> params = {castor::log::Param("REQID", myLogger->getId())};
            castor::log::write(LOG_ERR, "Could not get Conversion Service for Database", params);
            myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
            delete myLogger;
            myWriter.write(out);
            return SOAP_OK;
        }
    }
    catch (castor::exception::Exception e) {
        myLogger->LOGEXCEPTION(&e, &ad);
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        resp->returnStatus->explanation->assign( e.getMessage().str());
        myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
        delete myLogger;
        myWriter.write(out);
        return SOAP_OK;
    }

    castor::srm::server::RequestValidator validate;
    castor::srm::server::SrmUtils *utils = new castor::srm::server::SrmUtils();
    castor::srm::StageRequest *originalRequest = 0;
    std::string dn;
    std::string uname;
    try {
        // Validate incoming request
        validate.requestToken( srmAbortFilesRequest->requestToken );
        validate.notNull(srmAbortFilesRequest->arrayOfSURLs, "arrayOfSURLs");
        validate.arrayOfSurls( srmAbortFilesRequest->arrayOfSURLs->urlArray );

        // Get original request on which we are to operate
        originalRequest = srmSvc->getStageRequest(srmAbortFilesRequest->requestToken);
        validate.request( originalRequest );
        myLogger->setId(originalRequest->srmRequestId());

        // Get the requestor information and see if it matches the owner of the  
        // original request
        dn = utils->getDn(soap);
        uname=utils->getUserName(soap);
        svcs->fillObj(&ad, originalRequest, castor::srm::OBJ_SrmUser, false);
        validate.permission( uname, dn, 
                originalRequest->srmUser()->userID(), originalRequest->srmUser()->dn());
    } catch (castor::exception::Exception &e) {
        castor::srm::SrmException* se = dynamic_cast<castor::srm::SrmException*>(&e);
        if(se) {
            resp->returnStatus->statusCode = (srm__TStatusCode)se->code();
        } else {
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST;
        }
        resp->returnStatus->explanation->assign(e.getMessage().str());
        myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
        myWriter.write(out);
        delete myLogger;
        delete utils;
        return SOAP_OK;
    }

    // lock the original request and log that we have started processing
    srmSvc->lockStageRequest(originalRequest->id());
    srm__ArrayOfAnyURI* surls = srmAbortFilesRequest->arrayOfSURLs;
    if(surls) {
        myLogger->LOGSTARTEDREQ(surls->urlArray.size());
    }

    uid_t uid;
    gid_t gid;
    utils->map_user(soap, &uid, &gid);

    // OK, once we are here we should be in a position to check things.
    // First lets get a std::list of the spcified SURLs
    std::map<std::string, std::string> files;
    for(unsigned int i = 0; i < surls->urlArray.size(); i++) {
        if ( surls->urlArray[i].length() > 0 ) {
            try {
                files[castor::srm::SURL(surls->urlArray[i], srmAbortFilesRequest->authorizationID, soap).getFileName()] = surls->urlArray[i];
            } catch(castor::exception::Exception &e) {
                // An error which we should log
                std::list<castor::log::Param> params = {
                    castor::log::Param("REQID", myLogger->getId()),
                    castor::log::Param("Message", "Could not parse SURL"),
                    castor::log::Param("SURL", surls->urlArray[i]),
                    castor::log::Param("Details", e.getMessage().str() )
                };
                castor::log::write(LOG_USER_ERROR, "Exception caught", params);
            }
        }
        else {
            // An error which we should log
            std::list<castor::log::Param> params = {
                castor::log::Param("REQID", myLogger->getId()),
                castor::log::Param("Message", "No SURL value supplied"),
            };
            castor::log::write(LOG_USER_ERROR, "Invalid Request", params);
        }
    }

    // Now go through each subrequest, see if its filename is in the supplied std::list
    // and whether it is still pending - if not, set it to aborted
    svcs->fillObj(&ad, originalRequest, castor::srm::OBJ_SubRequest, false);
    unsigned nAborted = 0;
    // Add logging that we are aborting a file.  We'll need the CNS file id
    Cns_fileid nsId;
    nsId.fileid=0;
    strncpy( nsId.server, castor::srm::SrmConstants::getInstance()->nsHost().c_str(), sizeof(nsId.server)-1);
    for (std::map<std::string, std::string>::iterator iter = files.begin();
            iter != files.end(); 
            iter++) {
        srm__TSURLReturnStatus *status = soap_new_srm__TSURLReturnStatus(soap, -1);
        status->status = soap_new_srm__TReturnStatus(soap, -1);
        status->status->explanation = soap_new_std__string(soap, -1);
        status->status->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
        status->surl.assign(iter->second);
        bool found = false;
        for ( unsigned j=0; j<originalRequest->subRequests().size(); j++ ) {
            if ( originalRequest->subRequests()[j]->castorFileName() == iter->first ) {
                found = true;
                // See if we can stat this file
                Cns_filestat buf;
                myLogger->logCnsCall( "Cns_stat", originalRequest->subRequests()[j]->castorFileName().c_str() );
                int rtn;
                if ( (rtn = Cns_stat(originalRequest->subRequests()[j]->castorFileName().c_str(), &buf)) == 0 ) 
                    nsId.fileid = buf.fileid;
                else
                    nsId.fileid = 0;
                myLogger->logCnsReturn( "Cns_stat", rtn);
                myLogger->LOGFILEREQ(&nsId, originalRequest->subRequests()[j]->castorFileName());
                switch ( originalRequest->requestType() ) {
                    case castor::srm::REQUESTTYPE_COPYPUSH:
                    case castor::srm::REQUESTTYPE_COPYPULL:
                        // We can only abort copy subrequests while they are not being processed;
                        // Once the daemon has updated status of the request it is too late
                        if ( originalRequest->subRequests()[j]->status() == castor::srm::SUBREQUEST_PENDING ) {
                            status->status->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
                            originalRequest->subRequests()[j]->setStatus(castor::srm::SUBREQUEST_ABORTED);
                            svcs->updateRep(&ad, originalRequest->subRequests()[j], false);
                            nAborted++;
                        }
                        else if ( originalRequest->subRequests()[j]->status() == castor::srm::SUBREQUEST_ABORTED ) {
                            status->status->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
                            nAborted++;
                        }
                        else {
                            // Fail the subrequest
                            status->status->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
                            status->status->explanation->assign("This file is already being processed and cannot be aborted");
                        }
                        break;
                    case castor::srm::REQUESTTYPE_GET:
                    case castor::srm::REQUESTTYPE_BOL:
                    case castor::srm::REQUESTTYPE_PUT:
                        // Any other request can be aborted at any time before it is completed
                        if ( originalRequest->subRequests()[j]->status() == castor::srm::SUBREQUEST_PENDING ||
                             originalRequest->subRequests()[j]->status() == castor::srm::SUBREQUEST_INPROGRESS) {
                            // If the request is not pending then we call abort on Castor. INPROGRESS requests
                            // without castorReqId are considered as PENDING, the backend deals with this case
                            // properly thanks to locks. See also srmAbortRequest and OraSrmDaemonSvc.checkAbortedSubReqs()
                            if ( originalRequest->subRequests()[j]->status() != castor::srm::SUBREQUEST_PENDING &&
                                 !(originalRequest->subRequests()[j]->castorReqId().empty() && 
                                   originalRequest->castorReqId().empty()) ) {
                                std::string ret = abortFiles( myLogger, originalRequest, originalRequest->subRequests()[j]->castorFileName(),
                                                     (!originalRequest->subRequests()[j]->castorReqId().empty() ?
                                                      originalRequest->subRequests()[j]->castorReqId() :
                                                      originalRequest->castorReqId()),
                                                     nsId);
                                if(ret.length() == 0) {
                                    status->status->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
                                    originalRequest->subRequests()[j]->setStatus(castor::srm::SUBREQUEST_ABORTED);
                                    svcs->updateRep(&ad, originalRequest->subRequests()[j], false);
                                    nAborted++;
                                } else { 
                                    status->status->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
                                    status->status->explanation->assign(ret);
                                }                                  
                            } else {
                              // PENDING requests can be aborted straight
                              status->status->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
                              originalRequest->subRequests()[j]->setStatus(castor::srm::SUBREQUEST_ABORTED);
                              svcs->updateRep(&ad, originalRequest->subRequests()[j], false);
                              nAborted++;
                            }
                        }
                        else {
                            status->status->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
                            status->status->explanation->assign("This file has already been fully processed");
                        }
                        break;
                    default:
                        // If we get here the database is really really screwed up
                        std::list<castor::log::Param> screwedP = {
                            castor::log::Param("REQID", myLogger->getId()),
                            castor::log::Param("Message", "Request found with invalid type"),
                            castor::log::Param("RequestToken", originalRequest->id()),
                            castor::log::Param("Type", originalRequest->requestType())
                        };
                        castor::log::write(LOG_ALERT, "Configuration error", screwedP);
                        status->status->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
                        status->status->explanation->assign("Fatal internal database error");
                        break;
                }
                break;
            }
        }
        if ( !found ) {
            status->status->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREPATH;
            status->status->explanation->assign("The supplied surl does not exist in the original request");
            status->surl.assign(iter->second);
            std::list<castor::log::Param> params = {
                castor::log::Param("REQID", myLogger->getId()),
                castor::log::Param("SURL", iter->second),
                castor::log::Param("Message", "SURL does not exist in this context"),
            };
            castor::log::write(LOG_WARNING, "Invalid Request", params);
        }
        resp->arrayOfFileStatuses->statusArray.push_back(status);
    }
    if (nAborted == surls->urlArray.size()) {
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
    }
    else if ( nAborted == 0 ) {
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
    }
    else {
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREPARTIAL_USCORESUCCESS;
    }
    // Check whether there's anything left for this request: if not, set status to DONE
    bool allCompleted = true;
    for (unsigned i=0; i<originalRequest->subRequests().size(); i++) {
        if ( originalRequest->subRequests()[i]->status() == castor::srm::SUBREQUEST_PENDING ||
                originalRequest->subRequests()[i]->status() == castor::srm::SUBREQUEST_INPROGRESS ) {
            allCompleted = false;
            break;
        }
    }
    if(allCompleted) {
        originalRequest->setStatus(castor::srm::REQUEST_DONE);
        originalRequest->setHandler(0);   // this prevents the daemon from picking up the request
        svcs->updateRep(&ad, originalRequest, true);
    }
    else {
        svcs->commit(&ad);
    }
    myWriter.write(out);
    myLogger->LOGEXIT( resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str() );
    myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
    if (originalRequest) {
        if (originalRequest->srmUser()) delete originalRequest->srmUser();
        for (unsigned c=0; c<originalRequest->subRequests().size(); c++)
            delete originalRequest->subRequests()[c];
        delete originalRequest;
    }
    delete myLogger;
    myWriter.write(out);
    if(utils != 0) {
        utils->unmap_user();
        delete utils;
    }
    return SOAP_OK;
}


