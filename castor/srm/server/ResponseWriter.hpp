#include <unistd.h>
#include <soapH.h>

namespace castor {
 namespace srm {
    namespace server {
        class ResponseWriter {
            public:
                void write (srm__srmReserveSpaceResponse_&);
                void write (srm__srmStatusOfReserveSpaceRequestResponse_&);
                void write (srm__srmReleaseSpaceResponse_&);
                void write (srm__srmUpdateSpaceResponse_&);
                void write (srm__srmStatusOfUpdateSpaceRequestResponse_&);
                void write (srm__srmGetSpaceMetaDataResponse_&);
                void write (srm__srmChangeSpaceForFilesResponse_&);
                void write (srm__srmStatusOfChangeSpaceForFilesRequestResponse_&);
                void write (srm__srmExtendFileLifeTimeInSpaceResponse_&);
                void write (srm__srmPurgeFromSpaceResponse_&);
                void write (srm__srmGetSpaceTokensResponse_&);
                void write (srm__srmSetPermissionResponse_&);
                void write (srm__srmCheckPermissionResponse_&);
                void write (srm__srmGetPermissionResponse_&);
                void write (srm__srmMkdirResponse_&);
                void write (srm__srmRmdirResponse_&);
                void write (srm__srmRmResponse_&);
                void write (srm__srmLsResponse_&);
                void write (srm__srmStatusOfLsRequestResponse_&);
                void write (srm__srmMvResponse_&);
                void write (srm__srmPrepareToGetResponse_&);
                void write (srm__srmStatusOfGetRequestResponse_&);
                void write (srm__srmBringOnlineResponse_&);
                void write (srm__srmStatusOfBringOnlineRequestResponse_&);
                void write (srm__srmPrepareToPutResponse_&);
                void write (srm__srmStatusOfPutRequestResponse_&);
                void write (srm__srmCopyResponse_&);
                void write (srm__srmStatusOfCopyRequestResponse_&);
                void write (srm__srmReleaseFilesResponse_&);
                void write (srm__srmPutDoneResponse_&);
                void write (srm__srmAbortRequestResponse_&);
                void write (srm__srmAbortFilesResponse_&);
                void write (srm__srmSuspendRequestResponse_&);
                void write (srm__srmResumeRequestResponse_&);
                void write (srm__srmGetRequestSummaryResponse_&);
                void write (srm__srmExtendFileLifeTimeResponse_&);
                void write (srm__srmGetRequestTokensResponse_&);
                void write (srm__srmGetTransferProtocolsResponse_&);
                void write (srm__srmPingResponse_&);

            protected:

            private:
                int init( struct soap *soap, std::string type );
                static int doWrite(struct soap *soap, const char *buffer, size_t size);
        };
    }
} }  // end of namespace castor/srm
