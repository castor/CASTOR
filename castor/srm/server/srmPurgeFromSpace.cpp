#include <castor/BaseAddress.hpp>
#include <castor/BaseObject.hpp>
#include <castor/Constants.hpp>
#include <castor/Services.hpp>
#include <castor/exception/Exception.hpp>
#include <castor/log/log.hpp>

#include <castor/srm/server/SrmLogger.hpp>
#include <castor/srm/server/SrmUtils.hpp>
#include <castor/srm/server/RequestValidator.hpp>
#include <castor/srm/server/StageIncrementer.hpp>
#include <castor/srm/SrmConstants.hpp>
#include <castor/srm/SURL.hpp>
#include <castor/srm/server/ResponseWriter.hpp>
#include "castor/srm/ISrmSvc.hpp"
#include "castor/srm/StorageArea.hpp"
#include "castor/srm/StorageStatus.hpp"

#include "soapH.h"

int srm__srmPurgeFromSpace (
        struct soap *soap,
        srm__srmPurgeFromSpaceRequest    *myRequest,
        srm__srmPurgeFromSpaceResponse_  &myResponse)
{
    castor::srm::server::SrmLogger *myLogger = new castor::srm::server::SrmLogger();
    myLogger->LOGENTRY(soap);

    srm__srmPurgeFromSpaceResponse *resp;
    try {
        resp = soap_new_srm__srmPurgeFromSpaceResponse(soap, -1);
        resp->returnStatus = soap_new_srm__TReturnStatus(soap, -1);
        resp->returnStatus->explanation = soap_new_std__string(soap, -1);

        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
        resp->returnStatus->explanation->assign("");
        resp->arrayOfFileStatuses = NULL;
    }
    catch (...) {
        std::list<castor::log::Param> params = {
            castor::log::Param("REQID", myLogger->getId()),
            castor::log::Param("Message", "Out of memory")
        };
        castor::log::write(LOG_ALERT, "Exception caught", params);
        myLogger->LOGEXIT(srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR, "Out of memory");
        delete myLogger;
        return SOAP_EOM;
    }
    myResponse.srmPurgeFromSpaceResponse = resp;

    // Create a writer
    castor::srm::server::ResponseWriter myWriter;
    castor::srm::server::RequestValidator      validate;
    castor::srm::server::SrmUtils *utils = new castor::srm::server::SrmUtils();

    castor::srm::ISrmSvc* srmSvc;
    castor::BaseAddress ad;
    try {
        ad.setCnvSvcName("DbCnvSvc");
        ad.setCnvSvcType(castor::SVC_DBCNV);
        castor::IService *svc = castor::BaseObject::services()->service("DbSrmSvc", castor::SVC_DBSRMSVC);
        srmSvc = dynamic_cast<castor::srm::ISrmSvc*>(svc);
        if ( srmSvc == 0 ) {
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
            resp->returnStatus->explanation->assign("Unable to establish database service");
            std::list<castor::log::Param> params = {castor::log::Param("REQID", myLogger->getId())};
            castor::log::write(LOG_ERR, "Could not get Conversion Service for Database", params);
            myWriter.write(myResponse);
            myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
            delete utils;
            delete myLogger;
            return SOAP_OK;
        }
    }
    catch (castor::exception::Exception e) {
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        resp->returnStatus->explanation->assign( e.getMessage().str());
        myLogger->LOGEXCEPTION(&e, &ad);
        myWriter.write(myResponse);
        myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
        delete myLogger;
        return SOAP_OK;
    }

    std::string stagerHost;
    std::string svcClass;
    castor::srm::StorageArea* area=0;
    castor::srm::server::StageIncrementer inc;
    try {
        validate.notNull(myRequest->arrayOfSURLs, "arrayOfSURLs");
        validate.arrayOfSurls(myRequest->arrayOfSURLs->urlArray);
        validate.spaceToken(myRequest->spaceToken);
        stagerHost = utils->getStagerHost(soap);
        area = srmSvc->getStorageArea(myRequest->spaceToken);
        validate.storageArea(area, false);
        validate.svcClass(area);
        svcClass = area->svcClass();
        delete area;
    } catch (castor::exception::Exception &e) {
        castor::srm::SrmException* se = dynamic_cast<castor::srm::SrmException*>(&e);
        if(se) {
            resp->returnStatus->statusCode = (srm__TStatusCode)se->code();
        } else {
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        }
        resp->returnStatus->explanation->assign(e.getMessage().str());
        myWriter.write(myResponse);
        myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
        delete utils;
        delete myLogger;
        return SOAP_OK;
    }

    // Start real processing:
    uid_t uid;
    gid_t gid;
    utils->map_user(soap, &uid, &gid);

    // Now we need to loop over requests.  For each request we need to :
    //     check the SURL is valid
    //     check if we have a tape copy
    //     check if the SURL is in this area
    //     remove the disk copy
    // and we need to switch user - because we might not be allowed to access the
    // pool.
    resp->arrayOfFileStatuses = soap_new_srm__ArrayOfTSURLReturnStatus(soap, -1);

    // We will in this loop bulk up the query and rm requests to make life 
    // easier for CASTOR.
    struct stage_query_req *qryRequest=0;
    int okCount=0;
    std::map<std::string, Cns_fileid>   idmap;
    std::map<std::string, Cns_filestat> statmap;
    for (unsigned i=0; i<myRequest->arrayOfSURLs->urlArray.size(); i++) {
        // Create subresponse structure
        srm__TSURLReturnStatus *thisStatus = soap_new_srm__TSURLReturnStatus(soap, -1);
        thisStatus->surl.assign(myRequest->arrayOfSURLs->urlArray[i]);
        thisStatus->status = soap_new_srm__TReturnStatus(soap, -1);
        thisStatus->status->explanation = soap_new_std__string(soap, -1);
        thisStatus->status->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
        thisStatus->status->explanation->assign("");

        std::string thisFileName;
        try {
            castor::srm::SURL* surl = validate.surl(thisStatus->surl);
            thisFileName = surl->getFileName();
            delete surl;
        }
        catch (castor::exception::Exception &e) {
            thisStatus->status->statusCode = (srm__TStatusCode)e.code();
            thisStatus->status->explanation->assign(e.getMessage().str());
            resp->arrayOfFileStatuses->statusArray.push_back(thisStatus);
            continue;
        }

        // Try and do a stat on the file and see if it is available.  If its not in the 
        // namesserver we won't add it to the request
        Cns_filestat buf;
        Cns_fileid  nsId;
        if ( Cns_stat(thisFileName.c_str(), &buf) != 0 ) {
            thisStatus->status->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREPATH;
            thisStatus->status->explanation->assign(sstrerror(serrno));
            resp->arrayOfFileStatuses->statusArray.push_back(thisStatus);
            continue;
        }
        nsId.fileid = buf.fileid;
        strncpy(nsId.server, castor::srm::SrmConstants::getInstance()->nsHost().c_str(), sizeof(nsId.server)-1);
        myLogger->LOGFILEREQ(&nsId, thisFileName);
        idmap[thisFileName]   = nsId;
        statmap[thisFileName] = buf;

        okCount++;

        qryRequest = (stage_query_req*)realloc(qryRequest, okCount*sizeof(stage_query_req));
        qryRequest[okCount-1].type = ::BY_FILENAME;
        qryRequest[okCount-1].param = strdup(thisFileName.c_str());
        resp->arrayOfFileStatuses->statusArray.push_back(thisStatus);
    }

    // Now we bulk query the stager with what we have learned.  We perform the query first since attempting to remove
    // a non-existent file is a relatively expensive operation.
    struct stage_options opts;
    struct stage_filequery_resp *qryResponse;
    int nResp;
    opts.stage_host    = strdup(stagerHost.c_str());
    opts.stage_port    = STAGERPORT;
    opts.service_class = strdup(svcClass.c_str());

    myLogger->logStagerCall("stage_filequery", opts);
    int rc = stage_filequery(qryRequest, okCount, &qryResponse, &nResp, &opts );
    myLogger->logStagerReturn("stage_filequery", rc, "");
    if ( rc != 0 ) {
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        resp->returnStatus->explanation->assign( sstrerror(serrno) );
        myWriter.write(myResponse);
        myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
        utils->unmap_user();
        delete utils;
        delete myLogger;
        return SOAP_OK;
    }

    // At this point we can free the query request
    free_query_req(qryRequest, okCount);
    // Now loop over responses if things worked and add them to a stage_rm request.
    // For those that failed, record the error and move on....
    okCount = 0;
    struct stage_filereq *rmRequest = 0;
    Cns_fileid nsid;
    for (unsigned j=0; j<resp->arrayOfFileStatuses->statusArray.size(); j++) {
        bool matched = false;
        std::string fname;
        for (int i=0; i<nResp; i++) {
            // Look for matching responses
            if ( (strlen(qryResponse[i].castorfilename) == 0) || (resp->arrayOfFileStatuses->statusArray[j]->surl.find(qryResponse[i].castorfilename) == std::string::npos) ) 
                continue;
            // We have a match!  Check the return code for this request.
            matched = true;
            fname = qryResponse[i].castorfilename;
            nsid = idmap[fname];
            if ( qryResponse[i].errorCode != 0 ) {
                resp->arrayOfFileStatuses->statusArray[j]->status->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
                resp->arrayOfFileStatuses->statusArray[j]->status->explanation->assign(qryResponse[i].errorMessage);
                myLogger->logFileFailure(&nsid, qryResponse[i].castorfilename, qryResponse[i].errorMessage);
            }
            else {
                // See if the file has been migrated.  If not, check if it is in status CANBEMIGR.  If
                // if it is in CANBEMIGR we need to return FILE_BUSY, otherwise we return LAST_COPY.
                if ( statmap[qryResponse[i].castorfilename].status != 'm') {
                    if ( qryResponse[i].status == ::FILE_CANBEMIGR || qryResponse[i].status == ::FILE_STAGEOUT ) {
                        resp->arrayOfFileStatuses->statusArray[j]->status->statusCode = srm__TStatusCode__SRM_USCOREFILE_USCOREBUSY;
                        resp->arrayOfFileStatuses->statusArray[j]->status->explanation->assign("Transfer not yet completed");
                    }
                    else if ( qryResponse[i].status == ::FILE_STAGED ) {   
                        // we'll still try to purge CANBEMIGR files, the stager will properly handle this case without loosing files
                        resp->arrayOfFileStatuses->statusArray[j]->status->statusCode = srm__TStatusCode__SRM_USCORELAST_USCORECOPY;
                        resp->arrayOfFileStatuses->statusArray[j]->status->explanation->assign("File not backed to tape - purge will not be done");
                    }
                    myLogger->logFileFailure(&idmap[qryResponse[i].castorfilename], 
                            qryResponse[i].castorfilename, 
                            resp->arrayOfFileStatuses->statusArray[j]->status->explanation->c_str());
                    continue;
                }
                okCount++;
                rmRequest = (stage_filereq*)realloc(rmRequest, okCount*sizeof(stage_filereq));
                rmRequest[okCount-1].filename = strdup(qryResponse[i].castorfilename);
            }
            break;
        }
        if (!matched) {
            resp->arrayOfFileStatuses->statusArray[j]->status->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
            resp->arrayOfFileStatuses->statusArray[j]->status->explanation->assign("File not in this space");
            myLogger->logFileFailure(&idmap[fname], 
                    fname,
                    resp->arrayOfFileStatuses->statusArray[j]->status->explanation->c_str());
        }
    }

    // Here we can now free the query response
    free_filequery_resp(qryResponse, nResp);
    
    try {

      if (!rmRequest) {
          castor::srm::SrmException e(srm__TStatusCode__SRM_USCOREFAILURE);
          e.getMessage() << "No valid files to purge";
          throw e;
      }
  
      // Finally we can issue the stage_rm request
      inc.increment();
      stage_fileresp *rmResponse;
      char *requestId;
      myLogger->logStagerCall("stage_rm", opts);
      rc = stage_rm(rmRequest, okCount, &rmResponse, &nResp, &requestId, &opts);
      myLogger->logStagerReturn("stage_rm", rc, requestId ? requestId : "");
      // Free up request
      free_filereq(rmRequest, okCount);
      // Finally loop over responses and fill in the last stuctures...
      if (rc != 0) {
          castor::exception::Exception e;
          e.getMessage() << sstrerror(serrno);
          resp->arrayOfFileStatuses->statusArray.clear();
          throw e;
      }
      for (int i=0; i<nResp; i++) {
          for (unsigned j=0; j<resp->arrayOfFileStatuses->statusArray.size(); j++) {
              // Look for matching responses
              if ( resp->arrayOfFileStatuses->statusArray[j]->surl.find(rmResponse[i].filename) == std::string::npos ) continue;
              // Found a match
              if ( rmResponse[i].errorCode == 0 ) {
                  resp->arrayOfFileStatuses->statusArray[j]->status->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
              }
              else {
                  resp->arrayOfFileStatuses->statusArray[j]->status->statusCode = 
                      (rmResponse[i].errorCode == EBUSY ? srm__TStatusCode__SRM_USCOREFILE_USCOREBUSY : srm__TStatusCode__SRM_USCOREFAILURE);
                  resp->arrayOfFileStatuses->statusArray[j]->status->explanation->assign(rmResponse[i].errorMessage);
                  myLogger->logFileFailure(&idmap[rmResponse[i].filename], rmResponse[i].filename, rmResponse[i].errorMessage);
              }
              break;
          }
      }
      free_fileresp(rmResponse, nResp);
      if (nResp == 0) {
          resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
      }
      else if (nResp < (int)myRequest->arrayOfSURLs->urlArray.size()) {
          resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREPARTIAL_USCORESUCCESS;
      }
      else {
          resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
      }
    }
    catch (castor::exception::Exception &e) {
        castor::srm::SrmException* se = dynamic_cast<castor::srm::SrmException*>(&e);
        if(se) {
            resp->returnStatus->statusCode = (srm__TStatusCode)se->code();
        } else {
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        }
        resp->returnStatus->explanation->assign(e.getMessage().str());
    }
    
    // Send response and cleanup
    myWriter.write(myResponse);
    myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
    utils->unmap_user();
    delete utils;
    delete myLogger;
    return SOAP_OK;
}

