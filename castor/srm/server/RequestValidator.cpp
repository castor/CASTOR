#include <stdlib.h>
#include <algorithm>
#include <castor/srm/server/RequestValidator.hpp>

#include <castor/srm/SrmUser.hpp>



///////////////////////////////////////////////////////////////////////////////
// requestToken
///////////////////////////////////////////////////////////////////////////////
bool castor::srm::server::RequestValidator::requestToken(std::string token) 
throw (castor::srm::SrmException) {
    if (token.length() == 0 ) {
        generateException(srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST, 
            "No space token supplied");
    }
    char* ptr = 0;
    errno=0;
    strtoul(token.c_str(), &ptr, 10);
    if (errno != 0 || *ptr!='\0')
        generateException(srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST,
          "Invalid request token: expecting numeric");
    return true;
}

///////////////////////////////////////////////////////////////////////////////
// spaceToken
///////////////////////////////////////////////////////////////////////////////
bool castor::srm::server::RequestValidator::spaceToken(std::string token)
    throw (castor::srm::SrmException) {
    if (token.length() == 0) 
        generateException(srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST,
                "Invalid space token");
    return true;
}

///////////////////////////////////////////////////////////////////////////////
// arrayOfSurls
///////////////////////////////////////////////////////////////////////////////
bool castor::srm::server::RequestValidator::arrayOfSurls(std::vector<std::string> arrayOfSurls)
    throw (castor::srm::SrmException) {
    validateVectorOfStrings(arrayOfSurls);
    return true;
}

///////////////////////////////////////////////////////////////////////////////
// request
///////////////////////////////////////////////////////////////////////////////
bool castor::srm::server::RequestValidator::request(castor::srm::StageRequest* request, int reqType) 
throw (castor::srm::SrmException) {
    if ( 0 == request ) {
        generateException(srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST,
            "Request token does not correspond to an existing request");
    }
    if ( reqType != -1 ) {
        if (request->requestType() != reqType) {
            generateException(
                    srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST,
                    "Incorrect request type for this operation");
        }
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////
// permission
///////////////////////////////////////////////////////////////////////////////
bool castor::srm::server::RequestValidator::permission(std::string requestorName, std::string requestorDN,
        std::string ownerName, std::string ownerDN ) 
throw (castor::srm::SrmException) {
    if ( (requestorName != ownerName) || requestorDN != ownerDN ) {
        generateException(srm__TStatusCode__SRM_USCOREAUTHORIZATION_USCOREFAILURE,
            "User does not have permission to access this request");
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////
// permission
///////////////////////////////////////////////////////////////////////////////
bool castor::srm::server::RequestValidator::permission(castor::srm::StorageArea *area, std::string vo) 
throw (castor::srm::SrmException) {
    if ( (0 == area->srmUser()) || (area->srmUser()->vo() != vo) ) {
        std::stringstream ss;
        ss << "User belonging to VO " << vo
           << " not authorized to access space token " << area->tokenDescription();
        generateException(srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST, ss.str());
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////
// storageType
///////////////////////////////////////////////////////////////////////////////
bool castor::srm::server::RequestValidator::storageType(int type) 
throw (castor::srm::SrmException) {
    // explicitly disallow srm__TFileStorageType__VOLATILE as deprecated in WLCG,
    // otherwise a TFileStorageType is an enum and any other value is out of range 
    if ( (type != (int)srm__TFileStorageType__DURABLE) &&
         (type != (int)srm__TFileStorageType__PERMANENT ) ) {
        generateException(srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST,
            "Invalid storage type passed");
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////
// storageArea
///////////////////////////////////////////////////////////////////////////////
bool castor::srm::server::RequestValidator::storageArea(castor::srm::StorageArea* area, bool checkAvailable) 
throw (castor::srm::SrmException) {
    if ( 0 == area ) {
        generateException(srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST,
            "Space token does not correspond to any storage area");
    }
    if ( checkAvailable ) {
        if ( area->storageStatus() != castor::srm::STORAGESTATUS_ALLOCATED &&
             area->storageStatus() != castor::srm::STORAGESTATUS_INUSE) {
            generateException(srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST,
                "Space token currently not available for use");
        }
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////
// svcClass
///////////////////////////////////////////////////////////////////////////////
bool castor::srm::server::RequestValidator::svcClass(castor::srm::StorageArea *area) 
throw (castor::srm::SrmException) {
    // First make sure the area is not null
    storageArea(area, false);
    if ( area->svcClass().length() == 0 ) {
        generateException(srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR,
                "Space token misconfigured - not associated to service class");
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////
// protocol
///////////////////////////////////////////////////////////////////////////////
bool castor::srm::server::RequestValidator::protocol(std::vector<std::string> requestedProtocols, 
        std::string* agreedProtocol) 
throw (castor::srm::SrmException) {
    std::vector<std::string> supportedProtocols = castor::srm::SrmConstants::getInstance()->supportedProtocols();
    
    // Look for a matching supported protocol
    std::vector<std::string>::iterator res = std::find_first_of(
      requestedProtocols.begin(), requestedProtocols.end(),
      supportedProtocols.begin(), supportedProtocols.end());
    if(res == requestedProtocols.end()) { 
      generateException(srm__TStatusCode__SRM_USCORENOT_USCORESUPPORTED,
          "Unable to agree on protocol");
    }
    else
      *agreedProtocol = *res;
    return false;
}

///////////////////////////////////////////////////////////////////////////////
// spaceTokenList
///////////////////////////////////////////////////////////////////////////////
bool castor::srm::server::RequestValidator::spaceTokenList(std::vector<std::string> arrayOfSpaceTokens) 
throw (castor::srm::SrmException) {
    validateVectorOfStrings(arrayOfSpaceTokens);
    return true;
}

///////////////////////////////////////////////////////////////////////////////
// arrayOfGetFileRequests
///////////////////////////////////////////////////////////////////////////////
bool castor::srm::server::RequestValidator::arrayOfGetFileRequests(srm__ArrayOfTGetFileRequest* array)
throw (castor::srm::SrmException) {
    if ( 0 == array || array->requestArray.size() == 0 ) {
        generateException(srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST, "No file requests supplied");
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////
// arrayOfPutFileRequests
///////////////////////////////////////////////////////////////////////////////
bool castor::srm::server::RequestValidator::arrayOfPutFileRequests(srm__ArrayOfTPutFileRequest* array)
throw (castor::srm::SrmException) {
    if ( 0 == array || array->requestArray.size() == 0 ) {
        generateException(srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST, "No file requests supplied");
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////
// surl
///////////////////////////////////////////////////////////////////////////////
castor::srm::SURL* castor::srm::server::RequestValidator::surl(std::string surl)
throw (castor::srm::SrmException) {
    castor::srm::SURL *mySURL=0;
    if (surl.length() == 0) {
        generateException(srm__TStatusCode__SRM_USCOREINVALID_USCOREPATH,
                "Empty SURL");
    }
    try {
        mySURL = new castor::srm::SURL(surl);
    } catch (...) {
        generateException(srm__TStatusCode__SRM_USCOREINVALID_USCOREPATH,
                "Could not parse SURL");
    }
    return mySURL;
}

///////////////////////////////////////////////////////////////////////////////
// lifetime
///////////////////////////////////////////////////////////////////////////////
bool castor::srm::server::RequestValidator::lifetime(int* lifetime, bool allowNulls)
throw (castor::srm::SrmException) {
    if (0 == lifetime && !allowNulls) {
        generateException(srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST,
                "No lifetime supplied");
    }
    else if (lifetime && *lifetime < 0) {
        generateException(srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST,
                "Negative lifetimes are not permitted");
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////
// notNull
///////////////////////////////////////////////////////////////////////////////
bool castor::srm::server::RequestValidator::notNull(void *ptr, std::string param)
throw (castor::srm::SrmException) {
    if (0 == ptr) {
        std::string message("Null argument supplied: ");
        message.append(param);
        generateException(srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST,
                message);
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////
// null
///////////////////////////////////////////////////////////////////////////////
bool castor::srm::server::RequestValidator::null(void *ptr, std::string param)
throw (castor::srm::SrmException) {
    if (ptr) {
        std::string message("Argument not supported: ");
        message.append(param);
        generateException(srm__TStatusCode__SRM_USCORENOT_USCORESUPPORTED,
                message);
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////
// validateVectorOfStrings
///////////////////////////////////////////////////////////////////////////////
bool castor::srm::server::RequestValidator::validateVectorOfStrings(std::vector<std::string> v) 
throw (castor::srm::SrmException) {
    if (v.size() == 0) {
        generateException(srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST, "No SURLs supplied");
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////
// constructException
///////////////////////////////////////////////////////////////////////////////
void castor::srm::server::RequestValidator::generateException(enum srm__TStatusCode error, std::string message) throw (castor::srm::SrmException) {
    castor::srm::SrmException e( error );
    e.getMessage() << message.c_str();
    throw e;
}
