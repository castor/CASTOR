#ifndef SRM_SERVER_SRMUTILS_HPP
#define SRM_SERVER_SRMUTILS_HPP

#include <sys/types.h>
#include <map>

#include <castor/exception/Exception.hpp>

#include "soapH.h"
#include "castor/srm/SrmException.hpp"

namespace castor {
 namespace srm {

  namespace server {

    class SrmUtils {
        public:

            /** Default constructor */
            SrmUtils();

            /**
             * Map the user to the effective user of a request.
             */
            int map_user(struct soap *soap, uid_t *uid, gid_t *gid);

            /**
             * Unmap a previouslt mapped user
             */
            void unmap_user();

            /**
             * Get uid and gid of a user
             */
            void getUserAndGroup(std::string userID, uid_t *uid, gid_t *gid) throw (castor::exception::Exception);

            /**
             * Set delegated credentials
             */
            const std::string* set_delegated_credentials(struct soap *soap, uid_t uid, gid_t gid);

            /**
             * Convert an integer to a std::string
             */
            static const std::string toString(int val);

            /**
             * Tokenize a string
             */
            bool isGroupMapped ();

            /**
             * Get a stager based on the VO (or the default stager host of none
             * supplied).  If voName has 0 size, the default stager host will be 
             * returned. If no mapping found, an exception is thrown.
             */
            std::string getStagerHost(std::string voname) throw (castor::exception::Exception);

            /**
             * Get stager host from soap message - client does not need to
             * decode soap message
             */
            std::string getStagerHost(struct soap *soap) throw (castor::exception::Exception);

            /**
              * Get clients IP address from a SOAP meessage.
              */
            std::string getClientAddress(struct soap *soap);

            /**
              * Get DN of caller from a SOAP meessage.
              */
            std::string getDn(struct soap *soap);

            /**
              * Get VO of caller from a SOAP meessage.
              */
            std::string getVoName(struct soap *soap);

            /**
              * Get VO of caller from a user name
              */
            std::string getVoName(std::string username) throw (srm::SrmException);

            /**
              * Get VO role of caller from a SOAP meessage.
              */
            std::string getVoRole(struct soap *soap);

            /**
              * Get pool account of caller from a SOAP meessage.
              */
            std::string getUserName(struct soap *soap);

            /** 
            * Utility to find out whether a particular feature is supported in the current
            * version of the stager running on a specific host.
            * The input std::string should match one of the SRMFEATURE parameters in castor.conf.
            */
            bool supportedFeature (struct soap *soap, const std::string feature);

            /**
              * Get a std::list of the default service classses defined for the VO for
              * recalls.  Result is returned as a std::vector
              */
            std::vector<std::string> getSvcClassList(std::string vo);

            /**
              * Get the service class based on supplied accessLatency/retentionPolicy.
              * If neither is given, we assume the data is custodial/nearline
              */
            std::string getSvcClass(std::string &vo,
                    srm__TRetentionPolicy policy = srm__TRetentionPolicy__CUSTODIAL, 
                    srm__TAccessLatency latency = srm__TAccessLatency__NEARLINE)
                throw (castor::exception::Exception);

            /**
             * Retrieve the castor version by calling the stage_version API. Responses
             * are then cached for subsequent requests to avoid to call again the stager.
             */             
            std::string getCastorVersion (struct soap *soap);
            std::string getCastorVersion (std::string voName);


        protected:

        private:
            uid_t realUID;
            gid_t realGID;
            uid_t effectiveUID;
            gid_t effectiveGID;
            //std::string _myCurrentVo;

            std::string defaultVoName;
            static const int STAGEPORT;

            static pthread_mutex_t mutex;
            // cache of the castor version for each stager we will talk with
            static std::map<std::string, std::string> s_castorVersions;
    };
  } // end of namespace server
} } /* end of namespace castor/srm */

#endif
