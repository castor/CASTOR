#include <cgsi_plugin.h>

#include <castor/BaseAddress.hpp>
#include <castor/BaseObject.hpp>
#include <castor/Constants.hpp>
#include <castor/Services.hpp>
#include <castor/stager/StageAbortRequest.hpp>
#include <castor/stager/StageRmRequest.hpp>
#include <castor/stager/SubRequest.hpp>
#include <castor/stager/NsFileId.hpp>
#include <castor/rh/Response.hpp>

#include "soapH.h"
#include "castor/srm/Constants.hpp"
#include "castor/srm/ISrmSvc.hpp"
#include "castor/srm/StageRequest.hpp"
#include "castor/srm/StorageArea.hpp"
#include "castor/srm/SubRequest.hpp"
#include "castor/srm/SubRequestStatus.hpp"
#include "castor/srm/SrmUser.hpp"
#include "castor/srm/UserFile.hpp"
#include "castor/srm/CastorClient.hpp"
#include "castor/srm/server/ResponseWriter.hpp"
#include "castor/srm/server/RequestValidator.hpp"
#include "castor/srm/server/SrmLogger.hpp"
#include "castor/srm/server/SrmUtils.hpp"
#include "castor/srm/server/StageIncrementer.hpp"

int abortStageRequest(castor::srm::StageRequest *request, srm__srmAbortRequestResponse *response,
        castor::Services *svcs, castor::BaseAddress *ad, castor::srm::ISrmSvc* srmSvc,
        castor::srm::server::SrmLogger* myLogger, struct soap *soap, castor::srm::server::SrmUtils* utils)
        throw (castor::exception::Exception) {

    // lock the original request
    srmSvc->lockStageRequest(request->id());

    svcs->fillObj(ad, request, castor::srm::OBJ_SubRequest, false);
    std::string dn = utils->getDn(soap);
    std::string uname = utils->getUserName(soap);

    unsigned int aborted=0;
    switch (request->requestType()) {
        case castor::srm::REQUESTTYPE_COPYPUSH:
        case castor::srm::REQUESTTYPE_COPYPULL:
            // Set status to aborted. Even if the back end is still dealing
            // with the request, we abort in the hope the backend gives up
            // checking for outstanding BoL's even though we can't cancel
            // them.  
            for (std::vector<castor::srm::SubRequest*>::const_iterator srit = request->subRequests().begin();
                    srit != request->subRequests().end(); srit++) {
                (*srit)->setStatus(castor::srm::SUBREQUEST_ABORTED);
                svcs->updateRep(ad, (*srit), false);
                aborted++;
            }
            break;
        case castor::srm::REQUESTTYPE_GET:
        case castor::srm::REQUESTTYPE_BOL:
        case castor::srm::REQUESTTYPE_PUT:
            if(request->subRequests().empty()) {
                // There are cases of 'empty' requests (see CASTOR-5474), for which we just do nothing
                // and consider the abort successful
                break;
            }
            if(request->subRequests()[0]->castorReqId().empty() && request->castorReqId().empty()
               && request->requestType() != castor::srm::REQUESTTYPE_PUT) {
                // Only for GET/BOL requests: this means we arrived before the request went to the stager,
                // so we consider as if the request is still pending even if the backend could have picked it up:
                // the backend will deal with it properly (see OraSrmDaemonSvc.checkAbortedSubReqs()).
                // PUT requests are anyway aborted by calling StageRm to drop everything.
                request->setStatus(castor::srm::REQUEST_PENDING);
            }
            switch (request->status()) {
                case castor::srm::REQUEST_PENDING:
                    // Request still queued and the backend didn't pick it up, so we just abort
                    for (std::vector<castor::srm::SubRequest*>::const_iterator srit = request->subRequests().begin();
                            srit != request->subRequests().end(); srit++) {
                        (*srit)->setStatus(castor::srm::SUBREQUEST_ABORTED);
                        svcs->updateRep(ad, (*srit), false);
                        aborted++;
                    }
                    break;
                case castor::srm::REQUEST_INPROGRESS:
                case castor::srm::REQUEST_READYTOPOLL:
                  {
                    // The backend is dealing with it, so we call the stager to abort the request.
                    // This will be a StageRm request for PUTs and a StageAbort request for the rest.
                    castor::stager::Request* req = 0;
                    castor::srm::FileIdMap fileIds;
                    std::map<u_signed64, castor::srm::SubRequest*> fidToSubReq;
                    if(request->requestType() == castor::srm::REQUESTTYPE_PUT) {
                        // Prepare a StageRm request
                        castor::stager::StageRmRequest* rmReq = new castor::stager::StageRmRequest();
                        req = rmReq;
                        rmReq->setSvcClassName("*");
                        for (std::vector<castor::srm::SubRequest*>::const_iterator srit = request->subRequests().begin();
                              srit != request->subRequests().end(); srit++) {
                            castor::stager::SubRequest *subreq = new castor::stager::SubRequest();
                            subreq->setRequest(rmReq);
                            subreq->setFileName((*srit)->castorFileName());
                            rmReq->addSubRequests(subreq);
                            // XXX this is an extra call to the db for each subrequest, could be improved!
                            castor::srm::UserFile *uf = srmSvc->getUserFile((*srit)->castorFileName());
                            struct Cns_fileid fid;
                            fid.fileid = uf->fileId();
                            strncpy(fid.server, uf->nsHost().c_str(), sizeof(fid.server)-1);
                            fileIds[(*srit)->castorFileName()] = fid;
                            fidToSubReq[fid.fileid] = *srit;
                            delete uf;
                        }
                    } else {
                        // Prepare the Abort request
                        castor::stager::StageAbortRequest* aReq = new castor::stager::StageAbortRequest();
                        req = aReq;
                        for (std::vector<castor::srm::SubRequest*>::const_iterator srit = request->subRequests().begin();
                            srit != request->subRequests().end(); srit++) {
                            // XXX this is an extra call to the db for each subrequest, could be improved!
                            castor::srm::UserFile *uf = srmSvc->getUserFile((*srit)->castorFileName());
                            castor::stager::NsFileId *nsfileid = new castor::stager::NsFileId();  // deallocated when aReq goes out of scope
                            nsfileid->setFileid(uf->fileId());
                            nsfileid->setNsHost(uf->nsHost());
                            nsfileid->setRequest(aReq);
                            aReq->addFiles(nsfileid);
                            struct Cns_fileid fid;
                            fid.fileid = uf->fileId();
                            strncpy(fid.server, uf->nsHost().c_str(), sizeof(fid.server)-1);
                            fileIds[(*srit)->castorFileName()] = fid;
                            fidToSubReq[fid.fileid] = *srit;
                            delete uf;
                        }
                        // The reqId to be used is either the subreq's one(s) if already set, or the request's one (case of recall).
                        // XXX We should actually use the partitioning algorithm, this is a 0-level approximation.
                        aReq->setParentUuid(request->subRequests()[0]->castorReqId());
                        if(aReq->parentUuid().empty()) {
                          aReq->setParentUuid(request->castorReqId());
                        }
                    }
                    
                    std::vector<castor::rh::Response*> resps;
                    try {
                      // Send the request
                      resps = castor::srm::CastorClient::sendRequest(myLogger->getId(), *req,
                        request->rhHost(), request->rhPort(), fileIds);
                      // In case of put, we also delete the SURL as in srmRm
                      if(request->requestType() == castor::srm::REQUESTTYPE_PUT) {
                          for (std::vector<castor::srm::SubRequest*>::const_iterator srit = request->subRequests().begin();
                                srit != request->subRequests().end(); srit++) {
                              myLogger->logCnsCall("Cns_unlink", (*srit)->castorFileName(), &fileIds[(*srit)->castorFileName()], LOG_INFO);
                              int cns_rc = Cns_unlink((*srit)->castorFileName().c_str());
                              myLogger->logCnsReturn("Cns_unlink", cns_rc, &fileIds[(*srit)->castorFileName()]);
                          }
                      }
                      // handle responses coming from the stager
                      for (std::vector<castor::rh::Response*>::const_iterator it = resps.begin();
                           it != resps.end();
                           it++) {
                          castor::rh::FileResponse* fr = dynamic_cast<castor::rh::FileResponse*>((*it));
                          if ((*it)->errorCode() == 0) {
                            // abort successful, update db
                            castor::srm::SubRequest* sr = fidToSubReq[fr->fileId()];
                            if(sr) {
                              sr->setStatus(castor::srm::SUBREQUEST_ABORTED);
                              svcs->updateRep(ad, sr, false);
                              aborted++;
                            }
                          } else {
                            // abort failed, just log it
                            // "Request failed" message
                            std::list<castor::log::Param> snafu = {
                              castor::log::Param("REQID", myLogger->getId()),
                              castor::log::Param("RequestToken", request->id()),
                              castor::log::Param("FileName", fr->castorFileName()), 
                              castor::log::Param("ErrorMessage", (*it)->errorMessage())
                            };
                            castor::log::write(LOG_WARNING, "Request Failed", snafu);
                          }
                          delete fr;
                      }
                      svcs->commit(ad);
                    } catch (castor::exception::Exception e) {
                      // "Request failed" message
                      std::list<castor::log::Param> snafu = {
                        castor::log::Param("REQID", myLogger->getId()),
                        castor::log::Param("RequestToken", request->id()),
                        castor::log::Param("ErrorMessage", sstrerror(e.code())),
                        castor::log::Param("Details",  e.getMessage().str())
                      };
                      castor::log::write(LOG_ERR, "Request Failed", snafu);
                    }
                    delete req;
                    break;
                  }
                case castor::srm::REQUEST_DONE:
                    // Completed requests can't be aborted, but we still consider
                    // the abort operation successful as agreed by WLCG
                    aborted++;
                    break;
                default:
                    break;
            }
            break;
        default:
            // Should not reach here
            break;
    }

    // Fill in reponse
    if ( aborted == request->subRequests().size() ) {
        response->returnStatus->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
        response->returnStatus->explanation->assign("All subrequests have been aborted or have completed");
        request->setStatus(castor::srm::REQUEST_DONE); // All subrequests should be in a final state
        request->setHandler(0);   // this prevents the daemon from picking up the request
    }
    else if (aborted > 0) {
        response->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREPARTIAL_USCORESUCCESS;
        std::ostringstream ss;
        ss << aborted << " out of " << request->subRequests().size() << " subrequests aborted";
        response->returnStatus->explanation->assign(ss.str());
    }
    else {
        response->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
        response->returnStatus->explanation->assign("None of the subrequests could be aborted");
    }
    svcs->updateRep(ad, request, true);

    // Clean up
    for (unsigned c=0; c<request->subRequests().size(); c++)
        delete request->subRequests()[c];
    if ( request->srmUser() ) delete request->srmUser();
    delete request;
    return 0;
}

// deprecated - to be dropped
int abortSpaceReservation(castor::srm::StorageArea *request, srm__srmAbortRequestResponse *response,
        castor::Services *svcs, castor::BaseAddress *ad )
        throw (castor::exception::Exception) {
    if ( request->storageStatus() == castor::srm::STORAGESTATUS_REQUESTED ) {
        request->setStorageStatus(castor::srm::STORAGESTATUS_DEALLOCATED);
        svcs->updateRep(ad, request, true);
        response->returnStatus->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
        response->returnStatus->explanation->assign("");
    }
    else if ( request->storageStatus() == castor::srm::STORAGESTATUS_DEALLOCATED ) {
        // Ignore this 
        svcs->rollback(ad);
        response->returnStatus->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
        response->returnStatus->explanation->assign("");
    }
    else {
        // Space already allocated - cannot abort
        svcs->rollback(ad);
        response->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
        response->returnStatus->explanation->assign("Space already allocated");
    }

    // Clean up
    if (request->srmUser()) delete request->srmUser();
    delete request;
    return 0;
}

int srm__srmAbortRequest(
        struct soap *soap,
        srm__srmAbortRequestRequest*        srmAbortRequestRequest,
        srm__srmAbortRequestResponse_& response) //< response parameter
{
    castor::srm::server::SrmLogger *myLogger = new castor::srm::server::SrmLogger();
    myLogger->LOGENTRY(soap);
    srm__srmAbortRequestResponse *resp;
    try {
        resp = soap_new_srm__srmAbortRequestResponse(soap, -1);
        resp->returnStatus = soap_new_srm__TReturnStatus(soap, -1);
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
        resp->returnStatus->explanation = soap_new_std__string(soap, -1);
        resp->returnStatus->explanation->assign("");
    }
    catch (...) {
        std::list<castor::log::Param> params = {
            castor::log::Param("REQID", myLogger->getId()),
            castor::log::Param("Message", "Out of memory")
        };
        castor::log::write(LOG_ALERT, "Exception caught", params);
        delete myLogger;
        return SOAP_EOM;
    }
    response.srmAbortRequestResponse = resp;
    castor::srm::server::ResponseWriter myWriter;

    // Set up database services
    castor::Services* svcs;
    castor::srm::ISrmSvc* srmSvc;
    castor::BaseAddress ad;
    try {
        svcs = castor::BaseObject::services();
        ad.setCnvSvcName("DbCnvSvc");
        ad.setCnvSvcType(castor::SVC_DBCNV);
        castor::IService *svc = castor::BaseObject::services()->service("DbSrmSvc", castor::SVC_DBSRMSVC);
        srmSvc = dynamic_cast<castor::srm::ISrmSvc*>(svc);
        if ( srmSvc == 0 ) {
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
            resp->returnStatus->explanation->assign("Unable to establish database service");
            std::list<castor::log::Param> params = {castor::log::Param("REQID", myLogger->getId())};
            castor::log::write(LOG_ERR, "Could not get Conversion Service for Database", params);
            myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
            delete myLogger;
            myWriter.write(response);
            return SOAP_OK;
        }
    }
    catch (castor::exception::Exception e) {
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        resp->returnStatus->explanation->assign( e.getMessage().str());
        myLogger->LOGEXCEPTION(&e, &ad);
        myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
        delete myLogger;
        myWriter.write(response);
        return SOAP_OK;
    }

    castor::srm::server::RequestValidator validate;
    castor::srm::server::SrmUtils *utils = new castor::srm::server::SrmUtils();
    castor::srm::StageRequest *request = 0;
    castor::srm::StorageArea  *area    = 0;
    std::string dn;
    std::string uname;
    std::string vo;

    try {
        validate.requestToken(srmAbortRequestRequest->requestToken);
        try {
            // Get the original request on which we are to operate
            // We enclose this validation in its own block since we may be dealing
            // with a space reservation request.  This is tested in the catch block.
            request = srmSvc->getStageRequest( srmAbortRequestRequest->requestToken);
            validate.request(request);
        } catch (castor::exception::Exception e) {
            try {
                area = srmSvc->getStorageArea(srmAbortRequestRequest->requestToken);
                validate.storageArea(area, false);
            } catch (castor::exception::Exception x) {
                // If we get this far we know the request token is neither a storarge area
                // or any other async request.
                resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST;
                resp->returnStatus->explanation->assign(e.getMessage().str());
                myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
                myWriter.write(response);
                delete myLogger;
                delete utils;
                return SOAP_OK;
            }
        }
        dn    = utils->getDn(soap);
        uname = utils->getUserName(soap);
        vo    = utils->getVoName(soap);
        if (request) {
            svcs->fillObj(&ad, request, castor::srm::OBJ_SrmUser, false);
            validate.permission( uname, dn, 
                    request->srmUser()->userID(), request->srmUser()->dn());
        }
        else if (area) {
            svcs->fillObj(&ad, area, castor::srm::OBJ_SrmUser, false);
            validate.permission(area, vo);
        }
        else {
            // We shouild never get here
            castor::exception::Exception x( (const int)srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST );
            x.getMessage() << "Request token does not match any known request";
            throw x;
        }

        // perform the request
        if (request) {
            myLogger->setId(request->srmRequestId());
            myLogger->LOGSTARTEDREQ(1);
            // Increment stager calls
            castor::srm::server::StageIncrementer inc;
            inc.increment();
            abortStageRequest(request, resp, svcs, &ad, srmSvc, myLogger, soap, utils);
        }
        else if (area) {
            myLogger->setId(area->spaceToken());
            myLogger->LOGSTARTEDREQ(1);
            abortSpaceReservation(area, resp, svcs, &ad);
        }
    } catch (castor::exception::Exception &e) {
        castor::srm::SrmException* se = dynamic_cast<castor::srm::SrmException*>(&e);
        if(se) {
            resp->returnStatus->statusCode = (srm__TStatusCode)se->code();
        } else {
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        }
        resp->returnStatus->explanation->assign(e.getMessage().str());
    }

    // Send response and cleanup
    myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
    myWriter.write(response);
    delete myLogger;
    delete utils;
    return SOAP_OK;
}
