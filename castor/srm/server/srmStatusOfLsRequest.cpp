#include <castor/log/log.hpp>

#include "soapH.h"
#include "castor/srm/server/SrmLogger.hpp"
#include "castor/srm/server/ResponseWriter.hpp"

int srm__srmStatusOfLsRequest(
        struct soap *soap,
        srm__srmStatusOfLsRequestRequest*,
        srm__srmStatusOfLsRequestResponse_& response) //< response parameter
{
    castor::srm::server::SrmLogger *myLogger = new castor::srm::server::SrmLogger();
    myLogger->LOGENTRY(soap);

    castor::srm::server::ResponseWriter myWriter;

    srm__srmStatusOfLsRequestResponse *resp;
    try {
        resp = soap_new_srm__srmStatusOfLsRequestResponse(soap, -1);
        resp->details = NULL;
        resp->returnStatus = soap_new_srm__TReturnStatus( soap, -1 );
        resp->returnStatus->explanation = soap_new_std__string( soap, -1 );
    }
    catch (...) {
        std::list<castor::log::Param> params = {
            castor::log::Param("REQID", myLogger->getId()),
            castor::log::Param("Message", "Out of memory")
        };
        castor::log::write(LOG_ALERT, "Exception caught", params);
        myLogger->LOGEXIT(srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR, "Out of memory");
        delete myLogger;
        return SOAP_EOM;
    }

    resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCORENOT_USCORESUPPORTED;
    resp->returnStatus->explanation->assign("Not supported");
    response.srmStatusOfLsRequestResponse = resp;
    myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
    myWriter.write(response);
    delete myLogger;
    return SOAP_OK;
}


