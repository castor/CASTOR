#include <cgsi_plugin.h>

#include <stager_client_api.h>
#include <Cns_api.h>

#include <castor/BaseAddress.hpp>
#include <castor/BaseObject.hpp>
#include <castor/Constants.hpp>
#include <castor/Services.hpp>
#include <castor/exception/Exception.hpp>
#include <castor/log/log.hpp>

#include "castor/srm/ISrmSvc.hpp"
#include "castor/srm/SURL.hpp"
#include "castor/srm/server/SrmUtils.hpp"
#include "castor/srm/server/SrmLogger.hpp"
#include "castor/srm/SrmConstants.hpp"
#include "castor/srm/server/ResponseWriter.hpp"
#include "castor/srm/server/RequestValidator.hpp"
#include "castor/srm/server/StageIncrementer.hpp"
#include "soapH.h"
#include <vector>

/**
 * This function makes a database call which will try and ABORT outstanding PUT requests
 * for a given file.  This cas basically be done at any point before a putDone is issued.
 * Takes two arguments, the name of the file and the logger for logging purposes.
 */
void srmRm_abortPutRequest (std::string filepath, castor::srm::server::SrmLogger *myLogger, Cns_fileid nsId) throw (castor::exception::Exception) {
    try {
        castor::srm::ISrmSvc* srmSvc;
        castor::BaseAddress ad;
        ad.setCnvSvcName("DbCnvSvc");
        ad.setCnvSvcType(castor::SVC_DBCNV);
        castor::IService *svc = castor::BaseObject::services()->service("DbSrmSvc", castor::SVC_DBSRMSVC);
        srmSvc = dynamic_cast<castor::srm::ISrmSvc*>(svc);
        if ( srmSvc == 0 ) {
            // Dont throw an exception - we log the problem and just ignore it.
            std::list<castor::log::Param> p = {
                castor::log::Param("NSFILEID", nsId.fileid),
                castor::log::Param("REQID", myLogger->getId()),
                castor::log::Param ("Message", "Unable to abort put requests associated with srmRm request"),
            };
            castor::log::write(LOG_ERR, "Could not get Conversion Service for Database", p);
            return;
        }
        std::list<castor::log::Param> p = {
          castor::log::Param("NSFILEID", nsId.fileid),
          castor::log::Param("REQID", myLogger->getId()),
       };
        castor::log::write(LOG_DEBUG, "Aborting put requests associated with srmRm request", p);
        srmSvc->abortPutRequestByFile(filepath);
    }
    catch (castor::exception::Exception e) {
        std::list<castor::log::Param> p = {
            castor::log::Param("NSFILEID", nsId.fileid),
            castor::log::Param("REQID", myLogger->getId()),
            castor::log::Param ("Message", "Unable to abort put requests associated with srmRm request"),
            castor::log::Param ("Details", e.getMessage().str()),
        };
        castor::log::write(LOG_ERR, "Could not get Conversion Service for Database", p);
    }
}

int srm__srmRm(
        struct soap *soap,
        srm__srmRmRequest*                  srmRmRequest,
        srm__srmRmResponse_         &rep) //< response parameter
{
    castor::srm::server::SrmLogger *myLogger = new castor::srm::server::SrmLogger();
    myLogger->LOGENTRY(soap);

    castor::srm::server::SrmUtils *utils = new castor::srm::server::SrmUtils();

    srm__srmRmRequest *req;
    srm__srmRmResponse *resp;
    req = srmRmRequest;

    uid_t uid;
    gid_t gid;
    utils->map_user(soap, &uid, &gid);

    castor::srm::server::ResponseWriter myWriter;
    //Prepare the reply
    try {
        resp = soap_new_srm__srmRmResponse(soap, -1);
        resp->arrayOfFileStatuses = soap_new_srm__ArrayOfTSURLReturnStatus(soap, -1);
        resp->returnStatus = soap_new_srm__TReturnStatus(soap, -1);
        resp->returnStatus->explanation = soap_new_std__string(soap, -1);

        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
        resp->returnStatus->explanation->assign("");
    }
    catch (...) {
        std::list<castor::log::Param> params = {
            castor::log::Param("REQID", myLogger->getId()),
            castor::log::Param("Message", "Out of memory")
        };
        castor::log::write(LOG_ALERT, "Exception caught", params);
        utils->unmap_user();
        myLogger->LOGEXIT(srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR, "Out of memory");
        delete utils;
        delete myLogger;
        return SOAP_EOM;
    }
    //Put resp inside rep struct container
    rep.srmRmResponse = resp;

    castor::srm::server::RequestValidator validate;
    castor::srm::server::StageIncrementer inc;
    std::string                   stagerHost;
    try {
        inc.increment();
        stagerHost = utils->getStagerHost(soap);
        validate.notNull(req->arrayOfSURLs, "arrayOfSURLs");
        validate.arrayOfSurls(req->arrayOfSURLs->urlArray);
    } catch (castor::exception::Exception &e) {
        castor::srm::SrmException* se = dynamic_cast<castor::srm::SrmException*>(&e);
        if(se) {
            resp->returnStatus->statusCode = (srm__TStatusCode)se->code();
        } else {
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        }
        resp->returnStatus->explanation->assign(e.getMessage().str());
        myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
        myWriter.write(rep);
        utils->unmap_user();
        delete utils;
        delete myLogger;
        return SOAP_OK;
    }

    int nFailed = 0; // Counter for the number of requests that have failed

    //Call the Castor function to do the rm for all files & return success/error
    std::map<std::string, Cns_fileid> validFiles;
    for (unsigned i=0; i < req->arrayOfSURLs->urlArray.size(); i++) {
        srm__TSURLReturnStatus *retstatus = soap_new_srm__TSURLReturnStatus(soap, -1);
        retstatus->status = soap_new_srm__TReturnStatus(soap, -1);
        retstatus->status->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
        retstatus->status->explanation = soap_new_std__string(soap, -1);
        retstatus->status->explanation->assign("Unknown error");
        retstatus->surl = req->arrayOfSURLs->urlArray[i];
        std::string filepath;
        try {
            castor::srm::SURL* surl = validate.surl(req->arrayOfSURLs->urlArray[i]);
            filepath = surl->getFileName();
            delete surl;
        } catch (castor::exception::Exception e) {
            retstatus->status->statusCode = (srm__TStatusCode)e.code();
            retstatus->status->explanation->assign(e.getMessage().str());
            nFailed++;
            continue;
        }
        // Here we stat the file to get its fileid for logging purposes. If the stat fails,
        // we fail the request as there is no point in trying the unlink afterwards.
        struct Cns_filestat stat;
        Cns_fileid  nsId;
        nsId.fileid=0;
        strncpy(nsId.server, castor::srm::SrmConstants::getInstance()->nsHost().c_str(), sizeof(nsId.server)-1);
        myLogger->logCnsCall("Cns_stat", filepath.c_str());
        int myRC = Cns_stat(filepath.c_str(),&stat);
        myLogger->logCnsReturn("Cns_stat", myRC, &nsId);
        if ( myRC != 0 ) {
            switch (serrno) {
                case EACCES:
                    retstatus->status->explanation->assign(sstrerror(serrno));
                    retstatus->status->statusCode = srm__TStatusCode__SRM_USCOREAUTHORIZATION_USCOREFAILURE;
                    nFailed++;
                    break;
                case ENOTDIR:
                case ENOENT:
                case ENAMETOOLONG:
                    retstatus->status->explanation->assign(sstrerror(serrno));
                    retstatus->status->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREPATH;
                    nFailed++;
                    break;
                case EFAULT:
                case ENSNACT:
                case SENOSHOST:
                case SENOSSERV:
                case SECOMERR:
                default:
                    retstatus->status->explanation->assign(sstrerror(serrno));
                    retstatus->status->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
                    nFailed++;
                    break;
            }
        }
        else {
            nsId.fileid = stat.fileid;
            validFiles[filepath] = nsId;
        }
        resp->arrayOfFileStatuses->statusArray.push_back(retstatus);
    }

    // If no valid SURLs supplied - fail the request
    if (validFiles.empty()) {
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST;
        resp->returnStatus->explanation->assign("No valid SURLs supplied");
        utils->unmap_user();
        myWriter.write(rep);
        myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
        delete utils;
        delete myLogger;
        return SOAP_OK;
    }
    // else we proceed - and log how many valid files we are going to process
    myLogger->LOGSTARTEDREQ(validFiles.size());
    // create a request to hold the valid SURLs
    struct stage_filereq *filereq = 0;
    int rc = create_filereq( &filereq, validFiles.size() );
    int c=0;
    for ( std::map<std::string, Cns_fileid>::iterator i = validFiles.begin(); i != validFiles.end(); i++, c++ ) {
        filereq[c].filename = strdup(i->first.c_str());
    }
    struct stage_options opts;
    opts.stage_host = strdup(const_cast<char*> (stagerHost.c_str()));
    opts.stage_port = STAGERPORT;
    opts.service_class = strdup("*");
    struct stage_fileresp *fileresp;
    int nResp;
    char *reqId = 0;
    // call Castor
    myLogger->logStagerCall("stage_rm", opts);
    rc = stage_rm(filereq, validFiles.size(), &fileresp, &nResp, &reqId, &opts);
    myLogger->logStagerReturn("stage_rm", rc, reqId ? reqId : "");
    // Free stuff up
    if ( opts.stage_host ) free( opts.stage_host );
    free (opts.service_class);
    if ( rc != 0 ) {
        // Log the response but carry on and do the nameserver removal...
      std::list<castor::log::Param> params = {castor::log::Param("REQID", myLogger->getId())};
      castor::log::write(LOG_WARNING, "Exception caught : ignored" , params);
    }

    if (rc == 0) {
        // Loop over all responses -
        // 1.  If all failed - fail the request
        // 2.  If partial success indicate this
        // 3.  If all OK, carry on
        for ( int i=0; i<nResp;i++ ) {
            if ( fileresp[i].errorCode != 0 ) {
                nFailed++;
            }
        }
        if ((unsigned)nResp != validFiles.size() ) {
            // This should not happen, but log it and carry on
            std::list<castor::log::Param> snafu = {
                castor::log::Param("REQID", myLogger->getId()),
                castor::log::Param("Message", "stager_rm error: Number requested not equals to numer of responses"),
                castor::log::Param("CastorReqId", reqId),
                castor::log::Param("nRequests", req->arrayOfSURLs->urlArray.size()),
                castor::log::Param("nResponses", nResp)
            };
            castor::log::write(LOG_WARNING, "Stager returned", snafu);
        }
        else if ( nFailed == nResp ) {
            // All requests failed, but we wil just log it and blindly continue
            std::list<castor::log::Param> snafu = {
                castor::log::Param("REQID", myLogger->getId()),
                castor::log::Param("Message", "stager_rm error: All requests failed"),
                castor::log::Param("CastorReqId", reqId),
            };
            castor::log::write(LOG_WARNING, "Stager returned", snafu);
        }
        else if ( nFailed > 0 ) {
            // Some failed, but again we blindly crry on....
            std::list<castor::log::Param> snafu = {
                castor::log::Param("REQID", myLogger->getId()),
                castor::log::Param("Message", "stager_rm error: Not all requests succeeded"),
                castor::log::Param("CastorReqId", reqId),
                castor::log::Param("nRequests", req->arrayOfSURLs->urlArray.size()),
                castor::log::Param("nFailed", nFailed)
            };
            castor::log::write(LOG_WARNING, "Stager returned", snafu);
        }
        else {
            // Everything worked pretty much OK....
        }
        free (reqId);
        free_fileresp(fileresp, nResp);
    }
    // Free request regardless
    free_filereq(filereq, validFiles.size());

    // Now we try and do namespace removal...
    unsigned nOK = 0;
    for ( std::map<std::string, Cns_fileid>::iterator i = validFiles.begin(); i != validFiles.end(); i++ ) {
        // Find the right subresponse
        srm__TSURLReturnStatus *myStatus = 0;
        for ( unsigned j=0; j< resp->arrayOfFileStatuses->statusArray.size(); j++ ) {
            // We need to compare file response SURL with the filename.  All SURL should be valid
            // at this point, but we will check.  If they aren't then we just go back and ignore
            // this one
            if ( resp->arrayOfFileStatuses->statusArray[j]->status->statusCode == srm__TStatusCode__SRM_USCOREINVALID_USCOREPATH ) continue;
            std::string surlFile;
            try {
                surlFile = castor::srm::SURL( resp->arrayOfFileStatuses->statusArray[j]->surl,
                        req->authorizationID, soap).getFileName();
            }
            catch (...) {
                break;
            }
            if ( surlFile.compare( i->first ) == 0 ) {
                myStatus = resp->arrayOfFileStatuses->statusArray[j];
                break;
            }
        }
        if (0 == myStatus) continue;
        serrno=0;
        std::string filepath = castor::srm::SURL(myStatus->surl, req->authorizationID, soap).getFileName();
        myLogger->logCnsCall("Cns_unlink", filepath, &(i->second), LOG_INFO);
        rc = Cns_unlink(filepath.c_str());
        myLogger->logCnsReturn("Cns_unlink", rc, &(i->second));
        if (rc != 0) {
            switch (serrno) {
                case ENOENT:
                    srmRm_abortPutRequest (filepath, myLogger, i->second);
                    // No break here since we want to fall through anyway
                case EFAULT:
                case EEXIST:
                case ENOTDIR:
                case ENAMETOOLONG:
                    myStatus->status->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREPATH;
                    myStatus->status->explanation->assign(sstrerror(serrno));
                    break;
                case EPERM:
                case EACCES:
                    myStatus->status->statusCode = srm__TStatusCode__SRM_USCOREAUTHORIZATION_USCOREFAILURE;
                    myStatus->status->explanation->assign(sstrerror(serrno));
                    break;
                case SENOSHOST:
                case SENOSSERV:
                case SECOMERR:
                case ENSNACT:
                    myStatus->status->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
                    myStatus->status->explanation->assign(sstrerror(serrno));
                    break;
                default:
                    myStatus->status->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
                    myStatus->status->explanation->assign("Unknown error");
                    break;
            }
        }
        else {
            myStatus->status->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
            myStatus->status->explanation->assign("");
            // We now also need to abort any outstanding PUT requests for this file
            srmRm_abortPutRequest (filepath, myLogger, i->second);
            nOK++;
        }
    }

    if ( nOK == 0 ) {
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
        resp->returnStatus->explanation->assign("No files removed");
    }
    else if ( nOK == req->arrayOfSURLs->urlArray.size() ) {
        // Do nothing - already in correct status
    }
    else {
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREPARTIAL_USCORESUCCESS;
    }

    utils->unmap_user();
    myWriter.write(rep);
    delete utils;
    myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
    delete myLogger;
    return SOAP_OK;
}
