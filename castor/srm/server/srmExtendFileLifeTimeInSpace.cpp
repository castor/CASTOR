#include <cgsi_plugin.h>
#include <castor/log/log.hpp>
#include "soapH.h"
#include "castor/srm/server/SrmLogger.hpp"
#include "castor/srm/server/ResponseWriter.hpp"

int srm__srmExtendFileLifeTimeInSpace (
        struct soap *soap,
        srm__srmExtendFileLifeTimeInSpaceRequest*,
        srm__srmExtendFileLifeTimeInSpaceResponse_  &myResponse)
{
    castor::srm::server::SrmLogger *myLogger = new castor::srm::server::SrmLogger();
    myLogger->LOGENTRY(soap);

    srm__srmExtendFileLifeTimeInSpaceResponse *resp;
    try {
        resp = soap_new_srm__srmExtendFileLifeTimeInSpaceResponse(soap, -1);
        resp->returnStatus = soap_new_srm__TReturnStatus( soap, -1 );
        resp->returnStatus->explanation = soap_new_std__string( soap, -1 );
        resp->arrayOfFileStatuses = NULL;
    }
    catch (...) {
        std::list<castor::log::Param> params = {
            castor::log::Param("REQID", myLogger->getId()),
            castor::log::Param("Message", "Out of memory")
        };
        castor::log::write(LOG_ALERT, "Exception caught", params);
        myLogger->LOGEXIT(srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR, "Out of memory");
        delete myLogger;
        return SOAP_EOM;
    }

    resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCORENOT_USCORESUPPORTED;
    resp->returnStatus->explanation->assign("Not supported");
    myResponse.srmExtendFileLifeTimeInSpaceResponse = resp;
    myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
    delete myLogger;
    castor::srm::server::ResponseWriter myWriter;
    myWriter.write(myResponse);
    return SOAP_OK;
}

