#include <iostream>
#include <map>
#include <grp.h>
#include <sys/types.h>

#include <Cns_api.h>
#include <castor/log/log.hpp>
#include <castor/exception/Exception.hpp>

#include "soapH.h"
#include "castor/srm/server/SrmUtils.hpp"
#include "castor/srm/server/SrmLogger.hpp"
#include "castor/srm/SrmConstants.hpp"
#include "castor/srm/SURL.hpp"
#include "castor/srm/server/ResponseWriter.hpp"
#include "castor/srm/server/RequestValidator.hpp"

void handleError( srm__srmSetPermissionResponse* );


int srm__srmSetPermission(
        struct soap *soap,
        srm__srmSetPermissionRequest*       srmSetPermissionRequest,
        srm__srmSetPermissionResponse_      &response) //< response parameter
{
    castor::srm::server::SrmLogger *myLogger = new castor::srm::server::SrmLogger();
    myLogger->LOGENTRY(soap);
    castor::srm::server::SrmUtils *utils = new castor::srm::server::SrmUtils();

    castor::srm::server::ResponseWriter myWriter;
    srm__srmSetPermissionResponse *resp;
    try {
        resp = soap_new_srm__srmSetPermissionResponse(soap, -1);
        resp->returnStatus = soap_new_srm__TReturnStatus(soap, -1);
        resp->returnStatus->explanation = soap_new_std__string(soap, -1);

        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
        resp->returnStatus->explanation->assign("");
    }
    catch (...) {
        myLogger->LOGEXIT(srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR, "Out of memory");
        delete myLogger;
        return SOAP_EOM;
    }
    response.srmSetPermissionResponse = resp;

    castor::srm::server::RequestValidator validate;

    // Get the permission type now since we can simplify code for 
    // removal
    srm__TPermissionType permissionType = srmSetPermissionRequest->permissionType;

    // If all permissions are nill, this is an invalid request
    bool valid=true;
    if ( srmSetPermissionRequest->ownerPermission == NULL &&
            srmSetPermissionRequest->arrayOfUserPermissions  == NULL &&
            srmSetPermissionRequest->arrayOfGroupPermissions == NULL &&
            srmSetPermissionRequest->otherPermission == NULL ) {
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST;
        resp->returnStatus->explanation->assign("No permissions specified");
        valid=false;
    }
    else if ( srmSetPermissionRequest->SURL.length() == 0 ) { 
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST;
        resp->returnStatus->explanation->assign("Path not supplied");
        valid=false;
    }
    else if ( srmSetPermissionRequest->ownerPermission != NULL ) {
        if ( srmSetPermissionRequest->permissionType == srm__TPermissionType__REMOVE ) {
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST;
            resp->returnStatus->explanation->assign("Attempt to remove owner from permissions list");
            valid=false;
        }
        else if ( srmSetPermissionRequest->permissionType == srm__TPermissionType__ADD ) {
            permissionType = srm__TPermissionType__CHANGE;
        }
    }
    if (!valid) {
        myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
        myWriter.write(response);
        delete utils;
        delete myLogger;
        return SOAP_OK;
    }

    // End of Modification

    // Map the the user
    uid_t uid;
    gid_t gid;

    utils->map_user(soap, &uid, &gid);

    // Get castor filename
    std::string cfn;
    try {
        castor::srm::SURL* surl = validate.surl(srmSetPermissionRequest->SURL);
        cfn = surl->getFileName();
        delete surl;
    } catch(castor::exception::Exception &e) {
        resp->returnStatus->statusCode = (srm__TStatusCode)e.code();
        resp->returnStatus->explanation->assign(e.getMessage().str());
        utils->unmap_user();
        myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
        myWriter.write(response);
        delete utils;
        delete myLogger;
        return SOAP_OK;
    }

    // Make sure file exists and get the file id
    Cns_filestat stat;
    myLogger->logCnsCall("Cns_stat", cfn);
    int statrc = Cns_stat(cfn.c_str(), &stat);
    myLogger->logCnsReturn("Cns_stat", statrc);
    if ( statrc != 0 ) {
        handleError(resp);
        utils->unmap_user();
        delete utils;
        myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
        delete myLogger;
        myWriter.write(response);
        return SOAP_OK;
    }
    Cns_fileid nsId;
    nsId.fileid = stat.fileid;
    strncpy(nsId.server, castor::srm::SrmConstants::getInstance()->nsHost().c_str(), sizeof(nsId.server)-1);

    // After this we should be OK.
    // Get the current permissions from the stat structure and the current ACL std::list
    Cns_acl *currPermissions = NULL;
    int nCurrentPermissions = CA_MAXACLENTRIES;
    currPermissions = (Cns_acl*)malloc(nCurrentPermissions*sizeof(Cns_acl));
    myLogger->logCnsCall("Cns_getacl", cfn, &nsId, LOG_INFO);
    nCurrentPermissions = Cns_getacl(cfn.c_str(), nCurrentPermissions, currPermissions);
    if (nCurrentPermissions >= 0 )
        myLogger->logCnsReturn("Cns_getacl", 0, &nsId);
    else
        myLogger->logCnsReturn("Cns_getacl", nCurrentPermissions, &nsId);
    std::vector<Cns_acl> existingPerms;
    std::map<int, Cns_acl> userIds;
    std::map<int, Cns_acl> groupIds;
    bool hasACLMask = false;
    for ( int i=0; i<nCurrentPermissions; i++) {
        if ( currPermissions[i].a_type == CNS_ACL_MASK ) {
            hasACLMask = true;
            currPermissions[i].a_perm = 07;
        }
        if ( currPermissions[i].a_type == CNS_ACL_USER || currPermissions[i].a_type == CNS_ACL_USER_OBJ ||
                currPermissions[i].a_type == (CNS_ACL_DEFAULT | CNS_ACL_USER) ||
                currPermissions[i].a_type == (CNS_ACL_DEFAULT | CNS_ACL_USER_OBJ) ) {
            userIds[currPermissions[i].a_id] = currPermissions[i];
        }
        if ( currPermissions[i].a_type == CNS_ACL_GROUP || currPermissions[i].a_type == CNS_ACL_GROUP_OBJ ||
                currPermissions[i].a_type == (CNS_ACL_DEFAULT | CNS_ACL_GROUP) ||
                currPermissions[i].a_type == (CNS_ACL_DEFAULT | CNS_ACL_GROUP_OBJ) ) {
            groupIds[currPermissions[i].a_id] = currPermissions[i];
        }
        existingPerms.push_back(currPermissions[i]);
    }
    free (currPermissions);
    if (!hasACLMask) {
        // Set up a mask ACL assuming perm mode of 7
        Cns_acl maskacl;
        maskacl.a_type = CNS_ACL_MASK;
        maskacl.a_perm = 7;
        maskacl.a_id = 0;

        existingPerms.push_back(maskacl);

        Cns_acl *myACLs = (Cns_acl*) malloc(existingPerms.size() * sizeof(Cns_acl));
        for ( unsigned i=0; i<existingPerms.size(); i++) {
            myACLs[i].a_type = existingPerms[i].a_type;
            myACLs[i].a_id   = existingPerms[i].a_id;
            myACLs[i].a_perm = existingPerms[i].a_perm;
        }
        myLogger->logCnsCall("Cns_setacl", cfn, &nsId, LOG_INFO);
        int saclrc = Cns_setacl(cfn.c_str(), existingPerms.size(), myACLs);
        myLogger->logCnsReturn("Cns_setacl", saclrc, &nsId);
        if ( saclrc != 0 ) {
            handleError(resp);
            utils->unmap_user();
            delete utils;
            myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
            delete myLogger;
            myWriter.write(response);
            return SOAP_OK;
        }
        free (myACLs);
    } // End of setting up ACLs


    //=======================================================
    // Owner Permsission
    //=======================================================

    if ( srmSetPermissionRequest->ownerPermission ) {
        Cns_acl acl;
        acl.a_type = CNS_ACL_USER_OBJ;
        acl.a_id   = stat.uid;
        switch ((int)*(srmSetPermissionRequest->ownerPermission)) {
            case srm__TPermissionMode__NONE:
            case srm__TPermissionMode__R:
                acl.a_perm = S_IROTH;
                break;
            case srm__TPermissionMode__X:
                acl.a_perm = S_IXOTH;
                break;
            case srm__TPermissionMode__W:
                acl.a_perm = S_IWOTH;
                break;
            case srm__TPermissionMode__WX:
                acl.a_perm = S_IWOTH | S_IXOTH;
                break;
            case srm__TPermissionMode__RX:
                acl.a_perm = S_IROTH | S_IXOTH;
                break;
            case srm__TPermissionMode__RW:
                acl.a_perm = S_IROTH | S_IWOTH;
                break;
            case srm__TPermissionMode__RWX:
                acl.a_perm = S_IROTH | S_IWOTH | S_IXOTH;
                break;
        }
        //cout << "permissionType=" << permissionType << endl;
        switch (permissionType) {
            case srm__TPermissionType__CHANGE:
                {
                    // See if we have an existing user.  If we do, we need to
                    // modify their entry, if not, create new entry
                    std::map<int, Cns_acl>::iterator mapiter = userIds.find(uid);
                    if ( mapiter == userIds.end() ) {
                        existingPerms.push_back(acl);
                    }
                    // We have an existing user.  When going through existing 
                    // perms we need to make sure we distinguish between group
                    // and user id
                    std::vector<Cns_acl>::iterator iter = existingPerms.begin();
                    while ( iter != existingPerms.end() ) {
                        if ( ( ((*iter).a_type == (CNS_ACL_DEFAULT | CNS_ACL_USER)) ||
                                    ((*iter).a_type == (CNS_ACL_DEFAULT | CNS_ACL_USER_OBJ))
                             ) &&
                                (*iter).a_id == (int)uid
                           ) {
                            existingPerms.erase(iter);
                            existingPerms.push_back(acl);
                            break;
                        }
                        *iter++;
                    }
                }
                break;
            default:
                resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST;
                resp->returnStatus->explanation->assign("Invalid operation on owner");
                utils->unmap_user();
                myWriter.write(response);
                myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
                delete utils;
                delete myLogger;
                return SOAP_OK;
        }
    }


    //=======================================================
    // User Permsission
    //=======================================================
    if (srmSetPermissionRequest->arrayOfUserPermissions) {
        std::vector<srm__TUserPermission*> pVector = srmSetPermissionRequest->arrayOfUserPermissions->userPermissionArray;
        for ( unsigned i=0; i<pVector.size(); i++ ) {
            uid_t uid = 0;
            gid_t gid = 0;
            utils->getUserAndGroup(pVector[i]->userID,
                    &uid, &gid);
            // Create a default acl - we can always OR any existing values
            Cns_acl acl;
            acl.a_type = CNS_ACL_USER;
            acl.a_id = uid;
            switch (pVector[i]->mode) {
                case srm__TPermissionMode__NONE:
                case srm__TPermissionMode__R:
                    acl.a_perm = S_IROTH;
                    break;
                case srm__TPermissionMode__X:
                    acl.a_perm = S_IXOTH;
                    break;
                case srm__TPermissionMode__W:
                    acl.a_perm = S_IWOTH;
                    break;
                case srm__TPermissionMode__WX:
                    acl.a_perm = S_IWOTH | S_IXOTH;
                    break;
                case srm__TPermissionMode__RX:
                    acl.a_perm = S_IROTH | S_IXOTH;
                    break;
                case srm__TPermissionMode__RW:
                    acl.a_perm = S_IROTH | S_IWOTH;
                    break;
                case srm__TPermissionMode__RWX:
                    acl.a_perm = S_IROTH | S_IWOTH | S_IXOTH;
                    break;
            }
            switch (permissionType) {
                case srm__TPermissionType__ADD:
                    {
                        std::map<int, Cns_acl>::iterator iter = userIds.find(uid);
                        if ( iter != userIds.end() ) {
                            // Modify existing ACL
                            userIds[uid] = acl;
                        }
                        else {
                            // Add new acl
                            existingPerms.push_back(acl);
                        }
                    }
                    break;
                case srm__TPermissionType__REMOVE:
                    {
                        // Silenetly ignore non existent users.  
                        std::vector<Cns_acl>::iterator iter = existingPerms.begin();
                        while ( iter != existingPerms.end() ) {
                            if ( ( ((*iter).a_type == (CNS_ACL_DEFAULT | CNS_ACL_USER)) ||
                                        ((*iter).a_type == (CNS_ACL_DEFAULT | CNS_ACL_USER_OBJ))
                                 ) &&
                                    (*iter).a_id == (int)uid
                               ) {
                                existingPerms.erase(iter);
                                break;
                            }
                            *iter++;
                        }
                    }
                    break;
                case srm__TPermissionType__CHANGE:
                    {
                        // See if we have an existing user.  If we do, we need to
                        // modify their entry, if not, create new entry
                        std::map<int, Cns_acl>::iterator mapiter = userIds.find(uid);
                        if ( mapiter == userIds.end() ) {
                            existingPerms.push_back(acl);
                        }
                        // We have an existing user.  When going through existing 
                        // perms we need to make sure we distinguish between group
                        // and user id
                        std::vector<Cns_acl>::iterator iter = existingPerms.begin();
                        while ( iter != existingPerms.end() ) {
                            if ( ( ((*iter).a_type == (CNS_ACL_DEFAULT | CNS_ACL_USER)) ||
                                        ((*iter).a_type == (CNS_ACL_DEFAULT | CNS_ACL_USER_OBJ))
                                 ) &&
                                    (*iter).a_id == (int)uid
                               ) {
                                (*iter).a_perm = acl.a_perm;
                                break;
                            }
                            *iter++;
                        }
                    }
                    break;
            }
        }
    }

    //=======================================================
    // Group Permsission
    //=======================================================
    if ( srmSetPermissionRequest->arrayOfGroupPermissions ) {
        std::vector<srm__TGroupPermission*> pVector = srmSetPermissionRequest->arrayOfGroupPermissions->groupPermissionArray;
        for ( unsigned i=0; i<pVector.size(); i++ ) {
            // uid_t authorizationID = 0;
            // gid_t groupID = 0;
            // utils.getUserAndGroup(pVector[i]->groupID->value,
            //         &authorizationID, &groupID);
            struct group *g = getgrnam(pVector[i]->groupID.c_str());
            if ( g == NULL ) {
                // Error condition - user already exists
                resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST;
                std::stringstream ss;
                ss << "Group " << pVector[i]->groupID.c_str() << " does not exist";
                resp->returnStatus->explanation->assign(ss.str());
                utils->unmap_user();
                delete utils;
                myWriter.write(response);
                myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
                delete myLogger;
                return SOAP_OK;
            }
            Cns_acl acl;
            acl.a_type = CNS_ACL_GROUP;
            acl.a_id = g->gr_gid;
            switch (pVector[i]->mode) {
                case srm__TPermissionMode__NONE:
                case srm__TPermissionMode__R:
                    acl.a_perm = S_IROTH;
                    break;
                case srm__TPermissionMode__X:
                    acl.a_perm = S_IXOTH;
                    break;
                case srm__TPermissionMode__W:
                    acl.a_perm = S_IWOTH;
                    break;
                case srm__TPermissionMode__WX:
                    acl.a_perm = S_IWOTH | S_IXOTH;
                    break;
                case srm__TPermissionMode__RX:
                    acl.a_perm = S_IROTH | S_IXOTH;
                    break;
                case srm__TPermissionMode__RW:
                    acl.a_perm = S_IROTH | S_IWOTH;
                    break;
                case srm__TPermissionMode__RWX:
                    acl.a_perm = S_IROTH | S_IWOTH | S_IXOTH;
                    break;
            }
            switch (permissionType) {
                case srm__TPermissionType__ADD:
                    {
                        std::map<int, Cns_acl>::iterator iter = groupIds.find(acl.a_id);
                        if ( iter != groupIds.end() ) {
                            // Error condition - user already exists
                            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST;
                            std::stringstream ss;
                            ss << "Attempt to add group " << pVector[i]->groupID.c_str() << " failed: group exists";
                            resp->returnStatus->explanation->assign(ss.str());
                            utils->unmap_user();
                            delete utils;
                            myWriter.write(response);
                            myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
                            delete myLogger;
                            return SOAP_OK;
                        }
                        acl.a_type = 0 | CNS_ACL_GROUP;
                        existingPerms.push_back(acl);
                    }
                    break;
                case srm__TPermissionType__REMOVE:
                    {
                        std::vector<Cns_acl>::iterator iter = existingPerms.begin();
                        while ( iter != existingPerms.end() ) {
                            if ( ( ((*iter).a_type == (CNS_ACL_DEFAULT | CNS_ACL_GROUP)) ||
                                        ((*iter).a_type == (CNS_ACL_DEFAULT | CNS_ACL_GROUP_OBJ))
                                 ) &&
                                    (*iter).a_id == (int)acl.a_id
                               ) {
                                existingPerms.erase(iter);
                                break;
                            }
                            *iter++;
                        }
                    }
                    break;
                case srm__TPermissionType__CHANGE:
                    {
                        // See if we have an existing user.  If we do, we need to
                        // modify their entry, if not, report an error
                        std::map<int, Cns_acl>::iterator mapiter = groupIds.find(acl.a_id);
                        if ( mapiter == groupIds.end() ) {
                            // Error condition - user already exists
                            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST;
                            std::stringstream ss;
                            ss << "Attempt to change group permissions for " << pVector[i]->groupID.c_str() << " failed: group does not exist";
                            resp->returnStatus->explanation->assign(ss.str());
                            utils->unmap_user();
                            delete utils;
                            myWriter.write(response);
                            myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
                            delete myLogger;
                            return SOAP_OK;
                        }
                        // We have an existing user.  When going through existing 
                        // perms we need to make sure we distinguish between group
                        // and user id
                        std::vector<Cns_acl>::iterator iter = existingPerms.begin();
                        while ( iter != existingPerms.end() ) {
                            if ( ( (*iter).a_type == CNS_ACL_GROUP || (*iter).a_type == CNS_ACL_GROUP_OBJ ||
                                        ((*iter).a_type == (CNS_ACL_DEFAULT | CNS_ACL_GROUP) ) ||
                                        ((*iter).a_type == (CNS_ACL_DEFAULT | CNS_ACL_GROUP_OBJ) )
                                 ) &&
                                    (*iter).a_id == (int)acl.a_id
                               ) {
                                (*iter).a_perm = acl.a_perm;
                                break;
                            }
                            *iter++;
                        }
                    }
                    break;
            }
        }
    }

    //=======================================================
    // OTHER Permsission
    //=======================================================
    if ( srmSetPermissionRequest->otherPermission ) {
        Cns_acl acl;
        acl.a_type = CNS_ACL_OTHER;
        acl.a_id = 0;
        switch ((int)*(srmSetPermissionRequest->otherPermission)) {
            case srm__TPermissionMode__NONE:
            case srm__TPermissionMode__R:
                acl.a_perm = S_IROTH;
                break;
            case srm__TPermissionMode__X:
                acl.a_perm = S_IXOTH;
                break;
            case srm__TPermissionMode__W:
                acl.a_perm = S_IWOTH;
                break;
            case srm__TPermissionMode__WX:
                acl.a_perm = S_IWOTH | S_IXOTH;
                break;
            case srm__TPermissionMode__RX:
                acl.a_perm = S_IROTH | S_IXOTH;
                break;
            case srm__TPermissionMode__RW:
                acl.a_perm = S_IROTH | S_IWOTH;
                break;
            case srm__TPermissionMode__RWX:
                acl.a_perm = S_IROTH | S_IWOTH | S_IXOTH;
                break;
        }
        switch (permissionType) {
            case srm__TPermissionType__CHANGE: // Only thing allowed for other - we can not add or remove it
                {
                    // See if we have an existing user.  If we do, we need to
                    // modify their entry, if not, create new entry
                    std::map<int, Cns_acl>::iterator mapiter = userIds.find(uid);
                    if ( mapiter == userIds.end() ) {
                        existingPerms.push_back(acl);
                    }
                    // We have an existing user.  When going through existing 
                    // perms we need to make sure we distinguish between group
                    // and user id
                    std::vector<Cns_acl>::iterator iter = existingPerms.begin();
                    while ( iter != existingPerms.end() ) {
                        if ( (*iter).a_type == CNS_ACL_OTHER ) { // There can be only one 
                            (*iter).a_perm=acl.a_perm;
                            break;
                        }
                        *iter++;
                    }
                }
                break;
            default:
                resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST;
                resp->returnStatus->explanation->assign("Invalid operation on other");
                utils->unmap_user();
                myWriter.write(response);
                myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
                delete utils;
                delete myLogger;
                return SOAP_OK;
        }
    }

    // Now we need to the new acl
    if ( existingPerms.size() > CA_MAXACLENTRIES ) {
        // We are only allowing 300 values
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
        std::stringstream ss;
        ss << "Number of ACL entries (" << existingPerms.size() << ") exceeds max of 50" << std::endl;
        resp->returnStatus->explanation->assign(ss.str());
        utils->unmap_user();
        delete utils;
        myWriter.write(response);
        myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
        delete myLogger;
        return SOAP_OK;
    }
    else {
        Cns_acl *myACLs = (Cns_acl*) malloc(existingPerms.size() * sizeof(Cns_acl));
        for ( unsigned i=0; i<existingPerms.size(); i++) {
            myACLs[i].a_type = existingPerms[i].a_type;
            myACLs[i].a_id   = existingPerms[i].a_id;
            myACLs[i].a_perm = existingPerms[i].a_perm;
        }
        myLogger->logCnsCall("Cns_setacl", cfn, &nsId, LOG_INFO);
        int saclrc = Cns_setacl(cfn.c_str(), existingPerms.size(), myACLs);
        myLogger->logCnsReturn("Cns_setacl", saclrc, &nsId);
        if ( saclrc != 0 ) {
            handleError(resp);
            free (myACLs);
            utils->unmap_user();
            delete utils;
            myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
            delete myLogger;
            myWriter.write(response);
            return SOAP_OK;
        }
        free (myACLs);
    }
    utils->unmap_user();
    delete utils;
    myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
    myWriter.write(response);
    delete myLogger;
    return SOAP_OK;
}


void handleError(srm__srmSetPermissionResponse *resp) {
    switch (serrno) {
        case ENOENT:
        case EFAULT:
        case ENOTDIR:
        case EINVAL:
        case ENAMETOOLONG:
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREPATH;
            resp->returnStatus->explanation->assign(sstrerror(serrno));
            break;
        case EACCES:
        case EPERM:
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREAUTHORIZATION_USCOREFAILURE;
            resp->returnStatus->explanation->assign(sstrerror(serrno));
            break;
        default:
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
            resp->returnStatus->explanation->assign("Unexpected error");
            break;
    }
}

