#include "ResponseWriter.hpp"

void castor::srm::server::ResponseWriter::write(srm__srmReserveSpaceResponse_ &myResponse) {
    if ( getenv ("SRM_TRACE") ) {
        std::string method = "ReserveSpace";
        struct soap* soap = soap_new();
        int fd = init(soap, method);
        soap_init(soap);
        soap->sendfd = fd;
        soap->fsend = castor::srm::server::ResponseWriter::doWrite;
        soap_begin(soap);
        soap_set_omode(soap, SOAP_XML_GRAPH|SOAP_IO_STORE);
        soap_serialize_srm__srmReserveSpaceResponse_(soap, const_cast <srm__srmReserveSpaceResponse_*>(&myResponse));
        soap_begin_send(soap);
        soap_put_srm__srmReserveSpaceResponse_(soap, const_cast<srm__srmReserveSpaceResponse_*>(&myResponse), method.c_str(), NULL);
        soap_end_send(soap);
        close (soap->sendfd);
        soap->sendfd = -1;
        soap_end(soap);
        soap_done(soap);
    }
}
void castor::srm::server::ResponseWriter::write(srm__srmStatusOfReserveSpaceRequestResponse_ &myResponse){
    if ( getenv ("SRM_TRACE") ) {
        std::string method = "StatusOfReserveSpaceRequest";
        struct soap* soap = soap_new();
        int fd = init(soap, method);
        soap_init(soap);
        soap->sendfd = fd;
        soap->fsend = castor::srm::server::ResponseWriter::doWrite;
        soap_begin(soap);
        soap_set_omode(soap, SOAP_XML_GRAPH|SOAP_IO_STORE);
        soap_serialize_srm__srmStatusOfReserveSpaceRequestResponse_(soap, const_cast <srm__srmStatusOfReserveSpaceRequestResponse_*>(&myResponse));
        soap_begin_send(soap);
        soap_put_srm__srmStatusOfReserveSpaceRequestResponse_(soap, const_cast<srm__srmStatusOfReserveSpaceRequestResponse_*>(&myResponse), method.c_str(), NULL);
        soap_end_send(soap);
        close (soap->sendfd);
        soap->sendfd = -1;
        soap_end(soap);
        soap_done(soap);
    }
}
void castor::srm::server::ResponseWriter::write(srm__srmReleaseSpaceResponse_ &myResponse){
    if ( getenv ("SRM_TRACE") ) {
        std::string method = "ReleaseSpace";
        struct soap* soap = soap_new();
        int fd = init(soap, method);
        soap_init(soap);
        soap->sendfd = fd;
        soap->fsend = castor::srm::server::ResponseWriter::doWrite;
        soap_begin(soap);
        soap_set_omode(soap, SOAP_XML_GRAPH|SOAP_IO_STORE);
        soap_serialize_srm__srmReleaseSpaceResponse_(soap, const_cast <srm__srmReleaseSpaceResponse_*>(&myResponse));
        soap_begin_send(soap);
        soap_put_srm__srmReleaseSpaceResponse_(soap, const_cast<srm__srmReleaseSpaceResponse_*>(&myResponse), method.c_str(), NULL);
        soap_end_send(soap);
        close (soap->sendfd);
        soap->sendfd = -1;
        soap_end(soap);
        soap_done(soap);
    }
}
void castor::srm::server::ResponseWriter::write(srm__srmUpdateSpaceResponse_ &myResponse){
    if ( getenv ("SRM_TRACE") ) {
        std::string method = "UpdateSpace";
        struct soap* soap = soap_new();
        int fd = init(soap, method);
        soap_init(soap);
        soap->sendfd = fd;
        soap->fsend = castor::srm::server::ResponseWriter::doWrite;
        soap_begin(soap);
        soap_set_omode(soap, SOAP_XML_GRAPH|SOAP_IO_STORE);
        soap_serialize_srm__srmUpdateSpaceResponse_(soap, const_cast <srm__srmUpdateSpaceResponse_*>(&myResponse));
        soap_begin_send(soap);
        soap_put_srm__srmUpdateSpaceResponse_(soap, const_cast<srm__srmUpdateSpaceResponse_*>(&myResponse), method.c_str(), NULL);
        soap_end_send(soap);
        close (soap->sendfd);
        soap->sendfd = -1;
        soap_end(soap);
        soap_done(soap);
    }
}
void castor::srm::server::ResponseWriter::write(srm__srmStatusOfUpdateSpaceRequestResponse_ &myResponse){
    if ( getenv ("SRM_TRACE") ) {
        std::string method = "StatusOfUpdateSpaceRequest";
        struct soap* soap = soap_new();
        int fd = init(soap, method);
        soap_init(soap);
        soap->sendfd = fd;
        soap->fsend = castor::srm::server::ResponseWriter::doWrite;
        soap_begin(soap);
        soap_set_omode(soap, SOAP_XML_GRAPH|SOAP_IO_STORE);
        soap_serialize_srm__srmStatusOfUpdateSpaceRequestResponse_(soap, const_cast <srm__srmStatusOfUpdateSpaceRequestResponse_*>(&myResponse));
        soap_begin_send(soap);
        soap_put_srm__srmStatusOfUpdateSpaceRequestResponse_(soap, const_cast<srm__srmStatusOfUpdateSpaceRequestResponse_*>(&myResponse), method.c_str(), NULL);
        soap_end_send(soap);
        close (soap->sendfd);
        soap->sendfd = -1;
        soap_end(soap);
        soap_done(soap);
    }
}
void castor::srm::server::ResponseWriter::write(srm__srmGetSpaceMetaDataResponse_ &myResponse){
    if ( getenv ("SRM_TRACE") ) {
        std::string method = "GetSpaceMetaData";
        struct soap* soap = soap_new();
        int fd = init(soap, method);
        soap_init(soap);
        soap->sendfd = fd;
        soap->fsend = castor::srm::server::ResponseWriter::doWrite;
        soap_begin(soap);
        soap_set_omode(soap, SOAP_XML_GRAPH|SOAP_IO_STORE);
        soap_serialize_srm__srmGetSpaceMetaDataResponse_(soap, const_cast <srm__srmGetSpaceMetaDataResponse_*>(&myResponse));
        soap_begin_send(soap);
        soap_put_srm__srmGetSpaceMetaDataResponse_(soap, const_cast<srm__srmGetSpaceMetaDataResponse_*>(&myResponse), method.c_str(), NULL);
        soap_end_send(soap);
        close (soap->sendfd);
        soap->sendfd = -1;
        soap_end(soap);
        soap_done(soap);
    }
}
void castor::srm::server::ResponseWriter::write(srm__srmChangeSpaceForFilesResponse_ &myResponse){
    if ( getenv ("SRM_TRACE") ) {
        std::string method = "ChangeSpaceForFiles";
        struct soap* soap = soap_new();
        int fd = init(soap, method);
        soap_init(soap);
        soap->sendfd = fd;
        soap->fsend = castor::srm::server::ResponseWriter::doWrite;
        soap_begin(soap);
        soap_set_omode(soap, SOAP_XML_GRAPH|SOAP_IO_STORE);
        soap_serialize_srm__srmChangeSpaceForFilesResponse_(soap, const_cast <srm__srmChangeSpaceForFilesResponse_*>(&myResponse));
        soap_begin_send(soap);
        soap_put_srm__srmChangeSpaceForFilesResponse_(soap, const_cast<srm__srmChangeSpaceForFilesResponse_*>(&myResponse), method.c_str(), NULL);
        soap_end_send(soap);
        close (soap->sendfd);
        soap->sendfd = -1;
        soap_end(soap);
        soap_done(soap);
    }
}
void castor::srm::server::ResponseWriter::write(srm__srmStatusOfChangeSpaceForFilesRequestResponse_ &myResponse){
    if ( getenv ("SRM_TRACE") ) {
        std::string method = "StatusOfChangeSpaceForFiles";
        struct soap* soap = soap_new();
        int fd = init(soap, method);
        soap_init(soap);
        soap->sendfd = fd;
        soap->fsend = castor::srm::server::ResponseWriter::doWrite;
        soap_begin(soap);
        soap_set_omode(soap, SOAP_XML_GRAPH|SOAP_IO_STORE);
        soap_serialize_srm__srmStatusOfChangeSpaceForFilesRequestResponse_(soap, const_cast <srm__srmStatusOfChangeSpaceForFilesRequestResponse_*>(&myResponse));
        soap_begin_send(soap);
        soap_put_srm__srmStatusOfChangeSpaceForFilesRequestResponse_(soap, const_cast<srm__srmStatusOfChangeSpaceForFilesRequestResponse_*>(&myResponse), method.c_str(), NULL);
        soap_end_send(soap);
        close (soap->sendfd);
        soap->sendfd = -1;
        soap_end(soap);
        soap_done(soap);
    }
}
void castor::srm::server::ResponseWriter::write(srm__srmExtendFileLifeTimeInSpaceResponse_ &myResponse){
    if ( getenv ("SRM_TRACE") ) {
        std::string method = "ExtendFileLifeTimeInSpace";
        struct soap* soap = soap_new();
        int fd = init(soap, method);
        soap_init(soap);
        soap->sendfd = fd;
        soap->fsend = castor::srm::server::ResponseWriter::doWrite;
        soap_begin(soap);
        soap_set_omode(soap, SOAP_XML_GRAPH|SOAP_IO_STORE);
        soap_serialize_srm__srmExtendFileLifeTimeInSpaceResponse_(soap, const_cast <srm__srmExtendFileLifeTimeInSpaceResponse_*>(&myResponse));
        soap_begin_send(soap);
        soap_put_srm__srmExtendFileLifeTimeInSpaceResponse_(soap, const_cast<srm__srmExtendFileLifeTimeInSpaceResponse_*>(&myResponse), method.c_str(), NULL);
        soap_end_send(soap);
        close (soap->sendfd);
        soap->sendfd = -1;
        soap_end(soap);
        soap_done(soap);
    }
}
void castor::srm::server::ResponseWriter::write(srm__srmPurgeFromSpaceResponse_ &myResponse){
    if ( getenv ("SRM_TRACE") ) {
        std::string method = "PurgeFromSpace";
        struct soap* soap = soap_new();
        int fd = init(soap, method);
        soap_init(soap);
        soap->sendfd = fd;
        soap->fsend = castor::srm::server::ResponseWriter::doWrite;
        soap_begin(soap);
        soap_set_omode(soap, SOAP_XML_GRAPH|SOAP_IO_STORE);
        soap_serialize_srm__srmPurgeFromSpaceResponse_(soap, const_cast <srm__srmPurgeFromSpaceResponse_*>(&myResponse));
        soap_begin_send(soap);
        soap_put_srm__srmPurgeFromSpaceResponse_(soap, const_cast<srm__srmPurgeFromSpaceResponse_*>(&myResponse), method.c_str(), NULL);
        soap_end_send(soap);
        close (soap->sendfd);
        soap->sendfd = -1;
        soap_end(soap);
        soap_done(soap);
    }
}
void castor::srm::server::ResponseWriter::write(srm__srmGetSpaceTokensResponse_ &myResponse){
    if ( getenv ("SRM_TRACE") ) {
        std::string method = "GetSpaceTokens";
        struct soap* soap = soap_new();
        int fd = init(soap, method);
        soap_init(soap);
        soap->sendfd = fd;
        soap->fsend = castor::srm::server::ResponseWriter::doWrite;
        soap_begin(soap);
        soap_set_omode(soap, SOAP_XML_GRAPH|SOAP_IO_STORE);
        soap_serialize_srm__srmGetSpaceTokensResponse_(soap, const_cast <srm__srmGetSpaceTokensResponse_*>(&myResponse));
        soap_begin_send(soap);
        soap_put_srm__srmGetSpaceTokensResponse_(soap, const_cast<srm__srmGetSpaceTokensResponse_*>(&myResponse), method.c_str(), NULL);
        soap_end_send(soap);
        close (soap->sendfd);
        soap->sendfd = -1;
        soap_end(soap);
        soap_done(soap);
    }
}
void castor::srm::server::ResponseWriter::write(srm__srmSetPermissionResponse_ &myResponse){
    if ( getenv ("SRM_TRACE") ) {
        std::string method = "SetPermission";
        struct soap* soap = soap_new();
        int fd = init(soap, method);
        soap_init(soap);
        soap->sendfd = fd;
        soap->fsend = castor::srm::server::ResponseWriter::doWrite;
        soap_begin(soap);
        soap_set_omode(soap, SOAP_XML_GRAPH|SOAP_IO_STORE);
        soap_serialize_srm__srmSetPermissionResponse_(soap, const_cast <srm__srmSetPermissionResponse_*>(&myResponse));
        soap_begin_send(soap);
        soap_put_srm__srmSetPermissionResponse_(soap, const_cast<srm__srmSetPermissionResponse_*>(&myResponse), method.c_str(), NULL);
        soap_end_send(soap);
        close (soap->sendfd);
        soap->sendfd = -1;
        soap_end(soap);
        soap_done(soap);
    }
}
void castor::srm::server::ResponseWriter::write(srm__srmCheckPermissionResponse_ &myResponse){
    if ( getenv ("SRM_TRACE") ) {
        std::string method = "CheckPermissions";
        struct soap* soap = soap_new();
        int fd = init(soap, method);
        soap_init(soap);
        soap->sendfd = fd;
        soap->fsend = castor::srm::server::ResponseWriter::doWrite;
        soap_begin(soap);
        soap_set_omode(soap, SOAP_XML_GRAPH|SOAP_IO_STORE);
        soap_serialize_srm__srmCheckPermissionResponse_(soap, const_cast <srm__srmCheckPermissionResponse_*>(&myResponse));
        soap_begin_send(soap);
        soap_put_srm__srmCheckPermissionResponse_(soap, const_cast<srm__srmCheckPermissionResponse_*>(&myResponse), method.c_str(), NULL);
        soap_end_send(soap);
        close (soap->sendfd);
        soap->sendfd = -1;
        soap_end(soap);
        soap_done(soap);
    }
}
void castor::srm::server::ResponseWriter::write(srm__srmGetPermissionResponse_ &myResponse){
    if ( getenv ("SRM_TRACE") ) {
        std::string method = "GetPermission";
        struct soap* soap = soap_new();
        int fd = init(soap, method);
        soap_init(soap);
        soap->sendfd = fd;
        soap->fsend = castor::srm::server::ResponseWriter::doWrite;
        soap_begin(soap);
        soap_set_omode(soap, SOAP_XML_GRAPH|SOAP_IO_STORE);
        soap_serialize_srm__srmGetPermissionResponse_(soap, const_cast <srm__srmGetPermissionResponse_*>(&myResponse));
        soap_begin_send(soap);
        soap_put_srm__srmGetPermissionResponse_(soap, const_cast<srm__srmGetPermissionResponse_*>(&myResponse), method.c_str(), NULL);
        soap_end_send(soap);
        close (soap->sendfd);
        soap->sendfd = -1;
        soap_end(soap);
        soap_done(soap);
    }
}
void castor::srm::server::ResponseWriter::write(srm__srmMkdirResponse_ &myResponse){
    if ( getenv ("SRM_TRACE") ) {
        std::string method = "Mkdir";
        struct soap* soap = soap_new();
        int fd = init(soap, method);
        soap_init(soap);
        soap->sendfd = fd;
        soap->fsend = castor::srm::server::ResponseWriter::doWrite;
        soap_begin(soap);
        soap_set_omode(soap, SOAP_XML_GRAPH|SOAP_IO_STORE);
        soap_serialize_srm__srmMkdirResponse_(soap, const_cast <srm__srmMkdirResponse_*>(&myResponse));
        soap_begin_send(soap);
        soap_put_srm__srmMkdirResponse_(soap, const_cast<srm__srmMkdirResponse_*>(&myResponse), method.c_str(), NULL);
        soap_end_send(soap);
        close (soap->sendfd);
        soap->sendfd = -1;
        soap_end(soap);
        soap_done(soap);
    }
}
void castor::srm::server::ResponseWriter::write(srm__srmRmdirResponse_ &myResponse){
    if ( getenv ("SRM_TRACE") ) {
        std::string method = "Rmdir";
        struct soap* soap = soap_new();
        int fd = init(soap, method);
        soap_init(soap);
        soap->sendfd = fd;
        soap->fsend = castor::srm::server::ResponseWriter::doWrite;
        soap_begin(soap);
        soap_set_omode(soap, SOAP_XML_GRAPH|SOAP_IO_STORE);
        soap_serialize_srm__srmRmdirResponse_(soap, const_cast <srm__srmRmdirResponse_*>(&myResponse));
        soap_begin_send(soap);
        soap_put_srm__srmRmdirResponse_(soap, const_cast<srm__srmRmdirResponse_*>(&myResponse), method.c_str(), NULL);
        soap_end_send(soap);
        close (soap->sendfd);
        soap->sendfd = -1;
        soap_end(soap);
        soap_done(soap);
    }
}
void castor::srm::server::ResponseWriter::write(srm__srmRmResponse_ &myResponse){
    if ( getenv ("SRM_TRACE") ) {
        std::string method = "Rm";
        struct soap* soap = soap_new();
        int fd = init(soap, method);
        soap_init(soap);
        soap->sendfd = fd;
        soap->fsend = castor::srm::server::ResponseWriter::doWrite;
        soap_begin(soap);
        soap_set_omode(soap, SOAP_XML_GRAPH|SOAP_IO_STORE);
        soap_serialize_srm__srmRmResponse_(soap, const_cast <srm__srmRmResponse_*>(&myResponse));
        soap_begin_send(soap);
        soap_put_srm__srmRmResponse_(soap, const_cast<srm__srmRmResponse_*>(&myResponse), method.c_str(), NULL);
        soap_end_send(soap);
        close (soap->sendfd);
        soap->sendfd = -1;
        soap_end(soap);
        soap_done(soap);
    }
}
void castor::srm::server::ResponseWriter::write(srm__srmLsResponse_ &myResponse){
    if ( getenv ("SRM_TRACE") ) {
        std::string method = "Ls";
        struct soap* soap = soap_new();
        int fd = init(soap, method);
        soap_init(soap);
        soap->sendfd = fd;
        soap->fsend = castor::srm::server::ResponseWriter::doWrite;
        soap_begin(soap);
        soap_set_omode(soap, SOAP_XML_GRAPH|SOAP_IO_STORE);
        soap_serialize_srm__srmLsResponse_(soap, const_cast <srm__srmLsResponse_*>(&myResponse));
        soap_begin_send(soap);
        soap_put_srm__srmLsResponse_(soap, const_cast<srm__srmLsResponse_*>(&myResponse), method.c_str(), NULL);
        soap_end_send(soap);
        close (soap->sendfd);
        soap->sendfd = -1;
        soap_end(soap);
        soap_done(soap);
    }
}
void castor::srm::server::ResponseWriter::write(srm__srmStatusOfLsRequestResponse_ &myResponse){
    if ( getenv ("SRM_TRACE") ) {
        std::string method = "StatusOfLs";
        struct soap* soap = soap_new();
        int fd = init(soap, method);
        soap_init(soap);
        soap->sendfd = fd;
        soap->fsend = castor::srm::server::ResponseWriter::doWrite;
        soap_begin(soap);
        soap_set_omode(soap, SOAP_XML_GRAPH|SOAP_IO_STORE);
        soap_serialize_srm__srmStatusOfLsRequestResponse_(soap, const_cast <srm__srmStatusOfLsRequestResponse_*>(&myResponse));
        soap_begin_send(soap);
        soap_put_srm__srmStatusOfLsRequestResponse_(soap, const_cast<srm__srmStatusOfLsRequestResponse_*>(&myResponse), method.c_str(), NULL);
        soap_end_send(soap);
        close (soap->sendfd);
        soap->sendfd = -1;
        soap_end(soap);
        soap_done(soap);
    }
}
void castor::srm::server::ResponseWriter::write(srm__srmMvResponse_ &myResponse){
    if ( getenv ("SRM_TRACE") ) {
        std::string method = "Mv";
        struct soap* soap = soap_new();
        int fd = init(soap, method);
        soap_init(soap);
        soap->sendfd = fd;
        soap->fsend = castor::srm::server::ResponseWriter::doWrite;
        soap_begin(soap);
        soap_set_omode(soap, SOAP_XML_GRAPH|SOAP_IO_STORE);
        soap_serialize_srm__srmMvResponse_(soap, const_cast <srm__srmMvResponse_*>(&myResponse));
        soap_begin_send(soap);
        soap_put_srm__srmMvResponse_(soap, const_cast<srm__srmMvResponse_*>(&myResponse), method.c_str(), NULL);
        soap_end_send(soap);
        close (soap->sendfd);
        soap->sendfd = -1;
        soap_end(soap);
        soap_done(soap);
    }
}
void castor::srm::server::ResponseWriter::write(srm__srmPrepareToGetResponse_ &myResponse){
    if ( getenv ("SRM_TRACE") ) {
        std::string method = "PrepareToGet";
        struct soap* soap = soap_new();
        int fd = init(soap, method);
        soap_init(soap);
        soap->sendfd = fd;
        soap->fsend = castor::srm::server::ResponseWriter::doWrite;
        soap_begin(soap);
        soap_set_omode(soap, SOAP_XML_GRAPH|SOAP_IO_STORE);
        soap_serialize_srm__srmPrepareToGetResponse_(soap, const_cast <srm__srmPrepareToGetResponse_*>(&myResponse));
        soap_begin_send(soap);
        soap_put_srm__srmPrepareToGetResponse_(soap, const_cast<srm__srmPrepareToGetResponse_*>(&myResponse), method.c_str(), NULL);
        soap_end_send(soap);
        close (soap->sendfd);
        soap->sendfd = -1;
        soap_end(soap);
        soap_done(soap);
    }
}
void castor::srm::server::ResponseWriter::write(srm__srmStatusOfGetRequestResponse_ &myResponse){
    if ( getenv ("SRM_TRACE") ) {
        std::string method = "StatusOfGet";
        struct soap* soap = soap_new();
        int fd = init(soap, method);
        soap_init(soap);
        soap->sendfd = fd;
        soap->fsend = castor::srm::server::ResponseWriter::doWrite;
        soap_begin(soap);
        soap_set_omode(soap, SOAP_XML_GRAPH|SOAP_IO_STORE);
        soap_serialize_srm__srmStatusOfGetRequestResponse_(soap, const_cast <srm__srmStatusOfGetRequestResponse_*>(&myResponse));
        soap_begin_send(soap);
        soap_put_srm__srmStatusOfGetRequestResponse_(soap, const_cast<srm__srmStatusOfGetRequestResponse_*>(&myResponse), method.c_str(), NULL);
        soap_end_send(soap);
        close (soap->sendfd);
        soap->sendfd = -1;
        soap_end(soap);
        soap_done(soap);
    }
}
void castor::srm::server::ResponseWriter::write(srm__srmBringOnlineResponse_ &myResponse){
    if ( getenv ("SRM_TRACE") ) {
        std::string method = "BringOnline";
        struct soap* soap = soap_new();
        int fd = init(soap, method);
        soap_init(soap);
        soap->sendfd = fd;
        soap->fsend = castor::srm::server::ResponseWriter::doWrite;
        soap_begin(soap);
        soap_set_omode(soap, SOAP_XML_GRAPH|SOAP_IO_STORE);
        soap_serialize_srm__srmBringOnlineResponse_(soap, const_cast <srm__srmBringOnlineResponse_*>(&myResponse));
        soap_begin_send(soap);
        soap_put_srm__srmBringOnlineResponse_(soap, const_cast<srm__srmBringOnlineResponse_*>(&myResponse), method.c_str(), NULL);
        soap_end_send(soap);
        close (soap->sendfd);
        soap->sendfd = -1;
        soap_end(soap);
        soap_done(soap);
    }
}
void castor::srm::server::ResponseWriter::write(srm__srmStatusOfBringOnlineRequestResponse_ &myResponse){
    if ( getenv ("SRM_TRACE") ) {
        std::string method = "StatusOfBringOnline";
        struct soap* soap = soap_new();
        int fd = init(soap, method);
        soap_init(soap);
        soap->sendfd = fd;
        soap->fsend = castor::srm::server::ResponseWriter::doWrite;
        soap_begin(soap);
        soap_set_omode(soap, SOAP_XML_GRAPH|SOAP_IO_STORE);
        soap_serialize_srm__srmStatusOfBringOnlineRequestResponse_(soap, const_cast <srm__srmStatusOfBringOnlineRequestResponse_*>(&myResponse));
        soap_begin_send(soap);
        soap_put_srm__srmStatusOfBringOnlineRequestResponse_(soap, const_cast<srm__srmStatusOfBringOnlineRequestResponse_*>(&myResponse), method.c_str(), NULL);
        soap_end_send(soap);
        close (soap->sendfd);
        soap->sendfd = -1;
        soap_end(soap);
        soap_done(soap);
    }
}
void castor::srm::server::ResponseWriter::write(srm__srmPrepareToPutResponse_ &myResponse){
    if ( getenv ("SRM_TRACE") ) {
        std::string method = "PrepareToPut";
        struct soap* soap = soap_new();
        int fd = init(soap, method);
        soap_init(soap);
        soap->sendfd = fd;
        soap->fsend = castor::srm::server::ResponseWriter::doWrite;
        soap_begin(soap);
        soap_set_omode(soap, SOAP_XML_GRAPH|SOAP_IO_STORE);
        soap_serialize_srm__srmPrepareToPutResponse_(soap, const_cast <srm__srmPrepareToPutResponse_*>(&myResponse));
        soap_begin_send(soap);
        soap_put_srm__srmPrepareToPutResponse_(soap, const_cast<srm__srmPrepareToPutResponse_*>(&myResponse), method.c_str(), NULL);
        soap_end_send(soap);
        close (soap->sendfd);
        soap->sendfd = -1;
        soap_end(soap);
        soap_done(soap);
    }
}
void castor::srm::server::ResponseWriter::write(srm__srmStatusOfPutRequestResponse_ &myResponse){
    if ( getenv ("SRM_TRACE") ) {
        std::string method = "StatusOfPutRequest";
        struct soap* soap = soap_new();
        int fd = init(soap, method);
        soap_init(soap);
        soap->sendfd = fd;
        soap->fsend = castor::srm::server::ResponseWriter::doWrite;
        soap_begin(soap);
        soap_set_omode(soap, SOAP_XML_GRAPH|SOAP_IO_STORE);
        soap_serialize_srm__srmStatusOfPutRequestResponse_(soap, const_cast <srm__srmStatusOfPutRequestResponse_*>(&myResponse));
        soap_begin_send(soap);
        soap_put_srm__srmStatusOfPutRequestResponse_(soap, const_cast<srm__srmStatusOfPutRequestResponse_*>(&myResponse), method.c_str(), NULL);
        soap_end_send(soap);
        close (soap->sendfd);
        soap->sendfd = -1;
        soap_end(soap);
        soap_done(soap);
    }
}
void castor::srm::server::ResponseWriter::write(srm__srmCopyResponse_ &myResponse){
    if ( getenv ("SRM_TRACE") ) {
        std::string method = "Copy";
        struct soap* soap = soap_new();
        int fd = init(soap, method);
        soap_init(soap);
        soap->sendfd = fd;
        soap->fsend = castor::srm::server::ResponseWriter::doWrite;
        soap_begin(soap);
        soap_set_omode(soap, SOAP_XML_GRAPH|SOAP_IO_STORE);
        soap_serialize_srm__srmCopyResponse_(soap, const_cast <srm__srmCopyResponse_*>(&myResponse));
        soap_begin_send(soap);
        soap_put_srm__srmCopyResponse_(soap, const_cast<srm__srmCopyResponse_*>(&myResponse), method.c_str(), NULL);
        soap_end_send(soap);
        close (soap->sendfd);
        soap->sendfd = -1;
        soap_end(soap);
        soap_done(soap);
    }
}
void castor::srm::server::ResponseWriter::write(srm__srmStatusOfCopyRequestResponse_ &myResponse){
    if ( getenv ("SRM_TRACE") ) {
        std::string method = "StatusOfCopy";
        struct soap* soap = soap_new();
        int fd = init(soap, method);
        soap_init(soap);
        soap->sendfd = fd;
        soap->fsend = castor::srm::server::ResponseWriter::doWrite;
        soap_begin(soap);
        soap_set_omode(soap, SOAP_XML_GRAPH|SOAP_IO_STORE);
        soap_serialize_srm__srmStatusOfCopyRequestResponse_(soap, const_cast <srm__srmStatusOfCopyRequestResponse_*>(&myResponse));
        soap_begin_send(soap);
        soap_put_srm__srmStatusOfCopyRequestResponse_(soap, const_cast<srm__srmStatusOfCopyRequestResponse_*>(&myResponse), method.c_str(), NULL);
        soap_end_send(soap);
        close (soap->sendfd);
        soap->sendfd = -1;
        soap_end(soap);
        soap_done(soap);
    }
}
void castor::srm::server::ResponseWriter::write(srm__srmReleaseFilesResponse_ &myResponse){
    if ( getenv ("SRM_TRACE") ) {
        std::string method = "ReleaseFiles";
        struct soap* soap = soap_new();
        int fd = init(soap, method);
        soap_init(soap);
        soap->sendfd = fd;
        soap->fsend = castor::srm::server::ResponseWriter::doWrite;
        soap_begin(soap);
        soap_set_omode(soap, SOAP_XML_GRAPH|SOAP_IO_STORE);
        soap_serialize_srm__srmReleaseFilesResponse_(soap, const_cast <srm__srmReleaseFilesResponse_*>(&myResponse));
        soap_begin_send(soap);
        soap_put_srm__srmReleaseFilesResponse_(soap, const_cast<srm__srmReleaseFilesResponse_*>(&myResponse), method.c_str(), NULL);
        soap_end_send(soap);
        close (soap->sendfd);
        soap->sendfd = -1;
        soap_end(soap);
        soap_done(soap);
    }
}
void castor::srm::server::ResponseWriter::write(srm__srmPutDoneResponse_ &myResponse){
    if ( getenv ("SRM_TRACE") ) {
        std::string method = "PutDone";
        struct soap* soap = soap_new();
        int fd = init(soap, method);
        soap_init(soap);
        soap->sendfd = fd;
        soap->fsend = castor::srm::server::ResponseWriter::doWrite;
        soap_begin(soap);
        soap_set_omode(soap, SOAP_XML_GRAPH|SOAP_IO_STORE);
        soap_serialize_srm__srmPutDoneResponse_(soap, const_cast <srm__srmPutDoneResponse_*>(&myResponse));
        soap_begin_send(soap);
        soap_put_srm__srmPutDoneResponse_(soap, const_cast<srm__srmPutDoneResponse_*>(&myResponse), method.c_str(), NULL);
        soap_end_send(soap);
        close (soap->sendfd);
        soap->sendfd = -1;
        soap_end(soap);
        soap_done(soap);
    }
}
void castor::srm::server::ResponseWriter::write(srm__srmAbortRequestResponse_ &myResponse){
    if ( getenv ("SRM_TRACE") ) {
        std::string method = "AbortRequest";
        struct soap* soap = soap_new();
        int fd = init(soap, method);
        soap_init(soap);
        soap->sendfd = fd;
        soap->fsend = castor::srm::server::ResponseWriter::doWrite;
        soap_begin(soap);
        soap_set_omode(soap, SOAP_XML_GRAPH|SOAP_IO_STORE);
        soap_serialize_srm__srmAbortRequestResponse_(soap, const_cast <srm__srmAbortRequestResponse_*>(&myResponse));
        soap_begin_send(soap);
        soap_put_srm__srmAbortRequestResponse_(soap, const_cast<srm__srmAbortRequestResponse_*>(&myResponse), method.c_str(), NULL);
        soap_end_send(soap);
        close (soap->sendfd);
        soap->sendfd = -1;
        soap_end(soap);
        soap_done(soap);
    }
}
void castor::srm::server::ResponseWriter::write(srm__srmAbortFilesResponse_ &myResponse){
    if ( getenv ("SRM_TRACE") ) {
        std::string method = "AbortFiles";
        struct soap* soap = soap_new();
        int fd = init(soap, method);
        soap_init(soap);
        soap->sendfd = fd;
        soap->fsend = castor::srm::server::ResponseWriter::doWrite;
        soap_begin(soap);
        soap_set_omode(soap, SOAP_XML_GRAPH|SOAP_IO_STORE);
        soap_serialize_srm__srmAbortFilesResponse_(soap, const_cast <srm__srmAbortFilesResponse_*>(&myResponse));
        soap_begin_send(soap);
        soap_put_srm__srmAbortFilesResponse_(soap, const_cast<srm__srmAbortFilesResponse_*>(&myResponse), method.c_str(), NULL);
        soap_end_send(soap);
        close (soap->sendfd);
        soap->sendfd = -1;
        soap_end(soap);
        soap_done(soap);
    }
}
void castor::srm::server::ResponseWriter::write(srm__srmSuspendRequestResponse_ &myResponse){
    if ( getenv ("SRM_TRACE") ) {
        std::string method = "SuspendRequest";
        struct soap* soap = soap_new();
        int fd = init(soap, method);
        soap_init(soap);
        soap->sendfd = fd;
        soap->fsend = castor::srm::server::ResponseWriter::doWrite;
        soap_begin(soap);
        soap_set_omode(soap, SOAP_XML_GRAPH|SOAP_IO_STORE);
        soap_serialize_srm__srmSuspendRequestResponse_(soap, const_cast <srm__srmSuspendRequestResponse_*>(&myResponse));
        soap_begin_send(soap);
        soap_put_srm__srmSuspendRequestResponse_(soap, const_cast<srm__srmSuspendRequestResponse_*>(&myResponse), method.c_str(), NULL);
        soap_end_send(soap);
        close (soap->sendfd);
        soap->sendfd = -1;
        soap_end(soap);
        soap_done(soap);
    }
}
void castor::srm::server::ResponseWriter::write(srm__srmResumeRequestResponse_ &myResponse){
    if ( getenv ("SRM_TRACE") ) {
        std::string method = "ResumeRequest";
        struct soap* soap = soap_new();
        int fd = init(soap, method);
        soap_init(soap);
        soap->sendfd = fd;
        soap->fsend = castor::srm::server::ResponseWriter::doWrite;
        soap_begin(soap);
        soap_set_omode(soap, SOAP_XML_GRAPH|SOAP_IO_STORE);
        soap_serialize_srm__srmResumeRequestResponse_(soap, const_cast <srm__srmResumeRequestResponse_*>(&myResponse));
        soap_begin_send(soap);
        soap_put_srm__srmResumeRequestResponse_(soap, const_cast<srm__srmResumeRequestResponse_*>(&myResponse), method.c_str(), NULL);
        soap_end_send(soap);
        close (soap->sendfd);
        soap->sendfd = -1;
        soap_end(soap);
        soap_done(soap);
    }
}
void castor::srm::server::ResponseWriter::write(srm__srmGetRequestSummaryResponse_ &myResponse){
    if ( getenv ("SRM_TRACE") ) {
        std::string method = "GetRequestSummary";
        struct soap* soap = soap_new();
        int fd = init(soap, method);
        soap_init(soap);
        soap->sendfd = fd;
        soap->fsend = castor::srm::server::ResponseWriter::doWrite;
        soap_begin(soap);
        soap_set_omode(soap, SOAP_XML_GRAPH|SOAP_IO_STORE);
        soap_serialize_srm__srmGetRequestSummaryResponse_(soap, const_cast <srm__srmGetRequestSummaryResponse_*>(&myResponse));
        soap_begin_send(soap);
        soap_put_srm__srmGetRequestSummaryResponse_(soap, const_cast<srm__srmGetRequestSummaryResponse_*>(&myResponse), method.c_str(), NULL);
        soap_end_send(soap);
        close (soap->sendfd);
        soap->sendfd = -1;
        soap_end(soap);
        soap_done(soap);
    }
}
void castor::srm::server::ResponseWriter::write(srm__srmExtendFileLifeTimeResponse_ &myResponse){
    if ( getenv ("SRM_TRACE") ) {
        std::string method = "ExtendFileLifeTime";
        struct soap* soap = soap_new();
        int fd = init(soap, method);
        soap_init(soap);
        soap->sendfd = fd;
        soap->fsend = castor::srm::server::ResponseWriter::doWrite;
        soap_begin(soap);
        soap_set_omode(soap, SOAP_XML_GRAPH|SOAP_IO_STORE);
        soap_serialize_srm__srmExtendFileLifeTimeResponse_(soap, const_cast <srm__srmExtendFileLifeTimeResponse_*>(&myResponse));
        soap_begin_send(soap);
        soap_put_srm__srmExtendFileLifeTimeResponse_(soap, const_cast<srm__srmExtendFileLifeTimeResponse_*>(&myResponse), method.c_str(), NULL);
        soap_end_send(soap);
        close (soap->sendfd);
        soap->sendfd = -1;
        soap_end(soap);
        soap_done(soap);
    }
}
void castor::srm::server::ResponseWriter::write(srm__srmGetRequestTokensResponse_ &myResponse){
    if ( getenv ("SRM_TRACE") ) {
        std::string method = "GetRequestTokens";
        struct soap* soap = soap_new();
        int fd = init(soap, method);
        soap_init(soap);
        soap->sendfd = fd;
        soap->fsend = castor::srm::server::ResponseWriter::doWrite;
        soap_begin(soap);
        soap_set_omode(soap, SOAP_XML_GRAPH|SOAP_IO_STORE);
        soap_serialize_srm__srmGetRequestTokensResponse_(soap, const_cast <srm__srmGetRequestTokensResponse_*>(&myResponse));
        soap_begin_send(soap);
        soap_put_srm__srmGetRequestTokensResponse_(soap, const_cast<srm__srmGetRequestTokensResponse_*>(&myResponse), method.c_str(), NULL);
        soap_end_send(soap);
        close (soap->sendfd);
        soap->sendfd = -1;
        soap_end(soap);
        soap_done(soap);
    }
}
void castor::srm::server::ResponseWriter::write(srm__srmPingResponse_ &myResponse){
    if ( getenv ("SRM_TRACE") ) {
        std::string method = "Ping";
        struct soap* soap = soap_new();
        int fd = init(soap, method);
        soap_init(soap);
        soap->sendfd = fd;
        soap->fsend = castor::srm::server::ResponseWriter::doWrite;
        soap_begin(soap);
        soap_set_omode(soap, SOAP_XML_GRAPH|SOAP_IO_STORE);
        soap_serialize_srm__srmPingResponse_(soap, const_cast <srm__srmPingResponse_*>(&myResponse));
        soap_begin_send(soap);
        soap_put_srm__srmPingResponse_(soap, const_cast<srm__srmPingResponse_*>(&myResponse), method.c_str(), NULL);
        soap_end_send(soap);
        close (soap->sendfd);
        soap->sendfd = -1;
        soap_end(soap);
        soap_done(soap);
    }
}
void castor::srm::server::ResponseWriter::write(srm__srmGetTransferProtocolsResponse_ &myResponse){
    if ( getenv ("SRM_TRACE") ) {
        std::string method = "GetTransferProtocols";
        struct soap* soap = soap_new();
        int fd = init(soap, method);
        soap_init(soap);
        soap->sendfd = fd;
        soap->fsend = castor::srm::server::ResponseWriter::doWrite;
        soap_begin(soap);
        soap_set_omode(soap, SOAP_XML_GRAPH|SOAP_IO_STORE);
        soap_serialize_srm__srmGetTransferProtocolsResponse_(soap, const_cast <srm__srmGetTransferProtocolsResponse_*>(&myResponse));
        soap_begin_send(soap);
        soap_put_srm__srmGetTransferProtocolsResponse_(soap, const_cast<srm__srmGetTransferProtocolsResponse_*>(&myResponse), method.c_str(), NULL);
        soap_end_send(soap);
        close (soap->sendfd);
        soap->sendfd = -1;
        soap_end(soap);
        soap_done(soap);
    }
}

int castor::srm::server::ResponseWriter::init(struct soap*, std::string method) {
    // Open a unique file including timedate std::string in fmt YYYYMMDDhhmmss
    time_t  now = time(NULL);
    struct tm *tp = gmtime(&now);

    char dateStamp[14];
    strftime(dateStamp, sizeof(dateStamp), "%Y%m%d%H%M%S", tp);

    std::string s_template = "/tmp/";
    s_template.append(method).append("_").append(dateStamp).append("_XXXXXX");

    // See if we can open the file - if not return a NULL
    int fd = mkstemp(const_cast<char*>(s_template.c_str()));
    if ( fd == -1 ) return fd;

    // Construct SOAP
    /*
       soap_init(soap);
       soap->sendfd = fd;
       soap->fsend = castor::srm::server::ResponseWriter::doWrite;
       soap_begin(soap);
       soap_set_omode(soap, SOAP_XML_GRAPH|SOAP_IO_STORE);
     */

    return fd;

}

int  castor::srm::server::ResponseWriter::doWrite(struct soap *soap, const char *buffer, size_t size ) {
    ::write (soap->sendfd, (char *)buffer, size);
    return SOAP_OK;
}
