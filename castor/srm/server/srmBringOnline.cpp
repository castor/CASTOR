#include <cgsi_plugin.h>

#include <osdep.h>
#include <castor/BaseAddress.hpp>
#include <castor/BaseObject.hpp>
#include <castor/Constants.hpp>
#include <castor/Services.hpp>
#include <castor/server/Daemon.hpp>
#include <getconfent.h>

#include <castor/exception/Exception.hpp>

#include "soapH.h"

#include "castor/srm/server/SrmUtils.hpp"
#include "castor/srm/server/SrmLogger.hpp"
#include "castor/srm/server/RequestValidator.hpp"
#include "castor/srm/SURL.hpp"
#include "castor/srm/SrmConstants.hpp"
#include "castor/srm/Constants.hpp"
#include "castor/srm/ISrmSvc.hpp"
#include "castor/srm/SrmUser.hpp"
#include "castor/srm/StageRequest.hpp"
#include "castor/srm/StorageArea.hpp"
#include "castor/srm/SubRequest.hpp"
#include "castor/srm/SubRequestStatus.hpp"
#include "castor/srm/UserFile.hpp"
#include "ResponseWriter.hpp"

int srm__srmBringOnline(
        struct soap *soap,
        srm__srmBringOnlineRequest     *myRequest,
        srm__srmBringOnlineResponse_   &myResponse) //< response parameter
{

    castor::srm::server::SrmLogger *myLogger = new castor::srm::server::SrmLogger();
    myLogger->LOGENTRY(soap);

    srm__srmBringOnlineResponse *repp;
    uid_t uid;
    gid_t gid;

    // Prepare the reply structure 
    try {
        repp = soap_new_srm__srmBringOnlineResponse(soap, -1);
        repp->returnStatus = soap_new_srm__TReturnStatus(soap, -1);
        repp->returnStatus->explanation = soap_new_std__string(soap, -1);
        repp->requestToken = NULL;
        repp->arrayOfFileStatuses = NULL;
        repp->remainingTotalRequestTime = NULL;
        repp->remainingDeferredStartTime = NULL;

        repp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREREQUEST_USCOREQUEUED;
        repp->returnStatus->explanation->assign("");
    }
    catch (...) {
        std::list<castor::log::Param> params = {
            castor::log::Param("REQID", myLogger->getId()),
            castor::log::Param("Message", "Out of memory")
        };
        castor::log::write(LOG_ALERT, "Exception caught", params);
        myLogger->LOGEXIT(srm__TStatusCode__SRM_USCOREFAILURE, "Out of memory");
        delete myLogger;
        return SOAP_EOM;
    }
    myResponse.srmBringOnlineResponse = repp;

    if ( repp->returnStatus->statusCode == srm__TStatusCode__SRM_USCORENOT_USCORESUPPORTED ) {
        return SOAP_OK;
    }

    castor::srm::server::ResponseWriter myWriter;

    castor::srm::server::SrmUtils *utils = new castor::srm::server::SrmUtils();
    utils->map_user(soap, &uid, &gid);

    // set up database services
    castor::Services* svcs;
    castor::srm::ISrmSvc* srmSvc;
    castor::BaseAddress ad;
    try {
        svcs = castor::BaseObject::services();
        ad.setCnvSvcName("DbCnvSvc");
        ad.setCnvSvcType(castor::SVC_DBCNV);
        castor::IService *svc = castor::BaseObject::services()->service("DbSrmSvc", castor::SVC_DBSRMSVC);
        srmSvc = dynamic_cast<castor::srm::ISrmSvc*>(svc);
        if ( srmSvc == 0 ) {
            repp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
            repp->returnStatus->explanation->assign("Unable to establish database service");
            std::list<castor::log::Param> params = {castor::log::Param("REQID", myLogger->getId())};
            castor::log::write(LOG_ERR, "Could not get Conversion Service for Database", params);
            myLogger->LOGEXIT(repp->returnStatus->statusCode, repp->returnStatus->explanation->c_str());
            utils->unmap_user();
            delete utils;
            delete myLogger;
            myWriter.write(myResponse);
            return SOAP_OK;
        }
    }
    catch (castor::exception::Exception e) {
        repp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        repp->returnStatus->explanation->assign( e.getMessage().str());
        myLogger->LOGEXCEPTION(&e, &ad);
        myLogger->LOGEXIT(repp->returnStatus->statusCode, repp->returnStatus->explanation->c_str());
        utils->unmap_user();
        delete utils;
        delete myLogger;
        myWriter.write(myResponse);
        return SOAP_OK;
    }

    std::string dn; 
    std::string uname; 
    std::string vo;
    std::string stagerHost;
    // Validate incoming request
    castor::srm::server::RequestValidator validate;
    std::string svcClass("");
    try {
        validate.arrayOfGetFileRequests(myRequest->arrayOfFileRequests);
        if (myRequest->desiredFileStorageType) validate.storageType(*(myRequest->desiredFileStorageType));
        dn    = utils->getDn(soap);
        uname = utils->getUserName(soap);
        vo    = utils->getVoName(soap);
        if ( myRequest->targetSpaceToken ) {
            castor::srm::StorageArea *area = srmSvc->getStorageArea(*(myRequest->targetSpaceToken));
            validate.storageArea(area);
            validate.svcClass(area);
            svcs->fillObj(&ad, area, castor::srm::OBJ_SrmUser);
            validate.permission(area, vo);
            svcClass.assign(area->svcClass());
            delete area->srmUser();
            delete area;
        }
        stagerHost = utils->getStagerHost(soap);
    } catch (castor::exception::Exception &e) {
        castor::srm::SrmException* se = dynamic_cast<castor::srm::SrmException*>(&e);
        if(se) {
            repp->returnStatus->statusCode = (srm__TStatusCode)se->code();
        } else {
            repp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        }
        repp->returnStatus->explanation->assign(e.getMessage().str());
        myLogger->LOGEXIT(repp->returnStatus->statusCode, repp->returnStatus->explanation->c_str());
        utils->unmap_user();
        myWriter.write(myResponse);
        delete utils;
        delete myLogger;
        return SOAP_OK;
    }

    // The only thing left to fill in in the response for this subrequest is the
    // the request expiration time, to be taken either from the request or from the default value.
    signed64 reqTime;
    if ( myRequest->desiredTotalRequestTime ) {
        reqTime = *(myRequest->desiredTotalRequestTime);
    }
    else {
        reqTime = castor::srm::SrmConstants::getInstance()->defaultLifetime();
    }

    castor::srm::StageRequest* dbReq = new castor::srm::StageRequest();
    castor::srm::SrmUser *thisUser = 0;
    try {

        thisUser = srmSvc->getSrmUser(uname, vo, dn);

        // CastorRequestId is assigned by the daemon based on stage_PrepareToGet...
        if (myRequest->userRequestDescription) dbReq->setRequestDesc(*(myRequest->userRequestDescription));
        dbReq->setProtocol("rfio");
        dbReq->setEuid(uid);
        dbReq->setEgid(gid);
        dbReq->setStatus(castor::srm::REQUEST_PENDING);
        dbReq->setSrmUser(thisUser);
        dbReq->setRequestType(castor::srm::REQUESTTYPE_BOL);
        dbReq->setHandler(1);
        dbReq->setLastCheck(time(NULL));
        dbReq->setNextCheck(time(NULL) + 30);    // XXX has it to be configurable?
        dbReq->setProxyCert(" ");
        dbReq->setSvcClass(svcClass);
        dbReq->setExpirationInterval(reqTime);
        dbReq->setSrmRequestId(myLogger->getIdAsString());
        dbReq->setRhHost( stagerHost );
        dbReq->setRhPort( STAGERPORT ); 

        bool validSubrequest = false;
        repp->arrayOfFileStatuses = soap_new_srm__ArrayOfTBringOnlineRequestFileStatus(soap, -1);
        for (unsigned i = 0; i < myRequest->arrayOfFileRequests->requestArray.size(); i++) {
            // Get the subrequest from the request
            srm__TGetFileRequest *fileReq = myRequest->arrayOfFileRequests->requestArray[i];

            // Allocate space for response
            srm__TBringOnlineRequestFileStatus* fileStatus;
            fileStatus = soap_new_srm__TBringOnlineRequestFileStatus(soap, -1);
            fileStatus->sourceSURL.assign( fileReq->sourceSURL );
            fileStatus->fileSize                 = NULL;
            fileStatus->estimatedWaitTime        = NULL;
            fileStatus->status                   = soap_new_srm__TReturnStatus(soap, -1);
            fileStatus->status->explanation      = soap_new_std__string(soap, -1);
            fileStatus->remainingPinTime         = NULL;
            fileStatus->status->statusCode       = srm__TStatusCode__SRM_USCOREREQUEST_USCOREQUEUED;
            fileStatus->status->explanation->assign("");

            // Create a subrequest object
            castor::srm::SubRequest *subrequest = new castor::srm::SubRequest();
            subrequest->setRequest(dbReq);
            subrequest->setStatus(castor::srm::SUBREQUEST_PENDING);
            subrequest->setCastorFileName("");
            subrequest->setSurl(fileReq->sourceSURL);
            subrequest->setErrorCode(0);
            subrequest->setReason("");
            subrequest->setTurl("");

            castor::srm::SURL* surl=0;
            try {
                surl = validate.surl(fileReq->sourceSURL);
            } catch (castor::exception::Exception e) {
                // Surl supplied was invalid
                subrequest->setStatus(castor::srm::SUBREQUEST_FAILED);
                subrequest->setReason(e.getMessage().str());
                fileStatus->status->statusCode = (srm__TStatusCode)e.code();
                fileStatus->status->explanation->assign(e.getMessage().str());
                dbReq->addSubRequests(subrequest);
                repp->arrayOfFileStatuses->statusArray.push_back(fileStatus);
                continue;
            }

            // Get the castor file name and the SURL prefix, then delete the SURL
            std::string cfn = surl->getFileName();
            subrequest->setCastorFileName(cfn);
            delete surl;

            // Get the size of the file and include in reponse.
            Cns_fileid nsId;
            nsId.fileid = 0;
            strncpy(nsId.server, castor::srm::SrmConstants::getInstance()->nsHost().c_str(), sizeof(nsId.server)-1);
            struct Cns_filestat buf;
            myLogger->logCnsCall("Cns_stat", cfn);
            int rc =  Cns_stat(cfn.c_str(), &buf);
            myLogger->logCnsReturn("Cns_stat", rc);
            if ( rc == 0 ) {
                fileStatus->fileSize = (ULONG64*)soap_malloc(soap, sizeof(ULONG64));
                *(fileStatus->fileSize) = buf.filesize;
                nsId.fileid = buf.fileid;
                myLogger->LOGFILEREQ(&nsId, cfn);
                if ( (buf.filemode & S_IFDIR) ) {
                    subrequest->setStatus(castor::srm::SUBREQUEST_FAILED);
                    subrequest->setReason("SURL is a directory and recursion not supported");
                    fileStatus->status->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREPATH;
                    fileStatus->status->explanation->assign("SURL is a directory and recursion not supported");
                    dbReq->addSubRequests(subrequest);
                    repp->arrayOfFileStatuses->statusArray.push_back(fileStatus);
                    continue;
                }
            }
            else {
                switch (serrno) {
                    case ENOENT:
                    case EFAULT:
                    case ENOTDIR:
                    case ENAMETOOLONG:
                        fileStatus->status->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREPATH;
                        fileStatus->status->explanation->assign(sstrerror(serrno));
                        break;
                    case EACCES:
                        fileStatus->status->statusCode = srm__TStatusCode__SRM_USCOREAUTHORIZATION_USCOREFAILURE;
                        fileStatus->status->explanation->assign(sstrerror(serrno));
                        break;
                    case SENOSHOST:
                    case SENOSSERV:
                    case SECOMERR:
                    case ENSNACT:
                        fileStatus->status->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
                        fileStatus->status->explanation->assign(sstrerror(serrno));
                        break;
                    default:
                        fileStatus->status->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
                        fileStatus->status->explanation->assign("Unexpected error status");
                        break;
                }
                repp->arrayOfFileStatuses->statusArray.push_back(fileStatus);
                // We also need to insert this into the database since future StatusOf requests should
                // indicate that this request failed.
                subrequest->setStatus(castor::srm::SUBREQUEST_FAILED);
                subrequest->setErrorCode(serrno);
                subrequest->setReason(sstrerror(serrno));
                dbReq->addSubRequests(subrequest);
                continue;
            }


            // After this, we should have a valid subrequest.
            validSubrequest = true;

            fileStatus->estimatedWaitTime = (int*)soap_malloc(soap, sizeof(int));
            *(fileStatus->estimatedWaitTime) = 5;
            fileStatus->remainingPinTime = (int*)soap_malloc(soap, sizeof(int));
            *(fileStatus->remainingPinTime) = reqTime;
            repp->arrayOfFileStatuses->statusArray.push_back(fileStatus);


            // Create a sub-request for each file.
            if ( fileStatus->fileSize ) {
                subrequest->setReservedSize(buf.filesize); // fileSize as given by Cns_stat()
            }
            else {
                subrequest->setReservedSize(0);
            }

            // create the userFile
            svcs->createRep(&ad, subrequest, false);
            srmSvc->createUserFile(subrequest, nsId.fileid, castor::srm::SrmConstants::getInstance()->nsHost(), buf.filesize);

            // Pins will get added when the subrequest status changes to SUCCESS - 
            // Note this relies on users calling srmStatusOfGetRequest

            // Finally add the subrequest to the request
            dbReq->addSubRequests(subrequest);
        }

        // Finish off filling the request
        thisUser->addRequests(dbReq);
        svcs->createRep(&ad, dbReq, false);
        svcs->fillRep(&ad, dbReq, castor::srm::OBJ_SubRequest, false);
        svcs->fillRep(&ad, dbReq, castor::srm::OBJ_SrmUser, true);
        std::stringstream ss;
        ss << dbReq->id() << std::ends;
        repp->requestToken = soap_new_std__string(soap, -1);
        repp->requestToken->assign( ss.str());

        if ( !validSubrequest ) {
            repp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
            repp->returnStatus->explanation->assign("No valid subrequests found");
            myLogger->LOGEXIT(repp->returnStatus->statusCode, repp->returnStatus->explanation->c_str());
            utils->unmap_user();
            delete myLogger;
            delete utils;
            delete dbReq; dbReq=0;
            delete thisUser;
            myWriter.write(myResponse);
            return SOAP_OK;
        }

        // notify the daemon about the new request
        //try {
        //    castor::server::Daemon::sendNotification(castor::srm::SrmConstants::getInstance()->notifyHost(),
        //      castor::srm::SrmConstants::getInstance()->reqNotifyPort(), 'S');
        //} catch (castor::exception::Exception ignored) {}

    }
    catch (castor::exception::Exception e) {
        repp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        repp->returnStatus->explanation->assign(e.getMessage().str());
        myLogger->LOGEXCEPTION(&e, &ad);
    }
    for ( unsigned i=0; i < dbReq->subRequests().size(); i++ ) {
        delete dbReq->subRequests()[i];
    }
    dbReq->subRequests().clear();
    delete dbReq; dbReq=0;
    delete thisUser;

    myLogger->LOGEXIT(repp->returnStatus->statusCode, repp->returnStatus->explanation->c_str());
    utils->unmap_user();
    delete utils;
    delete myLogger;
    myWriter.write(myResponse);

    return SOAP_OK;
}


