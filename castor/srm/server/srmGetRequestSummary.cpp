#include <cgsi_plugin.h>

#include <castor/BaseAddress.hpp>
#include <castor/BaseObject.hpp>
#include <castor/Constants.hpp>
#include <castor/Services.hpp>
#include <castor/exception/Exception.hpp>

#include "soapH.h"

#include "castor/srm/Constants.hpp"
#include "castor/srm/ISrmSvc.hpp"
#include "castor/srm/StageRequest.hpp"
#include "castor/srm/SubRequest.hpp"
#include "castor/srm/SubRequestStatus.hpp"
#include "castor/srm/SrmUser.hpp"
#include "castor/srm/server/SrmUtils.hpp"
#include "castor/srm/server/SrmLogger.hpp"
#include "castor/srm/server/RequestValidator.hpp"
#include "ResponseWriter.hpp"

int srm__srmGetRequestSummary(
        struct soap *soap,
        srm__srmGetRequestSummaryRequest*   srmGetRequestSummaryRequest,
        srm__srmGetRequestSummaryResponse_ &response) //< response parameter
{
    castor::srm::server::SrmLogger *myLogger = new castor::srm::server::SrmLogger();
    myLogger->LOGENTRY(soap);

    srm__srmGetRequestSummaryResponse *resp;
    try {
        resp = soap_new_srm__srmGetRequestSummaryResponse(soap, -1);
        resp->returnStatus = soap_new_srm__TReturnStatus(soap, -1);
        resp->returnStatus->explanation = soap_new_std__string(soap, -1);
        resp->arrayOfRequestSummaries = NULL;

        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
        resp->returnStatus->explanation->assign("");
    }
    catch (...) {
        std::list<castor::log::Param> params = {
            castor::log::Param("REQID", myLogger->getId()),
            castor::log::Param("Message", "Out of memory")
        };
        castor::log::write(LOG_ALERT, "Exception caught", params);
        myLogger->LOGEXIT(srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR, "Out of memory");
        delete myLogger;
        return SOAP_EOM;
    }
    response.srmGetRequestSummaryResponse = resp;

    castor::srm::server::ResponseWriter myWriter;

    // Set up database
    castor::Services* svcs;
    castor::srm::ISrmSvc* srmSvc;
    castor::BaseAddress ad;
    try {
        svcs = castor::BaseObject::services();
        ad.setCnvSvcName("DbCnvSvc");
        ad.setCnvSvcType(castor::SVC_DBCNV);
        castor::IService *svc = castor::BaseObject::services()->service("DbSrmSvc", castor::SVC_DBSRMSVC);
        srmSvc = dynamic_cast<castor::srm::ISrmSvc*>(svc);
        if ( srmSvc == 0 ) {
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
            resp->returnStatus->explanation->assign("Unable to establish database service");
            std::list<castor::log::Param> params = {castor::log::Param("REQID", myLogger->getId())};
            castor::log::write(LOG_ERR, "Could not get Conversion Service for Database", params);
            myWriter.write(response);
            myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
            delete myLogger;
            return SOAP_OK;
        }
    }
    catch (castor::exception::Exception &e) {
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        resp->returnStatus->explanation->assign( e.getMessage().str());
        myLogger->LOGEXCEPTION(&e, &ad);
        myWriter.write(response);
        myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
        delete myLogger;
        return SOAP_OK;
    }

    // Get the current user
    castor::srm::server::SrmUtils     *utils = new castor::srm::server::SrmUtils();
    std::string dn    = utils->getDn(soap);
    std::string uname = utils->getUserName(soap);
    delete utils;
    castor::srm::server::RequestValidator validate;
    try {
        validate.notNull(srmGetRequestSummaryRequest->arrayOfRequestTokens, "arrayOfRequestTokens");
        validate.validateVectorOfStrings(srmGetRequestSummaryRequest->arrayOfRequestTokens->stringArray);
    } catch (castor::exception::Exception &e) {
        castor::srm::SrmException* se = dynamic_cast<castor::srm::SrmException*>(&e);
        if(se) {
            resp->returnStatus->statusCode = (srm__TStatusCode)se->code();
        } else {
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        }
        resp->returnStatus->explanation->assign(e.getMessage().str());
        myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
        delete myLogger;
        return SOAP_OK;
    }


    // Loop over all requested tokens, and make sure the user owns the token.
    std::vector<std::string> tokens = srmGetRequestSummaryRequest->arrayOfRequestTokens->stringArray;
    resp->arrayOfRequestSummaries = soap_new_srm__ArrayOfTRequestSummary(soap, -1);

    unsigned nOK=0;
    for ( unsigned i=0; i < tokens.size(); i++ ) {
        srm__TRequestSummary *summary   = soap_new_srm__TRequestSummary(soap, -1);
        summary->status                 = soap_new_srm__TReturnStatus(soap, -1);
        summary->status->explanation    = soap_new_std__string(soap, -1);
        summary->requestType            = NULL;
        summary->totalNumFilesInRequest = NULL;
        summary->numOfCompletedFiles    = NULL;
        summary->numOfWaitingFiles      = NULL;
        summary->numOfFailedFiles       = NULL;

        summary->requestToken.assign(tokens[i]);
        summary->status->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
        summary->status->explanation->assign("");


        castor::srm::StageRequest* thisRequest=0;
        try {
            validate.requestToken(tokens[i]);
            thisRequest = srmSvc->getStageRequest(tokens[i]);
        } catch (castor::srm::SrmException &e) {
            summary->status->statusCode = (srm__TStatusCode)e.code();
            summary->status->explanation->assign(e.getMessage().str());
            resp->arrayOfRequestSummaries->summaryArray.push_back(summary);
            continue;
        } catch (castor::exception::Exception &e) {
            summary->status->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST;
            summary->status->explanation->assign("Request token does not exist in SRM: ");
            summary->status->explanation->append(tokens[i]);
            resp->arrayOfRequestSummaries->summaryArray.push_back(summary);
            continue;
        }
        myLogger->setId(thisRequest->srmRequestId());
        myLogger->LOGSTARTEDREQ(1);
        srmSvc->lockStageRequest(thisRequest->id());
        svcs->fillObj(&ad, thisRequest, castor::srm::OBJ_SubRequest, false);
        svcs->commit(&ad);
        // Fill summary
        summary->requestType            = (srm__TRequestType*)soap_malloc(soap, sizeof(srm__TRequestType));
        summary->totalNumFilesInRequest = (int*)soap_malloc(soap, sizeof(int));
        summary->numOfCompletedFiles    = (int*)soap_malloc(soap, sizeof(int));
        summary->numOfWaitingFiles      = (int*)soap_malloc(soap, sizeof(int));
        summary->numOfFailedFiles       = (int*)soap_malloc(soap, sizeof(int));
        *(summary->totalNumFilesInRequest) = thisRequest->subRequests().size();
        *(summary->numOfCompletedFiles)    = 0;
        *(summary->numOfWaitingFiles)      = 0;
        *(summary->numOfFailedFiles)       = 0;
        for ( unsigned j=0; j<thisRequest->subRequests().size(); j++ ) {
            switch ( (int)thisRequest->subRequests()[j]->status() ) {
                case castor::srm::SUBREQUEST_PENDING:
                case castor::srm::SUBREQUEST_INPROGRESS:
                    (*(summary->numOfWaitingFiles))++;
                    break;
                case castor::srm::SUBREQUEST_SUCCESS:
                case castor::srm::SUBREQUEST_ABORTED:
                default:
                    (*(summary->numOfCompletedFiles))++;
                    break;
                case castor::srm::SUBREQUEST_FAILED:
                    (*(summary->numOfFailedFiles))++;
                    break;
            }
        }

        if ( thisRequest->requestType() == castor::srm::REQUESTTYPE_PUT ) {
            *(summary->requestType) = srm__TRequestType__PREPARE_USCORETO_USCOREPUT;
        }
        else if ( thisRequest->requestType() == castor::srm::REQUESTTYPE_GET ) {
            *(summary->requestType) = srm__TRequestType__PREPARE_USCORETO_USCOREGET;
        }
        else if ( thisRequest->requestType() == castor::srm::REQUESTTYPE_BOL ) {
            *(summary->requestType) = srm__TRequestType__BRING_USCOREONLINE;
        }
        else {
            *(summary->requestType) = srm__TRequestType__COPY;
        }
        resp->arrayOfRequestSummaries->summaryArray.push_back(summary);
        // Free up requests
        for (unsigned c=0; c<thisRequest->subRequests().size(); c++)
            delete thisRequest->subRequests()[c];
        delete thisRequest;
        nOK++;
    }

    if ( nOK == 0 ) {
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
        resp->returnStatus->explanation->assign("No request tokens found matching query");
    }
    else if (nOK == tokens.size()) {
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
        resp->returnStatus->explanation->assign("");
    }
    else {
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREPARTIAL_USCORESUCCESS;
        resp->returnStatus->explanation->assign("");
    }
    myWriter.write(response);
    myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
    delete myLogger;
    return SOAP_OK;
}


