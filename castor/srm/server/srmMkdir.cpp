#include <cgsi_plugin.h>
#include <Cns_api.h>
#include <castor/log/log.hpp>

#include <sstream>

#include "soapH.h"

#include "castor/srm/SrmConstants.hpp"
#include "castor/srm/server/SrmUtils.hpp"
#include "castor/srm/server/SrmLogger.hpp"
#include "castor/srm/server/RequestValidator.hpp"
#include "castor/srm/SURL.hpp"

#include "ResponseWriter.hpp"

int srm__srmMkdir(
        struct soap *soap,
        srm__srmMkdirRequest*               srmMkdirRequest,
        srm__srmMkdirResponse_      &rep)//< response parameter
{
    castor::srm::server::SrmLogger *myLogger = new castor::srm::server::SrmLogger();
    myLogger->LOGENTRY(soap);

    srm__srmMkdirRequest *req;
    srm__srmMkdirResponse *resp;
    req = srmMkdirRequest;
    int rc = 0;
    uid_t uid;
    gid_t gid;

    castor::srm::server::SrmUtils *utils = new castor::srm::server::SrmUtils();
    utils->map_user(soap, &uid, &gid);


    //Prepare the reply
    try {
        resp = soap_new_srm__srmMkdirResponse(soap, -1);
        resp->returnStatus = soap_new_srm__TReturnStatus(soap, -1);
        resp->returnStatus->explanation = soap_new_std__string(soap, -1);

        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
        resp->returnStatus->explanation->assign("");
    }
    catch (...) {
        std::list<castor::log::Param> params = {
            castor::log::Param("REQID", myLogger->getId()),
            castor::log::Param("Message", "Out of memory")
        };
        castor::log::write(LOG_ALERT, "Exception caught", params);
        utils->unmap_user();
        myLogger->LOGEXIT(srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR, "Out of memory");
        delete utils;
        delete myLogger;
        return SOAP_EOM;
    }

    //Put resp inside rep struct container
    rep.srmMkdirResponse = resp;

    castor::srm::server::ResponseWriter myWriter;
    castor::srm::server::RequestValidator      validate;

    std::string dirpath;
    try {
        castor::srm::SURL* surl = validate.surl(req->SURL);
        dirpath = surl->getFileName();
        delete surl;
    } catch (castor::exception::Exception e) {
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST;
        resp->returnStatus->explanation->assign("No SURL supplied");
        utils->unmap_user();
        myWriter.write(rep);
        myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
        delete utils;
        delete myLogger;
        return SOAP_OK;
    }

    // Make sure that we only call mkdir where we are allowed to...
    int pathElements=0;
    for ( unsigned letter=0; letter < dirpath.length(); letter++ ) {
        if (dirpath[letter] == '/' && dirpath[letter+1] != '/' ) pathElements++;
        if (pathElements > 3) break;
    }
    if ( pathElements < 3 ) {
        //resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCORENOT_USCORESUPPORTED;
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREAUTHORIZATION_USCOREFAILURE;
        resp->returnStatus->explanation->assign("User can not create a directory at this level");
        std::list<castor::log::Param> params = {
            castor::log::Param("REQID", myLogger->getId()),
            castor::log::Param("Message", *resp->returnStatus->explanation),
            castor::log::Param("SURL", req->SURL),
        };
        castor::log::write(LOG_USER_ERROR, "Invalid Request", params);
        utils->unmap_user();
        myWriter.write(rep);
        myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
        delete utils; 
        delete myLogger;
        return SOAP_OK;
    }

    mode_t mode    = 0775;   // default mode: it should really be 755, with umask = 022. Cf bug CASTOR-5437.
    mode_t oldMask = Cns_umask(0);
    myLogger->logCnsCall("Cns_mkdir", dirpath, 0, LOG_INFO);
    serrno = 0;
    rc = Cns_mkdir(dirpath.c_str(), mode);
    int saveserrno = serrno;
    myLogger->logCnsReturn("Cns_mkdir", rc);
    if (rc != 0) {
        switch (saveserrno) {
            case ENOENT:
            case EFAULT:
            case ENOTDIR:
            case ENAMETOOLONG:
                resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREPATH;
                resp->returnStatus->explanation->assign(sstrerror(serrno));
                break;
            case EEXIST:
                resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREDUPLICATION_USCOREERROR;
                resp->returnStatus->explanation->assign(sstrerror(serrno));
                break;
            case EACCES:
                resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREAUTHORIZATION_USCOREFAILURE;
                resp->returnStatus->explanation->assign(sstrerror(serrno));
                break;
            case ENOSPC:
            case SENOSHOST:
            case SENOSSERV:
            case SECOMERR:
            case ENSNACT:
                resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
                resp->returnStatus->explanation->assign(sstrerror(serrno));
                break;
            default:
                resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
                resp->returnStatus->explanation->assign("Unexpected error status");
                break;
        }
    }

    Cns_umask(oldMask);
    utils->unmap_user();
    myWriter.write(rep);
    myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
    delete utils; 
    delete myLogger;
    return SOAP_OK;
}


