#include <cgsi_plugin.h>

#include <castor/BaseAddress.hpp>
#include <castor/BaseObject.hpp>
#include <castor/Constants.hpp>
#include <castor/Services.hpp>

#include "soapH.h"
#include "castor/srm/Constants.hpp"
#include "castor/srm/ISrmSvc.hpp"
#include "castor/srm/SrmUser.hpp"
#include "castor/srm/StorageArea.hpp"

int srm__srmCompactSpace(
        struct soap *soap,
        srm__srmCompactSpaceRequest*        srmCompactSpaceRequest,
        srm__srmCompactSpaceResponse_& response) //< response parameter
{
    return SOAP_FAULT;
}


