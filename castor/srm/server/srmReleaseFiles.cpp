#include <cgsi_plugin.h>

#include <castor/BaseAddress.hpp>
#include <castor/BaseObject.hpp>
#include <castor/Constants.hpp>
#include <castor/Services.hpp>

#include <castor/exception/Exception.hpp>

#include "soapH.h"

#include "castor/srm/SURL.hpp"
#include "castor/srm/Constants.hpp"
#include "castor/srm/SrmConstants.hpp"
#include "castor/srm/ISrmSvc.hpp"
#include "castor/srm/StageRequest.hpp"
#include "castor/srm/StorageArea.hpp"
#include "castor/srm/SubRequest.hpp"
#include "castor/srm/SrmUser.hpp"
#include "castor/srm/UserFile.hpp"
#include "castor/srm/server/SrmUtils.hpp"
#include "castor/srm/server/SrmLogger.hpp"
#include "castor/srm/server/RequestValidator.hpp"
#include "castor/srm/server/StageIncrementer.hpp"

#include "ResponseWriter.hpp"

int srm__srmReleaseFiles(
        struct soap *soap,
        srm__srmReleaseFilesRequest*        srmReleaseFilesRequest,
        srm__srmReleaseFilesResponse_ &response) //< response parameter
{
    castor::srm::server::SrmLogger *myLogger = new castor::srm::server::SrmLogger();
    myLogger->LOGENTRY(soap);


    srm__srmReleaseFilesResponse *resp;
    try {
        resp = soap_new_srm__srmReleaseFilesResponse(soap, -1);
        resp->returnStatus = soap_new_srm__TReturnStatus(soap, -1);
        resp->returnStatus->explanation = soap_new_std__string(soap, -1);

        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
        resp->returnStatus->explanation->assign("");
        resp->arrayOfFileStatuses = NULL;
    }
    catch (...) {
        std::list<castor::log::Param> params = {
            castor::log::Param("REQID", myLogger->getId()),
            castor::log::Param("Message", "Out of memory")
        };
        castor::log::write(LOG_ALERT, "Exception caught", params);
        myLogger->LOGEXIT(srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR, "Out of memory");
        delete myLogger;
        return SOAP_EOM;
    }
    response.srmReleaseFilesResponse = resp;

    castor::srm::server::ResponseWriter myWriter;

    // We dont support forced releasing
    if ( srmReleaseFilesRequest->doRemove != NULL && *(srmReleaseFilesRequest->doRemove) != false ) {
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCORENOT_USCORESUPPORTED;
        resp->returnStatus->explanation->assign("doRemove option not supported - use srmRm to remove files");
        myWriter.write(response);
        myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
        delete myLogger;
        return SOAP_OK;
    }

    // Set up database services
    castor::Services* svcs;
    castor::srm::ISrmSvc* srmSvc;
    castor::BaseAddress ad;
    try {
        svcs = castor::BaseObject::services();
        ad.setCnvSvcName("DbCnvSvc");
        ad.setCnvSvcType(castor::SVC_DBCNV);
        castor::IService *svc = castor::BaseObject::services()->service("DbSrmSvc", castor::SVC_DBSRMSVC);
        srmSvc = dynamic_cast<castor::srm::ISrmSvc*>(svc);
        if ( srmSvc == 0 ) {
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
            resp->returnStatus->explanation->assign("Unable to establish database service");
            std::list<castor::log::Param> params = {castor::log::Param("REQID", myLogger->getId())};
            castor::log::write(LOG_ERR, "Could not get Conversion Service for Database", params);
            myWriter.write(response);
            myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
            delete myLogger;
            return SOAP_OK;
        }
    }
    catch (castor::exception::Exception e) {
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        resp->returnStatus->explanation->assign( e.getMessage().str());
        std::list<castor::log::Param> params = {
            castor::log::Param("REQID", myLogger->getId()),
            castor::log::Param("Message", "Exception setting up database"),
            castor::log::Param("Details", e.getMessage().str() )
        };
        castor::log::write(LOG_ERR, "Exception caught", params);
        myLogger->LOGEXCEPTION(&e, &ad);
        myWriter.write(response);
        myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
        delete myLogger;
        return SOAP_OK;
    }

    // Get information about current user and check against request owner
    castor::srm::server::SrmUtils *utils = new castor::srm::server::SrmUtils();
    castor::srm::server::RequestValidator      validate;
    std::string                        dn, uname, vo;
    castor::srm::StageRequest                  *thisRequest = 0;
    castor::srm::SrmUser                       *user=0;
    try {
        dn    = utils->getDn(soap);
        uname = utils->getUserName(soap);
        vo    = utils->getVoName(soap);
        user  = srmSvc->getSrmUser(uname, vo, dn);
        if (srmReleaseFilesRequest->requestToken) {
            thisRequest = srmSvc->getStageRequest(srmReleaseFilesRequest->requestToken->c_str());
            myLogger->setId(thisRequest->srmRequestId());
            srmSvc->lockStageRequest(thisRequest->id());
            try {
                validate.request(thisRequest, castor::srm::REQUESTTYPE_GET);
            } catch (...) {
                validate.request(thisRequest, castor::srm::REQUESTTYPE_BOL);
            }
            svcs->fillObj(&ad, thisRequest, castor::srm::OBJ_SrmUser, false);
            validate.permission(uname, dn, 
                    thisRequest->srmUser()->userID(),
                    thisRequest->srmUser()->dn());
            svcs->fillObj(&ad, thisRequest, castor::srm::OBJ_SubRequest, false);
        }
        else {
            // An array of SURL's must be supplied
            validate.notNull(srmReleaseFilesRequest->arrayOfSURLs, "arrayOfSURLs");
            validate.arrayOfSurls(srmReleaseFilesRequest->arrayOfSURLs->urlArray);
        }
    } catch(castor::exception::Exception &e) {
        castor::srm::SrmException* se = dynamic_cast<castor::srm::SrmException*>(&e);
        if(se) {
            resp->returnStatus->statusCode = (srm__TStatusCode)se->code();
        } else {
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        }
        resp->returnStatus->explanation->assign(e.getMessage().str());
        myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
        myWriter.write(response);
        delete utils;
        if ( user ) delete user;
        if ( thisRequest ) {
            if ( thisRequest->srmUser() ) delete thisRequest->srmUser();
            for (unsigned c=0; c<thisRequest->subRequests().size(); c++)
                delete thisRequest->subRequests()[c];
            delete thisRequest;
        }
        delete myLogger;
        return SOAP_OK;
    }

    srm__ArrayOfAnyURI* surls = srmReleaseFilesRequest->arrayOfSURLs;
    std::map<std::string, std::string> files;    //  map of surls and castor file names
    resp->arrayOfFileStatuses = soap_new_srm__ArrayOfTSURLReturnStatus(soap, -1);
    uid_t uid;
    gid_t gid;
    utils->map_user(soap, &uid, &gid);
    // Fill in the File request structure, marking any invalid requests. After this we will loop
    // over valid results to release pins
    if (thisRequest && surls) {
        myLogger->LOGSTARTEDREQ(surls->urlArray.size());
        // Releasing SURL's associated with a request
        for (unsigned i=0; i<surls->urlArray.size(); i++ ) {
            srm__TSURLReturnStatus *status = soap_new_srm__TSURLReturnStatus(soap, -1);
            status->status = soap_new_srm__TReturnStatus(soap, -1);
            status->status->explanation = soap_new_std__string(soap, -1);
            // Assume the surl is not in the subrequest set.
            status->status->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
            status->status->explanation->assign("SURL not in original request");
            status->surl.assign(surls->urlArray[i]);
            resp->arrayOfFileStatuses->statusArray.push_back(status);
            // See if this surl was in the original request
            for (unsigned j=0; j<thisRequest->subRequests().size(); j++) {
                if (surls->urlArray[i] == thisRequest->subRequests()[j]->surl() ) {
                    // Get the UserFile object
                    // so we can log the last known file id
                    std::string cfn;
                    Cns_fileid nsId;
                    nsId.fileid=0;
                    strncpy( nsId.server, castor::srm::SrmConstants::getInstance()->nsHost().c_str(), sizeof(nsId.server)-1);
                    try {
                        castor::srm::UserFile *ufile = 0;
                        castor::srm::SURL* surl = validate.surl(surls->urlArray[i]);
                        cfn = surl->getFileName();
                        ufile = srmSvc->getUserFile(surl->getFileName());
                        if (ufile) {
                            nsId.fileid=ufile->fileId();
                            delete ufile;
                        }
                        delete surl;
                    } catch (castor::exception::Exception e) {
                        status->status->statusCode = (srm__TStatusCode)e.code();
                        status->status->explanation->assign(e.getMessage().str());
                        break;
                    }
                    myLogger->LOGFILEREQ(&nsId, cfn);
                    if ( thisRequest->subRequests()[j]->status() == castor::srm::SUBREQUEST_ABORTED ) {
                        status->status->explanation->assign("This SURL has already been aborted");
                        myLogger->logFileFailure(&nsId, cfn, status->status->explanation->c_str());
                    }
                    else {
                        thisRequest->subRequests()[j]->setStatus(castor::srm::SUBREQUEST_RELEASED);
                        status->status->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
                        status->status->explanation->assign("");
                        svcs->updateRep(&ad, thisRequest->subRequests()[j], false);
                    }
                    break;
                }
            }
        }
    }
    else if (thisRequest) {
        // Release all SURLs associated with the request
        for (unsigned j=0; j<thisRequest->subRequests().size(); j++) {
            srm__TSURLReturnStatus *status = soap_new_srm__TSURLReturnStatus(soap, -1);
            status->status = soap_new_srm__TReturnStatus(soap, -1);
            status->status->explanation = soap_new_std__string(soap, -1);
            status->surl.assign(thisRequest->subRequests()[j]->surl());
            resp->arrayOfFileStatuses->statusArray.push_back(status);
            // Get the UserFile object
            // so we can log the last known file id
            std::string cfn;
            Cns_fileid nsId;
            nsId.fileid=0;
            strncpy( nsId.server, castor::srm::SrmConstants::getInstance()->nsHost().c_str(), sizeof(nsId.server)-1);
            try {
                castor::srm::UserFile *ufile = 0;
                ufile = srmSvc->getUserFile(thisRequest->subRequests()[j]->castorFileName());
                if (ufile) {
                    nsId.fileid=ufile->fileId();
                    delete ufile;
                }
            } catch (castor::exception::Exception e) {
                status->status->statusCode = (srm__TStatusCode)e.code();
                status->status->explanation->assign(e.getMessage().str());
                continue;
            }
            myLogger->LOGFILEREQ(&nsId, cfn);
            if ( thisRequest->subRequests()[j]->status() == castor::srm::SUBREQUEST_ABORTED ) {
                status->status->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
                status->status->explanation->assign("This SURL has already been aborted");
                myLogger->logFileFailure(&nsId, cfn, status->status->explanation->c_str());
            }
            else {
                thisRequest->subRequests()[j]->setStatus(castor::srm::SUBREQUEST_RELEASED);
                status->status->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
                status->status->explanation->assign("");
                svcs->updateRep(&ad, thisRequest->subRequests()[j], false);
            }
        }
    }
    else if (surls) {
      try {
        // Release all pins associated with this file and owned by the user.
        // Warning: this potentially affects multiple requests if the file was requested
        // multiple times from the same user, and as such it offers the possibility for
        // a DoS (see bug #82541 "RFE: improve service protection in case of srmReleaseFiles"
        // for more details).
        // To help protecting the service, we implement here the same throttling
        // that we have for calls going to CASTOR, even if we don't go to CASTOR
        castor::srm::server::StageIncrementer inc;
        inc.increment();
        for (unsigned i=0; i<surls->urlArray.size(); i++ ) {
            srm__TSURLReturnStatus *status = soap_new_srm__TSURLReturnStatus(soap, -1);
            status->status = soap_new_srm__TReturnStatus(soap, -1);
            status->status->explanation = soap_new_std__string(soap, -1);
            status->surl.assign(surls->urlArray[i]);
            status->status->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
            status->status->explanation->assign("");
            resp->arrayOfFileStatuses->statusArray.push_back(status);
            // Get the UserFile object
            // so we can log the last known file id
            std::string cfn;
            Cns_fileid nsId;
            nsId.fileid=0;
            strncpy( nsId.server, castor::srm::SrmConstants::getInstance()->nsHost().c_str(), sizeof(nsId.server)-1);
            try {
                castor::srm::UserFile *ufile = 0;
                castor::srm::SURL* surl = validate.surl(surls->urlArray[i]);
                cfn = surl->getFileName();
                ufile = srmSvc->getUserFile(surl->getFileName());
                if (ufile) {
                    nsId.fileid=ufile->fileId();
                    delete ufile;
                }
            } catch (castor::exception::Exception& e) {
                // Fill file level response and move on
                status->status->statusCode = (e.code() == ENOENT ?
                  srm__TStatusCode__SRM_USCOREINVALID_USCOREPATH : srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR);
                status->status->explanation->assign(e.getMessage().str());
                continue;
            }
            // Get a std::list of subrequests to release
            std::list<castor::srm::SubRequest*> subRequests;
            subRequests = srmSvc->subRequestsToRelease(cfn, user->id());
            if (subRequests.empty()) {
                status->status->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
                status->status->explanation->assign("No pins to release for this SURL");
                continue;
            }
            std::list<castor::srm::SubRequest*>::iterator iter = subRequests.begin();
            // Go through the list and release all subrequests.
            // We have to commit them one by one as this list may span different
            // requests, hence no master lock is protecting us from deadlocks!
            while (iter != subRequests.end()) {
                (*iter)->setStatus(castor::srm::SUBREQUEST_RELEASED);
                svcs->updateRep(&ad, (*iter), true);
                delete (*iter);
                iter++;
            }

            myLogger->LOGFILEREQ(&nsId, cfn);
        }
      } catch (castor::exception::Exception& e) {
          resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
          resp->returnStatus->explanation->assign(e.getMessage().str());
          resp->arrayOfFileStatuses->statusArray.clear();
          soap_delete (soap, resp->arrayOfFileStatuses);
          resp->arrayOfFileStatuses = 0;
          delete utils;
          delete myLogger;
          return SOAP_OK;
      }
    }
    else {
        // We should not get here - validation should stop it
    }

    // Loop over all Valid returns
    unsigned nOK=0;
    for (std::vector<srm__TSURLReturnStatus*>::iterator iter = resp->arrayOfFileStatuses->statusArray.begin();
            iter != resp->arrayOfFileStatuses->statusArray.end();
            iter++
        ) {
        if ((*iter)->status->statusCode == srm__TStatusCode__SRM_USCORESUCCESS) {
            // Make sure the file still exists.  Once we get here we know the SURL is valid
            castor::srm::SURL surl((*iter)->surl);
            std::string fname = surl.getFileName();

            Cns_fileid nsId;
            nsId.fileid=0;
            strncpy( nsId.server, castor::srm::SrmConstants::getInstance()->nsHost().c_str(), sizeof(nsId.server)-1);
            castor::srm::UserFile* ufile = srmSvc->getUserFile(fname);
            if (ufile) {
                nsId.fileid=ufile->fileId();
                delete ufile;
            }

            Cns_filestat buf;
            int rc =  Cns_stat(const_cast<char*>(fname.c_str()), &buf);
            if ( rc != 0 ) {
                switch (serrno) {
                    case ENOENT:
                    case ENOTDIR:
                    case ENAMETOOLONG:
                        (*iter)->status->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREPATH;
                        (*iter)->status->explanation->assign(sstrerror(serrno));
                        myLogger->logFileFailure(&nsId, fname, sstrerror(serrno));
                        continue;
                        break;
                    default:
                        break;
                }
            }
            nOK++;
        }
    }


    if ( nOK == resp->arrayOfFileStatuses->statusArray.size() ) {
        // All OK
    }
    else if ( nOK == 0 ) {
        // All failed
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
        resp->returnStatus->explanation->assign("No pins released");
    }
    else {
        // Partial success
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREPARTIAL_USCORESUCCESS;
        resp->returnStatus->explanation->assign("Some pins released");
    }
    svcs->commit(&ad);
    myWriter.write(response);
    utils->unmap_user();
    delete utils;
    if (user) delete user;
    if ( thisRequest ) {
        if ( thisRequest->srmUser() ) delete thisRequest->srmUser();
        for (unsigned c=0; c<thisRequest->subRequests().size(); c++)
            delete thisRequest->subRequests()[c];
        delete thisRequest;
    }
    myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
    delete myLogger;
    return SOAP_OK;
}


