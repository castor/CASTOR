#ifndef SRM_SERVER_SRMSPACETYPES_HPP
#define SRM_SERVER_SRMSPACETYPES_HPP
namespace castor {
 namespace srm {
    namespace server {
        static enum srm__TRetentionPolicy spaceTypes[] = {
            srm__TRetentionPolicy__REPLICA,
            srm__TRetentionPolicy__OUTPUT,
            srm__TRetentionPolicy__CUSTODIAL
        };
    }
}
#endif
