#include <cgsi_plugin.h>

#include <stager_client_api.h>
#include <castor/BaseAddress.hpp>
#include <castor/BaseObject.hpp>
#include <castor/Constants.hpp>
#include <castor/Services.hpp>
#include <castor/log/log.hpp>
#include <castor/stager/RequestQueryType.hpp>

#include <castor/exception/Exception.hpp>

#include "soapH.h"

#include "castor/srm/SURL.hpp"
#include "castor/srm/server/SrmUtils.hpp"
#include "castor/srm/server/SrmLogger.hpp"
#include "castor/srm/server/RequestValidator.hpp"
#include "castor/srm/server/StageIncrementer.hpp"
#include "castor/srm/SrmConstants.hpp"
#include "castor/srm/Constants.hpp"
#include "castor/srm/ISrmSvc.hpp"
#include "castor/srm/SrmUser.hpp"
#include "castor/srm/StageRequest.hpp"
#include "castor/srm/SubRequest.hpp"
#include "castor/srm/StorageArea.hpp"
#include "castor/srm/UserFile.hpp"

#include "ResponseWriter.hpp"

int srm__srmPutDone(
        struct soap *soap,
        srm__srmPutDoneRequest*             srmPutDoneRequest,
        srm__srmPutDoneResponse_& response) //< response parameter
{
    castor::srm::server::SrmLogger *myLogger = new castor::srm::server::SrmLogger();
    myLogger->LOGENTRY(soap);
    castor::srm::server::SrmUtils *utils = new castor::srm::server::SrmUtils();
    uid_t uid;
    gid_t gid;
    utils->map_user(soap, &uid, &gid);
    srm__srmPutDoneResponse *out;

    try {
        out = soap_new_srm__srmPutDoneResponse(soap, -1);
        out->returnStatus = soap_new_srm__TReturnStatus(soap, -1);
        out->returnStatus->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
        out->returnStatus->explanation = soap_new_std__string(soap, -1);
        out->returnStatus->explanation->assign("");
        out->arrayOfFileStatuses = soap_new_srm__ArrayOfTSURLReturnStatus(soap, -1);
        response.srmPutDoneResponse = out;
    }
    catch (...) {
        std::list<castor::log::Param> params = {
            castor::log::Param("REQID", myLogger->getId()),
            castor::log::Param("Message", "Out of memory")
        };
        castor::log::write(LOG_ALERT, "Exception caught", params);
        utils->unmap_user();
        myLogger->LOGEXIT(srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR, "Out of memory");
        delete utils;
        delete myLogger;
        return SOAP_EOM;
    }

    castor::srm::server::ResponseWriter myWriter;
    castor::srm::server::RequestValidator validate;
    castor::srm::StageRequest* thisRequest=0;
    std::string dn, uname, vo;
    castor::srm::server::StageIncrementer inc;
    castor::Services* svcs;
    castor::srm::ISrmSvc* srmSvc;
    castor::BaseAddress ad;
    
    try {
      // Set up database services
      svcs = castor::BaseObject::services();
      ad.setCnvSvcName("DbCnvSvc");
      ad.setCnvSvcType(castor::SVC_DBCNV);
      castor::IService *svc = castor::BaseObject::services()->service("DbSrmSvc", castor::SVC_DBSRMSVC);
      srmSvc = dynamic_cast<castor::srm::ISrmSvc*>(svc);
      if ( srmSvc == 0 ) {
        castor::exception::Exception e;
        e.getMessage() << "Unable to establish database service";
        throw e;
      }

      dn    = utils->getDn(soap);
      uname = utils->getUserName(soap);
      vo    = utils->getVoName(soap);
      validate.requestToken(srmPutDoneRequest->requestToken);
      validate.notNull(srmPutDoneRequest->arrayOfSURLs, "arrayOfSURLs");
      validate.arrayOfSurls(srmPutDoneRequest->arrayOfSURLs->urlArray);
      thisRequest = srmSvc->getStageRequest(srmPutDoneRequest->requestToken);
      validate.request(thisRequest, castor::srm::REQUESTTYPE_PUT);
      svcs->fillObj(&ad, thisRequest, castor::srm::OBJ_SrmUser, false);
      validate.permission( uname, dn, 
              thisRequest->srmUser()->userID(), 
              thisRequest->srmUser()->dn());
      delete thisRequest->srmUser();

      myLogger->setId(thisRequest->srmRequestId());
      std::vector<xsd__anyURI> surls = srmPutDoneRequest->arrayOfSURLs->urlArray;
      myLogger->LOGSTARTEDREQ(surls.size());
  
      // lock the original request
      srmSvc->lockStageRequest(thisRequest->id());
      // fetch subrequests and commit before going to Castor
      // XXX this may leave requests in an intermediate state
      svcs->fillObj(&ad, thisRequest, castor::srm::OBJ_SubRequest, true);
  
      // Keep a map of file name and file size, since we may lose size
      std::map<std::string,u_signed64> fileSizeMap;
      std::map<std::string, Cns_fileid> idmap;
      // Bulk stager put done structure...
      struct stage_filereq *filereq = 0;
      int nStagePutRequest = 0;
      for ( unsigned i=0; i< surls.size(); i++ ) {
          // Allocate space for a return element
          srm__TSURLReturnStatus *myReturn = soap_new_srm__TSURLReturnStatus(soap, -1);
          myReturn->status = soap_new_srm__TReturnStatus(soap, -1);
          myReturn->status->explanation = soap_new_std__string(soap, -1);
          myReturn->surl = surls[i];
          myReturn->status->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
          myReturn->status->explanation->assign("");
  
          if ( surls[i].length() == 0 ) {
              myReturn->status->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST;
              myReturn->status->explanation->assign("No surl value supplied");
              out->arrayOfFileStatuses->statusArray.push_back(myReturn);
              continue;
          }
  
          // Get the castor filename from the surl
          std::string castorFileName;
          try {
              castor::srm::SURL* thisSURL = validate.surl(surls[i]);
              castorFileName = thisSURL->getFileName();
              delete thisSURL;
          } catch(castor::exception::Exception &e) {
              myReturn->status->statusCode = (srm__TStatusCode)e.code();
              myReturn->status->explanation->assign(e.getMessage().str());
              out->arrayOfFileStatuses->statusArray.push_back(myReturn);
              continue;
          }
  
          // Make sure this SURL was in the original request
          bool validSURL = false;
          for ( unsigned j=0; j<thisRequest->subRequests().size(); j++ ) {
              if ( thisRequest->subRequests()[j]->castorFileName() == castorFileName ) {
                  // We have the subrequest
                  validSURL = true;
                  // Get the ns file id from the userfile object
                  castor::srm::UserFile *userFile = srmSvc->getUserFile(castorFileName);
                  Cns_fileid nsid;
                  nsid.fileid = userFile->fileId();
                  strncpy (nsid.server, castor::srm::SrmConstants::getInstance()->nsHost().c_str(), sizeof(nsid.server)-1);
                  delete userFile;
                  myLogger->LOGFILEREQ(&nsid, castorFileName);
                  if ( thisRequest->subRequests()[j]->status() == castor::srm::SUBREQUEST_SUCCESS) {
                      // This request is good, so we add it to the bulk putDone request
                      struct Cns_filestat stat;
                      u_signed64 size;
                      myLogger->logCnsCall("Cns_stat", castorFileName);
                      int cnsStatRC = Cns_stat(castorFileName.c_str(), &stat);
                      myLogger->logCnsReturn("Cns_stat", cnsStatRC);
                      if ( cnsStatRC != 0 ) {
                          switch (serrno) {
                              case ENOENT:
                              case ENAMETOOLONG:
                              case ENOTDIR:
                                  myReturn->status->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREPATH;
                                  myReturn->status->explanation->assign(sstrerror(serrno));
                                  myLogger->logFileFailure(&nsid, castorFileName, sstrerror(serrno));
                                  break;
                              case EACCES:
                                  myReturn->status->statusCode = srm__TStatusCode__SRM_USCOREAUTHORIZATION_USCOREFAILURE;
                                  myReturn->status->explanation->assign(sstrerror(serrno));
                                  myLogger->logFileFailure(&nsid, castorFileName, sstrerror(serrno));
                                  break;
                              case EFAULT:
                              case SENOSHOST:
                              case SENOSSERV:
                              case SECOMERR:
                              case ENSNACT:
                              default:
                                  // Do nothing - this is just a nameserver issue and should not affect putDone
                                  filereq = (struct stage_filereq *)realloc(filereq, ++nStagePutRequest*sizeof(struct stage_filereq));
                                  if ( !filereq ) {
                                      std::list<castor::log::Param> params = {
                                          castor::log::Param("REQID", myLogger->getId()),
                                          castor::log::Param("Message", "Out of memory")
                                      };
                                      castor::log::write(LOG_ALERT, "Exception caught", params);
                                      utils->unmap_user();
                                      myLogger->LOGEXIT(out->returnStatus->statusCode, out->returnStatus->explanation->c_str());
                                      delete thisRequest;
                                      delete myLogger;
                                      delete utils;
                                      return SOAP_EOM;
                                  }
                                  // Get the file id from the UerFile table
                                  idmap[castorFileName] = nsid;
                                  filereq[nStagePutRequest-1].filename = strdup(castorFileName.c_str());
                                  break;
                          }
                          size = 0;
                      } 
                      else {
                          filereq = (struct stage_filereq *)realloc(filereq, ++nStagePutRequest*sizeof(struct stage_filereq));
                          if ( !filereq ) {
                              std::list<castor::log::Param> params = {
                                  castor::log::Param("REQID", myLogger->getId()),
                                  castor::log::Param("Message", "Out of memory")
                              };
                              castor::log::write(LOG_ALERT, "Exception caught", params);
                              utils->unmap_user();
                              myLogger->LOGEXIT(out->returnStatus->statusCode, out->returnStatus->explanation->c_str());
                              delete thisRequest;
                              delete myLogger;
                              return SOAP_EOM;
                          }
                          // Use the file id from the stat object
                          nsid.fileid = stat.fileid;
                          idmap[castorFileName] = nsid;
                          filereq[nStagePutRequest-1].filename = strdup(castorFileName.c_str());
                          size = stat.filesize;
                          fileSizeMap[castorFileName.c_str()] = size;
                      }
                  }
                  else if ( thisRequest->subRequests()[j]->status() == castor::srm::SUBREQUEST_ABORTED) {
                      myReturn->status->statusCode = srm__TStatusCode__SRM_USCOREABORTED;
                      myReturn->status->explanation->assign("The SURL has been aborted");
                      myLogger->logFileFailure(&nsid, castorFileName, myReturn->status->explanation->c_str());
                  }
                  else if ( thisRequest->subRequests()[j]->status() == castor::srm::SUBREQUEST_PUTDONE ) {
                      myReturn->status->statusCode = srm__TStatusCode__SRM_USCOREDUPLICATION_USCOREERROR;
                      myReturn->status->explanation->assign("PutDone already performed on this SURL");
                      myLogger->logFileFailure(&nsid, castorFileName, myReturn->status->explanation->c_str());
                  }
                  else {
                      // We cant do PutDone on this request since it has the wrong status
                      myReturn->status->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
                      myReturn->status->explanation->assign("The SURL is not in a state where a PutDone can be applied");
                      myLogger->logFileFailure(&nsid, castorFileName, myReturn->status->explanation->c_str());
                  }
                  break;
              }
          } // End loop over subrequest
          if (!validSURL) {
              myReturn->status->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREPATH;
              myReturn->status->explanation->assign("This SURL does not exist in the original request");
          }
          out->arrayOfFileStatuses->statusArray.push_back(myReturn);
      }// End loop over PutDone request
  
      if ( 0 == filereq ) {
          out->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
          out->returnStatus->explanation->assign("No files can be completed");
          utils->unmap_user();
          myWriter.write(response);
          myLogger->LOGEXIT(out->returnStatus->statusCode, out->returnStatus->explanation->c_str());
          for ( unsigned i=0; i<thisRequest->subRequests().size(); i++ ) {
              delete thisRequest->subRequests()[i];
          }
          thisRequest->subRequests().clear();
          delete thisRequest;
          delete utils;
          delete myLogger;
          return SOAP_OK;
      }
  
      inc.increment();
      struct stage_options opts;
      opts.stage_host = strdup(const_cast<char*> (thisRequest->rhHost().c_str()));
      opts.stage_port = thisRequest->rhPort();
      opts.service_class = const_cast<char*>(thisRequest->svcClass().c_str());
      int      nResp;
      char     *respId = NULL;
      unsigned nOK     = 0; // Number of successful putDone's in this request.
      myLogger->logStagerCall("stage_putDone", opts);
      struct stage_fileresp *fileResp;
      int rc = stage_putDone(NULL, filereq, nStagePutRequest, &fileResp, &nResp, &respId, &opts);
      myLogger->logStagerReturn("stage_putDone", rc, respId ? respId : "");
      // Free up filereq structure and opts structure
      free_filereq(filereq, nStagePutRequest);
      free (opts.stage_host);
  
      // In case that took a long time, update the StageRequest and subrequests
      // just in case the user tried to abort the request
      svcs->updateObj(&ad, thisRequest);
      // and lock again the original request
      srmSvc->lockStageRequest(thisRequest->id());
  
      if ( rc == 0 ) {
          // Loop over all responses
          for (int i=0; i<nResp; i++) {
              srm__TSURLReturnStatus* myReturn = 0;
              castor::srm::SubRequest *mySubRequest = 0;
              if ( fileResp[i].filename != NULL && strlen(fileResp[i].filename) > 0 ) {
                  // OK, this is valid; get the SOAP return structure and the database
                  // subrequest corresponding to this file
                  for ( unsigned j = 0; j < out->arrayOfFileStatuses->statusArray.size(); j++ ) {
                    if ( out->arrayOfFileStatuses->statusArray[j]->surl.find(fileResp[i].filename) != std::string::npos ) {
                        myReturn = out->arrayOfFileStatuses->statusArray[j];
                        break;
                    }
                  }
                  for ( unsigned j = 0; j < thisRequest->subRequests().size(); j++) {
                    if ( thisRequest->subRequests()[j]->castorFileName() == fileResp[i].filename ) {
                      mySubRequest = thisRequest->subRequests()[j];
                      break;
                    }
                  }
              }
              if(myReturn && mySubRequest) {
                  // OK, found them. Check again in case someone aborted the request while it was being processed
                  if ( mySubRequest->status() == castor::srm::SUBREQUEST_ABORTED ) {
                      myReturn->status->statusCode = srm__TStatusCode__SRM_USCOREABORTED;
                      myReturn->status->explanation->assign("This subrequest was previously aborted.");
                      myLogger->logFileFailure(&idmap[fileResp[i].filename], fileResp[i].filename, "Subrequest Aborted");
                      continue;
                  }
                  switch (fileResp[i].errorCode) {
                      case 0:
                          myReturn->status->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
                          myReturn->status->explanation->assign("");
                          // Delete pin
                          mySubRequest->setStatus(castor::srm::SUBREQUEST_PUTDONE);
                          svcs->updateRep(&ad, mySubRequest, false);
                          nOK++;
                          break;
                      default:
                          myReturn->status->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
                          if ( fileResp[0].errorMessage != NULL ) {
                              myReturn->status->explanation->assign(fileResp[i].errorMessage);
                          }
                          else {
                              std::stringstream ss;
                              ss << "Bad stage_putDone response: " << fileResp[i].errorCode;
                              myReturn->status->explanation->assign(ss.str());
                          }
                          mySubRequest->setStatus(castor::srm::SUBREQUEST_FAILED);
                          mySubRequest->setReason(myReturn->status->explanation->c_str());
                          svcs->updateRep(&ad, mySubRequest, false);
                          myLogger->logFileFailure(&idmap[fileResp[i].filename], fileResp[i].filename, myReturn->status->explanation->c_str());
                          break;
                  }
              }
              else {
                  // No filename returned or not matching - not sure what to do in this case since we can't
                  // in general be sure which surl this corresponds to unless nStagePutRequest == 1
                  if (nStagePutRequest == 1) {
                      out->arrayOfFileStatuses->statusArray[0]->status->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
                      out->arrayOfFileStatuses->statusArray[0]->status->explanation->assign("");
                      thisRequest->subRequests()[0]->setStatus(castor::srm::SUBREQUEST_PUTDONE);
                      svcs->updateRep(&ad, thisRequest->subRequests()[0], false);
                      nOK++;
                      break;
                  }
                  else {
                      // In the genral case we just fail the request and don't bother returning the array
                  }
              }
          } // End of loop over file responses
          if (fileResp) free_fileresp(fileResp, nResp);
          if (respId) free (respId);
      }
      else {
          // Put Done call failed completely...
          castor::exception::Exception e;
          e.getMessage() << "stage_putdone failure: " << sstrerror(serrno);
          for (unsigned count = 0; count < out->arrayOfFileStatuses->statusArray.size(); count++ ) {
              out->arrayOfFileStatuses->statusArray[count]->status->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
              out->arrayOfFileStatuses->statusArray[count]->status->explanation->assign(out->returnStatus->explanation->c_str());
          }
          for ( unsigned i=0; i<thisRequest->subRequests().size(); i++ ) {
              delete thisRequest->subRequests()[i];
          }
          thisRequest->subRequests().clear();
          throw e;
      }
  
      if ( nOK == srmPutDoneRequest->arrayOfSURLs->urlArray.size() ) {
          // SUCCESS
          out->returnStatus->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
      }
      else if ( nOK == 0  ) {
          // FAILURE
          out->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
          out->returnStatus->explanation->assign("No files succeeded");
      }
      else {
          // PARTIAL_SUCCESS
          out->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREPARTIAL_USCORESUCCESS;
          std::ostringstream ss;
          ss << nOK << " out of " << srmPutDoneRequest->arrayOfSURLs->urlArray.size() << " subrequests succeeded";
          out->returnStatus->explanation->assign(ss.str());
      }
  
      // Update file sizes based on input value
      std::map<std::string, u_signed64>::iterator iter = fileSizeMap.begin();
      while ( iter != fileSizeMap.end() ) {
          castor::srm::UserFile *uf = srmSvc->getUserFile( (*iter).first );
          uf->setFileSize( (*iter).second );
          svcs->updateRep(&ad, uf, false);
          iter++;
          delete uf;
      }
      // Comit all changes.
      svcs->commit(&ad);
      for ( unsigned i=0; i<thisRequest->subRequests().size(); i++ ) {
          delete thisRequest->subRequests()[i];
      }
      thisRequest->subRequests().clear();
    }
    catch (castor::exception::Exception &e) {
        castor::srm::SrmException* se = dynamic_cast<castor::srm::SrmException*>(&e);
        if(se) {
            out->returnStatus->statusCode = (srm__TStatusCode)se->code();
        } else {
            out->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        }
        out->returnStatus->explanation->assign(e.getMessage().str());
    }

    // Send response and cleanup
    myWriter.write(response);
    myLogger->LOGEXIT(out->returnStatus->statusCode, out->returnStatus->explanation->c_str());
    utils->unmap_user();
    if (thisRequest) {
      if (thisRequest->srmUser()) delete thisRequest->srmUser();
      thisRequest->setSrmUser(0);
      delete thisRequest;
    }
    delete utils;
    delete myLogger;
    return SOAP_OK;
}
