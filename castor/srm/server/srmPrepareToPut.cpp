#include <algorithm>
#include <cgsi_plugin.h>

#include <castor/BaseAddress.hpp>
#include <castor/BaseObject.hpp>
#include <castor/Constants.hpp>
#include <castor/Services.hpp>
#include <castor/server/Daemon.hpp>
#include <castor/log/log.hpp>
#include <getconfent.h>

#include <castor/exception/Exception.hpp>

#include "soapH.h"

#include "castor/srm/SURL.hpp"
#include "castor/srm/server/SrmUtils.hpp"
#include "castor/srm/server/SrmLogger.hpp"
#include "castor/srm/server/RequestValidator.hpp"
#include "castor/srm/SrmConstants.hpp"
#include "castor/srm/server/SrmUtils.hpp"
#include "castor/srm/Constants.hpp"
#include "castor/srm/ISrmSvc.hpp"
#include "castor/srm/RequestType.hpp"
#include "castor/srm/SrmUser.hpp"
#include "castor/srm/server/SrmUtils.hpp"
#include "castor/srm/StageRequest.hpp"
#include "castor/srm/SubRequest.hpp"
#include "castor/srm/SubRequestStatus.hpp"
#include "castor/srm/StorageArea.hpp"
#include "castor/srm/StorageStatus.hpp"
#include "castor/srm/UserFile.hpp"

//#include "SrmFileStorageTypes.hpp"
#include "ResponseWriter.hpp"


int srm__srmPrepareToPut(
        struct soap *soap,
        srm__srmPrepareToPutRequest*        srmPrepareToPutRequest,
        srm__srmPrepareToPutResponse_& rep) //< response parameter
{
    castor::srm::server::SrmLogger *myLogger = new castor::srm::server::SrmLogger();
    myLogger->LOGENTRY(soap);


    srm__srmPrepareToPutResponse *repp;
    srm__srmPrepareToPutRequest *req;
    uid_t uid;
    gid_t gid;

    req = srmPrepareToPutRequest;

    // Prepare the reply structure 
    try {
        repp = soap_new_srm__srmPrepareToPutResponse(soap, -1);
        repp->returnStatus = soap_new_srm__TReturnStatus(soap, -1);
        repp->returnStatus->explanation = soap_new_std__string(soap, -1);
        repp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREREQUEST_USCOREQUEUED;
        repp->returnStatus->explanation->assign("");
        repp->requestToken              = NULL;
        repp->arrayOfFileStatuses       = NULL;
        repp->remainingTotalRequestTime = NULL;
    }
    catch (...) {
        std::list<castor::log::Param> params = {
            castor::log::Param("REQID", myLogger->getId()),
            castor::log::Param("Message", "Out of memory")
        };
        castor::log::write(LOG_INFO, "Exception caught", params);
        myLogger->LOGEXIT(srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR, "Out of memory");
        delete myLogger;
        return SOAP_EOM;
    }
    rep.srmPrepareToPutResponse = repp;

    castor::srm::server::ResponseWriter myWriter;
    castor::srm::server::RequestValidator      validate;

    // Set up database...
    castor::Services* svcs;
    castor::srm::ISrmSvc* srmSvc;
    castor::BaseAddress ad;
    try {
        svcs = castor::BaseObject::services();
        ad.setCnvSvcName("DbCnvSvc");
        ad.setCnvSvcType(castor::SVC_DBCNV);
        castor::IService *svc = castor::BaseObject::services()->service("DbSrmSvc", castor::SVC_DBSRMSVC);
        srmSvc = dynamic_cast<castor::srm::ISrmSvc*>(svc);
        if ( srmSvc == 0 ) {
          std::list<castor::log::Param> params = {castor::log::Param("REQID", myLogger->getId())};
          castor::log::write(LOG_ERR, "Could not get Conversion Service for Database", params);
            repp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
            repp->returnStatus->explanation->assign("Unable to establish database service");
            myWriter.write(rep);
            myLogger->LOGEXIT(repp->returnStatus->statusCode, repp->returnStatus->explanation->c_str());
            delete myLogger;
            return SOAP_OK;
        }
    }
    catch (castor::exception::Exception e) {
        myLogger->LOGEXCEPTION(&e, &ad);
        repp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        repp->returnStatus->explanation->assign( e.getMessage().str());
        myLogger->LOGEXIT(repp->returnStatus->statusCode, repp->returnStatus->explanation->c_str());
        delete myLogger;
        return SOAP_OK;
    }

    //Get the requestor information
    castor::srm::server::SrmUtils *utils = new castor::srm::server::SrmUtils();
    std::string dn, uname, vo;
    try {
        dn    = utils->getDn(soap);
        uname = utils->getUserName(soap);
        vo    = utils->getVoName(soap);
    } catch(castor::exception::Exception &e) {
        repp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREAUTHENTICATION_USCOREFAILURE;
        repp->returnStatus->explanation->assign ("Could not get user information");
        myLogger->LOGEXIT(repp->returnStatus->statusCode, repp->returnStatus->explanation->c_str());
        delete utils;   
        delete myLogger;  
        myWriter.write(rep);
        return SOAP_OK;
    }


    // Validate request. 
    // Get the constants class
    castor::srm::SrmConstants *srmConstants = castor::srm::SrmConstants::getInstance();
    std::string       protocol(srmConstants->supportedProtocols()[0]);
    std::string       stagerHost;
    std::string       svcClass;
    try {
        stagerHost = utils->getStagerHost(soap);
        validate.arrayOfPutFileRequests(req->arrayOfFileRequests);
        if (req->desiredFileStorageType)
            validate.storageType(*(req->desiredFileStorageType));
        validate.lifetime(req->desiredTotalRequestTime, true);
        validate.lifetime(req->desiredPinLifeTime, true);
        validate.lifetime(req->desiredFileLifeTime, true);
        if (req->transferParameters && 
            req->transferParameters->arrayOfTransferProtocols &&
            req->transferParameters->arrayOfTransferProtocols->stringArray.size() > 0)
            validate.protocol(req->transferParameters->arrayOfTransferProtocols->stringArray, &protocol);
        if ( req->targetSpaceToken ) {
            castor::srm::StorageArea* area = srmSvc->getStorageArea(*(req->targetSpaceToken));
            validate.storageArea(area);
            validate.svcClass(area);
            svcs->fillObj(&ad, area, castor::srm::OBJ_SrmUser);
            validate.permission(area, vo);
            svcClass = area->svcClass();
            delete area->srmUser();
            delete area;
        } 
        else if (req->targetFileRetentionPolicyInfo) {
            if ( req->targetFileRetentionPolicyInfo->accessLatency ) {
                svcClass = utils->getSvcClass(vo, req->targetFileRetentionPolicyInfo->retentionPolicy,
                        *(req->targetFileRetentionPolicyInfo->accessLatency));
            }
            else {
                svcClass = utils->getSvcClass(vo, req->targetFileRetentionPolicyInfo->retentionPolicy);
            }
        }
        else {
            svcClass = utils->getSvcClass(vo);
        }
                        
    } catch (castor::exception::Exception &e) {
        castor::srm::SrmException* se = dynamic_cast<castor::srm::SrmException*>(&e);
        if(se) {
            repp->returnStatus->statusCode = (srm__TStatusCode)se->code();
        } else {
            repp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        }
        repp->returnStatus->explanation->assign(e.getMessage().str());
        myLogger->LOGEXIT(repp->returnStatus->statusCode, repp->returnStatus->explanation->c_str());
        delete utils;   
        delete myLogger;  
        myWriter.write(rep);
        return SOAP_OK;
    }

    utils->map_user(soap, &uid, &gid);
    castor::srm::StageRequest *thisRequest = NULL;
    castor::srm::SrmUser*     thisUser = 0;
    try {
        thisUser = srmSvc->getSrmUser(uname, vo, dn);
        std::list<castor::log::Param> params = {castor::log::Param("REQID", myLogger->getId())};
        castor::log::write(LOG_DEBUG, "Creating Database Entry", params);
        // Populate the stage request
        thisRequest = new castor::srm::StageRequest();
        thisRequest->setSrmUser(thisUser);
        thisRequest->setProtocol(protocol);
        thisRequest->setRequestType(castor::srm::REQUESTTYPE_PUT);
        thisRequest->setHandler(1);
        thisRequest->setEuid(uid);
        thisRequest->setEgid(gid);
        thisRequest->setExpirationInterval(srmConstants->defaultLifetime());
        thisRequest->setStatus(castor::srm::REQUEST_PENDING);
        thisRequest->setProxyCert(" ");
        thisRequest->setSrmRequestId(myLogger->getIdAsString());
        thisRequest->setRhHost( stagerHost );
        thisRequest->setRhPort( STAGERPORT ); 

        if ( req->userRequestDescription) thisRequest->setRequestDesc(req->userRequestDescription->c_str());
        if ( req->overwriteOption == NULL || *(req->overwriteOption) == srm__TOverwriteMode__NEVER) {
            thisRequest->setOverwriteOption(0);
        }
        else {
            thisRequest->setOverwriteOption(1);
        }

        if ( svcClass.length() > 0 ) thisRequest->setSvcClass(svcClass);

        // Work over subrequests
        repp->arrayOfFileStatuses = soap_new_srm__ArrayOfTPutRequestFileStatus(soap, -1);
        for ( unsigned i=0; i<req->arrayOfFileRequests->requestArray.size(); i++ ) {
            // Get the file request
            srm__TPutFileRequest* fileReq = req->arrayOfFileRequests->requestArray[i];

            // Get the file size from the request, if given, or use the default size if not
            ULONG64 fileSize;
            if (fileReq->expectedFileSize)
                fileSize = *(fileReq->expectedFileSize);
            else 
                fileSize = srmConstants->defaultFileSize();
                
            // Create a subrequest object and a response object
            castor::srm::SubRequest *sub = new castor::srm::SubRequest();
            srm__TPutRequestFileStatus* fileStatus = soap_new_srm__TPutRequestFileStatus(soap, -1);

            // Initialise the response object
            fileStatus->SURL.assign(fileReq->targetSURL->c_str());
            fileStatus->status                = soap_new_srm__TReturnStatus(soap, -1);
            fileStatus->status->explanation   = soap_new_std__string(soap, -1);
            fileStatus->fileSize              = NULL;
            fileStatus->estimatedWaitTime     = NULL;
            fileStatus->remainingPinLifetime  = NULL;
            fileStatus->remainingFileLifetime = NULL;
            fileStatus->transferURL           = NULL;
            fileStatus->transferProtocolInfo  = NULL;
            fileStatus->status->statusCode = srm__TStatusCode__SRM_USCOREREQUEST_USCOREQUEUED;
            fileStatus->status->explanation->assign("");

            // Initialse the subrequest object
            if (req->targetSpaceToken)
                sub->setSpaceToken(req->targetSpaceToken->c_str());
            sub->setTurl("");
            sub->setCastorFileName("");
            sub->setReservedSize(0);
            sub->setReason("");
            sub->setErrorCode(0);
            sub->setSurl(fileReq->targetSURL->c_str());
            sub->setCastorReqId("");
            sub->setSvcClass(svcClass);
            sub->setStatus(castor::srm::SUBREQUEST_PENDING);
            sub->setRequest(thisRequest);


            // Get the castor file name
            std::string cfn;
            try {
                castor::srm::SURL* mySURL = validate.surl(fileReq->targetSURL->c_str());
                cfn = mySURL->getFileName();
                delete mySURL;
            } catch(castor::exception::Exception &e) {
                sub->setStatus(castor::srm::SUBREQUEST_FAILED);
                sub->setReason( e.getMessage().str() );
                thisRequest->addSubRequests(sub);
                fileStatus->status->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
                fileStatus->status->explanation->assign(e.getMessage().str());
                repp->arrayOfFileStatuses->statusArray.push_back(fileStatus);
                continue;
            }
                
            // OK, so now we have a valid request
            // Fill in some more of the subrequest
            fileStatus->fileSize = (ULONG64*)soap_malloc(soap, sizeof(ULONG64));
            *(fileStatus->fileSize) = fileSize;
            sub->setCastorFileName(cfn);
            sub->setReservedSize(fileSize);

            // create the subRequest in the database and attach the userFile
            svcs->createRep(&ad, sub, false);
            srmSvc->createUserFile(sub, 0, "");
            
            // Add the soap and databse entries to their parents
            thisRequest->addSubRequests(sub);
            repp->arrayOfFileStatuses->statusArray.push_back(fileStatus);
        }

        // Once we are here we can create the database entries and assign
        // a final value to the soap response.
        svcs->createRep(&ad, thisRequest, false);
        svcs->fillRep(&ad, thisRequest, castor::srm::OBJ_SubRequest, false);
        svcs->fillRep(&ad, thisRequest, castor::srm::OBJ_SrmUser, true);
        std::list<castor::log::Param> params2 = {castor::log::Param("REQID", myLogger->getId())};
        castor::log::write(LOG_DEBUG, "Request stored in DB", params2);

        // notify the daemon about the new request
        //try {
        //     castor::server::Daemon::sendNotification(castor::srm::SrmConstants::getInstance()->notifyHost(),
        //       castor::srm::SrmConstants::getInstance()->reqNotifyPort(), 'S');
        //} catch (castor::exception::Exception ignored) {}

        // If any request succeeded, assume the overall response is succeeded, otherwise failure
        bool succeeded = false;
        for ( unsigned i=0; i<thisRequest->subRequests().size(); i++ ) {
            if ( thisRequest->subRequests()[i]->status() == castor::srm::SUBREQUEST_PENDING ) {
                succeeded = true;
                break;
            }
        }
        if ( !succeeded ) {
            repp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
            repp->returnStatus->explanation->assign("All subrequests failed");
        }
        else {
            std::stringstream ss;
            ss << thisRequest->id();
            repp->requestToken = soap_new_std__string(soap, -1);
            repp->requestToken->assign (ss.str());
            std::list<castor::log::Param> okP = {
                castor::log::Param("REQID", myLogger->getId()),
                castor::log::Param("RequestToken", repp->requestToken->c_str())
            };
            castor::log::write(LOG_INFO, "RequestToken assigned", okP);
        }
    }
    catch ( castor::exception::Exception x ) {
        if ( x.code() == ENAMETOOLONG ) {
            repp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREPATH;
        }
        else {
            repp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        }
        repp->returnStatus->explanation->assign(x.getMessage().str());
        myLogger->LOGEXCEPTION(&x, &ad);
    }

    if(thisRequest) {
      for (std::vector<castor::srm::SubRequest*>::const_iterator it = thisRequest->subRequests().begin();
           it != thisRequest->subRequests().end();
           it++) {
        if((*it)->userFile()) {
          delete (*it)->userFile();
        }
        (*it)->setUserFile(0);
        (*it)->setRequest(0);
        delete (*it);
      }
      thisRequest->subRequests().clear();
      delete thisRequest;
    }
    delete thisUser;
    utils->unmap_user();
    myWriter.write(rep);
    myLogger->LOGEXIT(repp->returnStatus->statusCode, repp->returnStatus->explanation->c_str());
    delete utils;
    delete myLogger;
    return SOAP_OK;
}


