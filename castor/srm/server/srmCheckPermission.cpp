#include <castor/log/log.hpp>
#include "soapH.h"

#include "castor/srm/server/SrmUtils.hpp"
#include "castor/srm/server/SrmLogger.hpp"
#include "castor/srm/server/RequestValidator.hpp"
#include "castor/srm/SURL.hpp"
#include "castor/srm/SrmConstants.hpp"
#include "castor/srm/server/SrmPermissionTypes.hpp"
#include "castor/srm/server/ResponseWriter.hpp"

int srm__srmCheckPermission(
        struct soap *soap,
        srm__srmCheckPermissionRequest*     srmCheckPermissionRequest,
        srm__srmCheckPermissionResponse_    &response) //< response parameter
{
    castor::srm::server::SrmLogger *myLogger = new castor::srm::server::SrmLogger();
    myLogger->LOGENTRY(soap);

    srm__srmCheckPermissionResponse *resp;
    try {
        resp = soap_new_srm__srmCheckPermissionResponse(soap, -1);
        resp->arrayOfPermissions = soap_new_srm__ArrayOfTSURLPermissionReturn(soap, -1);
        resp->returnStatus = soap_new_srm__TReturnStatus(soap, -1);
        resp->returnStatus->explanation = soap_new_std__string(soap, -1);

        //resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCORENOT_USCORESUPPORTED;
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
        resp->returnStatus->explanation->assign("");
    }
    catch(...) {
        myLogger->LOGEXCEPTION(0, 0);
        myLogger->LOGEXIT(srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR, "Out of memory");
        delete myLogger;
        return SOAP_EOM;
    }
    response.srmCheckPermissionResponse = resp;
    
    castor::srm::server::ResponseWriter myWriter;
    castor::srm::server::RequestValidator      validate;

    // Get std::vector of SURLs
    try {
        validate.notNull(srmCheckPermissionRequest->arrayOfSURLs, "arrayOfSURLs");
        validate.arrayOfSurls(srmCheckPermissionRequest->arrayOfSURLs->urlArray);
    } catch (castor::exception::Exception &e) {
        castor::srm::SrmException* se = dynamic_cast<castor::srm::SrmException*>(&e);
        if(se) {
            resp->returnStatus->statusCode = (srm__TStatusCode)se->code();
        } else {
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        }
        resp->returnStatus->explanation->assign(e.getMessage().str());
        myLogger->LOGEXIT( resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
        myWriter.write(response);
        delete myLogger;
        return SOAP_OK;
    }
    std::vector<xsd__anyURI> surls = srmCheckPermissionRequest->arrayOfSURLs->urlArray;

    // Map the the user
    uid_t uid;
    gid_t gid;
    castor::srm::server::SrmUtils *utils = new castor::srm::server::SrmUtils();
    utils->map_user(soap, &uid, &gid);
    utils->unmap_user();
    delete utils;

    // Loop over all of these
    unsigned nOK = 0;
    for ( unsigned i=0; i<surls.size(); i++ ) {
        // We will need the Cns_fileid for logging purposes
        Cns_fileid nsId;
        strncpy( nsId.server, castor::srm::SrmConstants::getInstance()->nsHost().c_str(), sizeof(nsId.server)-1);
        nsId.fileid=0;

        // Allocate space for a permissions retrn
        srm__TSURLPermissionReturn *thisPermission = soap_new_srm__TSURLPermissionReturn(soap, -1);
        thisPermission->status = soap_new_srm__TReturnStatus(soap, -1);
        thisPermission->status->explanation = soap_new_std__string(soap, -1);
        thisPermission->permission = NULL;

        thisPermission->surl.assign(surls[i]);
        thisPermission->status->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
        thisPermission->status->explanation->assign("");

        // Get the castor file name
        std::string cfn;
        try {
            castor::srm::SURL* surl = validate.surl(surls[i]);
            cfn = surl->getFileName();
            delete surl;
        } catch(castor::exception::Exception &e) {
            thisPermission->status->statusCode = (srm__TStatusCode)e.code();
            thisPermission->status->explanation->assign(e.getMessage().str());
            resp->arrayOfPermissions->surlPermissionArray.push_back(thisPermission);
            continue;
        }

        Cns_filestat buf;
        if ( Cns_stat(cfn.c_str(), &buf) != 0 ) {
            switch (serrno) {
                case ENOENT:
                case ENOTDIR:
                    thisPermission->status->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREPATH;
                    break;
                case EACCES:
                    thisPermission->status->statusCode = srm__TStatusCode__SRM_USCOREAUTHORIZATION_USCOREFAILURE;
                    break;
                case EFAULT:
                case ENAMETOOLONG:
                    thisPermission->status->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST;
                case SENOSHOST:
                case SENOSSERV:
                case SECOMERR:
                case ENSNACT:
                default:
                    thisPermission->status->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
            }
            thisPermission->status->explanation->assign(sstrerror(serrno));
            resp->arrayOfPermissions->surlPermissionArray.push_back(thisPermission);
            std::list<castor::log::Param> params = {
                castor::log::Param("REQID", myLogger->getId()),
                castor::log::Param("Message", *thisPermission->status->explanation),
                castor::log::Param("FileName", cfn),
                castor::log::Param("Details", thisPermission->status->explanation->c_str() )
            };
            castor::log::write(LOG_WARNING, "Subrequest status", params);
            continue;
        }
        nsId.fileid = buf.fileid;
        myLogger->LOGFILEREQ(&nsId, cfn);

        // Allocate space for returns
        Cns_acl *mylist = (Cns_acl*)malloc(CA_MAXACLENTRIES * sizeof(Cns_acl));
        myLogger->logCnsCall("Cns_getacl", cfn, &nsId, LOG_INFO);
        Cns_setid(uid, gid);
        int nEntries = Cns_getacl(cfn.c_str(), CA_MAXACLENTRIES, mylist); 
        Cns_unsetid();
        myLogger->logCnsReturn( "Cns_getacl", nEntries, &nsId );
                
        if ( nEntries < 0 ) {
            switch (serrno) {
                case ENOENT:
                case EFAULT:
                case ENOTDIR:
                case EINVAL:
                case ENAMETOOLONG:
                    thisPermission->status->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREPATH;
                    thisPermission->status->explanation->assign(sstrerror(serrno));
                    break;
                case EACCES:
                    thisPermission->status->statusCode = srm__TStatusCode__SRM_USCOREAUTHENTICATION_USCOREFAILURE;
                    thisPermission->status->explanation->assign(sstrerror(serrno));
                    break;
                case ENOSPC:
                    thisPermission->status->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
                    thisPermission->status->explanation->assign(sstrerror(serrno));
                    break;
                case SENOSHOST:
                case SENOSSERV:
                case SECOMERR:
                case ENSNACT:
                    thisPermission->status->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
                    thisPermission->status->explanation->assign(sstrerror(serrno));
                    break;
                default:
                    thisPermission->status->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
                    thisPermission->status->explanation->assign("Unexpected error");
                    break;
            }
            resp->arrayOfPermissions->surlPermissionArray.push_back(thisPermission);
            free (mylist);
            continue;
        }

        thisPermission->permission = (srm__TPermissionMode*)soap_malloc(soap, sizeof(srm__TPermissionMode));
        // Loop over all entries
        Cns_acl *myacl = NULL;
        for ( int entry = 0; entry < nEntries; entry++ ) {
            // See is the user exists, if so, use their permission
            if ( mylist[entry].a_type ==  CNS_ACL_USER ||
                 mylist[entry].a_type ==  CNS_ACL_USER_OBJ ) {
                if ( mylist[entry].a_id == (int)uid ) {
                    myacl = &mylist[entry];
                    break;
                }
            }
            else if ( mylist[entry].a_type == CNS_ACL_GROUP ||
                      mylist[entry].a_type == CNS_ACL_GROUP_OBJ ) {
                if ( mylist[entry].a_id == (int)gid ) {
                    myacl = &mylist[entry];
                    break;
                }
            }
            else if ( mylist[entry].a_type == CNS_ACL_OTHER ) {
                myacl = &mylist[entry];
            }
                    
        }
        if ( myacl == NULL ) {
            // User has no permission to access this file
            *(thisPermission->permission) = castor::srm::server::ptypes[srm__TPermissionMode__NONE];
        }
        else {
            switch (myacl->a_perm) {
                case 0:
                default:
                    *(thisPermission->permission) = castor::srm::server::ptypes[0];
                    break;
                case 1:
                    *(thisPermission->permission) = castor::srm::server::ptypes[1];
                    break;
                case 2:
                    *(thisPermission->permission) = castor::srm::server::ptypes[2];
                    break;
                case 3:
                    *(thisPermission->permission) = castor::srm::server::ptypes[3];
                    break;
                case 4:
                    *(thisPermission->permission) = castor::srm::server::ptypes[4];
                    break;
                case 5:
                    *(thisPermission->permission) = castor::srm::server::ptypes[5];
                    break;
                case 6:
                    *(thisPermission->permission) = castor::srm::server::ptypes[6];
                    break;
                case 7:
                    *(thisPermission->permission) = castor::srm::server::ptypes[7];
                    break;
            }
        }
        resp->arrayOfPermissions->surlPermissionArray.push_back(thisPermission);
        free (mylist);
        nOK++;
    }

    if ( nOK == 0 ) {
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREFAILURE;
        resp->returnStatus->explanation->assign("No permissions successfully obtained");
    }
    else if ( nOK < surls.size() ) {
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREPARTIAL_USCORESUCCESS;
        resp->returnStatus->explanation->assign("");
    }

    myLogger->LOGEXIT( resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
    delete myLogger;
    myWriter.write(response);
    return SOAP_OK;
}
