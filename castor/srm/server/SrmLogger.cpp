// System Headers
#include <iostream>
#include <fstream>

// CASTOR headers
#include <castor/log/log.hpp>
#include <castor/BaseObject.hpp>
#include <castor/Services.hpp>

// SRM headers
#include <castor/srm/server/SrmUtils.hpp>
#include <castor/srm/server/SrmLogger.hpp>

castor::srm::server::SrmLogger::SrmLogger() {
    Cuuid_create(&_myReqId);
    // for monitoring purposes
    gettimeofday(&tvStart, NULL);
    _targetReqId = "";
}

castor::srm::server::SrmLogger::SrmLogger(std::string targetReqId) {
    SrmLogger();
    _targetReqId = targetReqId;
}

std::string castor::srm::server::SrmLogger::decodeStatus(srm__TStatusCode status) {
    switch ( status ) {
        case srm__TStatusCode__SRM_USCORESUCCESS: 
            return "SRM_SUCCESS";
            break;
        case srm__TStatusCode__SRM_USCOREFAILURE: 
            return "SRM_FAILURE";
            break;
        case srm__TStatusCode__SRM_USCOREAUTHENTICATION_USCOREFAILURE: 
            return "SRM_AUTHENTICATION_FAILURE";
            break;
        case srm__TStatusCode__SRM_USCOREAUTHORIZATION_USCOREFAILURE: 
            return "SRM_AUTHORIZATION_FAILURE";
            break;
        case srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST: 
            return "SRM_INVALID_REQUEST";
            break;
        case srm__TStatusCode__SRM_USCOREINVALID_USCOREPATH: 
            return "SRM_INVALID_PATH";
            break;
        case srm__TStatusCode__SRM_USCOREFILE_USCORELIFETIME_USCOREEXPIRED: 
            return "SRM_FILE_LIFETIME_EXPIRED";
            break;
        case srm__TStatusCode__SRM_USCORESPACE_USCORELIFETIME_USCOREEXPIRED: 
            return "SRM_SPECE_LIFETIME_EXPIRED";
            break;
        case srm__TStatusCode__SRM_USCOREEXCEED_USCOREALLOCATION: 
            return "SRM_EXCEED_ALLOCATION";
            break;
        case srm__TStatusCode__SRM_USCORENO_USCOREUSER_USCORESPACE: 
            return "SRM_NO_USER_SPACE";
            break;
        case srm__TStatusCode__SRM_USCORENO_USCOREFREE_USCORESPACE: 
            return "SRM_NO_FREE_SPACE";
            break;
        case srm__TStatusCode__SRM_USCOREDUPLICATION_USCOREERROR: 
            return "SRM_DUPLICATION_ERROR";
            break;
        case srm__TStatusCode__SRM_USCORENON_USCOREEMPTY_USCOREDIRECTORY: 
            return "SRM_NON_EMPTY_DIRECTORY";
            break;
        case srm__TStatusCode__SRM_USCORETOO_USCOREMANY_USCORERESULTS: 
            return "SRM_TOO_MANY_RESULTS";
            break;
        case srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR: 
            return "SRM_INTERNAL_ERROR";
            break;
        case srm__TStatusCode__SRM_USCOREFATAL_USCOREINTERNAL_USCOREERROR: 
            return "SRM_FATAL_INTERNAL_ERROR";
            break;
        case srm__TStatusCode__SRM_USCORENOT_USCORESUPPORTED: 
            return "SRM_NOT_SUPPORTED";
            break;
        case srm__TStatusCode__SRM_USCOREREQUEST_USCOREQUEUED: 
            return "SRM_REQUEST_QUEUED";
            break;
        case srm__TStatusCode__SRM_USCOREREQUEST_USCOREINPROGRESS: 
            return "SRM_REQUEST_INPROGESS";
            break;
        case srm__TStatusCode__SRM_USCOREREQUEST_USCORESUSPENDED: 
            return "SRM_REQUEST_SUSPENDED";
            break;
        case srm__TStatusCode__SRM_USCOREABORTED: 
            return "SRM_ABORTED";
            break;
        case srm__TStatusCode__SRM_USCORERELEASED: 
            return "SRM_RELEASED";
            break;
        case srm__TStatusCode__SRM_USCOREFILE_USCOREPINNED: 
            return "SRM_FILE_PINNED";
            break;
        case srm__TStatusCode__SRM_USCOREFILE_USCOREIN_USCORECACHE: 
            return "SRM_FILE_IN_CACHE";
            break;
        case srm__TStatusCode__SRM_USCORESPACE_USCOREAVAILABLE: 
            return "SRM_SPACE_AVAILABLE";
            break;
        case srm__TStatusCode__SRM_USCORELOWER_USCORESPACE_USCOREGRANTED: 
            return "SRM_LOWER_SPACE_GRANTED";
            break;
        case srm__TStatusCode__SRM_USCOREDONE: 
            return "SRM_DONE";
            break;
        case srm__TStatusCode__SRM_USCOREPARTIAL_USCORESUCCESS: 
            return "SRM_PARTIAL_SUCCESS";
            break;
        case srm__TStatusCode__SRM_USCOREREQUEST_USCORETIMED_USCOREOUT: 
            return "SRM_REQUEST_TIMED_OUT";
            break;
        case srm__TStatusCode__SRM_USCORELAST_USCORECOPY: 
            return "SRM_LAST_COPY";
            break;
        case srm__TStatusCode__SRM_USCOREFILE_USCOREBUSY: 
            return "SRM_FILE_BUSY";
            break;
        case srm__TStatusCode__SRM_USCOREFILE_USCORELOST: 
            return "SRM_FILE_LOST";
            break;
        case srm__TStatusCode__SRM_USCOREFILE_USCOREUNAVAILABLE: 
            return "SRM_FILE_UNAVAILABLE";
            break;
        case srm__TStatusCode__SRM_USCORECUSTOM_USCORESTATUS: 
            return "SRM_CUSTOM_STATUS";
            break;
        default:
            return "UNKNOWN STATUS/ERROR";
            break;
    }
    return "UNKNOWN STATUS/ERROR";
}

void castor::srm::server::SrmLogger::logEntry(struct soap *soap, std::string function) {
    castor::srm::server::SrmUtils *utils = new castor::srm::server::SrmUtils();
    std::string client = "unknown", dn = "unknown",
                  user = "unknown", vo = "unknown",
                  role = "unknown";
    try {
        client = utils->getClientAddress(soap);
        dn = utils->getDn(soap);
        user = utils->getUserName(soap);
        vo = utils->getVoName(soap);
        role = utils->getVoRole(soap);
    } catch(...) {
        // ignore
    }
    delete utils;

    std::list<castor::log::Param> initParams = {
        castor::log::Param("REQID", _myReqId),
        castor::log::Param("RequestType", function.substr(function.find('_')+2)),  // strip off the 'srm__' prefix
        castor::log::Param("Client", client),
        castor::log::Param("DN", dn),
        castor::log::Param("VO", vo),
        castor::log::Param("Role", role),
        castor::log::Param("UserName", user)
    };
    // "New Request Arrival"
    castor::log::write(LOG_INFO, "New Request Arrival", initParams);
}

void castor::srm::server::SrmLogger::logStartedReq(std::string function, unsigned count) {
    std::list<castor::log::Param> initParams = {
        castor::log::Param("REQID", _myReqId),
        castor::log::Param("RequestType", function.substr(function.find('_')+2)),  // strip off the 'srm__' prefix
        castor::log::Param("TargetSrmReqId", _targetReqId),
        castor::log::Param("NbFiles", count)
    };
    // "Started request processing"
    castor::log::write(LOG_INFO, "Started request processing", initParams);
}

void castor::srm::server::SrmLogger::logFileRequest(Cns_fileid* nsId, std::string filename, std::string function) {
    std::list<castor::log::Param> initParams = {
        castor::log::Param("NSFILEID", (nsId ? nsId->fileid : 0)),
        castor::log::Param("REQID", _myReqId),
        castor::log::Param("RequestType", function.substr(function.find('_')+2)),  // strip off the 'srm__' prefix
        castor::log::Param("FileName", filename),
        castor::log::Param("TargetSrmReqId", _targetReqId)
    };
    // "Started file processing"
    castor::log::write(LOG_INFO, "Started file processing", initParams);
}

void castor::srm::server::SrmLogger::logFileFailure(Cns_fileid* nsId, std::string filename, std::string message) {
    std::list<castor::log::Param> initParams = {
        castor::log::Param("NSFILEID", (nsId ? nsId->fileid : 0)),
        castor::log::Param("REQID", _myReqId),
        castor::log::Param("FileName", filename),
        castor::log::Param("Message", message),
        castor::log::Param("TargetSrmReqId", _targetReqId)
    };
    // "File processing failed"
    castor::log::write(LOG_WARNING, "File processing failed", initParams);
}

void castor::srm::server::SrmLogger::logExit(srm__TStatusCode status, std::string message, std::string function) {
    // Calculate statistics
    timeval tv;
    gettimeofday(&tv, NULL);
    signed64 procTime = ((tv.tv_sec * 1000000) + tv.tv_usec) - ((tvStart.tv_sec * 1000000) + tvStart.tv_usec);
    if ( status == srm__TStatusCode__SRM_USCORESUCCESS || 
         status == srm__TStatusCode__SRM_USCOREREQUEST_USCOREQUEUED || 
         status == srm__TStatusCode__SRM_USCOREREQUEST_USCOREINPROGRESS ) {
        std::list<castor::log::Param> exitParams = {
            castor::log::Param("REQID", _myReqId),
            castor::log::Param("RequestType", function.substr(function.find('_')+2)),  // strip off the 'srm__' prefix
            castor::log::Param("ExitStatus", decodeStatus(status) ),
            castor::log::Param("ProcessingTime", procTime * 0.000001),
            castor::log::Param("TargetSrmReqId", _targetReqId)
        };
        // "Request Succeeded"
        castor::log::write(LOG_INFO, "Request Succeeded", exitParams);
    }
    else {
        std::list<castor::log::Param> exitParams = {
            castor::log::Param("REQID", _myReqId),
            castor::log::Param("RequestType", function.substr(function.find('_')+2)),  // strip off the 'srm__' prefix
            castor::log::Param("ExitStatus", decodeStatus(status) ),
            castor::log::Param("ProcessingTime", procTime * 0.000001),
            castor::log::Param("Message", message),
            castor::log::Param("TargetSrmReqId", _targetReqId)
        };
        if ( status == srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST ) {
            // "Invalid Request"
          castor::log::write(LOG_USER_ERROR, "Invalid Request", exitParams);
        }
        else {
            // "Request Failed"
          castor::log::write(LOG_USER_ERROR, "Request Failed", exitParams);
        }
    }
}

void castor::srm::server::SrmLogger::logCnsCall(std::string cnsCall, std::string castorfile, Cns_fileid* nsId, unsigned int logLevel) {
    std::list<castor::log::Param> initParams = {
        castor::log::Param("NSFILEID", (nsId ? nsId->fileid : 0)),
        castor::log::Param("REQID", _myReqId),
        castor::log::Param("CnsCall", cnsCall),
        castor::log::Param("FileName", castorfile),
        castor::log::Param("TargetSrmReqId", _targetReqId)
    };
    // "Calling NameServer"
    castor::log::write(logLevel, "Calling NameServer", initParams);
}

void castor::srm::server::SrmLogger::logStagerCall(std::string stgCall, struct stage_options opts, Cns_fileid* nsId, unsigned int logLevel) {
    std::list<castor::log::Param> initParams = {
        castor::log::Param("NSFILEID", (nsId ? nsId->fileid : 0)),
        castor::log::Param("REQID", _myReqId),
        castor::log::Param("StagerCall", stgCall),
        castor::log::Param("StagerHost", opts.stage_host),
        castor::log::Param("SvcClass", (opts.service_class ? opts.service_class : "Not specified")),
        castor::log::Param("TargetSrmReqId", _targetReqId)
    };
    // "Calling Stager"
    castor::log::write(logLevel, "Calling stager", initParams);
}

void castor::srm::server::SrmLogger::logCnsReturn(std::string cnsCall, int status, Cns_fileid* nsId) {
    timeval tv;
    gettimeofday(&tv, NULL);
    signed64 elapsedTime = ((tv.tv_sec * 1000000) + tv.tv_usec) - ((tvStart.tv_sec * 1000000) + tvStart.tv_usec);
    
    if ( status == 0 ) { 
        std::list<castor::log::Param> initParams = {
          castor::log::Param("NSFILEID", (nsId ? nsId->fileid : 0)),
          castor::log::Param("REQID", _myReqId),
          castor::log::Param("CnsCall", cnsCall),
          castor::log::Param("ElapsedTime", elapsedTime * 0.000001)
        };
        // "NameServer returned"
        if(elapsedTime > 10000000) {
          // log to SYSTEM level only if the elapsed time so far has already exceeded 10 seconds
          castor::log::write(LOG_INFO, "NameServer returned", initParams);
        }
        else {
          castor::log::write(LOG_DEBUG, "NameServer returned", initParams);
        }
    }
    else {
        std::list<castor::log::Param> initParams = {
          castor::log::Param("NSFILEID", (nsId ? nsId->fileid : 0)),
          castor::log::Param("REQID", _myReqId),
          castor::log::Param("CnsCall", cnsCall),
          castor::log::Param("Message", sstrerror(serrno)),
          castor::log::Param("ElapsedTime", elapsedTime * 0.000001)
        };
        castor::log::write((serrno == ENOENT || serrno == EEXIST || serrno == EACCES ?
                            LOG_USER_ERROR : LOG_ERR), "NameServer returned", initParams);
    }
}

void castor::srm::server::SrmLogger::logStagerReturn(std::string stagerMethod, int status, std::string cReqId, Cns_fileid* nsId) {
    timeval tv;
    gettimeofday(&tv, NULL);
    signed64 elapsedTime = ((tv.tv_sec * 1000000) + tv.tv_usec) - ((tvStart.tv_sec * 1000000) + tvStart.tv_usec);
    
    if ( status == 0 ) { 
        std::list<castor::log::Param> initParams = {
          castor::log::Param("NSFILEID", (nsId ? nsId->fileid : 0)),
          castor::log::Param("REQID", _myReqId),
          castor::log::Param("StagerCall", stagerMethod),
          castor::log::Param("CastorReqId", cReqId),
          castor::log::Param("ElapsedTime", elapsedTime * 0.000001)
        };
        // "Stager returned"
        if(elapsedTime > 10000000) {
          // log to SYSTEM level only if the elapsed time so far has already exceeded 10 seconds
          castor::log::write(LOG_INFO, "Stager returned", initParams);
        }
        else {
          castor::log::write(LOG_DEBUG, "Stager returned", initParams);
        }
    }
    else {
        std::list<castor::log::Param> initParams = {
          castor::log::Param("NSFILEID", (nsId ? nsId->fileid : 0)),
          castor::log::Param("REQID", _myReqId),
          castor::log::Param("StagerCall", stagerMethod),
          castor::log::Param("CastorReqId", cReqId),
          castor::log::Param("Message", sstrerror(serrno)),
          castor::log::Param("ElapsedTime", elapsedTime * 0.000001)
        };
        // Only stager specific errors are considered at user-error level 
        castor::log::write((serrno >= ESTBASEOFF && serrno <= ESTBASEOFF + ESTMAXERR ?
                            LOG_USER_ERROR : LOG_ERR), "Stager returned", initParams);
    }
}

void castor::srm::server::SrmLogger::logException(castor::exception::Exception* e, castor::BaseAddress* ad, std::string function) {
    std::ostringstream msg;
    if(e != 0)
        msg << e->getMessage().str();
    else
        msg << "Caught general exception.";
    if(ad != 0) {
      try {
        castor::BaseObject::services()->rollback(ad);
      } catch(castor::exception::Exception e2) {
          msg << " Failed to rollback, request status may be inconsistent";
      }
    }
    std::list<castor::log::Param> params = {
          castor::log::Param("REQID", _myReqId),
          castor::log::Param("Exception", msg.str()),
          castor::log::Param("Function", function),
    };
    // "Exception caught"
    castor::log::write(LOG_ERR, "Exception caught", params);
}


std::string castor::srm::server::SrmLogger::getIdAsString() {
    char uuid_s[CUUID_STRING_LEN+1];
    uuid_s[CUUID_STRING_LEN]=0;
    Cuuid2string(uuid_s, CUUID_STRING_LEN+1, &_myReqId);
    return uuid_s;
}

void castor::srm::server::SrmLogger::setId(std::string newTargetReqId) {
    _targetReqId = newTargetReqId;
}
