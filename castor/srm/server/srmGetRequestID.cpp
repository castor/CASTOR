#include <cgsi_plugin.h>

#include <castor/BaseAddress.hpp>
#include <castor/BaseObject.hpp>
#include <castor/Constants.hpp>
#include <castor/Services.hpp>

#include <castor/exception/Exception.hpp>

#include "soapH.h"

#include "castor/srm/ISrmSvc.hpp"
#include "castor/srm/SrmUser.hpp"
#include "castor/srm/server/SrmUtils.hpp"
#include "castor/srm/server/SrmLogger.hpp"

#include "ResponseWriter.hpp"

int srm__srmGetRequestTokens(
        struct soap *soap,
        srm__srmGetRequestTokensRequest*        srmGetRequestIDRequest,
        srm__srmGetRequestTokensResponse_ &response) //< response parameter
{

    castor::srm::server::SrmLogger *myLogger = new castor::srm::server::SrmLogger();
    myLogger->LOGENTRY(soap);

    srm__srmGetRequestTokensResponse *resp;
    try {
        resp = soap_new_srm__srmGetRequestTokensResponse(soap, -1);
        resp->returnStatus = soap_new_srm__TReturnStatus(soap, -1);
        resp->returnStatus->explanation = soap_new_std__string(soap, -1);
        resp->arrayOfRequestTokens = NULL;

        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCORESUCCESS;
        resp->returnStatus->explanation->assign("");
    }
    catch (...) {
        std::list<castor::log::Param> params = {
            castor::log::Param("REQID", myLogger->getId()),
            castor::log::Param("Message", "Out of memory")
        };
        castor::log::write(LOG_ALERT, "Exception caught", params);
        myLogger->LOGEXIT(srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR, "Out of memory");
        delete myLogger; myLogger = 0;
        return SOAP_EOM;
    }
    response.srmGetRequestTokensResponse = resp;
     
    castor::srm::server::ResponseWriter myWriter;

    //
    //   * The point of this is to get all requests with a specific description, owned by the current user,
    //   * returning an array of request tokens.
    //   *
    //   * So first we needs to get the user.  If no description is specified, return all request tokens
    //   * owned by the current user, otherwise just the subset matching the description.  This will be
    //   * a simple literal match.
    //

    // Set up database services
    castor::srm::ISrmSvc* srmSvc;
    castor::BaseAddress ad;
    try {
        ad.setCnvSvcName("DbCnvSvc");
        ad.setCnvSvcType(castor::SVC_DBCNV);
        castor::IService *svc = castor::BaseObject::services()->service("DbSrmSvc", castor::SVC_DBSRMSVC);
        srmSvc = dynamic_cast<castor::srm::ISrmSvc*>(svc);
        if ( srmSvc == 0 ) {
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
            resp->returnStatus->explanation->assign("Unable to establish database service");
            std::list<castor::log::Param> params = {castor::log::Param("REQID", myLogger->getId())};
            castor::log::write(LOG_ERR, "Could not get Conversion Service for Database", params);
            myWriter.write(response);
            myLogger->LOGEXIT(resp->returnStatus->statusCode,  resp->returnStatus->explanation->c_str());
            delete myLogger; myLogger = 0;
            return SOAP_OK;
        }
    }
    catch (castor::exception::Exception e) {
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        resp->returnStatus->explanation->assign( e.getMessage().str());
        myLogger->LOGEXCEPTION(&e, &ad);
        myWriter.write(response);
        myLogger->LOGEXIT(resp->returnStatus->statusCode,  resp->returnStatus->explanation->c_str());
        delete myLogger; myLogger = 0;
        return SOAP_OK;
    }

    // Get information about current user and check against request owner
    castor::srm::server::SrmUtils *utils = new castor::srm::server::SrmUtils();
    std::string dn, uname, vo;
    try {
        dn    = utils->getDn(soap);
        uname = utils->getUserName(soap);
        vo    = utils->getVoName(soap);
    } catch(castor::exception::Exception &e) {
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREAUTHENTICATION_USCOREFAILURE;
        resp->returnStatus->explanation->assign ("Could not get user information");
        std::list<castor::log::Param> params = {
            castor::log::Param("REQID", myLogger->getId()),
            castor::log::Param("Details", e.getMessage().str() )
        };
        castor::log::write(LOG_ERR, "Authentication error", params);
        *resp->returnStatus->explanation += ": " + e.getMessage().str();
        delete utils;   
        myWriter.write(response);
        myLogger->LOGEXIT(resp->returnStatus->statusCode, resp->returnStatus->explanation->c_str());
        delete myLogger; myLogger = 0;
        return SOAP_OK;
    }

    delete utils; utils = 0;

    try {
        // Get the user
        castor::srm::SrmUser *thisUser = srmSvc->getSrmUser( uname, vo, dn );

        // Get the request tokens
        std::vector<std::string> tokens = srmSvc->getRequestTokens( thisUser->id(),  srmGetRequestIDRequest->userRequestDescription);
        std::list<castor::log::Param> params = {
            castor::log::Param("REQID", myLogger->getId()),
            castor::log::Param("nTokensOwned", tokens.size()),
        };
        castor::log::write(LOG_DEBUG, "Got Request tokens", params);
        delete thisUser;

        // Now loop over all results (assuming there are any...)
        if ( tokens.size() > 0 ) {
            resp->arrayOfRequestTokens = soap_new_srm__ArrayOfTRequestTokenReturn(soap, -1);
            for ( unsigned i=0; i<tokens.size(); i++ ) {
                srm__TRequestTokenReturn *thisToken = soap_new_srm__TRequestTokenReturn(soap, -1);
                thisToken->createdAtTime = NULL;
                thisToken->requestToken.assign(tokens[i]);
                resp->arrayOfRequestTokens->tokenArray.push_back(thisToken);
            }
        }
        else {
            resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST;
            resp->returnStatus->explanation->assign("No tokens match this description");
        }
    }
    catch (castor::exception::Exception x) {
        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR;
        resp->returnStatus->explanation->assign(x.getMessage().str());
        myLogger->LOGEXCEPTION(&x, &ad);
    }
    myWriter.write(response);
    myLogger->LOGEXIT(resp->returnStatus->statusCode,  resp->returnStatus->explanation->c_str());
    delete myLogger;
    return SOAP_OK;
}


