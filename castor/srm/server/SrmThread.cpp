/******************************************************************************
 *                      SrmThread.cpp
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 *
 *
 * @author Shaun De Witt
 *****************************************************************************/

// Include Files

#include "soapH.h"
#include "castor/srm/server/SrmThread.hpp"
#include <castor/log/log.hpp>
#include <castor/Constants.hpp>
#include <castor/BaseObject.hpp>
#include <castor/Services.hpp>
#include <castor/exception/Exception.hpp>
#include <castor/exception/Exception.hpp>

#include <iostream>
#include <errno.h>


//------------------------------------------------------------------------------
// run
//------------------------------------------------------------------------------
void castor::srm::server::SrmThread::run(void* param) {

  // We know it's a soap structure
  struct soap* soap = (struct soap*) param;

  // get the db service when we're root, before any other operation
  castor::BaseObject::services()->service("DbCnvSvc", castor::SVC_DBCNV);
  
  if ( soap_serve(soap) != SOAP_OK ) {
      // we got an error that is not a timeout
      char errstr[1024];
      soap_sprint_fault(soap, errstr, sizeof(errstr));
      // "SOAP Error". We assume this is due to the client, hence the level
      std::list<castor::log::Param> soapP = {
          castor::log::Param("ErrorDetails", errstr)
      };
      castor::log::write(LOG_USER_ERROR, "SOAP Error", soapP);
  }
  
  // cleanup
  soap_destroy(soap);
  soap_end(soap);
  soap_done(soap);
  soap_free(soap);
}
