#include <castor/log/log.hpp>

#include "soapH.h"

#include "ResponseWriter.hpp"

int srm__srmReassignToUser(
        struct soap *soap,
        srm__srmReassignToUserRequest*      srmReassignToUserRequest,
        srm__srmReassignToUserResponse_     &response) //< response parameter
{

    return SOAP_FAULT;
    /*
    srm__srmReassignToUserResponse *resp;
    try {
        resp = soap_new_srm__srmReassignToUserResponse(soap, -1);
        resp->returnStatus = soap_new_srm__TReturnStatus(soap, -1);

        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCORENOT_USCORESUPPORTED;
        resp->returnStatus->explanation = NULL;
    }
    catch (...) {
        return SOAP_EOM;
    }
    response.srmReassignToUserResponse = resp;

    return SOAP_OK;
    */

}
