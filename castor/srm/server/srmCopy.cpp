#include <castor/log/log.hpp>

#include "soapH.h"
#include "castor/srm/server/SrmLogger.hpp"
#include "castor/srm/server/ResponseWriter.hpp"

int srm__srmCopy(
        struct soap *soap,
        srm__srmCopyRequest*,
        srm__srmCopyResponse_& response) //< response parameter
{
    castor::srm::server::SrmLogger *myLogger = new castor::srm::server::SrmLogger();
    myLogger->LOGENTRY(soap);
    srm__srmCopyResponse *resp;
    try {
        resp = soap_new_srm__srmCopyResponse(soap, -1);
        resp->returnStatus = soap_new_srm__TReturnStatus(soap, -1);
        resp->returnStatus->explanation = soap_new_std__string(soap, -1);

        resp->returnStatus->statusCode = srm__TStatusCode__SRM_USCORENOT_USCORESUPPORTED;
        resp->returnStatus->explanation->assign("This SRM does not support copy functionality");
    }
    catch(...) {
        std::list<castor::log::Param> params = {
            castor::log::Param("REQID", myLogger->getId()),
            castor::log::Param("Message", "Out of memory")
        };
        castor::log::write(LOG_ALERT, "Exception caught", params);
        myLogger->LOGEXIT(srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR, "Out of memory");
        delete myLogger;
        return SOAP_EOM;
    }
    response.srmCopyResponse = resp;
    myLogger->LOGEXIT(resp->returnStatus->statusCode, "Not supported");
    delete myLogger;
    castor::srm::server::ResponseWriter myWriter;
    myWriter.write(response);
    return SOAP_OK;
}


