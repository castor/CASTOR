#ifndef SRM_SURL_HPP
#define SRM_SURL_HPP
#include <string>
#include <castor/exception/Exception.hpp>
#include <soapH.h>

class srm_TUserID;

namespace castor {
 namespace srm {
    class SURL {
        public:
            SURL(std::string& surl, std::string* &user, struct soap* &soap)
                throw (castor::exception::Exception);

            SURL (std::string surl)
                throw (castor::exception::Exception);

            const std::string getProtocol() {return _protocol;}
            const std::string getHost() {return _host;}
            const std::string getPort() {return _port;}
            const std::string getEndPoint() {return _endPoint;}
            const std::string getFileName() {return _siteFileName;}
            const std::string surl() {return _surl;}
            const std::string prefix();
            const std::string fullSURL();

        private:
            void parse(std::string surl);
            bool isRelativePath();
            void makeAbsolutePath( std::string * &userName, struct soap* &soap )
                throw (castor::exception::Exception);
            std::string _protocol;
            std::string _host;
            std::string _port;
            std::string _endPoint;
            std::string _siteFileName;
            std::string _surl;
            bool _hasPort;
            bool _hasEndpoint;
    };
} }  // end of namespace castor/srm

#endif
