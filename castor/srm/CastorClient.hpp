/******************************************************************************
 *                      CastorClient.hpp
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * A C++ based Castor client, with synch and asynch capabilities.
 *
 * @author castordev@cern.ch
 *****************************************************************************/

#ifndef SRM_CASTORCLIENT_HPP
#define SRM_CASTORCLIENT_HPP 1

// Include Files
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/poll.h>
#include <sys/times.h>
#include <string>
#include <map>

#include "h/Cuuid.h"
#include "h/Cns_api.h"
#include "castor/server/IThread.hpp"
#include "castor/server/Mutex.hpp"
#include "castor/server/DynamicThreadPool.hpp"
#include "castor/io/ServerSocket.hpp"
#include "castor/exception/Exception.hpp"
#include "castor/stager/FileRequest.hpp"
#include "castor/rh/IOResponse.hpp"

namespace castor {
 namespace srm {

  /// Convenience typedef
  typedef std::map<std::string, Cns_fileid> FileIdMap;

  /**
   * An asynchronous Castor client, running as a
   * producer thread for a DynamicThreadPool
   */
  class CastorClient : public castor::server::IThread {

  public:

    /**
     * Constructor
     * @param callbackPort the port opened for all callbacks
     * from the stager
     */
    CastorClient(unsigned int callbackPort) throw();

    /**
     * destructor
     */
    virtual ~CastorClient() throw();

    /**
     * Asynchronously sends a file-oriented request to a Castor instance
     * @param srmReqId the UUID of this request to be used for
     * matching the response later on
     * @param req the Castor FileRequest
     * @param rhHost the Request Handler host where to send the request
     * @param rhPort the Request Handler port
     * @param fileIds Castor NS fileids for logging purposes
     * @exception in case of an error
     */
    void asynchSendRequest(const Cuuid_t& srmReqId, 
      castor::stager::FileRequest& req,
      const std::string& rhHost, const unsigned int rhPort, 
      castor::srm::FileIdMap& fileIds)
      throw(castor::exception::Exception);
    
    /**
     * Synchronously sends a request to a Castor instance
     * @param srmReqId the UUID of this request for logging purposes
     * @param req the Castor request. The requestId will be stored in it.
     * @param rhHost the Request Handler host where to send the request
     * @param rhPort the Request Handler port
     * @param fileIds Castor NS fileids for logging purposes
     * @return a vector of Response objects contanining the responses
     * @exception in case of an error
     */
    static std::vector<castor::rh::Response*> sendRequest(
      const Cuuid_t& srmReqId, castor::stager::Request& req,
      const std::string& rhHost, const unsigned int rhPort,
      castor::srm::FileIdMap& fileIds)
      throw(castor::exception::Exception);
      
    /**
     * Initializes the thread by creating the listening socket
     * @exception when bind fails
     */
    virtual void init()
      throw (castor::exception::Exception);
    
    /**
     * Listen to all responses from Castor and dispatches them
     * for later handling. The logic is based on a multi-poll call
     * (cf. castor::client::BaseClient).
     * @param param a pointer to the DynamicThreadPool instance
     * to which to dispatch the received response object
     * @exception No exception is thrown by this method
     */
    virtual void run(void* param)
      throw (castor::exception::Exception);

    /**
     * Stops the listener
     */
    virtual void stop()
      throw (castor::exception::Exception) {
      m_stopped = true;
    };
    

  private:

    /// The callback port used to listen for the responses
    unsigned int m_callbackPort;
    
    /// The callback socket
    castor::io::ServerSocket* m_callbackSocket;

    /// The set of file descriptors to wait on
    struct pollfd m_fds[1024];

    /// The number of pollfd structures in the m_fds array
    nfds_t m_nfds;

    /// A vector to hold the list of accepted connections
    std::vector<castor::io::ServerSocket*> m_connected;

    /// Flag to say when we have to terminate
    bool m_stopped;
  };

} } /* end of namespace castor/srm */

#endif // SRM_CASTORCLIENT_HPP
