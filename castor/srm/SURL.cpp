#include <getconfent.h>
#include <Castor_limits.h>
#include <castor/exception/Exception.hpp>
#include "castor/srm/SrmConstants.hpp"
#include "SURL.hpp"

castor::srm::SURL::SURL( std::string& surl, std::string* &userName, struct soap* &soap )
throw (castor::exception::Exception) {
    try {
        _surl = surl;
        _hasPort = false;
        _hasEndpoint = false;
        parse(surl);

        if (_siteFileName.empty()) {
            castor::exception::Exception x(ENOENT);
            x.getMessage() << "The SURL '" << surl << "' could not be parsed";
            throw x;
        }

        if ( isRelativePath() ) {
            makeAbsolutePath(userName, soap);
        }
    }
    catch (castor::exception::Exception x) {
        throw x;
    }
}

castor::srm::SURL::SURL( std::string surl ) throw (castor::exception::Exception) {
    try {
        _surl = surl;
        _hasPort = false;
        _hasEndpoint = false;
        parse(surl);

        if (_siteFileName.empty()) {
            castor::exception::Exception x(ENOENT);
            x.getMessage() << "The SURL '" << surl << "' could not be parsed";
            throw x;
        }

        if ( isRelativePath() ) {
            castor::exception::Exception x;
            x.getMessage() << "Relative path supplied when absolute was expected.";
            throw x;
        }
    }
    catch (castor::exception::Exception x) {
        throw x;
    }
}

bool castor::srm::SURL::isRelativePath() {
    bool isRelative = false;
    //if ( _siteFileName.find("/castor") != 0 ) isRelative = true;
    if ( _siteFileName.find("/") != 0 ) isRelative = true;
    return isRelative;
}

void castor::srm::SURL::makeAbsolutePath(std::string * &userName, struct soap*&)
throw (castor::exception::Exception) {
    std::string domain = getconfent("SRM", "DOMAIN_NAME", 0);

    // TODO - need to add check for VOMS - stubbed for now
    bool voSupplied = false;
    if ( voSupplied ) {
    }
    else {
        if ( 0 == userName ) {
            castor::exception::Exception x;
            x.getMessage() << "No VOMS or userID supplied with relative path";
            throw x;
        }
        std::string userDir = "/user/";
        std::string initial = userName->substr(0, 1);
        userDir.append(initial);
        userDir.append("/");
        userDir.append(userName->data());
        userDir.append("/");

        _siteFileName = "/castor/" + domain + userDir + _siteFileName;
    }
}

void castor::srm::SURL::parse(std::string surl) {
    // Set the protocol
    if (surl.find("?SFN=") != std::string::npos) {
        _endPoint = surl.substr(0, surl.find("?SFN="));
    }
    else {
        int pos = surl.find("://")+3;
        pos = surl.find("/", pos);
        _endPoint = surl.substr(0, pos);
    }

    std::string surlCopy(surl);

    if ( surlCopy.find("://") != std::string::npos ) {
        _protocol = surlCopy.substr(0, surl.find("://"));
        surlCopy.erase(0, _protocol.length() + 3);

        // Now in position to get host and port
        bool _hasPort = false;
        if ( surlCopy.find(":") != std::string::npos ) {
            if ( surlCopy.find(":") < surlCopy.find("/") ) {
                _hasPort = true;
            }
        }
        if (_hasPort) {
            _host = surlCopy.substr(0, surlCopy.find(":"));
            surlCopy.erase(0, _host.length() + 1);
            _port = surlCopy.substr(0, surlCopy.find("/"));
            surlCopy.erase(0, _port.length());
        }
        else {
            _host = surlCopy.substr(0, surlCopy.find("/"));
            surlCopy.erase(0, _host.length());
            _port = "8443";
        }

        // The original std::string should now be at the start of the actual
        // SURL.  We need to do two things - 
        // We need tyo check whether we are using a format SFN= 
        // and also see whether we are using a relative path or an absolute.
        if ( surlCopy.find("?SFN") != std::string::npos ) {
            std::string endPoint = surlCopy.substr(0, surlCopy.find("?SFN"));
            surlCopy.erase(0, endPoint.length() + 5);
        }

        // Now get the filename
        _siteFileName = surlCopy;
        if ( _siteFileName[0] == _siteFileName[1] && _siteFileName[0] == '/' ) {
            _siteFileName.erase(0, 1);
        }

        // Validate this SURL is OK
        // While this is a good idea for PUT and GET it is less helpfull for other applications
        // so I remove it.
        /*
        if (_siteFileName.length() > CA_MAXPATHLEN ) {
            castor::exception::Exception x(ENAMETOOLONG);
            x.getMessage() << sstrerror(ENAMETOOLONG);
            throw x;
        }
        char *ptr;
        char *lst;
        char *sptr = const_cast<char*>(surl.c_str());
        for ( ptr = strtok_r(sptr, "/", &lst); ptr!=NULL; ptr=strtok_r(NULL, "/", &lst) ) {
            if ( strlen(ptr) > CA_MAXNAMELEN ) {
                castor::exception::Exception x(ENAMETOOLONG);
                x.getMessage() << sstrerror(ENAMETOOLONG);
                throw x;
            }
        }
        */
    }
}

const std::string castor::srm::SURL::prefix() {
    std::string prefix(_surl);
    prefix.erase(_surl.length()-_siteFileName.length(), _surl.length());
    return prefix;
}

const std::string castor::srm::SURL::fullSURL() {
    //std::string fullSURL(_protocol);
    std::string fullSURL(_endPoint);
    /*
    fullSURL.append("://");
    fullSURL.append(_host);
    if ( _hasPort ) {
        fullSURL.append(":");
        fullSURL.append(_port);
    }
    if (_hasEndpoint) {
        fullSURL.append(_endPoint);
        fullSURL.append("?SFN=");
    }
    */
    fullSURL.append(_siteFileName);

    return fullSURL;
}
