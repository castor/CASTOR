#include <algorithm>
#include <ctype.h>
#include <sstream>
#include <cstring>

#include "h/getconfent.h"
#include "castor/exception/Exception.hpp"
#include "castor/srm/SrmUtils.hpp"

//------------------------------------------------------------------------------
// buildTUrl
//------------------------------------------------------------------------------
std::string castor::srm::utils::buildTURL(const std::string protocol,
                                  const std::string host,
                                  const int port,
                                  const std::string path,
                                  const std::string svcClass)
  throw(castor::exception::Exception) {
  // get the TURL template for the protocol. 'Well-known' protocols have their hardcoded template:
  std::string turl;
  if (protocol == "gsiftp") {
    turl = "gsiftp://<host>:<port>/<path>";
  }
  else if (protocol == "rfiointernal") {
    turl = "rfio://<host>:<port>/<path>";
  }
  else if (protocol == "rfio") {
    turl = "rfio://<host>:<port>/<path>?svcClass=<svcClass>";
  }
  else if (protocol == "xroot") {
    turl = "root://<host>/<path>?svcClass=<svcClass>";
  }
  else {
    // try to get a template from the configuration file
    char* p = getconfent("SRM", protocol.c_str(), 0);
    if (p == NULL) {
      castor::exception::Exception e;
      e.getMessage() << "Bad configuration detected. No entry found for protocol "
                     << protocol;
      throw e;
    }
    turl = p;
  }
  // fill in the template
  std::stringstream s_port;
  s_port << port;
  size_t pos;
  if ((pos = turl.find("<host>")) != std::string::npos)
    turl.replace(turl.find("<host>"), std::string("<host>").length(), host);
  if ((pos = turl.find("<path>")) != std::string::npos)
    turl.replace(turl.find("<path>"), std::string("<path>").length(), path);
  if ((pos = turl.find("<port>")) != std::string::npos)
    turl.replace(turl.find("<port>"), std::string("<port>").length(), s_port.str());
  if ((pos = turl.find("<svcClass>")) != std::string::npos)
    turl.replace(turl.find("<svcClass>"), std::string("<svcClass>").length(), svcClass);
  return turl;
}
