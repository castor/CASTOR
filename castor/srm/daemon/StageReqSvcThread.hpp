/******************************************************************************
 *                      StageReqSvcThread.hpp
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * This thread is handling incoming requests (gets and puts).
 * It mainly calls CASTOR and hands over the post processing to
 * the RecallPoller and TurlHandler thread
 *
 * @author Giuseppe Lo Presti
 *****************************************************************************/

#ifndef SRM_DAEMON_STAGEREQSVCTHREAD_HPP
#define SRM_DAEMON_STAGEREQSVCTHREAD_HPP 1

#include "castor/srm/daemon/SrmdUtils.hpp"
#include "castor/server/SelectProcessThread.hpp"
#include "castor/exception/Exception.hpp"
#include "castor/srm/CastorClient.hpp"
#include "castor/srm/StageRequest.hpp"
#include "castor/srm/SubRequest.hpp"
#include "Cuuid.h"
#include <map>
#include <vector>
#include <string>

#define BUFSIZE 1024

namespace castor {
  // Forward declarations
  class IObject;
}

namespace castor {
 namespace srm {

  namespace daemon { 

    // Forward declarations
    class ISrmDaemonSvc;
  
    /**
     * A thread to process Put/Get request and submit them to the stager.
     */
    class StageReqSvcThread : public castor::server::SelectProcessThread {

    public:

      /**
       * Initializes the db access.
       */
      StageReqSvcThread(srm::CastorClient&);

      /**
       * Standard destructor
       */
      virtual ~StageReqSvcThread() throw() {};

      /**
       * Select part of the service: gives a StageRequest if any.
       */
      virtual castor::IObject* select() throw();

      /**
       * Process part of the service: expects a StageRequest.
       */
      virtual void process(castor::IObject* param) throw();

    private:

      /**
       * Handles a PrepareToGet request. This includes calling prepareToGet
       * on the stager and calling asynchronously Get to construct
       * the URL when needed.
       * @param req the request to handle
       * @param srmdSvc the SrmDaemon service to use
       * @param myId the uuid of the request, as a Cuuid_t struct
       */
      void handlePrepareToGet(srm::StageRequest* req,
                              castor::srm::daemon::ISrmDaemonSvc* srmdSvc,
                              const Cuuid_t &myId)
        throw();


      /**
       * Handles a PrepareToPut request. This includes calling prepareToPut
       * on the stager and calling asynchronously Put to construct
       * the URL when needed.
       * @param req the request to handle
       * @param srmdSvc the SrmDaemon service to use
       * @param myId the uuid of the request, as a Cuuid_t struct
       */
      void handlePrepareToPut(srm::StageRequest* req,
                              castor::srm::daemon::ISrmDaemonSvc* srmdSvc,
                              const Cuuid_t &myId)
        throw();


      /**
       * Tries to find out which service classes to use for the subrequests of a given request.
       * The best service class is the first one listed in the mapping file that
       * already contains on online copy of the file. If no service class is given
       * for a subrequest, that means that the default should be used.
       * @param req the request to handle
       * @param myId the uuid of the request, as a Cuuid_t struct, for logging purposes
       * @return a map where each subrequest is associated to the best service class
       * to use. Note that the caller is responsible for deleting the map.
       * In case of error, a null pointer is returned. Error logging is performed
       * inside the method.
       */
      castor::srm::daemon::utils::SvcClassMap*
      gatherSvcClass(srm::StageRequest* req, const Cuuid_t &myId) throw();


    private:

      // Utilities

      /**
       * binary functor checking the service class of a SubRequest
       * @param sr2sc the map from subRequests to service class names
       * @param svcClass the service class name to check
       */
      struct hasSvcClass {
        hasSvcClass(srm::daemon::utils::SvcClassMap* sr2sc, std::string& svcClass):
          m_sr2sc(sr2sc), m_svcClass(svcClass) {};
        bool operator() (srm::SubRequest* subreq) const throw()
          { return m_sr2sc->operator[](subreq).svcClass == m_svcClass; }
        castor::srm::daemon::utils::SvcClassMap* m_sr2sc;
        std::string& m_svcClass;
      };

      /**
       * functor taking care of failing a subrequest in case the file already
       * exists in the nameserver. Used by handlePrepareToPut in case of
       * OVERWRITE_NEVER option. The implementation inside a functor allows
       * to ease the removal of the failed subrequests from the request using
       * the remove_if algorithm.
       */
      struct failSubReqWhenFileExists {
        failSubReqWhenFileExists(srm::daemon::ISrmDaemonSvc* srmdSvc, const Cuuid_t &myId):
          m_srmdSvc(srmdSvc), m_myId(myId) {};
        bool operator() (srm::SubRequest* subreq) const throw();
        castor::srm::daemon::ISrmDaemonSvc* m_srmdSvc;
        const Cuuid_t &m_myId;
      };

      /**
       * functor taking care of failing a subrequest in case of errors on the nameserver.
       * Used by handlePrepareToPut before issuing a Put to get the TURL.
       * As for the previous one, the implementation inside a functor allows
       * to ease the removal of the failed subrequests from the request using
       * the remove_if algorithm.
       */
      struct failSubReqWhenNsError {
        failSubReqWhenNsError(srm::daemon::ISrmDaemonSvc* srmdSvc, const Cuuid_t &myId):
          m_srmdSvc(srmdSvc), m_myId(myId) {};
        bool operator() (srm::SubRequest* subreq) const throw();
        castor::srm::daemon::ISrmDaemonSvc* m_srmdSvc;
        const Cuuid_t &m_myId;
      };

      /**
       * helper method to cleanup the memory of a given subRequest
       * @parem subreq the subRequest to cleanup
       */
      inline static void deleteSubRequest(srm::SubRequest* req) throw();

      /**
       * helper method to cleanup the memory of a given request
       * @parem req the request to cleanup
       */
      inline static void deleteRequest(srm::StageRequest* req) throw();

      /**
       * helper method to log request failures.
       * This will issue one log message per subrequest
       * @param req the request that failed
       * @param message a string explaining how we failed
       * @param myId the uuid of the request, as a Cuuid_t struct
       */
      inline static void logRequestFailure(srm::StageRequest* req,
                                           const std::string message,
                                           const Cuuid_t &myId) throw();

    private:

      /// CastorClient to use
      castor::srm::CastorClient& m_castorClient;

    };

  } // end of namespace daemon

} } /* end of namespace castor/srm */


#endif // SRM_DAEMON_STAGEREQSVCTHREAD_HPP
