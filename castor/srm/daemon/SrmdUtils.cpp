/******************************************************************************
 *                      SrmdUtils.cpp
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * A static class with utilities for the srmDaemon
 *
 * @author Giuseppe Lo Presti
 *****************************************************************************/

#include <sstream>
#include <algorithm>
#include <cstring>
#include <errno.h>
#include <string.h>

#include <stager_client_api_common.hpp>
#include <getconfent.h>

#include "castor/log/log.hpp"
#include "castor/srm/SrmUtils.hpp"
#include "castor/srm/daemon/SrmdUtils.hpp"
#include "castor/srm/daemon/ISrmDaemonSvc.hpp"
#include "castor/srm/daemon/SupportedProtocols.hpp"
#include "castor/srm/SrmConstants.hpp"
#include "castor/srm/StageRequest.hpp"
#include "castor/srm/SrmUser.hpp"
#include "castor/srm/SubRequest.hpp"
#include "castor/srm/UserFile.hpp"
#include "castor/exception/Exception.hpp"
#include "castor/log/log.hpp"
#include "castor/stager/StageGetRequest.hpp"
#include "castor/stager/SubRequest.hpp"

//------------------------------------------------------------------------------
// buildTUrl
//------------------------------------------------------------------------------
void castor::srm::daemon::utils::buildTUrl(castor::srm::SubRequest* subreq,
                                   const std::string host,
                                   const int port,
                                   const std::string path,
                                   const std::string svcClass,
                                   castor::srm::daemon::ISrmDaemonSvc* srmdSvc,
                                   const Cuuid_t &myId,
                                   struct Cns_fileid* nsId)
  throw() {
  castor::srm::StageRequest *req = subreq->request();
  try {
    // build the TUrl
    subreq->setTurl(castor::srm::utils::buildTURL
                    (req->protocol(), host, port, path, svcClass));
    // update the DB
    srmdSvc->setTurl(subreq, myId);
    // "TURL available" message
    std::list<castor::log::Param> snafu = {
      castor::log::Param("NSFILEID", nsId->fileid),
      castor::log::Param("REQID", myId),
      castor::log::Param("TURL", subreq->turl()),
      castor::log::Param("Protocol", req->protocol()),
      castor::log::Param("FileName", subreq->castorFileName()),
      castor::log::Param("RequestToken", req->id()),
      castor::log::Param("Type", castor::srm::RequestTypeStrings[req->requestType()]),
      castor::log::Param("SvcClass", svcClass),
      castor::log::Param("DN", req->srmUser()->dn()),
      castor::log::Param("VO", req->srmUser()->vo()),
      castor::log::Param("uid", req->euid()),
      castor::log::Param("gid", req->egid()),
      castor::log::Param("ElapsedTime", time(NULL)-req->creationTime())
    };
    castor::log::write(LOG_INFO, "TURL available", snafu);
  } catch (castor::exception::Exception e) {
    // fail the subrequest
    srmdSvc->failSubRequest(subreq, SEINTERNAL, e.getMessage().str(), myId);
    // "Unable to build TURL" message
    std::list<castor::log::Param> snafu = {
      castor::log::Param("NSFILEID", nsId->fileid),
      castor::log::Param("REQID", myId),
      castor::log::Param("ErrorMessage", e.getMessage().str()),
      castor::log::Param("Protocol", req->protocol()),
      castor::log::Param("FileName", subreq->castorFileName()),
      castor::log::Param("RequestToken", req->id()),
      castor::log::Param("Type", castor::srm::RequestTypeStrings[req->requestType()]),
      castor::log::Param("SvcClass", svcClass),
      castor::log::Param("DN", req->srmUser()->dn()),
      castor::log::Param("VO", req->srmUser()->vo()),
      castor::log::Param("uid", req->euid()),
      castor::log::Param("gid", req->egid()),
      castor::log::Param("ElapsedTime", time(NULL)-req->creationTime())
    };
    castor::log::write(LOG_INFO, "Unable to build TURL", snafu);
  }
}


//------------------------------------------------------------------------------
// handleStagedFile
//------------------------------------------------------------------------------
void castor::srm::daemon::utils::handleStagedFile
(castor::srm::SubRequest* subreq,
 const std::string& svcClass,
 const std::string& diskServer,
 castor::srm::daemon::ISrmDaemonSvc* srmdSvc,
 const Cuuid_t &myId,
 std::list<castor::srm::SubRequest*>& getSRs)
  throw(){
  // In case of a GET (not BringOnline), we need a Turl
  if (!subreq) {
    // This is unacceptable, log an error and give up
    std::list<castor::log::Param> p = {
      castor::log::Param("REQID", myId),
      castor::log::Param("SvcClass", svcClass),
      castor::log::Param("DiskServer", diskServer),
      castor::log::Param("Message", "handleStagedFile: Subrequest is NULL")
    };
    // "Exception caught"
    castor::log::write(LOG_ERR, "Exception caught", p);
    return;
  }
  castor::srm::StageRequest* req = subreq->request();
  if (req->requestType() == castor::srm::REQUESTTYPE_GET) {
    if (req->protocol() == SRM_PROTOCOL_XROOT) {
      // XROOT does not need a GET to Castor, we simply build the TURL
      // build a nsId struct
      struct Cns_fileid nsId;
      nsId.fileid = subreq->userFile()->fileId();
      strncpy(nsId.server, subreq->userFile()->nsHost().c_str(), sizeof(nsId.server));
      // build the TUrl and update DB
      castor::srm::daemon::utils::buildTUrl
        (subreq, req->rhHost(), 0, subreq->castorFileName(),   // the port is irrelevant
         svcClass, srmdSvc, myId, &nsId);
    }
    else {
      // We need to issue a GET to Castor:
      // however, we will group them, so we just push the subrequest to
      // the lists of ones that need a get
      getSRs.push_back(subreq);
    }
  } else {
    // in case of BringOnline, we only mark the SubRequest as succeeded
    srmdSvc->succeedSubRequest(subreq, myId);
    // "Processing complete" message
    std::list<castor::log::Param> snafu = {
      castor::log::Param("NSFILEID", subreq->userFile()->fileId()),
      castor::log::Param("REQID", myId),
      castor::log::Param("FileName", subreq->castorFileName()),
      castor::log::Param("RequestToken", req->id()),
      castor::log::Param("Type", castor::srm::RequestTypeStrings[req->requestType()]),
      castor::log::Param("SvcClass", svcClass),
      castor::log::Param("DN", req->srmUser()->dn()),
      castor::log::Param("VO", req->srmUser()->vo()),
      castor::log::Param("uid", req->euid()),
      castor::log::Param("gid", req->egid()),
      castor::log::Param("ElapsedTime", time(NULL)-req->creationTime())
    };
    castor::log::write(LOG_INFO, "Processing complete", snafu);
  }
}


//------------------------------------------------------------------------------
// sendGetRequest
//------------------------------------------------------------------------------
void castor::srm::daemon::utils::sendGetRequest
(const std::list<castor::srm::SubRequest*>::iterator& subreqStart,
 const std::list<castor::srm::SubRequest*>::iterator& subreqEnd,
 const std::string& svcClass,
 castor::srm::daemon::ISrmDaemonSvc* srmdSvc,
 const Cuuid_t &myId,
 castor::srm::CastorClient& castorClient)
  throw(){
  // Build the Get request
  castor::stager::StageGetRequest gReq;
  gReq.setSvcClassName(svcClass);
  gReq.setEuid((*subreqStart)->request()->euid());
  gReq.setEgid((*subreqStart)->request()->egid());
  std::vector<u_signed64> idsVector;
  for (std::list<castor::srm::SubRequest*>::const_iterator it = subreqStart;
       it != subreqEnd;
       it++) {
    castor::stager::SubRequest *subreq = new castor::stager::SubRequest();
    subreq->setProtocol((*subreqStart)->request()->protocol());
    subreq->setFileName((*it)->castorFileName());
    subreq->setXsize((*it)->reservedSize());
    subreq->setModeBits(0644);
    gReq.addSubRequests(subreq);
    subreq->setRequest(&gReq);
    idsVector.push_back((*it)->id());
  }
  // fill in a request UUID to be passed to Castor
  gReq.setReqId(srmdSvc->prepareCastorReq(idsVector, svcClass, myId));
  // Send the get request
  try {
    // Note that the call is asynchronous, and the handling of the
    // reponses is done in the TurlHandler thread
    FileIdMap fileIds = buildFileIdMap(subreqStart, subreqEnd);
    castorClient.asynchSendRequest(myId, gReq,
                                   (*subreqStart)->request()->rhHost(),
                                   (*subreqStart)->request()->rhPort(),
                                   fileIds);
  } catch (castor::exception::Exception e) {
    // we need to fail the subrequests as they will not be reconsidered in
    // a future loop (we use the GetNext version of filequery)
    for (std::list<castor::srm::SubRequest*>::const_iterator it = subreqStart;
         it != subreqEnd;
         it++) {
      srmdSvc->failSubRequest(*it, e.code(), e.getMessage().str(), myId);
      // "Subrequest Failed"
      std::list<castor::log::Param> snafu = {
        castor::log::Param("NSFILEID", (*it)->userFile()->fileId()),
        castor::log::Param("REQID", myId),
        castor::log::Param("RequestToken", (*it)->request()->id()),
        castor::log::Param("SvcClass", svcClass),
        castor::log::Param("uid", (*it)->request()->euid()),
        castor::log::Param("gid", (*it)->request()->egid()),
        castor::log::Param("ElapsedTime", time(NULL)-(*it)->request()->creationTime()),
        castor::log::Param("Error", e.code()),
        castor::log::Param("Reason", e.getMessage().str())
      };
      castor::log::write(LOG_ERR, "Subrequest Failed", snafu);
    }
  }
}


//------------------------------------------------------------------------------
// buildFileIdMap
//------------------------------------------------------------------------------
castor::srm::FileIdMap castor::srm::daemon::utils::buildFileIdMap (castor::srm::StageRequest* req)
  throw() {
  return buildFileIdMap(req->subRequests().begin(), req->subRequests().end());
}


//------------------------------------------------------------------------------
// listSvcClassForVo
//------------------------------------------------------------------------------
void
castor::srm::daemon::utils::listSvcClassForVo(const std::string& vo,
                                      char**& svcClassList,
                                      int& nSvcClasses,
                                      const Cuuid_t &myId)
  throw() {
  // Get the mapping file to be used
  std::string mapFile = castor::srm::SrmConstants::getInstance()->storageMappingFile();
  // extract vo, and make it upper case
  std::string myVO = vo;
  transform(myVO.begin(), myVO.end(), myVO.begin(), toupper);
  int rc = getconfent_multi_fromfile
    (const_cast<char*>(mapFile.c_str()),
     const_cast<char*>(myVO.c_str()), "default", 10, &svcClassList, &nSvcClasses);
  // handle errors
  if (rc != 0 || nSvcClasses == 0) {
    // log the problem first
    // "Storagemap file misconfigured"
    std::list<castor::log::Param> snafu = {
      castor::log::Param("REQID", myId),
      castor::log::Param("Message", "Can not get default service classes from storagemap file."),
      castor::log::Param("MapFile", mapFile),
      castor::log::Param("VO", vo),
    };
    castor::log::write(LOG_ERR, "Storagemap file misconfigured", snafu);
    // be sure we return 0
    nSvcClasses = 0;
  }
}


//------------------------------------------------------------------------------
// getDefaultSvcClass
//------------------------------------------------------------------------------
std::string
castor::srm::daemon::utils::getDefaultSvcClass(const std::string vo,
                                       const Cuuid_t &myId)
  throw() {
  // local declarations
  char** svcClassList;
  int nSvcClasses;
  std::vector<std::string> res;
  // get the list
  listSvcClassForVo(vo, svcClassList, nSvcClasses, myId);
  // return first value
  return svcClassList[0];
}


//------------------------------------------------------------------------------
// getSvcClassList
//------------------------------------------------------------------------------
std::vector<std::string>
castor::srm::daemon::utils::getSvcClassList(const std::string vo,
                                    const Cuuid_t &myId)
  throw() {
  // local declarations
  char** svcClassList;
  int nSvcClasses;
  std::vector<std::string> res;
  // get the list
  listSvcClassForVo(vo, svcClassList, nSvcClasses, myId);
  // build C++ output
  for (int i = 0; i < nSvcClasses; i++) {
    res.push_back(svcClassList[i]);
    free(svcClassList[i]);
  }
  free(svcClassList);
  // return
  return res;
}
