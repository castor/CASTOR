/******************************************************************************
 *                      StageReqSvcThread.cpp
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * This thread is handling incoming requests (gets and puts).
 * It mainly calls CASTOR and hands over the post processing to
 * the RecallPoller and Turlhandler thread
 *
 * @author Giuseppe Lo Presti
 *****************************************************************************/

#include <algorithm>
#include <list>

#include "castor/Services.hpp"
#include "castor/IService.hpp"
#include "castor/Constants.hpp"
#include "castor/log/log.hpp"
#include "castor/exception/Exception.hpp"
#include "castor/exception/Exception.hpp"
#include "castor/rh/Response.hpp"
#include "castor/rh/FileResponse.hpp"
#include "castor/rh/FileQryResponse.hpp"
#include "castor/stager/StagePrepareToGetRequest.hpp"
#include "castor/stager/StagePrepareToPutRequest.hpp"
#include "castor/stager/StageFileQueryRequest.hpp"
#include "castor/stager/QueryParameter.hpp"
#include "castor/stager/StagePutRequest.hpp"
#include "castor/stager/SubRequest.hpp"
#include "castor/server/NotifierThread.hpp"
#include <Cns_api.h>
#include <getconfent.h>
#include <serrno.h>

#include "castor/srm/daemon/StageReqSvcThread.hpp"
#include "castor/srm/daemon/ISrmDaemonSvc.hpp"
#include "castor/srm/daemon/SupportedProtocols.hpp"
#include "castor/srm/StageRequest.hpp"
#include "castor/srm/SubRequest.hpp"
#include "castor/srm/SrmUser.hpp"
#include "castor/srm/UserFile.hpp"
#include "castor/srm/SrmUtils.hpp"
#include "castor/srm/daemon/SrmdUtils.hpp"
#include "castor/srm/CastorClient.hpp"
#include "castor/srm/SrmConstants.hpp"

namespace castor {
 namespace srm {
  enum overwriteOptionValues {
    OVERWRITE_NEVER = 0,
    OVERWRITE_ALWAYS = 1
  };
 }
}

//------------------------------------------------------------------------------
// constructor
//------------------------------------------------------------------------------
castor::srm::daemon::StageReqSvcThread::StageReqSvcThread
(castor::srm::CastorClient& castorClient) :
  SelectProcessThread(),
  m_castorClient(castorClient) {}


//------------------------------------------------------------------------------
// select
//------------------------------------------------------------------------------
castor::IObject* castor::srm::daemon::StageReqSvcThread::select() throw() {
  // Get srm service; we know (cf. SrmDaemon's constructor) we'll get a valid pointer
  castor::IService* s = svcs()->service("OraSrmDaemonSvc", castor::SVC_ORASRMDAEMONSVC);
  castor::srm::daemon::ISrmDaemonSvc* srmdSvc = dynamic_cast<castor::srm::daemon::ISrmDaemonSvc*>(s);
  // Get any new request to do
  return srmdSvc->stageRequestToDo();
}


//------------------------------------------------------------------------------
// process
//------------------------------------------------------------------------------
void castor::srm::daemon::StageReqSvcThread::process(castor::IObject* param) throw() {
  // Get srm service
  castor::IService* s = svcs()->service("OraSrmDaemonSvc", castor::SVC_ORASRMDAEMONSVC);
  castor::srm::daemon::ISrmDaemonSvc* srmdSvc = dynamic_cast<castor::srm::daemon::ISrmDaemonSvc*>(s);

  // Get StageRequest
  castor::srm::StageRequest* req = dynamic_cast<castor::srm::StageRequest*>(param);

  // Get uuid as a struct for logging purposes
  Cuuid_t myId = nullCuuid;
  string2Cuuid(&myId, const_cast<char*>(req->srmRequestId().c_str()));

  // Log the start of the handling of this request
  std::list<castor::log::Param> initParams = {
    castor::log::Param("REQID", myId),
    castor::log::Param("RequestToken", req->id()),
    castor::log::Param("Type", castor::srm::RequestTypeStrings[req->requestType()]),
    castor::log::Param("Protocol", req->protocol()),
    castor::log::Param("SvcClass", req->svcClass()),
    castor::log::Param("VO", req->srmUser()->vo()),
    castor::log::Param("DN", req->srmUser()->dn()),
    castor::log::Param("uid", req->euid()),
    castor::log::Param("gid", req->egid()),
    castor::log::Param("ElapsedTime", time(NULL)-req->creationTime())
  };
  // "Processing started" message
  castor::log::write(LOG_INFO, "Processing started", initParams);

  // dispatch work based on request type
  switch (req->requestType()) {
  case castor::srm::REQUESTTYPE_GET:
  case castor::srm::REQUESTTYPE_BOL:
    handlePrepareToGet(req, srmdSvc, myId);
    break;
  case castor::srm::REQUESTTYPE_PUT:
    handlePrepareToPut(req, srmdSvc, myId);
    break;
  default:
    castor::exception::Exception e;
    e.getMessage() << "Unknown Request type : " << req->requestType() << std::endl;
    deleteRequest(req);
    throw e;
  }
}


//------------------------------------------------------------------------------
// handlePrepareToGet
//------------------------------------------------------------------------------
void castor::srm::daemon::StageReqSvcThread::handlePrepareToGet
(castor::srm::StageRequest* req,
 castor::srm::daemon::ISrmDaemonSvc* srmdSvc,
 const Cuuid_t &myId) throw() {
  // In case no svcClass is given
  if (req->svcClass().empty()) {
    // gather best service classes where the files already are (for those online)
    castor::srm::daemon::utils::SvcClassMap* sr2sc = gatherSvcClass(req, myId);
    if (0 == sr2sc) {
      // gathering failed, we need to cancel the whole request
      srmdSvc->failRequest(req, SEINTERNAL, "Error trying to locate the file in the disk cache", myId);
      // log the failure
      logRequestFailure(req, "Error trying to locate the file in the disk cache", myId);
      // cleanup memory
      deleteRequest(req);
      return;
    }
    // Proceed to getting Turl for files where a svcClass was found,
    // as we know these files are already around
    // list of subrequests for which a get is needed
    std::list<castor::srm::SubRequest*> getSRs;
    for (castor::srm::daemon::utils::SvcClassMap::iterator it = sr2sc->begin();
         it != sr2sc->end();
         it++) {
      castor::srm::daemon::utils::handleStagedFile
        (it->first, it->second.svcClass, it->second.diskServer,
         srmdSvc, myId, getSRs);
    }
    // issue a GET request to castor if needed. Note that we may need to send several
    // requests in case a mix of service classes was found. We will send one per
    // service class
    std::list<castor::srm::SubRequest*>::iterator it = getSRs.begin();
    while (it != getSRs.end()) {
      // the service class that we will process right now
      std::string& svcClass = sr2sc->operator[](*it).svcClass;
      // put all subrequests that have the same svcclass as the current one in front
      std::list<castor::srm::SubRequest*>::iterator middle =
        std::partition(it, getSRs.end(), hasSvcClass(sr2sc, svcClass));
      // Send a get request for this service class
      castor::srm::daemon::utils::sendGetRequest
        (it, middle, svcClass, srmdSvc, myId, m_castorClient);
      // and go on
      it = middle;
    }
    // Remove files with a service class from the request as they are over
    for (castor::srm::daemon::utils::SvcClassMap::iterator it = sr2sc->begin();
         it != sr2sc->end();
         it++) {
      if(it->first) {
        it->first->request()->removeSubRequests(it->first);
        deleteSubRequest(it->first);
      }
    }
    // cleanup memory
    delete sr2sc;
    // For remaining files in the request, set service class to default
    if (req->subRequests().size() > 0) {
      try {
        req->setSvcClass(castor::srm::daemon::utils::getDefaultSvcClass(req->srmUser()->vo(), myId));
      } catch (castor::exception::Exception e) {
        // gathering the default service class failed, we need to cancel the whole request
        srmdSvc->failRequest(req, SEINTERNAL, "Misconfiguration: No default service classes defined", myId);
        // log the failure
        logRequestFailure(req, "Misconfiguration: no default service classes defined", myId);
        // cleanup memory
        deleteRequest(req);
        return;
      }
    }
  }
  // Check whether some part of the request was not canceled
  // This call will remove all aborted subrequests from req,
  // and take a lock on the request.
  srmdSvc->checkAbortedSubReqs(req, myId);

  // Back to normal case where service class is set at the request level
  // Build a PrepareToGet request to be sent to castor
  if (req->subRequests().size() > 0) {
    castor::stager::StagePrepareToGetRequest ptgReq;
    ptgReq.setSvcClassName(req->svcClass());
    ptgReq.setEuid(req->euid());
    ptgReq.setEgid(req->egid());
    for (std::vector<castor::srm::SubRequest*>::const_iterator it = req->subRequests().begin();
         it != req->subRequests().end();
         it++) {
      castor::stager::SubRequest *subreq = new castor::stager::SubRequest();
      subreq->setFileName((*it)->castorFileName());
      ptgReq.addSubRequests(subreq);
      subreq->setRequest(&ptgReq);
    }
    // Send the request, and handle error cases
    std::vector<castor::rh::Response*> resps;
    try {
      FileIdMap fileIds = castor::srm::daemon::utils::buildFileIdMap(req);
      resps = castor::srm::CastorClient::sendRequest(myId, ptgReq, req->rhHost(), req->rhPort(), fileIds);
      req->setCastorReqId(ptgReq.reqId());
    } catch (castor::exception::Exception e) {
      // "Stager call failed" message
      std::list<castor::log::Param> snafu = {
        castor::log::Param("REQID", myId),
        castor::log::Param("RequestToken", req->id()),
        castor::log::Param("ErrorMessage", sstrerror(e.code())),
        castor::log::Param("Details",  e.getMessage().str())
      };
      castor::log::write(LOG_ERR, "Stager call failed", snafu);
      // Try to mark the request as failed.
      // Note that this call will not raise any exception
      std::string fullerror = "Unable to issue PrepareToGet request to CASTOR: " + e.getMessage().str();
      srmdSvc->failRequest(req, SEINTERNAL, fullerror, myId);
      // final logging
      logRequestFailure(req, fullerror, myId);
      // cleanup memory
      deleteRequest(req);
      return;
    }
    // handle responses coming from the stager
    // Just check for failures and accordingly update the DB
    for (std::vector<castor::rh::Response*>::const_iterator it = resps.begin();
         it != resps.end();
         it++) {
      if ((*it)->errorCode() != 0) {
        // one failure, find the subrequest
        castor::rh::FileResponse* fr = dynamic_cast<castor::rh::FileResponse*>((*it));
        castor::srm::SubRequest* sr = 0;
        for (std::vector<castor::srm::SubRequest*>::const_iterator it2 = req->subRequests().begin();
             it2 != req->subRequests().end();
             it2++) {
          if ((*it2)->castorFileName() == fr->castorFileName() &&
              (*it2)->status() == castor::srm::SUBREQUEST_INPROGRESS) {
            sr = *it2;
            // this status is not used, we only need something no INPROGRESS
            // to not take 2x the same subrequest in case 2 are dealing with the
            // same file
            sr->setStatus(castor::srm::SUBREQUEST_SUCCESS);
            break;
          }
        }
        // no subreq found !?!?
        if (0 == sr) {
          // "Exception Error, mixed reponses detected" message
          std::list<castor::log::Param> snafu = {
            castor::log::Param("REQID", myId),
            castor::log::Param("Where", "StageReqSvcThread::handlePrepareToGet"),
            castor::log::Param("Unknown file", fr->castorFileName())
          };
          castor::log::write(LOG_ERR, "Exception Error mixed responses detected", snafu);
          continue;
        }
        // Fail the concerned subrequest
        // Note that this call will not raise any exception
        srmdSvc->failSubRequest(sr, fr->errorCode(), fr->errorMessage(), myId);
        // "Subrequest Failed"
        std::list<castor::log::Param> snafu = {
          castor::log::Param("REQID", myId),
          castor::log::Param("RequestToken", req->id()),
          castor::log::Param("File", fr->castorFileName()),
          castor::log::Param("Error", sstrerror(fr->errorCode())),
          castor::log::Param("Reason", fr->errorMessage()),
          castor::log::Param("Where", "StageReqSvcThread::handlePrepareToGet")
        };
        castor::log::write(LOG_ERR, "Subrequest Failed", snafu);
        // remove this subrequest from the list we have in memory, and release the memory
        req->removeSubRequests(sr);
        deleteSubRequest(sr);
      }
    delete (*it);
    }
  }
  
  // Release lock on request
  srmdSvc->commit(myId);
  // Are we over with this request ?
  // If yes, nothing to do, the fact that all subrequests are over
  // will have updated the status of the request in the DB to DONE
  if (0 != req->subRequests().size()) {
    // no, hand over to the recall poller
    srmdSvc->requestToBePolled(req, 0, myId);   // 0 will make the poller pick this up immediately
    // wake up a RecallPoller thread to speed up processing
    castor::server::NotifierThread::getInstance()->doNotify('R');
  }
  // final logging
  std::list<castor::log::Param> snafu = {
    castor::log::Param("REQID", myId),
    castor::log::Param("RequestToken", req->id()),
    castor::log::Param("Type", "GET"),
    castor::log::Param("SvcClass", req->svcClass()),
    castor::log::Param("DN", req->srmUser()->dn()),
    castor::log::Param("VO", req->srmUser()->vo()),
    castor::log::Param("uid", req->euid()),
    castor::log::Param("gid", req->egid()),
    castor::log::Param("ElapsedTime", time(NULL)-req->creationTime())
  };
  if (0 == req->subRequests().size()) {
    // "Processing complete" message
    castor::log::write(LOG_INFO, "Processing complete", snafu);
  } else {
    // "Handing off to RecallPoller" message
    castor::log::write(LOG_DEBUG, "Handing off to RecallPoller", snafu);
  }
  // cleanup memory
  deleteRequest(req);
}


//------------------------------------------------------------------------------
// handlePrepareToPut
//------------------------------------------------------------------------------
void castor::srm::daemon::StageReqSvcThread::handlePrepareToPut
(castor::srm::StageRequest* req,
 castor::srm::daemon::ISrmDaemonSvc* srmdSvc,
 const Cuuid_t &myId) throw() {
  std::vector<castor::srm::SubRequest*> subreqs;
  // check the overwrite option: if it's NEVER, we have first to insure
  // that the file does not exist before issuing the PrepareToPut
  // (Castor will always accept the PtP as if overwrite = ALWAYS)
  if(req->overwriteOption() == castor::srm::OVERWRITE_NEVER) {
    // Loop over all subrequests and check if file exists in namespace
    // if not, fail the subrequest and detach it from the request
    std::remove_if(req->subRequests().begin(), req->subRequests().end(),
                   failSubReqWhenFileExists(srmdSvc, myId));
  }
  if (req->subRequests().size() > 0) {
    // Build a PrepareToPut request to be sent to castor
    castor::stager::StagePrepareToPutRequest ptpReq;
    ptpReq.setSvcClassName(req->svcClass());
    ptpReq.setEuid(req->euid());
    ptpReq.setEgid(req->egid());
    for (std::vector<castor::srm::SubRequest*>::iterator it = req->subRequests().begin();
         it != req->subRequests().end();
         it++) {
      castor::stager::SubRequest *subreq = new castor::stager::SubRequest();
      subreq->setFileName((*it)->castorFileName());
      subreq->setModeBits(0644);
      ptpReq.addSubRequests(subreq);
      subreq->setRequest(&ptpReq);
    }
    // Send the request, and handle error cases
    std::vector<castor::rh::Response*> resps;
    try {
      FileIdMap fileIds = castor::srm::daemon::utils::buildFileIdMap(req);
      resps = castor::srm::CastorClient::sendRequest(myId, ptpReq, req->rhHost(), req->rhPort(), fileIds);
    } catch (castor::exception::Exception e) {
      // "Stager call failed" message
      std::list<castor::log::Param> snafu = {
        castor::log::Param("REQID", myId),
        castor::log::Param("RequestToken", req->id()),
        castor::log::Param("ErrorMessage", sstrerror(e.code())),
        castor::log::Param("Details",  e.getMessage().str())
      };
      castor::log::write(LOG_ERR, "Stager call failed", snafu);
      // Try to mark the request as failed.
      // Note that this call will not raise any exception
      std::string fullerror = "Unable to issue PrepareToPut request to CASTOR: " + e.getMessage().str();
      srmdSvc->failRequest(req, SEINTERNAL, fullerror, myId);
      // final logging
      logRequestFailure(req, fullerror, myId);
      // cleanup memory
      deleteRequest(req);
      return;
    }
    // handle responses coming from the stager
    // Just check for failures and accordingly update the DB
    for (std::vector<castor::rh::Response*>::const_iterator it = resps.begin();
         it != resps.end();
         it++) {
      if ((*it)->errorCode() != 0) {
        // one failure, find the subrequest
        castor::rh::FileResponse* fr = dynamic_cast<castor::rh::FileResponse*>((*it));
        castor::srm::SubRequest* sr = 0;
        for (std::vector<castor::srm::SubRequest*>::const_iterator it2 = req->subRequests().begin();
             it2 != req->subRequests().end();
             it2++) {
          if ((*it2)->castorFileName() == fr->castorFileName() &&
              (*it2)->status() == castor::srm::SUBREQUEST_INPROGRESS) {
            sr = *it2;
            // this status is not used, we only need something no INPROGRESS
            // to not take 2x the same subrequest in case 2 are dealing with the
            // same file
            sr->setStatus(castor::srm::SUBREQUEST_SUCCESS);
            break;
          }
        }
        // no subreq found !?!?
        if (0 == sr) {
          // "Exception Error, mixed reponses detected" message
          std::list<castor::log::Param> snafu = {
            castor::log::Param("REQID", myId),
            castor::log::Param("Where", "StageReqSvcThread::handlePrepareToPut"),
            castor::log::Param("Unknown file", fr->castorFileName())
          };
          castor::log::write(LOG_ERR, "Exception Error mixed responses detected", snafu);
          continue;
        }
        // We may have to change the error code and message in case
        // we got a race condition with another user while submitting our
        // prepareToPut
        if ((*it)->errorCode() == EBUSY && req->overwriteOption() == castor::srm::OVERWRITE_NEVER) {
          sr->setErrorCode(EEXIST);
          sr->setReason("File exists and overwrite option is OVERWRITE_NEVER");
        }
        // Fail the concerned subrequest
        // Note that this call will not raise any exception
        srmdSvc->failSubRequest(sr, fr->errorCode(), fr->errorMessage(), myId);
        // "Subrequest Failed"
        std::list<castor::log::Param> snafu = {
          castor::log::Param("REQID", myId),
          castor::log::Param("RequestToken", req->id()),
          castor::log::Param("File", fr->castorFileName()),
          castor::log::Param("Error", sstrerror(fr->errorCode())),
          castor::log::Param("Reason", fr->errorMessage())
        };
        castor::log::write(LOG_ERR, "Subrequest Failed", snafu);
        // remove this subrequest from the list we have in memory, and release the memory
        req->removeSubRequests(sr);
        deleteSubRequest(sr);
      }
    delete (*it);
    }
  }

  // Check whether some part of the request was not canceled
  // This call will remove all aborted subrequests from req,
  // and take a lock on the request.
  srmdSvc->checkAbortedSubReqs(req, myId);

  // If we get here we have good files and we want to store their fileids in the DB.
  // Except that a Cns_stat error here needs to be considered as a failure; so we use
  // the same pattern as above, more details in the functor.
  if (req->subRequests().size() > 0) {
    std::remove_if(req->subRequests().begin(), req->subRequests().end(),
      failSubReqWhenNsError(srmdSvc, myId));
  }

  // Now we are settled for requesting TURLs
  if (req->subRequests().size() > 0) {
    // We have now to issue a put (except in the XROOT case)
    if(req->protocol() != SRM_PROTOCOL_XROOT) {
      // build the put request
      castor::stager::StagePutRequest preq;
      preq.setSvcClassName(req->svcClass());
      preq.setEuid(req->euid());
      preq.setEgid(req->egid());
      std::string protocol = req->protocol();
      std::vector<u_signed64> idsVector;
      for (std::vector<castor::srm::SubRequest*>::const_iterator it = req->subRequests().begin();
           it != req->subRequests().end();
           it++) {
        castor::stager::SubRequest *subreq = new castor::stager::SubRequest();
        subreq->setProtocol(protocol);
        subreq->setFileName((*it)->castorFileName());
        subreq->setXsize((*it)->reservedSize());
        subreq->setModeBits(0644);
        preq.addSubRequests(subreq);
        subreq->setRequest(&preq);
        idsVector.push_back((*it)->id());
      }
      // fill in a request UUID to be passed to Castor
      preq.setReqId(srmdSvc->prepareCastorReq(idsVector, req->svcClass(), myId));
      // Send the put request
      FileIdMap fileIds = castor::srm::daemon::utils::buildFileIdMap(req);
      try {
        m_castorClient.asynchSendRequest(myId, preq, req->rhHost(), req->rhPort(), fileIds);
      } catch (castor::exception::Exception e) {
        // The call to Castor failed, fail all the subrequests
        for (std::vector<castor::srm::SubRequest*>::const_iterator it = req->subRequests().begin();
             it != req->subRequests().end();
             it++) {
          // Note that this call will not raise any exception
          srmdSvc->failSubRequest((*it), e.code(), e.getMessage().str(), myId);
          // "Subrequest Failed"
          std::list<castor::log::Param> snafu = {
            castor::log::Param("REQID", myId),
            castor::log::Param("RequestToken", req->id()),
            castor::log::Param("File", (*it)->castorFileName()),
            castor::log::Param("Error", sstrerror(e.code())),
            castor::log::Param("Reason", e.getMessage().str()),
            castor::log::Param("Where", "StageReqSvcThread::handlePrepareToPut")
          };
          castor::log::write(LOG_ERR, "Subrequest Failed", snafu);
          // remove this subrequest from the list we have in memory, and release the memory
          deleteSubRequest(*it);
        }        
        req->subRequests().clear();
      }
    } else {
      // in the xroot case, we are over, so it is enough to log the TURLs
      for (std::vector<castor::srm::SubRequest*>::iterator it = req->subRequests().begin();
           it != req->subRequests().end();
           it++) {
        // build a nsId struct
        struct Cns_fileid nsId;
        nsId.fileid = (*it)->userFile()->fileId();
        strncpy(nsId.server, (*it)->userFile()->nsHost().c_str(), sizeof(nsId.server));
        // build the TUrl and update DB
        castor::srm::daemon::utils::buildTUrl
          (*it, req->rhHost(), req->rhPort(), (*it)->castorFileName(),
           req->svcClass(), srmdSvc, myId, &nsId);
        // cleanup memory
        deleteSubRequest(*it);
      }
      req->subRequests().clear();
    }
  }

  // Release lock on request
  srmdSvc->commit(myId);
  // Log that we are over if ever we are
  if (0 == req->subRequests().size()) {
    // final logging for the request
    std::list<castor::log::Param> snafu = {
      castor::log::Param("REQID", myId),
      castor::log::Param("RequestToken", req->id()),
      castor::log::Param("Type", "PUT"),
      castor::log::Param("SvcClass", req->svcClass()),
      castor::log::Param("DN", req->srmUser()->dn()),
      castor::log::Param("VO", req->srmUser()->vo()),
      castor::log::Param("uid", req->euid()),
      castor::log::Param("gid", req->egid()),
      castor::log::Param("ElapsedTime", time(NULL)-req->creationTime())
    };
    // "Processing complete" message
    castor::log::write(LOG_INFO, "Processing complete", snafu);
  }
  // cleanup memory
  deleteRequest(req);
}

//------------------------------------------------------------------------------
// failSubReqWhenFileExists
//------------------------------------------------------------------------------
bool castor::srm::daemon::StageReqSvcThread::failSubReqWhenFileExists::operator()
  (castor::srm::SubRequest* subreq) const throw() {
  struct Cns_filestat buf;
  int rc = Cns_stat(subreq->castorFileName().c_str(), &buf);
  if (rc == 0) {
    // "Subrequest Failed"
    std::list<castor::log::Param> snafu = {
      castor::log::Param("NSFILEID", buf.fileid),
      castor::log::Param("REQID", m_myId),
      castor::log::Param("RequestToken", subreq->request()->id()),
      castor::log::Param("File", subreq->castorFileName()),
      castor::log::Param("Error", sstrerror(EEXIST)),
      castor::log::Param("Reason", "File exists and overwrite option is OVERWRITE_NEVER"),
      castor::log::Param("Where", "StageReqSvcThread::failSubReqWhenFileExists")
    };
    castor::log::write(LOG_ERR, "Subrequest Failed", snafu);
    // file exists, so fail this subrequest
    m_srmdSvc->failSubRequest(subreq, EEXIST, "File exists and overwrite option is OVERWRITE_NEVER", m_myId);
    subreq->request()->removeSubRequests(subreq);
    deleteSubRequest(subreq);
    return true;
  }
  return false;
}

//------------------------------------------------------------------------------
// failSubReqWhenNsError
//------------------------------------------------------------------------------
bool castor::srm::daemon::StageReqSvcThread::failSubReqWhenNsError::operator()
  (castor::srm::SubRequest* subreq) const throw() {
  struct Cns_filestat buf;
  serrno = 0;
  int rc = Cns_stat(subreq->castorFileName().c_str(), &buf);
  if (rc == 0) {
    // We have a good file so we store its fileid in the SRM DB
    subreq->userFile()->setFileId(buf.fileid);
    subreq->userFile()->setNsHost(castor::srm::SrmConstants::getInstance()->nsHost().c_str());
    m_srmdSvc->fillFileId(subreq->userFile(), m_myId);
    return false;
  }
  else {
    if(serrno == ENOENT) {
       // the file got deleted meanwhile!
       subreq->setReason("SURL deleted while being created");
    }
    else {
       // any other error is considered an internal error
       subreq->setReason(sstrerror(serrno));
    }
    // either case, we have to fail the subrequest
    m_srmdSvc->failSubRequest(subreq, (serrno == ENOENT ? ENOENT : SEINTERNAL),
      subreq->reason(), m_myId);
    // "Subrequest Failed"
    std::list<castor::log::Param> snafu = {
      castor::log::Param("REQID", m_myId),
      castor::log::Param("RequestToken", subreq->request()->id()),
      castor::log::Param("File", subreq->castorFileName()),
      castor::log::Param("Error", serrno),
      castor::log::Param("Reason", subreq->reason()),
      castor::log::Param("Where", "StageReqSvcThread::failSubReqWhenNsError")
    };
    castor::log::write(LOG_ERR, "Subrequest Failed", snafu);
    subreq->request()->removeSubRequests(subreq);
    deleteSubRequest(subreq);
    return true;
  }
}


//------------------------------------------------------------------------------
// gatherSvcClass
//------------------------------------------------------------------------------
castor::srm::daemon::utils::SvcClassMap*
castor::srm::daemon::StageReqSvcThread::gatherSvcClass
(castor::srm::StageRequest* req, const Cuuid_t &myId)
  throw() {
  // first get the list of possible service classes
  std::vector<std::string> scs =
    castor::srm::daemon::utils::getSvcClassList(req->srmUser()->vo(), myId);
  if (0 == scs.size()) return 0;
  // build a query request to be sent to CASTOR
  castor::stager::StageFileQueryRequest fqreq;
  fqreq.setEuid(req->euid());
  fqreq.setEgid(req->egid());
  fqreq.setSvcClassName("*");
  for (std::vector<castor::srm::SubRequest*>::const_iterator it = req->subRequests().begin();
       it != req->subRequests().end();
       it++) {
    castor::stager::QueryParameter *par = new castor::stager::QueryParameter();
    par->setQueryType(castor::stager::REQUESTQUERYTYPE_FILENAME_ALLSC); 
    par->setValue((*it)->castorFileName());
    par->setQuery(&fqreq);
    fqreq.addParameters(par);
  }
  // Send the request
  std::vector<castor::rh::Response*> resps;
  try {
    FileIdMap fileIds = castor::srm::daemon::utils::buildFileIdMap(req);
    resps = castor::srm::CastorClient::sendRequest(myId, fqreq, req->rhHost(), req->rhPort(), fileIds);
  } catch (castor::exception::Exception e) {
    // "Stager call failed" message
    std::list<castor::log::Param> snafu = {
      castor::log::Param("REQID", myId),
      castor::log::Param("RequestToken", req->id()),
      castor::log::Param("ErrorMessage", sstrerror(e.code())),
      castor::log::Param("Details",  e.getMessage().str())
    };
    castor::log::write(LOG_ERR, "Stager call failed", snafu);
    return 0;
  }
  // handy map from service classes to ranking
  std::map<std::string, int> scRank;
  for (unsigned int i = 0; i < scs.size(); i++) {
    scRank[scs[i]] = i;
  }
  // handy multi map from castorFileName to subrequests
  // Note the multi map as several subrequests could deal with the same file
  std::multimap<std::string, castor::srm::SubRequest*> f2sr;
  for (std::vector<castor::srm::SubRequest*>::iterator srit = req->subRequests().begin();
       srit != req->subRequests().end();
       srit++) {
    f2sr.insert(std::pair<std::string, castor::srm::SubRequest*>
                ((*srit)->castorFileName(), *srit));
  }
  // Work out the proper list of service classes
  // We go through responses and take the best known service class for each file,
  // that is the one with the best ranking
  castor::srm::daemon::utils::SvcClassMap* res = new castor::srm::daemon::utils::SvcClassMap();
  for (std::vector<castor::rh::Response*>::const_iterator respit = resps.begin();
       respit != resps.end();
       respit++) {
    // consider only staged and 'stageable' (i.e. needing internal replication) files
    castor::rh::FileQryResponse* resp = dynamic_cast<castor::rh::FileQryResponse*>(*respit);
    if (resp->status() != ::FILE_STAGED && resp->status() != ::FILE_CANBEMIGR
        && resp->status() != ::FILE_STAGEABLE) continue;
    // get rank of the service class of the current response. If none, ignore
    std::map<std::string, int>::const_iterator rankit = scRank.find(resp->poolName());
    if (rankit == scRank.end()) continue;
    // get an iterator on first subrequest for this file. Others can be accessed
    // by the operator++ on the iterator
    std::multimap<std::string, castor::srm::SubRequest*>::const_iterator srit =
      f2sr.find(resp->castorFileName());
    // check whether the new ranking is better than what we already have
    bool betterRanking = false;
    castor::srm::daemon::utils::SvcClassMap::const_iterator scit = res->find(srit->second);
    if (scit == res->end() || rankit->second < scRank[scit->second.svcClass]) {
      betterRanking = true;
    }
    // use the new pool as it has better ranking
    if (betterRanking) {
      // Go through all requests for the same file
      do {
        castor::srm::daemon::utils::FileLocation loc;
        loc.svcClass = resp->poolName();
        loc.diskServer = resp->diskServer();
        (*res)[srit->second] = loc;
        srit++;
      } while (srit != f2sr.end() && srit->first == resp->castorFileName());
    }
    delete (*respit);
  }
  // return
  return res;
}


//------------------------------------------------------------------------------
// deleteSubRequest
//------------------------------------------------------------------------------
void castor::srm::daemon::StageReqSvcThread::deleteSubRequest (castor::srm::SubRequest* subreq)
  throw() {
  if (0 != subreq->userFile()) delete subreq->userFile();
  subreq->setUserFile(0);
  subreq->setRequest(0);
  delete subreq;
}

//------------------------------------------------------------------------------
// deleteRequest
//------------------------------------------------------------------------------
void castor::srm::daemon::StageReqSvcThread::deleteRequest (castor::srm::StageRequest* req)
  throw() {
  for (std::vector<castor::srm::SubRequest*>::const_iterator it = req->subRequests().begin();
       it != req->subRequests().end();
       it++) {
    deleteSubRequest(*it);
  }
  req->subRequests().clear();
  if (0 != req->srmUser()) delete req->srmUser();
  delete req;
}


//------------------------------------------------------------------------------
// logRequestFailure
//------------------------------------------------------------------------------
void castor::srm::daemon::StageReqSvcThread::logRequestFailure
(castor::srm::StageRequest* req,
 const std::string message,
 const Cuuid_t &myId)
  throw() {
  // Loop over the subrequests
  for (std::vector<castor::srm::SubRequest*>::const_iterator it = req->subRequests().begin();
       it != req->subRequests().end();
       it++) {
    // Issue a log message for the subrequest
    std::list<castor::log::Param> snafu = {
      castor::log::Param("NSFILEID", (*it)->userFile()->fileId()),
      castor::log::Param("REQID", myId),
      castor::log::Param("RequestToken", req->id()),
      castor::log::Param("Failure", message),
      castor::log::Param("SvcClass", req->svcClass()),
      castor::log::Param("DN", req->srmUser()->dn()),
      castor::log::Param("VO", req->srmUser()->vo()),
      castor::log::Param("uid", req->euid()),
      castor::log::Param("gid", req->egid()),
      castor::log::Param("ElapsedTime", time(NULL)-req->creationTime())
    };
    // "Processing complete with failures" message
    castor::log::write(LOG_ERR, "Processing complete with failures", snafu);
  }
}
