/******************************************************************************
 *                      ISrmDaemonSvc.hpp
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * This class provides database API for the SRM daemon
 *
 * @author castor dev team
 *****************************************************************************/

#ifndef DAEMON_ISRMDAEMONSVC_HPP
#define DAEMON_ISRMDAEMONSVC_HPP 1

#include "castor/IService.hpp"
#include "castor/exception/Exception.hpp"
#include "Cuuid.h"
#include <string>

namespace castor {
 namespace srm {

  // Forward declaration
  class StageRequest;
  class SubRequest;
  class UserFile;

  namespace daemon {

    /**
     * Database API for the SRM daemon
     */
    class ISrmDaemonSvc : public virtual castor::IService {

    public:

      /**
       * Get a TURL base on the originating request and the castor
       * file name.
       * @param reqId The request id as a std::string
       * @param the castor file name to match as a string
       * @return The fully qualified TURL, or empty std::string if not found
       * @exception Exception in case of error
       */
      virtual std::string getTURL(std::string reqId, std::string cfn)
        throw (castor::exception::Exception) = 0;

      /**
       * Selects the next stage request the StageSvc threads should deal with.
       * @return the request to process. The request object, its SrmUser,
       * SubRequests and UserFiles dependency are filled, but only with
       * needed data. So no call to e.g. updateRep is allowed !
       * Note that no exception is thrown in case of error. The error will be
       * logged and a null pointer is sent back.
       * Also note that the user is responsible for the memory management
       * of the return request, including dependencies
       */
      virtual StageRequest* stageRequestToDo() throw() = 0;

      /**
       * Selects the next stage request the RecallPoller threads should deal with.
       * @return the request to process. The request object, its SrmUser,
       * SubRequests and UserFiles dependency are filled, but only with
       * needed data. So no call to e.g. updateRep is allowed !
       * Note that no exception is thrown in case of error. The error will be
       * logged and a null pointer is sent back.
       * Also note that the user is responsible for the memory management
       * of the return request, including dependencies
       */
      virtual StageRequest* requestToPoll() throw() = 0;

      /**
       * Fail a complete request by setting the status of all subrequest
       * still in progress to FAIL and the status of the request to DONE.
       * also updates the errorCode and error message and commits the
       * transaction.
       * Note that no exception is thrown as there is not much to do in
       * case of error, but logging it, which is done internally
       * @param req the request concerned
       * @param errorCode the code of the error that will be visible to the user
       * @param errorMessage the error message that will be visible to the user
       * @param reqId the request uuid to use for logging in case of error
       */
      virtual void failRequest(const castor::srm::StageRequest* req,
                               const int errorCode,
                               const std::string errorMessage,
                               const Cuuid_t& reqId)
        throw() = 0;

      /**
       * Fail a subRequest by setting its status to fail, updating errorCode
       * and errorMessage if the subrequest is still in progress and commiting
       * the transaction. The subrequest will not be touched in case it has
       * been aborted in the mean time.
       * In case the subrequest was the last one in progress for the request,
       * updates the request's status to DONE.
       * Note that no exception is thrown as there is not much to do in
       * case of error, but logging it, which is done internally
       * @param subreq the subRequest concerned
       * @param errorCode the code of the error that will be visible to the user
       * @param errorMessage the error message that will be visible to the user
       * @param reqId the request uuid to use for logging in case of error
       */
      virtual void failSubRequest(const castor::srm::SubRequest* subreq,
                                  const int errorCode,
                                  const std::string errorMessage,
                                  const Cuuid_t& reqId)
        throw() = 0;

      /**
       * Succeeds a subRequest by setting its status to SUCCESS and its
       * reservedSize. The subrequest will not be touched in case it has
       * been aborted in the mean time.
       * In case the subrequest was the last one in progress for the request,
       * updates the request's status to DONE.
       * Note that no exception is thrown as there is not much to do in
       * case of error, but logging it, which is done internally
       * @param subreq the subRequest concerned
       * @param reqId the request uuid to use for logging in case of error
       */
      virtual void succeedSubRequest(const castor::srm::SubRequest* subreq,
                                     const Cuuid_t& reqId)
        throw() = 0;

      /**
       * mark a request for polling by the RecallPoller thread by changing its
       * status to READYTOPOLL, and set the lastCheck and nextCheck values.
       * lastCheck will be set to current nextCheck while nextCheck will
       * be incremented by the given time interval. The transaction is
       * then commited.
       * In case the request was aborted in the mean time, no action is taken.
       * Note that no exception is thrown as there is not much to do in
       * case of error, but logging it, which is done internally
       * @param req the request concerned
       * @param interval the time interval in seconds until the next check
       * @param reqId the request uuid to use for logging in case of error
       */
      virtual void requestToBePolled(const castor::srm::StageRequest* req,
                                     const u_signed64 interval,
                                     const Cuuid_t& reqId)
        throw() = 0;

      /**
       * prepare a request to be asynchronously sent to Castor
       * by initializing the castorReqId of the given subrequests.
       * The status remains INPROGRESS.
       * Note that no exception is thrown as there is not much to do in
       * case of error, but logging it, which is done internally
       * @param subreqIds a vector containing the subreq ids to be processed
       * @param svcClass the service class which will be used in this request,
       *        for logging purposes once we receive the response back
       * @param reqId the request uuid to use for logging in case of error
       * @return the UUID generated for the Castor request
       */
      virtual std::string prepareCastorReq(std::vector<u_signed64>& subreqIds,
                                           const std::string& svcClass,
                                           const Cuuid_t& reqId)
        throw() = 0;

      /**
       * fills a userfile in the database with fileId and NsHost present in
       * memory and commits the transaction.
       * Note that no exception is thrown as there is not much to do in
       * case of error, but logging it, which is done internally
       * @param userFile the user file in memory
       * @param reqId the request uuid to use for logging in case of error
       */
      virtual void fillFileId(const castor::srm::UserFile* userFile,
                              const Cuuid_t& reqId)
        throw() = 0;

      /**
       * checks which subrequests were aborted out of the ones attached to
       * the given request and removes them from the request (and free the
       * memory)
       * Note that no exception is thrown. In case we do not manage to run
       * the check, the request is not touched
       * @param const castor::srm::StageRequest* req
       * @param reqId the request uuid to use for logging in case of error
       */
      virtual void checkAbortedSubReqs(srm::StageRequest* req,
                                       const Cuuid_t& reqId)
        throw() = 0;

      /**
       * retrieve a subrequest from the database based on request id and file name.
       * The first subrequest matching, with status INPROGRESS and not locked is
       * retrieved. This method takes a lock on the subrequest and keeps it, the
       * caller is in charge of releasing it.
       * The returned subrequest has it request attached as well as a UserFile
       * and an SrmUser.
       * The user is responsible the memory management of the 4 objects.
       * Note that no exception is thrown in case of error. The error will be
       * logged and a null pointer is sent back.
       * @param requestId the Castor request UUID used for the Get|Put request
       * @param fileId the file that the subrequest is dealing with
       * @param fileName the Castor filename, to double check that the file
       * didn't change meanwhile.
       * @return a subrequest with request and user file
       */
       virtual castor::srm::SubRequest* getSubRequestById(const std::string& reqId,
                                                 const u_signed64 fileId,
                                                 const std::string& fileName)
        throw() = 0;

      /**
       * Fills the Turl field of a given request in the database, updates the
       * subrequest status to SUCCESS and commit the transaction. In case
       * this subrequest was the last one not yet finished for its parent
       * request, then also change the status of the parent request to DONE.
       * Note that no exception is thrown as there is not much to do in
       * case of error, but logging it, which is done internally
       * @param subreq the subRequest concerned
       * @param reqId the request uuid to use for logging in case of error
       */
      virtual void setTurl(const castor::srm::SubRequest* req,
                           const Cuuid_t& reqId)
        throw() = 0;

      /**
       * Commit the database.
       * @param reqId the request uuid to use for logging in case of error
       */
      virtual void commit(const Cuuid_t& reqId) throw() = 0;

    }; // end of class ISrmDaemonSvc

  } // end of namespace daemon

} } /* end of namespace castor/srm */

#endif // DAEMON_ISRMDAEMONSVC_HPP
