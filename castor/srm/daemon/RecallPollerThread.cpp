/******************************************************************************
 *                      RecallPollerThread.cpp
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * This thread is polling castor for ongoing castor requests (recalls
 * for either prepareToGets or BringOnline SRM requests).
 * When the CASTOR requests are over, it either builds the TURL when
 * possible (all cases but GridFTP v2) or issues a Get request to Castor
 * in an asynchronous mode and hands over the postprocessing to the
 * TurlHandler thread
 *
 * @author castor-dev team
 *****************************************************************************/

#include "castor/Services.hpp"
#include "castor/Constants.hpp"
#include "castor/IService.hpp"
#include "castor/log/log.hpp"
#include "castor/rh/Response.hpp"
#include "castor/exception/Exception.hpp"
#include "castor/exception/Exception.hpp"
#include "castor/stager/StageGetRequest.hpp"
#include "castor/stager/StageFileQueryRequest.hpp"
#include "castor/stager/QueryParameter.hpp"
#include "castor/stager/RequestQueryType.hpp"
#include "castor/stager/SubRequest.hpp"
#include "castor/rh/FileQryResponse.hpp"
#include "castor/Constants.hpp"

#include "castor/srm/daemon/RecallPollerThread.hpp"
#include "castor/srm/daemon/ISrmDaemonSvc.hpp"
#include "castor/srm/daemon/SupportedProtocols.hpp"
#include "castor/srm/StageRequest.hpp"
#include "castor/srm/SubRequest.hpp"
#include "castor/srm/SrmUser.hpp"
#include "castor/srm/UserFile.hpp"
#include "castor/srm/CastorClient.hpp"
#include "castor/srm/SrmConstants.hpp"
#include "castor/srm/daemon/SrmdUtils.hpp"

//------------------------------------------------------------------------------
// constructor
//------------------------------------------------------------------------------
castor::srm::daemon::RecallPollerThread::RecallPollerThread
(castor::srm::CastorClient& castorClient) :
  SelectProcessThread(),
  m_castorClient(castorClient) {}

//------------------------------------------------------------------------------
// select
//------------------------------------------------------------------------------
castor::IObject* castor::srm::daemon::RecallPollerThread::select() throw() {
  // Get srm service; we know (cf. SrmDaemon's constructor) we'll get a valid pointer
  castor::IService* s = svcs()->service("OraSrmDaemonSvc", castor::SVC_ORASRMDAEMONSVC);
  castor::srm::daemon::ISrmDaemonSvc* srmdSvc = dynamic_cast<castor::srm::daemon::ISrmDaemonSvc*>(s);
  // Get any new request to do
  return srmdSvc->requestToPoll();
}


//------------------------------------------------------------------------------
// process
//------------------------------------------------------------------------------
void castor::srm::daemon::RecallPollerThread::process(castor::IObject* param) throw() {
  // Get srm service
  castor::IService* s = svcs()->service("OraSrmDaemonSvc", castor::SVC_ORASRMDAEMONSVC);
  castor::srm::daemon::ISrmDaemonSvc* srmdSvc = dynamic_cast<castor::srm::daemon::ISrmDaemonSvc*>(s);

  // Get StageRequest
  castor::srm::StageRequest* req = dynamic_cast<castor::srm::StageRequest*>(param);

  // Get uuid as a struct for logging purposes
  Cuuid_t myId = nullCuuid;
  string2Cuuid(&myId, const_cast<char*>(req->srmRequestId().c_str()));

  // "Checking state of Request" message
  std::list<castor::log::Param> initParams = {
    castor::log::Param("REQID", myId),
    castor::log::Param("RequestToken", req->id()),
    castor::log::Param("Type", "STAGEIN"),
    castor::log::Param("Protocol", req->protocol()),
    castor::log::Param("SvcClass", req->svcClass()),
    castor::log::Param("CastorReqId", req->castorReqId()),
    castor::log::Param("VO", req->srmUser()->vo()),
    castor::log::Param("DN", req->srmUser()->dn()),
    castor::log::Param("uid", req->euid()),
    castor::log::Param("gid", req->egid()),
    castor::log::Param("ElapsedTime", time(NULL)-req->creationTime())
  };
  castor::log::write(LOG_INFO, "Checking state of Request", initParams);

  // Build a FileQuery request to be sent to castor
  castor::stager::StageFileQueryRequest qreq;
  qreq.setSvcClassName(req->svcClass());
  castor::stager::QueryParameter *par = new castor::stager::QueryParameter();
  par->setQueryType(castor::stager::REQUESTQUERYTYPE_REQID_GETNEXT);
  par->setValue(req->castorReqId());
  par->setQuery(&qreq);
  qreq.addParameters(par);

  // Send the request, and handle error cases
  std::vector<castor::rh::Response*> resps;
  try {
    FileIdMap fileIds =
      castor::srm::daemon::utils::buildFileIdMap(req->subRequests().begin(),
                                         req->subRequests().end());
    resps = castor::srm::CastorClient::sendRequest
      (myId, qreq, req->rhHost(), req->rhPort(), fileIds);
  } catch (castor::exception::Exception e) {
    // "Stager call failed" message
    std::list<castor::log::Param> snafu = {
      castor::log::Param("REQID", myId),
      castor::log::Param("RequestToken", req->id()),
      castor::log::Param("ErrorMessage", sstrerror(e.code())),
      castor::log::Param("Details",  e.getMessage().str())
    };
    castor::log::write(LOG_ERR, "Stager call failed", snafu);
    // Give up for this round
    // We rely on the next rounds of polling to work better
    // and on the timeout on polling
    srmdSvc->requestToBePolled(req, (u_signed64)(1.5*(req->nextCheck()-req->lastCheck())), myId);
    deleteRequest(req);
    return;
  }

  // handle error case where the stager lost knowledge of our request
  if (resps.size() == 1 && resps[0]->errorCode() == EINVAL) {
    // The request is not anymore in the database.
    // This should not happen, we will fail the request
    srmdSvc->failRequest(req, SEINTERNAL, "Request not anymore in stager", myId);
    // final logging
    logRequestFailure(req, "Request not found in stager", LOG_ERR, myId);
  } else if (resps.size() == 1 && resps[0]->errorCode() == ENOENT) {
    // case where nothing new is reported by stager
    // compute next polling interval :
    u_signed64 nextCheckInt = (u_signed64)(1.5 * (req->nextCheck() - req->lastCheck()));
    if(nextCheckInt == 0) {
      // at bootstrap we have checked immediately and we have 0 as interval,
      // so set initial time interval
      nextCheckInt = castor::srm::SrmConstants::getInstance()->initialPollInterval();
    }
    // check whether it reaches the max request lifetime
    if (time(0) + nextCheckInt > req->creationTime() + req->expirationInterval()) {
      // we give up here
      srmdSvc->failRequest(req, ETIMEDOUT, "Some of the requested files have not been staged to disk within the requested time", myId);
      // final logging
      logRequestFailure(req, "Timeout", LOG_INFO, myId);
    } else {
      // we will go for another round
      srmdSvc->requestToBePolled(req, nextCheckInt, myId);
    }
    delete resps[0];
  } else {
    // standard case, we got a proper answer
    // list of subrequests for which a get is needed
    std::list<castor::srm::SubRequest*> getSRs;
    // let's go through the reponses
    for (std::vector<castor::rh::Response*>::const_iterator respIt = resps.begin();
         respIt != resps.end();
         respIt++) {
      // find the subrequest
      castor::rh::FileQryResponse* fr = dynamic_cast<castor::rh::FileQryResponse*>((*respIt));
      castor::srm::SubRequest* subreq = 0;
      for (std::vector<castor::srm::SubRequest*>::const_iterator srIt = req->subRequests().begin();
           srIt != req->subRequests().end();
           srIt++) {
        if ((*srIt)->castorFileName() == fr->castorFileName()) {
          subreq = *srIt;
          // depending on the response
          switch (fr->status()) {
          case ::FILE_STAGEIN:
            // still being recalled, keep waiting
            break;
          case ::FILE_STAGEOUT:
            {
              // we fail the request with EBUSY because there's an ongoing Put request in Castor.
              // Actually Castor behaves according to POSIX, as it would return the new file
              // being put (eventually garbage), but according to SRM specs this has to be prevented.
              srmdSvc->failSubRequest(subreq, EBUSY, "Ongoing PrepareToPut request on this SURL", myId);
              // "Subrequest Failed"
              std::list<castor::log::Param> snafu = {
                castor::log::Param("NSFILEID", subreq->userFile()->fileId()),
                castor::log::Param("REQID", myId),
                castor::log::Param("RequestToken", req->id()),
                castor::log::Param("SvcClass", subreq->request()->svcClass()),
                castor::log::Param("uid", subreq->request()->euid()),
                castor::log::Param("gid", subreq->request()->egid()),
                castor::log::Param("ElapsedTime", time(NULL)-subreq->request()->creationTime()),
                castor::log::Param("Error", sstrerror(EBUSY)),
                castor::log::Param("Reason", "Ongoing PrepareToPut request on this SURL")
              };
              castor::log::write(LOG_ERR, "Subrequest Failed", snafu);
            }
            break;
          case ::FILE_STAGED:
          case ::FILE_CANBEMIGR:
            // handle the case
            castor::srm::daemon::utils::handleStagedFile
              (subreq, subreq->request()->svcClass(),
               fr->diskServer(), srmdSvc, myId, getSRs);
            break;
          default:
            // unknown status, fail subrequest and report
            srmdSvc->failSubRequest(subreq, SEINTERNAL,
                                    std::string("Unexpected status for this file : ") +
                                    stage_fileStatusName(fr->status()), myId);
            // "Subrequest Failed"
            std::list<castor::log::Param> snafu = {
              castor::log::Param("NSFILEID", subreq->userFile()->fileId()),
              castor::log::Param("REQID", myId),
              castor::log::Param("RequestToken", req->id()),
              castor::log::Param("SvcClass", subreq->request()->svcClass()),
              castor::log::Param("uid", subreq->request()->euid()),
              castor::log::Param("gid", subreq->request()->egid()),
              castor::log::Param("ElapsedTime", time(NULL)-subreq->request()->creationTime()),
              castor::log::Param("Error", sstrerror(SEINTERNAL)),
              castor::log::Param("Reason", std::string("Unexpected status for this file : ") +
                                 stage_fileStatusName(fr->status()))
            };
            castor::log::write(LOG_ERR, "Subrequest Failed", snafu);
          }
        }
      }
      // no subreq found !?!?
      if (0 == subreq) {
        // "Exception Error, mixed responses detected" message
        std::list<castor::log::Param> snafu = {
          castor::log::Param("REQID", myId),
          castor::log::Param("Where", "RecallPollerThread"),
          castor::log::Param("Unknown file", fr->castorFileName())
        };
        castor::log::write(LOG_ERR, "Exception Error mixed responses detected", snafu);
        continue;
      }
      delete (*respIt);
    }
    // Send a get request to Castor for the subrequests that need it
    if (getSRs.size() > 0) {
      // find out the service class. May not be set, meaning default
      std::string svcClass = req->svcClass();
      if (svcClass.empty()) {
        svcClass = castor::srm::daemon::utils::getDefaultSvcClass(req->srmUser()->vo(), myId);
      }
      castor::srm::daemon::utils::sendGetRequest
        (getSRs.begin(), getSRs.end(), svcClass,
         srmdSvc, myId, m_castorClient);
    }
    // prepare for next round in case we are not over. The fact that we are over or
    // not is checked in the DB as the status of the request is automatically updated
    // by the failSubRequest/succeedSubRequest/setTurl calls
    // note the reset of the next check interval to the bootstrap value
    srmdSvc->requestToBePolled(req, castor::srm::SrmConstants::getInstance()->initialPollInterval(), myId);
  }
  // Memory cleanup
  deleteRequest(req);
}


//------------------------------------------------------------------------------
// deleteRequest
//------------------------------------------------------------------------------
void castor::srm::daemon::RecallPollerThread::deleteRequest (castor::srm::StageRequest* req)
  throw() {
  for (std::vector<castor::srm::SubRequest*>::const_iterator it = req->subRequests().begin();
       it != req->subRequests().end();
       it++) {
    if (0 != (*it)->userFile()) delete (*it)->userFile();
    (*it)->setUserFile(0);
    (*it)->setRequest(0);
    delete *it;
  }
  req->subRequests().clear();
  if (0 != req->srmUser()) delete req->srmUser();
  delete req;
}


//------------------------------------------------------------------------------
// logRequestFailure
//------------------------------------------------------------------------------
void castor::srm::daemon::RecallPollerThread::logRequestFailure
(castor::srm::StageRequest* req,
 const std::string message,
 const int level,
 const Cuuid_t &myId)
  throw() {
  // Loop over the subrequests
  for (std::vector<castor::srm::SubRequest*>::const_iterator it = req->subRequests().begin();
       it != req->subRequests().end();
       it++) {
    // Issue a log message for the subrequest
    std::list<castor::log::Param> snafu = {
      castor::log::Param("NSFILEID", (*it)->userFile()->fileId()),
      castor::log::Param("REQID", myId),
      castor::log::Param("RequestToken", req->id()),
      castor::log::Param("Type", "STAGEIN"),
      castor::log::Param("Failure", message),
      castor::log::Param("SvcClass", req->svcClass()),
      castor::log::Param("DN", req->srmUser()->dn()),
      castor::log::Param("VO", req->srmUser()->vo()),
      castor::log::Param("uid", req->euid()),
      castor::log::Param("gid", req->egid()),
      castor::log::Param("ElapsedTime", time(NULL)-req->creationTime())
    };
    // "Processing complete with failures" message
    castor::log::write(level, "Processing complete with failures", snafu);
  }
}
