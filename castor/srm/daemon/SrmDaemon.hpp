/******************************************************************************
 *                      SrmDaemon.hpp
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Deamon part of the SRM software suite.
 * Handles the asynchronous requests.
 *
 * @author castor-dev team
 *****************************************************************************/

#ifndef SRM_DAEMON_SRMDAEMON_HPP
#define SRM_DAEMON_SRMDAEMON_HPP 1

#include <iostream>
#include <string>
#include "castor/server/MultiThreadedDaemon.hpp"

namespace castor {
 namespace srm {

 namespace daemon {
  
  /**
   * CASTOR SRM2 backend daemon.
   */
  class SrmDaemon : public castor::server::MultiThreadedDaemon {

  public:

    /**
     * Constructor
     * @param stdOut Stream representing standard out.
     * @param stdErr Stream representing standard error.
     */
    SrmDaemon(std::ostream &stdOut, std::ostream &stdErr);

    /**
     * Destructor
     */
    virtual ~SrmDaemon() throw () {};

  protected:
   
    virtual void help(std::string programName);

  };

 } // end of namespace daemon

} } /* end of namespace castor/srm */



#endif // SRM_DAEMON_SRMDAEMON_HPP
