/******************************************************************************
 *                      OraSrmDaemonSvc.cpp
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Implementation of the ISrmDaemonSvc interface for oracle
 *
 * @author castor-dev team
 *****************************************************************************/

// Include Files
#include "castor/srm/daemon/OraSrmDaemonSvc.hpp"
#include "castor/SvcFactory.hpp"
#include "castor/Constants.hpp"
#include "castor/srm/StageRequest.hpp"
#include "castor/srm/SrmUser.hpp"
#include "castor/srm/SubRequest.hpp"
#include "castor/srm/UserFile.hpp"
#include "castor/srm/Constants.hpp"
#include "castor/srm/SrmConstants.hpp"
#include "castor/exception/Exception.hpp"
#include "castor/exception/Exception.hpp"
#include "castor/log/log.hpp"
#include <string>
#include "occi.h"


// -----------------------------------------------------------------------
// Instantiation of a static factory class
// -----------------------------------------------------------------------
static castor::SvcFactory<castor::srm::daemon::OraSrmDaemonSvc>* s_factoryOraSrmDaemonSvc =
  new castor::SvcFactory<castor::srm::daemon::OraSrmDaemonSvc>();

//------------------------------------------------------------------------------
// Static constants initialization
//------------------------------------------------------------------------------

/// SQL statement for getting turl from request and castor file name
const std::string castor::srm::daemon::OraSrmDaemonSvc::s_getTURL =
  "SELECT turl FROM subrequest WHERE request=:1 AND castorfilename=:2";

/// SQL statement for stageRequestToDo
const std::string castor::srm::daemon::OraSrmDaemonSvc::s_stageRequestToDo =
  "BEGIN stageRequestToDo(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17,:18); END;";

/// SQL statement for requestToPoll
const std::string castor::srm::daemon::OraSrmDaemonSvc::s_requestToPoll =
  "BEGIN requestToPoll(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17,:18); END;";

/// SQL statement for failRequest
const std::string castor::srm::daemon::OraSrmDaemonSvc::s_failRequest =
  "BEGIN failRequest(:1,:2,:3); END;";

/// SQL statement for failSubRequest
const std::string castor::srm::daemon::OraSrmDaemonSvc::s_failSubRequest =
  "BEGIN failSubRequest(:1,:2,:3); END;";

/// SQL statement for succeedSubRequest
const std::string castor::srm::daemon::OraSrmDaemonSvc::s_succeedSubRequest =
  "BEGIN succeedSubRequest(:1); END;";

/// SQL statement for requestToBePolled
const std::string castor::srm::daemon::OraSrmDaemonSvc::s_requestToBePolled =
  "BEGIN requestToBePolled(:1,:2,:3); END;";

/// SQL statement for prepareCastorReq
const std::string castor::srm::daemon::OraSrmDaemonSvc::s_prepareCastorReq =
  "BEGIN prepareCastorReq(:1,:2,:3); END;";

/// SQL statement for fillFileId
const std::string castor::srm::daemon::OraSrmDaemonSvc::s_fillFileId =
  "BEGIN fillFileId(:1,:2,:3); END;";

/// SQL statement for checkAbortedSubReqs
const std::string castor::srm::daemon::OraSrmDaemonSvc::s_checkAbortedSubReqs =
  "BEGIN checkAbortedSubReqs(:1,:2); END;";

/// SQL statement for getSubRequestById
const std::string castor::srm::daemon::OraSrmDaemonSvc::s_getSubRequestById =
  "BEGIN getSubRequestById(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17,:18,:19,:20,:21); END;";

/// SQL statement for setTurl
const std::string castor::srm::daemon::OraSrmDaemonSvc::s_setTurl =
  "BEGIN setTurl(:1,:2); END;";


// -----------------------------------------------------------------------
// OraSrmDaemonSvc
// -----------------------------------------------------------------------
castor::srm::daemon::OraSrmDaemonSvc::OraSrmDaemonSvc(const std::string name) :
  castor::db::ora::OraCommonSvc(name),
  m_getTURLStatement(0),
  m_stageRequestToDoStatement(0),
  m_requestToPollStatement(0),
  m_failRequestStatement(0),
  m_failSubRequestStatement(0),
  m_succeedSubRequestStatement(0),
  m_requestToBePolledStatement(0),
  m_prepareCastorReqStatement(0),
  m_fillFileIdStatement(0),
  m_checkAbortedSubReqsStatement(0),
  m_getSubRequestByIdStatement(0),
  m_setTurlStatement(0) {}

// -----------------------------------------------------------------------
// ~OraSrmDaemonSvc
// -----------------------------------------------------------------------
castor::srm::daemon::OraSrmDaemonSvc::~OraSrmDaemonSvc() throw() {
  reset();
}

// -----------------------------------------------------------------------
// id
// -----------------------------------------------------------------------
unsigned int castor::srm::daemon::OraSrmDaemonSvc::id() const {
  return ID();
}

// -----------------------------------------------------------------------
// ID
// -----------------------------------------------------------------------
unsigned int castor::srm::daemon::OraSrmDaemonSvc::ID() {
  return castor::SVC_ORASRMDAEMONSVC;
}

//------------------------------------------------------------------------------
// reset
//------------------------------------------------------------------------------
void castor::srm::daemon::OraSrmDaemonSvc::reset() throw() {
  //Here we attempt to delete the statements correctly
  // If something goes wrong, we just ignore it
  OraCommonSvc::reset();
  try {
    if (m_getTURLStatement) deleteStatement(m_getTURLStatement);
    if (m_stageRequestToDoStatement) deleteStatement(m_stageRequestToDoStatement);
    if (m_requestToPollStatement) deleteStatement(m_requestToPollStatement);
    if (m_failRequestStatement) deleteStatement(m_failRequestStatement);
    if (m_failSubRequestStatement) deleteStatement(m_failSubRequestStatement);
    if (m_succeedSubRequestStatement) deleteStatement(m_succeedSubRequestStatement);
    if (m_requestToBePolledStatement) deleteStatement(m_requestToBePolledStatement);
    if (m_prepareCastorReqStatement) deleteStatement(m_prepareCastorReqStatement);
    if (m_fillFileIdStatement) deleteStatement(m_fillFileIdStatement);
    if (m_checkAbortedSubReqsStatement) deleteStatement(m_checkAbortedSubReqsStatement);
    if (m_getSubRequestByIdStatement) deleteStatement(m_getSubRequestByIdStatement);
    if (m_setTurlStatement) deleteStatement(m_setTurlStatement);
  } catch (castor::exception::Exception& ignored) {};
  // Now reset all pointers to 0
  m_getTURLStatement = 0;
  m_stageRequestToDoStatement = 0;
  m_requestToPollStatement = 0;
  m_failRequestStatement = 0;
  m_failSubRequestStatement = 0;
  m_succeedSubRequestStatement = 0;
  m_requestToBePolledStatement = 0;
  m_prepareCastorReqStatement = 0;
  m_fillFileIdStatement = 0;
  m_checkAbortedSubReqsStatement = 0;
  m_getSubRequestByIdStatement = 0;
  m_setTurlStatement = 0;
}

// -----------------------------------------------------------------------
// getTURL
// -----------------------------------------------------------------------
std::string castor::srm::daemon::OraSrmDaemonSvc::getTURL(std::string s_id, std::string s_cfn)
  throw (castor::exception::Exception) {
  std::string  rtn;
  try {
    if ( 0 == m_getTURLStatement) {
      m_getTURLStatement = createStatement (s_getTURL);
    }
    m_getTURLStatement->setDouble(1, atoll(const_cast<char*>(s_id.c_str())));
    m_getTURLStatement->setString(2, s_cfn);

    oracle::occi::ResultSet *rset = m_getTURLStatement->executeQuery();
    if ( rset->next() ) {
      rtn = rset->getString(1);
    }
    delete rset;
    return rtn;
  }
  catch (oracle::occi::SQLException& e) {
    handleException(e);
    castor::exception::Exception ex;
    ex.getMessage()
      << "Error caught in castor::srm::getTURL "
      << e.getMessage();
    //std::cerr << ex.getMessage().str().c_str() << std::endl;
    throw ex;
  }
  return rtn;
}

// -----------------------------------------------------------------------
// buildRequest
// -----------------------------------------------------------------------
castor::srm::StageRequest*
castor::srm::daemon::OraSrmDaemonSvc::buildRequest (oracle::occi::Statement *stmt)
  throw(oracle::occi::SQLException) {
  // check whether we got something
  u_signed64 id = (u_signed64)stmt->getDouble(1);
  if (id == 0) {
    return 0;   // no result found
  }
  // Build returned objects from the output
  castor::srm::StageRequest* req = new castor::srm::StageRequest();
  req->setId(id);
  req->setOverwriteOption(stmt->getInt(2));
  req->setProtocol(stmt->getString(3));
  req->setEuid(stmt->getInt(4));
  req->setEgid(stmt->getInt(5));
  req->setRhHost(stmt->getString(6));
  req->setRhPort(stmt->getInt(7));
  req->setCreationTime((u_signed64)stmt->getDouble(8));
  req->setSvcClass(stmt->getString(9));
  req->setSrmRequestId(stmt->getString(10));
  req->setCastorReqId(stmt->getString(11));
  req->setRequestType((RequestType)stmt->getInt(12));
  req->setLastCheck((u_signed64)stmt->getDouble(16));
  req->setNextCheck((u_signed64)stmt->getDouble(17));
  req->setExpirationInterval((u_signed64)stmt->getDouble(18));
  castor::srm::SrmUser* user = new castor::srm::SrmUser();
  user->setVo(stmt->getString(13));
  user->setDn(stmt->getString(14));
  req->setSrmUser(user);
  user->addRequests(req);
  // Go over subrequests
  oracle::occi::ResultSet *rs = stmt->getCursor(15);
  oracle::occi::ResultSet::Status status = rs->next();
  while (status == oracle::occi::ResultSet::DATA_AVAILABLE) {
    castor::srm::SubRequest* sr = new castor::srm::SubRequest();
    sr->setId((u_signed64)rs->getDouble(1));
    sr->setCastorFileName(rs->getString(2));
    sr->setStatus(castor::srm::SUBREQUEST_INPROGRESS);
    req->addSubRequests(sr);
    castor::srm::UserFile* uf = new castor::srm::UserFile();
    uf->setId((u_signed64)rs->getDouble(3));
    uf->setFileId((u_signed64)rs->getDouble(4));
    sr->setUserFile(uf);
    sr->setRequest(req);
    status = rs->next();
  }
  stmt->closeResultSet(rs);
  return req;
}

// -----------------------------------------------------------------------
// stageRequestToDo
// -----------------------------------------------------------------------
castor::srm::StageRequest*
castor::srm::daemon::OraSrmDaemonSvc::stageRequestToDo()
  throw() {
  try {
    // Check whether the statement is ok
    if (0 == m_stageRequestToDoStatement) {
      m_stageRequestToDoStatement = createStatement(s_stageRequestToDo);
      m_stageRequestToDoStatement->registerOutParam(1, oracle::occi::OCCIDOUBLE);
      m_stageRequestToDoStatement->registerOutParam(2, oracle::occi::OCCIINT);
      m_stageRequestToDoStatement->registerOutParam(3, oracle::occi::OCCISTRING, 2048);
      m_stageRequestToDoStatement->registerOutParam(4, oracle::occi::OCCIINT);
      m_stageRequestToDoStatement->registerOutParam(5, oracle::occi::OCCIINT);
      m_stageRequestToDoStatement->registerOutParam(6, oracle::occi::OCCISTRING, 2048);
      m_stageRequestToDoStatement->registerOutParam(7, oracle::occi::OCCIINT);
      m_stageRequestToDoStatement->registerOutParam(8, oracle::occi::OCCIDOUBLE);
      m_stageRequestToDoStatement->registerOutParam(9, oracle::occi::OCCISTRING, 2048);
      m_stageRequestToDoStatement->registerOutParam(10, oracle::occi::OCCISTRING, 2048);
      m_stageRequestToDoStatement->registerOutParam(11, oracle::occi::OCCISTRING, 2048);
      m_stageRequestToDoStatement->registerOutParam(12, oracle::occi::OCCIINT);
      m_stageRequestToDoStatement->registerOutParam(13, oracle::occi::OCCISTRING, 2048);
      m_stageRequestToDoStatement->registerOutParam(14, oracle::occi::OCCISTRING, 2048);
      m_stageRequestToDoStatement->registerOutParam(15, oracle::occi::OCCICURSOR);
      m_stageRequestToDoStatement->registerOutParam(16, oracle::occi::OCCIDOUBLE);
      m_stageRequestToDoStatement->registerOutParam(17, oracle::occi::OCCIDOUBLE);
      m_stageRequestToDoStatement->registerOutParam(18, oracle::occi::OCCIDOUBLE);
      m_stageRequestToDoStatement->setAutoCommit(true);
    }
    // execute the statement
    m_stageRequestToDoStatement->execute();
    // gather result and return
    return buildRequest(m_stageRequestToDoStatement);
  } catch (oracle::occi::SQLException& e) {
    handleException(e);
    // "Exception caught" message
    std::list<castor::log::Param> params = {
      castor::log::Param("ErrorMessage", "stageRequestToDo Caught fatal exception"),
      castor::log::Param("Details", e.getMessage()) };
    castor::log::write(LOG_ERR, "Exception caught", params);
    return 0;
  }
  catch (castor::exception::Exception& e) {
    // Something is wrong with the db connection, just log it
    // "Exception caught" message
    std::list<castor::log::Param> params = {
      castor::log::Param("Details", e.getMessage().str())
    };
    castor::log::write(LOG_ERR, "Exception caught", params);
    return 0;
  }    
}


// -----------------------------------------------------------------------
// requestToPoll
// -----------------------------------------------------------------------
castor::srm::StageRequest*
castor::srm::daemon::OraSrmDaemonSvc::requestToPoll()
  throw() {
  try {
    // Check whether the statement is ok
    if (0 == m_requestToPollStatement) {
      m_requestToPollStatement = createStatement(s_requestToPoll);
      m_requestToPollStatement->registerOutParam(1, oracle::occi::OCCIDOUBLE);
      m_requestToPollStatement->registerOutParam(2, oracle::occi::OCCIINT);
      m_requestToPollStatement->registerOutParam(3, oracle::occi::OCCISTRING, 2048);
      m_requestToPollStatement->registerOutParam(4, oracle::occi::OCCIINT);
      m_requestToPollStatement->registerOutParam(5, oracle::occi::OCCIINT);
      m_requestToPollStatement->registerOutParam(6, oracle::occi::OCCISTRING, 2048);
      m_requestToPollStatement->registerOutParam(7, oracle::occi::OCCIINT);
      m_requestToPollStatement->registerOutParam(8, oracle::occi::OCCIDOUBLE);
      m_requestToPollStatement->registerOutParam(9, oracle::occi::OCCISTRING, 2048);
      m_requestToPollStatement->registerOutParam(10, oracle::occi::OCCISTRING, 2048);
      m_requestToPollStatement->registerOutParam(11, oracle::occi::OCCISTRING, 2048);
      m_requestToPollStatement->registerOutParam(12, oracle::occi::OCCIINT);
      m_requestToPollStatement->registerOutParam(13, oracle::occi::OCCISTRING, 2048);
      m_requestToPollStatement->registerOutParam(14, oracle::occi::OCCISTRING, 2048);
      m_requestToPollStatement->registerOutParam(15, oracle::occi::OCCICURSOR);
      m_requestToPollStatement->registerOutParam(16, oracle::occi::OCCIDOUBLE);
      m_requestToPollStatement->registerOutParam(17, oracle::occi::OCCIDOUBLE);
      m_requestToPollStatement->registerOutParam(18, oracle::occi::OCCIDOUBLE);
      m_requestToPollStatement->setAutoCommit(true);
    }
    // execute the statement
    m_requestToPollStatement->execute();
    // gather result and return
    return buildRequest(m_requestToPollStatement);
  } catch (oracle::occi::SQLException& e) {
    handleException(e);
    // "Exception caught" message
    std::list<castor::log::Param> params = {
      castor::log::Param("ErrorMessage", "requestToPoll Caught fatal exception"),
      castor::log::Param("Details", e.getMessage()) };
    castor::log::write(LOG_ERR, "Exception caught", params);
    return 0;
  }
  catch (castor::exception::Exception& e) {
    // Something is wrong with the db connection, just log it
    // "Exception caught" message
    std::list<castor::log::Param> params = {
      castor::log::Param("Details", e.getMessage().str())
    };
    castor::log::write(LOG_ERR, "Exception caught", params);
    return 0;
  }    
}


// -----------------------------------------------------------------------
// failRequest
// -----------------------------------------------------------------------
void castor::srm::daemon::OraSrmDaemonSvc::failRequest(const castor::srm::StageRequest* req,
                                               const int errorCode,
                                               const std::string errorMessage,
                                               const Cuuid_t& reqId) throw() {
  try {
    // Check whether the statement is ok
    if (0 == m_failRequestStatement) {
      m_failRequestStatement = createStatement(s_failRequest);
      m_failRequestStatement->setAutoCommit(true);
    }
    // execute the statement
    m_failRequestStatement->setDouble(1, req->id());
    m_failRequestStatement->setInt(2, errorCode);
    m_failRequestStatement->setString(3, errorMessage);
    m_failRequestStatement->execute();
  } catch (oracle::occi::SQLException& e) {
    handleException(e);
    // "Exception caught" message
    std::list<castor::log::Param> params = {
      castor::log::Param("REQID", reqId),
      castor::log::Param("ErrorMessage", "failRequest Caught fatal exception"),
      castor::log::Param("Details", e.getMessage()) };
    castor::log::write(LOG_ERR, "Exception caught", params);
  }
}


// -----------------------------------------------------------------------
// failSubRequest
// -----------------------------------------------------------------------
void castor::srm::daemon::OraSrmDaemonSvc::failSubRequest(const castor::srm::SubRequest* subreq,
                                                  const int errorCode,
                                                  const std::string errorMessage,
                                                  const Cuuid_t& reqId) throw() {
  try {
    // Check whether the statement is ok
    if (0 == m_failSubRequestStatement) {
      m_failSubRequestStatement = createStatement(s_failSubRequest);
      m_failSubRequestStatement->setAutoCommit(true);
    }
    // execute the statement
    m_failSubRequestStatement->setDouble(1, subreq->id());
    m_failSubRequestStatement->setInt(2, errorCode);
    m_failSubRequestStatement->setString(3, errorMessage);
    m_failSubRequestStatement->execute();
  } catch (oracle::occi::SQLException& e) {
    handleException(e);
    // "Exception caught" message
    std::list<castor::log::Param> params = {
      castor::log::Param("REQID", reqId),
      castor::log::Param("ErrorMessage", "failSubRequest Caught fatal exception"),
      castor::log::Param("Details", e.getMessage()) };
    castor::log::write(LOG_ERR, "Exception caught", params);
  }
}


// -----------------------------------------------------------------------
// succeedSubRequest
// -----------------------------------------------------------------------
void castor::srm::daemon::OraSrmDaemonSvc::succeedSubRequest(const castor::srm::SubRequest* subreq,
                                                     const Cuuid_t& reqId) throw() {
  try {
    // Check whether the statement is ok
    if (0 == m_succeedSubRequestStatement) {
      m_succeedSubRequestStatement = createStatement(s_succeedSubRequest);
      m_succeedSubRequestStatement->setAutoCommit(true);
    }
    // execute the statement
    m_succeedSubRequestStatement->setDouble(1, subreq->id());
    m_succeedSubRequestStatement->execute();
  } catch (oracle::occi::SQLException& e) {
    handleException(e);
    // "Exception caught" message
    std::list<castor::log::Param> params = {
      castor::log::Param("REQID", reqId),
      castor::log::Param("ErrorMessage", "succeedSubRequest Caught fatal exception"),
      castor::log::Param("Details", e.getMessage()) };
    castor::log::write(LOG_ERR, "Exception caught", params);
  }
}


// -----------------------------------------------------------------------
// requestToBePolled
// -----------------------------------------------------------------------
void castor::srm::daemon::OraSrmDaemonSvc::requestToBePolled(const castor::srm::StageRequest* req,
                                                     const u_signed64 interval,
                                                     const Cuuid_t& reqId) throw() {
  try {
    // Check whether the statement is ok
    if (0 == m_requestToBePolledStatement) {
      m_requestToBePolledStatement = createStatement(s_requestToBePolled);
      m_requestToBePolledStatement->setAutoCommit(true);
    }
    // execute the statement
    m_requestToBePolledStatement->setDouble(1, req->id());
    m_requestToBePolledStatement->setString(2, req->castorReqId());
    m_requestToBePolledStatement->setInt(3, interval);
    m_requestToBePolledStatement->execute();
  } catch (oracle::occi::SQLException& e) {
    handleException(e);
    // "Exception caught" message
    std::list<castor::log::Param> params = {
      castor::log::Param("REQID", reqId),
      castor::log::Param("ErrorMessage", "requestToBePolled Caught fatal exception"),
      castor::log::Param("Details", e.getMessage()) };
    castor::log::write(LOG_ERR, "Exception caught", params);
  }
}


// -----------------------------------------------------------------------
// prepareCastorReq
// -----------------------------------------------------------------------
std::string castor::srm::daemon::OraSrmDaemonSvc::prepareCastorReq
(std::vector<u_signed64>& subreqIds,
 const std::string& svcClass,
 const Cuuid_t& reqId) throw() {
  // buffers for the oracle call
  ub2 *lens = 0;
  unsigned char (*buffer)[21] = 0;
  try {
    // Check whether the statement is ok
    if (0 == m_prepareCastorReqStatement) {
      m_prepareCastorReqStatement = createStatement(s_prepareCastorReq);
      m_prepareCastorReqStatement->registerOutParam(3, oracle::occi::OCCISTRING, 40);
    }
    // deal with the list of SubRequest ids to pass
    lens = (ub2 *) malloc(sizeof(ub2)*subreqIds.size());
    buffer = (unsigned char(*)[21]) calloc(subreqIds.size() * 21, sizeof(unsigned char));
    for (unsigned int i = 0; i < subreqIds.size(); i++) {
      oracle::occi::Number n = (double)(subreqIds[i]);
      oracle::occi::Bytes b = n.toBytes();
      b.getBytes(buffer[i],b.length());
      lens[i] = b.length();
    }
    ub4 unused = subreqIds.size();
    m_prepareCastorReqStatement->setDataBufferArray
      (1, buffer, oracle::occi::OCCI_SQLT_NUM, subreqIds.size(), &unused, 21, lens);
    m_prepareCastorReqStatement->setString(2, svcClass);
    // execute the statement
    m_prepareCastorReqStatement->execute();
    // retrieve the UUID that has been assigned to the subrequests
    return m_prepareCastorReqStatement->getString(3);
  } catch (oracle::occi::SQLException& e) {
    handleException(e);
    // "Exception caught" message
    std::list<castor::log::Param> params = {
      castor::log::Param("REQID", reqId),
      castor::log::Param("ErrorMessage", "m_prepareCastorReqStatement Caught fatal exception"),
      castor::log::Param("Details", e.getMessage()) };
    castor::log::write(LOG_ERR, "Exception caught", params);
    return "";
  }
}


// -----------------------------------------------------------------------
// fillFileId
// -----------------------------------------------------------------------
void castor::srm::daemon::OraSrmDaemonSvc::fillFileId
(const castor::srm::UserFile* userFile,
 const Cuuid_t& reqId) throw() {
  try {
    // Check whether the statement is ok
    if (0 == m_fillFileIdStatement) {
      m_fillFileIdStatement = createStatement(s_fillFileId);
      m_fillFileIdStatement->setAutoCommit(true);
    }
    // execute the statement
    m_fillFileIdStatement->setDouble(1, userFile->id());
    m_fillFileIdStatement->setDouble(2, userFile->fileId());
    m_fillFileIdStatement->setString(3, userFile->nsHost());
    m_fillFileIdStatement->execute();
  } catch (oracle::occi::SQLException& e) {
    handleException(e);
    // "Exception caught" message
    std::list<castor::log::Param> params = {
      castor::log::Param("REQID", reqId),
      castor::log::Param("ErrorMessage", "fillFileId Caught fatal exception"),
      castor::log::Param("Details", e.getMessage()) };
    castor::log::write(LOG_ERR, "Exception caught", params);
  }
}


// -----------------------------------------------------------------------
// checkAbortedSubReqs
// -----------------------------------------------------------------------
void castor::srm::daemon::OraSrmDaemonSvc::checkAbortedSubReqs
(castor::srm::StageRequest* req,
 const Cuuid_t& reqId) throw() {
  // do not call oracle if not needed
  unsigned int nb = req->subRequests().size();
  if (0 == nb) return;
  // buffers for the oracle call
  ub2 *lens = 0;
  unsigned char (*buffer)[21] = 0;
  try {
    // Check whether the statement is ok
    if (0 == m_checkAbortedSubReqsStatement) {
      m_checkAbortedSubReqsStatement = createStatement(s_checkAbortedSubReqs);
      m_checkAbortedSubReqsStatement->registerOutParam(2, oracle::occi::OCCICURSOR);
    }
    // Deal with the list of SubRequests to check
    lens = (ub2 *) malloc(sizeof(ub2)*nb);
    buffer = (unsigned char(*)[21]) calloc(nb * 21, sizeof(unsigned char));
    for (unsigned int i = 0; i < nb; i++) {
      oracle::occi::Number n = (double)(req->subRequests()[i]->id());
      oracle::occi::Bytes b = n.toBytes();
      b.getBytes(buffer[i],b.length());
      lens[i] = b.length();
    }
    ub4 unused = nb;
    m_checkAbortedSubReqsStatement->setDataBufferArray
      (1, buffer, oracle::occi::OCCI_SQLT_NUM,
       nb, &unused, 21, lens);
    // execute the statement
    m_checkAbortedSubReqsStatement->executeUpdate();
    // get the result, that is a cursor on the subrequest that were aborted
    oracle::occi::ResultSet *rset =
      m_checkAbortedSubReqsStatement->getCursor(2);
    // Loop over the subrequests returned and detach them form the request
    while (oracle::occi::ResultSet::END_OF_FETCH != rset->next()) {
      u_signed64 id = (u_signed64)rset->getDouble(1);
      // find the subrequest
      castor::srm::SubRequest* sr = 0;
      for (std::vector<castor::srm::SubRequest*>::const_iterator it = req->subRequests().begin();
           it != req->subRequests().end();
           it++) {
        if ((*it)->id() == id) {
          sr = *it;
          break;
        }
      }
      // remove it from the request and cleanup memory
      req->removeSubRequests(sr);
      if (0 != sr->userFile()) delete sr->userFile();
      delete sr;
    }
    m_checkAbortedSubReqsStatement->closeResultSet(rset);
  } catch (oracle::occi::SQLException& e) {
    handleException(e);
    // "Exception caught" message
    std::list<castor::log::Param> params = {
      castor::log::Param("REQID", reqId),
      castor::log::Param("ErrorMessage", "checkAbortedSubReqs Caught fatal exception"),
      castor::log::Param("Details", e.getMessage()) };
    castor::log::write(LOG_ERR, "Exception caught", params);
  }
}


// -----------------------------------------------------------------------
// getSubRequestById
// -----------------------------------------------------------------------
castor::srm::SubRequest*
castor::srm::daemon::OraSrmDaemonSvc::getSubRequestById(const std::string& reqId,
                                                const u_signed64 fileId,
                                                const std::string& fileName)
  throw() {
  try {
    // Check whether the statement is ok
    if (0 == m_getSubRequestByIdStatement) {
      m_getSubRequestByIdStatement = createStatement(s_getSubRequestById);
      m_getSubRequestByIdStatement->registerOutParam(4, oracle::occi::OCCIINT);
      m_getSubRequestByIdStatement->registerOutParam(5, oracle::occi::OCCISTRING, 20);
      m_getSubRequestByIdStatement->registerOutParam(6, oracle::occi::OCCIINT);
      m_getSubRequestByIdStatement->registerOutParam(7, oracle::occi::OCCIINT);
      m_getSubRequestByIdStatement->registerOutParam(8, oracle::occi::OCCISTRING, 2048);
      m_getSubRequestByIdStatement->registerOutParam(9, oracle::occi::OCCIINT);
      m_getSubRequestByIdStatement->registerOutParam(10, oracle::occi::OCCIDOUBLE);
      m_getSubRequestByIdStatement->registerOutParam(11, oracle::occi::OCCISTRING, 2048);
      m_getSubRequestByIdStatement->registerOutParam(12, oracle::occi::OCCIINT);
      m_getSubRequestByIdStatement->registerOutParam(13, oracle::occi::OCCISTRING, 2048);
      m_getSubRequestByIdStatement->registerOutParam(14, oracle::occi::OCCIDOUBLE);
      m_getSubRequestByIdStatement->registerOutParam(15, oracle::occi::OCCIDOUBLE);
      m_getSubRequestByIdStatement->registerOutParam(16, oracle::occi::OCCISTRING, 2048);
      m_getSubRequestByIdStatement->registerOutParam(17, oracle::occi::OCCISTRING, 2048);
      m_getSubRequestByIdStatement->registerOutParam(18, oracle::occi::OCCISTRING, 2048);
      m_getSubRequestByIdStatement->registerOutParam(19, oracle::occi::OCCISTRING, 2048);
      m_getSubRequestByIdStatement->registerOutParam(20, oracle::occi::OCCIDOUBLE);
      m_getSubRequestByIdStatement->registerOutParam(21, oracle::occi::OCCIINT);
    }
    // execute the statement
    m_getSubRequestByIdStatement->setString(1, reqId);
    m_getSubRequestByIdStatement->setDouble(2, fileId);
    m_getSubRequestByIdStatement->setString(3, fileName);
    m_getSubRequestByIdStatement->execute();
    // check whether we got something
    u_signed64 id = (u_signed64)m_getSubRequestByIdStatement->getDouble(14);
    if (id == 0) {
      return 0;   // no result found
    }
    // Check the status of the request: CASTOR may indeed send us 2 answers
    // for the same request e.g. in case the request is canceled just after we
    // were answered for the first time or the user did not transfer any data
    // after having connected to the mover.
    // In such a case, the request will not be INPROGRESS anymore
    // and we should ignore the answer
    if (m_getSubRequestByIdStatement->getInt(21) != SUBREQUEST_INPROGRESS) {
      // "Response received from Castor for a request already in a final state" message
      std::list<castor::log::Param> errParams = {
        castor::log::Param("NSFILEID", fileId),
        castor::log::Param("REQID", m_getSubRequestByIdStatement->getString(13)),
        castor::log::Param("RequestToken",
                           (u_signed64)m_getSubRequestByIdStatement->getDouble(20)),
        castor::log::Param("Type", castor::srm::RequestTypeStrings
                           [m_getSubRequestByIdStatement->getInt(12)]),
        castor::log::Param("SvcClass", m_getSubRequestByIdStatement->getString(11)),
        castor::log::Param("FileName", m_getSubRequestByIdStatement->getString(16)),
        castor::log::Param("uid", m_getSubRequestByIdStatement->getInt(6)),
        castor::log::Param("gid", m_getSubRequestByIdStatement->getInt(7)),
        castor::log::Param("ElapsedTime",
                           time(NULL)-(u_signed64)m_getSubRequestByIdStatement->getDouble(10)),
        castor::log::Param("VO", m_getSubRequestByIdStatement->getString(17)),
        castor::log::Param("DN", m_getSubRequestByIdStatement->getString(18)),
        castor::log::Param("SubReqStatus", castor::srm::SubRequestStatusStrings
                           [m_getSubRequestByIdStatement->getInt(21)])
      };
      castor::log::write(LOG_INFO, "Response received from Castor for a request already in a final state", errParams);
      // do as if nothing was received
      return 0;
    }
    // gather result and return
    castor::srm::StageRequest* req = new castor::srm::StageRequest();
    req->setOverwriteOption(m_getSubRequestByIdStatement->getInt(4));
    req->setProtocol(m_getSubRequestByIdStatement->getString(5));
    req->setEuid(m_getSubRequestByIdStatement->getInt(6));
    req->setEgid(m_getSubRequestByIdStatement->getInt(7));
    req->setRhHost(m_getSubRequestByIdStatement->getString(8));
    req->setRhPort(m_getSubRequestByIdStatement->getInt(9));
    req->setCreationTime((u_signed64)m_getSubRequestByIdStatement->getDouble(10));
    req->setSvcClass(m_getSubRequestByIdStatement->getString(11));
    req->setRequestType((RequestType)m_getSubRequestByIdStatement->getInt(12));
    req->setSrmRequestId(m_getSubRequestByIdStatement->getString(13));
    req->setId((u_signed64)m_getSubRequestByIdStatement->getDouble(20));
    castor::srm::SrmUser* su = new castor::srm::SrmUser();
    su->setVo(m_getSubRequestByIdStatement->getString(17));
    su->setDn(m_getSubRequestByIdStatement->getString(18));
    req->setSrmUser(su);
    su->addRequests(req);
    castor::srm::SubRequest* sr = new castor::srm::SubRequest();
    sr->setId(id);
    sr->setCastorFileName(m_getSubRequestByIdStatement->getString(16));
    req->addSubRequests(sr);
    castor::srm::UserFile* uf = new castor::srm::UserFile();
    uf->setId((u_signed64)m_getSubRequestByIdStatement->getDouble(15));
    uf->setFileId(fileId);
    uf->setNsHost(m_getSubRequestByIdStatement->getString(19));
    sr->setUserFile(uf);
    sr->setRequest(req);
    return sr;
  } catch (oracle::occi::SQLException& e) {
    handleException(e);
    // "Exception caught" message. "No data found" is benign, just log a warning.
    if (e.getErrorCode() == 1403) {
      std::list<castor::log::Param> params = {
        castor::log::Param("NSFILEID", fileId),
        castor::log::Param("ErrorMessage", "getSubRequestById: request not found, ignoring"),
        castor::log::Param("CastorRequestId", reqId),
        castor::log::Param("FileName", fileName),
      };
      castor::log::write(LOG_WARNING, "Exception caught", params);
    } else {
      std::list<castor::log::Param> params = {
        castor::log::Param("NSFILEID", fileId),
        castor::log::Param("ErrorMessage", "getSubRequestById caught fatal exception"),
        castor::log::Param("ErrorCode", e.getErrorCode()),
        castor::log::Param("Details", e.getMessage()),
        castor::log::Param("CastorRequestId", reqId),
        castor::log::Param("FileName", fileName),
      };
      castor::log::write(LOG_ERR, "Exception caught", params);
    }
    return 0;
  }
}


// -----------------------------------------------------------------------
// setTurl
// -----------------------------------------------------------------------
void castor::srm::daemon::OraSrmDaemonSvc::setTurl(const castor::srm::SubRequest* subreq,
                                           const Cuuid_t& reqId) throw() {
  try {
    // Check whether the statement is ok
    if (0 == m_setTurlStatement) {
      m_setTurlStatement = createStatement(s_setTurl);
      m_setTurlStatement->setAutoCommit(true);
    }
    // execute the statement
    m_setTurlStatement->setDouble(1, subreq->id());
    m_setTurlStatement->setString(2, subreq->turl());
    m_setTurlStatement->execute();
  } catch (oracle::occi::SQLException& e) {
    handleException(e);
    // "Exception caught" message
    std::list<castor::log::Param> params = {
      castor::log::Param("REQID", reqId),
      castor::log::Param("ErrorMessage", "setTurl caught fatal exception"),
      castor::log::Param("Details", e.getMessage()) };
    castor::log::write(LOG_ERR, "Exception caught", params);
  }
}

// -----------------------------------------------------------------------
// commit
// -----------------------------------------------------------------------
void castor::srm::daemon::OraSrmDaemonSvc::commit(const Cuuid_t& reqId) throw() {
  try {
    cnvSvc()->commit();
  } catch (castor::exception::Exception& e) {
    // "Exception caught" message
    std::list<castor::log::Param> params = {
      castor::log::Param("REQID", reqId),
      castor::log::Param("ErrorMessage", "commit caught fatal exception"),
      castor::log::Param("Details", e.getMessage().str()) };
    castor::log::write(LOG_ERR, "Exception caught", params);
  }
}
