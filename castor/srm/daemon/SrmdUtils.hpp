/******************************************************************************
 *                      SrmdUtils.hpp
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * A static class with utilities for the srmDaemon
 *
 * @author Giuseppe Lo Presti
 *****************************************************************************/

#ifndef SRM_SERVER_SRMDUTILS_HPP
#define SRM_SERVER_SRMDUTILS_HPP

#include <string>
#include <list>
#include <map>
#include <Cuuid.h>
#include <stager_client_api.h>
#include <Cns_api.h>
#include <serrno.h>
#include <castor/exception/Exception.hpp>
#include <castor/srm/CastorClient.hpp>

namespace castor {
 namespace srm {

  // Forward declaration
  class StageRequest;
  class SubRequest;
  class CastorClient;

  namespace daemon {

    // Forward declaration
    class ISrmDaemonSvc;

    namespace utils {

      /// little struct handling information on the location of a file
      struct FileLocation {
        std::string svcClass;
        std::string diskServer;
      };

      /// typedef for a map from subrequest to a pair SvcClass, DiskServer
      typedef std::map<srm::SubRequest*, FileLocation> SvcClassMap;

      /**
       * Builds a TURL for a given subrequest, updates the DB and logs
       * both success and failures. Note that no exception is raised,
       * failures are only logged
       * @param subreq the concerned subrequest
       * @param host the host
       * @param port the port
       * @param path the path. Can be filename, uuid or anything else, depending
       * on the protocol
       * @param svcClass the service class
       * @param srmdSvc the SrmDaemon service to be used
       * @param myId the uuid of the request, as a Cuuid_t struct, for logging purposes
       * @param nsId a fileId struct, for logging purposes
       */
      void buildTUrl (srm::SubRequest* subreq,
                      const std::string host,
                      const int port,
                      const std::string path,
                      const std::string svcClass,
                      castor::srm::daemon::ISrmDaemonSvc* srmdSvc,
                      const Cuuid_t &myId,
                      struct Cns_fileid* nsId)
        throw();

      /**
       * handles the case of staged (or canbemigr) files where we
       * can get the Turl, or issue a Get request.
       * This method handles the building of the Turls but only
       * lists the subrequests for which a Get request should be
       * issued.
       * @param subreq the subrequest concerned
       * @param svcClass the service class to use
       * @param diskServer the diskServer on which the file resides
       * @param srmdSvc the SrmDaemon service to be used
       * @param myId the Cuuid to be used for logging
       * @param getSRs a list in which subrequests needed a GET
       * will be added
       */
      void handleStagedFile(srm::SubRequest* subreq,
                            const std::string& svcClass,
                            const std::string& diskServer,
                            castor::srm::daemon::ISrmDaemonSvc* srmdSvc,
                            const Cuuid_t &myId,
                            std::list<srm::SubRequest*>& getSRs)
        throw();

      /**
       * sends a get request to castor for all files given
       * @param subreqStart an iterator on the first subreq
       * @param subreqEnd an iterator on the last+1 subreq
       * @param svcClass the service class to use
       * @param srmdSvc the SrmDaemon service to be used
       * @param myId the Cuuid to be used for logging
       * @param castorClient the CastorClient to be used
       */
      void sendGetRequest
      (const std::list<srm::SubRequest*>::iterator& subreqStart,
       const std::list<srm::SubRequest*>::iterator& subreqEnd,
       const std::string& svcClass,
       castor::srm::daemon::ISrmDaemonSvc* srmdSvc,
       const Cuuid_t &myId,
       castor::srm::CastorClient& castorClient)
        throw();

      /**
       * builds a map of fileids for a list of files corresponding to
       * the subrequests within the 2 given iterators
       * @param subreqStart an iterator on the first subreq
       * @param subreqEnd an iterator on the last+1 subreq
       * @return the map of filenames/fileids
       */
      template <class iterator> static FileIdMap buildFileIdMap
      (iterator subreqStart, iterator subreqEnd) throw();

      /**
       * builds a map of fileids for a list of files corresponding to
       * the subrequests of a given request
       * @req the request concerned
       * @return the map of filenames/fileids
       */
      FileIdMap buildFileIdMap(srm::StageRequest* req) throw();

      /**
       * read a list of service classes from the config file for a given vo.
       * In case of error, the error is logged and 0 is returned in nSvcClasses.
       * @param vo the vo tp consider
       * @param svcClassList an array to be allocated and to fill with service classes
       * @param nSvcClasses number of items returned in svcClassList
       * @param myId the uuid to be used for logging purposes
       */
      void listSvcClassForVo(const std::string& vo,
                             char**& svcClassList,
                             int& nSvcClasses,
                             const Cuuid_t &myId)
        throw();

      /**
       * get default service class for a given vo
       * @param vo the concerned vo
       * @param myId the uuid of the request, as a Cuuid_t struct, for logging purposes
       * @result the service class name
       */
      std::string getDefaultSvcClass(const std::string vo,
                                     const Cuuid_t &myId) throw();
      /**
       * get ordered list of possible service classes for a given vo
       * @param vo the concerned vo
       * @param myId the uuid of the request, as a Cuuid_t struct
       * @result a vector of strings containing the service class names
       */
      std::vector<std::string> getSvcClassList(const std::string vo,
                                               const Cuuid_t &myId)
        throw();

    } // end of namespace utils

  } // end of namespace daemon

} } /* end of namespace castor/srm */

//------------------------------------------------------------------------------
// buildFileIdMap (templated)
//------------------------------------------------------------------------------
template <class iterator> castor::srm::FileIdMap
castor::srm::daemon::utils::buildFileIdMap
(iterator subreqStart, iterator subreqEnd)
  throw() {
  castor::srm::FileIdMap res;
  for (iterator it = subreqStart;
       it != subreqEnd;
       it++) {
    struct Cns_fileid fid;
    fid.fileid = (*it)->userFile()->fileId();
    strncpy(fid.server, (*it)->userFile()->nsHost().c_str(), sizeof(fid.server)-1);
    res[(*it)->castorFileName()] = fid;
  }
  return res;
}

#endif
