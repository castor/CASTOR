/******************************************************************************
 *                      TurlHandlerThread.cpp
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * This thread is handling the responses coming asynchronously from Castor
 * and dispatched by the CastorClient thread
 *
 * @author castor-dev@cern.ch
 *****************************************************************************/

#include "castor/Services.hpp"
#include "castor/Constants.hpp"
#include "castor/IService.hpp"
#include "castor/log/log.hpp"
#include "castor/exception/Exception.hpp"
#include "castor/exception/Exception.hpp"
#include "Cns_api.h"
#include "rfio_api.h"
#include "fcntl.h"

#include "castor/srm/daemon/TurlHandlerThread.hpp"
#include "castor/srm/daemon/ISrmDaemonSvc.hpp"
#include "castor/srm/daemon/SupportedProtocols.hpp"
#include "castor/srm/CastorClient.hpp"
#include "castor/srm/StageRequest.hpp"
#include "castor/srm/SubRequest.hpp"
#include "castor/srm/SrmUser.hpp"
#include "castor/srm/daemon/SrmdUtils.hpp"
#include "castor/srm/SrmUtils.hpp"
#include "castor/srm/UserFile.hpp"

//------------------------------------------------------------------------------
// constructor
//------------------------------------------------------------------------------
castor::srm::daemon::TurlHandlerThread::TurlHandlerThread
(castor::srm::CastorClient& castorClient) :
  castor::BaseObject(),
  m_castorClient(castorClient) {}


//------------------------------------------------------------------------------
// run
//------------------------------------------------------------------------------
void castor::srm::daemon::TurlHandlerThread::run(void* param) throw() {
  // Get srm service
  castor::IService* s = svcs()->service("OraSrmDaemonSvc", castor::SVC_ORASRMDAEMONSVC);
  castor::srm::daemon::ISrmDaemonSvc* srmdSvc = dynamic_cast<castor::srm::daemon::ISrmDaemonSvc*>(s);

  // Get the response we should deal with
  castor::rh::IOResponse* res = (castor::rh::IOResponse*)param;
  if(res->errorCode() != 0 && res->fileId() == 0) {
    // in case of errors, the stager unfortunately may not fill the fileid (bug #61468),
    // so we can only log it while we should fail the related request.
    // "Stager call failed"
    std::list<castor::log::Param> errParams = {
      castor::log::Param("ErrorCode", sstrerror(res->errorCode())),
      castor::log::Param("ErrorMessage", res->errorMessage()),
      castor::log::Param("CastorReqId", res->reqAssociated()),      
      castor::log::Param("FileName", res->castorFileName())};      
    castor::log::write(LOG_WARNING, "Stager call failed", errParams);
    delete res;
    return;
  }
  // first get the request we are dealing with
  castor::srm::SubRequest* sr;
  sr = srmdSvc->getSubRequestById(res->reqAssociated(), res->fileId(), res->castorFileName());
  if (0 == sr) {
    // error has already been logged, just give up
    delete res;
    return;
  }
  // prepare data for later logging
  Cuuid_t myId = nullCuuid;
  string2Cuuid(&myId, const_cast<char*>(sr->request()->srmRequestId().c_str()));
  struct Cns_fileid nsid;
  nsid.fileid = sr->userFile()->fileId();
  strncpy(nsid.server, sr->userFile()->nsHost().c_str(), sizeof(nsid.server));
  // call proper handling method
  if (sr->request()->requestType() == castor::srm::REQUESTTYPE_GET) {
    handleGetResponses(sr, res, srmdSvc, myId, &nsid);
  } else {
    handlePutResponses(sr, res, srmdSvc, myId, &nsid);
  }
  // cleanup memory
  delete sr->request()->srmUser();
  delete sr->request();
  sr->setRequest(0);
  delete sr->userFile();
  sr->setUserFile(0);
  delete sr;
  delete res;
}


//------------------------------------------------------------------------------
// handleGetResponses
//------------------------------------------------------------------------------
void castor::srm::daemon::TurlHandlerThread::handleGetResponses
(castor::srm::SubRequest* subreq,
 castor::rh::IOResponse* resp,
 castor::srm::daemon::ISrmDaemonSvc* srmdSvc,
 const Cuuid_t &myId,
 struct Cns_fileid* nsId)
  throw() {
  // useful pointer to request
  castor::srm::StageRequest* req = subreq->request();
  // check for errors
  if (0 != resp->errorCode()) {
    // "Subrequest Failed" message
    std::list<castor::log::Param> errParams = {
      castor::log::Param("NSFILEID", nsId->fileid),
      castor::log::Param("REQID", myId),
      castor::log::Param("RequestToken", req->id()),
      castor::log::Param("Type", "GET"),
      castor::log::Param("SvcClass", req->svcClass()),
      castor::log::Param("uid", req->euid()),
      castor::log::Param("gid", req->egid()),
      castor::log::Param("ElapsedTime", time(NULL)-req->creationTime()),
      castor::log::Param("Reason", resp->errorMessage())
    };
    castor::log::write(LOG_ERR, "Subrequest Failed", errParams);
    srmdSvc->failSubRequest(subreq, resp->errorCode(), resp->errorMessage(), myId);
    return;
  }
  // no errors detected, compute and set TURL;
  // this method logs a "TURL available" message
  castor::srm::daemon::utils::buildTUrl
    (subreq, resp->server(), resp->port(), resp->fileName(),
     req->svcClass(), srmdSvc, myId, nsId);
}


//------------------------------------------------------------------------------
// handlePutResponses
//------------------------------------------------------------------------------
void castor::srm::daemon::TurlHandlerThread::handlePutResponses
(castor::srm::SubRequest* subreq,
 castor::rh::IOResponse* resp,
 castor::srm::daemon::ISrmDaemonSvc* srmdSvc,
 const Cuuid_t &myId,
 struct Cns_fileid* nsId)
  throw() {
  // check for errors
  if (0 != resp->errorCode()) {
    // "Subrequest Failed" message
    std::list<castor::log::Param> errParams = {
      castor::log::Param("NSFILEID", nsId->fileid),
      castor::log::Param("REQID", myId),
      castor::log::Param("RequestToken", subreq->request()->id()),
      castor::log::Param("Type", "PUT"),
      castor::log::Param("SvcClass", subreq->request()->svcClass()),
      castor::log::Param("uid", subreq->request()->euid()),
      castor::log::Param("gid", subreq->request()->egid()),
      castor::log::Param("ElapsedTime", time(NULL)-subreq->request()->creationTime()),
      castor::log::Param("Reason", resp->errorMessage())
    };
    castor::log::write(LOG_ERR, "Subrequest Failed", errParams);
    srmdSvc->failSubRequest(subreq, resp->errorCode(), resp->errorMessage(), myId);
    // unlink the file from the nameserver
    Cns_unlink(subreq->castorFileName().c_str());
    return;
  }
  // Special case for rfio : we need to connect
  if (subreq->request()->protocol() == SRM_PROTOCOL_RFIO) {
    serrno = 0;
    // build the turl
    // Note that we use special protocol "rfiointernal" that does not need
    // service class, so we pass "" for the service class
    std::string turl = castor::srm::utils::buildTURL
      ("rfiointernal", resp->server(), resp->port(), resp->fileName(), "");
    // open file with rfio. This is needed to "kill" the pending rfiod
    int fd = rfio_open((char*)turl.c_str(), O_CREAT| O_WRONLY, 0644);
    if (fd > 0) {
      // close file
      rfio_close(fd);
    } else {
      // "Subrequest Failed" message
      std::list<castor::log::Param> errParams = {
        castor::log::Param("NSFILEID", nsId->fileid),
        castor::log::Param("REQID", myId),
        castor::log::Param("RequestToken", subreq->request()->id()),
        castor::log::Param("Type", "PUT"),
        castor::log::Param("SvcClass", subreq->request()->svcClass()),
        castor::log::Param("uid", subreq->request()->euid()),
        castor::log::Param("gid", subreq->request()->egid()),
        castor::log::Param("ElapsedTime", time(NULL)-subreq->request()->creationTime()),
        castor::log::Param("Reason", "rfio_open failed"),
        castor::log::Param("Details", rfio_serror())
      };
      castor::log::write(LOG_ERR, "Subrequest Failed", errParams);
      srmdSvc->failSubRequest(subreq, SEINTERNAL, "rfio_open failed", myId);
      // unlink the file from the nameserver
      Cns_unlink(subreq->castorFileName().c_str());
      return;
    }
  }
  // fill subrequest with the size
  subreq->setReservedSize(resp->fileSize());
  // no errors detected, compute and set TURL;
  // this method logs a "TURL available" message
  castor::srm::daemon::utils::buildTUrl
    (subreq, resp->server(), resp->port(), resp->fileName(),
     subreq->request()->svcClass(), srmdSvc, myId, nsId);
  srmdSvc->setTurl(subreq, myId);
}
