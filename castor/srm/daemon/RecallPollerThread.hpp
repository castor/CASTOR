/******************************************************************************
 *                      RecallPollerThread.hpp
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * This thread is polling castor for ongoing castor requests (recalls
 * for either prepareToGets or BringOnline SRM requests).
 * When the CASTOR requests are over, it either builds the TURL when
 * possible (all cases but GridFTP v2) or issues a Get request to Castor
 * in an asynchronous mode and hands over the postprocessing to the
 * TurlHandler thread
 *
 * @author castor-dev team
 *****************************************************************************/

#ifndef SRM_DAEMON_RECALLPOLLERTHREAD_HPP
#define SRM_DAEMON_RECALLPOLLERTHREAD_HPP 1

#include "castor/server/SelectProcessThread.hpp"
#include "castor/srm/CastorClient.hpp"
#include <list>

namespace castor {
  // Forward declarations
  class IObject;
}

namespace castor {
 namespace srm {

  // Forward declarations
  class StageRequest;
  class SubRequest;

  namespace daemon { 
    // Forward declarations
    class ISrmDaemonSvc;
  }
  
  namespace daemon {

    /**
     * A thread to poll current PrepareToGet recall requests.
     */
    class RecallPollerThread : public castor::server::SelectProcessThread {

    public:

      /**
       * Initializes the db access.
       */
      RecallPollerThread(srm::CastorClient& castorClient);

      /**
       * Standard destructor
       */
      ~RecallPollerThread() throw() {};

      /**
       * Select part of the service: gives a pending PrepareToGetRequest if any.
       */
      virtual castor::IObject* select() throw();

      /**
       * Process part of the service: checks for new staged in files
       */
      virtual void process(castor::IObject* param) throw();

    private:

      // Small utilities

      /**
       * helper method to cleanup the memory of a given request
       * @param req the request to cleanup
       */
      inline void deleteRequest(srm::StageRequest* req) throw();

      /**
       * helper method to log request failures.
       * This will issue one log message per subrequest
       * @param req the request that failed
       * @param message a string explaining how we failed
       * @param level the reporting level
       * @param myId the uuid of the request, as a Cuuid_t struct
       */
      inline void logRequestFailure(srm::StageRequest* req,
                                    const std::string message,
                                    const int level,
                                    const Cuuid_t &myId) throw();

    private:

      /// CastorClient to use
      castor::srm::CastorClient& m_castorClient;

    };
  
  } // end of namespace daemon

} } /* end of namespace castor/srm */


#endif // SRM_DAEMON_RECALLPOLLERTHREAD_HPP
