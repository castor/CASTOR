/******************************************************************************
 *                      SrmDaemon.cpp
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2004  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Deamon part of the SRM software suite.
 * Handles the asynchronous requests.
 *
 * @author castor-dev team
 *****************************************************************************/

// Include Files
#include <iostream>

#include "castor/BaseObject.hpp"
#include "castor/Constants.hpp"
#include "castor/Services.hpp"
#include "castor/log/log.hpp"
#include "castor/log/SyslogLogger.hpp"
#include "castor/server/SignalThreadPool.hpp"
#include "castor/server/DynamicThreadPool.hpp"
#include "castor/exception/Exception.hpp"
#include "castor/db/DbParamsSvc.hpp"
#include <dlfcn.h>

#include "castor/srm/daemon/SrmDaemon.hpp"
#include "castor/srm/daemon/StageReqSvcThread.hpp"
#include "castor/srm/daemon/TurlHandlerThread.hpp"
#include "castor/srm/daemon/RecallPollerThread.hpp"
#include "castor/srm/SrmConstants.hpp"
#include "castor/srm/CastorClient.hpp"

//------------------------------------------------------------------------------
// main method
//------------------------------------------------------------------------------
int main(int argc, char *argv[]) {
  try {
    castor::srm::SrmConstants* consts = castor::srm::SrmConstants::SrmConstants::getInstance();
    consts->readConfigFile();

    // Check availability of db service
    if(castor::BaseObject::services()->service("DbCnvSvc", castor::SVC_DBCNV) == 0 ||
       castor::BaseObject::services()->service("OraSrmDaemonSvc", castor::SVC_ORASRMDAEMONSVC) == 0) {
      castor::exception::Exception nosvc;
      nosvc.getMessage() << "No DB service available in config file";
      throw nosvc;
    }

    // Instantiate daemon and thread pools
    castor::log::init(new castor::log::SyslogLogger("srmbed"));
    castor::srm::daemon::SrmDaemon daemon(std::cout, std::cerr);
    
    castor::srm::CastorClient* cc = new castor::srm::CastorClient(consts->beCallbackPort());
    daemon.addThreadPool(
      new castor::server::SignalThreadPool("StageRequestHandler",
        new castor::srm::daemon::StageReqSvcThread(*cc)));
    daemon.getThreadPool('S')->setNbThreads(consts->nbReqThreads());
    daemon.addThreadPool(
      new castor::server::DynamicThreadPool("TurlHandler",
        new castor::srm::daemon::TurlHandlerThread(*cc), cc,    // cc is used as a producer for this pool
        consts->nbTurlThreads(), consts->nbTurlThreads()+5));
    daemon.addThreadPool(    
      new castor::server::SignalThreadPool("RecallPoller",
        new castor::srm::daemon::RecallPollerThread(*cc)));
    daemon.getThreadPool('R')->setNbThreads(consts->nbPollThreads());
    daemon.addNotifierThreadPool(consts->reqNotifyPort());

    daemon.parseCommandLine(argc, argv);

    // Create a db parameters service and fill with appropriate defaults
    castor::IService* s = castor::BaseObject::sharedServices()->service("DbParamsSvc", castor::SVC_DBPARAMSSVC);
    castor::db::DbParamsSvc* params = dynamic_cast<castor::db::DbParamsSvc*>(s);
    if(params == 0) {
      castor::exception::Exception e;
      e.getMessage() << "Could not instantiate the parameters service";
      throw e;
    }  
    params->setSchemaVersion(SRMSCHEMAVERSION);
    params->setDbAccessConfFile(ORASRMCONFIGFILE);

    const bool runAsStagerSuperuser = true;
    daemon.start(runAsStagerSuperuser);
    return 0;
  }
  catch (castor::exception::Exception& e) {
    std::cerr << "Caught castor exception : "
              << sstrerror(e.code()) << std::endl
              << e.getMessage().str() << std::endl;
  }
  catch (...) {
    std::cerr << "Caught general exception!" << std::endl;
  }
  return 1;
}


//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
castor::srm::daemon::SrmDaemon::SrmDaemon(std::ostream &stdOut, std::ostream &stdErr) :
castor::server::MultiThreadedDaemon(stdOut, stdErr)
{
  castor::log::write(LOG_INFO, "SrmDaemon started");
}

//------------------------------------------------------------------------------
// help
//------------------------------------------------------------------------------
void castor::srm::daemon::SrmDaemon::help(std::string programName)
{
  std::cout << "Usage: " << programName << " [options]\n"
    "\n"
    "where options can be:\n"
    "\n"
    "\t--foreground      or -f                \tForeground\n"
    "\t--metrics         or -m                \tEnable metrics collection\n"
    "\t--help            or -h                \tPrint this help and exit\n"
    "\t--Sthreads        or -S {integer >= 0} \tNumber of StageRequest handler threads\n"
    "\t--Tthreads        or -T {integer >= 0} \tNumber of TURL handler threads\n"
    "\t--Rthreads        or -R {integer >= 0} \tNumber of Recall poller threads\n"
    "\n"
    "Comments to: Castor.Support@cern.ch\n";
}
