/******************************************************************************
 *                      TurlHandlerThread.hpp
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * This thread is handling the responses coming asynchronously from Castor
 * and dispatched by the CastorClient thread
 *
 * @author castor-dev@cern.ch
 *****************************************************************************/

#ifndef SRM_DAEMON_TURLHANDLERTHREAD_HPP
#define SRM_DAEMON_TURLHANDLERTHREAD_HPP 1

#include "castor/server/IThread.hpp"
#include "castor/BaseObject.hpp"
#include "castor/srm/CastorClient.hpp"

// Forward declaration
struct Cns_fileid;

namespace castor {
  namespace rh {
    // Forward declaration
    class IOResponse;
  }
}

namespace castor {
 namespace srm {

  // Forward declaration
  class SubRequest;

  namespace daemon {

    // Forward declaration
    class ISrmDaemonSvc;

    /**
     * A thread to process an asynchronous response from Castor
     * containing the TURL for a given SRM request
     */
    class TurlHandlerThread : public castor::server::IThread,
                              public castor::BaseObject {

    public:

      /**
       * Standard constructor
       */
      TurlHandlerThread(srm::CastorClient& castorClient);

      /**
       * Standard destructor
       */
      ~TurlHandlerThread() throw() {};

      /**
       * Main processing method
       * @param pointer to an castor::srm::SrmResponse struct
       */
      virtual void run(void* param) throw();

      virtual void init() {};

      virtual void stop() {};

    private:

      /**
       * Handles the get case.
       * Note that no exception is thrown, errors are logged internally
       * @param subreq the subrequest concerned
       * @param resp the response from the stager
       * @param srmdSvc the SrmDaemon service to be used
       * @param myId the Cuuid to be used for logging
       * @param nsId the fileId to be used for logging purposes
       */
      void handleGetResponses(srm::SubRequest* subreq,
                              castor::rh::IOResponse* resp,
                              castor::srm::daemon::ISrmDaemonSvc* srmdSvc,
                              const Cuuid_t &myId,
                              struct Cns_fileid* nsId)
        throw();
        
      /**
       * Handles the put case.
       * Note that no exception is thrown, errors are logged internally
       * @param subreq the subrequest concerned
       * @param resp the response from the stager
       * @param srmdSvc the SrmDaemon service to be used
       * @param myId the Cuuid to be used for logging
       * @param nsId the fileId to be used for logging purposes
       */
      void handlePutResponses(srm::SubRequest* subreq,
                              castor::rh::IOResponse* resp,
                              castor::srm::daemon::ISrmDaemonSvc* srmdSvc,
                              const Cuuid_t &myId,
                              struct Cns_fileid* nsId)
        throw();

    private:

      /// CastorClient to use
      castor::srm::CastorClient& m_castorClient;
        
    };

  } // end of namespace daemon

} } /* end of namespace castor/srm */


#endif // SRM_DAEMON_TURLHANDLERTHREAD_HPP
