#ifndef SRM_SRMUTILS_HPP
#define SRM_SRMUTILS_HPP

#include <castor/exception/Exception.hpp>
#include <string>

namespace castor {
 namespace srm {

  namespace utils {

    /**
     * Builds a TURL for the given protocol
     * @param protocol the protocol to use
     * @param host the host to be used
     * @param port the port
     * @param path the path. Can be filename, uuid or anything else, depending
     * on the protocol
     * @param svcClass the service class, optional
     * @exception raises an exception in case a template TUrl cannot
     * be found in the config file for the given protocol
     */
    std::string buildTURL(const std::string protocol,
                          const std::string host,
                          const int port,
                          const std::string path,
                          const std::string svcClass = "")
      throw(castor::exception::Exception);

  } // end of namespace utils

} } /* end of namespace castor/srm */

#endif
