/******************************************************************************
 *                      SrmException.hpp
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * SRM exceptions returning TStatusCode values
 *
 * @author Shaun de Witt
 *****************************************************************************/
#ifndef EXCEPTION_SRM_HPP
#define EXCEPTION_SRM_HPP

#include "soapH.h"
#include "castor/exception/Exception.hpp"

namespace castor {
 namespace srm {
    /**
      * SRM Exception
      */
    class SrmException : public castor::exception::Exception {
        public:
            // Constructor
            SrmException(enum srm__TStatusCode error);
    };
} }  // end of castor/srm namespace

#endif

