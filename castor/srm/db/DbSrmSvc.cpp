/******************************************************************************
 *                      DbSrmSvc.cpp
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * @(#)DbSrmSvc.cpp,v 1.75 $Release$ 2008/10/31 11:07:31 itglp
 *
 * Implementation of the ISrmSvc for CDBC
 *
 * @author Giuseppe Lo Presti
 *****************************************************************************/

// Include Files
#include <string>
#include <sstream>
#include <vector>
#include <algorithm>

#include <Cuuid.h>
#include <Cns_api.h>
#include <vmgr_api.h>
#include <Ctape_api.h>
#include <serrno.h>

#include "castor/IAddress.hpp"
#include "castor/IObject.hpp"
#include "castor/IFactory.hpp"
#include "castor/SvcFactory.hpp"
#include "castor/Constants.hpp"
#include "castor/db/DbCnvSvc.hpp"
#include "castor/db/IDbResultSet.hpp"
#include "castor/exception/Exception.hpp"
#include "castor/exception/Exception.hpp"
#include "castor/exception/NoEntry.hpp"
#include "castor/exception/InvalidArgument.hpp"
#include "castor/BaseAddress.hpp"
#include "castor/BaseObject.hpp"
#include "castor/Services.hpp"

#include "castor/srm/Constants.hpp"
#include "castor/srm/SrmUser.hpp"
#include "castor/srm/StageRequest.hpp"
#include "castor/srm/SubRequest.hpp"
#include "castor/srm/StorageArea.hpp"
#include "castor/srm/UserFile.hpp"
#include "castor/srm/db/DbSrmSvc.hpp" 

// -----------------------------------------------------------------------
// Instantiation of a static factory class
// -----------------------------------------------------------------------
static castor::SvcFactory<castor::srm::db::DbSrmSvc>* s_factoryDbSrmSvc =
  new castor::SvcFactory<castor::srm::db::DbSrmSvc>();

//------------------------------------------------------------------------------
// Static constants initialization
//------------------------------------------------------------------------------
/// SQL statement for getSrmUser
const std::string castor::srm::db::DbSrmSvc::s_getSrmUserStatementString = 
  "SELECT id FROM SrmUser WHERE userID = :1 AND vo = :2 AND dn = :3";

/// SQL statement for getUserFile
const std::string castor::srm::db::DbSrmSvc::s_getUserFileStatementString = 
  "SELECT id FROM UserFile WHERE castorFileName = normalizePath(:1)";

/// SQL statement for createUserFile
const std::string castor::srm::db::DbSrmSvc::s_createUserFileStatementString = 
  "BEGIN createUserFile(:1, :2, :3, :4, :5, :6); END;";

/// SQL statement for getStorageArea
const std::string castor::srm::db::DbSrmSvc::s_getStorageAreaStatementString = 
  "SELECT id FROM StorageArea WHERE spacetoken = :1";

/// SQL statement for getRequestTokens
const std::string castor::srm::db::DbSrmSvc::s_getRequestTokensStatementString1 = 
  "SELECT id FROM StageRequest WHERE StageRequest.srmUser = :1";
const std::string castor::srm::db::DbSrmSvc::s_getRequestTokensStatementString2 = 
  "SELECT id FROM StageRequest WHERE srmUser = :1 AND requestdesc = :2";

/// SQL statement for getSpaceTokens
const std::string castor::srm::db::DbSrmSvc::s_getSpaceTokensStatementString = 
"SELECT S.spaceToken FROM StorageArea S, SrmUser U WHERE S.srmuser = U.id AND (U.vo = :1 OR :2 IS NULL) AND (S.tokenDescription = :3 OR :4 IS NULL)";

/// Take a lock on a request for further processing
const std::string castor::srm::db::DbSrmSvc::s_lockStageRequest = 
  "SELECT id FROM StageRequest WHERE id = :1 FOR UPDATE";

// SQL statement for abortPutRequestByFile
const std::string castor::srm::db::DbSrmSvc::s_abortPutRequestByFile = 
  "UPDATE /*+ INDEX(S I_SubRequest_CastorFileName) */ SubRequest S SET status=4 WHERE castorfilename=:1 AND status IN (0,1,2,5) AND EXISTS (SELECT id FROM StageRequest WHERE StageRequest.status <> 4 AND S.request=StageRequest.id)";

// SQL statement for subRequestsToRelease
const std::string castor::srm::db::DbSrmSvc::s_subRequestsToRelease = 
" SELECT /*+ INDEX(S I_SubRequest_CastorFileName) */ id FROM SubRequest S WHERE S.castorfilename=:1 AND S.status IN (2,5) AND EXISTS (SELECT id FROM StageRequest WHERE StageRequest.srmUser=:2 AND StageRequest.requestType IN (2,5) AND S.request=StageRequest.id)";

// SQL statement for getSpaceTokensList
const std::string castor::srm::db::DbSrmSvc::s_getSpaceTokensList =
  "SELECT svcClass, spaceToken FROM StorageArea A, SrmUser U WHERE A.srmUser = U.id AND A.storageStatus = 1 AND U.vo = :1";  


// -----------------------------------------------------------------------
// DbSrmSvc
// -----------------------------------------------------------------------
castor::srm::db::DbSrmSvc::DbSrmSvc(const std::string name) :
  BaseSvc(name), DbBaseObj(0),
  m_getSrmUserStatement(0),
  m_getUserFileStatement(0),
  m_createUserFileStatement(0),
  m_getStorageAreaStatement(0),
  m_getRequestTokensStatement1(0),
  m_getRequestTokensStatement2(0),
  m_getSpaceTokensStatement(0),
  m_lockStageRequest(0),
  m_abortPutRequestByFile(0),
  m_subRequestsToRelease(0),
  m_getSpaceTokensList(0)
{
  registerToCnvSvc(this);
}

// -----------------------------------------------------------------------
// ~DbSrmSvc
// -----------------------------------------------------------------------
castor::srm::db::DbSrmSvc::~DbSrmSvc() throw() {
  unregisterFromCnvSvc(this);
  reset();
}

// -----------------------------------------------------------------------
// id
// -----------------------------------------------------------------------
unsigned int castor::srm::db::DbSrmSvc::id() const {
  return ID();
}

// -----------------------------------------------------------------------
// ID
// -----------------------------------------------------------------------
unsigned int castor::srm::db::DbSrmSvc::ID() {
  return castor::SVC_DBSRMSVC;
}

//------------------------------------------------------------------------------
// reset
//------------------------------------------------------------------------------
void castor::srm::db::DbSrmSvc::reset() throw() {
  // Call upper level reset
  this->castor::BaseSvc::reset();
  this->castor::db::DbBaseObj::reset();
  //Here we attempt to delete the statements correctly
  // If something goes wrong, we just ignore it
  try {
    if(m_getSrmUserStatement) delete m_getSrmUserStatement;
    if(m_getUserFileStatement) delete m_getUserFileStatement;
    if(m_createUserFileStatement) delete m_createUserFileStatement;
    if(m_getStorageAreaStatement) delete m_getStorageAreaStatement;
    if(m_getRequestTokensStatement1) delete m_getRequestTokensStatement1;
    if(m_getRequestTokensStatement2) delete m_getRequestTokensStatement2;
    if(m_getSpaceTokensStatement) delete m_getSpaceTokensStatement;
    if(m_lockStageRequest) delete m_lockStageRequest;
    if(m_abortPutRequestByFile) delete m_abortPutRequestByFile;
    if(m_subRequestsToRelease) delete m_subRequestsToRelease;
    if(m_getSpaceTokensList) delete m_getSpaceTokensList;
  } catch (castor::exception::SQLError ignored) {};
  // Now reset all pointers to 0
  m_getSrmUserStatement = 0;
  m_getUserFileStatement = 0;
  m_createUserFileStatement = 0;
  m_getStorageAreaStatement = 0;
  m_getRequestTokensStatement1 = 0;
  m_getRequestTokensStatement2 = 0;
  m_getSpaceTokensStatement = 0;
  m_lockStageRequest = 0;
  m_abortPutRequestByFile = 0;
  m_subRequestsToRelease = 0;
  m_getSpaceTokensList = 0;
}


// -----------------------------------------------------------------------
// getSrmUser
// -----------------------------------------------------------------------
castor::srm::SrmUser* castor::srm::db::DbSrmSvc::getSrmUser(std::string username, std::string vo, std::string dn) 
    throw (castor::exception::Exception) {
        // Make sure the VO is in lowercase...
        transform ( vo.begin(), vo.end(), vo.begin(), ( int(*)(int))tolower );

        try {
            if ( 0 == m_getSrmUserStatement ) {
                m_getSrmUserStatement = createStatement(s_getSrmUserStatementString);
            }
            m_getSrmUserStatement->setString(1, username);
            m_getSrmUserStatement->setString(2, vo);
            m_getSrmUserStatement->setString(3, dn);

            // execute statement
            castor::db::IDbResultSet *rset = m_getSrmUserStatement->executeQuery();

            // See if we found anything...
            if(!rset->next()) {
                // Create a new user...
                delete rset;
                castor::srm::SrmUser *newUser = new castor::srm::SrmUser();
                newUser->setUserID(username);
                newUser->setVo(vo);
                newUser->setDn(dn);
                castor::BaseAddress ad;
                ad.setCnvSvcName("DbCnvSvc");
                ad.setCnvSvcType(castor::SVC_DBCNV);
                try {
                    cnvSvc()->createRep(&ad, newUser, true);
                    return newUser;
                }
                catch (castor::exception::InvalidArgument e) {
                    delete newUser;
                    // XXX Change createRep in CodeGenerator to forward the oracle errorcode
                    if ( e.getMessage().str().find("ORA-00001", 0) != std::string::npos ) {
                        // if violation of unique constraint, ie means that
                        // some other thread was quicker than us on the insertion
                        // So let's select what was inserted
                        rset = m_getSrmUserStatement->executeQuery();
                        if (!rset->next()) {
                            // Still nothing ! Here it's a real error
                            delete rset;
                            castor::exception::Exception ex;
                            ex.getMessage()
                                << "Unable to select user while inserting "
                                << "violated unique constraint :"
                                << std::endl << e.getMessage().str();
                            throw ex;
                        }
                    }
                    else {
                        // "standard" error, throw exception
                        castor::exception::Exception ex;
                        ex.getMessage()
                            << "Exception while inserting new user in the DB :"
                            << std::endl << e.getMessage().str();
                        throw ex;
                    }
                }
            }

            u_signed64 id = rset->getInt64(1);

            // Get the result set
            castor::IObject* obj = cnvSvc()->getObjFromId(id, castor::srm::OBJ_SrmUser);
            if ( 0 == obj ) {
                castor::exception::Exception ex;
                delete rset;
                ex.getMessage() << "getSrmUser: could not retrieve object from id " << id;
                throw ex;
            }
            castor::srm::SrmUser *result = dynamic_cast<castor::srm::SrmUser*>(obj);
            if ( 0 == result ) {
                castor::exception::Exception ex;
                ex.getMessage() << "getSrmUser: object retrieved for id " <<
                    id << " was a " << castor::srm::ObjectsIdStrings[obj->type() - castor::srm::ObjectsIdsOffset] <<
                    " while a castor::srm::SrmUser was expected.";
                delete rset;
                delete obj;
                throw ex;
            }
            delete rset;
            return result;
        }
        catch (castor::exception::Exception e) {
            rollback();
            castor::exception::Exception ex;
            ex.getMessage() << "Error caught in castor::srm::getSrmUser." <<
                std::endl << e.getMessage().str();
            throw ex;
        }
}

// -----------------------------------------------------------------------
// getUserFile
// -----------------------------------------------------------------------
castor::srm::UserFile* 
castor::srm::db::DbSrmSvc::getUserFile(std::string castorFileName)
  throw (castor::exception::Exception) {
    castor::db::IDbResultSet *rset = 0;
    castor::srm::UserFile *result = 0;

    try {
        if ( 0 == m_getUserFileStatement ) {
            m_getUserFileStatement = createStatement(s_getUserFileStatementString);
        }
        m_getUserFileStatement->setString(1, castorFileName);
        rset = m_getUserFileStatement->executeQuery();
        if ( !rset->next() ) {
            castor::exception::NoEntry ex;
            ex.getMessage() << "getUserFile: could not find file " << castorFileName << std::endl;
            throw ex;
        }
        u_signed64 id = rset->getInt64(1);
        castor::IObject* obj = cnvSvc()->getObjFromId(id, castor::srm::OBJ_UserFile);
        delete rset;
        if ( 0 == obj ) {
            castor::exception::Exception ex;
            ex.getMessage() << "getUserFile: could not retrieve object from id " << id << std::endl;
            throw ex;
        }

        result = dynamic_cast<castor::srm::UserFile*>(obj);
        if ( result == 0 ) {
            castor::exception::Exception ex;
            ex.getMessage() << "getUserFile: object retrieved for id " <<
                id << " was of type " << obj->type() - castor::srm::ObjectsIdsOffset <<
                " while a castor::srm::UserFile was expected.";
            delete obj;
            throw ex;
        }
    }
    catch ( castor::exception::SQLError& ex) {
        castor::exception::Exception ex;
        ex.getMessage() << "Error caught in getUserFile: " <<
            std::endl << ex.getMessage().str();
        throw ex;
    }
    return result;
}

// -----------------------------------------------------------------------
// createUserFile
// -----------------------------------------------------------------------
castor::srm::UserFile* 
castor::srm::db::DbSrmSvc::createUserFile(castor::srm::SubRequest* subreq, u_signed64 nsFileId,
  std::string nsHost, u_signed64 nsFileSize)
  throw (castor::exception::Exception) {
    try {
        if ( 0 == m_createUserFileStatement ) {
            m_createUserFileStatement = createStatement(s_createUserFileStatementString);
            m_createUserFileStatement->registerOutParam(6, castor::db::DBTYPE_UINT64);
            m_createUserFileStatement->endTransaction();
        }
        m_createUserFileStatement->setString(1, subreq->castorFileName());
        m_createUserFileStatement->setUInt64(2, nsFileId);
        m_createUserFileStatement->setString(3, nsHost);
        m_createUserFileStatement->setUInt64(4, nsFileSize);
        m_createUserFileStatement->setUInt64(5, subreq->id());
        m_createUserFileStatement->execute();
        u_signed64 id = m_createUserFileStatement->getUInt64(6);
        castor::srm::UserFile *result = new castor::srm::UserFile();
        result->setId(id);
        subreq->setUserFile(result);
        return result;
    }
    catch ( castor::exception::Exception cex ) {
        castor::exception::Exception ex;
        ex.getMessage() << "Error caught in createUserFile: " <<
            std::endl << cex.getMessage().str();
        throw ex;
    }
}

// -----------------------------------------------------------------------
// getStorageArea
// -----------------------------------------------------------------------
castor::srm::StorageArea* castor::srm::db::DbSrmSvc::getStorageArea(std::string spaceToken) throw (castor::exception::Exception) {
    try {
        if ( 0 == m_getStorageAreaStatement ) {
            m_getStorageAreaStatement = createStatement(s_getStorageAreaStatementString);
        }

        m_getStorageAreaStatement->setString(1, spaceToken);

        castor::db::IDbResultSet *rset = m_getStorageAreaStatement->executeQuery();

        if (!rset->next() ) {
            delete rset;
            return 0;
        }

        u_signed64 id = rset->getInt64(1);
        delete rset;
        
        castor::IObject *obj = cnvSvc()->getObjFromId(id, castor::srm::OBJ_StorageArea);
        if ( 0 == obj ) {
            castor::exception::Exception ex;
            ex.getMessage() << "getStorageArea: could not retrieve object from id " << id;
            throw ex;
        }
        castor::srm::StorageArea *result = dynamic_cast<castor::srm::StorageArea*>(obj);
        if ( 0 == result ) {
            castor::exception::Exception ex;
            ex.getMessage() << "srm::getStorageArea: object retrieved for id " << id <<
                " was of type " << castor::srm::ObjectsIdStrings[obj->type() - castor::srm::ObjectsIdsOffset] <<
                " while a castor::srm::StorageArea was expected.";
            delete obj;
            throw ex;
        }
        return result;
    }
    catch (castor::exception::SQLError e ) {
        rollback();
        castor::exception::Exception ex;
        ex.getMessage() << "Error caught in castor::srm::getStorageArea." <<
            std::endl << e.getMessage().str();
        throw ex;
    }
}

// -----------------------------------------------------------------------
// getStageRequest
// -----------------------------------------------------------------------
castor::srm::StageRequest* 
castor::srm::db::DbSrmSvc::getStageRequest(std::string requestToken) throw (castor::exception::Exception) {
    try {
        u_signed64 id = atoi(requestToken.c_str());
        if (id == 0) {
            castor::exception::Exception ex;
            throw ex;
        }
        castor::IObject *obj = cnvSvc()->getObjFromId(id, castor::srm::OBJ_StageRequest);
        castor::srm::StageRequest *result = dynamic_cast<castor::srm::StageRequest*>(obj);
        if ( 0 == result ) {
            delete obj;
            castor::exception::Exception ex;
            throw ex;
        }
        return result;
    }
    catch (castor::exception::Exception& e) {
        castor::exception::Exception ex;
        ex.getMessage() << "Request token " << requestToken << " not found";
        throw ex;
    }
}

// -----------------------------------------------------------------------
// getRequestTokens
// -----------------------------------------------------------------------
std::vector<std::string>
castor::srm::db::DbSrmSvc::getRequestTokens(u_signed64 userId, std::string* description)
  throw (castor::exception::Exception) {
    if ( 0 == m_getRequestTokensStatement1 ) {
        m_getRequestTokensStatement1 = createStatement(s_getRequestTokensStatementString1);
        m_getRequestTokensStatement2 = createStatement(s_getRequestTokensStatementString2);
    }
    castor::db::IDbResultSet *rset = 0;
    if ( description ) {
        m_getRequestTokensStatement2->setInt64(1, userId);
        m_getRequestTokensStatement2->setString(2, *description);
        rset = m_getRequestTokensStatement2->executeQuery();
    }
    else {
        m_getRequestTokensStatement1->setInt64(1, userId);
        rset = m_getRequestTokensStatement1->executeQuery();
    }

    std::vector<std::string> tokens;
    while (rset->next()) {
        tokens.push_back(rset->getString(1));
    }
    delete rset;
    return tokens;
}

// -----------------------------------------------------------------------
// getSpaceTokens
// -----------------------------------------------------------------------
std::vector<std::string>
castor::srm::db::DbSrmSvc::getSpaceTokens(std::string* vo, std::string* description)
  throw (castor::exception::Exception) {
    if ( !m_getSpaceTokensStatement ) {
        m_getSpaceTokensStatement = createStatement(s_getSpaceTokensStatementString);
    }
    // Note how crazy it is the way to pass optional bind variables to Oracle
    // because there's no way to reuse a bind variable more than once in a SQL statement 
    if ( 0 != vo ) {
        m_getSpaceTokensStatement->setString(1, *vo);
        m_getSpaceTokensStatement->setInt(2, 0);
    } else {
        m_getSpaceTokensStatement->setString(1, "");
        m_getSpaceTokensStatement->setNull(2);
    }
    if ( 0 != description ) {
        m_getSpaceTokensStatement->setString(3, *description);
        m_getSpaceTokensStatement->setInt(4, 0);
    } else {
        m_getSpaceTokensStatement->setString(3, "");
        m_getSpaceTokensStatement->setNull(4);
    }
    castor::db::IDbResultSet *rset;
    rset = m_getSpaceTokensStatement->executeQuery();
    std::vector<std::string> tokens;
    while (rset->next()) {
        tokens.push_back(rset->getString(1));
    }
    delete rset;
    return tokens;
}

// -----------------------------------------------------------------------
// lockStageRequest
// -----------------------------------------------------------------------
void castor::srm::db::DbSrmSvc::lockStageRequest(u_signed64 reqId)
  throw (castor::exception::Exception) {

  try {
      if(m_lockStageRequest == 0) {
        m_lockStageRequest = createStatement(s_lockStageRequest);
      }
      m_lockStageRequest->setDouble(1, reqId);
      castor::db::IDbResultSet *rset = m_lockStageRequest->executeQuery();
      // we don't need the result, we only want to grab the lock
      delete rset;
  }
  catch (castor::exception::SQLError e) {
    	castor::exception::Exception ex;
    	ex.getMessage()
      	<< "Error caught in castor::srm::lockStageRequest."
      	<< std::endl << e.getMessage().str();
    	throw ex;
  }
}

// -----------------------------------------------------------------------
// abortPutRequestByFile
// -----------------------------------------------------------------------
void castor::srm::db::DbSrmSvc::abortPutRequestByFile(std::string& filename)
  throw (castor::exception::Exception) {

  try {
      if(m_abortPutRequestByFile == 0) {
        m_abortPutRequestByFile = createStatement(s_abortPutRequestByFile);
        m_abortPutRequestByFile->endTransaction();
      }
      m_abortPutRequestByFile->setString(1, filename);
      m_abortPutRequestByFile->execute();
  }
  catch (castor::exception::SQLError e) {
    	castor::exception::Exception ex;
    	ex.getMessage()
      	<< "Error caught in castor::srm::abortPutRequestByFile."
      	<< std::endl << e.getMessage().str();
    	throw ex;
  }
}

// -----------------------------------------------------------------------
// subRequestsToRelease
// -----------------------------------------------------------------------
std::list<castor::srm::SubRequest*> castor::srm::db::DbSrmSvc::subRequestsToRelease(std::string& filename, u_signed64 userid)
    throw (castor::exception::Exception) {
        std::list<castor::srm::SubRequest*> myList;
        try {
            if ( 0 == m_subRequestsToRelease)
                m_subRequestsToRelease = createStatement(s_subRequestsToRelease);
            m_subRequestsToRelease->setString(1, filename);
            m_subRequestsToRelease->setUInt64(2, userid);

            // Execute
            castor::db::IDbResultSet *rset = m_subRequestsToRelease->executeQuery();

            // Check results
            while (rset->next()) {
                u_signed64 id = rset->getInt64(1);
                castor::IObject* obj = cnvSvc()->getObjFromId(id, castor::srm::OBJ_SubRequest);
                if ( 0 == obj) continue;
                castor::srm::SubRequest* sub = dynamic_cast<castor::srm::SubRequest*>(obj);
                if ( 0 == sub ) continue;
                myList.push_back(sub);
            }
            delete rset;
        } catch (castor::exception::Exception& e) {
            throw;
        }
        return myList;
    }


// -----------------------------------------------------------------------
// getSpaceTokensList
// -----------------------------------------------------------------------
std::map<std::string, std::string> castor::srm::db::DbSrmSvc::getSpaceTokensList(std::string vo)
  throw (castor::exception::Exception) {

  std::map<std::string, std::string> tokens;
  try {
      if(m_getSpaceTokensList == 0) {
        m_getSpaceTokensList = createStatement(s_getSpaceTokensList);
      }
      m_getSpaceTokensList->setString(1, vo);
      castor::db::IDbResultSet *rset = m_getSpaceTokensList->executeQuery();
      while (rset->next()) {
        tokens[rset->getString(1)] = rset->getString(2);
      }
      delete rset;
      return tokens;
  }
  catch (castor::exception::SQLError e) {
    	castor::exception::Exception ex;
    	ex.getMessage()
      	<< "Error caught in castor::srm::abortPutRequestByFile."
      	<< std::endl << e.getMessage().str();
    	throw ex;
  }
}


// -----------------------------------------------------------------------
// toString
// -----------------------------------------------------------------------
std::string castor::srm::db::DbSrmSvc::toString(int value) {
    std::stringstream ss;
    ss << value << std::flush;
    return ss.str();
}
