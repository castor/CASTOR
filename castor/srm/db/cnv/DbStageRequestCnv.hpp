/**** This file has been autogenerated by gencastor from Umbrello UML model ***/

/******************************************************************************
 *                      srm/db/cnv/DbStageRequestCnv.hpp
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

#ifndef SRM_DB_CNV_STAGEREQUEST_HPP
#define SRM_DB_CNV_STAGEREQUEST_HPP

// Include Files
#include "castor/Constants.hpp"
#include "castor/IAddress.hpp"
#include "castor/db/cnv/DbBaseCnv.hpp"
#include "castor/exception/Exception.hpp"
#include <vector>

// Forward declarations
namespace castor {

  // Forward declarations
  class IObject;
  class ICnvSvc;

} /* end of namespace castor */

namespace castor {
 namespace srm {

  // Forward declarations
  class StageRequest;

  namespace db {

    namespace cnv {

      /**
       * class DbStageRequestCnv
       * A converter for storing/retrieving StageRequest into/from a generic database
       */
      class DbStageRequestCnv : public castor::db::cnv::DbBaseCnv {

      public:

        /**
         * Constructor
         */
        DbStageRequestCnv(castor::ICnvSvc* cnvSvc);

        /**
         * Destructor
         */
        virtual ~DbStageRequestCnv() throw();

        /**
         * Gets the object type.
         * That is the type of object this converter can convert
         */
        static unsigned int ObjType();

        /**
         * Gets the object type.
         * That is the type of object this converter can convert
         */
        virtual unsigned int objType() const;

        /**
         * Creates foreign representation from a C++ Object.
         * @param address where to store the representation of
         * the object
         * @param object the object to deal with
         * @param endTransaction whether the changes to the database
         * should be commited or not
         * @param type if not OBJ_INVALID, the ids representing
         * the links to objects of this type will not set to 0
         * as is the default.
         * @exception Exception throws an Exception in case of error
         */
        virtual void createRep(castor::IAddress* address,
                               castor::IObject* object,
                               bool endTransaction,
                               unsigned int type = castor::OBJ_INVALID)
          throw (castor::exception::Exception);

        /**
         * Creates foreign representation from a set of C++ Objects.
         * @param address where to store the representation of
         * the objects
         * @param objects the list of objects to deal with
         * @param endTransaction whether the changes to the database
         * should be commited or not
         * @param type if not OBJ_INVALID, the ids representing
         * the links to objects of this type will not set to 0
         * as is the default.
         * @exception Exception throws an Exception in case of error
         */
        virtual void bulkCreateRep(castor::IAddress* address,
                               std::vector<castor::IObject*> &objects,
                               bool endTransaction,
                               unsigned int type = castor::OBJ_INVALID)
          throw (castor::exception::Exception);

        /**
         * Updates foreign representation from a C++ Object.
         * @param address where the representation of
         * the object is stored
         * @param object the object to deal with
         * @param endTransaction whether the changes to the database
         * should be commited or not
         * @exception Exception throws an Exception in case of error
         */
        virtual void updateRep(castor::IAddress* address,
                               castor::IObject* object,
                               bool endTransaction)
          throw (castor::exception::Exception);

        /**
         * Deletes foreign representation of a C++ Object.
         * @param address where the representation of
         * the object is stored
         * @param object the object to deal with
         * @param endTransaction whether the changes to the database
         * should be commited or not
         * @exception Exception throws an Exception in case of error
         */
        virtual void deleteRep(castor::IAddress* address,
                               castor::IObject* object,
                               bool endTransaction)
          throw (castor::exception::Exception);

        /**
         * Creates C++ object from foreign representation
         * @param address the place where to find the foreign
         * representation
         * @return the C++ object created from its reprensentation
         * or 0 if unsuccessful. Note that the caller is responsible
         * for the deallocation of the newly created object
         * @exception Exception throws an Exception in case of error
         */
        virtual castor::IObject* createObj(castor::IAddress* address)
          throw (castor::exception::Exception);

        /**
         * create C++ objects from foreign representations
         * @param address the place where to find the foreign
         * representations
         * @return the C++ objects created from the representations
         * or empty vector if unsuccessful. Note that the caller is
         * responsible for the deallocation of the newly created objects
         * @exception Exception throws an Exception in case of error
         */
        virtual std::vector<castor::IObject*> bulkCreateObj(castor::IAddress* address)
          throw (castor::exception::Exception);

        /**
         * Updates C++ object from its foreign representation.
         * @param obj the object to deal with
         * @exception Exception throws an Exception in case of error
         */
        virtual void updateObj(castor::IObject* obj)
          throw (castor::exception::Exception);

        /**
         * Fill the foreign representation with some of the objects.refered by a given C++
         * object.
         * @param address the place where to find the foreign representation
         * @param object the original C++ object
         * @param type the type of the refered objects to store
         * @param endTransaction whether the changes to the database
         * should be commited or not
         * @exception Exception throws an Exception in case of error
         */
        virtual void fillRep(castor::IAddress* address,
                             castor::IObject* object,
                             unsigned int type,
                             bool endTransaction)
          throw (castor::exception::Exception);

        /**
         * Fill the database with objects of type SrmUser refered by a given object.
         * @param obj the original object
         * @exception Exception throws an Exception in case of error
         */
        virtual void fillRepSrmUser(srm::StageRequest* obj)
          throw (castor::exception::Exception);

        /**
         * Fill the database with objects of type SubRequest refered by a given object.
         * @param obj the original object
         * @exception Exception throws an Exception in case of error
         */
        virtual void fillRepSubRequest(srm::StageRequest* obj)
          throw (castor::exception::Exception);

        /**
         * Fill the database with objects of type CopySubRequest refered by a given
         * object.
         * @param obj the original object
         * @exception Exception throws an Exception in case of error
         */
        virtual void fillRepCopySubRequest(srm::StageRequest* obj)
          throw (castor::exception::Exception);

        /**
         * Retrieve from the database some of the objects refered by a given object.
         * @param object the original object
         * @param type the type of the refered objects to retrieve
         * @exception Exception throws an Exception in case of error
         */
        virtual void fillObj(castor::IAddress* address,
                             castor::IObject* object,
                             unsigned int type,
                             bool endTransaction)
          throw (castor::exception::Exception);

        /**
         * Retrieve from the database objects of type SrmUser refered by a given object.
         * @param obj the original object
         * @exception Exception throws an Exception in case of error
         */
        virtual void fillObjSrmUser(srm::StageRequest* obj)
          throw (castor::exception::Exception);

        /**
         * Retrieve from the database objects of type SubRequest refered by a given
         * object.
         * @param obj the original object
         * @exception Exception throws an Exception in case of error
         */
        virtual void fillObjSubRequest(srm::StageRequest* obj)
          throw (castor::exception::Exception);

        /**
         * Retrieve from the database objects of type CopySubRequest refered by a given
         * object.
         * @param obj the original object
         * @exception Exception throws an Exception in case of error
         */
        virtual void fillObjCopySubRequest(srm::StageRequest* obj)
          throw (castor::exception::Exception);

      private:

        /// SQL statement for request insertion
        static const std::string s_insertStatementString;

        /// SQL statement object for request insertion
        castor::db::IDbStatement *m_insertStatement;

        /// SQL statement for request bulk insertion
        static const std::string s_bulkInsertStatementString;

        /// SQL statement object for request bulk insertion
        castor::db::IDbStatement *m_bulkInsertStatement;

        /// SQL statement for request deletion
        static const std::string s_deleteStatementString;

        /// SQL statement object for request deletion
        castor::db::IDbStatement *m_deleteStatement;

        /// SQL statement for request selection
        static const std::string s_selectStatementString;

        /// SQL statement object for request selection
        castor::db::IDbStatement *m_selectStatement;

        /// SQL statement for request bulk selection
        static const std::string s_bulkSelectStatementString;

        /// SQL statement object for request bulk selection
        castor::db::IDbStatement *m_bulkSelectStatement;

        /// SQL statement for request update
        static const std::string s_updateStatementString;

        /// SQL statement object for request update
        castor::db::IDbStatement *m_updateStatement;

        /// SQL checkExist statement for member srmUser
        static const std::string s_checkSrmUserExistStatementString;

        /// SQL checkExist statement object for member srmUser
        castor::db::IDbStatement *m_checkSrmUserExistStatement;

        /// SQL update statement for member srmUser
        static const std::string s_updateSrmUserStatementString;

        /// SQL update statement object for member srmUser
        castor::db::IDbStatement *m_updateSrmUserStatement;

        /// SQL select statement for member subRequests
        static const std::string s_selectSubRequestStatementString;

        /// SQL select statement object for member subRequests
        castor::db::IDbStatement *m_selectSubRequestStatement;

        /// SQL delete statement for member subRequests
        static const std::string s_deleteSubRequestStatementString;

        /// SQL delete statement object for member subRequests
        castor::db::IDbStatement *m_deleteSubRequestStatement;

        /// SQL remote update statement for member subRequests
        static const std::string s_remoteUpdateSubRequestStatementString;

        /// SQL remote update statement object for member subRequests
        castor::db::IDbStatement *m_remoteUpdateSubRequestStatement;

        /// SQL select statement for member copySubRequests
        static const std::string s_selectCopySubRequestStatementString;

        /// SQL select statement object for member copySubRequests
        castor::db::IDbStatement *m_selectCopySubRequestStatement;

        /// SQL delete statement for member copySubRequests
        static const std::string s_deleteCopySubRequestStatementString;

        /// SQL delete statement object for member copySubRequests
        castor::db::IDbStatement *m_deleteCopySubRequestStatement;

        /// SQL remote update statement for member copySubRequests
        static const std::string s_remoteUpdateCopySubRequestStatementString;

        /// SQL remote update statement object for member copySubRequests
        castor::db::IDbStatement *m_remoteUpdateCopySubRequestStatement;

      }; // end of class DbStageRequestCnv

    } /* end of namespace cnv */

  } /* end of namespace db */

} } /* end of namespace castor/srm */

#endif // SRM_DB_CNV_STAGEREQUEST_HPP
