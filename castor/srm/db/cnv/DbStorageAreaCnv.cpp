/**** This file has been autogenerated by gencastor from Umbrello UML model ***/

/******************************************************************************
 *                      srm/db/cnv/DbStorageAreaCnv.cpp
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

// Include Files
#include "DbStorageAreaCnv.hpp"
#include "castor/BaseAddress.hpp"
#include "castor/CnvFactory.hpp"
#include "castor/Constants.hpp"
#include "castor/IAddress.hpp"
#include "castor/ICnvSvc.hpp"
#include "castor/IObject.hpp"
#include "castor/VectorAddress.hpp"
#include "castor/db/DbCnvSvc.hpp"
#include "castor/exception/Exception.hpp"
#include "castor/exception/Exception.hpp"
#include "castor/exception/InvalidArgument.hpp"
#include "castor/exception/NoEntry.hpp"
#include "castor/exception/OutOfMemory.hpp"
#include "castor/srm/Constants.hpp"
#include "castor/srm/SrmUser.hpp"
#include "castor/srm/StorageArea.hpp"
#include "castor/srm/StorageStatus.hpp"
#include <stdlib.h>
#include <vector>

//------------------------------------------------------------------------------
// Instantiation of a static factory class - should never be used
//------------------------------------------------------------------------------
static castor::CnvFactory<castor::srm::db::cnv::DbStorageAreaCnv>* s_factoryDbStorageAreaCnv =
  new castor::CnvFactory<castor::srm::db::cnv::DbStorageAreaCnv>();

//------------------------------------------------------------------------------
// Static constants initialization
//------------------------------------------------------------------------------
/// SQL statement for request insertion
const std::string castor::srm::db::cnv::DbStorageAreaCnv::s_insertStatementString =
"INSERT INTO StorageArea (spaceToken, tokenDescription, storageLifetime, svcClass, storageType, id, srmUser, parent, storageStatus) VALUES (:1,:2,:3,:4,:5,ids_seq.nextval,:6,:7,:8) RETURNING id INTO :9";

/// SQL statement for request bulk insertion
const std::string castor::srm::db::cnv::DbStorageAreaCnv::s_bulkInsertStatementString =
"INSERT /* bulk */ INTO StorageArea (spaceToken, tokenDescription, storageLifetime, svcClass, storageType, id, srmUser, parent, storageStatus) VALUES (:1,:2,:3,:4,:5,ids_seq.nextval,:6,:7,:8) RETURNING id INTO :9";

/// SQL statement for request deletion
const std::string castor::srm::db::cnv::DbStorageAreaCnv::s_deleteStatementString =
"DELETE FROM StorageArea WHERE id = :1";

/// SQL statement for request selection
const std::string castor::srm::db::cnv::DbStorageAreaCnv::s_selectStatementString =
"SELECT spaceToken, tokenDescription, storageLifetime, svcClass, storageType, id, srmUser, parent, storageStatus FROM StorageArea WHERE id = :1";

/// SQL statement for bulk request selection
const std::string castor::srm::db::cnv::DbStorageAreaCnv::s_bulkSelectStatementString =
"DECLARE \
   TYPE RecordType IS RECORD (spaceToken VARCHAR2(2048), tokenDescription VARCHAR2(2048), storageLifetime INTEGER, svcClass VARCHAR2(2048), storageType NUMBER, id INTEGER, srmUser INTEGER, parent INTEGER, storageStatus INTEGER); \
   TYPE CurType IS REF CURSOR RETURN RecordType; \
   PROCEDURE bulkSelect(ids IN castor.\"cnumList\", \
                        objs OUT CurType) AS \
   BEGIN \
     FORALL i IN ids.FIRST..ids.LAST \
       INSERT INTO bulkSelectHelper VALUES(ids(i)); \
     OPEN objs FOR SELECT spaceToken, tokenDescription, storageLifetime, svcClass, storageType, id, srmUser, parent, storageStatus \
                     FROM StorageArea t, bulkSelectHelper h \
                    WHERE t.id = h.objId; \
     DELETE FROM bulkSelectHelper; \
   END; \
 BEGIN \
   bulkSelect(:1, :2); \
 END;";

/// SQL statement for request update
const std::string castor::srm::db::cnv::DbStorageAreaCnv::s_updateStatementString =
"UPDATE StorageArea SET spaceToken = :1, tokenDescription = :2, storageLifetime = :3, svcClass = :4, storageType = :5, storageStatus = :6 WHERE id = :7";

/// SQL existence statement for member srmUser
const std::string castor::srm::db::cnv::DbStorageAreaCnv::s_checkSrmUserExistStatementString =
"SELECT id FROM SrmUser WHERE id = :1";

/// SQL update statement for member srmUser
const std::string castor::srm::db::cnv::DbStorageAreaCnv::s_updateSrmUserStatementString =
"UPDATE StorageArea SET srmUser = :1 WHERE id = :2";

/// SQL existence statement for member parent
const std::string castor::srm::db::cnv::DbStorageAreaCnv::s_checkStorageAreaExistStatementString =
"SELECT id FROM StorageArea WHERE id = :1";

/// SQL update statement for member parent
const std::string castor::srm::db::cnv::DbStorageAreaCnv::s_updateStorageAreaStatementString =
"UPDATE StorageArea SET parent = :1 WHERE id = :2";

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
castor::srm::db::cnv::DbStorageAreaCnv::DbStorageAreaCnv(castor::ICnvSvc* cnvSvc) :
  DbBaseCnv(cnvSvc),
  m_insertStatement(0),
  m_bulkInsertStatement(0),
  m_deleteStatement(0),
  m_selectStatement(0),
  m_bulkSelectStatement(0),
  m_updateStatement(0),
  m_checkSrmUserExistStatement(0),
  m_updateSrmUserStatement(0),
  m_checkStorageAreaExistStatement(0),
  m_updateStorageAreaStatement(0) {}

//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
castor::srm::db::cnv::DbStorageAreaCnv::~DbStorageAreaCnv() throw() {
  //Here we attempt to delete the statements correctly
  // If something goes wrong, we just ignore it
  try {
    if(m_insertStatement) delete m_insertStatement;
    if(m_bulkInsertStatement) delete m_bulkInsertStatement;
    if(m_deleteStatement) delete m_deleteStatement;
    if(m_selectStatement) delete m_selectStatement;
    if(m_bulkSelectStatement) delete m_bulkSelectStatement;
    if(m_updateStatement) delete m_updateStatement;
    if(m_checkSrmUserExistStatement) delete m_checkSrmUserExistStatement;
    if(m_updateSrmUserStatement) delete m_updateSrmUserStatement;
    if(m_checkStorageAreaExistStatement) delete m_checkStorageAreaExistStatement;
    if(m_updateStorageAreaStatement) delete m_updateStorageAreaStatement;
  } catch (castor::exception::Exception& ignored) {};
}

//------------------------------------------------------------------------------
// ObjType
//------------------------------------------------------------------------------
unsigned int castor::srm::db::cnv::DbStorageAreaCnv::ObjType() {
  return castor::srm::StorageArea::TYPE();
}

//------------------------------------------------------------------------------
// objType
//------------------------------------------------------------------------------
unsigned int castor::srm::db::cnv::DbStorageAreaCnv::objType() const {
  return ObjType();
}

//------------------------------------------------------------------------------
// fillRep
//------------------------------------------------------------------------------
void castor::srm::db::cnv::DbStorageAreaCnv::fillRep(castor::IAddress*,
                                             castor::IObject* object,
                                             unsigned int type,
                                             bool endTransaction)
  throw (castor::exception::Exception) {
  castor::srm::StorageArea* obj = 
    dynamic_cast<castor::srm::StorageArea*>(object);
  try {
    switch (type) {
    case castor::srm::OBJ_SrmUser :
      fillRepSrmUser(obj);
      break;
    case castor::srm::OBJ_StorageArea :
      fillRepStorageArea(obj);
      break;
    default :
      castor::exception::InvalidArgument ex;
      ex.getMessage() << "fillRep called for type " << type 
                      << " on object of type " << obj->type() 
                      << ". This is meaningless.";
      throw ex;
    }
    if (endTransaction) {
      cnvSvc()->commit();
    }
  } catch (castor::exception::SQLError& e) {
    castor::exception::Exception ex;
    ex.getMessage() << "Error in fillRep for type " << type
                    << std::endl << e.getMessage().str() << std::endl;
    throw ex;
  }
}

//------------------------------------------------------------------------------
// fillRepSrmUser
//------------------------------------------------------------------------------
void castor::srm::db::cnv::DbStorageAreaCnv::fillRepSrmUser(castor::srm::StorageArea* obj)
  throw (castor::exception::Exception) {
  if (0 != obj->srmUser()) {
    // Check checkSrmUserExist statement
    if (0 == m_checkSrmUserExistStatement) {
      m_checkSrmUserExistStatement = createStatement(s_checkSrmUserExistStatementString);
    }
    // retrieve the object from the database
    m_checkSrmUserExistStatement->setUInt64(1, obj->srmUser()->id());
    castor::db::IDbResultSet *rset = m_checkSrmUserExistStatement->executeQuery();
    if (!rset->next()) {
      castor::BaseAddress ad;
      ad.setCnvSvcName("DbCnvSvc");
      ad.setCnvSvcType(castor::SVC_DBCNV);
      cnvSvc()->createRep(&ad, obj->srmUser(), false);
    }
    // Close resultset
    delete rset;
  }
  // Check update statement
  if (0 == m_updateSrmUserStatement) {
    m_updateSrmUserStatement = createStatement(s_updateSrmUserStatementString);
  }
  // Update local object
  m_updateSrmUserStatement->setUInt64(1, 0 == obj->srmUser() ? 0 : obj->srmUser()->id());
  m_updateSrmUserStatement->setUInt64(2, obj->id());
  m_updateSrmUserStatement->execute();
}

//------------------------------------------------------------------------------
// fillRepStorageArea
//------------------------------------------------------------------------------
void castor::srm::db::cnv::DbStorageAreaCnv::fillRepStorageArea(castor::srm::StorageArea* obj)
  throw (castor::exception::Exception) {
  if (0 != obj->parent()) {
    // Check checkStorageAreaExist statement
    if (0 == m_checkStorageAreaExistStatement) {
      m_checkStorageAreaExistStatement = createStatement(s_checkStorageAreaExistStatementString);
    }
    // retrieve the object from the database
    m_checkStorageAreaExistStatement->setUInt64(1, obj->parent()->id());
    castor::db::IDbResultSet *rset = m_checkStorageAreaExistStatement->executeQuery();
    if (!rset->next()) {
      castor::BaseAddress ad;
      ad.setCnvSvcName("DbCnvSvc");
      ad.setCnvSvcType(castor::SVC_DBCNV);
      cnvSvc()->createRep(&ad, obj->parent(), false);
    }
    // Close resultset
    delete rset;
  }
  // Check update statement
  if (0 == m_updateStorageAreaStatement) {
    m_updateStorageAreaStatement = createStatement(s_updateStorageAreaStatementString);
  }
  // Update local object
  m_updateStorageAreaStatement->setUInt64(1, 0 == obj->parent() ? 0 : obj->parent()->id());
  m_updateStorageAreaStatement->setUInt64(2, obj->id());
  m_updateStorageAreaStatement->execute();
}

//------------------------------------------------------------------------------
// fillObj
//------------------------------------------------------------------------------
void castor::srm::db::cnv::DbStorageAreaCnv::fillObj(castor::IAddress*,
                                             castor::IObject* object,
                                             unsigned int type,
                                             bool endTransaction)
  throw (castor::exception::Exception) {
  castor::srm::StorageArea* obj = 
    dynamic_cast<castor::srm::StorageArea*>(object);
  switch (type) {
  case castor::srm::OBJ_SrmUser :
    fillObjSrmUser(obj);
    break;
  case castor::srm::OBJ_StorageArea :
    fillObjStorageArea(obj);
    break;
  default :
    castor::exception::InvalidArgument ex;
    ex.getMessage() << "fillObj called on type " << type 
                    << " on object of type " << obj->type() 
                    << ". This is meaningless.";
    throw ex;
  }
  if (endTransaction) {
    cnvSvc()->commit();
  }
}

//------------------------------------------------------------------------------
// fillObjSrmUser
//------------------------------------------------------------------------------
void castor::srm::db::cnv::DbStorageAreaCnv::fillObjSrmUser(castor::srm::StorageArea* obj)
  throw (castor::exception::Exception) {
  // Check whether the statement is ok
  if (0 == m_selectStatement) {
    m_selectStatement = createStatement(s_selectStatementString);
  }
  // retrieve the object from the database
  m_selectStatement->setUInt64(1, obj->id());
  castor::db::IDbResultSet *rset = m_selectStatement->executeQuery();
  if (!rset->next()) {
    castor::exception::NoEntry ex;
    ex.getMessage() << "No object found for id :" << obj->id();
    throw ex;
  }
  u_signed64 srmUserId = rset->getInt64(7);
  // Close ResultSet
  delete rset;
  // Check whether something should be deleted
  if (0 != obj->srmUser() &&
      (0 == srmUserId ||
       obj->srmUser()->id() != srmUserId)) {
    obj->srmUser()->removeStorageArea(obj);
    obj->setSrmUser(0);
  }
  // Update object or create new one
  if (0 != srmUserId) {
    if (0 == obj->srmUser()) {
      obj->setSrmUser
        (dynamic_cast<castor::srm::SrmUser*>
         (cnvSvc()->getObjFromId(srmUserId, OBJ_SrmUser)));
    } else {
      cnvSvc()->updateObj(obj->srmUser());
    }
    obj->srmUser()->addStorageArea(obj);
  }
}

//------------------------------------------------------------------------------
// fillObjStorageArea
//------------------------------------------------------------------------------
void castor::srm::db::cnv::DbStorageAreaCnv::fillObjStorageArea(castor::srm::StorageArea* obj)
  throw (castor::exception::Exception) {
  // Check whether the statement is ok
  if (0 == m_selectStatement) {
    m_selectStatement = createStatement(s_selectStatementString);
  }
  // retrieve the object from the database
  m_selectStatement->setUInt64(1, obj->id());
  castor::db::IDbResultSet *rset = m_selectStatement->executeQuery();
  if (!rset->next()) {
    castor::exception::NoEntry ex;
    ex.getMessage() << "No object found for id :" << obj->id();
    throw ex;
  }
  u_signed64 parentId = rset->getInt64(8);
  // Close ResultSet
  delete rset;
  // Check whether something should be deleted
  if (0 != obj->parent() &&
      (0 == parentId ||
       obj->parent()->id() != parentId)) {
    obj->parent()->removeChild(obj);
    obj->setParent(0);
  }
  // Update object or create new one
  if (0 != parentId) {
    if (0 == obj->parent()) {
      obj->setParent
        (dynamic_cast<castor::srm::StorageArea*>
         (cnvSvc()->getObjFromId(parentId, OBJ_StorageArea)));
    } else {
      cnvSvc()->updateObj(obj->parent());
    }
    obj->parent()->addChild(obj);
  }
}

//------------------------------------------------------------------------------
// createRep
//------------------------------------------------------------------------------
void castor::srm::db::cnv::DbStorageAreaCnv::createRep(castor::IAddress*,
                                               castor::IObject* object,
                                               bool endTransaction,
                                               unsigned int type)
  throw (castor::exception::Exception) {
  castor::srm::StorageArea* obj = 
    dynamic_cast<castor::srm::StorageArea*>(object);
  // check whether something needs to be done
  if (0 == obj) return;
  if (0 != obj->id()) return;
  try {
    // Check whether the statements are ok
    if (0 == m_insertStatement) {
      m_insertStatement = createStatement(s_insertStatementString);
      m_insertStatement->registerOutParam(9, castor::db::DBTYPE_UINT64);
    }
    // Now Save the current object
    m_insertStatement->setString(1, obj->spaceToken());
    m_insertStatement->setString(2, obj->tokenDescription());
    m_insertStatement->setUInt64(3, obj->storageLifetime());
    m_insertStatement->setString(4, obj->svcClass());
    m_insertStatement->setInt(5, obj->storageType());
    m_insertStatement->setUInt64(6, (type == OBJ_SrmUser && obj->srmUser() != 0) ? obj->srmUser()->id() : 0);
    m_insertStatement->setUInt64(7, (type == OBJ_StorageArea && obj->parent() != 0) ? obj->parent()->id() : 0);
    m_insertStatement->setInt(8, (int)obj->storageStatus());
    m_insertStatement->execute();
    obj->setId(m_insertStatement->getUInt64(9));
    if (endTransaction) {
      cnvSvc()->commit();
    }
  } catch (castor::exception::SQLError& e) {
    // Always try to rollback
    try {
      if (endTransaction) cnvSvc()->rollback();
    } catch (castor::exception::Exception& ignored) {}
    castor::exception::InvalidArgument ex;
    ex.getMessage() << "Error in insert request :"
                    << std::endl << e.getMessage().str() << std::endl
                    << "Statement was : " << std::endl
                    << s_insertStatementString << std::endl
                    << " and parameters' values were :" << std::endl
                    << "  spaceToken : " << obj->spaceToken() << std::endl
                    << "  tokenDescription : " << obj->tokenDescription() << std::endl
                    << "  storageLifetime : " << obj->storageLifetime() << std::endl
                    << "  svcClass : " << obj->svcClass() << std::endl
                    << "  storageType : " << obj->storageType() << std::endl
                    << "  id : " << obj->id() << std::endl
                    << "  srmUser : " << (obj->srmUser() ? obj->srmUser()->id() : 0) << std::endl
                    << "  parent : " << (obj->parent() ? obj->parent()->id() : 0) << std::endl
                    << "  storageStatus : " << obj->storageStatus() << std::endl;
    throw ex;
  }
}

//------------------------------------------------------------------------------
// bulkCreateRep
//------------------------------------------------------------------------------
void castor::srm::db::cnv::DbStorageAreaCnv::bulkCreateRep(castor::IAddress*,
                                                   std::vector<castor::IObject*> &objects,
                                                   bool endTransaction,
                                                   unsigned int type)
  throw (castor::exception::Exception) {
  // check whether something needs to be done
  int nb = objects.size();
  if (0 == nb) return;
  // Casts all objects
  std::vector<castor::srm::StorageArea*> objs;
  for (int i = 0; i < nb; i++) {
    objs.push_back(dynamic_cast<castor::srm::StorageArea*>(objects[i]));
  }
  std::vector<void *> allocMem;
  try {
    // Check whether the statements are ok
    if (0 == m_bulkInsertStatement) {
      m_bulkInsertStatement = createStatement(s_bulkInsertStatementString);
      m_bulkInsertStatement->registerOutParam(9, castor::db::DBTYPE_UINT64);
    }
    // build the buffers for spaceToken
    unsigned int spaceTokenMaxLen = 0;
    for (int i = 0; i < nb; i++) {
      if (objs[i]->spaceToken().length()+1 > spaceTokenMaxLen)
        spaceTokenMaxLen = objs[i]->spaceToken().length()+1;
    }
    char* spaceTokenBuffer = (char*) calloc(nb, spaceTokenMaxLen);
    if (spaceTokenBuffer == 0) {
      castor::exception::OutOfMemory e;
      throw e;
    }
    allocMem.push_back(spaceTokenBuffer);
    unsigned short* spaceTokenBufLens = (unsigned short*) malloc(nb * sizeof(unsigned short));
    if (spaceTokenBufLens == 0) {
      castor::exception::OutOfMemory e;
      throw e;
    }
    allocMem.push_back(spaceTokenBufLens);
    for (int i = 0; i < nb; i++) {
      strncpy(spaceTokenBuffer+(i*spaceTokenMaxLen), objs[i]->spaceToken().c_str(), spaceTokenMaxLen);
      spaceTokenBufLens[i] = objs[i]->spaceToken().length()+1; // + 1 for the trailing \0
    }
    m_bulkInsertStatement->setDataBuffer
      (1, spaceTokenBuffer, castor::db::DBTYPE_STRING, spaceTokenMaxLen, spaceTokenBufLens);
    // build the buffers for tokenDescription
    unsigned int tokenDescriptionMaxLen = 0;
    for (int i = 0; i < nb; i++) {
      if (objs[i]->tokenDescription().length()+1 > tokenDescriptionMaxLen)
        tokenDescriptionMaxLen = objs[i]->tokenDescription().length()+1;
    }
    char* tokenDescriptionBuffer = (char*) calloc(nb, tokenDescriptionMaxLen);
    if (tokenDescriptionBuffer == 0) {
      castor::exception::OutOfMemory e;
      throw e;
    }
    allocMem.push_back(tokenDescriptionBuffer);
    unsigned short* tokenDescriptionBufLens = (unsigned short*) malloc(nb * sizeof(unsigned short));
    if (tokenDescriptionBufLens == 0) {
      castor::exception::OutOfMemory e;
      throw e;
    }
    allocMem.push_back(tokenDescriptionBufLens);
    for (int i = 0; i < nb; i++) {
      strncpy(tokenDescriptionBuffer+(i*tokenDescriptionMaxLen), objs[i]->tokenDescription().c_str(), tokenDescriptionMaxLen);
      tokenDescriptionBufLens[i] = objs[i]->tokenDescription().length()+1; // + 1 for the trailing \0
    }
    m_bulkInsertStatement->setDataBuffer
      (2, tokenDescriptionBuffer, castor::db::DBTYPE_STRING, tokenDescriptionMaxLen, tokenDescriptionBufLens);
    // build the buffers for storageLifetime
    double* storageLifetimeBuffer = (double*) malloc(nb * sizeof(double));
    if (storageLifetimeBuffer == 0) {
      castor::exception::OutOfMemory e;
      throw e;
    }
    allocMem.push_back(storageLifetimeBuffer);
    unsigned short* storageLifetimeBufLens = (unsigned short*) malloc(nb * sizeof(unsigned short));
    if (storageLifetimeBufLens == 0) {
      castor::exception::OutOfMemory e;
      throw e;
    }
    allocMem.push_back(storageLifetimeBufLens);
    for (int i = 0; i < nb; i++) {
      storageLifetimeBuffer[i] = objs[i]->storageLifetime();
      storageLifetimeBufLens[i] = sizeof(double);
    }
    m_bulkInsertStatement->setDataBuffer
      (3, storageLifetimeBuffer, castor::db::DBTYPE_UINT64, sizeof(storageLifetimeBuffer[0]), storageLifetimeBufLens);
    // build the buffers for svcClass
    unsigned int svcClassMaxLen = 0;
    for (int i = 0; i < nb; i++) {
      if (objs[i]->svcClass().length()+1 > svcClassMaxLen)
        svcClassMaxLen = objs[i]->svcClass().length()+1;
    }
    char* svcClassBuffer = (char*) calloc(nb, svcClassMaxLen);
    if (svcClassBuffer == 0) {
      castor::exception::OutOfMemory e;
      throw e;
    }
    allocMem.push_back(svcClassBuffer);
    unsigned short* svcClassBufLens = (unsigned short*) malloc(nb * sizeof(unsigned short));
    if (svcClassBufLens == 0) {
      castor::exception::OutOfMemory e;
      throw e;
    }
    allocMem.push_back(svcClassBufLens);
    for (int i = 0; i < nb; i++) {
      strncpy(svcClassBuffer+(i*svcClassMaxLen), objs[i]->svcClass().c_str(), svcClassMaxLen);
      svcClassBufLens[i] = objs[i]->svcClass().length()+1; // + 1 for the trailing \0
    }
    m_bulkInsertStatement->setDataBuffer
      (4, svcClassBuffer, castor::db::DBTYPE_STRING, svcClassMaxLen, svcClassBufLens);
    // build the buffers for storageType
    int* storageTypeBuffer = (int*) malloc(nb * sizeof(int));
    if (storageTypeBuffer == 0) {
      castor::exception::OutOfMemory e;
      throw e;
    }
    allocMem.push_back(storageTypeBuffer);
    unsigned short* storageTypeBufLens = (unsigned short*) malloc(nb * sizeof(unsigned short));
    if (storageTypeBufLens == 0) {
      castor::exception::OutOfMemory e;
      throw e;
    }
    allocMem.push_back(storageTypeBufLens);
    for (int i = 0; i < nb; i++) {
      storageTypeBuffer[i] = objs[i]->storageType();
      storageTypeBufLens[i] = sizeof(int);
    }
    m_bulkInsertStatement->setDataBuffer
      (5, storageTypeBuffer, castor::db::DBTYPE_INT, sizeof(storageTypeBuffer[0]), storageTypeBufLens);
    // build the buffers for srmUser
    double* srmUserBuffer = (double*) malloc(nb * sizeof(double));
    if (srmUserBuffer == 0) {
      castor::exception::OutOfMemory e;
      throw e;
    }
    allocMem.push_back(srmUserBuffer);
    unsigned short* srmUserBufLens = (unsigned short*) malloc(nb * sizeof(unsigned short));
    if (srmUserBufLens == 0) {
      castor::exception::OutOfMemory e;
      throw e;
    }
    allocMem.push_back(srmUserBufLens);
    for (int i = 0; i < nb; i++) {
      srmUserBuffer[i] = (type == OBJ_SrmUser && objs[i]->srmUser() != 0) ? objs[i]->srmUser()->id() : 0;
      srmUserBufLens[i] = sizeof(double);
    }
    m_bulkInsertStatement->setDataBuffer
      (6, srmUserBuffer, castor::db::DBTYPE_UINT64, sizeof(srmUserBuffer[0]), srmUserBufLens);
    // build the buffers for parent
    double* parentBuffer = (double*) malloc(nb * sizeof(double));
    if (parentBuffer == 0) {
      castor::exception::OutOfMemory e;
      throw e;
    }
    allocMem.push_back(parentBuffer);
    unsigned short* parentBufLens = (unsigned short*) malloc(nb * sizeof(unsigned short));
    if (parentBufLens == 0) {
      castor::exception::OutOfMemory e;
      throw e;
    }
    allocMem.push_back(parentBufLens);
    for (int i = 0; i < nb; i++) {
      parentBuffer[i] = (type == OBJ_StorageArea && objs[i]->parent() != 0) ? objs[i]->parent()->id() : 0;
      parentBufLens[i] = sizeof(double);
    }
    m_bulkInsertStatement->setDataBuffer
      (7, parentBuffer, castor::db::DBTYPE_UINT64, sizeof(parentBuffer[0]), parentBufLens);
    // build the buffers for storageStatus
    int* storageStatusBuffer = (int*) malloc(nb * sizeof(int));
    if (storageStatusBuffer == 0) {
      castor::exception::OutOfMemory e;
      throw e;
    }
    allocMem.push_back(storageStatusBuffer);
    unsigned short* storageStatusBufLens = (unsigned short*) malloc(nb * sizeof(unsigned short));
    if (storageStatusBufLens == 0) {
      castor::exception::OutOfMemory e;
      throw e;
    }
    allocMem.push_back(storageStatusBufLens);
    for (int i = 0; i < nb; i++) {
      storageStatusBuffer[i] = objs[i]->storageStatus();
      storageStatusBufLens[i] = sizeof(int);
    }
    m_bulkInsertStatement->setDataBuffer
      (8, storageStatusBuffer, castor::db::DBTYPE_INT, sizeof(storageStatusBuffer[0]), storageStatusBufLens);
    // build the buffers for returned ids
    double* idBuffer = (double*) calloc(nb, sizeof(double));
    if (idBuffer == 0) {
      castor::exception::OutOfMemory e;
      throw e;
    }
    allocMem.push_back(idBuffer);
    unsigned short* idBufLens = (unsigned short*) calloc(nb, sizeof(unsigned short));
    if (idBufLens == 0) {
      castor::exception::OutOfMemory e;
      throw e;
    }
    allocMem.push_back(idBufLens);
    m_bulkInsertStatement->setDataBuffer
      (9, idBuffer, castor::db::DBTYPE_UINT64, sizeof(double), idBufLens);
    m_bulkInsertStatement->execute(nb);
    for (int i = 0; i < nb; i++) {
      objects[i]->setId((u_signed64)idBuffer[i]);
    }
    // release the buffers
    for (unsigned int i = 0; i < allocMem.size(); i++) {
      free(allocMem[i]);
    }
    if (endTransaction) {
      cnvSvc()->commit();
    }
  } catch (castor::exception::SQLError& e) {
    // release the buffers
    for (unsigned int i = 0; i < allocMem.size(); i++) {
      free(allocMem[i]);
    }
    // Always try to rollback
    try {
      if (endTransaction) cnvSvc()->rollback();
    } catch (castor::exception::Exception& ignored) {}
    castor::exception::InvalidArgument ex;
    ex.getMessage() << "Error in bulkInsert request :"
                    << std::endl << e.getMessage().str() << std::endl
                    << " was called in bulk with "
                    << nb << " items." << std::endl;
    throw ex;
  }
}

//------------------------------------------------------------------------------
// updateRep
//------------------------------------------------------------------------------
void castor::srm::db::cnv::DbStorageAreaCnv::updateRep(castor::IAddress*,
                                               castor::IObject* object,
                                               bool endTransaction)
  throw (castor::exception::Exception) {
  castor::srm::StorageArea* obj = 
    dynamic_cast<castor::srm::StorageArea*>(object);
  // check whether something needs to be done
  if (0 == obj) return;
  try {
    // Check whether the statements are ok
    if (0 == m_updateStatement) {
      m_updateStatement = createStatement(s_updateStatementString);
    }
    // Update the current object
    m_updateStatement->setString(1, obj->spaceToken());
    m_updateStatement->setString(2, obj->tokenDescription());
    m_updateStatement->setUInt64(3, obj->storageLifetime());
    m_updateStatement->setString(4, obj->svcClass());
    m_updateStatement->setInt(5, obj->storageType());
    m_updateStatement->setInt(6, (int)obj->storageStatus());
    m_updateStatement->setUInt64(7, obj->id());
    m_updateStatement->execute();
    if (endTransaction) {
      cnvSvc()->commit();
    }
  } catch (castor::exception::SQLError& e) {
    // Always try to rollback
    try {
      if (endTransaction) cnvSvc()->rollback();
    } catch (castor::exception::Exception& ignored) {}
    castor::exception::InvalidArgument ex;
    ex.getMessage() << "Error in update request :"
                    << std::endl << e.getMessage().str() << std::endl
                    << "Statement was : " << std::endl
                    << s_updateStatementString << std::endl
                    << " and id was " << obj->id() << std::endl;;
    throw ex;
  }
}

//------------------------------------------------------------------------------
// deleteRep
//------------------------------------------------------------------------------
void castor::srm::db::cnv::DbStorageAreaCnv::deleteRep(castor::IAddress*,
                                               castor::IObject* object,
                                               bool endTransaction)
  throw (castor::exception::Exception) {
  castor::srm::StorageArea* obj = 
    dynamic_cast<castor::srm::StorageArea*>(object);
  // check whether something needs to be done
  if (0 == obj) return;
  try {
    // Check whether the statements are ok
    if (0 == m_deleteStatement) {
      m_deleteStatement = createStatement(s_deleteStatementString);
    }
    // Now Delete the object
    m_deleteStatement->setUInt64(1, obj->id());
    m_deleteStatement->execute();
    if (endTransaction) {
      cnvSvc()->commit();
    }
  } catch (castor::exception::SQLError& e) {
    // Always try to rollback
    try {
      if (endTransaction) cnvSvc()->rollback();
    } catch (castor::exception::Exception& ignored) {}
    castor::exception::InvalidArgument ex;
    ex.getMessage() << "Error in delete request :"
                    << std::endl << e.getMessage().str() << std::endl
                    << "Statement was : " << std::endl
                    << s_deleteStatementString << std::endl
                    << " and id was " << obj->id() << std::endl;;
    throw ex;
  }
}

//------------------------------------------------------------------------------
// createObj
//------------------------------------------------------------------------------
castor::IObject* castor::srm::db::cnv::DbStorageAreaCnv::createObj(castor::IAddress* address)
  throw (castor::exception::Exception) {
  castor::BaseAddress* ad = 
    dynamic_cast<castor::BaseAddress*>(address);
  try {
    // Check whether the statement is ok
    if (0 == m_selectStatement) {
      m_selectStatement = createStatement(s_selectStatementString);
    }
    // retrieve the object from the database
    m_selectStatement->setUInt64(1, ad->target());
    castor::db::IDbResultSet *rset = m_selectStatement->executeQuery();
    if (!rset->next()) {
      castor::exception::NoEntry ex;
      ex.getMessage() << "No object found for id :" << ad->target();
      throw ex;
    }
    // create the new Object
    castor::srm::StorageArea* object = new castor::srm::StorageArea();
    // Now retrieve and set members
    object->setSpaceToken(rset->getString(1));
    object->setTokenDescription(rset->getString(2));
    object->setStorageLifetime(rset->getUInt64(3));
    object->setSvcClass(rset->getString(4));
    object->setStorageType(rset->getInt(5));
    object->setId(rset->getUInt64(6));
    object->setStorageStatus((enum castor::srm::StorageStatus)rset->getInt(9));
    delete rset;
    return object;
  } catch (castor::exception::SQLError& e) {
    castor::exception::InvalidArgument ex;
    ex.getMessage() << "Error in select request :"
                    << std::endl << e.getMessage().str() << std::endl
                    << "Statement was : " << std::endl
                    << s_selectStatementString << std::endl
                    << " and id was " << ad->target() << std::endl;;
    throw ex;
  }
}

//------------------------------------------------------------------------------
// bulkCreateObj
//------------------------------------------------------------------------------
std::vector<castor::IObject*>
castor::srm::db::cnv::DbStorageAreaCnv::bulkCreateObj(castor::IAddress* address)
  throw (castor::exception::Exception) {
  // Prepare result
  std::vector<castor::IObject*> res;
  // check whether something needs to be done
  castor::VectorAddress* ad = 
    dynamic_cast<castor::VectorAddress*>(address);
  int nb = ad->target().size();
  if (0 == nb) return res;
  try {
    // Check whether the statement is ok
    if (0 == m_bulkSelectStatement) {
      m_bulkSelectStatement = createStatement(s_bulkSelectStatementString);
      m_bulkSelectStatement->registerOutParam(2, castor::db::DBTYPE_CURSOR);
    }
    // set the buffer for input ids
    m_bulkSelectStatement->setDataBufferUInt64Array(1, ad->target());
    // Execute statement
    m_bulkSelectStatement->execute();
    // get the result, that is a cursor on the selected rows
    castor::db::IDbResultSet *rset =
      m_bulkSelectStatement->getCursor(2);
    // loop and create the new objects
    bool status = rset->next();
    while (status) {
      // create the new Object
      castor::srm::StorageArea* object = new castor::srm::StorageArea();
      // Now retrieve and set members
      object->setSpaceToken(rset->getString(1));
      object->setTokenDescription(rset->getString(2));
      object->setStorageLifetime(rset->getUInt64(3));
      object->setSvcClass(rset->getString(4));
      object->setStorageType(rset->getInt(5));
      object->setId(rset->getUInt64(6));
      object->setStorageStatus((enum castor::srm::StorageStatus)rset->getInt(9));
      // store object in results and loop;
      res.push_back(object);
      status = rset->next();
    }
    delete rset;
    return res;
  } catch (castor::exception::SQLError& e) {
    castor::exception::InvalidArgument ex;
    ex.getMessage() << "Error in bulkSelect request :"
                    << std::endl << e.getMessage().str() << std::endl
                    << " was called in bulk with "
                    << nb << " items." << std::endl;
    throw ex;
  }
}

//------------------------------------------------------------------------------
// updateObj
//------------------------------------------------------------------------------
void castor::srm::db::cnv::DbStorageAreaCnv::updateObj(castor::IObject* obj)
  throw (castor::exception::Exception) {
  try {
    // Check whether the statement is ok
    if (0 == m_selectStatement) {
      m_selectStatement = createStatement(s_selectStatementString);
    }
    // retrieve the object from the database
    m_selectStatement->setUInt64(1, obj->id());
    castor::db::IDbResultSet *rset = m_selectStatement->executeQuery();
    if (!rset->next()) {
      castor::exception::NoEntry ex;
      ex.getMessage() << "No object found for id :" << obj->id();
      throw ex;
    }
    // Now retrieve and set members
    castor::srm::StorageArea* object = 
      dynamic_cast<castor::srm::StorageArea*>(obj);
    object->setSpaceToken(rset->getString(1));
    object->setTokenDescription(rset->getString(2));
    object->setStorageLifetime(rset->getUInt64(3));
    object->setSvcClass(rset->getString(4));
    object->setStorageType(rset->getInt(5));
    object->setId(rset->getUInt64(6));
    object->setStorageStatus((enum castor::srm::StorageStatus)rset->getInt(9));
    delete rset;
  } catch (castor::exception::SQLError& e) {
    castor::exception::InvalidArgument ex;
    ex.getMessage() << "Error in update request :"
                    << std::endl << e.getMessage().str() << std::endl
                    << "Statement was : " << std::endl
                    << s_updateStatementString << std::endl
                    << " and id was " << obj->id() << std::endl;;
    throw ex;
  }
}

