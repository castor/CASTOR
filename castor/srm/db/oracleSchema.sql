/* Type2Obj metatable definition */
CREATE TABLE Type2Obj (type INTEGER CONSTRAINT PK_Type2Obj_Type PRIMARY KEY, object VARCHAR2(100) CONSTRAINT NN_Type2Obj_Object NOT NULL, svcHandler VARCHAR2(100), CONSTRAINT UN_Type2Obj_typeObject UNIQUE (type, object));

/* ObjStatus metatable definition */
CREATE TABLE ObjStatus (object VARCHAR2(100) CONSTRAINT NN_ObjStatus_object NOT NULL, field VARCHAR2(100) CONSTRAINT NN_ObjStatus_field NOT NULL, statusCode INTEGER CONSTRAINT NN_ObjStatus_statusCode NOT NULL, statusName VARCHAR2(100) CONSTRAINT NN_ObjStatus_statusName NOT NULL, CONSTRAINT UN_ObjStatus_objectFieldCode UNIQUE (object, field, statusCode));

/* SQL statements for type StageRequest */
CREATE TABLE StageRequest (requestDesc VARCHAR2(2048), castorReqId VARCHAR2(2048), overwriteOption NUMBER, protocol VARCHAR2(2048), euid NUMBER, egid NUMBER, lastCheck INTEGER, nextCheck INTEGER, proxyCert CLOB, removeSourceFiles NUMBER, rhHost VARCHAR2(2048), rhPort NUMBER, creationTime INTEGER, svcClass VARCHAR2(2048), srmRequestId VARCHAR2(2048), handler NUMBER, expirationInterval INTEGER, id INTEGER CONSTRAINT PK_StageRequest_Id PRIMARY KEY, srmUser INTEGER, status INTEGER, requestType INTEGER) INITRANS 50 PCTFREE 50 ENABLE ROW MOVEMENT;

INSERT INTO ObjStatus (object, field, statusCode, statusName) VALUES ('StageRequest', 'status', 0, 'REQUEST_PENDING');
INSERT INTO ObjStatus (object, field, statusCode, statusName) VALUES ('StageRequest', 'status', 1, 'REQUEST_INPROGRESS');
INSERT INTO ObjStatus (object, field, statusCode, statusName) VALUES ('StageRequest', 'status', 2, 'REQUEST_READYTOPOLL');
INSERT INTO ObjStatus (object, field, statusCode, statusName) VALUES ('StageRequest', 'status', 4, 'REQUEST_DONE');

INSERT INTO ObjStatus (object, field, statusCode, statusName) VALUES ('StageRequest', 'requestType', 0, 'REQUESTTYPE_INVALID');
INSERT INTO ObjStatus (object, field, statusCode, statusName) VALUES ('StageRequest', 'requestType', 1, 'REQUESTTYPE_PUT');
INSERT INTO ObjStatus (object, field, statusCode, statusName) VALUES ('StageRequest', 'requestType', 2, 'REQUESTTYPE_GET');
INSERT INTO ObjStatus (object, field, statusCode, statusName) VALUES ('StageRequest', 'requestType', 3, 'REQUESTTYPE_COPYPUSH');
INSERT INTO ObjStatus (object, field, statusCode, statusName) VALUES ('StageRequest', 'requestType', 4, 'REQUESTTYPE_COPYPULL');
INSERT INTO ObjStatus (object, field, statusCode, statusName) VALUES ('StageRequest', 'requestType', 5, 'REQUESTTYPE_BOL');

/* SQL statements for type SrmUser */
CREATE TABLE SrmUser (userID VARCHAR2(2048), vo VARCHAR2(2048), dn VARCHAR2(2048), castorUser VARCHAR2(2048), id INTEGER CONSTRAINT PK_SrmUser_Id PRIMARY KEY) INITRANS 50 PCTFREE 50 ENABLE ROW MOVEMENT;

/* SQL statements for type UserFile */
CREATE TABLE UserFile (castorFileName VARCHAR2(2048), fileSize INTEGER, fileId INTEGER, nsHost VARCHAR2(2048), id INTEGER CONSTRAINT PK_UserFile_Id PRIMARY KEY, status INTEGER) INITRANS 50 PCTFREE 50 ENABLE ROW MOVEMENT;

INSERT INTO ObjStatus (object, field, statusCode, statusName) VALUES ('UserFile', 'status', 0, 'SRMFILE_ACTIVE');
INSERT INTO ObjStatus (object, field, statusCode, statusName) VALUES ('UserFile', 'status', 2, 'SRMFILE_GCCANDIDATE');

/* SQL statements for type StorageArea */
CREATE TABLE StorageArea (spaceToken VARCHAR2(2048), tokenDescription VARCHAR2(2048), storageLifetime INTEGER, svcClass VARCHAR2(2048), storageType NUMBER, id INTEGER CONSTRAINT PK_StorageArea_Id PRIMARY KEY, srmUser INTEGER, parent INTEGER, storageStatus INTEGER) INITRANS 50 PCTFREE 50 ENABLE ROW MOVEMENT;

INSERT INTO ObjStatus (object, field, statusCode, statusName) VALUES ('StorageArea', 'storageStatus', 0, 'STORAGESTATUS_REQUESTED');
INSERT INTO ObjStatus (object, field, statusCode, statusName) VALUES ('StorageArea', 'storageStatus', 1, 'STORAGESTATUS_ALLOCATED');
INSERT INTO ObjStatus (object, field, statusCode, statusName) VALUES ('StorageArea', 'storageStatus', 2, 'STORAGESTATUS_INUSE');
INSERT INTO ObjStatus (object, field, statusCode, statusName) VALUES ('StorageArea', 'storageStatus', 3, 'STORAGESTATUS_DEALLOCATED');
INSERT INTO ObjStatus (object, field, statusCode, statusName) VALUES ('StorageArea', 'storageStatus', 4, 'STORAGESTATUS_EXPIRED');

/* SQL statements for type SubRequest */
CREATE TABLE SubRequest (spaceToken VARCHAR2(2048), turl VARCHAR2(2048), castorFileName VARCHAR2(2048), reservedSize INTEGER, reason VARCHAR2(2048), errorCode NUMBER, surl VARCHAR2(2048), castorReqId VARCHAR2(2048), svcClass VARCHAR2(2048), id INTEGER CONSTRAINT PK_SubRequest_Id PRIMARY KEY, userFile INTEGER, request INTEGER, status INTEGER) INITRANS 50 PCTFREE 50 ENABLE ROW MOVEMENT;

INSERT INTO ObjStatus (object, field, statusCode, statusName) VALUES ('SubRequest', 'status', 0, 'SUBREQUEST_PENDING');
INSERT INTO ObjStatus (object, field, statusCode, statusName) VALUES ('SubRequest', 'status', 1, 'SUBREQUEST_INPROGRESS');
INSERT INTO ObjStatus (object, field, statusCode, statusName) VALUES ('SubRequest', 'status', 2, 'SUBREQUEST_SUCCESS');
INSERT INTO ObjStatus (object, field, statusCode, statusName) VALUES ('SubRequest', 'status', 3, 'SUBREQUEST_FAILED');
INSERT INTO ObjStatus (object, field, statusCode, statusName) VALUES ('SubRequest', 'status', 4, 'SUBREQUEST_ABORTED');
INSERT INTO ObjStatus (object, field, statusCode, statusName) VALUES ('SubRequest', 'status', 5, 'SUBREQUEST_RELEASED');
INSERT INTO ObjStatus (object, field, statusCode, statusName) VALUES ('SubRequest', 'status', 6, 'SUBREQUEST_PUTDONE');

/* SQL statements for type CopySubRequest */
CREATE TABLE CopySubRequest (localSurl VARCHAR2(2048), remoteSurl VARCHAR2(2048), localRequestToken VARCHAR2(2048), remoteRequestToken VARCHAR2(2048), reason VARCHAR2(2048), tgtSpaceToken VARCHAR2(2048), tgtExtraInfo VARCHAR2(2048), id INTEGER CONSTRAINT PK_CopySubRequest_Id PRIMARY KEY, request INTEGER, remoteStatus INTEGER, localStatus INTEGER, tgtStorageType INTEGER, tgtRetentionPolicy INTEGER, tgtAccessLatency INTEGER) INITRANS 50 PCTFREE 50 ENABLE ROW MOVEMENT;

INSERT INTO ObjStatus (object, field, statusCode, statusName) VALUES ('CopySubRequest', 'remoteStatus', 0, 'SUBREQUEST_PENDING');
INSERT INTO ObjStatus (object, field, statusCode, statusName) VALUES ('CopySubRequest', 'remoteStatus', 1, 'SUBREQUEST_INPROGRESS');
INSERT INTO ObjStatus (object, field, statusCode, statusName) VALUES ('CopySubRequest', 'remoteStatus', 2, 'SUBREQUEST_SUCCESS');
INSERT INTO ObjStatus (object, field, statusCode, statusName) VALUES ('CopySubRequest', 'remoteStatus', 3, 'SUBREQUEST_FAILED');
INSERT INTO ObjStatus (object, field, statusCode, statusName) VALUES ('CopySubRequest', 'remoteStatus', 4, 'SUBREQUEST_ABORTED');
INSERT INTO ObjStatus (object, field, statusCode, statusName) VALUES ('CopySubRequest', 'remoteStatus', 5, 'SUBREQUEST_RELEASED');
INSERT INTO ObjStatus (object, field, statusCode, statusName) VALUES ('CopySubRequest', 'remoteStatus', 6, 'SUBREQUEST_PUTDONE');

INSERT INTO ObjStatus (object, field, statusCode, statusName) VALUES ('CopySubRequest', 'localStatus', 0, 'SUBREQUEST_PENDING');
INSERT INTO ObjStatus (object, field, statusCode, statusName) VALUES ('CopySubRequest', 'localStatus', 1, 'SUBREQUEST_INPROGRESS');
INSERT INTO ObjStatus (object, field, statusCode, statusName) VALUES ('CopySubRequest', 'localStatus', 2, 'SUBREQUEST_SUCCESS');
INSERT INTO ObjStatus (object, field, statusCode, statusName) VALUES ('CopySubRequest', 'localStatus', 3, 'SUBREQUEST_FAILED');
INSERT INTO ObjStatus (object, field, statusCode, statusName) VALUES ('CopySubRequest', 'localStatus', 4, 'SUBREQUEST_ABORTED');
INSERT INTO ObjStatus (object, field, statusCode, statusName) VALUES ('CopySubRequest', 'localStatus', 5, 'SUBREQUEST_RELEASED');
INSERT INTO ObjStatus (object, field, statusCode, statusName) VALUES ('CopySubRequest', 'localStatus', 6, 'SUBREQUEST_PUTDONE');

INSERT INTO ObjStatus (object, field, statusCode, statusName) VALUES ('CopySubRequest', 'tgtStorageType', 0, 'STORAGETYPE_PERMANENT');
INSERT INTO ObjStatus (object, field, statusCode, statusName) VALUES ('CopySubRequest', 'tgtStorageType', 1, 'STORAGETYPE_DURABLE');
INSERT INTO ObjStatus (object, field, statusCode, statusName) VALUES ('CopySubRequest', 'tgtStorageType', 2, 'STORAGETYPE_VOLATILE');

INSERT INTO ObjStatus (object, field, statusCode, statusName) VALUES ('CopySubRequest', 'tgtRetentionPolicy', 0, 'RETENTIONPOLICY_CUSTODIAL');
INSERT INTO ObjStatus (object, field, statusCode, statusName) VALUES ('CopySubRequest', 'tgtRetentionPolicy', 1, 'RETENTIONPOLICY_REPLICA');
INSERT INTO ObjStatus (object, field, statusCode, statusName) VALUES ('CopySubRequest', 'tgtRetentionPolicy', 2, 'RETENTIONPOLICY_OUTPUT');

INSERT INTO ObjStatus (object, field, statusCode, statusName) VALUES ('CopySubRequest', 'tgtAccessLatency', 0, 'ACCESSLATENCY_ONLINE');
INSERT INTO ObjStatus (object, field, statusCode, statusName) VALUES ('CopySubRequest', 'tgtAccessLatency', 1, 'ACCESSLATENCY_NEARLINE');

/* Fill Type2Obj metatable */
INSERT INTO Type2Obj (type, object) VALUES (1000, 'INVALID');
INSERT INTO Type2Obj (type, object) VALUES (1002, 'StageRequest');
INSERT INTO Type2Obj (type, object) VALUES (1003, 'StorageArea');
INSERT INTO Type2Obj (type, object) VALUES (1004, 'SrmUser');
INSERT INTO Type2Obj (type, object) VALUES (1005, 'UserFile');
INSERT INTO Type2Obj (type, object) VALUES (1008, 'SubRequest');
INSERT INTO Type2Obj (type, object) VALUES (1009, 'CopySubRequest');
INSERT INTO Type2Obj (type, object) VALUES (1010, 'Request');
COMMIT;

