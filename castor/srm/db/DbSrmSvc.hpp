/******************************************************************************
 *                srm/db/DbSrmSvc.hpp
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * @(#)DbSrmSvc.hpp,v 1.35 $Release$ 2008/06/04 10:21:17 itglp
 *
 * Implementation of the ISrmSvc for CDBC
 *
 * @author Giuseppe Lo Presti
 *****************************************************************************/

#ifndef DB_DBSRMSVC_HPP
#define DB_DBSRMSVC_HPP 1

// Include Files
#include "castor/BaseSvc.hpp"
#include "castor/db/IDbStatement.hpp"
#include "castor/db/DbBaseObj.hpp"
#include "castor/srm/ISrmSvc.hpp"
#include <vector>


namespace castor {
 namespace srm {

  namespace db {

      /**
       * Implementation of the ISrmSvc for CDBC
       */
      class DbSrmSvc : public castor::BaseSvc,
                       public castor::db::DbBaseObj,
                       public virtual castor::srm::ISrmSvc {

      public:

        /**
         * default constructor
         */
        DbSrmSvc(const std::string name);

        /**
         * default destructor
         */
        virtual ~DbSrmSvc() throw();

        /**
         * Get the service id
         */
        virtual inline unsigned int id() const;

        /**
         * Get the service id
         */
        static unsigned int ID();

        /**
         * Reset the converter statements.
         */
        void reset() throw ();

      public:

        /**
          * Gets a user object based on distinguished name
          * and user name.  Entries in the SrmUser database
          * should be unique.
          * @param username  The user name obtained from get_client_username
          * @param dn  The distignuished name for the user obtained from
          * get_client_dn
          * @return The unique SrmUser object
          * @exception Exception is case of error
          */
        virtual castor::srm::SrmUser* getSrmUser (std::string username, std::string vo, std::string dn)
            throw (castor::exception::Exception);

        /**
         * Gets a UserFile object based on Castor file name
         * derived from SURL. The entry must exist from a previous
         * call to createUserFile.
         * @param castorFileName  Castor file name derived from SURL
         * @return The unique UserFile object
         * @exception Exception is case of error
         */
        virtual castor::srm::UserFile* getUserFile(std::string castorFileName)
            throw (castor::exception::Exception);
            
        /**
         * Gets a UserFile object based on the Castor file name
         * given in the SubRequest. Create an entry if it does not
         * exists, and fill the link accordingly.
         * @param subReq an SRM SubRequest
         * @param nsFileId the Castor NS fileid of this file
         * @param nsHost the Castor NS host
         * @param nsFileSize the Castor NS file size of this file
         * @return The unique UserFile object
         * @exception Exception is case of error
         */
        virtual castor::srm::UserFile* createUserFile(srm::SubRequest* subreq, u_signed64 nsFileId,
            std::string nsHost, u_signed64 nsFileSize = 0)
            throw (castor::exception::Exception);

        /**
          * Get a storage area based on the space token.
          * @param spaceToken  the space token associated with a
          *                    storage area
          * @return The StorageArea, or NULL is not found.
          * @excpetion Exception is thrown in case of error.
          */
        virtual castor::srm::StorageArea* getStorageArea(std::string spaceToken)
            throw (castor::exception::Exception);
    
        /**
          * Get a StageRequest object based on the request token
          * @param requestToken  the request token associated with a
          *                    StageRequest
          * @return The StageRequest object, or NULL is not found.
          * @excpetion Exception is thrown in case of error.
          */
        virtual castor::srm::StageRequest* getStageRequest(std::string requestToken)
            throw (castor::exception::Exception);

        /**
         * Get all the request tokens belonging to a user, optionally matching a
         * supplied request description.
         * @param userId  The user database id
         * @param description The request descriptions
         * @return A std::vector of request token strings
         * @exception Exception is thrown in case of error.
         */
        virtual std::vector<std::string> getRequestTokens(u_signed64 userId, std::string* description=NULL)
            throw (castor::exception::Exception);

        /**
         * Get all the space tokens belonging to a user, optionally matching a
         * supplied description.
         * @param userId  The user database id
         * @param description The space token description
         * @return A std::vector of space token strings
         * @exception Exception is thrown in case of error.
         */
        virtual std::vector<std::string> getSpaceTokens(std::string* voName=NULL, std::string* description=NULL)
            throw (castor::exception::Exception);

        /**
         * Lock a StageRequest by a SELECT FOR UPDATE
         * @param reqId the StageRequest id
         * @exception Exception is thrown in case of error.
         */
        virtual void lockStageRequest(u_signed64 reqId)
            throw (castor::exception::Exception);
        
        /**
         * Set StageRequests to ABORTED based on the file name.  This is a helper
         * function which is used by srmRm.  All PUT requests still pending a PUTDONE
         * are aborted (since the file is anyway removed)
         */
        virtual void abortPutRequestByFile(std::string& filename)
            throw (castor::exception::Exception);
        
        /**
         * Get a std::list of subrequests to release for srmReleaseFiles.  The subrequests are
         * selected based on the file name and the user.  Only subrequests which are completed
         * (status SUBREQUEST_SUCCESS and SUBREQUEST_RELEASED) are considered
         * and only those where the corresponding request type is a GET or BringOnline; PUTS
         * are not considered.
         * @param filename the Castor filename to look for
         * @param userid the SrmUser id of the user issuing the request
         * @exception Exception is thrown in case of error.
         */
        virtual std::list<castor::srm::SubRequest*> subRequestsToRelease( std::string& filename, u_signed64 userid)
            throw (castor::exception::Exception);

        /**
         * Get the list of space token descriptions for a given VO. Used by srmLs
         * @param vo The VO name
         * @return a map of service classes to space token descriptions
         * @exception Exception is thrown in case of error.
         */
        virtual std::map<std::string, std::string> getSpaceTokensList(std::string vo)
            throw (castor::exception::Exception);

        private:

        std::string toString(int value);

        /// SQL statement for function getSrmUser
        static const std::string s_getSrmUserStatementString;
        castor::db::IDbStatement *m_getSrmUserStatement;

        /// SQL statement for function getUserFile
        static const std::string s_getUserFileStatementString;
        castor::db::IDbStatement *m_getUserFileStatement;

        /// SQL statement for function createUserFile
        static const std::string s_createUserFileStatementString;
        castor::db::IDbStatement *m_createUserFileStatement;

        /// SQL statement for function getStorageArea
        static const std::string s_getStorageAreaStatementString;
        castor::db::IDbStatement *m_getStorageAreaStatement;

        /// SQL statement for function getRequestTokens
        static const std::string s_getRequestTokensStatementString1;
        static const std::string s_getRequestTokensStatementString2;
        castor::db::IDbStatement *m_getRequestTokensStatement1;
        castor::db::IDbStatement *m_getRequestTokensStatement2;

        /// SQL statement for function getSpaceTokens
        static const std::string s_getSpaceTokensStatementString;
        castor::db::IDbStatement *m_getSpaceTokensStatement;

        // SQL statement for lockStageRequest
        static const std::string s_lockStageRequest;
        castor::db::IDbStatement *m_lockStageRequest;
        
        // SQL statement for abortPutRequestByFile
        static const std::string s_abortPutRequestByFile;
        castor::db::IDbStatement *m_abortPutRequestByFile;

        // SQL staement for subRequestsToRelease
        static const std::string s_subRequestsToRelease;
        castor::db::IDbStatement *m_subRequestsToRelease;
        
        // SQL statement for getSpaceTokensList
        static const std::string s_getSpaceTokensList;
        castor::db::IDbStatement *m_getSpaceTokensList;
        
    }; // end of class DbSrmSvc

  } // end of namespace db

} } /* end of namespace castor/srm */

#endif // DB_DBSRMSVC_HPP
