/*******************************************************************
 *
 *
 * This file contains SQL code that is not generated automatically
 * and is inserted at the end of the generated code
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *******************************************************************/

/* Simple int array type */
CREATE OR REPLACE TYPE "NUMLIST" IS TABLE OF NUMBER;
/

/* PL/SQL declaration for the castor package, used by the bulk interfaces */
CREATE OR REPLACE PACKAGE castor AS
 TYPE "cnumList" IS TABLE OF NUMBER index by binary_integer;
END castor;
/

/* PL/SQL declaration for the srm package */
CREATE OR REPLACE PACKAGE srm AS
  TYPE SubReqUserFile IS RECORD (
    srId NUMBER,
    fileName VARCHAR2(2048),
    userFileId NUMBER,
    fileId NUMBER);
  TYPE SubReqUserFile_Cur IS REF CURSOR RETURN SubReqUserFile;
  TYPE IDRecord IS RECORD (id INTEGER);
  TYPE IDRecord_Cur IS REF CURSOR RETURN IDRecord;
END;
/

/* get current time as a time_t. Not that easy in ORACLE */
CREATE OR REPLACE FUNCTION getTime RETURN INTEGER IS
  epoch            TIMESTAMP WITH TIME ZONE;
  now              TIMESTAMP WITH TIME ZONE;
  interval         INTERVAL DAY(9) TO SECOND;
  interval_days    NUMBER;
  interval_hours   NUMBER;
  interval_minutes NUMBER;
  interval_seconds NUMBER;
BEGIN
  epoch := TO_TIMESTAMP_TZ('01-JAN-1970 00:00:00 00:00',
    'DD-MON-YYYY HH24:MI:SS TZH:TZM');
  now := SYSTIMESTAMP AT TIME ZONE '00:00';
  interval         := now - epoch;
  interval_days    := EXTRACT(DAY    FROM (interval));
  interval_hours   := EXTRACT(HOUR   FROM (interval));
  interval_minutes := EXTRACT(MINUTE FROM (interval));
  interval_seconds := EXTRACT(SECOND FROM (interval));

  RETURN interval_days * 24 * 60 * 60 + interval_hours * 60 * 60 +
    interval_minutes * 60 + interval_seconds;
END;
/

/* Generate a universally unique id (UUID) */
CREATE OR REPLACE FUNCTION uuidGen RETURN VARCHAR2 IS
  ret VARCHAR2(36);
BEGIN
  -- Note: the guid generator provided by ORACLE produces sequential uuid's, not
  -- random ones. The reason for this is because random uuid's are not good for
  -- indexing!
  RETURN lower(regexp_replace(sys_guid(), '(.{8})(.{4})(.{4})(.{4})(.{12})', '\1-\2-\3-\4-\5'));
END;
/

/* Function to normalize a filepath, i.e. to drop multiple '/'s and resolve any '..' */
/* Copied over from CASTOR2/castor/db/oracleCommon.sql */
CREATE OR REPLACE FUNCTION normalizePath(path IN VARCHAR2) RETURN VARCHAR2 IS
  buf VARCHAR2(2048);
  ret VARCHAR2(2048);
BEGIN
  -- drop '.'s and multiple '/'s
  ret := replace(regexp_replace(path, '[/]+', '/'), '/./', '/');
  LOOP
    buf := ret;
    -- a path component is by definition anything between two slashes, except
    -- the '..' string itself. This is not taken into account, resulting in incorrect
    -- parsing when relative paths are used (see bug #49002). We're not concerned by
    -- those cases; however this code could be fixed and improved by using string
    -- tokenization as opposed to expensive regexp parsing.
    ret := regexp_replace(buf, '/[^/]+/\.\./', '/');
    EXIT WHEN ret = buf;
  END LOOP;
  RETURN ret;
END;
/

/* This procedure resets the UserFile.castorFileName attribute for files
   that have been renamed. It runs inside an autonomous transaction.
   It should be called before creating/renaming any UserFile to insure
   the castorFileName stays unique */
CREATE OR REPLACE PROCEDURE dropReusedFile(fileName IN VARCHAR2) AS
  PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
  UPDATE /*+ INDEX(UserFile U_UserFile_CastorFileName) */ UserFile
     SET castorFileName = TO_CHAR(id)
   WHERE castorFileName = normalizePath(fileName);
  COMMIT;
END;
/


/* PL/SQL method to create a UserFile entry if missing */
CREATE OR REPLACE PROCEDURE createUserFile(fileName IN VARCHAR2,
                                           nsFileId IN INTEGER,
                                           nsHostName IN VARCHAR2,
                                           nsFileSize IN INTEGER,
                                           srId IN INTEGER,
                                           userFileId OUT INTEGER) AS
  CONSTRAINT_VIOLATED EXCEPTION;
  PRAGMA EXCEPTION_INIT(CONSTRAINT_VIOLATED, -1);
  previousFN VARCHAR2(2048);
BEGIN
  BEGIN
    -- Try to select an existing entry and lock it
    IF nsFileId > 0 THEN
      SELECT id, castorFileName INTO userFileId, previousFN FROM UserFile
       WHERE fileId = nsFileId FOR UPDATE;
      IF fileName != previousFN AND fileName != '' THEN
        dropReusedFile(fileName);
      END IF;
    ELSE
      SELECT id INTO userFileId FROM UserFile
       WHERE castorFileName = normalizePath(fileName) FOR UPDATE;
    END IF;
    -- we found one, update data; on prepareToPut, fileId is 0
    -- and we insert NULL waiting for the backend daemon to fill the value
    UPDATE UserFile
       SET fileId = decode(nsFileId, 0, NULL, nsFileId), fileSize = nsFileSize,
           nsHost = nsHostName, castorFileName = normalizePath(fileName)
     WHERE id = userFileId;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    -- we didn't find the file, create a new one and drop other old ones
    -- if any, that have the same filename and different fileid due to renaming
    dropReusedFile(fileName);
    INSERT INTO UserFile (id, fileId, nsHost, castorFileName, fileSize)
      VALUES (ids_seq.nextval, decode(nsFileId, 0, NULL, nsFileId),
        nsHostName, normalizePath(fileName), nsFileSize)
      RETURNING id INTO userFileId;
  END;
  UPDATE SubRequest
     SET userFile = userFileId,
         castorFileName = normalizePath(fileName)
   WHERE id = srId;
EXCEPTION WHEN CONSTRAINT_VIOLATED THEN
  -- retry the selection since a creation was done in between
  SELECT id INTO userFileId FROM UserFile
   WHERE castorFileName = normalizePath(fileName) FOR UPDATE;
  UPDATE UserFile
     SET fileId = decode(nsFileId, 0, NULL, nsFileId),
         fileSize = nsFileSize, nsHost = nsHostName
   WHERE id = userFileId;
  UPDATE SubRequest
     SET userFile = userFileId,
         castorFileName = normalizePath(fileName)
   WHERE id = srId;
END;
/

/* PL/SQL method implementing the common part between stageRequestToDo
   and requestToPoll, i.e. info retrieval and cursor handling */
CREATE OR REPLACE PROCEDURE gatherRequestFromCursor(SRcur IN srm.IDRecord_Cur,
                                                    expectedStatus IN NUMBER,
                                                    rid OUT NUMBER,
                                                    roverwriteOption OUT NUMBER,
                                                    rprotocol OUT VARCHAR2,
                                                    reuid OUT NUMBER,
                                                    regid OUT NUMBER,
                                                    rrhHost OUT VARCHAR2,
                                                    rrhPort OUT NUMBER,
                                                    rcreationTime OUT NUMBER,
                                                    rsvcClass OUT VARCHAR2,
                                                    rsrmRequestId OUT VARCHAR2,
                                                    rcastorRequestId OUT VARCHAR2,
                                                    rrequestType OUT INTEGER,
                                                    rvo OUT VARCHAR2,
                                                    rdn OUT VARCHAR2,
                                                    subreqs OUT srm.SubReqUserFile_Cur,
                                                    rlastcheck OUT NUMBER,
                                                    rnextcheck OUT NUMBER,
                                                    rexpirationInterval OUT NUMBER) AS
  LockException EXCEPTION;
  PRAGMA EXCEPTION_INIT (LockException, -54);
  rsrmUser NUMBER;
  intRId NUMBER;
BEGIN
  LOOP   -- loop over all possible candidates
    rId := 0;
    FETCH SRcur INTO intRId;
    EXIT WHEN SRcur%NOTFOUND;
    BEGIN
      -- try to take a lock on this one
      SELECT id, overwriteOption, protocol, euid, egid, rhHost, rhPort,
             creationTime, svcClass, srmRequestId, castorReqId, requestType, srmUser,
             lastcheck, nextcheck, expirationInterval
        INTO rid, roverwriteOption, rprotocol, reuid, regid, rrhHost, rrhPort,
             rcreationTime, rsvcClass, rsrmRequestId, rcastorRequestId, rrequestType, rsrmUser,
             rlastcheck, rnextcheck, rexpirationInterval
        FROM StageRequest
       WHERE id = intRId
         AND status = expectedStatus   -- PENDING or READYTOPOLL
         FOR UPDATE NOWAIT;
      -- we got a valid one, update statuses
      UPDATE StageRequest SET status = 1, handler = 0  -- INPROGRESS
       WHERE id = rid;
      UPDATE SubRequest SET status = 1  -- INPROGRESS
       WHERE request = rid AND status = 0;  -- QUEUED
      -- Get user and subrequests
      SELECT vo, dn INTO rvo, rdn FROM SrmUser WHERE id = rsrmUser;
      OPEN subreqs FOR 
        SELECT SubRequest.id, SubRequest.castorFileName, UserFile.id, fileId
          FROM SubRequest, UserFile
         WHERE request = rid AND UserFile.id = userFile;
      rid := intRId;
      EXIT;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        -- somebody else was faster than us, go to next candidate
        NULL;
      WHEN LockException THEN
        -- we didn't get the lock, go to next candidate
        NULL;
    END;
  END LOOP;
  CLOSE SRcur;
END;
/

/* PL/SQL method to get the next StageRequest to do */
CREATE OR REPLACE PROCEDURE stageRequestToDo(rid OUT NUMBER,
                                             roverwriteOption OUT NUMBER,
                                             rprotocol OUT VARCHAR2,
                                             reuid OUT NUMBER,
                                             regid OUT NUMBER,
                                             rrhHost OUT VARCHAR2,
                                             rrhPort OUT NUMBER,
                                             rcreationTime OUT NUMBER,
                                             rsvcClass OUT VARCHAR2,
                                             rsrmRequestId OUT VARCHAR2,
                                             rcastorRequestId OUT VARCHAR2,
                                             rrequestType OUT INTEGER,
                                             rvo OUT VARCHAR2,
                                             rdn OUT VARCHAR2,
                                             subreqs OUT srm.SubReqUserFile_Cur,
                                             rlastcheck OUT NUMBER,
                                             rnextcheck OUT NUMBER,
                                             rexpirationInterval OUT NUMBER) AS
  SRcur srm.IDRecord_Cur;
BEGIN
  OPEN SRcur FOR
    SELECT /*+ FIRST_ROWS(10) */ id FROM StageRequest
     WHERE handler = 1 -- handler = 1 for stage requests (Put, Get, and BoL)
    ORDER BY creationTime ASC;
  gatherRequestFromCursor(SRcur, 0, rid, roverwriteOption, rprotocol, reuid, regid,
                          rrhHost, rrhPort, rcreationTime, rsvcClass, rsrmRequestId,
                          rcastorRequestId, rrequestType, rvo, rdn, subreqs,
                          rlastcheck, rnextcheck, rexpirationInterval);
END;
/

/* PL/SQL method to get the next StageRequest to poll */
CREATE OR REPLACE PROCEDURE requestToPoll(rid OUT NUMBER,
                                          roverwriteOption OUT NUMBER,
                                          rprotocol OUT VARCHAR2,
                                          reuid OUT NUMBER,
                                          regid OUT NUMBER,
                                          rrhHost OUT VARCHAR2,
                                          rrhPort OUT NUMBER,
                                          rcreationTime OUT NUMBER,
                                          rsvcClass OUT VARCHAR2,
                                          rsrmRequestId OUT VARCHAR2,
                                          rcastorRequestId OUT VARCHAR2,
                                          rrequestType OUT INTEGER,
                                          rvo OUT VARCHAR2,
                                          rdn OUT VARCHAR2,
                                          subreqs OUT srm.SubReqUserFile_Cur,
                                          rlastcheck OUT NUMBER,
                                          rnextcheck OUT NUMBER,
                                          rexpirationInterval OUT NUMBER) AS
  SRcur srm.IDRecord_Cur;
BEGIN
  OPEN SRcur FOR
    SELECT /*+ INDEX(StageRequest I_StageRequest_Status) */ id FROM StageRequest
     WHERE status = 2  -- READYTOPOLL
       AND requestType IN (2, 5)  -- Get, BoL
       AND nextCheck < getTime();
  gatherRequestFromCursor(SRcur, 2, rid, roverwriteOption, rprotocol, reuid, regid,
                          rrhHost, rrhPort, rcreationTime, rsvcClass, rsrmRequestId,
                          rcastorRequestId, rrequestType, rvo, rdn, subreqs,
                          rlastcheck, rnextcheck, rexpirationInterval);
END;
/


CREATE OR REPLACE PROCEDURE failRequest(reqId IN NUMBER,
                                        ierrorCode IN NUMBER,
                                        errorMessage IN VARCHAR2) AS
BEGIN
  -- lock the stageRequest and mark the request DONE
  UPDATE StageRequest
     SET status = 4 -- DONE
   WHERE id = reqId;
  -- mark the subRequests FAILED for those not yet succeeded
  UPDATE SubRequest
     SET errorCode = ierrorCode, reason = errorMessage, status = 3 -- FAILED
   WHERE request = reqId
     AND status IN (0, 1);  -- QUEUED, INPROGRESS
END;
/

CREATE OR REPLACE PROCEDURE checkRequestDone(reqId NUMBER) AS
  unused NUMBER;
BEGIN
  -- lock the stageRequest
  SELECT id INTO unused FROM StageRequest
   WHERE id = reqId FOR UPDATE;
  -- try to find a non finished subrequest for this request
  SELECT id INTO unused FROM SubRequest
   WHERE request = reqId AND status IN (0, 1) AND ROWNUM < 2;
  -- if we found something, nothing to be done, the request is not over
EXCEPTION WHEN NO_DATA_FOUND THEN
  -- we did not find anything, so mark the request as DONE
  UPDATE StageRequest SET status = 4 -- DONE
   WHERE id = reqId;
END;
/

CREATE OR REPLACE PROCEDURE failSubRequest(subreqId IN NUMBER,
                                           ierrorCode IN NUMBER,
                                           errorMessage IN VARCHAR2) AS
  reqId NUMBER := 0;
BEGIN
  -- First take lock on the stageRequest
  SELECT request INTO reqId
    FROM SubRequest
   WHERE id = subReqId;
  SELECT id INTO reqId
    FROM StageRequest
   WHERE id = reqId FOR UPDATE;
  -- mark the subRequest FAILED if it was still INPROGRESS
  UPDATE SubRequest
     SET errorCode = ierrorCode, reason = errorMessage, status = 3 -- FAILED
   WHERE id = subreqId
     AND status = 1; -- INPROGRESS
  -- check whether the request should be marked DONE
  IF SQL%ROWCOUNT > 0 THEN
    checkRequestDone(reqId);
  END IF;
END;
/

CREATE OR REPLACE PROCEDURE succeedSubRequest(subreqId IN NUMBER) AS
  reqId NUMBER := 0;
BEGIN
  -- First take lock on the stageRequest
  SELECT request INTO reqId
    FROM SubRequest
   WHERE id = subReqId;
  SELECT id INTO reqId
    FROM StageRequest
   WHERE id = reqId FOR UPDATE;
  -- mark the subRequest SUCCESS if it was still INPROGRESS
  UPDATE SubRequest
     SET status = 2 -- SUCCESS
   WHERE id = subreqId
     AND status = 1; -- INPROGRESS
  -- check whether the request should be marked DONE
  IF SQL%ROWCOUNT > 0 THEN
    checkRequestDone(reqId);
  END IF;
END;
/

CREATE OR REPLACE PROCEDURE prepareCastorReq(sreqIds IN castor."cnumList",
                                             isvcClass IN VARCHAR2,
                                             rcastorReqId OUT VARCHAR2) AS
  reqId NUMBER := 0;
BEGIN
  -- First take lock on the stageRequest (we already have it
  -- when we are called after checkAbortedSubReqs, but we don't
  -- when we are called in the RecallPoller thread).
  SELECT request INTO reqId
    FROM SubRequest
   WHERE id = sreqIds(1);
  SELECT id INTO reqId
    FROM StageRequest
   WHERE id = reqId FOR UPDATE;
  -- generate a new UUID
  rcastorReqId := uuidGen();
  -- update SubRequests; note we don't change the status
  FORALL i IN sreqIds.FIRST..sreqIds.LAST 
    UPDATE SubRequest
       SET castorReqId = rcastorReqId,
           svcClass = isvcClass
     WHERE id = sreqIds(i);
END;
/

CREATE OR REPLACE PROCEDURE requestToBePolled(reqId IN NUMBER,
                                              icastorReqId IN VARCHAR2,
                                              interval IN INTEGER) AS
  now NUMBER := getTime();
BEGIN
  -- Update request if it is still INPROGRESS
  UPDATE StageRequest
     SET status = 2,  -- READYTOPOLL
         castorReqId = icastorReqId,
         lastCheck = now,
         nextCheck = now + interval - 1   
   -- -1 to quickly wake up the recallPoller when interval=0: not ideal,
   -- a refinement could be done here instead of relying on timing
   WHERE id = reqId AND status = 1; -- INPROGRESS
END;
/

CREATE OR REPLACE PROCEDURE fillFileId(userFileId IN NUMBER,
                                       ifileId IN NUMBER,
                                       insHost IN VARCHAR2) AS
BEGIN
  -- Update user file
  UPDATE UserFile SET fileId = ifileId, nsHost = insHost
   WHERE id = userFileId;
END;
/


CREATE OR REPLACE PROCEDURE checkAbortedSubReqs(sreqIds IN castor."cnumList",
                                                aborted OUT srm.IDRecord_Cur) AS
  reqId NUMBER := 0;
BEGIN
  -- First take lock on the stageRequest
  SELECT request INTO reqId
    FROM SubRequest
   WHERE id = sreqIds(1);
  SELECT id INTO reqId
    FROM StageRequest
   WHERE id = reqId FOR UPDATE;
  -- return all subrequests that have been aborted. Thanks to the lock,
  -- nothing else will happen now to them.
  FORALL i IN sreqIds.FIRST .. sreqIds.LAST
    INSERT INTO checkAbortedSubReqsTmpTable
     (SELECT id FROM SubRequest WHERE id = sreqIds(i) AND status != 1);  -- INPROGRESS
  OPEN aborted FOR SELECT * FROM checkAbortedSubReqsTmpTable;
END;
/


CREATE OR REPLACE PROCEDURE getSubRequestById(ireqId IN VARCHAR2,
                                              ifileId IN NUMBER,
                                              ifileName IN VARCHAR2,
                                              roverwriteOption OUT NUMBER,
                                              rprotocol OUT VARCHAR2,
                                              reuid OUT NUMBER,
                                              regid OUT NUMBER,
                                              rrhHost OUT VARCHAR2,
                                              rrhPort OUT NUMBER,
                                              rcreationTime OUT INTEGER,
                                              rsvcClass OUT VARCHAR2,
                                              rrequestType OUT INTEGER,
                                              rsrmReqId OUT VARCHAR2,
                                              rsubreqid OUT NUMBER,
                                              userFileId OUT NUMBER,
                                              fileName OUT VARCHAR2,
                                              rvo OUT VARCHAR2,
                                              rdn OUT VARCHAR2,
                                              rnshost OUT VARCHAR2,
                                              rreqid OUT INTEGER,
                                              rstatus OUT INTEGER) AS
BEGIN
  rsubreqId := 0;
  -- Select and take a lock on the stageRequest. This guarantees that TURLs
  -- in the same request are processed one by one, in particular when multiple
  -- TURLs are requested for a given file in a single request. The lock
  -- is released after the call to setTurl().
  -- Note that we already select on status, so we may fail the select; this is
  -- ok as the code in the exception handler doesn't need the lock.
  SELECT request INTO rreqId
    FROM SubRequest, UserFile
   WHERE SubRequest.castorReqId = ireqId
     AND UserFile.id = SubRequest.userFile
     AND UserFile.fileId = ifileId
     AND SubRequest.status = 1  -- INPROGRESS
     AND ROWNUM < 2;
  SELECT id INTO rreqId
    FROM StageRequest 
   WHERE id = rreqId FOR UPDATE;
  -- Something was found, reselect now after having a lock the data for this subRequest
  SELECT overwriteOption, protocol, euid, egid, rhHost, rhPort,
         creationTime, SubRequest.svcClass, requestType, srmRequestId,
         SubRequest.id, UserFile.id, SubRequest.castorFileName,
         vo, dn, nshost, SubRequest.status
    INTO roverwriteOption, rprotocol, reuid, regid, rrhHost, rrhPort,
         rcreationTime, rsvcClass, rrequestType, rsrmReqId, rsubreqid,
         userFileId, fileName, rvo, rdn, rnshost, rstatus
    FROM StageRequest, SubRequest, UserFile, SrmUser
   WHERE SubRequest.castorReqId = ireqId
     AND SrmUser.id = StageRequest.srmUser
     AND SubRequest.request = StageRequest.id
     AND UserFile.id = SubRequest.userFile
     AND UserFile.fileId = ifileId
     AND SubRequest.status = 1  -- INPROGRESS
     AND ROWNUM < 2;
EXCEPTION WHEN NO_DATA_FOUND THEN
  BEGIN
    -- We didn't find one, so select what is left including
    -- the current status to log the failure.
    -- In this case we don't need a lock, as no further processing is needed.
    SELECT overwriteOption, protocol, euid, egid, rhHost, rhPort,
           creationTime, SubRequest.svcClass, requestType, srmRequestId,
           SubRequest.id, UserFile.id, SubRequest.castorFileName,
           vo, dn, nshost, StageRequest.id, SubRequest.status
      INTO roverwriteOption, rprotocol, reuid, regid, rrhHost, rrhPort,
           rcreationTime, rsvcClass, rrequestType, rsrmReqId, rsubreqid,
           userFileId, fileName, rvo, rdn, rnshost, rreqid, rstatus
      FROM StageRequest, SubRequest, UserFile, SrmUser
     WHERE SubRequest.castorReqId = ireqId
       AND SrmUser.id = StageRequest.srmUser
       AND SubRequest.request = StageRequest.id
       AND UserFile.id = SubRequest.userFile
       AND UserFile.fileId = ifileId
       AND ROWNUM < 2;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    -- We didn't find anything. It might be that the file got
    -- overwritten meanwhile: let's check by fileName
    SELECT SubRequest.id
      INTO rsubreqid
      FROM StageRequest, SubRequest, UserFile
     WHERE SubRequest.castorReqId = ireqId
       AND SubRequest.request = StageRequest.id
       AND UserFile.id = SubRequest.userFile
       AND UserFile.castorFileName = ifileName;
    -- Yes, it worked: we then have to fail the subrequest as
    -- there's a mismatch between fileId and fileName
    failSubRequest(rsubreqid, 16, 'File overwritten while being processed');  -- EBUSY
    -- and we reset the return value so that we don't do anything else
    rsubreqid := 0;
    -- Otherwise, if nothing was found we really have a problem,
    -- so we forward the exception to the C++ layer
  END;
END;
/


CREATE OR REPLACE PROCEDURE setTurl(subreqId IN NUMBER,
                                    url IN VARCHAR2) AS
  reqId NUMBER;
BEGIN
  -- First take lock on the stageRequest
  SELECT request INTO reqId
    FROM SubRequest
   WHERE id = subreqId;
  SELECT id INTO reqId
    FROM StageRequest
   WHERE id = reqId FOR UPDATE;
  -- update subrequest
  UPDATE SubRequest
     SET turl = url, status = 2  -- SUCCESS
   WHERE id = subreqId AND status = 1;  -- INPROGRESS
  -- check whether the request should be marked DONE
  IF SQL%ROWCOUNT > 0 THEN
    checkRequestDone(reqId);
  END IF;
END;
/


/* A little generic method to delete efficiently */
CREATE OR REPLACE PROCEDURE bulkDelete(sel IN VARCHAR2, tab IN VARCHAR2) AS
BEGIN
  EXECUTE IMMEDIATE
  'DECLARE
    CURSOR s IS '||sel||'
    ids NUMLIST;
  BEGIN
    LOOP
      OPEN s;
      FETCH s BULK COLLECT INTO ids LIMIT 10000;
      EXIT WHEN ids.count = 0;
      FORALL i IN ids.FIRST..ids.LAST
        DELETE FROM '||tab||' WHERE id = ids(i);
      COMMIT;
      CLOSE s;
    END LOOP;
  END;';
END;
/


/* Procedure for SRM database garbage collection: 
 * delete all stage and sub requests which are expired and which have
 * no userfiles left, plus delete all files for which nothing is going on.
 */
CREATE OR REPLACE PROCEDURE garbageCollect(days IN INTEGER) AS
BEGIN
  -- Select all completed requests candidate for deletion, i.e. having
  -- expirationInterval older than the number of days passed as argument.
  -- We deliberately ignore the state as it may happen that requests
  -- don't reach the final state DONE, e.g. because of a missing call-back
  -- from the stager and the user never aborting/releasing the request.
  -- Note that this is a full table scan of StageRequest.
  EXECUTE IMMEDIATE 'TRUNCATE TABLE CleanupReqHelper';
  INSERT /*+ APPEND */ INTO CleanupReqHelper
    (SELECT id FROM StageRequest
      WHERE creationTime + expirationInterval < getTime() - days*86400);
  COMMIT;
  -- Delete all requests candidate for deletion. We do it in two steps
  -- to avoid ORA-01555 'snapshot too old' errors on the cursor
  -- that generated the CleanupReqHelper content.
  bulkDelete('SELECT R.id FROM StageRequest SR, CleanupReqHelper R
               WHERE SR.id = R.id;', 'StageRequest');
  -- Delete all subrequests now orphaned
  bulkDelete('SELECT SR.id FROM SubRequest SR WHERE NOT EXISTS
               (SELECT 1 FROM StageRequest WHERE id = SR.request);', 'SubRequest');
  -- Delete unused UserFile entries
  bulkDelete('SELECT id FROM UserFile WHERE NOT EXISTS
               (SELECT 1 FROM SubRequest WHERE userFile = UserFile.id);', 'UserFile');
END;
/

/* useful procedure to recompile all invalid items in the DB
   as many times as needed, until nothing can be improved anymore.
   Also reports the list of invalid items if any */
CREATE OR REPLACE PROCEDURE recompileAll AS
  varNbInvalids INTEGER;
  varNbInvalidsLastRun INTEGER := -1;
BEGIN
  WHILE varNbInvalidsLastRun != 0 LOOP
    varNbInvalids := 0;
    FOR a IN (SELECT object_name, object_type
                FROM user_objects
               WHERE object_type IN ('PROCEDURE', 'TRIGGER', 'FUNCTION', 'VIEW', 'PACKAGE BODY')
                 AND status = 'INVALID')
    LOOP
      IF a.object_type = 'PACKAGE BODY' THEN a.object_type := 'PACKAGE'; END IF;
      BEGIN
        EXECUTE IMMEDIATE 'ALTER ' ||a.object_type||' '||a.object_name||' COMPILE';
      EXCEPTION WHEN OTHERS THEN
        -- ignore, so that we continue compiling the other invalid items
        NULL;
      END;
    END LOOP;
    -- check how many invalids are still around
    SELECT count(*) INTO varNbInvalids FROM user_objects
     WHERE object_type IN ('PROCEDURE', 'TRIGGER', 'FUNCTION', 'VIEW', 'PACKAGE BODY') AND status = 'INVALID';
    -- should we give up ?
    IF varNbInvalids = varNbInvalidsLastRun THEN
      DECLARE
        varInvalidItems VARCHAR(2048);
      BEGIN
        -- yes, as we did not move forward on this run
        SELECT LISTAGG(object_name, ', ') WITHIN GROUP (ORDER BY object_name) INTO varInvalidItems
          FROM user_objects
         WHERE object_type IN ('PROCEDURE', 'TRIGGER', 'FUNCTION', 'VIEW', 'PACKAGE BODY') AND status = 'INVALID';
        raise_application_error(-20000, 'Revalidation of PL/SQL code failed. Still ' ||
                                        varNbInvalids || ' invalid items : ' || varInvalidItems);
      END;
    END IF;
    -- prepare for next loop
    varNbInvalidsLastRun := varNbInvalids;
    varNbInvalids := 0;
  END LOOP;
END;
/


/* Database jobs */
BEGIN
  -- Remove database jobs before recreating them
  FOR j IN (SELECT job_name FROM user_scheduler_jobs
             WHERE job_name IN ('GARBAGECOLLECTJOB'))
  LOOP
    DBMS_SCHEDULER.DROP_JOB(j.job_name, TRUE);
  END LOOP;
  
  -- Create a db job to be run once a day executing the garbageCollect procedure
  DBMS_SCHEDULER.CREATE_JOB(
      JOB_NAME        => 'GarbageCollectJob',
      JOB_TYPE        => 'PLSQL_BLOCK',
      JOB_ACTION      => 'BEGIN garbageCollect(7); END;',
      START_DATE      => SYSDATE,
      REPEAT_INTERVAL => 'FREQ=HOURLY; INTERVAL=24',
      ENABLED         => TRUE,
      COMMENTS        => 'Cleanup of terminated or expired entities');
END;
/
