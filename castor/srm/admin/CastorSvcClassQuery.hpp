/******************************************************************************
 *                CastorSvcClassQuery.hpp
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Query Castor database for available service classes
 *
 * @author Giuseppe Lo Presti
 *****************************************************************************/

#ifndef SRM_ADMIN_CASTORSVCCLASSQUERY_HPP
#define SRM_ADMIN_CASTORSVCCLASSQUERY_HPP 1

// Include Files
#include "castor/BaseAddress.hpp"
#include "castor/db/IDbStatement.hpp"
#include "castor/db/DbBaseObj.hpp"
#include "castor/stager/SvcClass.hpp"
#include <vector>


namespace castor {
 namespace srm {

  namespace admin {

      /**
       * Query Castor database for available service classes
       */
      class CastorSvcClassQuery : public castor::db::DbBaseObj {

      public:

        /**
         * Constructor with custom cnvSvc
         */
        CastorSvcClassQuery(castor::ICnvSvc* cnvSvc) : DbBaseObj(cnvSvc) {};

        // when the new DbBaseObj will be deployed...
        //CastorSvcClassQuery(std::string castorCnvSvcName) : DbBaseObj(castorCnvSvcName) {};

        /**
         * Default destructor
         */
        virtual ~CastorSvcClassQuery() throw() {};
         
        /**
         * reset method to destroy the prepared statement
         */
        virtual void reset() throw();

        /**
         * Returns a vector with all service classes available on
         * the castor instance referred by the cnvSvc used to initialize
         * this object.
         */
        std::vector<castor::stager::SvcClass*> getSvcClasses()
          throw (castor::exception::Exception);
      
      private:
      
        /// SQL statement for function getSvcClasses
        static const std::string s_getSvcClassesStatementString;

        /// SQL statement object for function getSvcClasses
        castor::db::IDbStatement *m_getSvcClassesStatement;

    }; // end of class CastorSvcClassQuery

  } // end of namespace admin

} } /* end of namespace castor/srm */

#endif // SRM_ADMIN_CASTORSVCCLASSQUERY_HPP
