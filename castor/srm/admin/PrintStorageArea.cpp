/******************************************************************************
 *                      PrintStorageArea.cpp
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * A tool to fill up the SRM database according to the service classes
 * and VOs defined in srm2_storagemap.conf
 *
 * @author Giuseppe Lo Presti
 *****************************************************************************/

#include <algorithm>
#include "castor/srm/SrmConstants.hpp"
#include "castor/srm/StorageArea.hpp"
#include "castor/srm/StorageStatus.hpp"
#include "castor/srm/StorageType.hpp"
#include "castor/srm/SrmUser.hpp"
#include "castor/srm/db/DbSrmSvc.hpp"

#include "serrno.h"
#include "castor/BaseAddress.hpp"
#include "castor/BaseObject.hpp"
#include "castor/Constants.hpp"
#include "castor/Services.hpp"
#include "castor/db/DbParamsSvc.hpp"
#include "castor/exception/Exception.hpp"

#include <iostream>
#include <fstream>
#include <string.h>


void briefUsage() {
    std::cout << "Usage: " << std::endl;
    std::cout << "\t printstoragearea [-v voName] [-D description]" << std::endl;
    std::cout << std::endl;
}


void usage() {
    std::cout << "Usage: " << std::endl;
    std::cout << "\t printstoragearea [-v voName] [-D description]" << std::endl;
    std::cout << std::endl;
    std::cout << "This tool allows you to list StorageAreas owned by a specific VO.  This list can be further " << std::endl;
    std::cout << "restricted by using the -D option, which will only return results with this description." << std::endl << std::endl;
    std::cout << "The format of the results is <spaceToken> <tokenDescription> <svcClass> <type>" << std::endl;
    std::cout << "where type will have the value 0 (PERMANENT), 1 (DURABLE) or 2 (VOLATILE)." << std::endl;
}


int main (int argc, char** argv) {

    // parse the arguments if any
    std::string *in_description = 0;
    std::string *in_voName = 0;
    int cArg = 1;
    while (cArg < argc) {
        if ( strcmp( argv[cArg], "-D" ) == 0 ) {
            if ( cArg+1 >= argc ) {
                std::cerr << "No description supplied." << std::endl;
                usage();
                exit(1);
            }
            in_description = new std::string (argv[++cArg]);
        }
        else if ( strcmp(argv[cArg], "-v" ) == 0 ) {
            if ( cArg+1 >= argc ) {
                std::cerr << "No VO information supplied." << std::endl;
                usage();
                exit(1);
            }
            in_voName = new std::string (argv[++cArg]);
            // Convert VO to lower case
            transform(in_voName->begin(), in_voName->end(), in_voName->begin(), ( int(*)(int))tolower);
        }
        else if ( strcmp(argv[cArg], "-h") == 0 || strcmp(argv[cArg], "-?") == 0 || strcmp(argv[cArg], "--help") == 0 ) {
            usage();
            exit(0);
        }
        else {
            std::cerr << "Invalid argument" << std::endl;
            briefUsage();
            exit(1);
        }
        cArg++;
    }

    castor::BaseAddress srmAd;
    try {
        // Create a db parameters service and fill with appropriate defaults
        castor::IService* s = castor::BaseObject::sharedServices()->service("DbParamsSvc", castor::SVC_DBPARAMSSVC);
        castor::db::DbParamsSvc* params = dynamic_cast<castor::db::DbParamsSvc*>(s);
        if(params == 0) {
            castor::exception::Exception e;
            e.getMessage() << "Could not instantiate the parameters service";
            throw e;
        }
        params->setSchemaVersion(SRMSCHEMAVERSION);
        params->setDbAccessConfFile(ORASRMCONFIGFILE);

        srmAd.setCnvSvcName("DbCnvSvc");
        srmAd.setCnvSvcType(castor::SVC_DBCNV);
        castor::IService* s1 = castor::BaseObject::sharedServices()->service("DbSrmSvc", castor::SVC_DBSRMSVC);
        castor::srm::db::DbSrmSvc* srmSvc = dynamic_cast<castor::srm::db::DbSrmSvc*>(s1);
        if(srmSvc == 0) {
            std::cout << "Fatal, cannot instantiate DbSrmSvc. Check castor.conf." << std::endl;
            return 1;
        }

        // Try and get the storage area used as input:
        castor::srm::StorageArea *sa;
        std::vector<std::string> tokens = srmSvc->getSpaceTokens(in_voName, in_description);
        if ( tokens.size() == 0 ) {
            std::cout << "No Storage areas found owned by specified VO and matching specified description" << std::endl;
            exit(0);
        }

        // Print out some header information
        std::cout << "<SpaceToken> \t\t <Description> \t <svcClass> \t <type> \t <status>" << std::endl;

        for (unsigned i=0; i<tokens.size(); i++) {
            sa = srmSvc->getStorageArea(tokens[i]);
            std::cout << sa->spaceToken() <<
                "\t\t\"" << sa->tokenDescription() << "\"" << 
                "\t\"" << sa->svcClass() << "\"";
            switch (sa->storageType()) {
                case castor::srm::STORAGETYPE_PERMANENT:
                    std::cout << "\t\"PERMANENT\"";
                    break;
                case castor::srm::STORAGETYPE_DURABLE:
                    std::cout << "\t\"DURABLE\"";
                    break;
                case castor::srm::STORAGETYPE_VOLATILE:
                    std::cout << "\t\"VOLATILE\"";
                    break;
            }
            switch (sa->storageStatus()) {
                case castor::srm::STORAGESTATUS_REQUESTED:
                    std::cout << "\t\"REQUESTED\"" << std::endl;
                    break;
                case castor::srm::STORAGESTATUS_ALLOCATED:
                case castor::srm::STORAGESTATUS_INUSE:
                    std::cout << "\t\"ALLOCATED\"" << std::endl;
                    break;
                case castor::srm::STORAGESTATUS_DEALLOCATED:
                default:
                    std::cout << "\t\"DEALLOCATED\"" << std::endl;
                    break;
                case castor::srm::STORAGESTATUS_EXPIRED:
                    std::cout << "\t\"EXPIRED\"" << std::endl;
            }
            delete sa;
        }
    }
    catch (castor::exception::Exception e) {
        std::cout << "Error caught while querying in db: "
            << sstrerror(e.code()) << std::endl
            << e.getMessage().str() << std::endl;
        return 1;
    }
    return 0;
}
