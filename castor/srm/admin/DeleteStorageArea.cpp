/******************************************************************************
 *                      DeleteStorageArea.cpp
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * A tool to fill up the SRM database according to the service classes
 * and VOs defined in srm2_storagemap.conf
 *
 * @author Giuseppe Lo Presti
 *****************************************************************************/

#include <algorithm>
#include "castor/srm/StorageArea.hpp"
#include "castor/srm/StorageStatus.hpp"
#include "castor/srm/StorageType.hpp"
#include "castor/srm/SrmUser.hpp"
#include "castor/srm/Constants.hpp"
#include "castor/srm/SrmConstants.hpp"
#include "castor/srm/db/DbSrmSvc.hpp"

#include <serrno.h>
#include "castor/Constants.hpp"
#include "castor/Services.hpp"
#include "castor/BaseObject.hpp"
#include "castor/BaseAddress.hpp"
#include "castor/IService.hpp"
#include "castor/IObject.hpp"
#include "castor/db/DbParamsSvc.hpp"
#include "castor/exception/Exception.hpp"

#include <Cuuid.h>
#include <getconfent.h>
#include <iostream>
#include <fstream>
#include <string.h>

void briefUsage() {
    std::cout << "Usage: " << std::endl;
    std::cout << "\t deletestoragearea -X spaceToken [-f]" << std::endl;
    std::cout << std::endl;
}


void usage() {
    std::cout << "Usage: " << std::endl;
    std::cout << "\t deletestoragearea -X spaceToken [-f]" << std::endl;
    std::cout << std::endl;
    std::cout << "This tool will permanently and irrevocably remove an existing StorageArea from the SRM database." << std::endl;
    std::cout << "The internal space token itself (-X) is used to specify the area, and it must already be in" << std::endl ;
    std::cout << "DEALLOCATED status (see modifystoragearea for how to set this). " << std::endl;
    std::cout << "User will be prompted to confirm removal unless the -f option is supplied, in" << std::endl;
    std::cout << "which case the entry in the database will be removed without additional input." << std::endl;
}


int main (int argc, char** argv) {

    // parse the arguments if any
    std::string in_token;
    bool confirm = true;
    int cArg = 1;
    while (cArg < argc) {
        if ( strcmp(argv[cArg], "-X" ) == 0 ) {
            if ( in_token.length() > 0 ) {
                briefUsage();
                exit(1);
            }
            in_token.assign(argv[++cArg]);
        }
        else if ( strcmp(argv[cArg], "-f" ) == 0 ) {
            confirm = false;
        }
        else if (  strcmp(argv[cArg], "-h") == 0 || strcmp(argv[cArg], "-?") == 0 || strcmp(argv[cArg], "--help") == 0 ) {
            usage();
            exit(0);
        }
        else {
            std::cerr << "Invalid argument" << std::endl;
            briefUsage();
            exit(1);
        }
        cArg++;
    }
    if ( in_token.length() == 0 ) {
        std::cout << "Missing argument" << std::endl;
        briefUsage();
        exit(1);
    }

    castor::Services* svcs = 0;
    castor::BaseAddress srmAd;
    try {
        // Create a db parameters service and fill with appropriate defaults
        castor::IService* s = castor::BaseObject::sharedServices()->service("DbParamsSvc", castor::SVC_DBPARAMSSVC);
        castor::db::DbParamsSvc* params = dynamic_cast<castor::db::DbParamsSvc*>(s);
        if(params == 0) {
            castor::exception::Exception e;
            e.getMessage() << "Could not instantiate the parameters service";
            throw e;
        }
        params->setSchemaVersion(SRMSCHEMAVERSION);
        params->setDbAccessConfFile(ORASRMCONFIGFILE);

        // gets db services
        svcs = castor::BaseObject::services();
        srmAd.setCnvSvcName("DbCnvSvc");
        srmAd.setCnvSvcType(castor::SVC_DBCNV);
        castor::IService* s1 = castor::BaseObject::sharedServices()->service("DbSrmSvc", castor::SVC_DBSRMSVC);
        castor::srm::db::DbSrmSvc* srmSvc = dynamic_cast<castor::srm::db::DbSrmSvc*>(s1);
        if(srmSvc == 0) {
            std::cout << "Fatal, cannot instantiate DbSrmSvc. Check castor.conf." << std::endl;
            return 1;
        }


        // Try and get the storage area used as input:
        castor::srm::StorageArea *sa = srmSvc->getStorageArea(in_token);
        if (sa) {
            svcs->fillObj(&srmAd, sa, castor::srm::OBJ_SrmUser, true);
            if ( confirm || sa->storageStatus() != castor::srm::STORAGESTATUS_DEALLOCATED ) {
                std::cout << "Are you sure you wish to remove space with token " << 
                    in_token << " owned by VO " << sa->srmUser()->vo() << 
                    " and description " << sa->tokenDescription() << "?" << std::endl;
                std::string response;
                std::cin >> response;
                if ( response != "y" && response != "yes" ) {
                    std::cout << "Operation terminated." << std::endl;
                    exit (0);
                }
            }
            // Now in principle we can try and delete the entry from the database
            svcs->deleteRep(&srmAd, sa, true);
        }
        else {
            std::cerr << "No Storage Area with this space token" << std::endl;
            exit(0);
        }
    }
    catch (castor::exception::Exception e) {
        svcs->rollback(&srmAd);
        std::cout << "Error caught while updating in db: "
            << sstrerror(e.code()) << std::endl
            << e.getMessage().str() << std::endl;
        return 1;
    }
    svcs->commit(&srmAd);
    return 0;
}
