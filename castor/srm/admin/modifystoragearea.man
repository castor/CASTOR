.\" @(#)$Id: modifystoragearea.man,v 1.2 2008/12/04 23:20:16 sdewitt Exp $
.\" Copyright (C) 2008 by CERN/IT
.\" All rights reserved
.\"
.TH MODIFYSTORAGEAREA 1castor "$Date: 2008/12/04 23:20:16 $" CASTOR "SRM Admin Commands"
.SH NAME
modifystoragearea \- Modify an existing SRM 'space' in the SRM database
.SH SYNOPSIS
.B deletestoragearea
.BI "-V VO Name"
[
.BI "-X space token"
.BI |
.BI "-D token description"
]
[
.BI "-s newServiceClass"
]
[
.BI "-t newStorageType"
]
[
.BI "-d newDescription"
]
[
.BI "-x newStatus"
]
[
.BI -h
]

.SH DESCRIPTION
The \fBmodifystoragearea\fP command lets a user modify the parameters of an existing space in the SRM database.  This space must be 
identified by either a space token or a description.  If using the description, it must refer to a unique description for a VO.
.P
Valid values for the \fInewStorageType\fP are:
.br
[0 | PERMANENT]
.br
[1 | DURABLE]
.br
[2 | VOLATILE]
.P
For \fInewStatus\fP the valid values are:
.br
[0 | REQUESTED]
.br
[1 | ALLOCATED]
.br
[3 | DEALLOCATED]
.br
(Note that the value 2 (INUSE) is still acceptable, but its use is deprecated).

.SH OPTIONS
.IP "-V VO Name"
The name of the VO owning the space to be modified.
.IP "-X SpaceToken"
Specify the unique space token of the space to be modified.  This is an internal unique identifier obtainable by using \fIprintstoragearea\fP.
.IP "-D Description"
The textual description assigned to the space.
.IP "-s newServiceClass"
Modifies the existing service class to the new parameter specified by this option.
.IP "-t newStorageType"
Modifies the existing storage type to the new parameter specified by this option.
.IP "-t newDescription"
Modifies the existing space description to the new parameter specified by this option.
.IP "-x newStatus"
Modifies the existing status to the new parameter specified by this option.
.IP -h
Print a brief help message.

.SH FILES
None

.SH EXAMPLES
.nf
$ printstoragearea -V dteam
<Space Token>                            <Description>   <service class>         <type>          <status>
4755c348-0000-1000-b34d-956ba3ff0000    ""               ""                      "PERMANENT"     "REQUESTED"
$ modifySvcClass -V dteam -X 4755c348-0000-1000-b34d-956ba3ff0000 -d foo -s bar -t DURABLE -x ALLOCATED
$ printstoragearea -V dteam
<Space Token>                           <Description>   <service class>         <type>          <status>
4755c348-0000-1000-b34d-956ba3ff0000    "foo"           "bar"                   "DURABLE"       "ALLOCATED"
.fi
.P
.nf
$ printstoragearea -V dteam -D foo
<Space Token>                           <Description>   <service class>         <type>          <status>
4755c348-0000-1000-b34d-956ba3ff0000    "foo"           "bar"                   "DURABLE"       "ALLOCATED"
47d8479f-0000-1000-9f98-f99125ef0000    "foo"           "bar"                   "DURABLE"       "ALLOCATED"
$ modifySvcClass -V dteam -D foo -t PERMANENT
More than 1 token with required description owned by this VO
Please use printstoragearea to determine the unique token of the one you want to modify
$ modifySvcClass -V dteam -X 4755c348-0000-1000-b34d-956ba3ff0000 -t PERMANENT
$ printstoragearea -V dteam -D foo
<Space Token>                           <Description>   <service class>         <type>          <status>
4755c348-0000-1000-b34d-956ba3ff0000    "foo"           "bar"                   "PERMANENT"     "ALLOCATED"
47d8479f-0000-1000-9f98-f99125ef0000    "foo"           "bar"                   "DURABLE"       "ALLOCATED"
.fi

.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.

.SH FILES
.I /etc/castor/castor.conf

.SH NOTES
It should be noted that no consistency checking is done when using this function, neither using SRM configuration files nor
the CASTOR back end.
.P
Also, before a user can remove a space using \fIdeletestoragearea\fP, the status of the space \fBmust\fP be set to DEALLOCATED using this tool.

.SH SEE ALSO
.BR deletestoragearea(1castor)
.BR enterStorageArea(1castor)
.BR printstoragearea(1castor)

.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
