.\" @(#)$Id: enterStorageArea.man,v 1.3 2008/12/04 23:20:16 sdewitt Exp $
.\" Copyright (C) 2008 by CERN/IT
.\" All rights reserved
.\"
.TH ENTERSTORAGEAREA 1castor "$Date: 2008/12/04 23:20:16 $" CASTOR "SRM Admin Commands"
.SH NAME
enterStorageArea \- Enter a new storage area ('space') into the SRM database.
.SH SYNOPSIS
.B enterStorageArea
.BI -v
.BI VOName
.BI -s
.BI svcClass
.BI -d
.BI tokenDescription
.BI -t
.BI StorageType
[
.BI -f
]
[
.BI -h
]

.SH DESCRIPTION
The \fBenterStorageArea\fP command can be used to enter new space tokens into the SRM database instance.  By default, the
service class  will be checked again any existing entry in the \fIsrm2_storagemap.conf\fP file to ensure any data entered
via this command is in agreement with the contents of this file, specifically with respect to the \fIstorageType\fP parameter.
This validation assumes that a \fIstorageType\fP of PERMANENT corresponds to TAPE1DISKx in the configuration file, DURABLE
corresponds to TAPE0DISK1 and VOLATILE to TAPE0DISK0.
This validation step can be omitted by using the \fI-f\fP (force) option.
.P
Spaces created with this tool have 'infinite' lifetime and are set to a status of ALLOCATED by default.  This latter
can be changed using \fImodifystoragearea\fP.


.SH OPTIONS

.IP "-v VOName"
Specify the VO that will own the space.  This is not case sensitive.
.IP "-s svcClass"
Enter the name of the corresponding CASTOR service class which this space will map to.
.IP "-d tokenDescription"
Specify a description to assign to this space. This decription need not be unique, since the space created by this
call will be assigned a unique identifier.
.IP "-t storageType"
The storage type for the space.  Permitted values are:
.br 
"0 (PERMANENT)"
.br 
"1 (DURABLE)"
.br 
"2 (VOLATILE)"
.IP -f
Force the creation of the space regardless of any conflicts with configuration files.

.SH FILES
.I /etc/castor/srm2_storagemap.conf
.RS
The configuration file allowing mapping of request parameters to service classes without the need to define SRM spaces directly.
.RE


.SH EXAMPLES
.fi
$ enterStorageArea -v dteam -s bar -d foo -t 1

$ enterStorageArea -v DTEAM -s bar -d foo -t 0
.fi
Mismatch between storage type and contents of storagemap file

.SH FILES
.I /etc/castor/castor.conf

.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.

.SH SEE ALSO
.BR modifystoragearea(1castor)
.BR printstoragearea(1castor)
.BR deletestoragearea(1castor)

.SH NOTES
There is currently no validation performed against the backend CASTOR instance to ensure the space token/service class have the 
same properties.

.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
