.\" @(#)$Id: deletestoragearea.man,v 1.2 2008/12/04 23:20:16 sdewitt Exp $
.\" Copyright (C) 2008 by CERN/IT
.\" All rights reserved
.\"
.TH RMSTORAGEAREA 1castor "$Date: 2008/12/04 23:20:16 $" CASTOR "SRM Admin Commands"
.SH NAME
deletestoragearea \- Delete a SRM 'space' from the SRM database
.SH SYNOPSIS
.B deletestoragearea
.BI -X
.BI spaceToken
[
.BI -f
]
[
.BI -h
]

.SH DESCRIPTION
The \fBdeletestoragearea\fP command is used to permanently and irrevocably remove a space from the SRM database.  This space must first be 
deallocated using \fImodifystoragearea\fP.  Without the \fI-f\fP option the user is prompted to confirm the deletion; the forcing
\fI-f\fP option will just remove the entry without any prompt.
.P
The removal
is done on a per space token basis.  The space token is the unique identifier identifying a space in the SRM.  This can be obtained
from the command \fIprintstoragearea\fP.

.SH OPTIONS

.IP "-X SpaceToken"
Specify the space token to delete.  This is an internal unique identifier obtainable by using \fIprintstoragearea\fP.
.IP -f
Turns off prompting for confirmation.
.IP -h
Print a brief help message.

.SH FILES
None

.SH EXAMPLES
.nf
$ deletestoragearea -X 47d7f581-0000-1000-9cff-fe87695f0000
Are you sure you wish to remove space with token 47d7f581-0000-1000-9cff-fe87695f0000 owned by VO dteam and description foo?
y
.fi


.nf
$ deletestoragearea -X 47d7f79f-0000-1000-9630-acc54fc70000 -f
.fi

.nf
$ deletestoragearea -X foo
No Storage Area with this space token
.fi

.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.

.SH FILES
.I /etc/castor/castor.conf

.SH SEE ALSO
.BR modifystoragearea(1castor)
.BR enterStorageArea(1castor)
.BR printstoragearea(1castor)

.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
