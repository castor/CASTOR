/******************************************************************************
 *                      ModifyStorageArea.cpp
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * A tool to fill up the SRM database according to the service classes
 * and VOs defined in srm2_storagemap.conf
 *
 * @author Giuseppe Lo Presti
 *****************************************************************************/

#include <algorithm>
#include "castor/srm/SrmConstants.hpp"
#include "castor/srm/StorageArea.hpp"
#include "castor/srm/StorageStatus.hpp"
#include "castor/srm/StorageType.hpp"
#include "castor/srm/SrmUser.hpp"
#include "castor/srm/Constants.hpp"
#include "castor/srm/SrmConstants.hpp"
#include "castor/srm/db/DbSrmSvc.hpp"

#include <serrno.h>
#include "castor/Constants.hpp"
#include "castor/Services.hpp"
#include "castor/BaseObject.hpp"
#include "castor/BaseAddress.hpp"
#include "castor/IService.hpp"
#include "castor/IObject.hpp"
#include "castor/log/log.hpp"
#include "castor/db/DbParamsSvc.hpp"
#include "castor/exception/Exception.hpp"

#include <Cuuid.h>
#include <getconfent.h>
#include <iostream>
#include <fstream>
#include <string.h>

/**
  * This routine will validate manually entered data against the contents of the storage map file.
  * In the event of an inconsistentcy, no update is performed.  Currently, the database is not
  * checked so any existing entry will be ovwerwritten
  */
bool validate (std::string vo, std::string svcClass, int storageType, std::string mapFile) {
    bool isValid = true;

    std::ifstream mapFileStream (mapFile.c_str());
    if(!mapFileStream.is_open()) {
        std::cerr << "Unable to open mapfile " << mapFile << " to perform validation." << std::endl;
        return !isValid;
    }

    // We need to make sure the VO is in upper case 
    std::string voUpper = vo;
    transform (voUpper.begin(), voUpper.end(), voUpper.begin(), (int(*)(int))toupper);

    std::string line;
    static const std::string delimiters = " \t";
    while ( !mapFileStream.eof() ) {
        getline(mapFileStream, line);
        // ignore STAGEMAP entries, blank lines or comments...
        if ( line.length() == 0 || line[0] == '#' || line.find("USTGMAP") != std::string::npos || mapFileStream.eof() ) continue;

        // We need to split the line.  This should be into 3 substrings.  Wish we had java here :(
        // Skip delimiters at beginning.
        std::string::size_type lastPos = line.find_first_not_of(delimiters, 0);
        // Find first "non-delimiter".
        std::string::size_type pos     = line.find_first_of(delimiters, lastPos);

        std::vector<std::string> tokens;
        while (std::string::npos != pos || std::string::npos != lastPos) {
            // Found a token, add it to the std::vector.
            tokens.push_back(line.substr(lastPos, pos - lastPos));
            // Skip delimiters.  Note the "not_of"
            lastPos = line.find_first_not_of(delimiters, pos);
            // Find next "non-delimiter"
            pos = line.find_first_of(delimiters, lastPos);
        }
        if ( tokens.size() != 3 ) continue;

        // See if we have a match for input svcclass and vo
        if ( tokens[0] == vo && tokens[2] == svcClass ) {
            // We have a match, so check the type
            switch (storageType) {
                case castor::srm::STORAGETYPE_PERMANENT:
                    if ( tokens[1].find("TAPE1") == std::string::npos ) isValid = false;
                    break;
                case castor::srm::STORAGETYPE_DURABLE:
                    if ( tokens[1] != "TAPE0DISK1" ) isValid = false;
                    break;
                case castor::srm::STORAGETYPE_VOLATILE:
                    if ( tokens[1] != "TAPE0DISK0" ) isValid = false;
                    break;
                default:
                    // Unless the second token is default we assume this is an error
                    if ( tokens[1] != "default" ) {
                        isValid = false;
                        std::cerr << "Input conflicts with storage map file for svc class " << svcClass << ", VO " << vo << std::endl;
                    }
                    break;
            }
            break;
        }
    }
    return isValid;
}

void briefUsage() {
    std::cout << "Usage: " << std::endl;
    std::cout << "\t modifystoragearea -v voName [-D description | -X token] [-s newSvcClass] [-t newType] [-x newStatus]" << std::endl;
    std::cout << std::endl;
}


void usage() {
    std::cout << "Usage: " << std::endl;
    std::cout << "\t modifystoragearea -v voName [-D description | -X token] [-s newSvcClass] [-t newType] [-x newStatus]" << std::endl;
    std::cout << std::endl;
    std::cout << "This tool allows you to modify an existing StorageArea in the SRM database based on either the " << std::endl;
    std::cout << "existing space token description (-D), or the space token itself (-X)." << std::endl << std::endl;
    std::cout << "In the case that there are multiple entries with the same description" << std::endl;
    std::cout << "it will be necessary to use the -X argument.  The internal token can be obtained via the tool " << std::endl;
    std::cout << "printstoragearea." << std::endl << std::endl ;
    std::cout << "Valid values for newType are: " << std::endl;
    std::cout << "\t[0 | PERMANENT]" << std::endl << "\t[1 | DURABLE]" << std::endl << "\t[2 | VOLATILE]" << std::endl;
    std::cout << "Valid values for newStatus are: " << std::endl;
    std::cout << "\t[0 | REQUESTED]" << std::endl << "\t[1 | ALLOCATED]" << std::endl << "\t[2 | INUSE]" << std::endl << "\t[3 | DEALLOCATED]" << std::endl;
    std::cout << "Note that it is not possible to change StorageArea ownership via this tool.  In this case it " << std::endl;
    std::cout << "will be necessary to first remove the existing area using removeStorageArea and recreate it" << std::endl;
    std::cout << "using enterStorageArea" << std::endl;
}


int main (int argc, char** argv) {

    // gets mapping file (srm2_storagemap.conf by default) and make sure it is readable
    // if we are not forcing entries
    std::string mapFile = castor::srm::SrmConstants::getInstance()->storageMappingFile();

    // parse the arguments if any
    std::string in_description;
    std::string in_token;
    std::string in_voName;
    std::string new_svcClass;
    std::string new_storageType;
    std::string new_Status;
    //int new_storageType = -1;
    int cArg = 1;
    while (cArg < argc) {
        if ( strcmp( argv[cArg], "-D" ) == 0 ) {
            if ( cArg+1 >= argc ) {
                std::cerr << "No description information supplied." << std::endl;
                briefUsage();
                exit(1);
            }
            in_description.assign(argv[++cArg]);
        }
        else if ( strcmp(argv[cArg], "-X" ) == 0 ) {
            if ( cArg+1 >= argc ) {
                std::cerr << "No space token information supplied." << std::endl;
                briefUsage();
                exit(1);
            }
            in_token.assign(argv[++cArg]);
        }
        else if ( strcmp(argv[cArg], "-v" ) == 0 ) {
            if ( cArg+1 >= argc ) {
                std::cerr << "No VO information supplied." << std::endl;
                briefUsage();
                exit(1);
            }
            in_voName.assign(argv[++cArg]);
        }
        else if ( strcmp(argv[cArg], "-t" ) == 0 ) {
            if ( cArg+1 >= argc ) {
                std::cerr << "No type information supplied." << std::endl;
                briefUsage();
                exit(1);
            }
            new_storageType.assign( argv[++cArg] );
        }
        else if ( strcmp(argv[cArg], "-s" ) == 0 ) {
            if ( cArg+1 >= argc ) {
                std::cerr << "No service class information supplied." << std::endl;
                briefUsage();
                exit(1);
            }
            new_svcClass.assign(argv[++cArg]);
        }
        else if ( strcmp(argv[cArg], "-x" ) == 0 ) {
            if ( cArg+1 >= argc ) {
                std::cerr << "No status information supplied." << std::endl;
                briefUsage();
                exit(1);
            }
            new_Status.assign(argv[++cArg]);
        }
        else if ( strcmp(argv[cArg], "-h") == 0 || strcmp(argv[cArg], "-?") == 0 || strcmp(argv[cArg], "--help") == 0 ) {
            usage();
            exit(0);
        }
        else {
            std::cerr << "Invalid argument" << argv[cArg] << std::endl;
            briefUsage();
            exit(1);
        }
        cArg++;
    }
    if ( in_voName.length() == 0 || (in_description.length() == 0 && in_token.length() == 0) ) {
        std::cout << "Missing argument" << std::endl;
        briefUsage();
        exit(1);
    }

    // Make sure we have legit values for Storagetype and Status
    if ( new_storageType.length() > 0 &&
            (new_storageType != "0" && new_storageType != "PERMANENT" &&
             new_storageType != "1" && new_storageType != "DURABLE" &&
             new_storageType != "2" && new_storageType != "VOLATILE" ) ) {
        std::cerr << "Invalid Storage Type specified" << std::endl;
        usage();
        exit(1);
    }
    if ( new_Status.length() > 0 &&
            (new_Status != "0" && new_Status != "REQUESTED" &&
             new_Status != "1" && new_Status != "ALLOCATED" &&
             new_Status != "2" && new_Status != "INUSE" &&
             new_Status != "3" && new_Status != "DEALLOCATED" ) ) {
        std::cerr << "Invalid Status specified" << std::endl;
        usage();
        exit(1);
    }

    if ( new_storageType.length() == 0 && 
         new_svcClass.length()    == 0 &&
         new_Status.length()      == 0 ) {
        // Nothing to change
        exit(0);
    }

    castor::Services* svcs = 0;
    castor::BaseAddress srmAd;
    try {
        // Create a db parameters service and fill with appropriate defaults
        castor::IService* s = castor::BaseObject::sharedServices()->service("DbParamsSvc", castor::SVC_DBPARAMSSVC);
        castor::db::DbParamsSvc* params = dynamic_cast<castor::db::DbParamsSvc*>(s);
        if(params == 0) {
            castor::exception::Exception e;
            e.getMessage() << "Could not instantiate the parameters service";
            throw e;
        }
        params->setSchemaVersion(SRMSCHEMAVERSION);
        params->setDbAccessConfFile(ORASRMCONFIGFILE);

        // gets db services
        svcs = castor::BaseObject::services();
        srmAd.setCnvSvcName("DbCnvSvc");
        srmAd.setCnvSvcType(castor::SVC_DBCNV);
        castor::IService* s1 = castor::BaseObject::sharedServices()->service("DbSrmSvc", castor::SVC_DBSRMSVC);
        castor::srm::db::DbSrmSvc* srmSvc = dynamic_cast<castor::srm::db::DbSrmSvc*>(s1);
        if(srmSvc == 0) {
            std::cout << "Fatal, cannot instantiate DbSrmSvc. Check castor.conf." << std::endl;
            return 1;
        }

        // Transform VO name:
        transform(in_voName.begin(), in_voName.end(), in_voName.begin(), ( int(*)(int))tolower);

        // Try and get the storage area used as input:
        castor::srm::StorageArea *sa;
        if ( in_description.length() > 0 ) {
            std::vector<std::string> tokens = srmSvc->getSpaceTokens(&in_voName, &in_description);
            if ( tokens.size() == 0 ) {
                std::cerr << "No space tokens matching description in database" << std::endl;
                exit(1);
            }
            if ( tokens.size() > 1 ) {
                std::cerr << "More than 1 token with required description owned by this VO" << std::endl;
                std::cerr << "Please use printstoragearea to determine the unique token of the one you want to modify" << std::endl;
                exit (1);
            }
            sa = srmSvc->getStorageArea(tokens[0]);
        }
        else {
            sa = srmSvc->getStorageArea(in_token);
        }
        if (!sa) {
            std::cerr << "No StorageArea found with token " << in_token << std::endl;
            exit(1);
        }

        // Now we have an area, update it with the new information required
        if (new_storageType.length() > 0 ) {
            int newType;
            if ( new_storageType == "0" || new_storageType == "PERMANENT") newType=0;
            if ( new_storageType == "1" || new_storageType == "DURABLE")   newType=1;
            if ( new_storageType == "2" || new_storageType == "VOLATILE")  newType=2;
            sa->setStorageType( newType );
        }
        if (new_Status.length() > 0 ) {
            castor::srm::StorageStatus  newStatus;
            if ( new_Status == "0" || new_Status == "REQUESTED")    newStatus = castor::srm::STORAGESTATUS_REQUESTED;
            if ( new_Status == "1" || new_Status == "ALLOCATED")    newStatus = castor::srm::STORAGESTATUS_ALLOCATED;
            if ( new_Status == "2" || new_Status == "INUSE")        newStatus = castor::srm::STORAGESTATUS_INUSE;
            if ( new_Status == "3" || new_Status == "DEALLOCATED")  newStatus = castor::srm::STORAGESTATUS_DEALLOCATED;
            sa->setStorageStatus( newStatus );
        }
        if (new_svcClass.length() > 0 ) {
            sa->setSvcClass(new_svcClass);
        }

        svcs->updateRep(&srmAd, sa, true);
    }
    catch (castor::exception::Exception e) {
        std::cout << "Error caught while updating in db: "
            << sstrerror(e.code()) << std::endl
            << e.getMessage().str() << std::endl;
        return 1;
    }
    return 0;
}
