/******************************************************************************
 *                      EnterStorageArea.cpp
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * A tool to fill up the SRM database according to the service classes
 * and VOs defined in srm2_storagemap.conf
 *
 * @author Giuseppe Lo Presti
 *****************************************************************************/

#include <algorithm>
#include "castor/srm/StorageArea.hpp"
#include "castor/srm/StorageStatus.hpp"
#include "castor/srm/StorageType.hpp"
#include "castor/srm/SrmUser.hpp"
#include "castor/srm/Constants.hpp"
#include "castor/srm/SrmConstants.hpp"
#include "castor/srm/db/DbSrmSvc.hpp"

#include <serrno.h>
#include "castor/Constants.hpp"
#include "castor/Services.hpp"
#include "castor/BaseObject.hpp"
#include "castor/BaseAddress.hpp"
#include "castor/IService.hpp"
#include "castor/IObject.hpp"
#include "castor/db/DbParamsSvc.hpp"
#include "castor/exception/Exception.hpp"

#include <Cuuid.h>
#include <getconfent.h>
#include <iostream>
#include <fstream>
#include <string.h>

/**
  * This routine will validate manually entered data against the contents of the storage map file.
  * In the event of an inconsistentcy, no update is performed.  Currently, the database is not
  * checked so any existing entry will be ovwerwritten
  */
bool validate (std::string vo, std::string svcClass, int storageType, std::string mapFile) {
    bool isValid = true;

    std::ifstream mapFileStream (mapFile.c_str());
    if(!mapFileStream.is_open()) {
        std::cerr << "Unable to open mapfile " << mapFile << " to perform validation." << std::endl;
        return !isValid;
    }

    // We need to make sure the VO is in upper case 
    std::string voUpper = vo;
    transform (voUpper.begin(), voUpper.end(), voUpper.begin(), (int(*)(int))toupper);

    std::string line;
    static const std::string delimiters = " \t";
    while ( !mapFileStream.eof() ) {
        getline(mapFileStream, line);
        // ignore STAGEMAP entries, blank lines or comments...
        if ( line.length() == 0 || line[0] == '#' || line.find("USTGMAP") != std::string::npos || mapFileStream.eof() ) continue;

        // We need to split the line.  This should be into 3 substrings.  Wish we had java here :(
        // Skip delimiters at beginning.
        std::string::size_type lastPos = line.find_first_not_of(delimiters, 0);
        // Find first "non-delimiter".
        std::string::size_type pos     = line.find_first_of(delimiters, lastPos);

        std::vector<std::string> tokens;
        while (std::string::npos != pos || std::string::npos != lastPos) {
            // Found a token, add it to the std::vector.
            tokens.push_back(line.substr(lastPos, pos - lastPos));
            // Skip delimiters.  Note the "not_of"
            lastPos = line.find_first_not_of(delimiters, pos);
            // Find next "non-delimiter"
            pos = line.find_first_of(delimiters, lastPos);
        }
        if ( tokens.size() != 3 ) continue;

        // See if we have a match for input svcclass and vo
        if ( tokens[0] == vo && tokens[2] == svcClass ) {
            // We have a match, so check the type
            switch (storageType) {
                case castor::srm::STORAGETYPE_PERMANENT:
                    if ( tokens[1].find("TAPE1") == std::string::npos ) isValid = false;
                    break;
                case castor::srm::STORAGETYPE_DURABLE:
                    if ( tokens[1] != "TAPE0DISK1" ) isValid = false;
                    break;
                case castor::srm::STORAGETYPE_VOLATILE:
                    if ( tokens[1] != "TAPE0DISK0" ) isValid = false;
                    break;
                default:
                    // Unless the second token is default we assume this is an error
                    if ( tokens[1] != "default" ) {
                        isValid = false;
                        std::cerr << "Input conflicts with storage map file for svc class " << svcClass << ", VO " << vo << std::endl;
                    }
                    break;
            }
            //break;
        }
    }
    return isValid;
}


void usage() {
    std::cout << "Usage: " << std::endl;
    std::cout << "\t enterStorageArea [-h] -v voName -d description -s svcClass -t type [-f]" << std::endl;
    std::cout << std::endl;
    std::cout << "The first form reads entries from the storage map file (normally srm2_storagemap.conf). " << std::endl;
    std::cout << "where voName is the name of the VO owning this space, " << std::endl;
    std::cout << "svcClass is the CASTOR service class that the area maps and " << std::endl;
    std::cout << "type is the storagetype (0=PERMANENT, 1=DURABLE, 2=VOLATILE)" << std::endl;
    std::cout << "-f is a force option and will prevent consistency checking. Without this option, the entered" << std::endl;
    std::cout << "values are checked against the values in the default storage map file and inconsistent values" << std::endl;
    std::cout << "are reported back and not entered into the database." << std::endl;
}


int main (int argc, char** argv) {

    // gets mapping file (srm2_storagemap.conf by default) and make sure it is readable
    // if we are not forcing entries
    std::string mapFile = castor::srm::SrmConstants::getInstance()->storageMappingFile();

    // parse the arguments if any
    std::string vo;
    std::string svcClass;
    std::string tokenDescription;
    int storageType;
    bool force=false;
    if ( argc > 1 ) {
        int cArg = 1;
        while (cArg < argc) {
            if ( strcmp( argv[cArg], "-v" ) == 0 ) {
                vo.assign(argv[++cArg]);
            }
            else if ( strcmp(argv[cArg], "-s" ) == 0 ) {
                svcClass.assign(argv[++cArg]);
            }
            else if ( strcmp(argv[cArg], "-t" ) == 0 ) {
                storageType = atoi( argv[++cArg] );
            }
            else if ( strcmp(argv[cArg], "-d" ) == 0 ) {
                tokenDescription.assign( argv[++cArg] );
            }
            else if ( strcmp(argv[cArg], "-f" ) == 0 ) {
                force=true;
            }
            else {
                std::cerr << "Inavlid argumentr" << std::endl;
                usage();
                exit(1);
            }
            cArg++;
        }
        if ( vo.length() == 0 || svcClass.length() == 0 || tokenDescription.length() == 0 ) {
            std::cout << "Missing argument" << std::endl;
            usage();
            exit(1);
        }

        if ( !force ) {
            if (!validate(vo, svcClass, storageType, mapFile)) {
                std::cerr << "Mismatch between storage type and contents of storagemap file" << std::endl;
                exit(1);
            }
        }
    }

    castor::Services* svcs = 0;
    castor::BaseAddress srmAd;

    try {
        // Create a db parameters service and fill with appropriate defaults
        castor::IService* s = castor::BaseObject::sharedServices()->service("DbParamsSvc", castor::SVC_DBPARAMSSVC);
        castor::db::DbParamsSvc* params = dynamic_cast<castor::db::DbParamsSvc*>(s);
        if(params == 0) {
            castor::exception::Exception e;
            e.getMessage() << "Could not instantiate the parameters service";
            throw e;
        }
        params->setSchemaVersion(SRMSCHEMAVERSION);
        params->setDbAccessConfFile(ORASRMCONFIGFILE);

        // gets db services
        svcs = castor::BaseObject::services();
        srmAd.setCnvSvcName("DbCnvSvc");
        srmAd.setCnvSvcType(castor::SVC_DBCNV);
        castor::IService* s1 = castor::BaseObject::sharedServices()->service("DbSrmSvc", castor::SVC_DBSRMSVC);
        castor::srm::db::DbSrmSvc* srmSvc = dynamic_cast<castor::srm::db::DbSrmSvc*>(s1);
        if(srmSvc == 0) {
            std::cout << "Fatal, cannot instantiate DbSrmSvc. Check castor.conf." << std::endl;
            return 1;
        }

        if ( argc > 1 ) {
            // Putting in a single entry
            // Create or Get a user based on the vo
            transform(vo.begin(), vo.end(), vo.begin(), ( int(*)(int))tolower);
            castor::srm::SrmUser* user = srmSvc->getSrmUser("castor", vo, "<CASTOR>");
            svcs->fillObj(&srmAd, user, castor::srm::OBJ_StorageArea, false);

            // Create a generic storage area
            castor::srm::StorageArea *area = new castor::srm::StorageArea();

            // Fill in the values
            std::ostringstream sptok;
            sptok << vo << ":" << tokenDescription;   // space token ::= <VO>:<description>
            area->setSpaceToken(sptok.str());
            area->setTokenDescription(tokenDescription);
            area->setStorageLifetime(time(NULL)*2);  // this is big enough for any purpose.
                                                     // ULONG_MAX gets rounded to 0 at first read back so it can't be used!
            area->setSvcClass(svcClass);
            area->setStorageType(storageType);   // VO managed
            area->setStorageStatus(castor::srm::STORAGESTATUS_ALLOCATED);
            area->setSrmUser(user);
            svcs->createRep(&srmAd, area, false);
            svcs->fillRep(&srmAd, area, castor::srm::OBJ_SrmUser, false);
            user->addStorageArea(area);
            svcs->updateRep(&srmAd, user, true);
        }
        else {
            usage();
        }
    }
    catch (castor::exception::Exception e) {
        std::cout << "Error caught while storing in db: "
            << sstrerror(e.code()) << std::endl
            << e.getMessage().str() << std::endl;
        return 1;
    }
    return 0;
}
