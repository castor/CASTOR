/******************************************************************************
 *                      CastorSvcClassQuery.cpp
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Query Castor database for available service classes
 *
 * @author Giuseppe Lo Presti
 *****************************************************************************/

// Include Files
#include <string>
#include <sstream>
#include <vector>
#include "castor/IAddress.hpp"
#include "castor/IObject.hpp"
#include "castor/Constants.hpp"
#include "castor/Services.hpp"
#include "castor/db/DbCnvSvc.hpp"
#include "castor/db/IDbResultSet.hpp"
#include "castor/exception/Exception.hpp"
#include "castor/exception/Exception.hpp"
#include "castor/exception/SQLError.hpp"
#include "castor/stager/SvcClass.hpp"
#include "castor/srm/admin/CastorSvcClassQuery.hpp"

//------------------------------------------------------------------------------
// Static constants initialization
//------------------------------------------------------------------------------
/// SQL statement for getRequestStatus
const std::string castor::srm::admin::CastorSvcClassQuery::s_getSvcClassesStatementString =
  "SELECT id FROM SvcClass ORDER BY name DESC";

// -----------------------------------------------------------------------
// getSvcClasses
// -----------------------------------------------------------------------
void castor::srm::admin::CastorSvcClassQuery::reset() throw() {
  try {
    if(m_getSvcClassesStatement) delete m_getSvcClassesStatement;
  }
  catch (castor::exception::SQLError ignored) {};
  m_getSvcClassesStatement = 0;
}

// -----------------------------------------------------------------------
// getSvcClasses
// -----------------------------------------------------------------------
std::vector<castor::stager::SvcClass*> 
castor::srm::admin::CastorSvcClassQuery::getSvcClasses()
  throw (castor::exception::Exception) {
  try {
    // Check whether the statements is ok
    if (0 == m_getSvcClassesStatement)
      m_getSvcClassesStatement = createStatement(s_getSvcClassesStatementString);
    
    // Execute statement and get result
    std::vector<castor::stager::SvcClass*> result;
    castor::db::IDbResultSet *rset = m_getSvcClassesStatement->executeQuery();
    
    while (rset->next()) {
      u_signed64 id = rset->getInt64(1);
      castor::IObject* obj = m_cnvSvc->getObjFromId(id);
      castor::stager::SvcClass* s = dynamic_cast<castor::stager::SvcClass*>(obj);
      if(0 == s) {
         castor::exception::Exception ex;
         delete rset;
         ex.getMessage() << "getSvcClasses: could not retrieve object from id " << id;
         throw ex;
      }
      
      result.push_back(s);
    }
    delete rset;
    return result;
  }
  catch (castor::exception::SQLError e) {
    rollback();
    castor::exception::Exception ex;
    ex.getMessage()
      << "Error caught in castor::srm::getRequestStatus."
      << std::endl << e.getMessage().str();
    throw ex;
  }
}
