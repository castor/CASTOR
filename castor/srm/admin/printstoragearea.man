.\" @(#)$Id: printstoragearea.man,v 1.3 2008/12/04 23:20:16 sdewitt Exp $
.\" Copyright (C) 2008 by CERN/IT
.\" All rights reserved
.\"
.TH LISTSTORAGEAREA 1castor "$Date: 2008/12/04 23:20:16 $" CASTOR "SRM Admin Commands"
.SH NAME
printstoragearea \- List SRM 'spaces' currently in the SRM database.
.SH SYNOPSIS
.B printstoragearea
.BI -V
.BI VOName
[
.BI -D
.BI tokenDescription
]
[
.BI -h
]

.SH DESCRIPTION
The \fBprintstoragearea\fP command is used to list SRM spaces owned by a specified VO.  In addition, a \fItokenDescription\fP may
also be specified which will return information only for spaces with that description.
.P
The information returned shows the actual space token (a unique identifier required for other calls), the token description, 
the CASTOR service class to which the space maps, its storage type and its current status.  Valid values for the \fIstorage type\fP
are:
.br
PERMANENT:  The files are backed up onto tape by the CASTOR back end.
.br
DURABLE: Files placed in this space are not backed up to tape, but are not deleted by CASTOR backend
.br
VOLATILE: Files placed in this space are not backed up to tape, and are subject the garbage collection by the CASTOR backend.
.P
Valid values for the \fIstatus\fP of the space are:
.br
REQUESTED: The space has been requested, but no disk servers have been assigned to the corresponding CASTOR service class.
.br
ALLOCATED: The space is ready for use.
.br
DEALLOCATED: The space has been deallocated by the CASTOR admins and should no longer be used.
.br
EXPIRED: The lifetime of the space has expired (currently not used since spaces are assumed to last forever).


.SH OPTIONS

.IP "-V VOName"
Specify the VO that owns the spaces.  This is not case sensitive.
.IP "-d tokenDescription"
Limit returns to spaces with this description.
.IP -h
Print a brief help message.

.SH FILES
None


.SH EXAMPLES
.nf
$ printstoragearea -V dteam 
<Space Token>                            <Description>   <service class>         <type>          <status>
0749ae7b-e672-4a9b-ace8-647217f2f54b    "default"       "default"                "PERMANENT"     "ALLOCATED"
4798a75c-0000-1000-b2f4-a4b4238c0000    "itdc"          "itdc"                   "PERMANENT"     "ALLOCATED"
4755c348-0000-1000-b34d-956ba3ff0000    ""              ""                       "PERMANENT"     "REQUESTED"
47d7f79f-0000-1000-9630-acc54fc70000    "foo"           "bar1"                   "PERMANENT"     "ALLOCATED"
47d7f581-0000-1000-9cff-fe87695f0000    "foo"           "bar2"                   "PERMANENT"     "ALLOCATED"
.fi


.nf
$ printstoragearea -V dteam -D foo
<Space Token>                            <Description>   <service class>         <type>          <status>
47d7f581-0000-1000-9cff-fe87695f0000    "foo"            "bar2"                  "PERMANENT"     "ALLOCATED"
47d7f79f-0000-1000-9630-acc54fc70000    "foo"            "bar1"                  "PERMANENT"     "ALLOCATED"
.fi

.nf
$ printstoragearea -V foo
No Storage areas found owned by this VO and matching specified description
.fi

.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.

.SH FILES
.I /etc/castor/castor.conf

.SH SEE ALSO
.BR modifystoragearea(1castor)
.BR enterStorageArea(1castor)
.BR deletestoragearea(1castor)

.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
