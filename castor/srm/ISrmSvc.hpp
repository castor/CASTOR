/******************************************************************************
 *                srm/ISrmSvc.hpp
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * @(#)ISrmSvc.hpp,v 1.28 $Release$ 2008/06/04 10:20:00 itglp
 *
 * This class provides database API for SRM
 *
 * @author Giuseppe Lo Presti
 *****************************************************************************/

#ifndef SRM_ISRMSVC_HPP
#define SRM_ISRMSVC_HPP 1

// Include Files
#include "castor/Constants.hpp"
#include "castor/IService.hpp"
#include "castor/exception/Exception.hpp"
#include <vector>
#include <string>
#include <list>
#include <map>


namespace castor {

  // Forward declarations
  class IObject;
  class IClient;
  class IAddress;

  namespace srm {

    // Forward declarations
    class StageRequest;
    class SubRequest;
    class UserFile;
    class SrmUser;
    class StorageArea;
    class Pin;
    
    /**
     * This class provides database API for SRM
     */
    class ISrmSvc : public virtual castor::IService {

    public:

      /**
       * Gets a user object based on distinguished name
       * and user name.  Entries in the SrmUser database
       * should be unique.
       * @param username  The user name obtained from get_client_username
       * @param dn  The distignuished name for the user obtained from
       * get_client_dn
       * @return The unique SrmUser object
       * @exception Exception is case of error
       */
        virtual castor::srm::SrmUser* getSrmUser (std::string username, std::string vo, std::string dn)
            throw (castor::exception::Exception) = 0;

        /**
         * Gets a UserFile object based on Castor file name
         * derived from SURL. The entry must exist from a previous
         * call to createUserFile.
         * @param castorFileName  Castor file name derived from SURL
         * @return The unique UserFile object
         * @exception Exception is case of error
         */
        virtual castor::srm::UserFile* getUserFile (std::string castorFileName)
            throw (castor::exception::Exception) = 0;
            
        /**
         * Gets a UserFile object based on the Castor file name
         * given in the SubRequest. Create an entry if it does not
         * exists, and fill the link accordingly.
         * @param subReq an SRM SubRequest
         * @param nsFileId the Castor NS fileid of this file
         * @param nsHost the Castor NS host
         * @param nsFileSize the Castor NS file size of this file
         * @return The unique UserFile object
         * @exception Exception is case of error
         */
        virtual castor::srm::UserFile* createUserFile(srm::SubRequest* subreq, u_signed64 nsFileId,
            std::string nsHost, u_signed64 nsFileSize = 0)
            throw (castor::exception::Exception) = 0;
            
        /**
          * Get a storage area based on the space token.
          * @param spaceToken  the space token associated with a
          *                    storage area
          * @return The StorageArea, or NULL is not found.
          * @excpetion Exception is thrown in case of error.
          */
        virtual castor::srm::StorageArea* getStorageArea(std::string spaceToken)
            throw (castor::exception::Exception) = 0;

        /**
          * Get a StageRequest object based on the request token
          * @param requestToken  the request token associated with a
          *                    StageRequest
          * @return The StageRequest object, or NULL is not found.
          * @excpetion Exception is thrown in case of error.
          */
        virtual castor::srm::StageRequest* getStageRequest(std::string requestToken)
            throw (castor::exception::Exception) = 0;

        /**
          * Get all the request tokens belonging to a user, optionally matching a
          * supplied request description.
          * @param userId  The user database id
          * @param description The request description
          * @return A std::vector of request token strings
          */
        virtual std::vector<std::string> getRequestTokens( u_signed64 userId, std::string* description=NULL)
          throw (castor::exception::Exception) = 0;

        /**
          * Get all the space tokens belonging to a user, optionally matching a
          * supplied description.
          * @param userId  The user database id
          * @param description The space token description
          * @return A std::vector of space token strings
          */
        virtual std::vector<std::string> getSpaceTokens( std::string *voName=NULL, std::string* description=NULL)
          throw (castor::exception::Exception) = 0;

        /**
         * Lock a StageRequest by a SELECT FOR UPDATE
         * @param reqId the StageRequest id
         * @exception Exception is thrown in case of error.
         */
        virtual void lockStageRequest(u_signed64 reqId)
          throw (castor::exception::Exception) = 0;

        /**
          * Set StageRequests to ABORTED based on the file name.  This is a helper
          * function which is used by srmRm.  All PUT requests still pending a PUTDONE
          * are aborted (since the file is anyway removed)
          */
        virtual void abortPutRequestByFile (std::string& filename)
          throw (castor::exception::Exception) = 0;

        /**
          * Get a std::list of subrequests to release for srmReleaseFiles.  The subrequests are
          * selected based on the file name and the user.  Only subrequests which are competed
          * (status SUBREQUEST_SUCCESS, SUBREQUEST_TURLSENT and SUBREQUEST_RELEASED) are considered
          * and only those where the corresponding request type is a GET or BringOnline; PUTS
          * are not considered.
          */
        virtual std::list<srm::SubRequest*> subRequestsToRelease( std::string& filename, u_signed64 userid)
          throw (castor::exception::Exception) = 0;

        /**
         * Get the list of space token descriptions for a given VO. Used by srmLs
         * @param vo The VO name
         * @return a map of service classes to space token descriptions
         */
        virtual std::map<std::string, std::string> getSpaceTokensList(std::string vo)
          throw (castor::exception::Exception) = 0;
    }; // end of class ISrmSvc

} } /* end of namespace castor/srm */

#endif // SRM_ISRMSVC_HPP
