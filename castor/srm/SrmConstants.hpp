#ifndef SRM_SERVER_CONSTANTS_HPP
#define SRM_SERVER_CONSTANTS_HPP

#include <pthread.h>
#include <string>
#include <vector>

#include <castor/exception/Exception.hpp>
#include <osdep.h>

/**
  * Class containing constants read from the srm configuration file.  
  * This will read either a user specifed
  * configuration file, a configuration file specified by the environment variable 
  * SRMV2_CONFIG_PATH, or /etc/castor/castor.conf (in that order). This is a singleton class and
  * all parameters are given a default value by the constructor.  Where command line arguments
  * can be used to override the defaults, setter methods are provided.  Note that in the
  * configuration file, SRM indicates a parameter used by the SRM server and SRMD is used
  * by the daemon.
  */

// hardcoded schema version for the SRM database
const std::string SRMSCHEMAVERSION = "2_14_0";

// hardcoded port number for the request handler
const int STAGERPORT = 9002;

namespace castor {
 namespace srm {
  class SrmConstants {
      public:

          //=============================================================
          // Getter Methods
          //=============================================================
          /**
            * Get default size of storage area.
            */
          const u_signed64& defaultStorageAreaSize() { return _storageAreaSize; }

          /**
            * Get default pin lifetime.
            */
          const long& defaultLifetime() {return _pinlifetime;}

          /**
            * Get default storage lifetime.
            */
          const long& defaultStorageLifetime() {return _storlifetime;}

          /**
            * Get default file size
            */
          const u_signed64& defaultFileSize() {return _fileSize;}

          /**
            * Get maximum number of returns from srmLs
            */
          const unsigned& maxReturns() {return _lsMaxReturns;}

          /**
            * get Maximum value allowed for (count + offset) in srmLs
            */
          const unsigned& maxCount() {return _lsMaxCount;}

          /**
            * Get the SOAP backlog
            */
          const unsigned& soapBacklog() {return _soapBacklog;}

          /**
            * Get the SOAP recieve timeout
            */
          const unsigned& soapRecieveTimeout() {return _soapRecvTime;}

          /**
            * Get the SOAP send timeout
            */
          const unsigned& soapSendTimeout() {return _soapSendTime;}
          
          /**
            * Get the max number of Castor threads that can consurrently run
            */
          const unsigned& maxCastorThreads() { return _maxCastorThreads; }

          /**
            * Get the port to listen  on
            */
          const unsigned& port() {return _port;}

          /**
            * Get the supported protocols.
            */
          const std::vector<std::string>& supportedProtocols() { return _supportedProtocols;}

          /**
            * Get init number of threads for SRM server
            */
          const unsigned& nbServerThreads() { return _serverThreads; }

          /**
            * Get max number of threads for SRM server
            */
          const unsigned& nbMaxServerThreads() { return _maxServerThreads; }

          /**
            * Get domain name
            */
          const std::string& domainname() {return _domainName;}; 

          /**
            * Get gsiftp turl prefix
            */
          const std::string& gsiTURL() {return _gsiTURL;}; 

          /**
            * Get rfio turl prefix
            */
          const std::string& rfioTURL() {return _rfioTURL;}; 

          /**
            * Get number of req threads for SRM daemon
            */
          const unsigned& nbReqThreads() { return _reqThreads; }

          /**
            * Get number of Turl threads for SRM daemon
            */
          const unsigned& nbTurlThreads() { return _turlThreads; }

          /**
            * Get number of poller threads for SRM daemon
            */
          const unsigned& nbPollThreads() { return _pollThreads; }

          /**
            * Get number of copy threads for SRM daemon
            */
          const unsigned& nbCopyThreads() { return _copyThreads; }
          
          /**
            * Get notify host
            */
          const std::string& notifyHost() { return _notifyHost; }

          /**
            * Get daemon req notify port
            */
          const unsigned& reqNotifyPort() { return _reqNotifyPort; }

          /**
            * Get back-end callback port
            */
          const unsigned& beCallbackPort() { return _beCallbackPort; }

          /**
            * Get timeout for asynch stager requests
            */
          const unsigned& initialPollInterval() { return _initialPollInterval; }

          /**
            * Get GSI FTP transfer command
            */
          const std::string& gsiCmd() { return _gsiFtpCmd; }

          /**
            * Get rfio transfer command
            */
          const std::string& rfioCmd() { return _rfioCpCmd; }
          
          /**
            * Get GSI Key file
            */
          const std::string& keyFile() {return _keyFile;}

          /**
            * Get the CA certificate file
            */
          const std::string& certificateAuthorityFile() {return _caFile;}

          /**
            * Get CA password
            */
          const std::string& certificateAuthorityPassword() {return _caPasswd;}


          /**
            * Get the mapping file which maps retention policy to service 
            * class.
            */
          const std::string & storageMappingFile() {return _mapFile;}

          /**
            * Get the Grid FTP port number
            */
          const unsigned& gsiftpPort() {return _gsiftpPort;}

          /**
            * Get the name server host
            */
          const std::string & nsHost() {return _nsHost;}
          
          /**
           * Get stage superuser uid & gid
           */
          uid_t getStageUid() { return _stage_uid; }
          gid_t getStageGid() { return _stage_gid; }

          //=============================================================
          // Setter Methods
          //=============================================================

          /**
            * Set the soap backlog
            */
          void setSoapBacklog(unsigned value);

          /**
            * Set the soap revieve timeout
            */
          void setSoapRecvTimout(unsigned value);

          /**
            * Set the soap send timeout
            */
          void setSoapSendTimout(unsigned value);

          /**
            * Set maximum number of concurrent ls threads
            */
          void setMaxCastorThreads(int value);

          /**
            * Set the port number to listen on
            */
          void setPort(unsigned port);

          /**
            * Set number of init threads for SRM server
            */
          void setNbServerThreads( unsigned nThreads );

          /**
            * Set number of max threads for SRM server
            */
          void setNbMaxServerThreads( unsigned nThreads );

          /**
            * Set number of req threads for SRM daemon
            */
          void setNbReqThreads( unsigned nThreads );

          /**
            * Set number of TURL threads for SRM daemon
            */
          void setNbTurlThreads( unsigned nThreads );

          /**
            * Set number of poller threads for SRM daemon
            */
          void setNbPollThreads( unsigned nThreads );

          /**
            * Set number of copy threads for SRM daemon
            */
          void setNbCopyThreads( unsigned nThreads );

          /**
            * Set callback port for asynch client
            */
          void setBECallbackPort( unsigned value );

          /**
            * Set initial time interval for polling the stager
            */
          void setInitialPollInterval( unsigned value );
          
          //=============================================================
          // Other Methods
          //=============================================================

          /**
            * Get instance of class
            */
          static SrmConstants *getInstance();

          /**
            * Read default config file, i.e. contents of PATH_CONFIG
            */
          void readConfigFile() throw (castor::exception::Exception);

      protected:

      private:
          SrmConstants();
          ~SrmConstants();

          // Local parameters
          static SrmConstants *instance;
          static pthread_mutex_t _mutex;
          bool   _isParsed;   // Indicates if we have read the config file yet
          bool   _readCallingSetter; // Is readConfig calling the setter?
          std::string _nsHost;

          // Server parameters
          u_signed64  _storageAreaSize;
          long        _pinlifetime;
          long        _storlifetime;
          u_signed64  _fileSize;
          unsigned    _lsMaxReturns;
          unsigned    _lsMaxCount;
          unsigned    _soapBacklog;
          unsigned    _soapRecvTime;
          unsigned    _soapSendTime;
          unsigned    _port;
          unsigned    _gsiftpPort;
          std::vector<std::string> _supportedProtocols;
          unsigned    _serverThreads;
          unsigned    _maxServerThreads;
          unsigned    _maxCastorThreads;
          std::string _domainName;
          std::string _gsiTURL;
          std::string _rfioTURL;
          //std::vector<std::string> _altSURL;
          std::string _mapFile;
          std::string _gsiFtpCmd;
          std::string _rfioCpCmd;
          std::string _keyFile;
          std::string _caFile;
          std::string _caPasswd;

          // Daemon parameters
          unsigned    _reqThreads;
          unsigned    _turlThreads;
          unsigned    _pollThreads;
          unsigned    _copyThreads;
          std::string _notifyHost;
          unsigned    _reqNotifyPort;
          unsigned    _beCallbackPort;
          unsigned    _initialPollInterval;
          
          uid_t _stage_uid;
          gid_t _stage_gid;       
  };

} } // end of namespace castor/srm

#endif
