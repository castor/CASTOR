#include <sys/stat.h>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <errno.h>

#include <Cpwd.h>
#include <Cgrp.h>
#include <getconfent.h>
#include <stage_constants.h>   // for STAGERSUPERUSER/GROUP
#include <castor/exception/Exception.hpp>

#include "SrmConstants.hpp"

castor::srm::SrmConstants *castor::srm::SrmConstants::instance = 0;
pthread_mutex_t castor::srm::SrmConstants::_mutex = PTHREAD_MUTEX_INITIALIZER;

castor::srm::SrmConstants* castor::srm::SrmConstants::getInstance() {
    if ( 0 == instance ) {
        pthread_mutex_lock(&_mutex); 
        if ( 0 == instance ) {
            instance = new SrmConstants();
        }
        pthread_mutex_unlock(&_mutex);
    }
    return instance;
}

castor::srm::SrmConstants::SrmConstants() {
    // Original initial values
    // Server specific
    _storageAreaSize = 10737418240LL;
    _pinlifetime     = 3600L;
    _storlifetime    = -1L;
    _fileSize        = 1073741824LL;
    _lsMaxReturns    = 10000;
    _lsMaxCount      = 50000;
    _soapBacklog     = 500;
    _soapRecvTime    = 30;
    _soapSendTime    = 30;
    _port            = 8443;
    _gsiftpPort      = 2811;
    _supportedProtocols.push_back("xroot");
    _supportedProtocols.push_back("gsiftp");
    _supportedProtocols.push_back("rfio");
    _serverThreads   = 50;
    _maxServerThreads= 60;
    _maxCastorThreads= 45;
    _domainName      = "";
    _gsiTURL         = "gsiftp://";
    _rfioTURL        = "rfio://";
    _mapFile         = "/etc/castor/srm2_storagemap.conf";
    _nsHost          = "castorns";

    // Daemon specific
    _reqThreads         = 5;
    _turlThreads        = 5;
    _pollThreads        = 5;
    _copyThreads        = 1;
    _notifyHost         = "";
    _reqNotifyPort      = 65001;
    _beCallbackPort     = 30000;
    _initialPollInterval= 20;
    
    _gsiFtpCmd       = "/usr/bin/globus-url-copy -nodcau";
    _rfioCpCmd       = "/usr/bin/rfcp";

    // Other
    _readCallingSetter = false;
    _isParsed = false;
    
    // Retrieve stage:st uid & gid
    struct passwd *usr = getpwnam(STAGERSUPERUSER);
    if (usr == NULL) {
      castor::exception::Exception e(errno);
      e.getMessage() << "Failed to getpwnam for user: " << STAGERSUPERUSER << std::endl;
      throw e;
    }
    struct group *grp = getgrnam(STAGERSUPERGROUP);
    if (grp == NULL) {
      castor::exception::Exception e(errno);
      e.getMessage() << "Failed to getgrnam for group: " << STAGERSUPERGROUP << std::endl;
      throw e;
    }
    _stage_uid = usr->pw_uid;
    _stage_gid = grp->gr_gid;
}

castor::srm::SrmConstants::~SrmConstants() {}

void castor::srm::SrmConstants::setPort(unsigned value) {
    static bool isSet = false; // Set to true is overridden by command line
    if ( !_readCallingSetter ) {
        // Something else is calling the setter
        if ( !isSet ) {
            // Take a lock
            pthread_mutex_lock(&_mutex);
            if ( !isSet ) {
                isSet = true;
                _port = value;
            }
            pthread_mutex_unlock(&_mutex);
        }
    }
    else {
        // read already has a mutex - if we have not been set by something else
        // set to the config value
        if ( !isSet ) _port = value;
    }
}

void castor::srm::SrmConstants::setNbServerThreads(unsigned value) {
    static bool isSet = false; // Set to true is overridden by command line
    if ( !_readCallingSetter ) {
        // Something else is calling the setter
        if ( !isSet ) {
            // Take a lock
            pthread_mutex_lock(&_mutex);
            if ( !isSet ) {
                isSet = true;
                _serverThreads = value;
            }
            pthread_mutex_unlock(&_mutex);
        }
    }
    else {
        // read already has a mutex - if we have not been set by something else
        // set to the config value
        if ( !isSet ) _serverThreads = value;
    }
}

void castor::srm::SrmConstants::setNbMaxServerThreads(unsigned value) {
    static bool isSet = false; // Set to true is overridden by command line
    if ( !_readCallingSetter ) {
        // Something else is calling the setter
        if ( !isSet ) {
            // Take a lock
            pthread_mutex_lock(&_mutex);
            if ( !isSet ) {
                isSet = true;
                _maxServerThreads = value;
            }
            pthread_mutex_unlock(&_mutex);
        }
    }
    else {
        // read already has a mutex - if we have not been set by something else
        // set to the config value
        if ( !isSet ) _maxServerThreads = value;
    }
}

void castor::srm::SrmConstants::setSoapBacklog(unsigned value) {
    static bool isSet = false; // Set to true is overridden by command line
    if ( !_readCallingSetter ) {
        // Something else is calling the setter
        if ( !isSet ) {
            // Take a lock
            pthread_mutex_lock(&_mutex);
            if ( !isSet ) {
                isSet = true;
                _soapBacklog = value;
            }
            pthread_mutex_unlock(&_mutex);
        }
    }
    else {
        // read already has a mutex - if we have not been set by something else
        // set to the config value
        if ( !isSet ) _soapBacklog = value;
    }
}

void castor::srm::SrmConstants::setSoapRecvTimout(unsigned value) {
    static bool isSet = false; // Set to true is overridden by command line
    if ( !_readCallingSetter ) {
        // Something else is calling the setter
        if ( !isSet ) {
            // Take a lock
            pthread_mutex_lock(&_mutex);
            if ( !isSet ) {
                isSet = true;
                _soapRecvTime = value;
            }
            pthread_mutex_unlock(&_mutex);
        }
    }
    else {
        // read already has a mutex - if we have not been set by something else
        // set to the config value
        if ( !isSet ) _soapRecvTime = value;
    }
}

void castor::srm::SrmConstants::setSoapSendTimout(unsigned value) {
    static bool isSet = false; // Set to true is overridden by command line
    if ( !_readCallingSetter ) {
        // Something else is calling the setter
        if ( !isSet ) {
            // Take a lock
            pthread_mutex_lock(&_mutex);
            if ( !isSet ) {
                isSet = true;
                _soapSendTime = value;
            }
            pthread_mutex_unlock(&_mutex);
        }
    }
    else {
        // read already has a mutex - if we have not been set by something else
        // set to the config value
        if ( !isSet ) _soapSendTime = value;
    }
}

void castor::srm::SrmConstants::setNbReqThreads(unsigned value) {
    static bool isSet = false; // Set to true is overridden by command line
    if ( !_readCallingSetter ) {
        // Something else is calling the setter
        if ( !isSet ) {
            // Take a lock
            pthread_mutex_lock(&_mutex);
            if ( !isSet ) {
                isSet = true;
                _reqThreads = value;
            }
            pthread_mutex_unlock(&_mutex);
        }
    }
    else {
        // read already has a mutex - if we have not been set by something else
        // set to the config value
        if ( !isSet ) _reqThreads = value;
    }
}

void castor::srm::SrmConstants::setNbTurlThreads(unsigned value) {
    static bool isSet = false; // Set to true is overridden by command line
    if ( !_readCallingSetter ) {
        // Something else is calling the setter
        if ( !isSet ) {
            // Take a lock
            pthread_mutex_lock(&_mutex);
            if ( !isSet ) {
                isSet = true;
                _turlThreads = value;
            }
            pthread_mutex_unlock(&_mutex);
        }
    }
    else {
        // read already has a mutex - if we have not been set by something else
        // set to the config value
        if ( !isSet ) _turlThreads = value;
    }
}

void castor::srm::SrmConstants::setNbPollThreads(unsigned value) {
    static bool isSet = false; // Set to true is overridden by command line
    if ( !_readCallingSetter ) {
        // Something else is calling the setter
        if ( !isSet ) {
            // Take a lock
            pthread_mutex_lock(&_mutex);
            if ( !isSet ) {
                isSet = true;
                _pollThreads = value;
            }
            pthread_mutex_unlock(&_mutex);
        }
    }
    else {
        // read already has a mutex - if we have not been set by something else
        // set to the config value
        if ( !isSet ) _pollThreads = value;
    }
}

void castor::srm::SrmConstants::setNbCopyThreads(unsigned value) {
    static bool isSet = false; // Set to true is overridden by command line
    if ( !_readCallingSetter ) {
        // Something else is calling the setter
        if ( !isSet ) {
            // Take a lock
            pthread_mutex_lock(&_mutex);
            if ( !isSet ) {
                isSet = true;
                _copyThreads = value;
            }
            pthread_mutex_unlock(&_mutex);
        }
    }
    else {
        // read already has a mutex - if we have not been set by something else
        // set to the config value
        if ( !isSet ) _copyThreads = value;
    }
}

void castor::srm::SrmConstants::setBECallbackPort(unsigned value) {
    static bool isSet = false; // Set to true is overridden by command line
    if ( !_readCallingSetter ) {
        // Something else is calling the setter
        if ( !isSet ) {
            // Take a lock
            pthread_mutex_lock(&_mutex);
            if ( !isSet ) {
                isSet = true;
                _beCallbackPort = value;
            }
            pthread_mutex_unlock(&_mutex);
        }
    }
    else {
        // read already has a mutex - if we have not been set by something else
        // set to the config value
        if ( !isSet ) _beCallbackPort = value;
    }
}

void castor::srm::SrmConstants::setInitialPollInterval(unsigned value) {
    static bool isSet = false; // Set to true is overridden by command line
    if ( !_readCallingSetter ) {
        // Something else is calling the setter
        if ( !isSet ) {
            // Take a lock
            pthread_mutex_lock(&_mutex);
            if ( !isSet ) {
                isSet = true;
                _initialPollInterval = value;
            }
            pthread_mutex_unlock(&_mutex);
        }
    }
    else {
        // read already has a mutex - if we have not been set by something else
        // set to the config value
        if ( !isSet ) _initialPollInterval = value;
    }
}

void castor::srm::SrmConstants::setMaxCastorThreads(int value) {
    /*
     * This is a bit of a special case.  While it can not be set by the command
     * line, the number of server threads can be.  Since the ls call is blocking,
     * we need to ensure that there are always less ls threads than server threads
     */
    static unsigned isSet = false;
    if ( !_readCallingSetter ) {
        // Something else called the setNumberOfServerThreads method, and that has called us
        if ( !isSet ) {
            pthread_mutex_lock(&_mutex);
            if ( !isSet ) {
                if ( nbServerThreads() == 1 ) {
                    _maxCastorThreads = 1;
                }
                else {
                  if ( value < 0 || value < (int)nbServerThreads() ) {
                        _maxCastorThreads = value;
                        isSet = true;
                    }
                }
                pthread_mutex_unlock(&_mutex);
            }
        }
        else {
            if ( !isSet )  _maxCastorThreads = value;
        }
    }
}


void castor::srm::SrmConstants::readConfigFile() throw (castor::exception::Exception) {

    char* ptr = NULL;
    if ( !_isParsed ) {
        pthread_mutex_lock(&_mutex);
        if ( !_isParsed ) {
            _readCallingSetter = true;

            // Port
            if ( (ptr = getenv("SRM_PORT")) == NULL ) 
                ptr = getconfent("SRM", "PORT", 0);
            if (ptr) setPort(atoi(ptr));

            // StorageArea
            if ( (ptr = getenv("SRM_STORAGEAREA")) == NULL )
                ptr = getconfent("SRM", "STORAGEAREA", 0);
            if (ptr) _storageAreaSize = (u_signed64)atoll(ptr);

            if ( (ptr = getenv("SRM_STORAGEMAP")) == NULL )
                ptr = getconfent("SRM", "STORAGEMAP", 0);
            if (ptr) _mapFile = ptr;

            // Pin time
            if ( (ptr = getenv("SRM_PINTIME")) == NULL )
                ptr = getconfent("SRM", "PINTIME", 0);
            if (ptr) _pinlifetime = atol(ptr);

            // Storage time
            if ( (ptr = getenv("SRM_STORTIME")) == NULL )
                ptr = getconfent("SRM", "STORTIME", 0);
            if (ptr) _storlifetime = atol(ptr);

            // File size
            if ( (ptr = getenv("SRM_FILESIZE")) == NULL )
                ptr = getconfent("SRM", "FILESIZE", 0);
            if (ptr) _fileSize = (u_signed64)atoll(ptr);

            // Maximum number of returns from srmLs
            if ( (ptr = getenv("SRM_MAXLSRETURNS")) == NULL )
                ptr = getconfent("SRM", "MAXLSRETURNS", 0);
            if (ptr) _lsMaxReturns = atoi(ptr);

            // Maximum value of (count + offset) for srmLs
            if ( (ptr = getenv("SRM_MAXLSCOUNT")) == NULL )
                ptr = getconfent( "SRM", "MAXLSCOUNT", 0);
            if (ptr) _lsMaxCount = atoi(ptr);

            // SOAP message backlog
            if ( (ptr = getenv("SRM_SOAPBACKLOG")) == NULL )
                ptr = getconfent("SRM", "SOAPBACKLOG", 0);
            if (ptr) _soapBacklog = atoi(ptr);

            // SOAP message backlog
            if ( (ptr = getenv("SRM_SOAPRECVTIMEOUT")) == NULL )
                ptr = getconfent("SRM", "SOAPRECVTIMEOUT", 0);
            if (ptr) _soapRecvTime = atoi(ptr);

            // SOAP message backlog
            if ( (ptr = getenv("SRM_SOAPSENDTIMEOUT")) == NULL )
                ptr = getconfent("SRM", "SOAPSENDTIMEOUT", 0);
            if (ptr) _soapSendTime = atoi(ptr);

            // Number of threads
            if ( (ptr = getenv("SRM_SERVERTHREADS")) == NULL )
                ptr = getconfent("SRM", "SERVERTHREADS", 0);
            if (ptr) _serverThreads = atoi(ptr);

            // Max number of threads
            if ( (ptr = getenv("SRM_MAXSERVERTHREADS")) == NULL )
                ptr = getconfent("SRM", "MAXSERVERTHREADS", 0);
            if (ptr) {
                unsigned mThreads = atoi(ptr);
                if ( mThreads < _serverThreads ) {
                  mThreads = _serverThreads;
                }
                _maxServerThreads = mThreads;
            }

            // Number of Castor threads
            if ( (ptr = getenv("SRM_CASTORTHREADS")) == NULL )
                ptr = getconfent("SRM", "CASTORTHREADS", 0);
            if (ptr) {
                unsigned cThreads = atoi(ptr);
                if ( cThreads <= _serverThreads ) {
                    setMaxCastorThreads(cThreads);
                }
            }

            // Supported protocols
            if ( (ptr = getenv("SRM_PROTOCOLS")) == NULL )
                ptr = getconfent("SRM", "PROTOCOLS", 1);
            if ( ptr ) {
                char *q;
                _supportedProtocols.clear();
                char *last;
                for ( q = strtok_r(ptr," \t", &last);q != NULL; q = strtok_r(NULL," \t", &last)) {
                    _supportedProtocols.push_back(q);
                }
            }

            // Grid FTP port number
            if ( (ptr = getenv("SRM_GSIFTPPORT")) == NULL )
                ptr = getconfent("SRM", "GSIFTPPORT", 1);
            if (ptr) _gsiftpPort = atoi(ptr);

            // Domain name
            if ( (ptr = getenv("SRM_DOMAIN_NAME")) == NULL )
                ptr = getconfent("SRM", "DOMAIN_NAME", 0);
            if (ptr) {
                _domainName = ptr;
            }
            else {
                castor::exception::Exception x;
                x.getMessage() << "Mandatory parameter DOMAIN_NAME not set in config file. This should be set to the domain of the disk servers" << std::endl;
                throw x;
            }

            // GSI TURL prefix
            if ( (ptr = getenv("SRM_GSITURL")) == NULL )
                ptr = getconfent("SRM", "GSITURL", 0);
            if (ptr) _gsiTURL = ptr;

            // RFIO TURL prefix
            if ( (ptr = getenv("SRM_RFIOTURL")) == NULL )
                ptr = getconfent("SRM", "RFIOTURL", 0);
            if (ptr) _rfioTURL = ptr;

            /*** SRM Daemon parameters ***/
            if ( (ptr = getenv("SRMD_STAGETHREADS")) == NULL )
                ptr = getconfent("SRMD", "STAGETHREADS", 0);
            if (ptr) setNbReqThreads(atoi(ptr));

            if ( (ptr = getenv("SRMD_TURLTHREADS")) == NULL )
                ptr = getconfent("SRMD", "TURLTHREADS", 0);
            if (ptr) setNbTurlThreads(atoi(ptr));

            if ( (ptr = getenv("SRMD_POLLTHREADS")) == NULL )
                ptr = getconfent("SRMD", "POLLTHREADS", 0);
            if (ptr) setNbPollThreads(atoi(ptr));

            if ( (ptr = getenv("SRMD_COPYTHREADS")) == NULL )
                ptr = getconfent("SRMD", "COPYTHREADS", 0);
            if (ptr) setNbCopyThreads(atoi(ptr));

            if ( (ptr = getenv("SRMD_CALLBACKPORT")) == NULL )
                ptr = getconfent("SRMD", "CALLBACKPORT", 0);
            if (ptr) setBECallbackPort(atoi(ptr));

            if ( (ptr = getenv("SRMD_INITIALPOLLINTERVAL")) == NULL )
                ptr = getconfent("SRMD", "INITIALPOLLINTERVAL", 0);
            if (ptr) setInitialPollInterval(atoi(ptr));

            if ( (ptr = getenv("SRMD_GSIFTPCMD")) == NULL )
                ptr = getconfent("SRMD", "GSIFTPCMD", 1);
            if ( ptr ) _gsiFtpCmd = ptr;

            if ( (ptr = getenv("SRMD_RFIOCPCMD")) == NULL )
                ptr = getconfent("SRMD", "RFIOCPCMD", 0);
            if ( ptr ) _rfioCpCmd = ptr;

            if ( (ptr = getenv("SRMD_NOTIFYHOST")) == NULL )
                ptr = getconfent("SRMD", "NOTIFYHOST", 0);
            if ( ptr ) _notifyHost = ptr;
            if (_notifyHost.length() == 0 ) {
                castor::exception::Exception x;
                x.getMessage() << "Mandatory parameter NOTIFYHOST missing from config file.  This should be the machine running the srm2 daemon";
                throw x;
            }

            if ( (ptr = getenv("SRMD_REQNOTIFYPORT")) == NULL )
                ptr = getconfent("SRMD", "REQNOTIFYPORT", 0);
            if ( ptr ) _reqNotifyPort = atoi(ptr);

            if ( (ptr = getenv("CNS_HOST")) == NULL )
                ptr = getconfent("CNS", "HOST", 0);
            if ( ptr ) _nsHost = ptr;

            _isParsed = true;
            _readCallingSetter = false;
        }
        pthread_mutex_unlock(&_mutex);
    }
}

