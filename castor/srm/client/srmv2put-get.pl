#!/usr/bin/perl -w

use strict;

use POSIX qw(strftime);                                                         
use Time::Local;                                                                
use File::Basename;                                                             
use File::Find;                                                                 
my $ProgramName=basename "$0";  # to get the directory: dirname "$0";           

### User variables ###################################################
my $target = &warn_default("SRM2_HOST", "lxb1389.cern.ch");
my $baseDpm = &warn_default("SRM2_BASE", "/castor/cern.ch/user/s/shah/tps_test");
my $srmPort = &warn_default("SRM2_PORT", "8439");
my $baseSrm = "srm://${target}:${srmPort}/$baseDpm";
my $gftp = "/opt/globus/bin/globus-url-copy";

### Variables ########################################################
my $date = `date '+%Y%m%d-%H%M%S'`; chomp($date);
my $u_token = $ENV{'USER'} . $date;
my $file = "${baseSrm}/$date";

my $fflag = 1;
my $struct;
my $r_token;
my $f_token;
my $g_token;
my $TURL;
my $short;
my $lTURL;
my $status;
my $tstatus;
my $sTURL;
my $findex;
my $command;
my @res;
my $dss;
$struct->{$file}->{input} = "/etc/group";
($struct->{$file}->{size}, $dss) = split /\s+/, `ls -s $struct->{$file}->{input} | awk '{print \$1}'`;


### Functions ########################################################
sub warn_default {
  my $var = shift;
  my $var_def = shift;

  if(defined $ENV{$var}) {
    return $ENV{$var};
  } else {
    warn "Using default value $var_def for variable $var!\n";
  }

  return $var_def;
}

sub XtractGoodTurl {
    my $TURL = shift;
    my $sTURL;

    print "=======> $TURL\n";
    $TURL =~ s/\.gridpp\.rl\.ac\.uk//; # dMan hack (rfcp only works locally)

    	if ( $TURL =~ /rfio/ ) {  
	    $TURL =~ m[(.*?)://.*?/(.*)];
	    $TURL =~ m[(.*?)://(.*)];
	    $sTURL = $2;
	    $sTURL =~ s;//;:/; ;
	} else {
	    $TURL =~ m[(.*?://.*?)(/.*?:)(/.*)];
	    $sTURL = $1 . $3;

	    $sTURL = $TURL;  ###  For the NEW FTPD 23/05/05

	}
    print " Xtract\n INPUT: $TURL\n OUTPUT: $sTURL\n";
    return $sTURL;
}


sub srm2_testPut()
{
  my $command = "./srm2_testPut $u_token 1 $file 1000 2 $struct->{$file}->{size}";

  print STDERR "$command\n";
  @res = `$command 2>&1`;

  die "$!\n" if ($? ne 0);
  
  foreach ( @res ) {
      print "L: $_";
      my @m = split;
      if ( /r_token / ) { $r_token = $m[3]; }
#      if ( /TURL =/ ) { $lTURL = $_; $tstatus = $m[2]; $TURL = $m[5]; }
      if ( /state\[(\d+?)\]/ ) { 
  	$findex = $1;
  	$lTURL = $_; 
  	chop($m[2]);
  	$tstatus = $m[2]; 
  	##NEW$tstatus = $RStatusCode{$m[2]}; 
  	$TURL = $m[5]; 
  	##PREV$TURL =~ m[(.*?)://(.*)];
  	$TURL =~ m[(.*?)://.*?/(.*)];
  	$sTURL = $2;

	$struct->{$file}->{TURL} = $TURL;
	$struct->{$file}->{sTURL} = $sTURL;
##	$struct->{$lfile}->{TURL} = $TURL;
	$struct->{$file}->{r_token} = $r_token;
      }
      ##PREVif ( /request state/ ) { $status = ( "Done" eq $m[2] ) ? "[OK]" : "[FAILED]" ; }
      if ( /request state/ ) { $status = ( "26" eq $m[2] ) ? "[OK]" : "[FAILED]" ; }
  }
  if ( $status ne "[OK]" || $tstatus ne "23" ) {
      print " Put FAILED: $lTURL >$status< >$tstatus<\n";
      exit 1; 
  }
}

sub srm2_testPutRFIO()
{
  system("which rfcp");
  
  foreach my $kfile ( sort keys %{$struct} ) {
    undef $status;
    @res = ();

    my $lTURL = XtractGoodTurl($struct->{$kfile}->{TURL});
    $command = "rfcp $struct->{$kfile}->{input} $lTURL";
    @res = `$command 2>&1`;
    my $exitFlag = 0;
    foreach ( @res ) {
        print " RFCP_TRACE $_";
        if ( $ENV{RFIO_TRACE} ) {
    #p	print " RFIO_TRACE $_";
        }
        if ( /bytes/ ) { $status = "[OK]"; }
        if ( /Command exited/ ) { $exitFlag = 1; }
    }
    if ( $exitFlag && $ENV{RFIO_TRACE} ) {
        foreach ( @res ) {
          print " RFIO_FAIL $_";
        }
    }
    
    if ( $status ne "[OK]" ) { exit 1; }
  }
}

sub srm2_testPutGFTP()
{
  system("which rfcp");
  
  foreach my $kfile ( sort keys %{$struct} ) {
    undef $status;
    @res = ();

    my $lTURL = XtractGoodTurl($struct->{$kfile}->{TURL});
    $command = "rfcp $struct->{$kfile}->{input} $lTURL";
    @res = `$command 2>&1`;
    my $exitFlag = 0;
    foreach ( @res ) {
        print " RFCP_TRACE $_";
        if ( $ENV{RFIO_TRACE} ) {
    #p	print " RFIO_TRACE $_";
        }
        if ( /bytes/ ) { $status = "[OK]"; }
        if ( /Command exited/ ) { $exitFlag = 1; }
    }
    if ( $exitFlag && $ENV{RFIO_TRACE} ) {
        foreach ( @res ) {
          print " RFIO_FAIL $_";
        }
    }
    
    if ( $status ne "[OK]" ) { exit 1; }
  }
}

sub srm2_testPutDone()
{
  undef $status;
  my $SURL;
  @res = ();
  $command = "./srm2_testPutDone $struct->{$file}->{r_token} $file";
  @res = `$command 2>&1`;

  print "@res\n";
  foreach ( @res ) {
      print "D: $_";
      my @m = split;
      if ( /SURL =/ ) { $SURL = $m[5]; }
      if ( /request state/ ) { $status = ( "0" eq $m[2] ) ? "[OK]" : "[FAILED]" ; }
  }
  if ( $status ne "[OK]" ) { exit 1; }
  
  if ( $SURL ne $file ) { 
      print " Mismatch: $SURL $file \n";
      exit 1;
  }
}

sub srm2_testGetRFIO()
{
  my $fTURL;

  $TURL = $struct->{$file}->{TURL};
  undef $status;
  @res = ();
  $command = "./srm2_testGet rfio $u_token $file";

  @res = `$command 2>&1`;

  foreach ( @res ) {
      print "GF: $_";
      my @m = split;
      if ( /r_token / ) { $f_token = $m[3]; }
      if ( /TURL =/ ) { $fTURL = $m[5]; }
      if ( /request state/ ) { $status = ( "26" eq $m[2] ) ? "[OK]" : "[FAILED]" ; }
  }
  if ( $status ne "[OK]" ) { exit 1; }
  
  if ( $TURL ne $fTURL ) { 
      print " Mismatch: $TURL $fTURL \n";
      exit 1;
  } else {
      print " TURL[rfio]: $fTURL \n";
  }
  $struct->{$file}->{f_token} = $f_token;
  $struct->{$file}->{fTURL} = $fTURL;

#  $fTURL =~ s|^rfio://||;
#
#  print "|||rfcp $fTURL file:///tmp/rfio|||\n";
#  system("rfcp $fTURL file:///tmp/rfio");
}

sub srm2_testGetGFTP()
{
  my $gTURL;
  undef $status;
  @res = ();
  $command = "./srm2_testGet gsiftp $u_token $file";

  @res = `$command 2>&1`;
  foreach ( @res ) {
      print "GG: $_";
      my @m = split;
      if ( /r_token / ) { $g_token = $m[3]; }
      if ( /TURL =/ ) { $gTURL = $m[5]; }
      if ( /request state/ ) { $status = ( "26" eq $m[2] ) ? "[OK]" : "[FAILED]" ; }
  }
  if ( $status ne "[OK]" ) { exit 1; }
  print " TURL[gsiftp]: $gTURL \n";
  $struct->{$file}->{g_token} = $g_token;
  $struct->{$file}->{gTURL} = $gTURL;

  system("$gftp $struct->{$file}->{gTURL} file:///tmp/gftp");
}

sub srm2_put_get()
{
  &srm2_testPut();
  &srm2_testPutRFIO();
#  &srm2_testPutGFTP();
  &srm2_testPutDone();

  &srm2_testGetRFIO();
#  &srm2_testGetGFTP();
}

&srm2_put_get();
