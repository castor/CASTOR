/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testMkdir.cpp,v 1.11 2008/11/03 11:15:05 sdewitt Exp $

#include "soapH.h"
#include "srmNsmap.h"
#include "cgsi_plugin.h"

void usage() {
    std::cout << "Usage:" << std::endl;
    std::cout << "\t srm2_testMkdir -e endPoint -s SURL (-a authorizationId) (-k key:value)" << std::endl;
    std::cout << std::endl;
    std::cout << "The key/value pairs may be repeated.  These arepresent additional storage system information" << std::endl;
    std::cout << "which the underlying storage system may choose to utilise or not." << std::endl;
    return;
}


    int
main(int argc, char *argv[])
{
    srm__srmMkdirResponse_ rep;
    srm__srmMkdirRequest req;
    struct soap* soap = soap_new();
    std::string srm_endpoint;

    soap_cgsi_init(soap, CGSI_OPT_DISABLE_NAME_CHECK|CGSI_OPT_DELEG_FLAG);
    soap_set_namespaces(soap, namespaces);

    req.authorizationID = NULL;
    req.storageSystemInfo = NULL;

    // Parse arguments
    int keyCount=0;
    int valCount=0;
    int carg    =1;
    while (carg < argc) {
        if ( std::string(argv[carg]) == "-e" ) {
            srm_endpoint.assign(argv[++carg]);
        }
        else if ( std::string(argv[carg]) == "-s" ) {
            req.SURL.assign(argv[++carg]);
            break;
        }
        else if ( std::string(argv[carg]) == "-a" ) {
            req.authorizationID = soap_new_std__string(soap, -1);
            req.authorizationID->assign(argv[++carg]);
        }
        else if ( std::string(argv[carg]) == "-k" ) {
            std::string arg(argv[++carg]);
            std::string separator(":");
            size_t sepP = arg.find(separator);
            if ( sepP == std::string::npos || 
                    sepP == arg.length() ||
                    sepP == 0) {
                std::cout << "Invalid key/value pair" << std::endl;
                usage();
                exit(1);
            }
            std::string key = arg.substr(0, arg.find(separator));
            std::string val = arg.substr(arg.find(separator)+1, arg.length());
            if (!req.storageSystemInfo) req.storageSystemInfo = soap_new_srm__ArrayOfTExtraInfo(soap, -1);
            srm__TExtraInfo *xinfo = soap_new_srm__TExtraInfo(soap, -1);
            xinfo->key.assign(key);
            xinfo->value = 0;
            if ( val.length() > 0 ) {
                xinfo->value = soap_new_std__string(soap, -1);
                xinfo->value->assign(val);
            }
            req.storageSystemInfo->extraInfoArray.push_back(xinfo);
        }
        else {
            usage();
            exit(1);
        }
        carg++;
    }

    // Make sure everything looks OK
    if ( srm_endpoint.length() == 0 ) {
        std::cerr << "No SRM end point specified:";
        usage();
        exit(1);
    }
    if ( req.SURL.length() == 0 ) {
        std::cerr << "No SURL specified:";
        usage();
        exit(1);
    }
    if ( keyCount != valCount ) {
        std::cerr << "Key/Value pairs do not match:";
        usage();
        exit(1);
    }

    // Convert enbd point to use httpg
    srm_endpoint.replace(0, srm_endpoint.find("://"), "httpg");
    if ( srm_endpoint.find("/srm/managerv2") == std::string::npos ) {
        srm_endpoint.append("/srm/managerv2");
    }


    if (soap_call_srm__srmMkdir (soap, srm_endpoint.c_str(), "Mkdir",
                &req, rep)) {
        soap_print_fault (soap, stderr);
        soap_print_fault_location (soap, stderr);
        soap_end (soap);
        exit (1);
    }

    std::cout << "Request Status: " << soap_srm__TStatusCode2s(soap, rep.srmMkdirResponse->returnStatus->statusCode) << std::endl;
    if (rep.srmMkdirResponse->returnStatus->explanation)
        std::cout << "Explanation: " << rep.srmMkdirResponse->returnStatus->explanation->c_str() << std::endl;

    soap_end (soap);
    exit (0);
}
