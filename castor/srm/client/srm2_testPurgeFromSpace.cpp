/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testPurgeFromSpace.cpp,v 1.1 2008/06/13 14:24:25 sdewitt Exp $

#include "soapH.h"
#include "srmNsmap.h"

#include "cgsi_plugin.h"

#include <sstream>

using namespace std;

void usage () {
    std::cout << "Usage:" << std::endl;
    std::cout << "srm2_testPurgeFromSpace" << std::endl;
    std::cout << "\t -e endPoint         End point to contact SRM (mandatory)" << std::endl;
    std::cout << "\t -s SURL             One or more SURLs to purge (mandatory)" << std::endl;
    std::cout << "\t -x spaceToken       The space token to purge from. (mandatory)" << std::endl;
    std::cout << "\t -a authorisationID  Any authorisation ID required" << std::endl;
    std::cout << "\t -k key:value        Key/value pair for additional info" << std::endl;
    return;
}

    int
main(int argc, char *argv[])
{
    srm__srmPurgeFromSpaceResponse_ rep;
    srm__srmPurgeFromSpaceRequest req;
    struct soap* soap = soap_new();

    soap_cgsi_init(soap, CGSI_OPT_DISABLE_NAME_CHECK|CGSI_OPT_DELEG_FLAG);
    soap_set_namespaces(soap, namespaces);

    // Init request
    req.authorizationID   = NULL;
    req.arrayOfSURLs      = soap_new_srm__ArrayOfAnyURI(soap, -1);
    req.storageSystemInfo = NULL;

    std::string endPoint;
    // Parse arguments
    int carg = 1;
    while ( carg < argc ) {
        if (strcmp (argv[carg], "-e") == 0 ) {
            endPoint.assign(argv[++carg]);
        }
        else if (strcmp (argv[carg], "-s") == 0 ) {
            std::string thisSURL(argv[++carg]);
            req.arrayOfSURLs->urlArray.push_back(thisSURL);
        }
        else if (strcmp(argv[carg],"-a") == 0) {
            if (!req.authorizationID) req.authorizationID = soap_new_std__string(soap, -1);
            req.authorizationID->assign(argv[++carg]);
        }
        else if (strcmp (argv[carg],"-x") == 0 ) {
            req.spaceToken.assign(argv[++carg]);
        }
        else if ( strcmp(argv[carg], "-k") == 0 ) {
            std::string arg(argv[++carg]);
            std::string separator(":");
            size_t sepP = arg.find(separator);
            if ( sepP == std::string::npos || 
                 sepP == arg.length() ||
                 sepP == 0) {
                std::cout << "Invalid key/value pair" << std::endl;
                usage();
                exit(1);
            }
            std::string key = arg.substr(0, arg.find(separator));
            std::string val = arg.substr(arg.find(separator)+1, arg.length());
            if (!req.storageSystemInfo) req.storageSystemInfo = soap_new_srm__ArrayOfTExtraInfo(soap, -1);
            srm__TExtraInfo *xinfo = soap_new_srm__TExtraInfo(soap, -1);
            xinfo->key.assign(key);
            xinfo->value = 0;
            if ( val.length() > 0 ) {
                xinfo->value = soap_new_std__string(soap, -1);
                xinfo->value->assign(val);
            }
            req.storageSystemInfo->extraInfoArray.push_back(xinfo);
        }
        else {
            std::cout << "Unknown option: " << argv[carg] << std::endl;
            usage();
            exit(1);
        }
        carg++;
    }

    if ( endPoint.length() == 0 || req.arrayOfSURLs->urlArray.size() == 0 || req.spaceToken.length() == 0 ) {
        usage();
        exit (1);
    }

    if (soap_call_srm__srmPurgeFromSpace (soap, endPoint.c_str(), "PurgeFromSpace",
                &req, rep)) {
        soap_print_fault (soap, stderr);
        soap_print_fault_location (soap, stderr);
        soap_end (soap);
        exit (1);
    }
    srm__TReturnStatus* reqstatp = rep.srmPurgeFromSpaceResponse->returnStatus;

    std::cout << "Request Status: " << soap_srm__TStatusCode2s(soap, reqstatp->statusCode) <<std::endl;
    if (reqstatp->explanation) std::cout << "Expanation    : " << reqstatp->explanation->c_str()  << std::endl;
    
    if ( rep.srmPurgeFromSpaceResponse->arrayOfFileStatuses != NULL ) {
        std::vector<srm__TSURLReturnStatus*> statusArray = rep.srmPurgeFromSpaceResponse->arrayOfFileStatuses->statusArray;
        for (unsigned i=0; i<statusArray.size(); i++) {
            std::cout << "\tFile: " << statusArray[i]->surl << std::endl;
            std::cout << "\t\tstatusCode: " << soap_srm__TStatusCode2s(soap, statusArray[i]->status->statusCode) << std::endl;
            if ( statusArray[i]->status->explanation ) 
                std::cout << "\t\texplanation: " << statusArray[i]->status->explanation->c_str() << std::endl;
        }
    }
    soap_end (soap);
    exit (0);
}

void printDetails( std::vector<srm__TMetaDataPathDetail*> details, std::string indent) {
    for ( unsigned i=0; i<details.size(); i++ ) {
        std::cout << indent << "Path: " << details[i]->path << std::endl;
        std::cout << indent << "Status: " << details[i]->status->statusCode << std::endl;
        if ( details[i]->status->explanation) 
            std::cout << indent << "Explanation : " << details[i]->status->explanation->c_str() << std::endl;
        if ( details[i]->size ) {
            std::cout << indent << indent << "size: " << *(details[i]->size) << std::endl;
        }
        if ( details[i]->createdAtTime ) {
            std::cout << indent << indent << "ctime=" << *(details[i]->createdAtTime) << std::endl;
        }
        if ( details[i]->lastModificationTime) {
            std::cout << indent << indent << "mtime=" << *(details[i]->lastModificationTime) << std::endl;
        }
        if ( details[i]->fileStorageType ) {
            std::cout << indent << indent << "fileStorageType=" << *(details[i]->fileStorageType) << std::endl;
        }
        if ( details[i]->retentionPolicyInfo ) {
            std::cout << indent << indent << "retentionPolicy: " << details[i]->retentionPolicyInfo->retentionPolicy << std::endl;
            if ( details[i]->retentionPolicyInfo->accessLatency ) 
                std::cout << indent << indent << "accessLatency: " << *(details[i]->retentionPolicyInfo->accessLatency) << std::endl;
        }
        if ( details[i]->fileLocality ) std::cout << indent << indent << "fileLocality=" << *(details[i]->fileLocality) << std::endl;
        if ( details[i]->arrayOfSpaceTokens ) {
            std::cout << indent << indent << "SpaceToken: ";
            for ( unsigned token = 0; token<details[i]->arrayOfSpaceTokens->stringArray.size(); token++) {
                std::cout << details[i]->arrayOfSpaceTokens->stringArray[token] << ", ";
            }
            std::cout << std::endl;
        }
        if ( details[i]->type ) std::cout << indent << indent << "fileType=" << *(details[i]->type) << std::endl;
        if ( details[i]->ownerPermission ) {
            std::cout << indent << indent << "ownerId =" << details[i]->ownerPermission->userID << std::endl;
            std::cout << indent << indent << "ownerPermission=" << details[i]->ownerPermission->mode << std::endl;
        }
        if ( details[i]->groupPermission ) std::cout << indent << indent << "groupPermission=" << details[i]->groupPermission->mode << std::endl;
        if ( details[i]->otherPermission ) std::cout << indent << indent << "other: perm=" << *(details[i]->otherPermission) << std::endl;
        if ( details[i]->checkSumType ) std::cout << indent << indent << "checkSumType=" << details[i]->checkSumType << std::endl;
        if ( details[i]->checkSumValue ) std::cout << indent << indent << "checkSumValue=" << details[i]->checkSumValue << std::endl;
        if ( details[i]->arrayOfSubPaths ) {
            std::vector<srm__TMetaDataPathDetail*> subdetails = details[i]->arrayOfSubPaths->pathDetailArray;
            printDetails( subdetails, indent + "  " );
        }
    }
}
