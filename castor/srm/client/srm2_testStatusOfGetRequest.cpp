/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testStatusOfGetRequest.cpp,v 1.3 2008/01/25 17:06:30 sdewitt Exp $

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef DG_DIAGNOSE
#include "diagnose/dg.h"
#endif

#include "soapH.h"
#include "srmNsmap.h"
#define DEFPOLLINT 10

#define SRM_EP_PATH "/v2_1_1/srm"
#include "cgsi_plugin.h"

#include "parsesurl.ic"
#include "soapcallns1.ic"

#include <fstream>
#include <map>

using namespace std;

void usage() {
    cerr << "Usage: srm2_testPrepareToGet" << endl;
    std::cerr << "\t -e    SRM endPoint (mandatory)" << std::endl;
    std::cerr << "\t -r    Request Token from previous PreapreToGet (mandatory)" << std::endl;
    std::cerr << "\t -a    authorization ID" << std::endl;
    std::cerr << "\t -s    0 or more SURLs associated with the request token" << std::endl;
    return;
}


void CheckStatus (
        struct soap *soap,
        std::string endpoint,
        srm__srmStatusOfGetRequestRequest req
        ) {

    srm__srmStatusOfGetRequestResponse_ resp;


    if ( soap_call_srm__srmStatusOfGetRequest(soap, endpoint.c_str(), "srmStatusOfGetRequest", &req, resp) ) {
        soap_print_fault (soap, stderr);
        soap_print_fault_location (soap, stderr);
        return;
    }

    // Output results
    cout << "StatusOfGet for request " << req.requestToken << ":" << endl;
    cout << "StatusCode = " << soap_srm__TStatusCode2s( soap, resp.srmStatusOfGetRequestResponse->returnStatus->statusCode) << endl;
    if ( resp.srmStatusOfGetRequestResponse->returnStatus->explanation ) {
        cout << "Explanation = " << resp.srmStatusOfGetRequestResponse->returnStatus->explanation->c_str() << endl;
    }
    if ( resp.srmStatusOfGetRequestResponse->remainingTotalRequestTime ) {
        cout << "remainingTotalRequestTime = " << *(resp.srmStatusOfGetRequestResponse->remainingTotalRequestTime) << endl;
    }

    // Loop over all subrequest...
    if ( resp.srmStatusOfGetRequestResponse->arrayOfFileStatuses ) {
        std::vector<srm__TGetRequestFileStatus*> stati =resp.srmStatusOfGetRequestResponse->arrayOfFileStatuses->statusArray;
        for ( unsigned int i=0; i<stati.size(); i++ ) {
            srm__TGetRequestFileStatus *fileStatus = stati[i];
            std::cout << "\tsourceSURL = " << fileStatus->sourceSURL.c_str() << std::endl;
            std::cout << "\tstatusCode = " << soap_srm__TStatusCode2s(soap, fileStatus->status->statusCode) << std::endl;
            if ( fileStatus->status->explanation) std::cout << "\texplanation = " << fileStatus->status->explanation->c_str() << std::endl;
            if ( fileStatus->estimatedWaitTime)   std::cout << "\testimatedWaitTime = " << *(fileStatus->estimatedWaitTime) << std::endl;
            if ( fileStatus->fileSize )           std::cout << "\tfileSize = " << *(fileStatus->fileSize) << std::endl;
            if ( fileStatus->remainingPinTime )   std::cout << "\tremainingPinTime = " << *(fileStatus->remainingPinTime) << std::endl;
            if ( fileStatus->transferURL )        std::cout << "\ttransferURL = " << fileStatus->transferURL->c_str() << std::endl;
            if ( fileStatus->transferProtocolInfo && fileStatus->transferProtocolInfo->extraInfoArray.size() > 0 ) {
                cout << "\ttransferProtocolInfo:" << endl;
                for ( unsigned int i=0; i<fileStatus->transferProtocolInfo->extraInfoArray.size() ; i++ ) {
                    std::cout << "\t\t" << fileStatus->transferProtocolInfo->extraInfoArray[i]->key << ": ";
                    if (fileStatus->transferProtocolInfo->extraInfoArray[i]->value) {
                        std::cout << fileStatus->transferProtocolInfo->extraInfoArray[i]->value << std::endl;
                    }
                    else {
                        std::cout << std::endl;
                    }
                }
            }
        }
    }
}

    int
main(int argc, char *argv[])
{
    struct soap* soap = soap_new();

    soap_cgsi_init(soap, CGSI_OPT_DISABLE_NAME_CHECK|CGSI_OPT_DELEG_FLAG);
    soap_set_namespaces(soap, namespaces);

    srm__srmStatusOfGetRequestRequest request;
    request.authorizationID = 0;
    request.arrayOfSourceSURLs = 0;

    std::string endPoint;

    int carg = 1;
    while (carg < argc-1) {
        if ( strcmp( argv[carg], "-r") == 0 ) {
            request.requestToken.assign(argv[++carg]);
        }
        else if ( strcmp( argv[carg], "-a" ) == 0 ) {
            request.authorizationID = soap_new_std__string(soap, -1);
            request.authorizationID->assign(argv[++carg]);
        }
        else if ( strcmp( argv[carg], "-s" ) == 0 ) {
            if ( !request.arrayOfSourceSURLs ) request.arrayOfSourceSURLs = soap_new_srm__ArrayOfAnyURI(soap, -1);
            std::string  thisSURL = argv[++carg];
            request.arrayOfSourceSURLs->urlArray.push_back(thisSURL);
        }
        else if ( strcmp( argv[carg], "-e") == 0 ) {
            endPoint.assign(argv[++carg]);
        }
        else {
            usage();
            exit (1);
        }
        carg++;
    }

    if (endPoint.length() == 0 || request.requestToken.length() == 0 ) {
        usage();
        exit(1);
    }

    CheckStatus( 
            soap,
            endPoint,
            request
            );
    exit (0);
}
