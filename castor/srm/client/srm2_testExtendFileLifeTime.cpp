/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testExtendFileLifeTime.cpp,v 1.8 2007/02/20 15:44:14 sdewitt Exp $

#include "soapH.h"
#include "srmNsmap.h"

#include "cgsi_plugin.h"


void usage() {
    std::cout << "srm2_testExtendFileLifeTime -e endPoint [-s SURL]+ (-u authorizationID) (-r requestToken) (-f lifetime) (-p pinLifeTime) (-h)" << std::endl;
}

    int
main(int argc, char** argv)
{
    srm__srmExtendFileLifeTimeResponse_ rep;
    srm__srmExtendFileLifeTimeRequest req;
    struct soap* soap = soap_new();
    std::string endPoint;


    soap_cgsi_init(soap, CGSI_OPT_DISABLE_NAME_CHECK|CGSI_OPT_DELEG_FLAG);
    soap_set_namespaces(soap, namespaces);

    // Allocate request memory
    req.authorizationID = NULL;
    req.requestToken    = NULL;
    req.arrayOfSURLs    = soap_new_srm__ArrayOfAnyURI(soap, -1);
    req.newFileLifeTime = NULL;
    req.newPinLifeTime  = NULL;

    // Fill request structure
    int carg = 1;
    while (carg < argc) {
        switch (argv[carg][1]) {
            case 'e':
                endPoint.assign(argv[++carg]);
                break;
            case 's':
                req.arrayOfSURLs->urlArray.push_back(argv[++carg]);
                break;
            case 'u':
                if ( req.authorizationID == NULL ) {
                    req.authorizationID = soap_new_std__string(soap, -1);
                }
                req.authorizationID->assign(argv[++carg]);
                break;
            case 'r':
                if ( req.requestToken == NULL ) {
                    req.requestToken = soap_new_std__string(soap, -1);
                }
                req.requestToken->assign(argv[++carg]);
                break;
            case 'f':
                if ( req.newFileLifeTime == NULL ) {
                    req.newFileLifeTime = (int*)soap_malloc(soap, sizeof(int));
                }
                *(req.newFileLifeTime) = atoi(argv[++carg]);
                break;
            case 'p':
                if ( req.newPinLifeTime == NULL ) {
                    req.newPinLifeTime = (int*)soap_malloc(soap, sizeof(int));
                }
                *(req.newPinLifeTime) = atoi(argv[++carg]);
                break;
            default:
                usage();
                exit(1);
        }
        carg++;
    }
    if (endPoint.length() == 0 ) {
        std::cerr << "No end point supplied." << std::endl;
        usage();
        exit (1);
    }

    if (soap_call_srm__srmExtendFileLifeTime (soap, const_cast<char*>(endPoint.c_str()), "ExtendFileLifeTime",
                &req, rep)) {
        soap_print_fault (soap, stderr);
        soap_print_fault_location (soap, stderr);
        soap_end (soap);
        exit (1);
    }

    srm__TReturnStatus *reqstatp = 
        rep.srmExtendFileLifeTimeResponse->returnStatus;

    std::cout << "Request Status: " << soap_srm__TStatusCode2s(soap, reqstatp->statusCode) << std::endl;
    if ( reqstatp->explanation )
        std::cout << reqstatp->explanation->c_str() << std::endl;

    if ( rep.srmExtendFileLifeTimeResponse->arrayOfFileStatuses != NULL ) {
        std::vector<srm__TSURLLifetimeReturnStatus*> v = 
            rep.srmExtendFileLifeTimeResponse->arrayOfFileStatuses->statusArray;
        for (unsigned i=0; i < v.size(); i++ ) {
            std::cout << "SURL: " << v[i]->surl << std::endl;
            std::cout << "\tStatusCode: " << soap_srm__TStatusCode2s(soap, v[i]->status->statusCode) << std::endl;
            std::cout << "\tExplanation: " << v[i]->status->explanation->c_str() << std::endl;
            if ( v[i]->fileLifetime ) std::cout << "\tFile Lifetime: " << *(v[i]->fileLifetime) << std:: endl;
            if ( v[i]->pinLifetime )  std::cout << "\tPin Lifetime: "  << *(v[i]->pinLifetime)  << std::endl;
        }
    }

    soap_end (soap);
    exit (0);
}
