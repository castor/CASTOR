/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testGetRequestSummary.cpp,v 1.8 2007/02/28 10:35:45 sdewitt Exp $


#include "soapH.h"
#include "srmNsmap.h"
#include "cgsi_plugin.h"


void usage() {
    std::cout << "Usage:" << std::endl;
    std::cout << "\tsrm2_testGetRequestSummary -e endPoint [-t token]+ (-a authorizationId) (-h)" << std::endl;
}

    int
main(int argc, char *argv[])
{
    srm__srmGetRequestSummaryResponse_ rep;
    srm__srmGetRequestSummaryRequest req;
    struct soap* soap = soap_new();
    std::string endPoint;

    req.authorizationID = NULL;
    req.arrayOfRequestTokens = NULL;

    int cArg=1;
    while (cArg < argc) {
        if (std::string(argv[cArg]) == "-h") {
            usage();
            exit (0);
        }
        else if (std::string(argv[cArg]) == "-a") {
            if (!req.authorizationID) 
                req.authorizationID = soap_new_std__string(soap, -1);
            req.authorizationID->assign(argv[++cArg]);
        }
        else if (std::string(argv[cArg]) == "-t") {
            if (!req.arrayOfRequestTokens)
                req.arrayOfRequestTokens = soap_new_srm__ArrayOfString(soap, -1);
            req.arrayOfRequestTokens->stringArray.push_back(argv[++cArg]);
        }
        else if (std::string(argv[cArg]) == "-e") {
            endPoint.assign(argv[++cArg]);
        }
        else {
            std::cerr << "Invalid argument: " << argv[cArg] << std::endl;
            usage();
            exit(1);
        }
        cArg++;
    }

    if (endPoint.length() == 0) {
        std::cerr << "No end point supplied." << std::endl;
        usage();
        exit(1);
    }

    soap_cgsi_init(soap, CGSI_OPT_DISABLE_NAME_CHECK|CGSI_OPT_DELEG_FLAG);
    soap_set_namespaces(soap, namespaces);

    if (soap_call_srm__srmGetRequestSummary (soap, const_cast<char*>(endPoint.c_str()), "GetRequestSummary",
                &req, rep)) {
        soap_print_fault (soap, stderr);
        soap_print_fault_location (soap, stderr);
        soap_end (soap);
        exit (1);
    }

    srm__TReturnStatus* status = rep.srmGetRequestSummaryResponse->returnStatus;
    srm__ArrayOfTRequestSummary* summaries = 
        rep.srmGetRequestSummaryResponse->arrayOfRequestSummaries;


    std::cout << "Request Status: " << soap_srm__TStatusCode2s(soap, status->statusCode) << std::endl;
    if ( status->explanation ) 
        std::cout << "Explanation: " << status->explanation->c_str() << std::endl;

    if (summaries) {
        std::cout << "======= BEGIN SUMMARY ========" << std::endl;
        for (unsigned i=0; i< summaries->summaryArray.size(); i++) {
            status = summaries->summaryArray[i]->status;
            std::cout << "Token: " << summaries->summaryArray[i]->requestToken << std::endl;
            if (status) {
                std::cout << "\tStatus: " << soap_srm__TStatusCode2s(soap, status->statusCode) << std::endl;
                if (status->explanation)
                    std::cout << "\tExplanation: " << status->explanation->c_str() << std::endl;
            }
            if (summaries->summaryArray[i]->requestType)
                std::cout << "\tRequest Type: " << *(summaries->summaryArray[i]->requestType) << std::endl;
            if (summaries->summaryArray[i]->totalNumFilesInRequest)
                std::cout << "\tTotal Files: " << *(summaries->summaryArray[i]->totalNumFilesInRequest) << std::endl;
            if (summaries->summaryArray[i]->numOfCompletedFiles)
                std::cout << "\tCompleted Files: " << *(summaries->summaryArray[i]->numOfCompletedFiles) << std::endl;
            if (summaries->summaryArray[i]->numOfWaitingFiles)
                std::cout << "\tPending Files: " << *(summaries->summaryArray[i]->numOfWaitingFiles) << std::endl;
            if (summaries->summaryArray[i]->numOfFailedFiles)
                std::cout << "\tFailed Files: " << *(summaries->summaryArray[i]->numOfFailedFiles) << std::endl;
        }

    }
    soap_end (soap);
    exit (0);
}

