#!/bin/sh
i=0;
endPoint=$1
testfile=$2
cycles=$3
testdir=test_$testfile
dt=`date --utc -Iminutes`
while [ $i -lt $cycles ]; do
        echo "./srm2_testPing srm://$endPoint"
	./srm2_testPing srm://$endPoint
        echo "./srm2_testPut -f srm://$endPoint$CASTOR_HOME/foo/$testfile -c $dt"
	./srm2_testPut -f srm://$endPoint$CASTOR_HOME/foo/$testfile -c $dt 
	exec 3<TokenHolder_$testfile.dat
	read token 0<&3
	read turl 0<&3
	3>&-
        rm TokenHolder_$testfile.dat
        if [ -z turl ]; then
                ./srm2_testRm srm://$endPoint$CASTOR_HOME/foo/$testfile
                exit
        fi
        echo "globus-url-copy file:///etc/vimrc $turl"
	globus-url-copy file:///etc/vimrc $turl 
        echo "./srm2_testPutDone $token srm://$endPoint$CASTOR_HOME/foo/$testfile" 
	./srm2_testPutDone $token srm://$endPoint$CASTOR_HOME/foo/$testfile
#        exit 0
        echo "./srm2_testGetRequestID srm://$endPoint$CASTOR_HOME $dt" 
	./srm2_testGetRequestID srm://$endPoint$CASTOR_HOME $dt
        echo "./srm2_testGetRequestSummary srm://$endPoint$CASTOR_HOME $token" 
	./srm2_testGetRequestSummary srm://$endPoint$CASTOR_HOME $token
        echo "./srm2_testLs  -d -s srm://$endPoint$CASTOR_HOME/foo/$testfile"
	./srm2_testLs  -d -s srm://$endPoint$CASTOR_HOME/foo/$testfile 
        echo "./srm2_testMkdir srm://$endPoint$CASTOR_HOME/foo/test_$testfile"
	./srm2_testMkdir srm://$endPoint$CASTOR_HOME/foo/$testdir 
        echo "./srm2_testLs  -d -n 0 -s srm://$endPoint$CASTOR_HOME/foo/$testdir"
	./srm2_testLs  -d -n 0 -s srm://$endPoint$CASTOR_HOME/foo/$testdir 
#        echo "./srm2_testCopyCycle foo srm://$endPoint$CASTOR_HOME/foo/$testfile srm://$endPoint$CASTOR_HOME/foo/test/copied"
#	./srm2_testCopyCycle foo srm://$endPoint$CASTOR_HOME/foo/$testfile srm://$endPoint$CASTOR_HOME/foo/test/copied
#        echo "./srm2_testMv srm://$endPoint$CASTOR_HOME/foo/test/copied srm://$endPoint$CASTOR_HOME/foo/test/moved"
#	./srm2_testMv srm://$endPoint$CASTOR_HOME/foo/test/copied srm://$endPoint$CASTOR_HOME/foo/test/moved
        echo "./srm2_testMv srm://$endPoint$CASTOR_HOME/foo/$testfile srm://$endPoint$CASTOR_HOME/foo/$testdir/moved"
	./srm2_testMv srm://$endPoint$CASTOR_HOME/foo/$testfile srm://$endPoint$CASTOR_HOME/foo/$testdir/moved
        echo "./srm2_testLs  -d -n 1 -s srm://$endPoint$CASTOR_HOME/foo/$testdir"
	./srm2_testLs  -d -n 1 -s srm://$endPoint$CASTOR_HOME/foo/$testdir 
        echo "./srm2_testBringOnline -f srm://$endPoint$CASTOR_HOME/foo/$testdir/moved"
        ./srm2_testBringOnline -f srm://$endPoint$CASTOR_HOME/foo/$testdir/moved
        echo "./srm2_testGet -f srm://$endPoint$CASTOR_HOME/foo/$testdir/moved"
	./srm2_testGetCycle -f srm://$endPoint$CASTOR_HOME/foo/$testdir/moved -l 1800
	exec 3<xfer.url
	read xurl 0<&3
        read getToken 0<&3
	3>&-
	globus-url-copy $xurl  file:///tmp/g-u-c-fubar
        echo "./srm2_testGet -f srm://$endPoint$CASTOR_HOME/foo/$testdir/moved -p rfio"
	./srm2_testGetCycle -f srm://$endPoint$CASTOR_HOME/foo/$testdir/moved -l 1800 -p rfio
	exec 3<xfer.url
	read xurl 0<&3
        read getToken 0<&3
	3>&-
	rfcp $xurl  /tmp/rfio-fubar
        echo "./srm2_testSetPermission -s srm://$endPoint$CASTOR_HOME/foo/$testdir/moved -t add -p atlas001"
	./srm2_testSetPermission -s srm://$endPoint$CASTOR_HOME/foo/$testdir/moved -t add -p atlas001 7
        echo "./srm2_testGetPermission -s srm://$endPoint$CASTOR_HOME/foo/$testdir/moved -u dteam001"
	./srm2_testGetPermission -s srm://$endPoint$CASTOR_HOME/foo/$testdir/moved -u dteam001
        echo "./srm2_testCheckPermission srm://$endPoint$CASTOR_HOME/foo/$testdir/moved"
	./srm2_testCheckPermission srm://$endPoint$CASTOR_HOME/foo/$testdir/moved
        echo "./srm2_testGetSpaceToken srm://$endPoint$CASTOR_HOME dteam"
	./srm2_testGetSpaceToken srm://$endPoint$CASTOR_HOME dteam
        echo "./srm2_testGetSpaceMetaData srm://$endPoint$CASTOR_HOME 2ac27ca1-5d4e-4f05-ae90-91526a4ffbcb"
	./srm2_testGetSpaceMetaData srm://$endPoint$CASTOR_HOME 2ac27ca1-5d4e-4f05-ae90-91526a4ffbcb
        echo "./srm2_testExtendFileLifeTime -s srm://$endPoint$CASTOR_HOME/foo/$testdir/moved -t $token -p 86400"
	./srm2_testExtendFileLifeTime -s srm://$endPoint$CASTOR_HOME/foo/$testdir/moved -t $getToken -p 86400
	./srm2_testRm srm://$endPoint$CASTOR_HOME/foo/$testdir/moved
	./srm2_testRmdir srm://$endPoint$CASTOR_HOME/foo/$testdir
	./srm2_testRm srm://$endPoint$CASTOR_HOME/foo/$testfile
	let i+=1
done
