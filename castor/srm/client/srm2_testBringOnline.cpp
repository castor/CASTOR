/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testBringOnline.cpp,v 1.5 2009/06/30 10:48:28 sdewitt Exp $

#include "soapH.h"
#include "srmNsmap.h"
#define DEFPOLLINT 10

#include "cgsi_plugin.h"


using namespace std;

void usage() {
    std::cerr << "Usage:" << endl;
    std::cerr << "\"srm2_testBringOnline \"" << endl;
    std::cerr << "Options are:" << std::endl;
    std::cerr << "\t -e endPoint           The end point being contacted (mandatory)" << std::endl;
    std::cerr << "\t -a authorizationID    Any authorization ID required by the server" << std::endl;
    std::cerr << "\t -s SURL               At least 1 SURL; multiple -s options are supported" << std::endl;
    std::cerr << "\t -c comment            A user defined comment which can be used to identify this request." << std::endl;
    std::cerr << "\t -r requestLifetime    The total length of time that this request should be completed within." << std::endl;
    std::cerr << "\t -l desiredLifetime    The pin lifetime once the file is available." << std::endl;
    std::cerr << "\t -x spaceToken         The spaceToken of the desired space." << std::endl;
    std::cerr << "\t -p protocol           This can be repeated to allow choice of protocols." << std::endl;
    std::cerr << "\t -n networks           This can be repeated to supply a range of client networks." << std::endl;
    std::cerr << "\t -D deferredStartTime  The number of seconds before this request should be started" << std::endl;

    std::cerr << "\t --volatile" << std::endl;
    std::cerr << "\t --durable" << std::endl;
    std::cerr << "\t --permananent" << std::endl;
    std::cerr << "\t\t Type of storage requred" << std::endl;
    std::cerr << "\t --processing-mode" << std::endl;
    std::cerr << "\t --xfer-mode" << std::endl;
    std::cerr << "\t\t Mode in which data will be accessed.  xfer mode commonly refers to bulk xfers like gridftp" << std::endl;
    std::cerr << "\t\t while processing mode is for slower write requests from jobs." << std::endl;
    std::cerr << "\t --wan "  << std::endl;
    std::cerr << "\t --lan" << std::endl;
    std::cerr << "\t\t Indicates whether the xfer is expected to be over the WAN or internal network." << std::endl;
    std::cerr << "\t --replica" << std::endl;
    std::cerr << "\t --output" << std::endl;
    std::cerr << "\t --custodial" << std::endl;
    std::cerr << "\t\t Indicates quality of retention required.  Typically replica means no back-up, while " << std::endl;
    std::cerr << "\t\t custodial implies the data is backed up un secure, recoverable media" << std::endl;
    std::cerr << "\t --online" << std::endl;
    std::cerr << "\t --nearline" << std::endl;
    std::cerr << "\t\t Indicates whether the data should remain online, or disk copies can be removed by the system" << std::endl;
    return;
}

void PrepareToBringOnline (
        std::string endPoint,
        struct soap *soap,
        srm__srmBringOnlineRequest request
        ) {
    srm__srmBringOnlineResponse_ response;

    // Now make the soap call...
    if ( soap_call_srm__srmBringOnline(soap, endPoint.c_str(), "srmBringOnline", &request, response) ) {
        soap_print_fault (soap, stderr);
        soap_print_fault_location (soap, stderr);
        return; 
    }

    // If we get here, print out the response structure
    cout << "srmBringOnlineResponse:" << endl;
    cout << "Return Status: " << soap_srm__TStatusCode2s(soap, response.srmBringOnlineResponse->returnStatus->statusCode) << endl;
    cout << "Explanation:   " << response.srmBringOnlineResponse->returnStatus->explanation->c_str() << endl;
    if ( response.srmBringOnlineResponse->requestToken ) {
        cout << "requestToken:  " << response.srmBringOnlineResponse->requestToken->c_str() << endl;
    }
    if ( response.srmBringOnlineResponse->remainingTotalRequestTime ) {
        cout << "remainingTotalRequestTime = " << *(response.srmBringOnlineResponse->remainingTotalRequestTime) << endl;
    }

    if ( response.srmBringOnlineResponse->arrayOfFileStatuses ) {
        for ( unsigned int i=0; i<response.srmBringOnlineResponse->arrayOfFileStatuses->statusArray.size(); i++ ) {
            cout << "File status [" << i << "]:" << endl;
            srm__TBringOnlineRequestFileStatus *fileStatus = response.srmBringOnlineResponse->arrayOfFileStatuses->statusArray[i];
            cout << "\tsourceSURL = " << fileStatus->sourceSURL << endl;
            cout << "\tstatusCode = " << soap_srm__TStatusCode2s(soap, fileStatus->status->statusCode) << endl;
            cout << "\texplanation = " << fileStatus->status->explanation->c_str() << endl;

            if ( fileStatus->fileSize ) {
                cout << "\tfileSize = " << *(fileStatus->fileSize) << endl;
            }
            else {
                cout << "\tfileSize = NULL" << endl;
            }

            if ( fileStatus->estimatedWaitTime ) {
                cout << "\testimatedWaitTime = " << *(fileStatus->estimatedWaitTime) << endl;
            }
            else {
                cout << "\testimatedWaitTime = NULL" << endl;
            }

            if ( fileStatus->remainingPinTime ) {
                cout << "\tremainingPinTime = " << *(fileStatus->remainingPinTime) << endl;
            }
            else {
                cout << "\tremainingPinTime = NULL" << endl;
            }
        }
    }
    else {
        cout << "No file statuses returned" << endl;
        return; 
    }
    return;

}


int
main(int argc, char *argv[])
{
    struct soap* soap = soap_new();

    soap_cgsi_init(soap, CGSI_OPT_DISABLE_NAME_CHECK|CGSI_OPT_DELEG_FLAG);
    soap_set_namespaces(soap, namespaces);

    srm__srmBringOnlineRequest request;

    // Initialise request
    request.authorizationID               = NULL;
    request.arrayOfFileRequests           = soap_new_srm__ArrayOfTGetFileRequest(soap, -1);
    request.userRequestDescription        = NULL;
    request.storageSystemInfo             = NULL;
    request.desiredFileStorageType        = NULL;
    request.desiredTotalRequestTime       = NULL;
    request.desiredLifeTime               = NULL;
    request.targetSpaceToken              = NULL;
    request.targetFileRetentionPolicyInfo = NULL;
    request.deferredStartTime             = NULL;
    request.transferParameters            = NULL;

    std::string endPoint;

    int currentArg=1;
    while ( currentArg < argc ) {
        if ( strcmp( argv[currentArg], "-a" ) == 0 ) {
            request.authorizationID = soap_new_std__string(soap, -1);
            request.authorizationID->assign(argv[++currentArg]);
        }
        else if ( strcmp( argv[currentArg], "-e") == 0 ) {
            endPoint.assign(argv[++currentArg]);
        }
        else if ( strcmp( argv[currentArg], "-s" ) == 0 ) {
            srm__TGetFileRequest *fileReq = soap_new_srm__TGetFileRequest(soap, -1);
            fileReq->dirOption = NULL;
            fileReq->sourceSURL.assign( argv[++currentArg] );
            request.arrayOfFileRequests->requestArray.push_back(fileReq);
        }
        else if ( strcmp( argv[currentArg], "-c") == 0 ) {
            request.userRequestDescription = soap_new_std__string(soap, -1);
            request.userRequestDescription->assign(argv[++currentArg]);
        }
        else if ( strcmp( argv[currentArg], "-r" ) == 0 ) {
            request.desiredTotalRequestTime = (int*)soap_malloc(soap, sizeof(int));
            *(request.desiredTotalRequestTime) = atoi(argv[++currentArg]);
        }
        else if ( strcmp( argv[currentArg], "-l" ) == 0 ) {
            request.desiredLifeTime = (int*)soap_malloc(soap, sizeof(int));
            *(request.desiredLifeTime) = atoi(argv[++currentArg]);
        }
        else if ( strcmp( argv[currentArg], "-x" ) == 0 ) {
            request.targetSpaceToken = soap_new_std__string(soap, -1);
            request.targetSpaceToken->assign(argv[++currentArg]);
        }
        else if ( strcmp( argv[currentArg], "-D" ) == 0 ) {
            request.deferredStartTime = (int*)soap_malloc(soap, sizeof(int));
            *(request.deferredStartTime) = atoi(argv[++currentArg]);
        }
        else if ( strcmp( argv[currentArg], "-p" ) == 0 ) {
            if (!request.transferParameters) {
                request.transferParameters = soap_new_srm__TTransferParameters(soap, -1);
                request.transferParameters->accessPattern = NULL;
                request.transferParameters->connectionType = NULL;
                request.transferParameters->arrayOfClientNetworks = NULL;
                request.transferParameters->arrayOfTransferProtocols = NULL;
                
                request.transferParameters->arrayOfTransferProtocols = soap_new_srm__ArrayOfString(soap, -1);
            }
            else if (!request.transferParameters->arrayOfTransferProtocols) {
                request.transferParameters->arrayOfTransferProtocols = soap_new_srm__ArrayOfString(soap, -1);
            }
            request.transferParameters->arrayOfTransferProtocols->stringArray.push_back( argv[++currentArg] );
        }
        else if ( strcmp( argv[currentArg], "-n" ) == 0 ) {
            if (!request.transferParameters) {
                request.transferParameters = soap_new_srm__TTransferParameters(soap, -1);
                request.transferParameters->accessPattern = NULL;
                request.transferParameters->connectionType = NULL;
                request.transferParameters->arrayOfClientNetworks = NULL;
                request.transferParameters->arrayOfTransferProtocols = NULL;

                request.transferParameters->arrayOfClientNetworks = soap_new_srm__ArrayOfString(soap, -1);
            }
            else if (!request.transferParameters->arrayOfClientNetworks) {
                request.transferParameters->arrayOfClientNetworks = soap_new_srm__ArrayOfString(soap, -1);
            }
            request.transferParameters->arrayOfClientNetworks->stringArray.push_back( argv[++currentArg] );
        }
        else if ( (strcmp( argv[currentArg], "--volatile" ) == 0) ||
                  (strcmp( argv[currentArg], "--durable" ) == 0) ||
                  (strcmp( argv[currentArg], "--permananent" ) == 0) ) {
            if ( request.desiredFileStorageType) {
                usage();
                exit(1);
            }
            request.desiredFileStorageType = (srm__TFileStorageType*)soap_malloc(soap, sizeof(srm__TFileStorageType));
            if ( strcmp( argv[currentArg], "--volatile" ) == 0)  *(request.desiredFileStorageType) = srm__TFileStorageType__VOLATILE;
            if ( strcmp( argv[currentArg], "--durable" ) == 0)   *(request.desiredFileStorageType) = srm__TFileStorageType__DURABLE;
            if ( strcmp( argv[currentArg], "--permanent" ) == 0) *(request.desiredFileStorageType) = srm__TFileStorageType__PERMANENT;
        }
        else if ( (strcmp( argv[currentArg], "--processing-mode" ) == 0) ||
                  (strcmp( argv[currentArg], "--xfer-mode" ) == 0) ) {
            if (!request.transferParameters) {
                request.transferParameters = soap_new_srm__TTransferParameters(soap, -1);
                request.transferParameters->accessPattern = NULL;
                request.transferParameters->connectionType = NULL;
                request.transferParameters->arrayOfClientNetworks = NULL;
                request.transferParameters->arrayOfTransferProtocols = NULL;

                request.transferParameters->accessPattern = (srm__TAccessPattern*)soap_malloc(soap, sizeof(srm__TAccessPattern));
            }
            else if (!request.transferParameters->accessPattern) {
                request.transferParameters->accessPattern = (srm__TAccessPattern*)soap_malloc(soap, sizeof(srm__TAccessPattern));
            }
            else {
                usage();
                exit (1);
            }
            if ( strcmp( argv[currentArg], "--processing-mode" ) == 0)  *(request.transferParameters->accessPattern) = srm__TAccessPattern__PROCESSING_USCOREMODE;
            if ( strcmp( argv[currentArg], "--xfer-mode" ) == 0)        *(request.transferParameters->accessPattern) = srm__TAccessPattern__TRANSFER_USCOREMODE;
        }
        else if ( (strcmp( argv[currentArg], "--wan" ) == 0) ||
                  (strcmp( argv[currentArg], "--lan" ) == 0) ) {
            if (!request.transferParameters) {
                request.transferParameters = soap_new_srm__TTransferParameters(soap, -1);
                request.transferParameters->accessPattern = NULL;
                request.transferParameters->connectionType = NULL;
                request.transferParameters->arrayOfClientNetworks = NULL;
                request.transferParameters->arrayOfTransferProtocols = NULL;

                request.transferParameters->connectionType = (srm__TConnectionType*)soap_malloc(soap, sizeof(srm__TConnectionType));
            }
            else if (!request.transferParameters->connectionType) {
                request.transferParameters->connectionType = (srm__TConnectionType*)soap_malloc(soap, sizeof(srm__TConnectionType));
            }
            else {
                usage();
                exit (1);
            }
            if ( strcmp( argv[currentArg], "--wan" ) == 0)  *(request.transferParameters->connectionType) = srm__TConnectionType__WAN;
            if ( strcmp( argv[currentArg], "--lan" ) == 0)  *(request.transferParameters->connectionType) = srm__TConnectionType__LAN;
        }
        else if ( (strcmp( argv[currentArg], "--replica" ) == 0) ||
                  (strcmp( argv[currentArg], "--output" ) == 0) ||
                  (strcmp( argv[currentArg], "--custodial" ) == 0) ) {
            if (!request.targetFileRetentionPolicyInfo) {
                request.targetFileRetentionPolicyInfo = soap_new_srm__TRetentionPolicyInfo(soap, -1);
                request.targetFileRetentionPolicyInfo->retentionPolicy = srm__TRetentionPolicy__REPLICA;
                request.targetFileRetentionPolicyInfo->accessLatency   = NULL;

            }
            if ( strcmp( argv[currentArg], "--replica" ) == 0)   request.targetFileRetentionPolicyInfo->retentionPolicy = srm__TRetentionPolicy__REPLICA;
            if ( strcmp( argv[currentArg], "--output" ) == 0)    request.targetFileRetentionPolicyInfo->retentionPolicy = srm__TRetentionPolicy__OUTPUT;
            if ( strcmp( argv[currentArg], "--custodial" ) == 0) request.targetFileRetentionPolicyInfo->retentionPolicy = srm__TRetentionPolicy__CUSTODIAL;
        }
        else if ( (strcmp( argv[currentArg], "--online" ) == 0) ||
                  (strcmp( argv[currentArg], "--nearline" ) == 0) ) {
            if (!request.targetFileRetentionPolicyInfo) {
                request.targetFileRetentionPolicyInfo = soap_new_srm__TRetentionPolicyInfo(soap, -1);
                request.targetFileRetentionPolicyInfo->retentionPolicy = srm__TRetentionPolicy__REPLICA;
                request.targetFileRetentionPolicyInfo->accessLatency   = NULL;

                request.targetFileRetentionPolicyInfo->accessLatency = (srm__TAccessLatency*)soap_malloc(soap, sizeof(srm__TAccessLatency));
            }
            else if (!request.targetFileRetentionPolicyInfo->accessLatency) {
                request.targetFileRetentionPolicyInfo->accessLatency = (srm__TAccessLatency*)soap_malloc(soap, sizeof(srm__TAccessLatency));
            }
            else {
                usage();
                exit (1);
            }
            if ( strcmp( argv[currentArg], "--nearline" ) == 0) *(request.targetFileRetentionPolicyInfo->accessLatency) = srm__TAccessLatency__NEARLINE;
            if ( strcmp( argv[currentArg], "--online" ) == 0)   *(request.targetFileRetentionPolicyInfo->accessLatency) = srm__TAccessLatency__ONLINE;
        }
        else {
            usage();
            exit(1);
        }
        currentArg++;
    }

    if ( endPoint.length() == 0 || request.arrayOfFileRequests == NULL ) {
        usage();
        exit(1);
    }

    PrepareToBringOnline( 
            endPoint,
            soap,
            request);

    exit (0);
}
