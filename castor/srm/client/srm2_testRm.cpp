/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testRm.cpp,v 1.11 2008/06/18 13:38:06 sdewitt Exp $

#include "soapH.h"
#include "srmNsmap.h"

#include "cgsi_plugin.h"


void usage() {
    std::cerr << "Usage:" << std::endl;
    std::cerr << "\tsrm2_testRm -e endPoint -s SURL (-h) (-a authID) (-k key:value)" << std::endl;
    std::cerr << "where: " << std::endl;
    std::cerr << "\t -e endpoint         Endpoint used to contact the SRM (mandatory)" << std::endl;
    std::cerr << "\t -s SURL             One or more SURLS (each with a separate -s option) to remove (manadatory)" << std::endl;
    std::cerr << "\t -a authID           The users authorization ID" << std::endl;
    std::cerr << "\t -key key:value      Key value pairs of additional information" << std::endl;
    std::cerr << "\t -h                  Print this message" << std::endl;
    return;
}

    int
main(int argc, char *argv[])
{
    srm__srmRmResponse_ rep;
    srm__srmRmRequest   req;
    struct soap* soap = soap_new();

    // Initialise soap structure
    soap_cgsi_init(soap, CGSI_OPT_DISABLE_NAME_CHECK|CGSI_OPT_DELEG_FLAG);
    soap_set_namespaces(soap, namespaces);

    req.authorizationID   = NULL;
    req.arrayOfSURLs      = NULL;
    req.storageSystemInfo = NULL;

    //Parse arguments
    std::string endPoint;
    int carg = 1;
    while ( carg < argc ) {
        if (strcmp (argv[carg], "-e") == 0 ) {
            endPoint.assign(argv[++carg]);
        }
        else if (strcmp (argv[carg], "-s") == 0 ) {
            if (!req.arrayOfSURLs)
                req.arrayOfSURLs = soap_new_srm__ArrayOfAnyURI(soap, -1);
            req.arrayOfSURLs->urlArray.push_back(argv[++carg]);
        }
        else if (strcmp(argv[carg],"-a") == 0) {
            if (!req.authorizationID) req.authorizationID = soap_new_std__string(soap, -1);
            req.authorizationID->assign(argv[++carg]);
        }
        else if ( strcmp(argv[carg], "-k") == 0 ) {
            std::string arg(argv[++carg]);
            std::string separator(":");
            size_t sepP = arg.find(separator);
            if ( sepP == std::string::npos || 
                    sepP == arg.length() ||
                    sepP == 0) {
                std::cout << "Invalid key/value pair" << std::endl;
                usage();
                exit(1);
            }
            std::string key = arg.substr(0, arg.find(separator));
            std::string val = arg.substr(arg.find(separator)+1, arg.length());
            if (!req.storageSystemInfo) req.storageSystemInfo = soap_new_srm__ArrayOfTExtraInfo(soap, -1);
            srm__TExtraInfo *xinfo = soap_new_srm__TExtraInfo(soap, -1);
            xinfo->key.assign(key);
            xinfo->value = 0;
            if ( val.length() > 0 ) {
                xinfo->value = soap_new_std__string(soap, -1);
                xinfo->value->assign(val);
            }
            req.storageSystemInfo->extraInfoArray.push_back(xinfo);
        }
        else if ( strcmp(argv[carg], "-h") == 0 || strcmp(argv[carg], "--help") == 0 ) {
            usage();
            exit(0);
        }
        else {
            std::cout << "Unknown option: " << argv[carg] << std::endl;
            usage();
            exit(1);
        }
        carg++;
    }
    if ( endPoint.length() == 0 ) {
        std::cerr << "No end point specified!" << std::endl;
        usage();
        exit (1);
    }
    if ( req.arrayOfSURLs->urlArray.size() == 0 ) {
        std::cerr << "No SURL specified!" << std::endl;
        usage();
        exit (1);
    }

    if (soap_call_srm__srmRm (soap, endPoint.c_str(), "SrmRm",
                &req, rep)) {
        soap_print_fault (soap, stderr);
        soap_print_fault_location (soap, stderr);
        soap_end (soap);
        exit (1);
    }

    srm__TReturnStatus *status = rep.srmRmResponse->returnStatus;
    srm__ArrayOfTSURLReturnStatus *response = rep.srmRmResponse->arrayOfFileStatuses;

    /* wait for file "ready" */

    std::cout << "Request Status: " << soap_srm__TStatusCode2s(soap, status->statusCode) << std::endl;
    if ( status->explanation) std::cout << "Explanation: " << status->explanation->c_str() << std::endl;
    if ( response ) {
        std::cout << "========== File Status ==========" << std::endl;
        for (unsigned i = 0; i < response->statusArray.size(); i++) {
            std::cout << "SURL: " << response->statusArray[i]->surl.c_str() << std::endl;
            std::cout << "\tStatus: " << soap_srm__TStatusCode2s(soap, response->statusArray[i]->status->statusCode) << std::endl;
            if ( response->statusArray[i]->status->explanation) 
                std::cout << "\tExplanation: " << response->statusArray[i]->status->explanation->c_str() << std::endl;
        }
    }

    exit (0);
}
