/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */


#include "soapH.h"
#include "srmNsmap.h"
#include "cgsi_plugin.h"

void usage() {
    std::cout << "Usage:" << std::endl;
    std::cout << "\tsrm2_testSpaceMetaData -e endPoint [-x spaceToken]+ (-a authorizationId) (-h)" << std::endl;
}

    int
main(int argc, char *argv[])
{
    srm__srmGetSpaceMetaDataResponse_ rep;
    srm__srmGetSpaceMetaDataRequest req;
    struct soap* soap = soap_new();
    std::string endPoint;

    req.authorizationID = NULL;
    req.arrayOfSpaceTokens = NULL;

    int cArg=1;
    while (cArg < argc) {
        if (std::string(argv[cArg]) == "-h") {
            usage();
            exit (0);
        }
        else if (std::string(argv[cArg]) == "-a") {
            if (!req.authorizationID) 
                req.authorizationID = soap_new_std__string(soap, -1);
            req.authorizationID->assign(argv[++cArg]);
        }
        else if (std::string(argv[cArg]) == "-x") {
            if (!req.arrayOfSpaceTokens)
                req.arrayOfSpaceTokens = soap_new_srm__ArrayOfString(soap, -1);
            req.arrayOfSpaceTokens->stringArray.push_back(argv[++cArg]);
        }
        else if (std::string(argv[cArg]) == "-e") {
            endPoint.assign(argv[++cArg]);
        }
        else {
            std::cerr << "Invalid argument: " << argv[cArg] << std::endl;
            usage();
            exit(1);
        }
        cArg++;
    }

    if (endPoint.length() == 0) {
        std::cerr << "No end point supplied." << std::endl;
        usage();
        exit(1);
    }

    soap_cgsi_init(soap, CGSI_OPT_DISABLE_NAME_CHECK|CGSI_OPT_DELEG_FLAG);
    soap_set_namespaces(soap, namespaces);


    /* To send the request ... */

    if (soap_call_srm__srmGetSpaceMetaData (soap, const_cast<char*>(endPoint.c_str()), "SrmGetSpaceMetaData",
                &req, rep)) {
        soap_print_fault (soap, stderr);
        soap_print_fault_location (soap, stderr);
        soap_end (soap);
        exit (1);
    }


    srm__TReturnStatus* status = rep.srmGetSpaceMetaDataResponse->returnStatus;
    srm__ArrayOfTMetaDataSpace* summaries = rep.srmGetSpaceMetaDataResponse->arrayOfSpaceDetails;

    std::cout << "Request Status: " << soap_srm__TStatusCode2s(soap, status->statusCode) << std::endl;
    if (status->explanation)
        std::cout << "Explanation: " << status->explanation->c_str() << std::endl;

    if (summaries) {
        std::cout << "======= BEGIN SUMMARY ========" << std::endl;
        for (unsigned i=0; i< summaries->spaceDataArray.size(); i++) {
            status = summaries->spaceDataArray[i]->status;
            std::cout << "Token: " << summaries->spaceDataArray[i]->spaceToken << std::endl;
            if (status) {
                std::cout << "\tStatus: " << soap_srm__TStatusCode2s(soap, status->statusCode) << std::endl;
                if (status->explanation)
                    std::cout << "\tExplanation: " << status->explanation->c_str() << std::endl;
            }
            if ( summaries->spaceDataArray[i]->owner ) 
                std::cout << "\tOwner: " << summaries->spaceDataArray[i]->owner->c_str() << std::endl;
            if ( summaries->spaceDataArray[i]->totalSize ) 
                std::cout << "\tTotal Size: " << *(summaries->spaceDataArray[i]->totalSize) << std::endl;
            if ( summaries->spaceDataArray[i]->guaranteedSize )
                std::cout << "\tGuaranteed Size: " << *(summaries->spaceDataArray[i]->guaranteedSize) << std::endl;
            if ( summaries->spaceDataArray[i]->unusedSize )
                std::cout << "\tUnused Size: " << *(summaries->spaceDataArray[i]->unusedSize) << std::endl;
            if ( summaries->spaceDataArray[i]->lifetimeAssigned )
                std::cout << "\tAssigned lifetime: " << *(summaries->spaceDataArray[i]->lifetimeAssigned) << std::endl;
            if ( summaries->spaceDataArray[i]->lifetimeLeft) 
                std::cout << "\tRemaining lifetime: " << *(summaries->spaceDataArray[i]->lifetimeLeft) << std::endl;
            if (summaries->spaceDataArray[i]->retentionPolicyInfo) {
                std::cout << "\tRetention Policy: " << summaries->spaceDataArray[i]->retentionPolicyInfo->retentionPolicy << std::endl;
                if (summaries->spaceDataArray[i]->retentionPolicyInfo->accessLatency)
                    std::cout << "\tAccess Latency: " << *(summaries->spaceDataArray[i]->retentionPolicyInfo->accessLatency) << std::endl;
            }
        }
    }

    soap_end (soap);
    exit (0);
}
