/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testPrepareToPut.cpp,v 1.11 2008/10/15 21:19:44 sdewitt Exp $

#include "soapH.h"
#include "srmNsmap.h"
#include "cgsi_plugin.h"

using namespace std;


void usage() {
    cout << "Usage: srm2_testPrepareToPut" << endl;
    std::cerr << "\t -e endPoint*        The endpoint of the SRM" << std::endl;
    std::cerr << "\t -a authorizationID" << std::endl;
    std::cerr << "\t -s SURL             (once for each SURL required)." << std::endl;
    std::cerr << "\t -b fileSize         File size in bytes. There must be one value per SURL is the same order that the SURLs are specified." << std::endl;
    std::cerr << "\t -B fileSize " << std::endl;
    std::cerr << "\t\t File size in bytes.  If -b id used, there must be one value per SURL in the same order that the SURLs are specified." << std::endl;
    std::cerr << "\t\t If -B is used - the same file size will be assumed for each SURL specified" << std::endl;
    std::cerr << "\t -c comment          A user defined comment which can be used to identify this request." << std::endl;
    std::cerr << "\t -o (true|false)     Overwrite option; whether an existing file should be overwritten or not" << std::endl;
    std::cerr << "\t -r requestLifetime  The total length of time that this request should be completed within." << std::endl;
    std::cerr << "\t -t pinLifetime      The lifetime of the TURL returned." << std::endl;
    std::cerr << "\t -f fileLifetime     The duration of the file within the namespace." << std::endl;
    std::cerr << "\t -x spaceToken       The spaceToken of the desired space." << std::endl;
    std::cerr << "\t -p protocol         This can be repeated to allow choice of protocols." << std::endl;
    std::cerr << "\t -n networks         This can be repeated to supply a range of client networks." << std::endl;
    std::cerr << "\t -h" << std::endl;
    std::cerr << "\t --help              Print this message" << std::endl;

    std::cerr << "\t --volatile" << std::endl;
    std::cerr << "\t --durable" << std::endl;
    std::cerr << "\t --permanent" << std::endl;
    std::cerr << "\t\t Type of storage requred" << std::endl;
    std::cerr << "\t --processing-mode" << std::endl;
    std::cerr << "\t --xfer-mode" << std::endl;
    std::cerr << "\t\t Mode in which data will be accessed.  xfer mode commonly refers to bulk xfers like gridftp" << std::endl;
    std::cerr << "\t\t while processing mode is for slower write requests from jobs." << std::endl;
    std::cerr << "\t --wan "  << std::endl;
    std::cerr << "\t --lan" << std::endl;
    std::cerr << "\t\t Indicates whether the xfer is expected to be over the WAN or internal network." << std::endl;
    std::cerr << "\t --replica" << std::endl;
    std::cerr << "\t --output" << std::endl;
    std::cerr << "\t --custodial" << std::endl;
    std::cerr << "\t\t Indicates quality of retention required.  Typically replica means no back-up, while " << std::endl;
    std::cerr << "\t\t custodial implies the data is backed up un secure, recoverable media" << std::endl;
    std::cerr << "\t --online" << std::endl;
    std::cerr << "\t --nearline" << std::endl;
    std::cerr << "\t\t Indicates whether the data should remain online, or disk copies can be removed by the system" << std::endl;
    std::cerr << "NOTE: If specifying SURLs and file sizes, the SURLs **MUST** be specified before the size, and must be" << std::endl;
    std::cerr << "\t specified interlaced.  In other words - the following is valid:" << std::endl;
    std::cerr << "\t\t\t srm2_testPrepareToPut -s file1 -b 300 -s file2 -b 900" << std::endl;
    std::cerr << "\t while the following would not work and will give unexpected results: " << std::endl;
    std::cerr << "\t\t\t srm2_testPrepareToPut -s file1 -s file2 -b 300 -b 900. " << std::endl;
    return;
}

bool validArg (char *arg) {
    bool valid = true;
    if ( arg[0] == '-' ) {
        valid = false;
        cerr << "Validation failed on argument" << arg << endl;
    }
    return valid;
}

void PrepareRequest(
        std::string endPoint,
        struct soap *soap, 
        srm__srmPrepareToPutRequest req) {

    srm__srmPrepareToPutResponse_ rtn;

    // Make the soap call...
    if ( soap_call_srm__srmPrepareToPut (soap, endPoint.c_str(), "srmPrepareToPut", &req, rtn) ) {
        soap_print_fault (soap, stderr);
        soap_print_fault_location (soap, stderr);
        return;
    }

    // Check the response
    if ( rtn.srmPrepareToPutResponse == NULL) {
        cerr << "SOAP call returned null!" << endl;
    }
    else if ( rtn.srmPrepareToPutResponse->returnStatus == NULL) {
        cerr << "rtn.srmPrepareToPutResponse->returnStatus == NULL !" << endl;
    }
    else {
        // Print return structure....
        cout << "Status: " << soap_srm__TStatusCode2s(soap, rtn.srmPrepareToPutResponse->returnStatus->statusCode) << endl;

        if ( rtn.srmPrepareToPutResponse->returnStatus->explanation ) {
            cout << "Explanation: " << rtn.srmPrepareToPutResponse->returnStatus->explanation->c_str() << endl;
        }

        if ( rtn.srmPrepareToPutResponse->requestToken ) {
            cout << "rtn.srmPrepareToPutResponse->requestToken = " <<
                rtn.srmPrepareToPutResponse->requestToken->c_str() << endl;
        }

        if ( rtn.srmPrepareToPutResponse->remainingTotalRequestTime ) {
            cout << "rtn.srmPrepareToPutResponse->remainingTotalRequestTime = " <<
                *(rtn.srmPrepareToPutResponse->remainingTotalRequestTime) << endl;
        }

        if ( rtn.srmPrepareToPutResponse->arrayOfFileStatuses ) {
            std::vector<srm__TPutRequestFileStatus*> statusVector = rtn.srmPrepareToPutResponse->arrayOfFileStatuses->statusArray;
            for ( unsigned int i=0; i< statusVector.size(); i++ ) {
                cout << "\tFile[" << i << "]:" << endl;
                cout << "\t\tsiteURL = " << statusVector[i]->SURL << endl;
                if ( statusVector[i]->transferURL ) {
                    cout << "\t\ttransferURL = " << statusVector[i]->transferURL<< endl;
                }
                if ( statusVector[i]->estimatedWaitTime ) {
                    cout << "\t\testimatedWaitTime = " << *(statusVector[i]->estimatedWaitTime) << endl;
                }
                if ( statusVector[i]->fileSize ) {
                    cout << "\t\tfileSize = " << *(statusVector[i]->fileSize) << endl;
                }
                if ( statusVector[i]->remainingPinLifetime ) {
                    cout << "\t\tremainingPinTime = " << *(statusVector[i]->remainingPinLifetime) << endl;
                }
                if ( statusVector[i]->remainingFileLifetime ) {
                    cout << "\t\tremainingFileTime = " << *(statusVector[i]->remainingFileLifetime) << endl;
                }
                if ( statusVector[i]->status->statusCode ) {
                    cout << "\t\tstatusCode = " << soap_srm__TStatusCode2s(soap, statusVector[i]->status->statusCode) << endl;
                }
                if ( statusVector[i]->status->explanation ) {
                    cout << "\t\texplanation = " << statusVector[i]->status->explanation->c_str() << endl;
                }
            }

        }
    }
}


int main(int argc, char*argv[])
{
    struct soap* soap = soap_new();

    int flags = CGSI_OPT_CLIENT|CGSI_OPT_DISABLE_NAME_CHECK|CGSI_OPT_DELEG_FLAG;
    if ( soap_cgsi_init (soap, flags) != 0 ) {
        exit(1);
    }
    soap_set_namespaces(soap, namespaces);

    if ( argc < 3) {
        std::cerr << "Too few arguments" << std::endl;
        usage();
        exit(1);
    }

    srm__srmPrepareToPutRequest request;

    // Initialise structure
    request.authorizationID               = NULL;
    request.arrayOfFileRequests           = NULL;
    request.userRequestDescription        = NULL;
    request.overwriteOption               = NULL;
    request.storageSystemInfo             = NULL;
    request.desiredTotalRequestTime       = NULL;
    request.desiredPinLifeTime            = NULL;
    request.desiredFileLifeTime           = NULL;
    request.desiredFileStorageType        = NULL;
    request.targetSpaceToken              = NULL;
    request.targetFileRetentionPolicyInfo = NULL;
    request.transferParameters            = NULL;

    ULONG64 defFileSize=0;
    std::string endPoint;

    int currentArg = 1; // Current argument
    while (currentArg < argc) {
        if ( strcmp( argv[currentArg], "-a" ) == 0 ) {
            request.authorizationID = soap_new_std__string(soap, -1);
            request.authorizationID->assign(argv[++currentArg]);
        }
        else if ( strcmp( argv[currentArg], "-s" ) == 0 ) {
            if ( !request.arrayOfFileRequests ) request.arrayOfFileRequests = soap_new_srm__ArrayOfTPutFileRequest(soap, -1);
            srm__TPutFileRequest *thisrequest = soap_new_srm__TPutFileRequest(soap, -1);
            thisrequest->targetSURL = soap_new_std__string(soap, -1);
            thisrequest->targetSURL->assign(argv[++currentArg]);
            thisrequest->expectedFileSize = NULL;
            request.arrayOfFileRequests->requestArray.push_back(thisrequest);
        }
        else if ( strcmp( argv[currentArg], "-b") == 0 ) {
            request.arrayOfFileRequests->requestArray[request.arrayOfFileRequests->requestArray.size() - 1]->expectedFileSize = (ULONG64*)soap_malloc(soap, sizeof(ULONG64));
            *(request.arrayOfFileRequests->requestArray[request.arrayOfFileRequests->requestArray.size() - 1]->expectedFileSize) = atoll( argv[++currentArg] );
        }
        else if ( strcmp( argv[currentArg], "-B") == 0 ) {
            defFileSize = atoll(argv[++currentArg]);
        }
        else if ( strcmp( argv[currentArg], "-e") == 0 ) {
            endPoint.assign(argv[++currentArg]);
        }
        else if ( strcmp( argv[currentArg], "-c") == 0 ) {
            request.userRequestDescription = soap_new_std__string(soap, -1);
            request.userRequestDescription->assign(argv[++currentArg]);
        }
        else if ( strcmp( argv[currentArg], "-o") == 0 ) {
            request.overwriteOption = (enum srm__TOverwriteMode*)soap_malloc(soap, sizeof(enum srm__TOverwriteMode));
            if ( strcmp (argv[currentArg+1], "true") == 0 ) {
                *(request.overwriteOption) = srm__TOverwriteMode__ALWAYS;
            }
            else if ( strcmp (argv[currentArg+1], "false") == 0 ) {
                *(request.overwriteOption) = srm__TOverwriteMode__NEVER;
            }
            else {
                usage();
                exit (1);
            }
            currentArg++;
        }
        else if ( strcmp( argv[currentArg], "-r" ) == 0 ) {
            request.desiredTotalRequestTime = (int*)soap_malloc(soap, sizeof(int));
            *(request.desiredTotalRequestTime) = atoi(argv[++currentArg]);
        }
        else if ( strcmp( argv[currentArg], "-t" ) == 0 ) {
            request.desiredPinLifeTime = (int*)soap_malloc(soap, sizeof(int));
            *(request.desiredPinLifeTime) = atoi(argv[++currentArg]);
        }
        else if ( strcmp( argv[currentArg], "-f" ) == 0 ) {
            request.desiredFileLifeTime = (int*)soap_malloc(soap, sizeof(int));
            *(request.desiredFileLifeTime) = atoi(argv[++currentArg]);
        }
        else if ( strcmp( argv[currentArg], "-x" ) == 0 ) {
            request.targetSpaceToken = soap_new_std__string(soap, -1);
            request.targetSpaceToken->assign(argv[++currentArg]);
        }
        else if ( strcmp( argv[currentArg], "-p" ) == 0 ) {
            if (!request.transferParameters) {
                request.transferParameters = soap_new_srm__TTransferParameters(soap, -1);
                request.transferParameters->accessPattern = NULL;
                request.transferParameters->connectionType = NULL;
                request.transferParameters->arrayOfClientNetworks = NULL;
                request.transferParameters->arrayOfTransferProtocols = NULL;

                request.transferParameters->arrayOfTransferProtocols = soap_new_srm__ArrayOfString(soap, -1);
            }
            else if (!request.transferParameters->arrayOfTransferProtocols) {
                request.transferParameters->arrayOfTransferProtocols = soap_new_srm__ArrayOfString(soap, -1);
            }
            request.transferParameters->arrayOfTransferProtocols->stringArray.push_back( argv[++currentArg] );
        }
        else if ( strcmp( argv[currentArg], "-n" ) == 0 ) {
            if (!request.transferParameters) {
                request.transferParameters = soap_new_srm__TTransferParameters(soap, -1);
                request.transferParameters->accessPattern = NULL;
                request.transferParameters->connectionType = NULL;
                request.transferParameters->arrayOfClientNetworks = NULL;
                request.transferParameters->arrayOfTransferProtocols = NULL;

                request.transferParameters->arrayOfClientNetworks = soap_new_srm__ArrayOfString(soap, -1);
            }
            else if (!request.transferParameters->arrayOfClientNetworks) {
                request.transferParameters->arrayOfClientNetworks = soap_new_srm__ArrayOfString(soap, -1);
            }
            request.transferParameters->arrayOfClientNetworks->stringArray.push_back( argv[++currentArg] );
        }
        else if ( (strcmp( argv[currentArg], "--volatile" ) == 0) ||
                (strcmp( argv[currentArg], "--durable" ) == 0) ||
                (strcmp( argv[currentArg], "--permanent" ) == 0) ) {
            if ( request.desiredFileStorageType) {
                std::cerr << "Multiple storage types on command line" << std::endl;
                usage();
                exit(1);
            }
            request.desiredFileStorageType = (srm__TFileStorageType*)soap_malloc(soap, sizeof(srm__TFileStorageType));
            if ( strcmp( argv[currentArg], "--volatile" ) == 0)  *(request.desiredFileStorageType) = srm__TFileStorageType__VOLATILE;
            if ( strcmp( argv[currentArg], "--durable" ) == 0)   *(request.desiredFileStorageType) = srm__TFileStorageType__DURABLE;
            if ( strcmp( argv[currentArg], "--permanent" ) == 0) *(request.desiredFileStorageType) = srm__TFileStorageType__PERMANENT;
        }
        else if ( (strcmp( argv[currentArg], "--processing-mode" ) == 0) ||
                (strcmp( argv[currentArg], "--xfer-mode" ) == 0) ) {
            if (!request.transferParameters) {
                request.transferParameters = soap_new_srm__TTransferParameters(soap, -1);
                request.transferParameters->accessPattern = NULL;
                request.transferParameters->connectionType = NULL;
                request.transferParameters->arrayOfClientNetworks = NULL;
                request.transferParameters->arrayOfTransferProtocols = NULL;

                request.transferParameters->accessPattern = (srm__TAccessPattern*)soap_malloc(soap, sizeof(srm__TAccessPattern));
            }
            else if (!request.transferParameters->accessPattern) {
                request.transferParameters->accessPattern = (srm__TAccessPattern*)soap_malloc(soap, sizeof(srm__TAccessPattern));
            }
            else {
                std::cerr << "Multiple access modes on command line" << std::endl;
                usage();
                exit (1);
            }
            if ( strcmp( argv[currentArg], "--processing-mode" ) == 0)  *(request.transferParameters->accessPattern) = srm__TAccessPattern__PROCESSING_USCOREMODE;
            if ( strcmp( argv[currentArg], "--xfer-mode" ) == 0)        *(request.transferParameters->accessPattern) = srm__TAccessPattern__TRANSFER_USCOREMODE;
        }
        else if ( (strcmp( argv[currentArg], "--wan" ) == 0) ||
                (strcmp( argv[currentArg], "--lan" ) == 0) ) {
            if (!request.transferParameters) {
                request.transferParameters = soap_new_srm__TTransferParameters(soap, -1);
                request.transferParameters->accessPattern = NULL;
                request.transferParameters->connectionType = NULL;
                request.transferParameters->arrayOfClientNetworks = NULL;
                request.transferParameters->arrayOfTransferProtocols = NULL;

                request.transferParameters->connectionType = (srm__TConnectionType*)soap_malloc(soap, sizeof(srm__TConnectionType));
            }
            else if (!request.transferParameters->connectionType) {
                request.transferParameters->connectionType = (srm__TConnectionType*)soap_malloc(soap, sizeof(srm__TConnectionType));
            }
            else {
                std::cerr << "Multiple network modes on command line" << std::endl;
                usage();
                exit (1);
            }
            if ( strcmp( argv[currentArg], "--wan" ) == 0)  *(request.transferParameters->connectionType) = srm__TConnectionType__WAN;
            if ( strcmp( argv[currentArg], "--lan" ) == 0)  *(request.transferParameters->connectionType) = srm__TConnectionType__LAN;
        }
        else if ( (strcmp( argv[currentArg], "--replica" ) == 0) ||
                (strcmp( argv[currentArg], "--output" ) == 0) ||
                (strcmp( argv[currentArg], "--custodial" ) == 0) ) {
            if (!request.targetFileRetentionPolicyInfo) {
                request.targetFileRetentionPolicyInfo = soap_new_srm__TRetentionPolicyInfo(soap, -1);
                request.targetFileRetentionPolicyInfo->retentionPolicy = srm__TRetentionPolicy__REPLICA;
                request.targetFileRetentionPolicyInfo->accessLatency   = NULL;

            }
            if ( strcmp( argv[currentArg], "--replica" ) == 0)   request.targetFileRetentionPolicyInfo->retentionPolicy = srm__TRetentionPolicy__REPLICA;
            if ( strcmp( argv[currentArg], "--output" ) == 0)    request.targetFileRetentionPolicyInfo->retentionPolicy = srm__TRetentionPolicy__OUTPUT;
            if ( strcmp( argv[currentArg], "--custodial" ) == 0) request.targetFileRetentionPolicyInfo->retentionPolicy = srm__TRetentionPolicy__CUSTODIAL;
        }
        else if ( (strcmp( argv[currentArg], "--online" ) == 0) ||
                (strcmp( argv[currentArg], "--nearline" ) == 0) ) {
            if (!request.targetFileRetentionPolicyInfo) {
                request.targetFileRetentionPolicyInfo = soap_new_srm__TRetentionPolicyInfo(soap, -1);
                request.targetFileRetentionPolicyInfo->retentionPolicy = srm__TRetentionPolicy__REPLICA;
                request.targetFileRetentionPolicyInfo->accessLatency   = NULL;

                request.targetFileRetentionPolicyInfo->accessLatency = (srm__TAccessLatency*)soap_malloc(soap, sizeof(srm__TAccessLatency));
            }
            else if (!request.targetFileRetentionPolicyInfo->accessLatency) {
                request.targetFileRetentionPolicyInfo->accessLatency = (srm__TAccessLatency*)soap_malloc(soap, sizeof(srm__TAccessLatency));
            }
            else {
                usage();
                exit (1);
            }
            if ( strcmp( argv[currentArg], "--nearline" ) == 0) *(request.targetFileRetentionPolicyInfo->accessLatency) = srm__TAccessLatency__NEARLINE;
            if ( strcmp( argv[currentArg], "--online" ) == 0)   *(request.targetFileRetentionPolicyInfo->accessLatency) = srm__TAccessLatency__ONLINE;
        }
        else if ( strcmp( argv[currentArg], "-h") == 0 || strcmp ( argv[currentArg], "--help") == 0 ) {
            usage();
            exit (0);
        }
        else {
            std::cerr << "Invalid argument: " << argv[currentArg] << std::endl;
            usage();
            exit(1);
        }
        currentArg++;
    }

    if (endPoint.size() == 0 ) {
        std::cerr << "No endPoint specified" << std::endl;
        usage();
        exit (1);
    }

    if ( defFileSize > 0 && request.arrayOfFileRequests) {
        for ( unsigned i=0; i<request.arrayOfFileRequests->requestArray.size(); i++ ) {
            request.arrayOfFileRequests->requestArray[i]->expectedFileSize = (ULONG64*)soap_malloc(soap, sizeof(ULONG64));
            *(request.arrayOfFileRequests->requestArray[i]->expectedFileSize) = defFileSize;
        }
    }


    PrepareRequest( 
            endPoint,
            soap,
            request);
    soap_end (soap);
}
