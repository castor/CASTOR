/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testReserveSpace.cpp,v 1.10 2008/06/13 14:24:25 sdewitt Exp $

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <map>

#ifdef DG_DIAGNOSE
#include "diagnose/dg.h"
#endif

#include "soapH.h"
#include "srmNsmap.h"
#define DEFPOLLINT 10

#define SRM_EP_PATH "/v2_1_1/srm"
#include "cgsi_plugin.h"

using namespace std;

void usage () {
    cerr << "Usage: srm2_testReserveSpace -e endpoint -t (0|1|2) [-c comment] [-s desiredSize] [-m minimumSize] [-l lifetime] -i [storageSystemInfo]" << endl;
}

#include "parsesurl.ic"
#include "soapcallns1.ic"

int main(int argc, char** argv)
{
    struct srm__srmReserveSpaceResponse_ rep;
    //struct ArrayOfTSURLPermissionReturn *repfs;
    srm__srmReserveSpaceRequest req;
    //struct srm__TSURLInfo *reqfilep;
    srm__TReturnStatus *reqstatp;
    struct soap* soap = soap_new();
    std::string srm_endpoint;

    if (argc < 4) {
        usage();
        exit (1);
    }

    std::map<srm__TStatusCode, std::string> codes;
    codes[srm__TStatusCode__SRM_USCORESUCCESS] = "SRM_SUCCESS";
    codes[srm__TStatusCode__SRM_USCOREFAILURE] = "SRM_FAILURE";
    codes[srm__TStatusCode__SRM_USCOREAUTHENTICATION_USCOREFAILURE] = "SRM_AUTHENTICATION_FAILURE";
    codes[srm__TStatusCode__SRM_USCOREAUTHORIZATION_USCOREFAILURE]= "SRM_AUTHORIZATION_FAILURE";
    codes[srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST]= "SRM_INVALID_REQUEST";
    codes[srm__TStatusCode__SRM_USCOREINVALID_USCOREPATH]= "SRM_INVALID_PATH";
    codes[srm__TStatusCode__SRM_USCOREFILE_USCORELIFETIME_USCOREEXPIRED]= "SRM_FILE_LIFETIME_EXPIRED";
    codes[srm__TStatusCode__SRM_USCORESPACE_USCORELIFETIME_USCOREEXPIRED]= "SRM_SPECE_LIFETIME_EXPIRED";
    codes[srm__TStatusCode__SRM_USCOREEXCEED_USCOREALLOCATION]= "SRM_EXCEED_ALLOCATION";
    codes[srm__TStatusCode__SRM_USCORENO_USCOREUSER_USCORESPACE]= "SRM_NO_USER_SPACE";
    codes[srm__TStatusCode__SRM_USCORENO_USCOREFREE_USCORESPACE]= "SRM_NO_FREE_SPACE";
    codes[srm__TStatusCode__SRM_USCOREDUPLICATION_USCOREERROR]= "SRM_DUPLICATION_ERROR";
    codes[srm__TStatusCode__SRM_USCORENON_USCOREEMPTY_USCOREDIRECTORY]= "SRM_NON_EMPTY_DIRECTORY";
    codes[srm__TStatusCode__SRM_USCORETOO_USCOREMANY_USCORERESULTS]= "SRM_TOO_MANY_RESULTS";
    codes[srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR]= "SRM_INTERNAL_ERROR";
    codes[srm__TStatusCode__SRM_USCOREFATAL_USCOREINTERNAL_USCOREERROR]= "SRM_FATAL_INTERNAL_ERROR";
    codes[srm__TStatusCode__SRM_USCORENOT_USCORESUPPORTED]= "SRM_NOT_SUPPORTED";
    codes[srm__TStatusCode__SRM_USCOREREQUEST_USCOREQUEUED]= "SRM_REQUEST_QUEUED";
    codes[srm__TStatusCode__SRM_USCOREREQUEST_USCOREINPROGRESS]= "SRM_REQUEST_INPROGESS";
    codes[srm__TStatusCode__SRM_USCOREREQUEST_USCORESUSPENDED]= "SRM_REQUEST_SUSPENDED";
    codes[srm__TStatusCode__SRM_USCOREABORTED]= "SRM_ABORTED";
    codes[srm__TStatusCode__SRM_USCORERELEASED]= "SRM_RELEASED";
    codes[srm__TStatusCode__SRM_USCOREFILE_USCOREPINNED]= "SRM_FILE_PINNED";
    codes[srm__TStatusCode__SRM_USCOREFILE_USCOREIN_USCORECACHE]= "SRM_FILE_IN_CACHE";
    codes[srm__TStatusCode__SRM_USCORESPACE_USCOREAVAILABLE]= "SRM_SPACE_AVAILABLE";
    codes[srm__TStatusCode__SRM_USCORELOWER_USCORESPACE_USCOREGRANTED]= "SRM_LOWER_SPACE_GRANTED";
    codes[srm__TStatusCode__SRM_USCOREDONE]= "SRM_DONE";
    codes[srm__TStatusCode__SRM_USCOREPARTIAL_USCORESUCCESS]= "SRM_PARTIAL_SUCCESS";
    codes[srm__TStatusCode__SRM_USCOREREQUEST_USCORETIMED_USCOREOUT]= "SRM_REQUEST_TIMED_OUT";
    codes[srm__TStatusCode__SRM_USCORELAST_USCORECOPY]= "SRM_LAST_COPY";
    codes[srm__TStatusCode__SRM_USCOREFILE_USCOREBUSY]= "SRM_FILE_BUSY";
    codes[srm__TStatusCode__SRM_USCOREFILE_USCORELOST]= "SRM_FILE_LOST";
    codes[srm__TStatusCode__SRM_USCOREFILE_USCOREUNAVAILABLE]= "SRM_FILE_UNAVAILABLE";
    codes[srm__TStatusCode__SRM_USCORECUSTOM_USCORESTATUS]= "SRM_CUSTOM_STATUS";


    soap_cgsi_init(soap, CGSI_OPT_DISABLE_NAME_CHECK|CGSI_OPT_DELEG_FLAG);
    soap_set_namespaces(soap, namespaces);

    req.authorizationID = NULL;
    req.userSpaceTokenDescription = NULL;
    req.retentionPolicyInfo = soap_new_srm__TRetentionPolicyInfo(soap, -1);
    req.retentionPolicyInfo->accessLatency = NULL;
    req.desiredSizeOfTotalSpace = NULL;
    req.desiredSizeOfGuaranteedSpace = 0;
    req.desiredLifetimeOfReservedSpace = NULL;
    req.storageSystemInfo = NULL;
    req.arrayOfExpectedFileSizes = NULL;
    req.storageSystemInfo = NULL;
    req.transferParameters = NULL;

    // Parse the arguments
    int carg = 0;
    while ( carg+1 < argc ) {
        carg++;
        if ( argv[carg][0] != '-' ) {
            usage();
            exit(1);
        }
        switch (argv[carg][1]) {
            case 'e':
                srm_endpoint = argv[++carg];
                break;
            case 't':
                switch ( atoi(argv[++carg]) ) {
                    case 0:
                        req.retentionPolicyInfo->retentionPolicy = srm__TRetentionPolicy__REPLICA;
                        break;
                    case 1:
                        req.retentionPolicyInfo->retentionPolicy = srm__TRetentionPolicy__OUTPUT;
                        break;
                    case 2:
                        req.retentionPolicyInfo->retentionPolicy = srm__TRetentionPolicy__CUSTODIAL;
                        break;
                    default:
                        usage();
                        exit(1);
                }
                break;
            case 'c':
                req.userSpaceTokenDescription = soap_new_std__string(soap, -1);
                req.userSpaceTokenDescription->assign(argv[++carg]);
                break;
            case 's':
                req.desiredSizeOfTotalSpace = (ULONG64*)soap_malloc(soap, sizeof(ULONG64));
                *(req.desiredSizeOfTotalSpace) = atoi(argv[++carg]);
                break;
            case 'm':
                req.desiredSizeOfGuaranteedSpace = atoi(argv[++carg]);
                break;
            case 'l':
                req.desiredLifetimeOfReservedSpace = (int*)soap_malloc(soap, sizeof(int));
                *(req.desiredLifetimeOfReservedSpace) = atoi(argv[++carg]);
                break;
                /*
            case 'i':
                req.storageSystemInfo = soap_new_srm__ArrayOfTExtraInfo(soap, -1);
                req.storageSystemInfo->value.assign(argv[++carg]);
                break;
                */
            case 'u':
                req.authorizationID = soap_new_std__string(soap, -1);
                req.authorizationID->assign(argv[++carg]);
                break;
            default:
                usage();
                exit(1);
        }
    }

#if 0
    if (parsesurl (argv[3], &srm_endpoint, &sfn) < 0) {
        perror ("parsesurl");
        exit (1);
    }
#endif


    /* To send the request ... */

    if (soap_call_srm__srmReserveSpace (soap, srm_endpoint.c_str(), "srmReserveSpace",
                &req, rep)) {
        soap_print_fault (soap, stderr);
        soap_print_fault_location (soap, stderr);
        std::cerr << "Error: " << 
            *soap_faultcode(soap) << " " <<
            *soap_faultstring(soap) << " " << 
            *soap_faultdetail(soap) << std::endl;
        soap_end (soap);
        exit (1);
    }
    reqstatp = rep.srmReserveSpaceResponse->returnStatus;
    //repfs = rep.srmCheckPermissionResponse->arrayOfPermissions;

    cout << "Request Status: " << soap_srm__TStatusCode2s(soap, reqstatp->statusCode) << endl;
    if( reqstatp->explanation )
        cout << "explanation: " << reqstatp->explanation->c_str() << endl;
    if ( rep.srmReserveSpaceResponse->requestToken )
        cout << "requestToken = " << rep.srmReserveSpaceResponse->requestToken->c_str() << endl;
    if ( rep.srmReserveSpaceResponse->estimatedProcessingTime )
        cout << "estimatedProcessingTime = " << *(rep.srmReserveSpaceResponse->estimatedProcessingTime) << endl;
    if ( rep.srmReserveSpaceResponse->retentionPolicyInfo )
        cout << "typeOfReservedSpace = " << rep.srmReserveSpaceResponse->retentionPolicyInfo->retentionPolicy << endl;
    if ( rep.srmReserveSpaceResponse->sizeOfTotalReservedSpace )
        cout << "sizeOfTotalReservedSpace = " << *(rep.srmReserveSpaceResponse->sizeOfTotalReservedSpace) << endl;
    if ( rep.srmReserveSpaceResponse->sizeOfGuaranteedReservedSpace )
        cout << "sizeOfGuaranteedReservedSpace = " << *(rep.srmReserveSpaceResponse->sizeOfGuaranteedReservedSpace) << endl;
    if ( rep.srmReserveSpaceResponse->lifetimeOfReservedSpace )
        cout << "lifetimeOfReservedSpace = " << *(rep.srmReserveSpaceResponse->lifetimeOfReservedSpace) << endl;
    if ( rep.srmReserveSpaceResponse->spaceToken )
        cout << "spaceToken = " << rep.srmReserveSpaceResponse->spaceToken->c_str() << endl;
    soap_end (soap);
    exit (0);
}
