/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testRmdir.cpp,v 1.10 2008/06/18 13:38:07 sdewitt Exp $

#include "soapH.h"
#include "srmNsmap.h"
#include "cgsi_plugin.h"

void usage() {
    std::cout << "Usage: " << std::endl;
    std::cout << "\tsrm2_testRmdir -e endPoint -s SURL (-R) (-a authorizationID) (-k key:value) (-h)" << std::endl;
    std::cout << "NOTE: The -R argument implies a request for a recursive delete." << std::endl;
}

int
main(int argc, char *argv[])
{
    srm__srmRmdirResponse_ rep;
    srm__srmRmdirRequest req;
    struct soap* soap = soap_new();
    std::string endPoint;

    req.authorizationID=0;
    req.storageSystemInfo=0;
    req.recursive=0;

    int cArg=1;
    while (cArg < argc) {
        if (std::string(argv[cArg]) == "-h") {
            usage();
            exit(0);
        }
        else if (std::string(argv[cArg]) == "-e") {
            endPoint.assign(argv[++cArg]);
        }
        else if (std::string(argv[cArg]) == "-a") {
            if (!req.authorizationID)
                req.authorizationID = soap_new_std__string(soap, -1);
            req.authorizationID->assign(argv[++cArg]);
        }
        else if (std::string(argv[cArg]) == "-R") {
            if (0 == req.recursive)
                req.recursive = (bool*)soap_malloc(soap, sizeof(bool));
            *(req.recursive) = true;
        }
        else if (std::string(argv[cArg]) == "-s") {
            req.SURL.assign(argv[++cArg]);
        }
        else if ( std::string(argv[cArg]) == "-k" ) {
            std::string arg(argv[++cArg]);
            std::string separator(":");
            size_t sepP = arg.find(separator);
            if ( sepP == std::string::npos || 
                 sepP == arg.length() ||
                 sepP == 0) {
                std::cout << "Invalid key/value pair" << std::endl;
                usage();
                exit(1);
            }
            std::string key = arg.substr(0, arg.find(separator));
            std::string val = arg.substr(arg.find(separator)+1, arg.length());
            if (!req.storageSystemInfo) req.storageSystemInfo = soap_new_srm__ArrayOfTExtraInfo(soap, -1);
            srm__TExtraInfo *xinfo = soap_new_srm__TExtraInfo(soap, -1);
            xinfo->key.assign(key);
            xinfo->value = 0;
            if ( val.length() > 0 ) {
                xinfo->value = soap_new_std__string(soap, -1);
                xinfo->value->assign(val);
            }
            req.storageSystemInfo->extraInfoArray.push_back(xinfo);
        }
        else {
            std::cout << "Invalid argument: " << argv[cArg] << std::endl;
            usage();
            exit(1);
        }
        cArg++;
    }

    if (endPoint.length()==0) {
        std::cerr << "No end point supplied." << std::endl;
        usage();
        exit (1);
    }

    soap_cgsi_init(soap, CGSI_OPT_DISABLE_NAME_CHECK|CGSI_OPT_DELEG_FLAG);
    soap_set_namespaces(soap, namespaces);


    if (soap_call_srm__srmRmdir (soap, const_cast<char*>(endPoint.c_str()), "Rmdir",
                &req, rep)) {
        soap_print_fault (soap, stderr);
        soap_print_fault_location (soap, stderr);
        soap_end (soap);
        exit (1);
    }
    
    srm__TReturnStatus* status = rep.srmRmdirResponse->returnStatus;

    std::cout << "Request Status: " << soap_srm__TStatusCode2s(soap, status->statusCode) << std::endl;
    if (status->explanation)
        std::cout << "Explanation: " << status->explanation->c_str() << std::endl;;

    soap_end (soap);
    exit (0);
}
