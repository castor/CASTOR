/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testAbortFiles.cpp,v 1.9 2007/01/16 16:45:13 sdewitt Exp $

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "soapH.h"
#include "srmNsmap.h"
#define DEFPOLLINT 10

#define SRM_EP_PATH "/v2_1_1/srm"
#include "cgsi_plugin.h"

#include "parsesurl.ic"
#include "soapcallns1.ic"

using namespace std;

void usage() {
    std::cerr << "Usage:" << std::endl;
    std::cerr << "srm2_testAbortFiles -e endPoint -r requestToken [-s SURL]+  (-a authorizationID) (-h)" << std::endl;
    return;
}


int
main(int argc, char *argv[])
{
    srm__srmAbortFilesResponse_ rep;
    srm__srmAbortFilesRequest req;
    std::string endPoint;
    struct soap* soap = soap_new();

    soap_cgsi_init(soap, CGSI_OPT_DISABLE_NAME_CHECK|CGSI_OPT_DELEG_FLAG);
    soap_set_namespaces(soap, namespaces);

    req.authorizationID = 0;
    req.arrayOfSURLs    = 0;

    int cArg=1;
    while (cArg < argc) {
        if (strcmp (argv[cArg], "-e") == 0 ) {
            endPoint.assign(argv[++cArg]);
        }
        else if (strcmp(argv[cArg],"-a") == 0) {
            if (!req.authorizationID) req.authorizationID = soap_new_std__string(soap, -1);
            req.authorizationID->assign(argv[++cArg]);
        }
        else if (strcmp(argv[cArg],"-r") == 0) {
            req.requestToken.assign(argv[++cArg]);
        }
        else if (strcmp(argv[cArg],"-s") == 0 ) {
            if ( !req.arrayOfSURLs ) req.arrayOfSURLs = soap_new_srm__ArrayOfAnyURI(soap, -1);
            req.arrayOfSURLs->urlArray.push_back(argv[++cArg]);
        }
        else if (strcmp(argv[cArg],"-h") == 0) {
            usage();
            exit(0);
        }
        cArg++;
    }

    if ( endPoint.length() == 0 ) {
        std::cerr << "Mandatory parameter endPoint missing" << std::endl;
        usage();
        exit(1);
    }

    if (soap_call_srm__srmAbortFiles (soap, endPoint.c_str(), "AbortFiles",
                &req, rep)) {
        soap_print_fault (soap, stderr);
        soap_print_fault_location (soap, stderr);
        soap_end (soap);
        exit (1);
    }

    srm__TReturnStatus *reqstatp         = rep.srmAbortFilesResponse->returnStatus;
    srm__ArrayOfTSURLReturnStatus *repfs = rep.srmAbortFilesResponse->arrayOfFileStatuses;

    std::cout << "Request status: " << soap_srm__TStatusCode2s(soap, reqstatp->statusCode) << std::endl;
    if ( reqstatp->explanation ) std::cout << "Explanation: " << reqstatp->explanation->c_str() << std::endl;
    if (repfs) {
        for (unsigned i = 0; i < repfs->statusArray.size(); i++) {
                std::cout << "\tSURL: " << repfs->statusArray[i]->surl.c_str() << std::endl;
                std::cout << "\t\tStatus: " << soap_srm__TStatusCode2s(soap, repfs->statusArray[i]->status->statusCode) << std::endl;
                if ( repfs->statusArray[i]->status->explanation ) {
                    std::cout << "\t\t Explanation: " << repfs->statusArray[i]->status->explanation->c_str() << std::endl;
                }
        }
    }
    soap_end (soap);
    exit (0);
}

