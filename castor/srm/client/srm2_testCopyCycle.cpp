/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testCopyCycle.cpp,v 1.2 2007/01/24 19:18:59 sdewitt Exp $

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef DG_DIAGNOSE
#include "diagnose/dg.h"
#endif

#include "soapH.h"
#include "srmNsmap.h"
#define DEFPOLLINT 10

#define SRM_EP_PATH "/v2_1_1/srm"
#include "cgsi_plugin.h"

#include <map>
#include "parsesurl.ic"
#include "soapcallns1.ic"

using namespace std;

char* picklocalsurl(char** argv);

int
main(int argc, char *argv[])
{
    unsigned int i;
    unsigned int nbfiles;
    int r = 0;
    struct srm__srmCopyResponse_ rep;
    struct srm__ArrayOfTCopyRequestFileStatus *repfs;
    struct srm__srmCopyRequest req;
    struct srm__TCopyFileRequest *reqfilep;
    struct srm__TReturnStatus *reqstatp;
    char *sfn;
    struct soap* soap = soap_new();
    struct srm__srmStatusOfCopyRequestResponse_ srep;
    struct srm__srmStatusOfCopyRequestRequest sreq;
    char *srm_endpoint;

    //userRequestDescription
    //char *u_token;

    if (argc < 3) {
        fprintf (stderr, "usage: %s u_token fromSURLs toSURLs\n", argv[0]);
        exit (1);
    }
    nbfiles = (argc - 2)/2;
    if ( (int)(nbfiles*2 + 2) < argc ) {
        fprintf (stderr, "usage: %s u_token fromSURLs toSURLs\n", argv[0]);
        exit (1);
    }
    // Find the surl on local host
    char* localSURL = argv[2];
    //char* localSURL = picklocalsurl(argv);
    //if (localSURL == NULL) {
    //    perror ("Could not define localSURL");
    //    exit (1);
   // }

    if (parsesurl (localSURL, &srm_endpoint, &sfn) < 0) {
        perror ("parsesurl");
        exit (1);
    }

    std::map<srm__TStatusCode, std::string> codes;
    codes[srm__TStatusCode__SRM_USCORESUCCESS] = "SRM_SUCCESS";
    codes[srm__TStatusCode__SRM_USCOREFAILURE] = "SRM_FAILURE";
    codes[srm__TStatusCode__SRM_USCOREAUTHENTICATION_USCOREFAILURE] = "SRM_AUTHENTICATION_FAILURE";
    codes[srm__TStatusCode__SRM_USCOREAUTHORIZATION_USCOREFAILURE]= "SRM_AUTHORIZATION_FAILURE";
    codes[srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST]= "SRM_INVALID_REQUEST";
    codes[srm__TStatusCode__SRM_USCOREINVALID_USCOREPATH]= "SRM_INVALID_PATH";
    codes[srm__TStatusCode__SRM_USCOREFILE_USCORELIFETIME_USCOREEXPIRED]= "SRM_FILE_LIFETIME_EXPIRED";
    codes[srm__TStatusCode__SRM_USCORESPACE_USCORELIFETIME_USCOREEXPIRED]= "SRM_SPECE_LIFETIME_EXPIRED";
    codes[srm__TStatusCode__SRM_USCOREEXCEED_USCOREALLOCATION]= "SRM_EXCEED_ALLOCATION";
    codes[srm__TStatusCode__SRM_USCORENO_USCOREUSER_USCORESPACE]= "SRM_NO_USER_SPACE";
    codes[srm__TStatusCode__SRM_USCORENO_USCOREFREE_USCORESPACE]= "SRM_NO_FREE_SPACE";
    codes[srm__TStatusCode__SRM_USCOREDUPLICATION_USCOREERROR]= "SRM_DUPLICATION_ERROR";
    codes[srm__TStatusCode__SRM_USCORENON_USCOREEMPTY_USCOREDIRECTORY]= "SRM_NON_EMPTY_DIRECTORY";
    codes[srm__TStatusCode__SRM_USCORETOO_USCOREMANY_USCORERESULTS]= "SRM_TOO_MANY_RESULTS";
    codes[srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR]= "SRM_INTERNAL_ERROR";
    codes[srm__TStatusCode__SRM_USCOREFATAL_USCOREINTERNAL_USCOREERROR]= "SRM_FATAL_INTERNAL_ERROR";
    codes[srm__TStatusCode__SRM_USCORENOT_USCORESUPPORTED]= "SRM_NOT_SUPPORTED";
    codes[srm__TStatusCode__SRM_USCOREREQUEST_USCOREQUEUED]= "SRM_REQUEST_QUEUED";
    codes[srm__TStatusCode__SRM_USCOREREQUEST_USCOREINPROGRESS]= "SRM_REQUEST_INPROGESS";
    codes[srm__TStatusCode__SRM_USCOREREQUEST_USCORESUSPENDED]= "SRM_REQUEST_SUSPENDED";
    codes[srm__TStatusCode__SRM_USCOREABORTED]= "SRM_ABORTED";
    codes[srm__TStatusCode__SRM_USCORERELEASED]= "SRM_RELEASED";
    codes[srm__TStatusCode__SRM_USCOREFILE_USCOREPINNED]= "SRM_FILE_PINNED";
    codes[srm__TStatusCode__SRM_USCOREFILE_USCOREIN_USCORECACHE]= "SRM_FILE_IN_CACHE";
    codes[srm__TStatusCode__SRM_USCORESPACE_USCOREAVAILABLE]= "SRM_SPACE_AVAILABLE";
    codes[srm__TStatusCode__SRM_USCORELOWER_USCORESPACE_USCOREGRANTED]= "SRM_LOWER_SPACE_GRANTED";
    codes[srm__TStatusCode__SRM_USCOREDONE]= "SRM_DONE";
    codes[srm__TStatusCode__SRM_USCOREPARTIAL_USCORESUCCESS]= "SRM_PARTIAL_SUCCESS";
    codes[srm__TStatusCode__SRM_USCOREREQUEST_USCORETIMED_USCOREOUT]= "SRM_REQUEST_TIMED_OUT";
    codes[srm__TStatusCode__SRM_USCORELAST_USCORECOPY]= "SRM_LAST_COPY";
    codes[srm__TStatusCode__SRM_USCOREFILE_USCOREBUSY]= "SRM_FILE_BUSY";
    codes[srm__TStatusCode__SRM_USCOREFILE_USCORELOST]= "SRM_FILE_LOST";
    codes[srm__TStatusCode__SRM_USCOREFILE_USCOREUNAVAILABLE]= "SRM_FILE_UNAVAILABLE";
    codes[srm__TStatusCode__SRM_USCORECUSTOM_USCORESTATUS]= "SRM_CUSTOM_STATUS";



    soap_cgsi_init(soap, CGSI_OPT_DISABLE_NAME_CHECK|CGSI_OPT_DELEG_FLAG);
    soap_set_namespaces(soap, namespaces);

    try {
        // Allocate request memory
	req.authorizationID = soap_new_std__string(soap, -1);
        req.arrayOfFileRequests = soap_new_srm__ArrayOfTCopyFileRequest(soap, -1);
    
        for (i = 0; i < nbfiles; i++) {
            srm__TCopyFileRequest *myInfo = soap_new_srm__TCopyFileRequest(soap, -1);
            req.arrayOfFileRequests->requestArray.push_back(myInfo);
        }
        req.userRequestDescription        = NULL;
	req.overwriteOption               = (srm__TOverwriteMode*)soap_malloc(soap, sizeof(int));
	req.desiredTotalRequestTime       = NULL;
	req.desiredTargetSURLLifeTime     = NULL;
	req.targetFileStorageType         = NULL;
	req.targetSpaceToken              = NULL;
	req.targetFileRetentionPolicyInfo = NULL;
	req.sourceStorageSystemInfo       = NULL;
        req.targetStorageSystemInfo       = NULL;
    
        for (i = 0; i < nbfiles; i++) {
            reqfilep = req.arrayOfFileRequests->requestArray[i];
	    
            reqfilep->sourceSURL.assign(argv[2*i + 2]);
            reqfilep->targetSURL.assign(argv[2*i + 3]);
	    reqfilep->dirOption = NULL;
        }
    
        if (soap_call_srm__srmCopy (soap, srm_endpoint, "Copy",
        &req, rep)) {
            soap_print_fault (soap, stderr);
            soap_print_fault_location (soap, stderr);
            soap_end (soap);
            exit (1);
        }
        reqstatp = rep.srmCopyResponse->returnStatus;
        repfs = rep.srmCopyResponse->arrayOfFileStatuses;
        if (rep.srmCopyResponse->requestToken) {
            printf ("soap_call_srm__srmCopy returned r_token %s\n", rep.srmCopyResponse->requestToken->c_str());
        }
    
//        memset (&sreq, 0, sizeof(sreq));
        sreq.requestToken.assign( rep.srmCopyResponse->requestToken->c_str() );
    
        /* wait for file "ready" */
    
        while (reqstatp->statusCode == srm__TStatusCode__SRM_USCOREREQUEST_USCOREQUEUED ||
            reqstatp->statusCode == srm__TStatusCode__SRM_USCOREREQUEST_USCOREINPROGRESS ||
	    reqstatp->statusCode == srm__TStatusCode__SRM_USCOREREQUEST_USCORESUSPENDED) {
            printf("request state %s\n", codes[reqstatp->statusCode].c_str());
            sleep ((r++ == 0) ? 1 : DEFPOLLINT);
            if (soap_call_srm__srmStatusOfCopyRequest (soap, srm_endpoint,
            "StatusOfCopyRequest", &sreq, srep)) {
                soap_print_fault (soap, stderr);
                soap_end (soap);
                exit (1);
            }
            reqstatp = srep.srmStatusOfCopyRequestResponse->returnStatus;
            repfs = srep.srmStatusOfCopyRequestResponse->arrayOfFileStatuses;
        }
        printf ("request state %s\n", codes[reqstatp->statusCode].c_str());
        if (reqstatp->statusCode != srm__TStatusCode__SRM_USCORESUCCESS &&
                reqstatp->statusCode != srm__TStatusCode__SRM_USCOREDONE) {
            if (reqstatp->explanation)
                printf ("explanation: %s\n", reqstatp->explanation->c_str());
            for (i = 0; i < repfs->statusArray.size(); i++) {
                if (repfs->statusArray[i]->targetSURL.length() > 0 ) {
                    printf ("state[%d] = %s, target SURL = %s, source SURL = %s", i,
                            codes[(repfs->statusArray[i])->status->statusCode].c_str(),
                            (repfs->statusArray[i])->targetSURL.c_str(),
                            (repfs->statusArray[i])->sourceSURL.c_str());
                    if ( (repfs->statusArray[i])->status->explanation ) {
                        printf (", explanantion = %s", (repfs->statusArray[i])->status->explanation->c_str() );
                    }
                    printf("\n");
                }
                else if ((repfs->statusArray[i])->status->explanation)
                    printf ("state[%d] = %s, explanation = %s\n", i,
                            codes[(repfs->statusArray[i])->status->statusCode].c_str(),
                            (repfs->statusArray[i])->status->explanation->c_str());
                else
                    printf ("state[%d] = %s\n", i,
                            codes[(repfs->statusArray[i])->status->statusCode].c_str());
            }
            soap_end (soap);
            exit (1);
        }
        if (! repfs) {
            printf ("arrayOfFileStatuses is NULL\n");
            soap_end (soap);
            exit (1);
        }
    
        cout << "Number of return elements = " << repfs->statusArray.size() << endl;
        for (i = 0; i < repfs->statusArray.size(); i++) {
            if (repfs->statusArray[i]->targetSURL.length() > 0 )
                printf ("state[%d] = %s, target SURL = %s\n", i,
                    codes[(repfs->statusArray[i])->status->statusCode].c_str(),
                    (repfs->statusArray[i])->targetSURL.c_str());
            else if ((repfs->statusArray[i])->status->explanation)
                printf ("state[%d] = %s, explanation = %s\n", i,
                        codes[(repfs->statusArray[i])->status->statusCode].c_str(),
                        (repfs->statusArray[i])->status->explanation->c_str());
            else
                printf ("state[%d] = %s\n", i,
                    codes[(repfs->statusArray[i])->status->statusCode].c_str());
        }
    }
    catch (...) {
        perror ("...");
    }
    soap_end (soap);
    exit (0);
}

char* picklocalsurl(char** argv)

#include <unistd.h>
#include <string.h>


{

    char name[255];
    int len = 255;

    int ret = gethostname(name, len);
    if (ret != 0) {
      return NULL;
    }

    char *start = strstr(argv[2], name);
    if (start != NULL) {
      return argv[2];
    }

    start = strstr(argv[3], name);
    if (start != NULL) {
      return argv[3];
    }

    return NULL;

}
