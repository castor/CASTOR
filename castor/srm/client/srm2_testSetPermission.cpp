/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testSetPermission.cpp,v 1.11 2008/10/15 21:19:44 sdewitt Exp $

#include "soapH.h"
#include "srmNsmap.h"

#include "cgsi_plugin.h"

void usage() {
    std::cout << "Usage:\n\t srm2_testSetPermission " << std::endl << std::endl;
    std::cout << "\t -e endPoint                The end point to cotact." << std::endl;
    std::cout << "\t -s SURL                    The SURL to change permission for." << std::endl;
    std::cout << "\t -t [add|remove|change]     Type of permission change required." << std::endl;
    std::cout << "\t (-a authorizationID )      Any required authorisation ID." << std::endl;
    std::cout << "\t (-o <mode> )               Owner permission change required." << std::endl;
    std::cout << "\t (-ui userId  -um <mode> )  Zero or more user permissions. " << std::endl;
    std::cout << "\t (-gi groupId -gm <mode> )  Zero or more group permission changes." << std::endl;
    std::cout << "\t (-w <mode> )               World permission changes." << std::endl;
    std::cout << "\t (-h )                      Prints this message." << std::endl << std::endl;;
    std::cout << "<mode> should be a value from 0 to 7 same as unix permission modes" << std::endl;
    std::cout << "Note: Additional Storage system information passing is not currently supported by this client" << std::endl;
}

    int
main(int argc, char *argv[])
{

    struct soap* soap = soap_new();
    soap_cgsi_init(soap, CGSI_OPT_DISABLE_NAME_CHECK|CGSI_OPT_DELEG_FLAG);
    soap_set_namespaces(soap, namespaces);

    srm__srmSetPermissionRequest req;
    req.authorizationID         = 0;
    req.ownerPermission         = 0;
    req.arrayOfUserPermissions  = 0;
    req.arrayOfGroupPermissions = 0;
    req.otherPermission         = 0;
    req.storageSystemInfo       = 0;
    // Parse incoming request
    std::string endPt;
    std::vector<std::string>                userList;
    std::vector<enum srm__TPermissionMode>  userPermissions;
    std::vector<std::string>                grpList;
    std::vector<enum srm__TPermissionMode>  grpPermissions;
    bool typeSupplied = false;
    int cArg=1;
    while (cArg < argc ) {
        if ( std::string(argv[cArg]) == "-e" ) {
            endPt.assign( argv[++cArg] );
        }
        else if ( std::string(argv[cArg]) == "-h" ) {
            usage();
            exit(0);
        }
        else if ( std::string(argv[cArg]) == "-s" ) {
            req.SURL.assign( argv[++cArg] );
        }
        else if ( std::string(argv[cArg]) == "-a" ) {
            if (!(req.authorizationID)) {
                req.authorizationID = soap_new_std__string(soap, -1);
                req.authorizationID->assign(argv[++cArg]);
            }
            else {
                std::cerr << "Only one authorisation identifer permitted" << std::endl;
                exit(1);
            }
        }
        else if ( std::string(argv[cArg]) == "-t" ) {
            typeSupplied = true;
            if ( std::string (argv[cArg+1]) == "add" ) {
                req.permissionType = srm__TPermissionType__ADD;
            }
            else if ( std::string (argv[cArg+1]) == "remove" ) {
                req.permissionType = srm__TPermissionType__REMOVE;
            }
            else if ( std::string (argv[cArg+1]) == "change" ) {
                req.permissionType = srm__TPermissionType__CHANGE;
            }
            else {
                usage();
                exit(1);
            }
            cArg++;
        }
        else if ( std::string(argv[cArg]) == "-o" ) {
            if ( !(req.ownerPermission) ) {
                req.ownerPermission = (enum srm__TPermissionMode*)soap_malloc(soap, sizeof(enum srm__TPermissionMode));
                int permission = atoi(argv[++cArg]);
                if ( (permission < 0) | (permission > 7) ) {
                    usage();
                    exit(1);
                }
                *(req.ownerPermission)=(srm__TPermissionMode)permission;
            }
            else {
                std::cout << "Multiple owner changes are not allowed!" << std::endl;
                exit(1);
            }
        }
        else if ( std::string(argv[cArg]) == "-ui" ) {
            userList.push_back( argv[++cArg]);
        }
        else if ( std::string(argv[cArg]) == "-um" ) {
            int permission = atoi(argv[++cArg]);
            if ( permission < 0 || permission > 7 ) {
                std::cerr << "User permissions must be in the range [0-7]" << std::endl;
                exit(1);
            }
            userPermissions.push_back((enum srm__TPermissionMode)permission);
        }
        else if ( std::string(argv[cArg]) == "-gi" ) {
            grpList.push_back( argv[++cArg]);
        }
        else if ( std::string(argv[cArg]) == "-gm" ) {
            int permission = atoi(argv[++cArg]);
            if ( permission < 0 || permission > 7 ) {
                std::cerr << "Group permissions must be in the range [0-7]" << std::endl;
                exit(1);
            }
            grpPermissions.push_back((enum srm__TPermissionMode)permission);
        }
        else if ( std::string(argv[cArg]) == "-w" ) {
            int permission = atoi(argv[++cArg]);
            if ( permission < 0 || permission > 7 ) {
                std::cerr << "Group permissions must be in the range [0-7]" << std::endl;
                exit(1);
            }

            if ( !(req.otherPermission) ) {
                req.otherPermission = (enum srm__TPermissionMode*)soap_malloc(soap, sizeof(enum srm__TPermissionMode));
                *(req.otherPermission) = (enum srm__TPermissionMode) permission;
            }
            else {
                std::cerr << "Only one world permission may be specified" << std::endl;
                exit(1);
            }
        }
        else {
            std::cerr << "Unknown argument " << argv[cArg] << std::endl;
            usage();
            exit(1);
        }
        cArg ++;
    }

    if ( userList.size() > 0 || userPermissions.size() > 0 ) {
        if ( userList.size() == userPermissions.size() ) {
            req.arrayOfUserPermissions = soap_new_srm__ArrayOfTUserPermission(soap, -1);
            for (unsigned i=0; i<userList.size(); i++) {
                srm__TUserPermission* foo = soap_new_srm__TUserPermission(soap, -1);
                foo->userID.assign(userList[i]);
                foo->mode = userPermissions[i];
                req.arrayOfUserPermissions->userPermissionArray.push_back(foo);
            }
        }
        else {
            std::cerr << "The number of users and user permissions do not match" << std::endl;
            exit(1);
        }
    }


    if ( grpList.size() > 0 || grpPermissions.size() > 0 ) {
        if ( grpList.size() == grpPermissions.size() ) {
            req.arrayOfGroupPermissions = soap_new_srm__ArrayOfTGroupPermission(soap, -1);
            for (unsigned i=0; i<grpList.size(); i++) {
                srm__TGroupPermission* foo = soap_new_srm__TGroupPermission(soap, -1);
                foo->groupID.assign(grpList[i]);
                foo->mode = grpPermissions[i];
                req.arrayOfGroupPermissions->groupPermissionArray.push_back(foo);
            }
        }
        else {
            std::cerr << "The number of groups and group permissions do not match" << std::endl;
            exit(1);
        }
    }

    if ( endPt.length() == 0 ) {
        std::cerr << "Mandatory endPoint parameter not supplied" << std::endl;
        exit(1);
    }

    if ( req.SURL.size() == 0 ) {
        std::cerr << "Mandatory SURL parameter not supplied" << std::endl;
        exit(1);
    }

    if (!typeSupplied) {
        std::cerr << "Mandatory permission type parameter not specified" << std::endl;
        exit(1);
    }

    // To help dcache we modify the end point
    endPt.replace(0, endPt.find("://"), "httpg");
    if ( endPt.find("/srm/managerv2") == std::string::npos ) {
        endPt.append("/srm/managerv2");
    }



        /* To send the request ... */
    srm__srmSetPermissionResponse_ rep;

    if (soap_call_srm__srmSetPermission (soap, endPt.c_str(), "SetPermission", &req, rep)) {
        soap_print_fault (soap, stderr);
        soap_print_fault_location (soap, stderr);
        soap_end (soap);
        exit (1);
    }


    // Output results
    srm__srmSetPermissionResponse* x = rep.srmSetPermissionResponse;
    std::cout << "Return Status: " << soap_srm__TStatusCode2s(soap, x->returnStatus->statusCode) << std::endl;
    if ( x->returnStatus->explanation && x->returnStatus->explanation->length() > 0 ) {
        std::cout << x->returnStatus->explanation->c_str() << std::endl;
    }
    

    soap_end (soap);
    exit (0);
}
