/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testStatusOfPutRequest.cpp,v 1.13 2008/01/25 17:06:30 sdewitt Exp $

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef DG_DIAGNOSE
#include "diagnose/dg.h"
#endif

#include "soapH.h"
#include "srmNsmap.h"
#define DEFPOLLINT 10

#define SRM_EP_PATH "/v2_1_1/srm"
#include "cgsi_plugin.h"

#include "parsesurl.ic"
#include "soapcallns1.ic"
#include <map>

using namespace std;

void usage() {
    cout << "Usage: " << endl;
    cout << "\"srm2_testStatusOfPutRequest\"" << std::endl;
    std::cerr << "\t -e endPoint       The end point to query (madatory)" << std::endl;
    std::cerr << "\t -r requestToken   Request token returned from PrepareToPut (madatory) " << std::endl; 
    std::cerr << "\t -a authId         Authorization ID of the user" << std::endl;
    std::cerr << "\t -s SURL           O or more SURLs associated with the request" << std::endl;
}

int CheckStatusOfRequest(struct soap *soap,
        std::string endPoint, 
        srm__srmStatusOfPutRequestRequest req) {

    srm__srmStatusOfPutRequestResponse_ resp;

    int status = -1;

    if ( soap_call_srm__srmStatusOfPutRequest(soap, endPoint.c_str(), "srmStatusOfPutRequest", &req, resp) ) {
        soap_print_fault (soap, stderr);
        soap_print_fault_location (soap, stderr);
        exit (1);
    }

    std::cout << "Status of request " << req.requestToken << ": " << soap_srm__TStatusCode2s(soap, resp.srmStatusOfPutRequestResponse->returnStatus->statusCode) << std::endl;
    if ( resp.srmStatusOfPutRequestResponse->returnStatus->explanation != NULL && resp.srmStatusOfPutRequestResponse->returnStatus->explanation->length() > 0 ) {
        std::cout << "Explanation: " << resp.srmStatusOfPutRequestResponse->returnStatus->explanation->c_str() << std::endl;
    }

    if ( resp.srmStatusOfPutRequestResponse->remainingTotalRequestTime )
        cout << "\tremainingTotalRequestTime = " << *(resp.srmStatusOfPutRequestResponse->remainingTotalRequestTime) << endl;

    if ( resp.srmStatusOfPutRequestResponse->arrayOfFileStatuses) {
        std::vector<srm__TPutRequestFileStatus*> statusVector = resp.srmStatusOfPutRequestResponse->arrayOfFileStatuses->statusArray;
        for ( unsigned int i=0; i< statusVector.size(); i++ ) {
            cout << "\t\tsiteURL = " << statusVector[i]->SURL << endl;
            if ( statusVector[i]->transferURL ) {
                cout << "\t\ttransferURL = " << statusVector[i]->transferURL->c_str() << endl;
            }
            if ( statusVector[i]->estimatedWaitTime ) {
                cout << "\t\testimatedProcessingTime = " << *(statusVector[i]->estimatedWaitTime) << endl;
            }
            if ( statusVector[i]->fileSize ) {
                cout << "\t\tfileSize = " << *(statusVector[i]->fileSize) << endl;
            }
            if ( statusVector[i]->remainingPinLifetime ) {
                cout << "\t\tremainingPinTime = " << *(statusVector[i]->remainingPinLifetime) << endl;
            }
            if ( statusVector[i]->remainingFileLifetime ) {
                cout << "\t\tremainingFileTime = " << *(statusVector[i]->remainingFileLifetime) << endl;
            }
            if ( statusVector[i]->status->statusCode ) {
                cout << "\t\tstatusCode = " << soap_srm__TStatusCode2s(soap, statusVector[i]->status->statusCode) << endl;
            }
            if ( statusVector[i]->status->explanation ) {
                cout << "\t\texplanation = " << statusVector[i]->status->explanation->c_str() << endl;
            }
        }
    }
    else {
        cout << "resp.srmStatusOfPutRequestResponse->statusArray = NULL" << endl;
        status = -1;
    }
    return status;
}


    int
main(int argc, char*argv[])
{
    struct soap* soap = soap_new();

    soap_cgsi_init(soap, CGSI_OPT_DISABLE_NAME_CHECK|CGSI_OPT_DELEG_FLAG);
    soap_set_namespaces(soap, namespaces);

    srm__srmStatusOfPutRequestRequest request;

    request.authorizationID    = NULL;
    request.arrayOfTargetSURLs = NULL;

    std::string endPoint;
    int carg = 1; // Current argument
    while (carg < argc-1) {
        if ( strcmp( argv[carg], "-r") == 0 ) {
            request.requestToken.assign(argv[++carg]);
        }
        else if ( strcmp( argv[carg], "-a" ) == 0 ) {
            request.authorizationID = soap_new_std__string(soap, -1);
            request.authorizationID->assign(argv[++carg]);
        }
        else if ( strcmp( argv[carg], "-s" ) == 0 ) {
            if ( !request.arrayOfTargetSURLs ) request.arrayOfTargetSURLs = soap_new_srm__ArrayOfAnyURI(soap, -1);
            std::string  thisSURL = argv[++carg];
            request.arrayOfTargetSURLs->urlArray.push_back(thisSURL);
        }
        else if ( strcmp( argv[carg], "-e") == 0 ) {
            endPoint.assign(argv[++carg]);
        }
        else {
            usage();
            exit (1);
        }
        carg++;
    }

    if (endPoint.length() == 0 || request.requestToken.length() == 0 ) {
        usage();
        exit(1);
    }

    CheckStatusOfRequest( soap, endPoint, request );
}
