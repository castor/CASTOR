/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// srm2_testCopy.cpp,v 1.16 2008/10/10 13:18:08 sdewitt Exp

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef DG_DIAGNOSE
#include "diagnose/dg.h"
#endif

#include "soapH.h"
#include "srmNsmap.h"
#define DEFPOLLINT 10

#define SRM_EP_PATH "/v2_1_1/srm"
#include "cgsi_plugin.h"

#include <map>
#include "parsesurl.ic"
#include "soapcallns1.ic"

using namespace std;

char* picklocalsurl(char** argv);

void usage() {
    std::cout << "srm2_testCopy " << std::endl;
    std::cout << "-e endPoint " << std::endl;
    std::cout << "[-s sourceSURL]+" << std::endl;
    std::cout << "[-t targetSURL]+ " << std::endl;
    std::cout << "(-a authorizationId) " << std::endl;
    std::cout << "(-o) " << std::endl;
    std::cout << "(-r desiredTotalRequestTime) " << std::endl;
    std::cout << "(-T desiredTargetSURLLifeTime) " << std::endl;
    std::cout << "([--volatile | --permanent | --durable]) " << std::endl;
    std::cout << "([--replica | --output | --custodial]) " << std::endl;
    std::cout << "([--online | --nearline]) " << std::endl;
    std::cout << "(-x tgtSpaceToken) " << std::endl;
    std::cout << "(-X sourceSpaceToken)" << std::endl;
}

    int
main(int argc, char *argv[])
{

    if (argc == 1) {
        usage();
        exit(0);
    }

    struct soap* soap = soap_new();
    soap_cgsi_init(soap, CGSI_OPT_DISABLE_NAME_CHECK|CGSI_OPT_DELEG_FLAG);
    soap_set_namespaces(soap, namespaces);

    srm__srmCopyRequest request;
    request.authorizationID               = NULL;
    request.arrayOfFileRequests           = soap_new_srm__ArrayOfTCopyFileRequest(soap, -1);
    request.userRequestDescription        = NULL;
    request.overwriteOption               = NULL;
    request.desiredTotalRequestTime       = NULL;
    request.desiredTargetSURLLifeTime     = NULL;
    request.targetFileStorageType         = NULL;
    request.targetSpaceToken              = NULL;
    request.targetFileRetentionPolicyInfo = NULL;
    request.sourceStorageSystemInfo       = NULL;
    request.targetStorageSystemInfo       = NULL;

    int carg=1;
    std::string currentArg;
    std::string endPoint;
    std::vector<std::string> sources;
    std::vector<std::string> targets;
    while ( carg < argc ) {
        currentArg.assign(argv[carg]);
        std::cout << "Argument: " << currentArg << std::endl;
        if (currentArg == "-e") {
            endPoint.assign(argv[++carg]);
        }
        else if (currentArg == "-s") {
            sources.push_back(argv[++carg]);
        }
        else if (currentArg == "-t") {
            targets.push_back(argv[++carg]);
        }
        else if (currentArg == "-a") {
            if (request.authorizationID) {
                std::cerr << "Error: Multiple -a argument specified" << std::endl;
                exit(1);
            }
            request.authorizationID = soap_new_std__string(soap, -1);
            request.authorizationID->assign(argv[++carg]);
        }
        else if (currentArg == "-o") {
            if (request.overwriteOption) {
                std::cerr << "Error: Multiple -o argument specified" << std::endl;
                exit(1);
            }
            request.overwriteOption = (enum srm__TOverwriteMode*)soap_malloc(soap, sizeof(enum srm__TOverwriteMode));
            *(request.overwriteOption) = srm__TOverwriteMode__ALWAYS;
        }
        else if (currentArg == "-r") {
            if (request.desiredTotalRequestTime) {
                std::cerr << "Error: Multiple -r argument specified" << std::endl;
                exit(1);
            }
            request.desiredTotalRequestTime = (int*)soap_malloc(soap, sizeof(int));
            *(request.desiredTotalRequestTime) = atoi(argv[++carg]);
        }
        else if (currentArg == "-T") {
            if (request.desiredTargetSURLLifeTime) {
                std::cerr << "Error: Multiple -T argument specified" << std::endl;
                exit(1);
            }
            request.desiredTargetSURLLifeTime = (int*)soap_malloc(soap, sizeof(int));
            *(request.desiredTargetSURLLifeTime) = atoi(argv[++carg]);
        }
        else if (currentArg == "--volatile" || currentArg == "--durable" || currentArg == "--permanent") {
            if (request.targetFileStorageType) {
                std::cerr << "Error: Multiple targetFileStorageType arguments specified" << std::endl;
                exit(1);
            }
            request.targetFileStorageType = (enum srm__TFileStorageType*)soap_malloc(soap, sizeof(enum srm__TFileStorageType));
            if ( currentArg == "--volatile" ) 
                *(request.targetFileStorageType) = srm__TFileStorageType__VOLATILE;
            else if ( currentArg == "--durable" ) 
                *(request.targetFileStorageType) = srm__TFileStorageType__DURABLE;
            else
                *(request.targetFileStorageType) = srm__TFileStorageType__PERMANENT;
        }
        else if (currentArg == "--replica" || currentArg == "--output" || currentArg == "--custodial") {
            if ( !request.targetFileRetentionPolicyInfo) {
                request.targetFileRetentionPolicyInfo = soap_new_srm__TRetentionPolicyInfo(soap, -1);
                request.targetFileRetentionPolicyInfo->accessLatency = NULL;
            }
            if ( currentArg == "--replica" )
                request.targetFileRetentionPolicyInfo->retentionPolicy = srm__TRetentionPolicy__REPLICA;
            else if ( currentArg == "--output" )
                request.targetFileRetentionPolicyInfo->retentionPolicy = srm__TRetentionPolicy__OUTPUT;
            else {
                request.targetFileRetentionPolicyInfo->retentionPolicy = srm__TRetentionPolicy__CUSTODIAL;
            }
        }
        else if (currentArg == "--online" || currentArg == "--nearline" ) {
            if ( !request.targetFileRetentionPolicyInfo) {
                request.targetFileRetentionPolicyInfo = soap_new_srm__TRetentionPolicyInfo(soap, -1);
                request.targetFileRetentionPolicyInfo->retentionPolicy = srm__TRetentionPolicy__CUSTODIAL;
                request.targetFileRetentionPolicyInfo->accessLatency = NULL;
            }
            if ( !request.targetFileRetentionPolicyInfo->accessLatency)
                request.targetFileRetentionPolicyInfo->accessLatency = (enum srm__TAccessLatency*)soap_malloc(soap, sizeof(enum srm__TAccessLatency));

            if (currentArg == "--online") 
                *(request.targetFileRetentionPolicyInfo->accessLatency) = srm__TAccessLatency__ONLINE;
            else
                *(request.targetFileRetentionPolicyInfo->accessLatency) = srm__TAccessLatency__NEARLINE;
        }
        else if (currentArg == "-x") {
            if ( request.targetSpaceToken ) {
                std::cerr << "Error: Multiple -x argument specified" << std::endl;
                exit(1);
            }
            request.targetSpaceToken = soap_new_std__string(soap, -1);
            request.targetSpaceToken->assign(argv[++carg]);
        }
        else if (currentArg == "-X") {
            if ( request.sourceStorageSystemInfo ) {
                std::cerr << "Error: Multiple -X argument specified" << std::endl;
                exit(1);
            }
            request.sourceStorageSystemInfo = soap_new_srm__ArrayOfTExtraInfo(soap,-1);
            srm__TExtraInfo *info = soap_new_srm__TExtraInfo(soap, -1);
            info->value = soap_new_std__string(soap, -1);
            info->key.assign("SourceSpaceToken");
            info->value->assign(argv[++carg]);
            request.sourceStorageSystemInfo->extraInfoArray.push_back(info);
        }
        else {
            std::cerr << "Unknown option: " << currentArg << std::endl;
            usage();
            exit (1);
            carg++;
        }
        carg++;
    }
    if ( endPoint.length() == 0 ) {
        std::cerr << "No endpoint specified" << std::endl;
        usage();
        exit(1);
    }
    if ( targets.size() == 0 || sources.size() == 0 ) {
        cout << "One or both srcs or tgts not specified" << endl;
        usage();
        exit(1);
    }
    if ( targets.size() != sources.size() ) {
        cout << "Unequal src and tgt" << endl;
        cout << "SrcSize:" << sources.size() << ", tgtSize:" << targets.size() << endl;
        usage();
        exit(1);
    }

    endPoint.replace(0, endPoint.find("://"), "httpg");
    if ( endPoint.find("/srm/managerv2") == std::string::npos ) {
        endPoint.append("/srm/managerv2");
    }

    // Fill in sources and targets
    for (unsigned i=0; i<sources.size(); i++) {
        srm__TCopyFileRequest *copyRequest = soap_new_srm__TCopyFileRequest(soap, -1);
        copyRequest->sourceSURL.assign(sources[i]);
        copyRequest->targetSURL.assign(targets[i]);
        request.arrayOfFileRequests->requestArray.push_back(copyRequest);
    }


    std::map<srm__TStatusCode, std::string> codes;
    codes[srm__TStatusCode__SRM_USCORESUCCESS] = "SRM_SUCCESS";
    codes[srm__TStatusCode__SRM_USCOREFAILURE] = "SRM_FAILURE";
    codes[srm__TStatusCode__SRM_USCOREAUTHENTICATION_USCOREFAILURE] = "SRM_AUTHENTICATION_FAILURE";
    codes[srm__TStatusCode__SRM_USCOREAUTHORIZATION_USCOREFAILURE]= "SRM_AUTHORIZATION_FAILURE";
    codes[srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST]= "SRM_INVALID_REQUEST";
    codes[srm__TStatusCode__SRM_USCOREINVALID_USCOREPATH]= "SRM_INVALID_PATH";
    codes[srm__TStatusCode__SRM_USCOREFILE_USCORELIFETIME_USCOREEXPIRED]= "SRM_FILE_LIFETIME_EXPIRED";
    codes[srm__TStatusCode__SRM_USCORESPACE_USCORELIFETIME_USCOREEXPIRED]= "SRM_SPECE_LIFETIME_EXPIRED";
    codes[srm__TStatusCode__SRM_USCOREEXCEED_USCOREALLOCATION]= "SRM_EXCEED_ALLOCATION";
    codes[srm__TStatusCode__SRM_USCORENO_USCOREUSER_USCORESPACE]= "SRM_NO_USER_SPACE";
    codes[srm__TStatusCode__SRM_USCORENO_USCOREFREE_USCORESPACE]= "SRM_NO_FREE_SPACE";
    codes[srm__TStatusCode__SRM_USCOREDUPLICATION_USCOREERROR]= "SRM_DUPLICATION_ERROR";
    codes[srm__TStatusCode__SRM_USCORENON_USCOREEMPTY_USCOREDIRECTORY]= "SRM_NON_EMPTY_DIRECTORY";
    codes[srm__TStatusCode__SRM_USCORETOO_USCOREMANY_USCORERESULTS]= "SRM_TOO_MANY_RESULTS";
    codes[srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR]= "SRM_INTERNAL_ERROR";
    codes[srm__TStatusCode__SRM_USCOREFATAL_USCOREINTERNAL_USCOREERROR]= "SRM_FATAL_INTERNAL_ERROR";
    codes[srm__TStatusCode__SRM_USCORENOT_USCORESUPPORTED]= "SRM_NOT_SUPPORTED";
    codes[srm__TStatusCode__SRM_USCOREREQUEST_USCOREQUEUED]= "SRM_REQUEST_QUEUED";
    codes[srm__TStatusCode__SRM_USCOREREQUEST_USCOREINPROGRESS]= "SRM_REQUEST_INPROGESS";
    codes[srm__TStatusCode__SRM_USCOREREQUEST_USCORESUSPENDED]= "SRM_REQUEST_SUSPENDED";
    codes[srm__TStatusCode__SRM_USCOREABORTED]= "SRM_ABORTED";
    codes[srm__TStatusCode__SRM_USCORERELEASED]= "SRM_RELEASED";
    codes[srm__TStatusCode__SRM_USCOREFILE_USCOREPINNED]= "SRM_FILE_PINNED";
    codes[srm__TStatusCode__SRM_USCOREFILE_USCOREIN_USCORECACHE]= "SRM_FILE_IN_CACHE";
    codes[srm__TStatusCode__SRM_USCORESPACE_USCOREAVAILABLE]= "SRM_SPACE_AVAILABLE";
    codes[srm__TStatusCode__SRM_USCORELOWER_USCORESPACE_USCOREGRANTED]= "SRM_LOWER_SPACE_GRANTED";
    codes[srm__TStatusCode__SRM_USCOREDONE]= "SRM_DONE";
    codes[srm__TStatusCode__SRM_USCOREPARTIAL_USCORESUCCESS]= "SRM_PARTIAL_SUCCESS";
    codes[srm__TStatusCode__SRM_USCOREREQUEST_USCORETIMED_USCOREOUT]= "SRM_REQUEST_TIMED_OUT";
    codes[srm__TStatusCode__SRM_USCORELAST_USCORECOPY]= "SRM_LAST_COPY";
    codes[srm__TStatusCode__SRM_USCOREFILE_USCOREBUSY]= "SRM_FILE_BUSY";
    codes[srm__TStatusCode__SRM_USCOREFILE_USCORELOST]= "SRM_FILE_LOST";
    codes[srm__TStatusCode__SRM_USCOREFILE_USCOREUNAVAILABLE]= "SRM_FILE_UNAVAILABLE";
    codes[srm__TStatusCode__SRM_USCORECUSTOM_USCORESTATUS]= "SRM_CUSTOM_STATUS";

    srm__srmCopyResponse_ response;
    if (soap_call_srm__srmCopy (soap, endPoint.c_str(), "Copy",
                &request, response)) {
        soap_print_fault (soap, stderr);
        soap_print_fault_location (soap, stderr);
        soap_end (soap);
        exit (1);
    }

    srm__srmCopyResponse *resp = response.srmCopyResponse;
    if (!resp) {
        std::cerr << "SOAP call returned null!" << std::endl;
        exit(1);
    }
    cout << "srmCopyRequestResponse:" << endl;
    cout << "Retuen Status: " << codes[resp->returnStatus->statusCode] << endl;
    if (resp->returnStatus->explanation)
        std::cout << "Explanation: " << resp->returnStatus->explanation->c_str() << std::endl;

    if ( resp->requestToken ) 
        std::cout << "requestToken = " << resp->requestToken->c_str() << std::endl;
    if ( resp->remainingTotalRequestTime ) 
        std::cout << "RemainingTotalRequestTime" << *(resp->remainingTotalRequestTime) << std::endl;

    srm__ArrayOfTCopyRequestFileStatus *repfs = response.srmCopyResponse->arrayOfFileStatuses;
    if ( repfs != NULL && repfs->statusArray.size() != 0 ) {
        for ( unsigned count=0; count < repfs->statusArray.size(); count++ ) {
            std::cout << "\tFile Status: " << codes[repfs->statusArray[count]->status->statusCode] << std::endl;
            std::cout << "\tsourceSURL: " << repfs->statusArray[count]->sourceSURL << std::endl;
            std::cout << "\ttargetSURL: " << repfs->statusArray[count]->targetSURL << std::endl;
            std::cout << "\tExplanation: " << repfs->statusArray[count]->status->explanation->c_str() << std::endl;
            if ( repfs->statusArray[count]->fileSize != NULL ) {
                std::cout << "\tfileSize: " << *(repfs->statusArray[count]->fileSize) << std::endl;
            }
            if ( repfs->statusArray[count]->estimatedWaitTime != NULL ) {
                std::cout << "\testimatedWaitTime: " << *(repfs->statusArray[count]->estimatedWaitTime) << std::endl;
            }
            if ( repfs->statusArray[count]->remainingFileLifetime != NULL ) {
                std::cout << "\tremainingFileLifetime: " << *(repfs->statusArray[count]->remainingFileLifetime) << std::endl;
            }
        }
    }

    soap_end (soap);
    exit (0);
}
