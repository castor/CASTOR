/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testGet.cpp,v 1.12 2007/01/16 16:45:14 sdewitt Exp $

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef DG_DIAGNOSE
#include "diagnose/dg.h"
#endif

#include "soapH.h"
#include "srmNsmap.h"
#define DEFPOLLINT 10

#define SRM_EP_PATH "/v2_1_1/srm"
#include "cgsi_plugin.h"

#include "parsesurl.ic"
#include "soapcallns1.ic"

using namespace std;

void usage() {
    cerr << "Usage:" << endl;
    cerr << "\"srm2_testGet -f <fileList> [-l <lifetimes>] [-c comment] [-p protocol] [-t spacetoken]\"" << endl;
    cerr << "\tlifetimes is comma separated std::list of lifetimes (either one or same number as number of files)" << endl;
    cerr << "\tfileList is comma separated list of SURLs" << endl;
    return;
}

std::string PrepareToGet (
        std::string endPoint,
        std::vector<std::string> files,
        std::vector<std::string> protocols,
        std::vector<int> lifetimes,
        std::string description,
        std::string spaceToken
        ) {
    srm__srmPrepareToGetRequest request;
    srm__srmPrepareToGetResponse_ response;
    struct soap* soap = soap_new();

    soap_cgsi_init(soap, CGSI_OPT_DISABLE_NAME_CHECK|CGSI_OPT_DELEG_FLAG);
    soap_set_namespaces(soap, namespaces);

    // Initialise request
    request.authorizationID               = NULL;
    request.arrayOfFileRequests           = soap_new_srm__ArrayOfTGetFileRequest(soap, -1);
    request.userRequestDescription        = NULL;
    request.storageSystemInfo             = NULL;
    request.desiredFileStorageType        = NULL;
    request.desiredTotalRequestTime       = NULL;
    request.desiredPinLifeTime            = NULL;
    request.targetSpaceToken              = NULL;
    request.targetFileRetentionPolicyInfo = NULL;
    request.transferParameters            = NULL;

    if ( lifetimes.size() > 0 ) {
        request.desiredPinLifeTime = (int*)soap_malloc(soap, sizeof(int));
        *(request.desiredPinLifeTime) = lifetimes[0];
    }

    if ( spaceToken.length() > 0 ) {
        request.targetSpaceToken = soap_new_std__string(soap, -1);
        request.targetSpaceToken->assign(spaceToken);
    }

    if ( description.length() > 0 ) {
        request.userRequestDescription = soap_new_std__string(soap, -1);
        request.userRequestDescription->assign(description);
    }

    if (protocols.size() > 0 ) {
        request.transferParameters = soap_new_srm__TTransferParameters(soap, -1);
        request.transferParameters->accessPattern            = NULL;
        request.transferParameters->connectionType           = NULL;
        request.transferParameters->arrayOfClientNetworks    = NULL;
        request.transferParameters->arrayOfTransferProtocols = soap_new_srm__ArrayOfString(soap, -1);
        for ( int i=0; i<protocols.size(); i++ ) {
            request.transferParameters->arrayOfTransferProtocols->stringArray.push_back(protocols[i]);
        }
    }
    
    if ( description.length() > 0 ) {
        request.userRequestDescription = soap_new_std__string(soap, -1);
        request.userRequestDescription->assign(description);
    }

    request.desiredTotalRequestTime = (int*)soap_malloc(soap, sizeof(int));
    *(request.desiredTotalRequestTime) = 36000;

    // Fill in all files....
    for ( int i=0; i<files.size(); i++ ) {
        srm__TGetFileRequest *fileReq = soap_new_srm__TGetFileRequest(soap, -1);
        fileReq->dirOption = NULL;
        fileReq->sourceSURL.assign(files[i]);
        request.arrayOfFileRequests->requestArray.push_back(fileReq);
    }

    // Now make the soap call...
    if ( soap_call_srm__srmPrepareToGet(soap, endPoint.c_str(), "srmPrepareToGet", &request, response) ) {
        soap_print_fault (soap, stderr);
        soap_print_fault_location (soap, stderr);
        return "";
    }

    // If we get here, print out the response structure
    cout << "srmPrepareToGetResponse:" << endl;
    cout << "Return Status: " << soap_srm__TStatusCode2s(response.srmPrepareToGetResponse->returnStatus->statusCode) << endl;
    cout << "Explanation:   " << response.srmPrepareToGetResponse->returnStatus->explanation->c_str() << endl;
    if ( response.srmPrepareToGetResponse->requestToken ) {
        cout << "Request Token = " << response.srmPrepareToGetResponse->requestToken->c_str() << endl;
    }
    if ( response.srmPrepareToGetResponse->remainingTotalRequestTime ) {
        cout << "remainingTotalRequestTime = " << *(response.srmPrepareToGetResponse->remainingTotalRequestTime) << endl;
    }
    if ( response.srmPrepareToGetResponse->returnStatus->statusCode != srm__TStatusCode__SRM_USCOREREQUEST_USCOREQUEUED ) {
        return "";
    }

    if ( response.srmPrepareToGetResponse->arrayOfFileStatuses ) {
        for ( int i=0; i<response.srmPrepareToGetResponse->arrayOfFileStatuses->statusArray.size(); i++ ) {
            cout << "File status [" << i << "]:" << endl;
            srm__TGetRequestFileStatus *fileStatus = response.srmPrepareToGetResponse->arrayOfFileStatuses->statusArray[i];
            cout << "\tsourceSURL = " << fileStatus->sourceSURL << endl;
            cout << "\tstatusCode = " << soap_srm__TStatusCode2s(soap, fileStatus->status->statusCode) << endl;
            cout << "\texplanation = " << fileStatus->status->explanation << endl;

            if ( fileStatus->fileSize ) {
                cout << "\tfileSize = " << *(fileStatus->fileSize) << endl;
            }
            else {
                cout << "\tfileSize = NULL" << endl;
            }

            if ( fileStatus->estimatedWaitTime ) {
                cout << "\testimatedWaitTime = " << *(fileStatus->estimatedWaitTime) << endl;
            }
            else {
                cout << "\testimatedWaitTime = NULL" << endl;
            }

            if ( fileStatus->remainingPinTime ) {
                cout << "\tremainingPinTime = " << fileStatus->remainingPinTime << endl;
            }
            else {
                cout << "\tremainingPinTime = NULL" << endl;
            }

            if ( fileStatus->transferURL ) {
                cout << "\ttransferURL = " << fileStatus->transferURL->c_str() << endl;
            }
            else {
                cout << "\ttransferURL = NULL" << endl;
            }

            if (fileStatus->transferProtocolInfo && fileStatus->transferProtocolInfo->extraInfoArray.size() > 0 ) {
                cout << "\ttransferProtocolInfo:" << endl;
                for ( int i=0; i<fileStatus->transferProtocolInfo->extraInfoArray.size(); i++ ) {
                    cout << "(" << fileStatus->transferProtocolInfo->extraInfoArray[i]->key << ", " << fileStatus->transferProtocolInfo->extraInfoArray[i]->value << ")" << endl;
                }
            }
            else {
                cout << "transferProtocolInfo = NULL" << endl;

            }
        }
    }
    else {
        cout << "No file statuses returned" << endl;
        return "";
    }
    return *(response.srmPrepareToGetResponse->requestToken);

}


int CheckStatus (
        std::string endpoint,
        std::string requestToken,
        std::vector<std::string> files,
        std::vector<int> lifetimes) {

    srm__srmStatusOfGetRequestRequest req;
    srm__srmStatusOfGetRequestResponse_ resp;

    // Initialise request

    req.authorizationID = NULL;
    req.requestToken.assign(requestToken);

    /*
    req.arrayOfSourceSURLs = soap_new_srm__ArrayOfAnyURI(soap, -1);
    for ( int i=0; i<files.size(); i++ ) {
        req.arrayOfSourceSURLs->urlArray.push_back(files[i]);
    }
    */
    req.arrayOfSourceSURLs = NULL;

    // Loop until we have finished....
    int count = 0;
    while (true) {

        struct soap* soap = soap_new();

        soap_cgsi_init(soap, CGSI_OPT_DISABLE_NAME_CHECK|CGSI_OPT_DELEG_FLAG);
        soap_set_namespaces(soap, namespaces);

        cout << "Calling StatusOfGetRequest" << endl;
        if ( soap_call_srm__srmStatusOfGetRequest(soap, endpoint.c_str(), "srmStatusOfGetRequest", &req, resp) ) {
            soap_print_fault (soap, stderr);
            soap_print_fault_location (soap, stderr);
            return -1;
        }

        // Output results
        cout << "Results of attempt " << count << endl;
        cout << "StatusCode = " << resp.srmStatusOfGetRequestResponse->returnStatus->statusCode << endl;
        if ( resp.srmStatusOfGetRequestResponse->returnStatus->explanation ) {
            cout << "Explanation = " << resp.srmStatusOfGetRequestResponse->returnStatus->explanation->c_str() << endl;
        }
        if ( resp.srmStatusOfGetRequestResponse->remainingTotalRequestTime ) {
            cout << "remainingTotalRequestTime = " << *(resp.srmStatusOfGetRequestResponse->remainingTotalRequestTime) << endl;
        }
        else {
            cout << "remainingTotalRequestTime = NULL" << endl;
        }
        std::vector<srm__TGetRequestFileStatus*> stati =resp.srmStatusOfGetRequestResponse->arrayOfFileStatuses->statusArray;
        int completed = 0;
        int failed = 0;
        int inprogress = 0;
        int pending = 0;
        for ( int i=0; i<stati.size(); i++ ) {
            cout << "File status [" << i << "]:" << endl;
            srm__TGetRequestFileStatus *fileStatus = stati[i];
            cout << "\tsourceSURL = " << fileStatus->sourceSURL.c_str() << endl;
            cout << "\tstatusCode = " << fileStatus->status->statusCode << endl;
            cout << "\texplanation = " << fileStatus->status->explanation->c_str() << endl;
            if ( fileStatus->estimatedWaitTime) {
                cout << "\testimatedWaitTime = " << *(fileStatus->estimatedWaitTime) << endl;
            }
            else {
                cout << "\testimatedWaitTime = NULL" << endl;
            }

            if ( fileStatus->fileSize ) {
                cout << "\tfileSize = " << *(fileStatus->fileSize) << endl;
            }
            else {
                cout << "\tfileSize = NULL" << endl;
            }

            if ( fileStatus->remainingPinTime ) {
                cout << "\tremainingPinTime = " << *(fileStatus->remainingPinTime) << endl;
            }
            else {
                cout << "\tremainingPinTime = NULL" << endl;
            }

            if ( fileStatus->transferURL ) {
                cout << "\ttransferURL = " << fileStatus->transferURL->c_str() << endl;
            }
            else {
                cout << "\ttransferURL = NULL" << endl;
            }

            if ( fileStatus->transferProtocolInfo && fileStatus->transferProtocolInfo->extraInfoArray.size() > 0 ) {
                cout << "\ttransferProtocolInfo:" << endl;
                for ( int i=0; i<fileStatus->transferProtocolInfo->extraInfoArray.size() ; i++ ) {
                }
            }
            else {
                cout << "\ttransferProtocolInfo = NULL" << endl;
            }

            switch (fileStatus->status->statusCode) {
                case srm__TStatusCode__SRM_USCOREREQUEST_USCOREQUEUED:
                    pending++;
                    break;
                case srm__TStatusCode__SRM_USCOREREQUEST_USCOREINPROGRESS:
                    inprogress++;
                    break;
                case srm__TStatusCode__SRM_USCOREDONE:
                    completed++;
                    break;
                case srm__TStatusCode__SRM_USCOREFAILURE:
                default:
                    failed++;
                    break;
            }
        }
        cout << completed << "/" << stati.size() << "completed" << endl;
        cout << failed << "/" << stati.size() << "failed" << endl;
        cout << pending << "/" << stati.size() << "pending" << endl;
        cout << inprogress << "/" << stati.size() << "inprogress" << endl;
        if ( (completed + failed) == stati.size() ) break;
        sleep(10);
        count++;
    }
    return 0;
}

int
main(int argc, char *argv[])
{
    int flags;
    int i;
    int nbfiles;
    int nbproto = 0;
    char** lprotos;
    int r = 0;
    char *r_token;
    char *sfn;
    char *srm_endpoint;


    //    nbproto = 1;
    /*
       lprotos = calloc (nbproto, sizeof(char*));
       lprotos[0] = strdup(argv[1]);
     */


    // Process arguments
    int currentArg = 1;
    std::string comment;
    std::string spacetoken;
    std::vector<std::string> protocols;
    std::vector<std::string> files;
    std::vector<int> lifetimes;
    if (argc < 3 ) {
        usage();
        exit(1);
    }
    while ( currentArg < argc ) {
        if ( strcmp( argv[currentArg], "-p" ) == 0 ) {
            if (argv[currentArg+1][0] == '-' ) {
                usage();
                exit(1);
            }
            protocols.push_back(argv[++currentArg]);
        }
        else if ( strcmp( argv[currentArg], "-c") == 0 ) {
            if (argv[currentArg+1][0] == '-' ) {
                usage();
                exit(1);
            }
            comment.assign(argv[++currentArg]);
        }
        else if ( strcmp( argv[currentArg], "-t" ) == 0 ) {
            if (argv[currentArg+1][0] == '-' ) {
                usage();
                exit(1);
            }
            spacetoken.assign(argv[++currentArg]);
        }
        else if ( strcmp( argv[currentArg], "-l" ) == 0 ) {
            if (argv[currentArg+1][0] == '-' ) {
                usage();
                exit(1);
            }
            char *pEnd;
            pEnd = argv[currentArg+1];
            while ( *pEnd != '\0' ) {
                int lifetime = strtol(argv[currentArg+1], &pEnd, 0 );
                lifetimes.push_back(lifetime);
            }
            currentArg++;
        }
        else if ( strcmp( argv[currentArg], "-f" ) == 0 ) {
            if (argv[currentArg+1][0] == '-' ) {
                usage();
                exit(1);
            }
            char *file = strtok(argv[currentArg+1], ",");
            while ( file != NULL ) {
                files.push_back(file);
                file = strtok(NULL, ",");
            }
        }
        currentArg++;
    }

    if (parsesurl (files[0].c_str(), &srm_endpoint, &sfn) < 0) {
        perror ("parsesurl");
        exit (1);
    }

    time_t now = time(NULL);
    cout << "Test Started: " << ctime(&now) << endl;
    std::string reqtoken = PrepareToGet( 
            srm_endpoint,
            files,
            protocols,
            lifetimes,
            comment,
            spacetoken);

    if ( reqtoken.size() > 0 ) {
        int status = CheckStatus( 
                srm_endpoint,
                reqtoken,
                files,
                lifetimes );
    }
    now = time(NULL);
    cout << "Test Stopped: " << ctime(&now) << endl;
    exit (0);
}
