/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testPutDone.cpp,v 1.6 2008/01/25 17:06:30 sdewitt Exp $

#include "soapH.h"
#include "srmNsmap.h"
//#include "srmReturnCodes.hpp"

#include "cgsi_plugin.h"

void usage() {
    std::cerr << "Usage:" << std::endl;
    std::cerr << "\t srm2_testPutDone" << std::endl << "with the following arguments:" << std::endl;
    std::cerr << "\t\t -e       srm end point(*)" << std::endl;
    std::cerr << "\t\t -r       Request token from previous put request (*)." << std::endl;
    std::cerr << "\t\t -a       Authorisation ID if required" << std::endl;
    std::cerr << "\t\t -s       SURL - repeated for ech SURL required" << std::endl;
    std::cerr << "Arguments marked (*) are mandatory" << std::endl;
}

    int
main(int argc, char *argv[])
{
    struct soap* soap = soap_new();
    struct srm__srmStatusOfGetRequestRequest sreq;


    soap_cgsi_init(soap, CGSI_OPT_DISABLE_NAME_CHECK|CGSI_OPT_DELEG_FLAG);
    soap_set_namespaces(soap, namespaces);

    srm__srmPutDoneRequest req;
    req.authorizationID = NULL;
    req.arrayOfSURLs    = NULL;

    std::string endPoint;

    int cArg = 1;
    while ( cArg < argc ) {
        if ( strcmp( argv[cArg], "-e" ) == 0 ) {
            endPoint.assign(argv[++cArg]);
        }
        else if ( strcmp(argv[cArg], "-r") == 0 ) {
            req.requestToken.assign(argv[++cArg]);
        }
        else if (strcmp(argv[cArg], "-a") == 0 ) {
            if (!req.authorizationID) req.authorizationID = soap_new_std__string(soap, -1);
            req.authorizationID->assign(argv[++cArg]);
        }
        else if (strcmp(argv[cArg], "-s") == 0 ) {
            if (!req.arrayOfSURLs) req.arrayOfSURLs = soap_new_srm__ArrayOfAnyURI(soap, -1);
            req.arrayOfSURLs->urlArray.push_back(argv[++cArg]);
        }
        else {
            usage();
            exit(1);
        }
        cArg++;
    }

    if (endPoint.length() == 0 ) {
        usage();
        exit(1);
    }

    srm__srmPutDoneResponse_ rep;

    if (soap_call_srm__srmPutDone (soap, endPoint.c_str(), "PutDone",
                &req, rep)) {
        soap_print_fault (soap, stderr);
        soap_print_fault_location (soap, stderr);
        soap_end (soap);
        exit (1);
    }
    
    srm__TReturnStatus *reqstatp         = rep.srmPutDoneResponse->returnStatus;
    srm__ArrayOfTSURLReturnStatus *repfs = rep.srmPutDoneResponse->arrayOfFileStatuses;

    //reqstatp = rep.srmPutDoneResponse->returnStatus;
    //repfs    = rep.srmPutDoneResponse->arrayOfFileStatuses;

    //std::cout << "Request status: " << castor::srm::client::srmReturnCodes::toString(reqstatp->statusCode) << std::endl;
    std::cout << "Request status: " << soap_srm__TStatusCode2s(soap, reqstatp->statusCode) << std::endl;
    if (reqstatp->explanation)
        std::cerr << "Explanation: " << reqstatp->explanation->c_str() << std::endl;;

    if (repfs) {
        for (unsigned i = 0; i < repfs->statusArray.size(); i++) {
            std::cerr << "\t SURL: " << repfs->statusArray[i]->surl.c_str() << std::endl;
            std::cerr << "\t\t Status: " << soap_srm__TStatusCode2s(soap, repfs->statusArray[i]->status->statusCode) << std::endl;;
            if ( repfs->statusArray[i]->status->explanation) 
                std::cerr << "\t\t Explanation: " << repfs->statusArray[i]->status->explanation->c_str() << std::endl;
        }
    }
}
