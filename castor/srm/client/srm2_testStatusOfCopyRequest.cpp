/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testStatusOfCopyRequest.cpp,v 1.3 2009/04/23 13:45:49 sdewitt Exp $

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef DG_DIAGNOSE
#include "diagnose/dg.h"
#endif

#include "soapH.h"
#include "srmNsmap.h"
#define DEFPOLLINT 10

#define SRM_EP_PATH "/v2_1_1/srm"
#include "cgsi_plugin.h"

#include <map>
#include "parsesurl.ic"
#include "soapcallns1.ic"

using namespace std;

char* picklocalsurl(char** argv);

int
main(int argc, char *argv[])
{
    unsigned int i;
    struct srm__ArrayOfTCopyRequestFileStatus *repfs;
    struct srm__srmCopyRequest req;
    struct srm__TReturnStatus *reqstatp;
    struct soap* soap = soap_new();

    //userRequestDescription
    //char *u_token;

    if (argc < 3) {
        fprintf (stderr, "usage: %s -r requestToken -e endPoint [-f fromSURLs] [-t toSURLs]\n", argv[0]);
        exit (1);
    }

    std::string token;
    std::string endPoint;
    std::vector<std::string> fromList;
    std::vector<std::string> toList;
    int carg = 1;
    while (carg < argc ) {
        switch (argv[carg][1]) {
            case 'r':
                token.assign(argv[++carg]);
                break;
            case 'e':
                endPoint.assign(argv[++carg]);
                break;
            case 'f':
                fromList.push_back(argv[++carg]);
                break;
            case 't':
                toList.push_back(argv[++carg]);
                break;
        }
        carg++;
    }

    if ( token.length() == 0 ) {
        std::cerr << "No request token supplied" << endl;
        exit(1);
    }
    if ( endPoint.length() == 0 ) {
        std::cerr << "No endPoint supplied" << endl;
        exit(1);
    }

    if ( fromList.size() != toList.size() ) {
        std::cerr << "Unequal from/to lists" << std::endl;
        exit(1);
    }

    std::map<srm__TStatusCode, std::string> codes;
    codes[srm__TStatusCode__SRM_USCORESUCCESS] = "SRM_SUCCESS";
    codes[srm__TStatusCode__SRM_USCOREFAILURE] = "SRM_FAILURE";
    codes[srm__TStatusCode__SRM_USCOREAUTHENTICATION_USCOREFAILURE] = "SRM_AUTHENTICATION_FAILURE";
    codes[srm__TStatusCode__SRM_USCOREAUTHORIZATION_USCOREFAILURE]= "SRM_AUTHORIZATION_FAILURE";
    codes[srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST]= "SRM_INVALID_REQUEST";
    codes[srm__TStatusCode__SRM_USCOREINVALID_USCOREPATH]= "SRM_INVALID_PATH";
    codes[srm__TStatusCode__SRM_USCOREFILE_USCORELIFETIME_USCOREEXPIRED]= "SRM_FILE_LIFETIME_EXPIRED";
    codes[srm__TStatusCode__SRM_USCORESPACE_USCORELIFETIME_USCOREEXPIRED]= "SRM_SPECE_LIFETIME_EXPIRED";
    codes[srm__TStatusCode__SRM_USCOREEXCEED_USCOREALLOCATION]= "SRM_EXCEED_ALLOCATION";
    codes[srm__TStatusCode__SRM_USCORENO_USCOREUSER_USCORESPACE]= "SRM_NO_USER_SPACE";
    codes[srm__TStatusCode__SRM_USCORENO_USCOREFREE_USCORESPACE]= "SRM_NO_FREE_SPACE";
    codes[srm__TStatusCode__SRM_USCOREDUPLICATION_USCOREERROR]= "SRM_DUPLICATION_ERROR";
    codes[srm__TStatusCode__SRM_USCORENON_USCOREEMPTY_USCOREDIRECTORY]= "SRM_NON_EMPTY_DIRECTORY";
    codes[srm__TStatusCode__SRM_USCORETOO_USCOREMANY_USCORERESULTS]= "SRM_TOO_MANY_RESULTS";
    codes[srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR]= "SRM_INTERNAL_ERROR";
    codes[srm__TStatusCode__SRM_USCOREFATAL_USCOREINTERNAL_USCOREERROR]= "SRM_FATAL_INTERNAL_ERROR";
    codes[srm__TStatusCode__SRM_USCORENOT_USCORESUPPORTED]= "SRM_NOT_SUPPORTED";
    codes[srm__TStatusCode__SRM_USCOREREQUEST_USCOREQUEUED]= "SRM_REQUEST_QUEUED";
    codes[srm__TStatusCode__SRM_USCOREREQUEST_USCOREINPROGRESS]= "SRM_REQUEST_INPROGESS";
    codes[srm__TStatusCode__SRM_USCOREREQUEST_USCORESUSPENDED]= "SRM_REQUEST_SUSPENDED";
    codes[srm__TStatusCode__SRM_USCOREABORTED]= "SRM_ABORTED";
    codes[srm__TStatusCode__SRM_USCORERELEASED]= "SRM_RELEASED";
    codes[srm__TStatusCode__SRM_USCOREFILE_USCOREPINNED]= "SRM_FILE_PINNED";
    codes[srm__TStatusCode__SRM_USCOREFILE_USCOREIN_USCORECACHE]= "SRM_FILE_IN_CACHE";
    codes[srm__TStatusCode__SRM_USCORESPACE_USCOREAVAILABLE]= "SRM_SPACE_AVAILABLE";
    codes[srm__TStatusCode__SRM_USCORELOWER_USCORESPACE_USCOREGRANTED]= "SRM_LOWER_SPACE_GRANTED";
    codes[srm__TStatusCode__SRM_USCOREDONE]= "SRM_DONE";
    codes[srm__TStatusCode__SRM_USCOREPARTIAL_USCORESUCCESS]= "SRM_PARTIAL_SUCCESS";
    codes[srm__TStatusCode__SRM_USCOREREQUEST_USCORETIMED_USCOREOUT]= "SRM_REQUEST_TIMED_OUT";
    codes[srm__TStatusCode__SRM_USCORELAST_USCORECOPY]= "SRM_LAST_COPY";
    codes[srm__TStatusCode__SRM_USCOREFILE_USCOREBUSY]= "SRM_FILE_BUSY";
    codes[srm__TStatusCode__SRM_USCOREFILE_USCORELOST]= "SRM_FILE_LOST";
    codes[srm__TStatusCode__SRM_USCOREFILE_USCOREUNAVAILABLE]= "SRM_FILE_UNAVAILABLE";
    codes[srm__TStatusCode__SRM_USCORECUSTOM_USCORESTATUS]= "SRM_CUSTOM_STATUS";



    soap_cgsi_init(soap, CGSI_OPT_DISABLE_NAME_CHECK|CGSI_OPT_DELEG_FLAG);
    soap_set_namespaces(soap, namespaces);

    try {
        srm__srmStatusOfCopyRequestResponse_ srep;
        srm__srmStatusOfCopyRequestRequest   sreq;
    
        sreq.requestToken.assign(token);
        if ( fromList.size() > 0 ) {
            sreq.arrayOfSourceSURLs = soap_new_srm__ArrayOfAnyURI(soap, -1);
            sreq.arrayOfTargetSURLs = soap_new_srm__ArrayOfAnyURI(soap, -1);
            for ( unsigned nFiles=0; nFiles < fromList.size(); nFiles++ ) {
                sreq.arrayOfSourceSURLs->urlArray.push_back(fromList[nFiles]);
                sreq.arrayOfTargetSURLs->urlArray.push_back(toList[nFiles]);
            }
        }
        if (soap_call_srm__srmStatusOfCopyRequest (soap, endPoint.c_str(),
                    "StatusOfCopyRequest", &sreq, srep)) {
            soap_print_fault (soap, stderr);
            soap_end (soap);
            exit (1);
        }
        reqstatp = srep.srmStatusOfCopyRequestResponse->returnStatus;
        repfs = srep.srmStatusOfCopyRequestResponse->arrayOfFileStatuses;

        cout << "StatusOfCopy for request " << sreq.requestToken << ":" << endl;
        cout << "StatusCode = " << soap_srm__TStatusCode2s(soap, reqstatp->statusCode) << endl;
        if ( reqstatp->explanation ) 
            std::cout << "Explanation = " << reqstatp->explanation->c_str() << std::endl;
        if ( repfs ) {
            for (i = 0; i < repfs->statusArray.size(); i++) {
                std::cout << "\tsourceSURL[" << i << "] = " << repfs->statusArray[i]->sourceSURL << std::endl;
                std::cout << "\ttargetSURL[" << i << "] = " << repfs->statusArray[i]->targetSURL << std::endl;
                std::cout << "\tstatusCode[" << i << "] = " << soap_srm__TStatusCode2s(soap, repfs->statusArray[i]->status->statusCode) << std::endl;
                if ( (repfs->statusArray[i])->status->explanation ) {
                    std::cout << "\texplanation[" << i << "] = " << repfs->statusArray[i]->status->explanation->c_str() << std::endl;
                }
            }
            soap_end (soap);
            exit (1);
        }
    
    }
    catch (...) {
        perror ("...");
    }
    soap_end (soap);
    exit (0);
}

char* picklocalsurl(char** argv)

#include <unistd.h>
#include <string.h>


{

    char name[255];
    int len = 255;

    int ret = gethostname(name, len);
    if (ret != 0) {
      return NULL;
    }

    char *start = strstr(argv[2], name);
    if (start != NULL) {
      return argv[2];
    }

    start = strstr(argv[3], name);
    if (start != NULL) {
      return argv[3];
    }

    return NULL;

}
