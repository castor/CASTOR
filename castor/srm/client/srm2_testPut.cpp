/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testPut.cpp,v 1.30 2007/02/27 15:17:32 sdewitt Exp $

#include <stdio.h>
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef DG_DIAGNOSE
#include "diagnose/dg.h"
#endif

#include "soapH.h"
#include "srmNsmap.h"
#define DEFPOLLINT 10

#define SRM_EP_PATH "/v2_1_1/srm"
#include "cgsi_plugin.h"

#include "parsesurl.ic"
#include "soapcallns1.ic"

using namespace std;

void usage() {
    cout << "Usage: " << endl;
    cout << "\"srm2_testPut " <<
        "-f <fileList> " <<
        "[-u authorizationID] " <<
        "[-c comment] " <<
        "[-o 0/1] " <<
        "[-r retryLifetime] " <<
        "[-l pinLifetime] " <<
        "[-L fileLifetime] " <<
        "[-p <protocollist>] " <<
        "[-s <sizeList>] " <<
        "[-t <spacetokens>]\"" << endl;
}

bool validArg (char *arg) {
    bool valid = true;
    if ( arg[0] == '-' ) {
        valid = false;
        cerr << "Validation failed on argument" << arg << endl;
    }
    return valid;
}

std::string PrepareRequest(struct soap *soap, 
        std::string endPoint,
        std::string user,
        std::string userComment,
        std::vector<std::string> clientprotocols,
        bool,
        int retryLifetime,
        std::vector<int> lifetimeList,
        std::vector<int> sizeList,
        std::vector<std::string> spaceTokenList, 
        std::vector<std::string> fileList) {
    srm__srmPrepareToPutRequest req;
    srm__srmPrepareToPutResponse_ rtn;
    std::string response;


    // Initialise structure
    req.authorizationID               = NULL;
    req.arrayOfFileRequests           = NULL;
    req.userRequestDescription        = NULL;
    req.overwriteOption               = NULL;
    req.storageSystemInfo             = NULL;
    req.desiredTotalRequestTime       = NULL;
    req.desiredPinLifeTime            = NULL;
    req.desiredFileLifeTime           = NULL;
    req.desiredFileStorageType        = NULL;
    req.targetSpaceToken              = NULL;
    req.targetFileRetentionPolicyInfo = NULL;
    req.transferParameters            = NULL;

    try {
        req.desiredTotalRequestTime = (int*)soap_malloc(soap, sizeof(int));
        *(req.desiredTotalRequestTime) = retryLifetime;
        // Allocate memory
        if ( user.length() > 0 ) {
            req.authorizationID = soap_new_std__string(soap, -1);
            req.authorizationID->assign(user);
        }
        if ( userComment.length() > 0 ) {
            req.userRequestDescription = soap_new_std__string(soap, -1);
            req.userRequestDescription->assign(userComment);
        }
        if ( lifetimeList.size() > 0 ) {
            req.desiredPinLifeTime = (int*)soap_malloc(soap, sizeof(int));
            *(req.desiredPinLifeTime) = lifetimeList[0];
        }
        if ( spaceTokenList.size() > 0 ) {
            req.targetSpaceToken = soap_new_std__string(soap, -1);
            req.targetSpaceToken->assign(spaceTokenList[0]);
        }
        if ( fileList.size() > 0 ) {
            req.arrayOfFileRequests = soap_new_srm__ArrayOfTPutFileRequest(soap, -1);
            for (unsigned int i=0; i<fileList.size(); i++ ) {
                srm__TPutFileRequest *thisRequest = soap_new_srm__TPutFileRequest(soap, -1);
                thisRequest->targetSURL = soap_new_std__string(soap, -1);
                thisRequest->targetSURL->assign(fileList[i]);
                thisRequest->expectedFileSize = NULL;
                if ( sizeList.size() > 0 ) {
                    thisRequest->expectedFileSize = (ULONG64*)soap_malloc(soap, sizeof(ULONG64));
                    *(thisRequest->expectedFileSize) = sizeList[i];
                }
                req.arrayOfFileRequests->requestArray.push_back(thisRequest);
            }
        }

        if ( clientprotocols.size() > 0 ) {
            req.transferParameters = soap_new_srm__TTransferParameters(soap, -1);
            req.transferParameters->accessPattern  = NULL;
            req.transferParameters->connectionType = NULL;
            req.transferParameters->arrayOfClientNetworks = NULL;
            req.transferParameters->arrayOfTransferProtocols = soap_new_srm__ArrayOfString(soap, -1);
            for (unsigned int i=0; i<clientprotocols.size(); i++ ) {
                req.transferParameters->arrayOfTransferProtocols->stringArray.push_back(clientprotocols[i]);
            }
        }

        // Make the soap call...
        if ( soap_call_srm__srmPrepareToPut (soap, endPoint.c_str(), "srmPrepareToPut", &req, rtn) ) {
            soap_print_fault (soap, stderr);
            soap_print_fault_location (soap, stderr);
            return response;
        }

        // Check the response
        if ( rtn.srmPrepareToPutResponse == NULL) {
            cerr << "SOAP call returned null!" << endl;
            return response;
        }
        else if ( rtn.srmPrepareToPutResponse->returnStatus == NULL) {
            cerr << "rtn.srmPrepareToPutResponse->returnStatus == NULL !" << endl;
            return response;
        }
        else {
            // Print return structure....
            if ( rtn.srmPrepareToPutResponse->returnStatus->statusCode ) {
                cout << "Status: " << rtn.srmPrepareToPutResponse->returnStatus->statusCode << endl;
	    }

            if ( rtn.srmPrepareToPutResponse->returnStatus->explanation ) {
                cout << "Explanation" << rtn.srmPrepareToPutResponse->returnStatus->explanation->c_str() << endl;
	    }

            if ( rtn.srmPrepareToPutResponse->requestToken ) {
                cout << "\trtn.srmPrepareToPutResponse->requestToken = " <<
                    rtn.srmPrepareToPutResponse->requestToken->c_str() << endl;
                response.assign(rtn.srmPrepareToPutResponse->requestToken->data());
            }
            else {
                cout << "\trtn.srmPrepareToPutResponse->requestToken = NULL" << endl;
            }

            if ( rtn.srmPrepareToPutResponse->remainingTotalRequestTime ) {
                cout << "\trtn.srmPrepareToPutResponse->remainingTotalRequestTime = " <<
                    *(rtn.srmPrepareToPutResponse->remainingTotalRequestTime) << endl;
            }
            else {
                cout << "\trtn.srmPrepareToPutResponse->remainingTotalRequestTime = NULL" << endl;
            }

            if ( rtn.srmPrepareToPutResponse->arrayOfFileStatuses ) {
                std::vector<srm__TPutRequestFileStatus*> statusVector = rtn.srmPrepareToPutResponse->arrayOfFileStatuses->statusArray;
                for (unsigned int i=0; i< statusVector.size(); i++ ) {
                    cout << "\tFile[" << i << "]:" << endl;
                    cout << "\t\tsiteURL = " << statusVector[i]->SURL << endl;
                    if ( statusVector[i]->transferURL ) {
                        cout << "\t\ttransferURL = " << statusVector[i]->transferURL<< endl;
                    }
                    if ( statusVector[i]->estimatedWaitTime ) {
                        cout << "\t\testimatedWaitTime = " << *(statusVector[i]->estimatedWaitTime) << endl;
                    }
                    if ( statusVector[i]->fileSize ) {
                        cout << "\t\tfileSize = " << *(statusVector[i]->fileSize) << endl;
                    }
                    if ( statusVector[i]->remainingPinLifetime ) {
                        cout << "\t\tremainingPinTime = " << *(statusVector[i]->remainingPinLifetime) << endl;
                    }
                    if ( statusVector[i]->remainingFileLifetime ) {
                        cout << "\t\tremainingFileTime = " << *(statusVector[i]->remainingFileLifetime) << endl;
                    }
                    if ( statusVector[i]->status->statusCode ) {
		        cout << "\t\tstatusCode = " << statusVector[i]->status->statusCode << endl;
	            }
                    if ( statusVector[i]->status->explanation ) {
                        cout << "\t\texplanation = " << statusVector[i]->status->explanation->c_str() << endl;
		    }
                }

            }
            else {
                cout << "\trtn.srmPrepareToPutResponse->arrayOfFileStatuses = NULL" << endl;
            }
        }
    }
    catch (...) {
        cout << "Unknown exception!" << endl;
        return response;
    }

    return response;
}

int CheckStatusOfRequest(struct soap *soap,
        std::string endPoint, 
        std::string requestToken,
        std::string user,
        std::vector<std::string>, 
        std::string opfile) {

    srm__srmStatusOfPutRequestRequest req;
    srm__srmStatusOfPutRequestResponse_ resp;

    int status = -1;


    // Initialise request
    req.requestToken.assign(requestToken);
    req.authorizationID    = NULL;
    req.arrayOfTargetSURLs = NULL;

    try {
        // Allocate request memory (or set to NULL)
        if ( user.length() > 0 ) {
            req.authorizationID = soap_new_std__string(soap, -1);
            req.authorizationID->assign(user);
        }

        /*
        if ( files.size() > 0 ) {
            req.arrayOfTargetSURLs = soap_new_srm__ArrayOfAnyURI(soap, -1);
            for ( int i=0; i<files.size(); i++ ) {
                req.arrayOfTargetSURLs->urlArray.push_back(files[i]);
            }
        }
        */

        // Make the call and loop for a while until we get a good response
        int attempt = 0;
        while (attempt < 5) {
            cout << "Calling srmStatusOfPutRequest..." << endl;
            if ( soap_call_srm__srmStatusOfPutRequest(soap, endPoint.c_str(), "srmStatusOfPutRequest", &req, resp) ) {
                soap_print_fault (soap, stderr);
                soap_print_fault_location (soap, stderr);
                status = -1;
                break;
            }
            int rtnCode = resp.srmStatusOfPutRequestResponse->returnStatus->statusCode;
            if ( rtnCode == srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR ||
                 rtnCode == srm__TStatusCode__SRM_USCOREFATAL_USCOREINTERNAL_USCOREERROR ) {
                cout << "Internal SRM error encountered" << endl;
                status = -1;
                break;
            }

            // Now loop through all files to check their status...
            unsigned int success = 0;
            unsigned int failed  = 0;
            unsigned int pending = 0;
            unsigned int inProgress = 0;
            for (unsigned int i=0; i<resp.srmStatusOfPutRequestResponse->arrayOfFileStatuses->statusArray.size(); i++ ) {
                switch ( resp.srmStatusOfPutRequestResponse->arrayOfFileStatuses->statusArray[i]->status->statusCode ) {
                    case srm__TStatusCode__SRM_USCOREREQUEST_USCOREQUEUED:
                        pending++;
                        break;
                    case srm__TStatusCode__SRM_USCOREREQUEST_USCOREINPROGRESS:
                        inProgress++;
                        break;
                    case srm__TStatusCode__SRM_USCOREDONE:
                    case srm__TStatusCode__SRM_USCORESUCCESS:
                    case srm__TStatusCode__SRM_USCORESPACE_USCOREAVAILABLE:
                        success++;
                        break;
                    case srm__TStatusCode__SRM_USCOREFAILURE:
                        failed++;
                        break;
                    default:
                        cout << "Unexpected status code: " << resp.srmStatusOfPutRequestResponse->arrayOfFileStatuses->statusArray[i]->status->statusCode << endl;
                        failed++;
                        break;
                }
            }
            cout << "Attempt " << attempt << " results..." << endl;
            cout << success    << "/" << resp.srmStatusOfPutRequestResponse->arrayOfFileStatuses->statusArray.size() << " succeeded" << endl;
            cout << failed     << "/" << resp.srmStatusOfPutRequestResponse->arrayOfFileStatuses->statusArray.size() << " failed" << endl;
            cout << pending    << "/" << resp.srmStatusOfPutRequestResponse->arrayOfFileStatuses->statusArray.size() << " failed" << endl;
            cout << inProgress << "/" << resp.srmStatusOfPutRequestResponse->arrayOfFileStatuses->statusArray.size() << " failed" << endl;
            if ( (failed + success) == resp.srmStatusOfPutRequestResponse->arrayOfFileStatuses->statusArray.size() ) {
                status = success;
                break;
            }
            else {
                attempt++;
                sleep(5);
            }
        }

        // So we have some status by now - lets print out the results...
        cout << "Results of srmStatusOfPutRequest: " << endl;
	if ( resp.srmStatusOfPutRequestResponse->returnStatus->statusCode ) {
	    cout << "\tStatusCode = " << resp.srmStatusOfPutRequestResponse->returnStatus->statusCode << endl;
	}
	if ( resp.srmStatusOfPutRequestResponse->returnStatus->explanation ) {
	    cout << "\tExplanation = " << resp.srmStatusOfPutRequestResponse->returnStatus->explanation->c_str() << endl;
	}
        if ( resp.srmStatusOfPutRequestResponse->arrayOfFileStatuses ) {
            std::vector<srm__TPutRequestFileStatus*> statusVector = resp.srmStatusOfPutRequestResponse->arrayOfFileStatuses->statusArray;
            for (unsigned int i=0; i< statusVector.size(); i++ ) {
                cout << "\tFile[" << i << "]:" << endl;
                cout << "\t\tSURL = " << statusVector[i]->SURL << endl;
                if ( statusVector[i]->transferURL ) {
                    cout << "\t\ttransferURL = " << statusVector[i]->transferURL->c_str() << endl;
                    FILE* file = fopen(opfile.c_str(), "a");
                    fprintf(file, "%s\n", statusVector[i]->transferURL->c_str());
                    fclose(file);
                }
                if ( statusVector[i]->estimatedWaitTime ) {
                    cout << "\t\testimatedWaitTime = " << *(statusVector[i]->estimatedWaitTime) << endl;
                }
                if ( statusVector[i]->fileSize ) {
                    cout << "\t\tfileSize = " << *(statusVector[i]->fileSize) << endl;
                }
                if ( statusVector[i]->remainingPinLifetime ) {
                    cout << "\t\tremainingPinLifetime = " << *(statusVector[i]->remainingPinLifetime) << endl;
                }
                if ( statusVector[i]->remainingFileLifetime ) {
                    cout << "\t\tremainingFileLifetime = " << *(statusVector[i]->remainingFileLifetime) << endl;
                }
		if ( statusVector[i]->status->statusCode ) {
                    cout << "\t\tstatusCode = " << statusVector[i]->status->statusCode << endl;
		}
		if ( statusVector[i]->status->explanation ) {
		    cout << "\t\texplanation = " << statusVector[i]->status->explanation->c_str() << endl;
		}
            }
        }
        else {
            cout << "resp.srmStatusOfPutRequestResponse->arrayOfFileStatuses = NULL" << endl;
            status = -1;
        }
    }
    catch (...) {
        cout << "Got some exception when checking status..." << endl;
        status = -1;
    }
    return status;
}

int FinishRequest(
        struct soap *soap,
        std::string endpoint,
        std::string token,
        std::string user,
        std::vector<std::string> files) {

    srm__srmPutDoneRequest   request;
    srm__srmPutDoneResponse_ response;
    int status = -1;

    // Init request
    request.authorizationID = NULL;
    request.requestToken.assign(token);
    request.arrayOfSURLs = soap_new_srm__ArrayOfAnyURI(soap, -1);

    try {
        if ( user.length() > 0 ) {
            request.authorizationID = soap_new_std__string(soap, -1);
            request.authorizationID->assign(user);
        }

        for (unsigned int i=0; i<files.size(); i++ ) {
            request.arrayOfSURLs->urlArray.push_back(files[i]);
        }

        // Make the call
        if ( soap_call_srm__srmPutDone(soap, endpoint.c_str(), "srmPutDone", &request, response) ) {
            soap_print_fault (soap, stderr);
            soap_print_fault_location (soap, stderr);
            status = -1;
        }
        
        // Check response
        status = 0;
        cout << "srmPutDone response = " << response.srmPutDoneResponse->returnStatus->statusCode << endl;
        if ( response.srmPutDoneResponse->returnStatus->explanation != NULL &&
                response.srmPutDoneResponse->returnStatus->explanation->length() > 0 ) {
            cout << "explanation = " << response.srmPutDoneResponse->returnStatus->explanation->c_str() << endl;
        }

        if ( response.srmPutDoneResponse->arrayOfFileStatuses != NULL ) {
            std::vector<srm__TSURLReturnStatus*> statuses = response.srmPutDoneResponse->arrayOfFileStatuses->statusArray;
            for (unsigned int i=0; i<statuses.size(); i++ ) {
                cout << "Status of file " << statuses[i]->surl << endl;
                cout << "\tstatusCode = " << statuses[i]->status->statusCode << endl;
                if ( statuses[i]->status->explanation != NULL ) {
                    cout << "\texplnation = " << statuses[i]->status->explanation->c_str() << endl;
                }
                else {
                    cout << "\texplnation = NULL " << endl;
                }
            }
        }
    }
    catch (...) {
        status = -1;
    }
    return status;
}


int
main(int argc, char*argv[])
{
    struct soap* soap = soap_new();

    soap_cgsi_init(soap, CGSI_OPT_DISABLE_NAME_CHECK|CGSI_OPT_DELEG_FLAG);
    soap_set_namespaces(soap, namespaces);

    if ( argc < 3) {
        usage();
        exit(1);
    }

    bool overwrite = false;
    int retryLifetime = 0;
    std::string comment;
    std::string user;
    std::vector<std::string> protocolList;
    std::vector<std::string> surls;
    std::vector<int> lifetimes;
    std::vector<int> sizes;
    std::vector<std::string> spacetokens;

    int carg = 1; // Current argument
    while (carg < argc) {
        switch (argv[carg][1]) {
            case 'r':
                //if (validArg(argv[carg+1])) 
                //{
                cout << "arg is " << argv[carg+1] << endl;
                    retryLifetime = atoi(argv[++carg]);
                    cout << "retryLifetime set to " << retryLifetime << endl;
                //}
                //else { usage(); exit(1);}
                carg++;
                break;
            case 'u':
                if (validArg(argv[carg+1])) {
                    user.assign(argv[++carg]);
                }
                carg++;
                break;
            case 'c':
                if (validArg(argv[carg+1])) {
                    comment.assign(argv[++carg]);
                }
                else {
                    usage();
                    exit(1);
                }
                carg++;
                break;
            case 'p':
                if (validArg(argv[carg+1])) {
                    char *ptr = strtok(argv[++carg], ",");
                    while (ptr) {
                        protocolList.push_back(ptr);
                        ptr = strtok(NULL, ",");
                    }
                    carg++;
                }
                else { usage(); exit(1);}
                break;
            case 'o':
                if (validArg(argv[carg+1])) {
                    if (argv[carg++][0] != '0') overwrite = true;
                }
                else { usage(); exit(1);}
                carg++;
                break;
            case 'f':
                if (validArg(argv[carg+1])) {
                    char *ptr = strtok(argv[++carg], ",");
                    while (ptr) {
                        surls.push_back(ptr);
                        ptr = strtok(NULL, ",");
                    }
                    carg++;
                }
                else { usage(); exit(1);}
                break;
            case 's':
                if (validArg(argv[carg+1])) {
                    char *ptr = strtok(argv[++carg], ",");
                    while (ptr) {
                        sizes.push_back(atoi(ptr));
                        ptr = strtok(NULL, ",");
                    }
                    carg++;
                }
                else { usage(); exit(1);}
                break;
            case 'l':
                if (validArg(argv[carg+1])) {
                    char *ptr = strtok(argv[++carg], ",");
                    while (ptr) {
                        lifetimes.push_back(atoi(ptr));
                        ptr = strtok(NULL, ",");
                    }
                    carg++;
                }
                else { usage(); exit(1);}
                break;
            case 't':
                if (validArg(argv[carg+1])) {
                    char *ptr = strtok(argv[++carg], ",");
                    while (ptr) {
                        spacetokens.push_back(ptr);
                        ptr = strtok(NULL, ",");
                    }
                    carg++;
                }
                else { usage(); exit(1);}
                break;
            default:
                usage(); exit(1);
        }
    }

    for (unsigned int i=0; i<surls.size(); i++) {
        cout << "surl[" << i << "] = " << surls[i].c_str() << endl;
    }
    char* endPoint;
    char* sfn;
    if ( parsesurl (surls[0].c_str(), &endPoint, &sfn) < 0) {
        perror("parsesurl");
        exit(1);
    }
    char *bname = basename(sfn);
    for (unsigned int i=0; i<surls.size(); i++) {
        cout << "surl[" << i << "] = " << surls[i].c_str() << endl;
    }


    std::string requestToken = PrepareRequest( 
            soap,
            endPoint,
            user,
            comment,
            protocolList,
            overwrite,
            retryLifetime,
            lifetimes,
            sizes,
            spacetokens,
            surls );
    if ( requestToken.length() > 0 ) {
        // OK, so that worked - now see if we can get the status.
#if 0
	cout << "endPoint" << endPoint << endl ;
	cout << "requestToken" << requestToken << endl ;
#endif
        std::string fname = "TokenHolder";
        fname.append("_").append(bname).append(".dat");
        //FILE* file = fopen("TokenHolder.dat", "w");
        FILE* file = fopen(fname.c_str(), "w");
        fprintf(file, "%s\n", requestToken.c_str());
        fclose(file);
        if ( CheckStatusOfRequest( soap, endPoint, requestToken, user, surls, fname ) > 0 ) {
            //char c;
            //cout << "Enter eny key to continue after putting the file...";
            //cin >> c;
            //FinishRequest(soap, endPoint, requestToken, user, surls );
        }
    }
    else {
        cout << "Failure of PrepareToPut" << endl;
    }

    soap_end (soap);
    exit (0);
}
