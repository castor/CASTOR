/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testUpdateSpace.cpp,v 1.9 2007/01/16 16:45:15 sdewitt Exp $

#include "soapH.h"
#include "srmNsmap.h"
#include "cgsi_plugin.h"

void usage() {
    std::cout << "Usage:" << std::endl;
    std::cout << "\tsrm2_testUpdateSpace -e endPoint -x spaceToken (-a authorizationID) " << std::endl;
    std::cout << "\t\t(-S newTotalSize) (-G newGuaranteedSize) (-L newLifetime) (-k key:value) (-h)" << std::endl;
}


    int
main(int argc, char *argv[])
{
    srm__srmUpdateSpaceResponse_ rep;
    srm__srmUpdateSpaceRequest req;
    struct soap* soap = soap_new();
    std::string endPoint;

    req.authorizationID=0;
    req.newSizeOfTotalSpaceDesired=0;
    req.newSizeOfGuaranteedSpaceDesired=0;
    req.newLifeTime=0;
    req.storageSystemInfo=0;

    int cArg=1;
    while (cArg < argc) {
        if (std::string(argv[cArg]) == "-h") {
            usage();
            exit(0);
        }
        else if (std::string(argv[cArg]) == "-e"){
            endPoint.assign(argv[++cArg]);
        }
        else if (std::string(argv[cArg]) == "-a"){
            if (!req.authorizationID)
                req.authorizationID = soap_new_std__string(soap, -1);
            req.authorizationID->assign(argv[++cArg]);
        }
        else if (std::string(argv[cArg]) == "-x"){
            req.spaceToken.assign(argv[++cArg]);
        }
        else if (std::string(argv[cArg]) == "-S"){
            if (!req.newSizeOfTotalSpaceDesired) 
                req.newSizeOfTotalSpaceDesired=(ULONG64*)soap_malloc(soap, sizeof(ULONG64));
            *(req.newSizeOfTotalSpaceDesired) = atol(argv[++cArg]);
        }
        else if (std::string(argv[cArg]) == "-G"){
            if (!req.newSizeOfGuaranteedSpaceDesired) 
                req.newSizeOfGuaranteedSpaceDesired=(ULONG64*)soap_malloc(soap, sizeof(ULONG64));
            *(req.newSizeOfTotalSpaceDesired) = atol(argv[++cArg]);
        }
        else if (std::string(argv[cArg]) == "-L"){
            if (!req.newLifeTime) 
                req.newLifeTime=(int*)soap_malloc(soap, sizeof(int));
            *(req.newLifeTime) = atoi(argv[++cArg]);
        }
        else if (std::string(argv[cArg]) == "-k"){
            std::string arg(argv[++cArg]);
            std::string separator(":");
            size_t sepP = arg.find(separator);
            if ( sepP == std::string::npos || 
                    sepP == arg.length() ||
                    sepP == 0) {
                std::cout << "Invalid key/value pair" << std::endl;
                usage();
                exit(1);
            }
            std::string key = arg.substr(0, arg.find(separator));
            std::string val = arg.substr(arg.find(separator)+1, arg.length());
            if (!req.storageSystemInfo) req.storageSystemInfo = soap_new_srm__ArrayOfTExtraInfo(soap, -1);
            srm__TExtraInfo *xinfo = soap_new_srm__TExtraInfo(soap, -1);
            xinfo->key.assign(key);
            xinfo->value = 0;
            if ( val.length() > 0 ) {
                xinfo->value = soap_new_std__string(soap, -1);
                xinfo->value->assign(val);
            }
            req.storageSystemInfo->extraInfoArray.push_back(xinfo);
        }
        cArg++;
    }

    if (endPoint.length() == 0) {
        std::cerr << "No end point supplied." << std::endl;
        usage();
        exit(1);
    }

    soap_cgsi_init(soap, CGSI_OPT_DISABLE_NAME_CHECK|CGSI_OPT_DELEG_FLAG);
    soap_set_namespaces(soap, namespaces);


    if (soap_call_srm__srmUpdateSpace (soap, const_cast<char*>(endPoint.c_str()), "SrmUpdateSpace",
                &req, rep)) {
        soap_print_fault (soap, stderr);
        soap_print_fault_location (soap, stderr);
        soap_end (soap);
        exit (1);
    }

    srm__TReturnStatus* status = rep.srmUpdateSpaceResponse->returnStatus;

    std::cout << "Request Status: " << soap_srm__TStatusCode2s(soap, status->statusCode) << std::endl;
    if (status->explanation)
        std::cout << "Explanation: " << status->explanation->c_str() << std::endl;
    if (rep.srmUpdateSpaceResponse->requestToken)
        std::cout << "Request Token: " << rep.srmUpdateSpaceResponse->requestToken->c_str() << std::endl;
    if (rep.srmUpdateSpaceResponse->sizeOfTotalSpace)
        std::cout << "Total Space: " << *(rep.srmUpdateSpaceResponse->sizeOfTotalSpace) << std::endl;
    if (rep.srmUpdateSpaceResponse->sizeOfGuaranteedSpace)
        std::cout << "Guaranteed Space: " << *(rep.srmUpdateSpaceResponse->sizeOfGuaranteedSpace) << std::endl;
    if (rep.srmUpdateSpaceResponse->lifetimeGranted)
        std::cout << "Lifetime: " << *(rep.srmUpdateSpaceResponse->lifetimeGranted) << std::endl;

    soap_end (soap);
    exit (0);
}
