/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testCheckPermission.cpp,v 1.6 2007/01/16 16:45:13 sdewitt Exp $

#include "soapH.h"
#include "srmNsmap.h"
#define DEFPOLLINT 10

#define SRM_EP_PATH "/v2_1_1/srm"
#include "cgsi_plugin.h"

void usage() {
    std::cout << "Usage:" << std::endl;
    std::cout << "\tsrm2_testCheckPermission -e endPoint [-s SURL]+ (-a authorizationID) (-k key:value)* (-h)" << std::endl;
    std::cout << "NOTE: Atl least 1 SURL should be supplied" << std::endl;
}


int
main(int argc, char *argv[])
{
    srm__srmCheckPermissionResponse_ rep;
    srm__srmCheckPermissionRequest req;
    struct soap* soap = soap_new();

    std::string endPoint;

    req.authorizationID=0;
    req.arrayOfSURLs = soap_new_srm__ArrayOfAnyURI(soap, -1);
    req.storageSystemInfo=0;

    int cArg=1;
    while (cArg < argc) {
        if (std::string(argv[cArg]) == "-h") {
            usage();
            exit(0);
        }
        else if ( std::string(argv[cArg]) == "-e") {
            endPoint.assign(argv[++cArg]);
        }
        else if (std::string(argv[cArg]) == "-s") {
            req.arrayOfSURLs->urlArray.push_back(argv[++cArg]);
        }
        else if ( std::string(argv[cArg]) == "-a") {
            if (!req.authorizationID) req.authorizationID = soap_new_std__string(soap, -1);
            req.authorizationID->assign(argv[++cArg]);
        }
        else if ( std::string(argv[cArg]) == "-k" ) {
            std::string arg(argv[++cArg]);
            std::string separator(":");
            size_t sepP = arg.find(separator);
            if ( sepP == std::string::npos || 
                 sepP == arg.length() ||
                 sepP == 0) {
                std::cout << "Invalid key/value pair" << std::endl;
                usage();
                exit(1);
            }
            std::string key = arg.substr(0, arg.find(separator));
            std::string val = arg.substr(arg.find(separator)+1, arg.length());
            if (!req.storageSystemInfo) req.storageSystemInfo = soap_new_srm__ArrayOfTExtraInfo(soap, -1);
            srm__TExtraInfo *xinfo = soap_new_srm__TExtraInfo(soap, -1);
            xinfo->key.assign(key);
            xinfo->value = 0;
            if ( val.length() > 0 ) {
                xinfo->value = soap_new_std__string(soap, -1);
                xinfo->value->assign(val);
            }
            req.storageSystemInfo->extraInfoArray.push_back(xinfo);
        }
        else {
            std::cerr << "Invalid Argument " << argv[cArg] << std::endl;
            usage();
            exit(1);
        }
        ++cArg;
    }
    //while (*protocols[nbproto]) nbproto++;

    if (endPoint.length()==0) {
        std::cerr << "No endpoint supplied." << std::endl;
        usage();
        exit(1);
    }

    soap_cgsi_init(soap, CGSI_OPT_DISABLE_NAME_CHECK|CGSI_OPT_DELEG_FLAG);
    soap_set_namespaces(soap, namespaces);

    if (soap_call_srm__srmCheckPermission (soap, const_cast<char*>(endPoint.c_str()), "SrmCheckPermission",
                &req, rep)) {
        soap_print_fault (soap, stderr);
        soap_print_fault_location (soap, stderr);
        soap_end (soap);
        exit (1);
    }


    srm__TReturnStatus *status = rep.srmCheckPermissionResponse->returnStatus;
    srm__ArrayOfTSURLPermissionReturn *perms =  rep.srmCheckPermissionResponse->arrayOfPermissions;

    std::cout << "Request Status: " << soap_srm__TStatusCode2s(soap, status->statusCode) << std::endl;
    if ( status->explanation )
        std::cout << "Explanation: " << status->explanation->c_str() << std::endl;

    if ( perms ) {
        for (unsigned i = 0; i < perms->surlPermissionArray.size(); i++) {
            std::cout << "SURL: " << perms->surlPermissionArray[i]->surl.c_str() << std::endl;
            std::cout << "\tStatus: " << soap_srm__TStatusCode2s(soap, perms->surlPermissionArray[i]->status->statusCode) << std::endl;
            if ( perms->surlPermissionArray[i]->status->explanation ) 
                std::cout << "\tExplanation: " << perms->surlPermissionArray[i]->status->explanation->c_str() << std::endl;;
            if ( perms->surlPermissionArray[i]->permission ) 
                std::cout << "\tPermission: " << *(perms->surlPermissionArray[i]->permission) << std::endl; 
        }
    }

    soap_end (soap);
    exit (0);
}
