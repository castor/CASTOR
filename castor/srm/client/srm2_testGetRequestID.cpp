/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testGetRequestID.cpp,v 1.7 2007/01/31 16:51:17 sdewitt Exp $

#include "soapH.h"
#include "srmNsmap.h"

#include "cgsi_plugin.h"

void usage() {
    std::cout << "Usage;" << std::endl;
    std::cout << "\tsrm2_testGetRequestId -e endPoint (-a authorizationId) (-d userRequestDescription) (-h)" << std::endl;
    std::cout << "NOTE: userRequestDesciption is case sensitive" << std::endl;
}

    int
main(int argc, char *argv[])
{
    srm__srmGetRequestTokensResponse_ rep;
    srm__srmGetRequestTokensRequest req;
    struct soap* soap = soap_new();

    soap_cgsi_init(soap, CGSI_OPT_DISABLE_NAME_CHECK|CGSI_OPT_DELEG_FLAG);
    soap_set_namespaces(soap, namespaces);

    // Allocate request memory
    req.authorizationID        = NULL;
    req.userRequestDescription = NULL;
    std::string endPoint;
    int cArg=1;
    while (cArg < argc ) {
        if (strcmp (argv[cArg], "-e") == 0 ) {
            endPoint.assign(argv[++cArg]);
        }
        else if (strcmp(argv[cArg],"-a") == 0) {
            if (!req.authorizationID) req.authorizationID = soap_new_std__string(soap, -1);
            req.authorizationID->assign(argv[++cArg]);
        }
        else if (strcmp(argv[cArg],"-d") == 0) {
            if (!req.userRequestDescription) 
                req.userRequestDescription = soap_new_std__string(soap, -1);
            req.userRequestDescription->assign(argv[++cArg]);
        }
        else if (strcmp(argv[cArg],"-h") == 0) {
            usage();
            exit(0);
        }
        cArg++;
    }
    if ( endPoint.length() == 0 ) {
        std::cerr << "Mandatory parameter endPoint missing" << std::endl;
        usage();
        exit(1);
    }
    if (soap_call_srm__srmGetRequestTokens (soap, const_cast<char*>(endPoint.c_str()), "GetRequestID",
                &req, rep)) {
        soap_print_fault (soap, stderr);
        soap_print_fault_location (soap, stderr);
        soap_end (soap);
        exit (1);
    }
    srm__TReturnStatus* status = rep.srmGetRequestTokensResponse->returnStatus;

    std::cout << "Request Status: " << soap_srm__TStatusCode2s(soap, status->statusCode) << std::endl;
    if ( status->explanation ) 
        std::cout << status->explanation->c_str() << std::endl;

    srm__ArrayOfTRequestTokenReturn* tokens = 
        rep.srmGetRequestTokensResponse->arrayOfRequestTokens;

    /* wait for file "ready" */
    std::cout << "=== RETURNED TOKENS ===" << std::endl;;

    if ( tokens ) {
        for (unsigned i = 0; 
                i < tokens->tokenArray.size(); 
                i++) {
            std::cout << tokens->tokenArray[i]->requestToken.c_str() << std::endl;
            if ( tokens->tokenArray[i]->createdAtTime)
                std::cout << *(tokens->tokenArray[i]->createdAtTime) << std::endl;
        }
    }
    soap_end (soap);
    exit (0);
}

