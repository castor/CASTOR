#include "soapH.h"
#include "srmNsmap.h"
#include "cgsi_plugin.h"

void usage() {
    std::cerr << "Usage: srm2_testReleaseFiles -e endPoint (-r <requestToken>) (-u <authorizationId>) (-f <forceRemoval true|false>) ((-s <SURL>).)" << std::endl;
}

int main (int argc, char* argv[] ) {


    srm__srmReleaseFilesRequest request;

    request.requestToken    = NULL;
    request.authorizationID = NULL;
    request.arrayOfSURLs    = NULL;
    request.doRemove         = NULL;

    std::string endPoint;

    // Initialise soap
    struct soap* soap = soap_new();
    soap_cgsi_init(soap, CGSI_OPT_DISABLE_NAME_CHECK|CGSI_OPT_DELEG_FLAG);
    soap_set_namespaces(soap, namespaces);

    // Now parse arguments
    int currentArg = 1;
    while ( currentArg < argc ) {
        if ( argv[currentArg][0] == '-' ) {
            switch (argv[currentArg++][1] ) {
                case 'r': // requestToken
                    if ( request.requestToken == NULL ) {
                        request.requestToken = soap_new_std__string(soap, -1);
                        request.requestToken->assign(argv[currentArg++]);
                    }
                    else {
                        usage();
                        exit(1);
                    }
                    break;
                case 's': // SURL
                    if ( request.arrayOfSURLs == NULL ) {
                        request.arrayOfSURLs = soap_new_srm__ArrayOfAnyURI(soap, -1);
                    }
                    request.arrayOfSURLs->urlArray.push_back(argv[currentArg++]);
                    break;
                case 'u': // authorizationID
                    if ( request.authorizationID == NULL ) {
                        request.authorizationID = soap_new_std__string(soap, -1);
                        request.authorizationID->assign(argv[currentArg++]);
                    }
                    else {
                        usage();
                        exit(1);
                    }
                    break;
                case 'f': //doRemove
                    if ( request.doRemove == NULL ) {
                        request.doRemove = (bool*)soap_malloc(soap, sizeof(bool));
                        if (strcmp (argv[currentArg], "true") == 0 ) {
                            *(request.doRemove) = true;
                        }
                        else if ( strcmp (argv[currentArg], "false") == 0 ) {
                            *(request.doRemove) = false;
                        }
                        else {
                            usage();
                            exit(1);
                        }
                        currentArg++;
                    }
                    else {
                        usage();
                        exit(1);
                    }
                    break;
                case 'e':
                    endPoint.assign(argv[currentArg++]);
                    break;
                default:
                    usage();
                    exit(1);
            }
        }
        else {
            usage();
            break;
        }
    }

    if ( endPoint.length() == 0 ) {
        usage();
        exit(1);
    }

    // Now make the call...
    srm__srmReleaseFilesResponse_ response;

    if ( soap_call_srm__srmReleaseFiles( soap, endPoint.c_str(), "srmReleaseFiles", &request, response ) ) {
        soap_print_fault (soap, stderr);
        soap_print_fault_location (soap, stderr);
        exit(1);
    }

    // Print out results...
    std::cout << "Return Code:" << soap_srm__TStatusCode2s(soap, response.srmReleaseFilesResponse->returnStatus->statusCode) << std::endl;
    std::cout << "Explanation: " << response.srmReleaseFilesResponse->returnStatus->explanation->c_str() << std::endl;
    if ( response.srmReleaseFilesResponse->arrayOfFileStatuses ) {
        for ( unsigned i=0; i< response.srmReleaseFilesResponse->arrayOfFileStatuses->statusArray.size(); i++ ) {
            srm__TSURLReturnStatus* myStatus = response.srmReleaseFilesResponse->arrayOfFileStatuses->statusArray[i];
            std::cout << "SURL: " << myStatus->surl << std::endl;
            std::cout << "\tStatus: " << soap_srm__TStatusCode2s(soap, myStatus->status->statusCode) << std::endl;
            std::cout << "\tExplanation: " << myStatus->status->explanation->c_str() << std::endl;
        }
    }
    
}
