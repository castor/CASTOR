/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testGetSpaceToken.cpp,v 1.9 2008/10/28 13:25:02 sdewitt Exp $

#include "soapH.h"
#include "srmNsmap.h"
#include "cgsi_plugin.h"

void usage() {
    std::cout << "Usage:" << std::endl;
    std::cout << "srm2_testGetSpaceToken -e endPoint (-d description) (-a authorizationId) (-h)" << std::endl;
}

    int
main(int argc, char *argv[])
{
    srm__srmGetSpaceTokensResponse_ rep;
    srm__srmGetSpaceTokensRequest req;
    struct soap* soap = soap_new();
    std::string endPoint;

    req.userSpaceTokenDescription=0;
    req.authorizationID=0;

    int cArg=1;
    while (cArg < argc) {
        if ( std::string(argv[cArg]) == "-e") 
            endPoint.assign(argv[++cArg]);
        else if (std::string(argv[cArg]) == "-d") {
            if (!req.userSpaceTokenDescription)
                req.userSpaceTokenDescription = soap_new_std__string(soap, -1);
            req.userSpaceTokenDescription->assign(argv[++cArg]);
        }
        else if (std::string(argv[cArg]) == "-a") {
            if (!req.authorizationID)
                req.authorizationID = soap_new_std__string(soap, -1);
            req.authorizationID->assign(argv[++cArg]);
        }
        else if (std::string(argv[cArg]) == "-h") {
            usage();
            exit(0);
        }
        else {
            std::cerr << "Invalid argument: " << argv[cArg] << std::endl;
            usage();
            exit(1);
        }
        cArg++;
    }

    if (endPoint.length() == 0) {
        std::cerr << "No end point argument supplied." << std::endl;
        usage();
        exit(1);
    }

    soap_cgsi_init(soap, CGSI_OPT_DISABLE_NAME_CHECK|CGSI_OPT_DELEG_FLAG);
    soap_set_namespaces(soap, namespaces);

    if (soap_call_srm__srmGetSpaceTokens (soap, const_cast<char*>(endPoint.c_str()), "GetSpaceToken",
                &req, rep)) {
        soap_print_fault (soap, stderr);
        soap_print_fault_location (soap, stderr);
        soap_end (soap);
        exit (1);
    }
    srm__TReturnStatus* status = rep.srmGetSpaceTokensResponse->returnStatus;
    srm__ArrayOfString* tokens = rep.srmGetSpaceTokensResponse->arrayOfSpaceTokens;

    std::cout << "Request Status: " << soap_srm__TStatusCode2s(soap, status->statusCode) << std::endl;
    if (status->explanation)
        std::cout << "Explanation: " << status->explanation->c_str() << std::endl;

    if (tokens) {
        for (unsigned i=0; i<tokens->stringArray.size(); i++) {
            std::cout << "Token[" << i << "]: " << tokens->stringArray[i] << std::endl;
        }
    }

    soap_end (soap);
    exit (0);
}
