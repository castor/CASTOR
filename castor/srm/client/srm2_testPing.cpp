/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testPing.cpp,v 1.6 2008/11/03 11:15:05 sdewitt Exp $

#include "soapH.h"
#include "srmNsmap.h"

#include "cgsi_plugin.h"

void usage() {
    std::cerr << "Usage:" << std::endl;
    std::cerr << "\tsrm2_testPing -e endPoint (-a authorizationId)" << std::endl;
}


    int
main(int argc, char *argv[])
{
    srm__srmPingResponse_ rep;
    srm__srmPingRequest req;
    struct soap* soap = soap_new();
    std::string endPoint;

    soap_cgsi_init(soap, CGSI_OPT_DISABLE_NAME_CHECK|CGSI_OPT_DELEG_FLAG);
    soap_set_namespaces(soap, namespaces);

    req.authorizationID = NULL;

    // Parse input
    int carg = 1;
    while (carg < argc) {
        if (strcmp (argv[carg], "-e") == 0 ) {
            endPoint.assign(argv[++carg]);
        }
        else if (strcmp(argv[carg],"-a") == 0) {
            if (!req.authorizationID) req.authorizationID = soap_new_std__string(soap, -1);
            req.authorizationID->assign(argv[++carg]);
        }
        carg++;
    }

    if ( endPoint.length() == 0 ) {
        usage();
        exit(1);
    }

    endPoint.replace(0, endPoint.find("://"), "httpg");
    if ( endPoint.find("/srm/managerv2") == std::string::npos ) {
        endPoint.append("/srm/managerv2");
    }
    
    if (soap_call_srm__srmPing (soap, endPoint.c_str(), "Ping", &req, rep)) {
        soap_print_fault (soap, stderr);
        soap_print_fault_location (soap, stderr);
        soap_end (soap);
        soap_free (soap);
        exit (1);
    }
    if ( !rep.srmPingResponse ) {
      printf ("Empty response!\n");
      exit (1);
    }
    printf ("version %s\n", rep.srmPingResponse->versionInfo.c_str());
    if ( rep.srmPingResponse->otherInfo ) {
        for (unsigned int i=0; i<rep.srmPingResponse->otherInfo->extraInfoArray.size() ; i++ ) {
            srm__TExtraInfo* info = rep.srmPingResponse->otherInfo->extraInfoArray[i];
            std::cout << info->key << " = " << info->value->c_str() << std::endl;
        }
    }
    soap_end (soap);
    soap_free (soap);
    exit (0);
}
