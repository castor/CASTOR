#include "soapH.h"
#include "srmNsmap.h"

#include <cgsi_plugin.h>

using namespace std;

void usage () {
    cerr << "srm2_testStatusOfReserveSpaceRequest -e endPoint -r requestToken" << endl;
}

int main( int argc, char *argv[] ) {

    if ( argc < 5 ) {
        usage();
        exit (1);
    }

    struct soap* soap = soap_new();
    soap_cgsi_init(soap, CGSI_OPT_DISABLE_NAME_CHECK);
    soap_set_namespaces(soap, namespaces);

    std::string ep;
    std::string rt;

    int carg = 1;
    while ( carg < argc ) {
        switch (argv[carg][1]) {
            case 'e':
                ep = argv[++carg];
                break;
            case 'r':
                rt = argv[++carg];
                break;
            default:
                usage();
                exit (1);
        }
        carg++;
    }

    srm__srmStatusOfReserveSpaceRequestRequest   request;
    srm__srmStatusOfReserveSpaceRequestResponse_ response;

    request.authorizationID = NULL;
    request.requestToken.assign(rt);

    if ( soap_call_srm__srmStatusOfReserveSpaceRequest(soap, ep.c_str(), "srmStatusOfReserveSpace", &request, response) != SOAP_OK ) {
        soap_print_fault (soap, stderr);
        soap_print_fault_location (soap, stderr);
        exit (1);
    }

    srm__srmStatusOfReserveSpaceRequestResponse *resp = response.srmStatusOfReserveSpaceRequestResponse;
    cout << "Request Status: " << soap_srm__TStatusCode2s(soap, resp->returnStatus->statusCode) << endl;
    if ( resp->returnStatus->explanation ) 
        cout << "explanation: " << resp->returnStatus->explanation->c_str() << endl;
    if (resp->estimatedProcessingTime) 
        cout << "estimatedProcessingTime: " << *(resp->estimatedProcessingTime) << endl;
    if ( resp->retentionPolicyInfo ) {
        cout << "retentionPolicy: " << resp->retentionPolicyInfo->retentionPolicy << endl;
        if ( resp->retentionPolicyInfo->accessLatency ) 
            cout << "accessLatency: " << *(resp->retentionPolicyInfo->accessLatency) << endl;
    }
    if ( resp->sizeOfTotalReservedSpace ) 
        cout << "sizeOfTotalReservedSpace: " << *(resp->sizeOfTotalReservedSpace) << endl;
    if ( resp->sizeOfGuaranteedReservedSpace ) 
        cout << "sizeOfGuaranteedReservedSpace: " << *(resp->sizeOfGuaranteedReservedSpace) << endl;
    if ( resp->lifetimeOfReservedSpace ) 
        cout << "lifetimeOfReservedSpace: " << *(resp->lifetimeOfReservedSpace) << endl;
    if ( resp->spaceToken ) 
        cout << "spaceToken: " << resp->spaceToken << endl;
    exit(0);
}
