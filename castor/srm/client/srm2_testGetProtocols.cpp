/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testGetProtocols.cpp,v 1.1 2008/11/18 15:43:45 sdewitt Exp $

#include "soapH.h"
#include "srmNsmap.h"
#include "cgsi_plugin.h"
#include <sstream>

void usage() {
    std::cerr << "Usage:" << std::endl;
    std::cerr << "\tsrm2_testGetProtocols -e endPoint (-a authorizationId)" << std::endl;
}


    int
main(int argc, char *argv[])
{
    srm__srmGetTransferProtocolsResponse_ rep;
    srm__srmGetTransferProtocolsRequest   req;
    struct soap* soap = soap_new();
    std::string endPoint;

    soap_cgsi_init(soap, CGSI_OPT_DISABLE_NAME_CHECK|CGSI_OPT_DELEG_FLAG);
    soap_set_namespaces(soap, namespaces);

    req.authorizationID = NULL;

    // Parse input
    int carg = 1;
    while (carg < argc) {
        if (strcmp (argv[carg], "-e") == 0 ) {
            endPoint.assign(argv[++carg]);
        }
        else if (strcmp(argv[carg],"-a") == 0) {
            if (!req.authorizationID) req.authorizationID = soap_new_std__string(soap, -1);
            req.authorizationID->assign(argv[++carg]);
        }
        carg++;
    }

    if ( endPoint.length() == 0 ) {
        usage();
        exit(1);
    }

    endPoint.replace(0, endPoint.find("://"), "httpg");
    if ( endPoint.find("/srm/managerv2") == std::string::npos ) {
        endPoint.append("/srm/managerv2");
    }
    
    if (soap_call_srm__srmGetTransferProtocols (soap, endPoint.c_str(), "GetTransferProtocols",
                &req, rep)) {
        soap_print_fault (soap, stderr);
        soap_print_fault_location (soap, stderr);
        soap_end (soap);
        exit (1);
    }

    srm__srmGetTransferProtocolsResponse *resp = rep.srmGetTransferProtocolsResponse;
    std::cout << "Request Status: " << soap_srm__TStatusCode2s(soap, resp->returnStatus->statusCode) << std::endl;
    if ( resp->returnStatus->explanation && resp->returnStatus->explanation->length() > 0 ) 
        std::cout << resp->returnStatus->explanation->c_str() << std::endl;

    if ( resp->protocolInfo ) {
        for (unsigned i=0; i<resp->protocolInfo->protocolArray.size(); i++) {
            std::cout << "\tProtocol: " << resp->protocolInfo->protocolArray[i]->transferProtocol << std::endl;
            if ( resp->protocolInfo->protocolArray[i]->attributes ) {
                for (unsigned j=0; j<resp->protocolInfo->protocolArray[i]->attributes->extraInfoArray.size(); j++) {
                    std::stringstream ss;
                    ss << "(" << resp->protocolInfo->protocolArray[i]->attributes->extraInfoArray[j]->key;
                    if (resp->protocolInfo->protocolArray[i]->attributes->extraInfoArray[j]->value)
                        ss << ", " << resp->protocolInfo->protocolArray[i]->attributes->extraInfoArray[j]->value->c_str();
                    ss << ")";
                    std::cout << "\t\t" << ss.str() << std::endl;
                }
            }
        }
    }
    soap_end (soap);
    exit (0);
}
