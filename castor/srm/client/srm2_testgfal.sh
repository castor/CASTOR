#! /bin/sh

if [ ! $1 ]
then
  echo "Usage: $0 endpoint-name [spacetoken] [castor path]"
  exit 1
fi

ENDPOINT=$1
if [ ! `echo ${ENDPOINT} | grep ':'` ]; then
  ENDPOINT=$1:8443
fi
SPACE_TOKEN=${2:-srm2_d1t0}
CASTORPATH=${3:-/castor/cern.ch/grid/dteam/castordev}
SURL=srm://${ENDPOINT}/srm/managerv2?SFN=${CASTORPATH}/test-`echo $1 | sed 's/:/_/'`-${SPACE_TOKEN}-`uuidgen`

echo 'Using '`gfal2_version`

#
# Check grid proxy
#
grid-proxy-info 1>/dev/null 2>/dev/null
if [ ! $? ]; then
    printf "\n#\n# Could not find valid grid proxy, exiting...\n#\n\n"
    exit 1
fi

#
# Copy
#
cmd="gfal-copy --verbose -D\"BDII:ENABLED\"=false -S ${SPACE_TOKEN} file:///etc/group"
printf "\n#\n# Executing \"${cmd} '${SURL}'\"\n\n"
time ${cmd} ${SURL} || exit 1

#
# List (with checksum)
#
cmd="gfal-ls --verbose -D\"BDII:ENABLED\"=false -l "
printf "\n#\n# Executing \"${cmd} '${SURL}'\"\n\n"
time ${cmd} ${SURL} || exit 1

#
# Get it back (bring online + real get)
#
cmd="gfal-legacy-bringonline --verbose -D\"BDII:ENABLED\"=false ${SURL}"
printf "\n#\n# Executing \"${cmd}\"\n#\n\n"
${cmd} || exit 1

cmd="gfal-copy -f --verbose -D\"BDII:ENABLED\"=false "
printf "\n#\n# Executing \"${cmd} '${SURL}' file:///tmp/test-group\"\n\n"
time ${cmd} ${SURL} file:///tmp/test-group || exit 1

#
# get TURL (gsiftp)
#
cmd="gfal-xattr -D\"BDII:ENABLED\"=false ${SURL} user.replicas"
printf "\n#\n# Executing \"${cmd}\"\n#\n\n"
${cmd} || exit 1

#
# legacy get TURL for xroot
#
cmd="lcg-getturls -D srmv2 --nobdii ${SURL} -p xroot"
printf "\n#\n# Executing \"${cmd}\"\n#\n\n"
${cmd} || exit 1

#
# delete it
#
cmd="gfal-rm --verbose -D\"BDII:ENABLED\"=false "
printf "\n#\n# Executing \"${cmd}\" '${SURL}'\n#\n\n"
time ${cmd} ${SURL} || exit 1

#
printf "\n#\n# Done!\n#\n"

