/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testLs.cpp,v 1.17 2009/06/30 10:47:26 sdewitt Exp $

#include "soapH.h"
#include "srmNsmap.h"

#include "cgsi_plugin.h"

#include <sstream>

using namespace std;

int _offset;
int _count;
int _nLevels;
void printDetails( struct soap* soap, std::vector<srm__TMetaDataPathDetail*> details, std::string indent);

void usage () {
    std::cout << "Usage:" << std::endl;
    std::cout << "srm2_testLs" << std::endl;
    std::cout << "\t -e endPoint         End point to contact SRM (mandatory)" << std::endl;
    std::cout << "\t -s SURL             One or more SURLs to get listing for (mandatory)" << std::endl;
    std::cout << "\t -a authorisationID  Any authorisation ID required" << std::endl;
    std::cout << "\t -t fileStorageType  Only get files in the specified storage type" << std::endl;
    std::cout << "\t\t 0=PERMANENT, 1=DURABLE, 2=VOLATILE" << std::endl;
    std::cout << "\t -V                  Verbose information; detailed listing" << std::endl;
    std::cout << "\t -r                  Full recursion in listing directories" << std::endl;
    std::cout << "\t -n numLevels        Number of levels recursion is permitted" << std::endl;
    std::cout << "\t -o offset           Number of files to offset whn listing" << std::endl;
    std::cout << "\t -c count            Number of results to return" << std::endl;
    std::cout << "\t -k key:value        Key/value pair for additional info" << std::endl;
    return;
}

    int
main(int argc, char *argv[])
{
    srm__srmLsResponse_ rep;
    srm__srmLsRequest req;
    struct soap* soap = soap_new();

    soap_cgsi_init(soap, CGSI_OPT_DISABLE_NAME_CHECK|CGSI_OPT_DELEG_FLAG);
    soap_set_namespaces(soap, namespaces);

    // Init request
    req.authorizationID   = NULL;
    req.arrayOfSURLs      = soap_new_srm__ArrayOfAnyURI(soap, -1);
    req.storageSystemInfo = NULL;
    req.fileStorageType   = NULL;
    req.fullDetailedList  = NULL;
    req.allLevelRecursive = NULL;
    req.numOfLevels       = NULL;
    req.offset            = NULL;
    req.count             = NULL;

    std::string endPoint;
    // Parse arguments
    int carg = 1;
    while ( carg < argc ) {
        if (strcmp (argv[carg], "-e") == 0 ) {
            endPoint.assign(argv[++carg]);
        }
        else if (strcmp (argv[carg], "-s") == 0 ) {
            std::string thisSURL(argv[++carg]);
            req.arrayOfSURLs->urlArray.push_back(thisSURL);
        }
        else if (strcmp(argv[carg],"-a") == 0) {
            if (!req.authorizationID) req.authorizationID = soap_new_std__string(soap, -1);
            req.authorizationID->assign(argv[++carg]);
        }
        else if (strcmp (argv[carg],"-t") == 0 ) {
            if (!req.fileStorageType) req.fileStorageType = (enum srm__TFileStorageType*)soap_malloc(soap, sizeof(enum srm__TFileStorageType));
            switch (atoi(argv[++carg])) {
                case 0:
                    *(req.fileStorageType) = srm__TFileStorageType__PERMANENT;
                    break;
                case 1:
                    *(req.fileStorageType) = srm__TFileStorageType__DURABLE;
                    break;
                case 2:
                    *(req.fileStorageType) = srm__TFileStorageType__VOLATILE;
                    break;
                default:
                    usage();
                    exit(1);
            }
        }
        else if (strcmp(argv[carg], "-V") == 0 ) {
            if (!req.fullDetailedList) req.fullDetailedList = (bool*)soap_malloc(soap, sizeof(bool));
            *(req.fullDetailedList) = true;
        }
        else if (strcmp(argv[carg], "-r") == 0 ) {
            if (!req.allLevelRecursive) req.allLevelRecursive = (bool*)soap_malloc(soap, sizeof(bool));
            *(req.allLevelRecursive) = true;
        }
        else if ( strcmp(argv[carg], "-n") == 0 ) {
            if (!req.numOfLevels) req.numOfLevels = (int*)soap_malloc(soap, sizeof(int));
            *(req.numOfLevels) = atoi(argv[++carg]);
        }
        else if ( strcmp(argv[carg], "-o") == 0 ) {
            if (!req.offset) req.offset = (int*)soap_malloc(soap, sizeof(int));
            *(req.offset) = atoi(argv[++carg]);
        }
        else if ( strcmp(argv[carg], "-c") == 0 ) {
            if (!req.count) req.count = (int*)soap_malloc(soap, sizeof(int));
            *(req.count) = atoi(argv[++carg]);
        }
        else if ( strcmp(argv[carg], "-k") == 0 ) {
            std::string arg(argv[++carg]);
            std::string separator(":");
            size_t sepP = arg.find(separator);
            if ( sepP == std::string::npos || 
                 sepP == arg.length() ||
                 sepP == 0) {
                std::cout << "Invalid key/value pair" << std::endl;
                usage();
                exit(1);
            }
            std::string key = arg.substr(0, arg.find(separator));
            std::string val = arg.substr(arg.find(separator)+1, arg.length());
            if (!req.storageSystemInfo) req.storageSystemInfo = soap_new_srm__ArrayOfTExtraInfo(soap, -1);
            srm__TExtraInfo *xinfo = soap_new_srm__TExtraInfo(soap, -1);
            xinfo->key.assign(key);
            xinfo->value = 0;
            if ( val.length() > 0 ) {
                xinfo->value = soap_new_std__string(soap, -1);
                xinfo->value->assign(val);
            }
            req.storageSystemInfo->extraInfoArray.push_back(xinfo);
        }
        else {
            std::cout << "Unknown option: " << argv[carg] << std::endl;
            usage();
            exit(1);
        }
        carg++;
    }

    if ( endPoint.length() == 0 || req.arrayOfSURLs->urlArray.size() == 0 ) {
        usage();
        exit (1);
    }

    if (soap_call_srm__srmLs (soap, endPoint.c_str(), "Ls",
                &req, rep)) {
        soap_print_fault (soap, stderr);
        soap_print_fault_location (soap, stderr);
        soap_end (soap);
        exit (1);
    }
    srm__TReturnStatus* reqstatp = rep.srmLsResponse->returnStatus;
    srm__ArrayOfTMetaDataPathDetail* repfs = rep.srmLsResponse->details;

    std::cout << "Request Status: " << soap_srm__TStatusCode2s(soap, reqstatp->statusCode) <<std::endl;
    if (reqstatp->explanation) std::cout << "Expanation    : " << reqstatp->explanation->c_str() << std::endl;
    if (rep.srmLsResponse->requestToken)     std::cout << "Request Token : " << rep.srmLsResponse->requestToken->c_str() << std::endl;
    if ( repfs )
        printDetails(soap, repfs->pathDetailArray, "  ");
    soap_end (soap);
    exit (0);
}

void printDetails( struct soap *soap, std::vector<srm__TMetaDataPathDetail*> details, std::string indent) {
    for ( unsigned i=0; i<details.size(); i++ ) {
        std::cout << indent << "Path: " << details[i]->path << std::endl;
        std::cout << indent << "Status: " << soap_srm__TStatusCode2s(soap, details[i]->status->statusCode) << std::endl;
        if ( details[i]->status->explanation) 
            std::cout << indent << "Explanation : " << details[i]->status->explanation->c_str() << std::endl;
        if ( details[i]->size ) {
            std::cout << indent << indent << "size: " << *(details[i]->size) << std::endl;
        }
        if ( details[i]->createdAtTime ) {
            std::cout << indent << indent << "ctime=" << *(details[i]->createdAtTime) << std::endl;
        }
        if ( details[i]->lastModificationTime) {
            std::cout << indent << indent << "mtime=" << *(details[i]->lastModificationTime) << std::endl;
        }
        if ( details[i]->fileStorageType ) {
            std::cout << indent << indent << "fileStorageType=" << *(details[i]->fileStorageType) << std::endl;
        }
        if ( details[i]->retentionPolicyInfo ) {
            std::cout << indent << indent << "retentionPolicy: " << details[i]->retentionPolicyInfo->retentionPolicy << std::endl;
            if ( details[i]->retentionPolicyInfo->accessLatency ) 
                std::cout << indent << indent << "accessLatency: " << *(details[i]->retentionPolicyInfo->accessLatency) << std::endl;
        }
        //if ( details[i]->fileLocality ) std::cout << indent << indent << "fileLocality=" << *(details[i]->fileLocality) << std::endl;
        if ( details[i]->fileLocality ) {
            std::cout << indent << indent << "fileLocality="; 
            switch ((int)(*(details[i]->fileLocality))) {
                case 0:
                    std::cout << "ONLINE" << std::endl;
                    break;
                case 1:
                    std::cout << "NEARLINE" << std::endl;
                    break;
                case 2:
                    std::cout << "ONLINE_AND_NEARLINE" << std::endl;
                    break;
                case 3:
                    std::cout << "LOST" << std::endl;
                    break;
                case 4:
                    std::cout << "NONE" << std::endl;
                    break;
                case 5:
                    std::cout << "UNAVAILABLE" << std::endl;
                    break;
                default:
                    std::cout << "UNKNOWN" << std::endl;
                    break;
            }
        }
        if ( details[i]->arrayOfSpaceTokens ) {
            std::cout << indent << indent << "SpaceToken: ";
            for ( unsigned token = 0; token<details[i]->arrayOfSpaceTokens->stringArray.size(); token++) {
                std::cout << details[i]->arrayOfSpaceTokens->stringArray[token] << ", ";
            }
            std::cout << std::endl;
        }
        if ( details[i]->type ) std::cout << indent << indent << "fileType=" << *(details[i]->type) << std::endl;
        if ( details[i]->ownerPermission ) {
            std::cout << indent << indent << "ownerId =" << details[i]->ownerPermission->userID << std::endl;
            std::cout << indent << indent << "ownerPermission=" << details[i]->ownerPermission->mode << std::endl;
        }
        if ( details[i]->groupPermission ) std::cout << indent << indent << "groupPermission=" << details[i]->groupPermission->mode << std::endl;
        if ( details[i]->otherPermission ) std::cout << indent << indent << "other: perm=" << *(details[i]->otherPermission) << std::endl;
        if ( details[i]->checkSumType ) std::cout << indent << indent << "checkSumType=" << details[i]->checkSumType->c_str() << std::endl;
        if ( details[i]->checkSumValue ) std::cout << indent << indent << "checkSumValue=" << details[i]->checkSumValue->c_str() << std::endl;
        if ( details[i]->arrayOfSubPaths ) {
            std::vector<srm__TMetaDataPathDetail*> subdetails = details[i]->arrayOfSubPaths->pathDetailArray;
            printDetails( soap, subdetails, indent + "  " );
        }
    }
}
