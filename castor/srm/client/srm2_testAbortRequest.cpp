/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testAbortRequest.cpp,v 1.7 2008/11/03 11:13:14 sdewitt Exp $

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef DG_DIAGNOSE
#include "diagnose/dg.h"
#endif

#include "soapH.h"
#include "srmNsmap.h"
#define DEFPOLLINT 10

#define SRM_EP_PATH "/v2_1_1/srm"
#include "cgsi_plugin.h"

void usage() {
    std::cerr << "Usage:" << std::endl;
    std::cerr << "srm2_testAbortRequest -e endPoint -r requestToken (-a authorizationID) (-h)" << std::endl;
    return;
}

int
main(int argc, char** argv)
{
    srm__srmAbortRequestResponse_ rep;
    srm__srmAbortRequestRequest req;
    std::string endPoint;
    struct soap* soap = soap_new();

    soap_cgsi_init(soap, CGSI_OPT_DISABLE_NAME_CHECK|CGSI_OPT_DELEG_FLAG);
    soap_set_namespaces(soap, namespaces);

    req.authorizationID = NULL;


    // Parse arguments
    int carg = 1;
    while ( carg < argc ) {
        if (strcmp (argv[carg], "-e") == 0 ) {
            endPoint.assign(argv[++carg]);
        }
        else if (strcmp(argv[carg],"-a") == 0) {
            if (!req.authorizationID) req.authorizationID = soap_new_std__string(soap, -1);
            req.authorizationID->assign(argv[++carg]);
        }
        else if (strcmp(argv[carg],"-r") == 0) {
            req.requestToken.assign(argv[++carg]);
        }
        else if (strcmp(argv[carg],"-h") == 0) {
            usage();
            exit(0);
        }
        else {
            usage();
            exit(1);
        }
        carg++;
    }

    if ( endPoint.length() == 0 ) {
        usage();
        exit(1);
    }

    endPoint.replace(0, endPoint.find("://"), "httpg");
    if ( endPoint.find("/srm/managerv2") == std::string::npos ) {
        endPoint.append("/srm/managerv2");
    }

    if (soap_call_srm__srmAbortRequest (soap, endPoint.c_str(), "AbortRequest",
                &req, rep)) {
        soap_print_fault (soap, stderr);
        soap_print_fault_location (soap, stderr);
        soap_end (soap);
        exit (1);
    }

    srm__srmAbortRequestResponse *resp = rep.srmAbortRequestResponse;

    std::cout << "Request Status: " << soap_srm__TStatusCode2s(soap, resp->returnStatus->statusCode) << std::endl;
    if ( resp->returnStatus->explanation) std::cout << "Expanation    : " << resp->returnStatus->explanation->c_str() << std::endl;
    
    soap_end (soap);
    exit (0);
}
