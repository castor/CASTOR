/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testStatusOfBringOnline.cpp,v 1.2 2008/01/25 17:06:30 sdewitt Exp $

#include "soapH.h"
#include "srmNsmap.h"
#include "cgsi_plugin.h"

#include <fstream>
#include <map>

using namespace std;

void usage() {
    cerr << "Usage: srm2_testStatusOfBringOnline" << endl;
    std::cerr << "\t -e    SRM endPoint (mandatory)" << std::endl;
    std::cerr << "\t -r    Request Token from previous BringOnline request (mandatory)" << std::endl;
    std::cerr << "\t -a    authorization ID" << std::endl;
    std::cerr << "\t -s    0 or more SURLs associated with the request token" << std::endl;
    return;
}


void CheckStatus (
        struct soap *soap,
        std::string endpoint,
        srm__srmStatusOfBringOnlineRequestRequest req
        ) {

    std::map<srm__TStatusCode, std::string> codes;
    codes[srm__TStatusCode__SRM_USCORESUCCESS] = "SRM_SUCCESS";
    codes[srm__TStatusCode__SRM_USCOREFAILURE] = "SRM_FAILURE";
    codes[srm__TStatusCode__SRM_USCOREAUTHENTICATION_USCOREFAILURE] = "SRM_AUTHENTICATION_FAILURE";
    codes[srm__TStatusCode__SRM_USCOREAUTHORIZATION_USCOREFAILURE]= "SRM_AUTHORIZATION_FAILURE";
    codes[srm__TStatusCode__SRM_USCOREINVALID_USCOREREQUEST]= "SRM_INVALID_REQUEST";
    codes[srm__TStatusCode__SRM_USCOREINVALID_USCOREPATH]= "SRM_INVALID_PATH";
    codes[srm__TStatusCode__SRM_USCOREFILE_USCORELIFETIME_USCOREEXPIRED]= "SRM_FILE_LIFETIME_EXPIRED";
    codes[srm__TStatusCode__SRM_USCORESPACE_USCORELIFETIME_USCOREEXPIRED]= "SRM_SPECE_LIFETIME_EXPIRED";
    codes[srm__TStatusCode__SRM_USCOREEXCEED_USCOREALLOCATION]= "SRM_EXCEED_ALLOCATION";
    codes[srm__TStatusCode__SRM_USCORENO_USCOREUSER_USCORESPACE]= "SRM_NO_USER_SPACE";
    codes[srm__TStatusCode__SRM_USCORENO_USCOREFREE_USCORESPACE]= "SRM_NO_FREE_SPACE";
    codes[srm__TStatusCode__SRM_USCOREDUPLICATION_USCOREERROR]= "SRM_DUPLICATION_ERROR";
    codes[srm__TStatusCode__SRM_USCORENON_USCOREEMPTY_USCOREDIRECTORY]= "SRM_NON_EMPTY_DIRECTORY";
    codes[srm__TStatusCode__SRM_USCORETOO_USCOREMANY_USCORERESULTS]= "SRM_TOO_MANY_RESULTS";
    codes[srm__TStatusCode__SRM_USCOREINTERNAL_USCOREERROR]= "SRM_INTERNAL_ERROR";
    codes[srm__TStatusCode__SRM_USCOREFATAL_USCOREINTERNAL_USCOREERROR]= "SRM_FATAL_INTERNAL_ERROR";
    codes[srm__TStatusCode__SRM_USCORENOT_USCORESUPPORTED]= "SRM_NOT_SUPPORTED";
    codes[srm__TStatusCode__SRM_USCOREREQUEST_USCOREQUEUED]= "SRM_REQUEST_QUEUED";
    codes[srm__TStatusCode__SRM_USCOREREQUEST_USCOREINPROGRESS]= "SRM_REQUEST_INPROGESS";
    codes[srm__TStatusCode__SRM_USCOREREQUEST_USCORESUSPENDED]= "SRM_REQUEST_SUSPENDED";
    codes[srm__TStatusCode__SRM_USCOREABORTED]= "SRM_ABORTED";
    codes[srm__TStatusCode__SRM_USCORERELEASED]= "SRM_RELEASED";
    codes[srm__TStatusCode__SRM_USCOREFILE_USCOREPINNED]= "SRM_FILE_PINNED";
    codes[srm__TStatusCode__SRM_USCOREFILE_USCOREIN_USCORECACHE]= "SRM_FILE_IN_CACHE";
    codes[srm__TStatusCode__SRM_USCORESPACE_USCOREAVAILABLE]= "SRM_SPACE_AVAILABLE";
    codes[srm__TStatusCode__SRM_USCORELOWER_USCORESPACE_USCOREGRANTED]= "SRM_LOWER_SPACE_GRANTED";
    codes[srm__TStatusCode__SRM_USCOREDONE]= "SRM_DONE";
    codes[srm__TStatusCode__SRM_USCOREPARTIAL_USCORESUCCESS]= "SRM_PARTIAL_SUCCESS";
    codes[srm__TStatusCode__SRM_USCOREREQUEST_USCORETIMED_USCOREOUT]= "SRM_REQUEST_TIMED_OUT";
    codes[srm__TStatusCode__SRM_USCORELAST_USCORECOPY]= "SRM_LAST_COPY";
    codes[srm__TStatusCode__SRM_USCOREFILE_USCOREBUSY]= "SRM_FILE_BUSY";
    codes[srm__TStatusCode__SRM_USCOREFILE_USCORELOST]= "SRM_FILE_LOST";
    codes[srm__TStatusCode__SRM_USCOREFILE_USCOREUNAVAILABLE]= "SRM_FILE_UNAVAILABLE";
    codes[srm__TStatusCode__SRM_USCORECUSTOM_USCORESTATUS]= "SRM_CUSTOM_STATUS";


    srm__srmStatusOfBringOnlineRequestResponse_ resp;


    if ( soap_call_srm__srmStatusOfBringOnlineRequest(soap, endpoint.c_str(), "srmStatusOfBringOnline", &req, resp) ) {
        soap_print_fault (soap, stderr);
        soap_print_fault_location (soap, stderr);
        return;
    }

    // Output results
    cout << "StatusOfBringonline for request " << req.requestToken << ":" << endl;
    cout << "StatusCode = " << soap_srm__TStatusCode2s(soap, resp.srmStatusOfBringOnlineRequestResponse->returnStatus->statusCode) << endl;
    if ( resp.srmStatusOfBringOnlineRequestResponse->returnStatus->explanation ) {
        cout << "Explanation = " << resp.srmStatusOfBringOnlineRequestResponse->returnStatus->explanation->c_str() << endl;
    }
    if ( resp.srmStatusOfBringOnlineRequestResponse->remainingTotalRequestTime ) {
        cout << "remainingTotalRequestTime = " << *(resp.srmStatusOfBringOnlineRequestResponse->remainingTotalRequestTime) << endl;
    }
    if ( resp.srmStatusOfBringOnlineRequestResponse->remainingDeferredStartTime ) {
        cout << "remainingDeferredStartTime = " << *(resp.srmStatusOfBringOnlineRequestResponse->remainingDeferredStartTime) << endl;
    }

    // Loop over all subrequest...
    if ( resp.srmStatusOfBringOnlineRequestResponse->arrayOfFileStatuses ) {
        std::vector<srm__TBringOnlineRequestFileStatus*> stati =resp.srmStatusOfBringOnlineRequestResponse->arrayOfFileStatuses->statusArray;
        for ( unsigned int i=0; i<stati.size(); i++ ) {
            srm__TBringOnlineRequestFileStatus *fileStatus = stati[i];
            std::cout << "\tsourceSURL = " << fileStatus->sourceSURL.c_str() << std::endl;
            std::cout << "\tstatusCode = " << soap_srm__TStatusCode2s(soap, fileStatus->status->statusCode) << std::endl;
            if ( fileStatus->status->explanation) std::cout << "\texplanation = " << fileStatus->status->explanation->c_str() << std::endl;
            if ( fileStatus->estimatedWaitTime)   std::cout << "\testimatedWaitTime = " << *(fileStatus->estimatedWaitTime) << std::endl;
            if ( fileStatus->fileSize )           std::cout << "\tfileSize = " << *(fileStatus->fileSize) << std::endl;
            if ( fileStatus->remainingPinTime )   std::cout << "\tremainingPinTime = " << *(fileStatus->remainingPinTime) << std::endl;
        }
    }
}

    int
main(int argc, char *argv[])
{
    struct soap* soap = soap_new();

    soap_cgsi_init(soap, CGSI_OPT_DISABLE_NAME_CHECK|CGSI_OPT_DELEG_FLAG);
    soap_set_namespaces(soap, namespaces);

    srm__srmStatusOfBringOnlineRequestRequest request;

    std::string endPoint;

    int carg = 1;
    while (carg < argc-1) {
        if ( strcmp( argv[carg], "-r") == 0 ) {
            request.requestToken.assign(argv[++carg]);
        }
        else if ( strcmp( argv[carg], "-a" ) == 0 ) {
            request.authorizationID = soap_new_std__string(soap, -1);
            request.authorizationID->assign(argv[++carg]);
        }
        else if ( strcmp( argv[carg], "-s" ) == 0 ) {
            if ( !request.arrayOfSourceSURLs ) request.arrayOfSourceSURLs = soap_new_srm__ArrayOfAnyURI(soap, -1);
            std::string  thisSURL = argv[++carg];
            request.arrayOfSourceSURLs->urlArray.push_back(thisSURL);
        }
        else if ( strcmp( argv[carg], "-e") == 0 ) {
            endPoint.assign(argv[++carg]);
        }
        else {
            usage();
            exit (1);
        }
        carg++;
    }

    if (endPoint.length() == 0 || request.requestToken.length() == 0 ) {
        usage();
        exit(1);
    }

    CheckStatus( 
            soap,
            endPoint,
            request
            );
    exit (0);
}
