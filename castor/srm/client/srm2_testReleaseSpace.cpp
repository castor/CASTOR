/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testReleaseSpace.cpp,v 1.10 2008/06/13 14:24:25 sdewitt Exp $

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef DG_DIAGNOSE
#include "diagnose/dg.h"
#endif

#include "soapH.h"
#include "srmNsmap.h"
#define DEFPOLLINT 10

#define SRM_EP_PATH "/v2_1_1/srm"
#include "cgsi_plugin.h"

#include "parsesurl.ic"
#include "soapcallns1.ic"

void usage() {
    std::cerr << "Usage: %s " << std::endl;
    std::cerr << "Available options are (mandatory indicated with *) " << std::endl;
    std::cerr << "\t -e endPoint*        End point of srm" << std::endl;
    std::cerr << "\t -x spaceToken*      The space token to release." << std::endl;
    std::cerr << "\t -F (true|false)     Force existing pins to be released in this space" << std::endl;
    std::cerr << "\t -k key:value        Kay/value pair of extra information" << std::endl;
    std::cerr << "\t -h                  Print this message." << std::endl;
}

    int
main(int argc, char *argv[])
{
    srm__srmReleaseSpaceResponse_ rep;
    srm__srmReleaseSpaceRequest req;
    struct soap* soap = soap_new();
    std::string endPoint;

    if (argc < 3) {
        usage();
        exit (1);
    }

    soap_cgsi_init(soap, CGSI_OPT_DISABLE_NAME_CHECK|CGSI_OPT_DELEG_FLAG);
    soap_set_namespaces(soap, namespaces);

    try {

        req.authorizationID = NULL;
        req.storageSystemInfo = NULL;
        req.forceFileRelease = NULL;

        int cArg = 1;
        while (cArg < argc) {
            switch (argv[cArg][1]) {
                case 'e':
                    endPoint.assign(argv[++cArg]);
                    break;
                case 'x':
                    req.spaceToken.assign(argv[++cArg]);
                    break;
                case 'F':
                    if ( req.forceFileRelease == NULL ) req.forceFileRelease = (bool*)soap_malloc(soap, sizeof(bool));
                    if (strcmp (argv[cArg+1], "true") == 0 ) {
                        *(req.forceFileRelease) = true;
                    }
                    else if ( strcmp (argv[cArg+1], "false") == 0 ) {
                        *(req.forceFileRelease) = false;
                    }
                    else {
                        usage();
                        exit(1);
                    }
                    cArg++;
                    break;
                case 'k':
                    {
                        std::string arg(argv[++cArg]);
                        std::string separator(":");
                        size_t sepP = arg.find(separator);
                        if ( sepP == std::string::npos || 
                                sepP == arg.length() ||
                                sepP == 0) {
                            std::cout << "Invalid key/value pair" << std::endl;
                            usage();
                            exit(1);
                        }
                        std::string key = arg.substr(0, arg.find(separator));
                        std::string val = arg.substr(arg.find(separator)+1, arg.length());
                        if (!req.storageSystemInfo) req.storageSystemInfo = soap_new_srm__ArrayOfTExtraInfo(soap, -1);
                        srm__TExtraInfo *xinfo = soap_new_srm__TExtraInfo(soap, -1);
                        xinfo->key.assign(key);
                        xinfo->value = 0;
                        if ( val.length() > 0 ) {
                            xinfo->value = soap_new_std__string(soap, -1);
                            xinfo->value->assign(val);
                        }
                        req.storageSystemInfo->extraInfoArray.push_back(xinfo);
                    }
                    break;
                case 'h':
                    usage();
                    exit(0);
                default:
                    usage();
                    exit(1);
            }
            cArg++;
        }

        if (endPoint.length() == 0 || req.spaceToken.length() == 0 ) {
            usage();
            exit(1);
        }


        /* To send the request ... */

        if (soap_call_srm__srmReleaseSpace (soap, endPoint.c_str(), "SrmReleaseSpace",
                    &req, rep)) {
            soap_print_fault (soap, stderr);
            soap_print_fault_location (soap, stderr);
            soap_end (soap);
            exit (1);
        }
        srm__TReturnStatus *reqstatp = rep.srmReleaseSpaceResponse->returnStatus;

        std::cout <<  "Request Status: " <<  soap_srm__TStatusCode2s(soap, reqstatp->statusCode) << std::endl;
        if (reqstatp->explanation) std::cout << "Explanation; " << reqstatp->explanation->c_str() << std::endl;
    }
    catch (...) {
    }

    soap_end (soap);
    exit (0);
}
