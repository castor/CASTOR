/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testStatusOfLsRequest.cpp,v 1.1 2009/07/13 14:24:50 sdewitt Exp $

#include "soapH.h"
#include "srmNsmap.h"
#include "cgsi_plugin.h"
#include "map"

using namespace std;

void usage() {
    cout << "Usage: " << endl;
    cout << "\"srm2_testStatusOfLsRequest\"" << std::endl;
    std::cerr << "\t -e endPoint       The end point to query (madatory)" << std::endl;
    std::cerr << "\t -r requestToken   Request token returned from PrepareToPut (madatory) " << std::endl; 
    std::cerr << "\t -o offset         Offset into result set" << std::endl;
    std::cerr << "\t -c count          Number of elements to return" << std::endl;
}

void printDetails( struct soap* soap, std::vector<srm__TMetaDataPathDetail*> details, std::string indent) {
    for ( unsigned i=0; i<details.size(); i++ ) {
        std::cout << indent << "Path: " << details[i]->path << std::endl;
        std::cout << indent << "Status: " << soap_srm__TStatusCode2s(soap, details[i]->status->statusCode) << std::endl;
        if ( details[i]->status->explanation) 
            std::cout << indent << "Explanation : " << details[i]->status->explanation->c_str() << std::endl;
        if ( details[i]->size ) {
            std::cout << indent << indent << "size: " << *(details[i]->size) << std::endl;
        }
        if ( details[i]->createdAtTime ) {
            std::cout << indent << indent << "ctime=" << *(details[i]->createdAtTime) << std::endl;
        }
        if ( details[i]->lastModificationTime) {
            std::cout << indent << indent << "mtime=" << *(details[i]->lastModificationTime) << std::endl;
        }
        if ( details[i]->fileStorageType ) {
            std::cout << indent << indent << "fileStorageType=" << *(details[i]->fileStorageType) << std::endl;
        }
        if ( details[i]->retentionPolicyInfo ) {
            std::cout << indent << indent << "retentionPolicy: " << details[i]->retentionPolicyInfo->retentionPolicy << std::endl;
            if ( details[i]->retentionPolicyInfo->accessLatency ) 
                std::cout << indent << indent << "accessLatency: " << *(details[i]->retentionPolicyInfo->accessLatency) << std::endl;
        }
        //if ( details[i]->fileLocality ) std::cout << indent << indent << "fileLocality=" << *(details[i]->fileLocality) << std::endl;
        if ( details[i]->fileLocality ) {
            std::cout << indent << indent << "fileLocality="; 
            switch ((int)(*(details[i]->fileLocality))) {
                case 0:
                    std::cout << "ONLINE" << std::endl;
                    break;
                case 1:
                    std::cout << "NEARLINE" << std::endl;
                    break;
                case 2:
                    std::cout << "ONLINE_AND_NEARLINE" << std::endl;
                    break;
                case 3:
                    std::cout << "LOST" << std::endl;
                    break;
                case 4:
                    std::cout << "NONE" << std::endl;
                    break;
                case 5:
                    std::cout << "UNAVAILABLE" << std::endl;
                    break;
                default:
                    std::cout << "UNKNOWN" << std::endl;
                    break;
            }
        }
        if ( details[i]->arrayOfSpaceTokens ) {
            std::cout << indent << indent << "SpaceToken: ";
            for ( unsigned token = 0; token<details[i]->arrayOfSpaceTokens->stringArray.size(); token++) {
                std::cout << details[i]->arrayOfSpaceTokens->stringArray[token] << ", ";
            }
            std::cout << std::endl;
        }
        if ( details[i]->type ) std::cout << indent << indent << "fileType=" << *(details[i]->type) << std::endl;
        if ( details[i]->ownerPermission ) {
            std::cout << indent << indent << "ownerId =" << details[i]->ownerPermission->userID << std::endl;
            std::cout << indent << indent << "ownerPermission=" << details[i]->ownerPermission->mode << std::endl;
        }
        if ( details[i]->groupPermission ) std::cout << indent << indent << "groupPermission=" << details[i]->groupPermission->mode << std::endl;
        if ( details[i]->otherPermission ) std::cout << indent << indent << "other: perm=" << *(details[i]->otherPermission) << std::endl;
        if ( details[i]->checkSumType ) std::cout << indent << indent << "checkSumType=" << details[i]->checkSumType->c_str() << std::endl;
        if ( details[i]->checkSumValue ) std::cout << indent << indent << "checkSumValue=" << details[i]->checkSumValue->c_str() << std::endl;
        if ( details[i]->arrayOfSubPaths ) {
            std::vector<srm__TMetaDataPathDetail*> subdetails = details[i]->arrayOfSubPaths->pathDetailArray;
            printDetails( soap, subdetails, indent + "  " );
        }
    }
}

int CheckStatusOfRequest(struct soap *soap,
        std::string endPoint, 
        srm__srmStatusOfLsRequestRequest req) {

    srm__srmStatusOfLsRequestResponse_ resp;

    int status = -1;

    if ( soap_call_srm__srmStatusOfLsRequest(soap, endPoint.c_str(), "srmStatusOfLsRequest", &req, resp) ) {
        soap_print_fault (soap, stderr);
        soap_print_fault_location (soap, stderr);
        exit (1);
    }

    std::cout << "Status of request " << req.requestToken << ": " << soap_srm__TStatusCode2s(soap, resp.srmStatusOfLsRequestResponse->returnStatus->statusCode) << std::endl;
    if ( resp.srmStatusOfLsRequestResponse->returnStatus->explanation != NULL && resp.srmStatusOfLsRequestResponse->returnStatus->explanation->length() > 0 ) {
        std::cout << "Explanation: " << resp.srmStatusOfLsRequestResponse->returnStatus->explanation->c_str() << std::endl;
    }
    if ( resp.srmStatusOfLsRequestResponse->details ) printDetails(soap, resp.srmStatusOfLsRequestResponse->details->pathDetailArray, "  ");

    return status;
}


    int
main(int argc, char*argv[])
{
    struct soap* soap = soap_new();

    soap_cgsi_init(soap, CGSI_OPT_DISABLE_NAME_CHECK|CGSI_OPT_DELEG_FLAG);
    soap_set_namespaces(soap, namespaces);

    srm__srmStatusOfLsRequestRequest request;

    request.authorizationID    = NULL;
    request.offset             = NULL;
    request.count              = NULL;

    std::string endPoint;
    int carg = 1; // Current argument
    while (carg < argc-1) {
        if ( strcmp( argv[carg], "-r") == 0 ) {
            request.requestToken.assign(argv[++carg]);
        }
        else if ( strcmp( argv[carg], "-a" ) == 0 ) {
            request.authorizationID = soap_new_std__string(soap, -1);
            request.authorizationID->assign(argv[++carg]);
        }
        else if ( strcmp( argv[carg], "-o" ) == 0 ) {
            if ( !request.offset ) request.offset = (int*)soap_malloc(soap, sizeof(int));
            *(request.offset) = atoi(argv[++carg]);
        }
        else if ( strcmp( argv[carg], "-c" ) == 0 ) {
            if ( !request.count ) request.count = (int*)soap_malloc(soap, sizeof(int));
            *(request.count) = atoi(argv[++carg]);
        }
        else if ( strcmp( argv[carg], "-e") == 0 ) {
            endPoint.assign(argv[++carg]);
        }
        else {
            usage();
            exit (1);
        }
        carg++;
    }

    if (endPoint.length() == 0 || request.requestToken.length() == 0 ) {
        usage();
        exit(1);
    }

    CheckStatusOfRequest( soap, endPoint, request );
}
