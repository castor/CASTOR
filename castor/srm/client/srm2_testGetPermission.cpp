/*
 * Copyright (C) 2004-2005 by CERN/IT/GD/CT & CNRS/IN2P3/LAL
 * All rights reserved
 */

// $Id: srm2_testGetPermission.cpp,v 1.5 2007/01/31 16:51:17 sdewitt Exp $

#include "soapH.h"
#include "srmNsmap.h"
#include "cgsi_plugin.h"



void usage() {
    std::cout << "Usage: " << std::endl;
    std::cout << "\tsrm2_testSetPermission -e endPoint [-s SURL]+  (-a authorizationID) (-k key:value) (-h)" << std::endl;
}

    int
main(int argc, char *argv[])
{
    srm__srmGetPermissionResponse_ rep;
    srm__srmGetPermissionRequest req;
    struct soap* soap = soap_new();
    std::string endPoint;

    req.authorizationID   = NULL;
    req.arrayOfSURLs      = NULL;
    req.storageSystemInfo = NULL;

    int cArg=1;
    while (cArg < argc) {
        if (std::string(argv[cArg]) == "-h") {
            usage();
            exit(0);
        }
        else if ( std::string(argv[cArg]) == "-e") {
            endPoint.assign(argv[++cArg]);
        }
        else if (std::string(argv[cArg]) == "-s") {
            if (!req.arrayOfSURLs)
                req.arrayOfSURLs = soap_new_srm__ArrayOfAnyURI(soap, -1);
            req.arrayOfSURLs->urlArray.push_back(argv[++cArg]);
        }
        else if ( std::string(argv[cArg]) == "-a") {
            if (!req.authorizationID) req.authorizationID = soap_new_std__string(soap, -1);
            req.authorizationID->assign(argv[++cArg]);
        }
        else if ( std::string(argv[cArg]) == "-k" ) {
            std::string arg(argv[++cArg]);
            std::string separator(":");
            size_t sepP = arg.find(separator);
            if ( sepP == std::string::npos || 
                    sepP == arg.length() ||
                    sepP == 0) {
                std::cout << "Invalid key/value pair" << std::endl;
                usage();
                exit(1);
            }
            std::string key = arg.substr(0, arg.find(":"));
            std::string val = arg.substr(arg.find(":")+1, arg.length());
            if (!req.storageSystemInfo) 
                req.storageSystemInfo = soap_new_srm__ArrayOfTExtraInfo(soap, -1);
            srm__TExtraInfo *xinfo = soap_new_srm__TExtraInfo(soap, -1);
            xinfo->key.assign(key);
            xinfo->value = 0;
            if ( val.length() > 0 ) {
                xinfo->value = soap_new_std__string(soap, -1);
                xinfo->value->assign(val);
            }
            req.storageSystemInfo->extraInfoArray.push_back(xinfo);
        }
        else {
            std::cerr << "Invalid Argument " << argv[cArg] << std::endl;
            usage();
            exit(1);
        }
        ++cArg;
    }

    if (endPoint.length() == 0) {
        std::cerr << "No endpoint supplied." << std::endl;
        usage();
        exit(1);
    }

    soap_cgsi_init(soap, CGSI_OPT_DISABLE_NAME_CHECK|CGSI_OPT_DELEG_FLAG);
    soap_set_namespaces(soap, namespaces);

    /* To send the request ... */

    if (soap_call_srm__srmGetPermission (soap, const_cast<char*>(endPoint.c_str()), "GetPermission", &req, rep)) {
        soap_print_fault (soap, stderr);
        soap_print_fault_location (soap, stderr);
        soap_end (soap);
        exit (1);
    }

    srm__TReturnStatus* status = rep.srmGetPermissionResponse->returnStatus;

    std::cout << "Request Status: " << soap_srm__TStatusCode2s(soap, status->statusCode) << std::endl;
    if ( status->explanation ) 
        std::cout << "Explanation :" << status->explanation->c_str() << std::endl;
    
    if (rep.srmGetPermissionResponse->arrayOfPermissionReturns) {
        std::vector<srm__TPermissionReturn*> permissions = rep.srmGetPermissionResponse->arrayOfPermissionReturns->permissionArray;

        for (unsigned int i=0; i<permissions.size(); i++ ) {
            srm__TPermissionReturn *thisP = permissions[i];

            std::cout << "SURL: " << thisP->surl << std::endl;
            std::cout << "\tStatus: " << soap_srm__TStatusCode2s(soap, thisP->status->statusCode) << std::endl;
            std::cout << "\tExplanation: " << thisP->status->explanation->c_str() << std::endl;
            std::cout << "\tOwner: " << thisP->owner->c_str() << std::endl;
            if ( thisP->ownerPermission ) {
                std::cout << "\tOwnerP: " << *(thisP->ownerPermission) << std::endl;
            }
            if ( thisP->arrayOfUserPermissions ) {
                for ( unsigned int i=0; i<thisP->arrayOfUserPermissions->userPermissionArray.size(); i++ ) {
                    srm__TUserPermission *userP = thisP->arrayOfUserPermissions->userPermissionArray[i];
                    std::cout << "\tUser Permission: (" << userP->userID << ", " << userP->mode << ")" << std::endl;
                }
            }
            else {
                std::cout << "\tNo user permissions" << std::endl;
            }
            if ( thisP->arrayOfGroupPermissions ) {
                for ( unsigned int i=0; i<thisP->arrayOfGroupPermissions->groupPermissionArray.size(); i++ ) {
                    srm__TGroupPermission *groupP = thisP->arrayOfGroupPermissions->groupPermissionArray[i];
                    std::cout << "\tGroup Permission: (" << groupP->groupID << ", " << groupP->mode << ")" << std::endl;
                }
            }
            else {
                std::cout << "\tNo group permissions" << std::endl;
            }
            if (thisP->otherPermission) {
                std::cout << "\tOtherP: " << *(thisP->otherPermission) << std::endl;
            }
        }
    }
    soap_end (soap);
    exit (0);
}
