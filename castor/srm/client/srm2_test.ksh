#!/bin/ksh

file=results.out #_`date +%Y%m%d%H%M%S`.out
endpoint=
domainname=`hostname -d`
castorbase=castor/$domainname;
clientDir=/usr/bin
userdir=${CASTOR_HOME}
recallTokenD=
spaceTokenD=

usage() {
    print "Usage test.ksh -e endPoint -p path [-o file] [-c clientDir] [-h]";
    print "\t-e - The endpoint under test";
    print "\t-p - The path (castor directory) to write files into for testing";
    print "\t-o - The output file to write results to (default is results.out)";
    print "\t-c - The directory containing the clients.  Otherwise \$PATH will be used.";
    print "\t-x - A generic space token that can be used for put and get tests with a defined token.  If not provided, tests will not be done"; 
    print "\t-d - The space token description to be used for tape recall tests.  If not provided, recall tests will not be done"; 
    print "\t-h - Print this message"
}

# Print result of a test.  
# Single argument, true or false indicating the expected result
# of a test (some tests may expect a failure, but the result should print OK)
printStatus() {
    case $status in
        0) echo "SRM_SUCCESS";;
        1) echo "SRM_FAILURE";;
        2) echo "SRM_AUTHENTICATION_FAILURE";;
        3) echo "SRM_AUTHORIZATION_FAILURE";;
        4) echo "SRM_INVALID_REQUEST";;
        5) echo "SRM_INVALID_PATH";;
        6) echo "SRM_FILE_LIFETIME_EXPIRED";;
        7) echo "SRM_SPACE_LIFETIME_EXPIRED";;
        8) echo "SRM_EXCEED_ALLOCATION";;
        9) echo "SRM_NO_USER_SPACE";;
        10) echo "SRM_NO_FREE_SPACE";;
        11) echo "SRM_DUPLICATION_ERROR";;
        12) echo "SRM_NON_EMPTY_DIRECTORY";;
        13) echo "SRM_TOO_MANY_RESULTS";;
        14) echo "SRM_INTERNAL_ERROR";;
        15) echo "SRM_FATAL_INTERNAL_ERROR";;
        16) echo "SRM_NOT_SUPPORTED";;
        17) echo "SRM_REQUEST_QUEUED";;
        18) echo "SRM_REQUEST_INPROGRESS";;
        19) echo "SRM_REQUEST_SUSPENDED";;
        20) echo "SRM_ABORTED";;
        21) echo "SRM_RELEASED";;
        22) echo "SRM_FILE_PINNED";;
        23) echo "SRM_FILE_IN_CACHE";;
        24) echo "SRM_SPACE_AVAILABLE";;
        25) echo "SRM_LOWER_SPACE_GRANTED";;
        26) echo "SRM_DONE";;
        27) echo "SRM_PARTIAL_SUCCESS";;
        27) echo "SRM_REQUEST_TIMED_OUT";;
        28) echo "SRM_LAST_COPY";;
        29) echo "SRM_FILE_BUSY";;
        30) echo "SRM_FILE_LOST";;
        31) echo "SRM_FILE_UNAVAILABLE";;
        32) echo "SRM_CUSTOM_STATUS";;
        *) echo $status;;
    esac
}

checkValue () {
# Find last occuring instance of string
    field=`grep -n $1 $file | sort -nr | cut -d \  -f 3`;
    if [[ $2 != $field ]]; then
        if [[ $field != "NULL" ]]; then
            echo "checkVlaue found $field; expected $2";
            return 1;
        fi;
    fi;
    return 0;
}

checkTotalSize() {
    start=`grep -n "Test $1" ${file} | cut -d : -f1`;
    line=$(($start + 3));
    size=`head -$line ${file} | tail -1 | cut -d \  -f 3`;
    if [[ $size != $2 ]]; then
        echo "Expected total size of $2; found $size"
        return 1;
    fi;
    return 0;
}

checkGuaranteed() {
    start=`grep -n "Test $1" ${file} | cut -d : -f1`;
    line=$(($start + 4));
    size=`head -$line ${file} | tail -1 | cut -d \  -f 3`;
    if [[ $size != $2 ]]; then
        echo "Expected guaranteed size of $2; found $size"
        return 1;
    fi;
    return 0;
}

checkLifetime() {
    start=`grep -n "Test $1" ${file} | cut -d : -f1`;
    line=$(($start + 5));
    size=`head -$line ${file} | tail -1 | cut -d \  -f 3`;
    if [[ $size != $2 ]]; then
        if [[ $size -ne "NULL" ]]; then
            echo "Expected lifetime of $2; found $size";
            return 1;
        fi;
    fi;
    return 0;
}

validateParams() {
    if [[ $2 != -1 ]]; then
        checkTotalSize $1 $2;
       if [[ $? != 0 ]]; then return 1; fi;
    fi;
    if [[ $3 != -1 ]]; then
        checkGuaranteed $1 $3;
        if [[ $? != 0 ]]; then return 1; fi;
    fi;
    if [[ $4 != -1 ]]; then
        checkLifetime $1 $4;
        if [[ $? != 0 ]]; then return 1; fi;
    fi;
}

checkSpaceToken() {
    st=`tail -1 ${file}`;
    count=`grep -c ${st} ${file}`;
    if [[ $count == 2 ]]; then
        return 0;
    fi;
    return 1;
}

checkForSOAPError() {
    status=`grep -c CGSI ${file}`;
}

getRequestToken() {
    requestToken=`grep 'requestToken' ${file} | tail -1 | awk '{print $3}'`;
}

getTURL() {
    turl=`grep 'transferURL' $file | tail -1 | awk '{print $3}'`;
}

getStatus() {
    status=`grep 'Status' $file | tail -1 | awk '{print $NF}'`;
}

getSpaceToken() {
    spaceToken=`grep 'Space Token' $file | tail -1 | awk '{print $NF}'`;
}

getFileLocality() {
    fileLocality=`grep 'fileLocality' $file | tail -1 | awk 'BEGIN {FS="=";} {print $2}'`
}

putCycle() {
    echo "$1 Starting PUT cycle with space token " $spaceToken;
    echo -n "$1    Issuing PrepareToPut...";
    echo ${clientDir}/srm2_testPrepareToPut -e ${endpoint} -s ${endpoint}${userdir}/${testfile} -p gsiftp -x $spaceToken >> ${file};
    ${clientDir}/srm2_testPrepareToPut -e ${endpoint} -s ${endpoint}${userdir}/${testfile} -p gsiftp -x $spaceToken >> ${file} 2>&1;
    getStatus;
    printStatus;
    if [[ ${status} != "17" ]]; then
        return $status;
    fi;
    getRequestToken;
    iter=0;
    interrupt=0;
    trap interrupt=1 2
    echo -n "$1    Waiting for TURL.";
    while (( !interrupt )); do
         echo ${clientDir}/srm2_testStatusOfPutRequest -e ${endpoint} -r ${requestToken} >> ${file};
         ${clientDir}/srm2_testStatusOfPutRequest -e ${endpoint} -r ${requestToken} >> ${file} 2>&1;
         sync;
         getStatus;
         if [[ ${status} == "SRM_SUCCESS" ]]; then
             printStatus;
             getTURL;
             echo -n "$1    Performing transfer...";
# Create a file for xfer
             dd if=/dev/zero of=/tmp/bigfile bs=1048576 count=1000 > /dev/null 2>&1;
             globus-url-copy file:///tmp/bigfile ${turl};
             echo "$1    ...done";
             echo -n "$1    Performing putDone...";
             echo ${clientDir}/srm2_testPutDone -e ${endpoint} -r ${requestToken} -s ${endpoint}/${userdir}/${testfile} >> ${file};
             ${clientDir}/srm2_testPutDone -e ${endpoint} -r ${requestToken} -s ${endpoint}/${userdir}/${testfile} >> ${file} 2>&1;
             getStatus;
             printStatus;
             break;
         elif [[ ${status} != "SRM_REQUEST_QUEUED" && ${status} != "SRM_REQUEST_INPROGESS" ]]; then
             interrupt=1;
             break;
         fi
         sleep 5;
         echo -n ".";
    done;
}

getCycle() {
    echo "Starting GET cycle with space token " $spaceToken;
    echo -n "    Issuing PrepareToGet...";
    ${clientDir}/srm2_testPrepareToGet -s ${endpoint}${userdir}/${testfile} -p gsiftp -x $spaceToken >> ${file} 2>&1;
    getStatus;
    printStatus;
    if [[ ${status} != "17" ]]; then
        return $status;
    fi;
    getRequestToken;
    iter=0;
    interrupt=0;
    trap interrupt=1 2
    echo -n "    Waiting for TURL.";
    while (( !interrupt )); do
         ${clientDir}/srm2_testStatusOfGetRequest -e ${endpoint} -r ${requestToken} >> ${file} 2>&1;
         sync;
         getStatus;
         if [[ ${status} == "SRM_SUCCESS" ]]; then
             printStatus;
             getTURL;
             echo -n "    Performing transfer...";
             globus-url-copy ${turl} file:///tmp/fromCASTOR
             echo "    ...done";
             break;
         fi
         sleep 5;
         echo -n ".";
    done;
}

##############################################################################
# Main function.  Goes over a whole bunch of tests.  Not all tests need be
# run.  The user can start tests at some random point, or just go to a 
# specific section.  So the usage is something like:
        #    tests.ksh [--start-at i] [--section <space|dir|...>
##############################################################################
if [[ -e ${file} ]]; then
    rm -f ${file}
fi

# Parse arguments:
# Arguments should be:
#    -e  endPoint  - the srm end point to use.
#    -p  path      - directory in which to create files etc during testing
#    -o  file      - file in which results get logged (optional)
#    -c  clientDir - Directory containing client executables (optional)
#    -d  descriptn - Space token description to be used for testing
#    -t            - svcClass is tape backed (allows recall tests)
#    -h            - print mesage with argument list
# First make sure all arguments are present
if [[ $# < 4 ]]; then
    usage;
    exit 1;
fi

clientDir=/usr/bin;
spaceToken="";

while getopts "e:p:o:c:d:x:h:" arg
do
    case $arg in
        e)
            endpoint=$OPTARG
            ;;
        p)
            userdir=$OPTARG
            ;;
        o)
            file=$OPTARG
            ;;
        c)
            clientDir=$OPTARG
            ;;
        d)
            recallTokenD=$OPTARG
            ;;
        x)
            spaceTokenD=$OPTARG
            ;;
        h)
            usage
            exit 1
            ;;
    esac
done;

#echo "endpoint="$endpoint;
#echo "userdir="$userdir;
#echo "file="$file;
#echo "clientDir="$clientDir;
#echo "recallTokenD="$recallTokenD;

echo "Results logged in ${file}"

#Quick PUT test...
testfile=testfile_`date +%Y%m%d%H%M%S`
testdir=testdir_`date +%Y%m%d%H%M%S`

echo ">>>Test Ping" > ${file}
echo -n "Testing srmPing [SRM_SUCCESS] - "
echo ${clientDir}/srm2_testPing -e ${endpoint}  >> ${file}
${clientDir}/srm2_testPing -e ${endpoint} >> ${file} 2>&1
checkForSOAPError
printStatus
if [[ $status -ne 0 ]]; then
    echo "Unable to contact end point - giving up";
    exit 1;
fi



echo ">>>Test PrepareToPut" >> ${file}
echo -n "Testing PrepareToPut [SRM_REQUEST_QUEUED] - "
echo ${clientDir}/srm2_testPrepareToPut -e ${endpoint} -s ${endpoint}${userdir}/${testfile} -p gsiftp >> ${file}
${clientDir}/srm2_testPrepareToPut -e ${endpoint} -s ${endpoint}${userdir}/${testfile} -p gsiftp >> ${file} 2>&1
getStatus
printStatus

getRequestToken

iter=0
echo -n "Testing StatusOfPut [SRM_SUCCESS] - "
echo ${clientDir}/srm2_testStatusOfPutRequest -e ${endpoint} -r ${requestToken} >> ${file}
while [[ ${iter} -le 18 ]];
do
    (( iter += 1 ));
    ${clientDir}/srm2_testStatusOfPutRequest -e ${endpoint} -r ${requestToken} >> ${file} 2>&1
    getStatus
    if [[ ${status} == "SRM_SUCCESS" || ${status} == "SRM_FAILURE" ]]; then
        break;
    fi;
    echo -n "."
    sleep 10
done;
printStatus
if [[ ${status} != "SRM_SUCCESS" ]]; then
   echo "\nPUT request failed - aborting";
   echo -n "Testing srmAbortRequest [SRM_SUCCESS] - "
   echo ${clientDir}/srm2_testAbortRequest -e ${endpoint} -r ${requestToken} >> ${file}
   ${clientDir}/srm2_testAbortRequest -e ${endpoint} -r ${requestToken} >> ${file}
   getStatus;
   printStatus;
   exit 1;
fi

echo -n "Repeating Testing StatusOfPut [SRM_SUCCESS] - "
echo ${clientDir}/srm2_testStatusOfPutRequest -e ${endpoint} -r ${requestToken} >> ${file}
${clientDir}/srm2_testStatusOfPutRequest -e ${endpoint} -r ${requestToken} >> ${file} 2>&1
getStatus
printStatus

if [[ ${status} == "SRM_SUCCESS" ]]; then
   getTURL

   globus-url-copy file:///etc/hosts ${turl} >> ${file} 2>&1

    echo -n "Testing putDone [SRM_SUCCESS] - ";
    echo ${clientDir}/srm2_testPutDone -e ${endpoint} -r ${requestToken} -s ${endpoint}/${userdir}/${testfile} >> ${file}
    ${clientDir}/srm2_testPutDone -e ${endpoint} -r ${requestToken} -s ${endpoint}/${userdir}/${testfile} >> ${file} 2>&1
    printStatus
fi


# Now do a basic GET request - but we won't bother with the xfer
echo -n "Testing PrepareToGet request [SRM_REQUEST_QUEUED] - "
echo ${clientDir}/srm2_testPrepareToGet -s ${endpoint}${userdir}/${testfile} -p gsiftp >> ${file}
${clientDir}/srm2_testPrepareToGet -s ${endpoint}${userdir}/${testfile} -p gsiftp >> ${file} 2>&1
getStatus
printStatus

iter=0
getRequestToken
echo -n "Testing StatusOfGet [SRM_SUCCESS] - "
echo ${clientDir}/srm2_testStatusOfGetRequest -e ${endpoint} -r ${requestToken} >> ${file}
while [[ $iter -le 10 ]];
do
    (( iter += 1 ));
    ${clientDir}/srm2_testStatusOfGetRequest -e ${endpoint} -r ${requestToken} >> ${file} 2>&1
    getStatus
    if [[ ${status} == "SRM_SUCCESS" ]]; then
        break;
    fi;
    echo -n ".";
    sleep 5;
done;
printStatus
if [[ ${status} != "SRM_SUCCESS" ]]; then
   echo "\nGET request failed - aborting";
   echo -n "Testing srmAbortRequest [SRM_SUCCESS] - "
   echo ${clientDir}/srm2_testAbortRequest -e ${endpoint} -r ${requestToken} >> ${file}
   ${clientDir}/srm2_testAbortRequest -e ${endpoint} -r ${requestToken} >> ${file}
   getStatus;
   printStatus;
   exit 1;
fi

echo -n "Repeating Testing StatusOfGet [SRM_SUCCESS] - "
echo ${clientDir}/srm2_testStatusOfGetRequest -e ${endpoint} -r ${requestToken} >> ${file}
${clientDir}/srm2_testStatusOfGetRequest -e ${endpoint} -r ${requestToken} >> ${file} 2>&1
getStatus
printStatus

# Try releasing the file...
echo -n "Testing ReleaseFile [SRM_SUCCESS] - "
echo ${clientDir}/srm2_ReleaseFiles -e ${endpoint} -r ${requestToken} -s ${endpoint}${userdir}/${testfile} >> ${file}
${clientDir}/srm2_testReleaseFiles -e ${endpoint} -r ${requestToken} -s ${endpoint}${userdir}/${testfile} >> ${file} 2>&1
getStatus
printStatus

echo -n "Testing srmLs (verbose, non-recursive) [SRM_SUCCESS] - "
echo ${clientDir}/srm2_testLs -e ${endpoint} -s ${endpoint}${userdir}/${testfile} -V >> ${file}
${clientDir}/srm2_testLs -e ${endpoint} -s ${endpoint}${userdir}/${testfile} -V >> ${file} 2>&1
getStatus
printStatus

echo -n "Testing srmLs (brief, recursive) [SRM_SUCCESS] - "
echo ${clientDir}/srm2_testLs -e ${endpoint} -s ${endpoint}${userdir} >> ${file}
${clientDir}/srm2_testLs -e ${endpoint} -s ${endpoint}${userdir} >> ${file} 2>&1
getStatus
printStatus

echo -n "Testing srmLs (invalid directory, offset=1, count=1) [SRM_NOT_SUPPORTED] - "
echo ${clientDir}/srm2_testLs -e ${endpoint} -s ${endpoint}/ >> ${file}
${clientDir}/srm2_testLs -e ${endpoint} -s ${endpoint}/ >> ${file} 2>&1
getStatus
printStatus

echo -n "Testing srmMkdir [SRM_SUCCESS] - "
echo ${clientDir}/srm2_testMkdir -e ${endpoint} -s ${endpoint}${userdir}/${testdir} >> ${file}
${clientDir}/srm2_testMkdir -e ${endpoint} -s ${endpoint}${userdir}/${testdir} >> ${file} 2>&1
getStatus
printStatus

echo -n "Testing srmMkdir (trying to create test in SAroot) [SRM_AUTHORIZATION_FAILURE] - "
echo ${clientDir}/srm2_testMkdir -e ${endpoint} -s ${endpoint}/castor/${testdir} >> ${file}
${clientDir}/srm2_testMkdir -e ${endpoint} -s ${endpoint}/castor/${testdir} >> ${file} 2>&1
getStatus
printStatus


echo -n "Testing srmRmdir [SRM_SUCCESS] - "
echo ${clientDir}/srm2_testRmdir ${endpoint}${userdir}/${testdir} >> ${file}
${clientDir}/srm2_testRmdir ${endpoint}${userdir}/${testdir} >> ${file} 2>&1
getStatus
printStatus

testfile1=testfile_`date +%Y%m%d%H%M%S`
echo -n "Testing srmCopy [SRM_REQUEST_QUEUED] - "
echo ${clientDir}/srm2_testCopy -e ${endpoint} -s ${endpoint}${userdir}/${testfile} -t ${endpoint}${userdir}/${testfile1}  >> ${file}
${clientDir}/srm2_testCopy -e ${endpoint} -s ${endpoint}${userdir}/${testfile} -t ${endpoint}${userdir}/${testfile1}  >> ${file} 2>&1
getStatus
printStatus

iter=0
getRequestToken
echo -n "Testing StatusOfCopyRequest [SRM_SUCCESS] - "
echo ${clientDir}/srm2_testStatusOfCopyRequest -e ${endpoint} -r ${requestToken} >> ${file}
while [[ $iter -le 10 ]];
do
    (( iter += 1 ));
    ${clientDir}/srm2_testStatusOfCopyRequest -e ${endpoint} -r ${requestToken} >> ${file} 2>&1
    getStatus
    if [[ ${status} == "SRM_SUCCESS" ]]; then
        break;
    fi;
    if [[ ${status} == "SRM_FAILURE" ]]; then
        break;
    fi;
    echo -n ".";
    sleep 10;
done;
printStatus
if [[ ${status} != "SRM_SUCCESS" ]]; then
   echo "\nCOPY request failed - aborting";
   echo -n "Testing srmAbortRequest [SRM_SUCCESS] - "
   echo ${clientDir}/srm2_testAbortRequest -e ${endpoint} -r ${requestToken} >> ${file}
   ${clientDir}/srm2_testAbortRequest -e ${endpoint} -r ${requestToken} >> ${file}
   getStatus;
   printStatus;
else
   echo -n "Testing srmRm [SRM_SUCCESS] - "
   echo ${clientDir}/srm2_testRm -e ${endpoint} -s ${endpoint}${userdir}/${testfile1} >> ${file}
   ${clientDir}/srm2_testRm -e ${endpoint} -s ${endpoint}${userdir}/${testfile1} >> ${file} 2>&1
   getStatus
   printStatus
fi

if [[ -n $spaceTokenD ]]; then
    echo -n "Test srmGetSpaceToken [SRM_SUCCESS] - "
    echo ${clientDir}/srm2_testGetSpaceToken ${endpoint} ${spaceTokenD}  >> ${file}
    ${clientDir}/srm2_testGetSpaceToken ${endpoint} ${spaceTokenD} >> ${file} 2>&1
    getStatus
    printStatus;
    getSpaceToken;
    
   echo -n "Testing srmCopy with target space token [SRM_REQUEST_QUEUED] - "
   echo ${clientDir}/srm2_testCopy -e ${endpoint} -s ${endpoint}${userdir}/${testfile} -t ${endpoint}${userdir}/${testfile1}  -x ${spaceToken} >> ${file}
   ${clientDir}/srm2_testCopy -e ${endpoint} -s ${endpoint}${userdir}/${testfile} -t ${endpoint}${userdir}/${testfile1} -x ${spaceToken}  >> ${file} 2>&1
   getStatus
   printStatus

   iter=0
   getRequestToken
   echo -n "Testing StatusOfCopyRequest [SRM_SUCCESS] - "
   echo ${clientDir}/srm2_testStatusOfCopyRequest -e ${endpoint} -r ${requestToken} >> ${file}
   while [[ $iter -le 10 ]];
   do
      (( iter += 1 ));
      ${clientDir}/srm2_testStatusOfCopyRequest -e ${endpoint} -r ${requestToken} >> ${file} 2>&1
      getStatus
      if [[ ${status} == "SRM_SUCCESS" ]]; then
        break;
      fi;
      if [[ ${status} == "SRM_FAILURE" ]]; then
        break;
      fi;
      echo -n ".";
      sleep 10;
  done;
  printStatus
  if [[ ${status} != "SRM_SUCCESS" ]]; then
    echo "\nCOPY request failed - aborting";
    echo -n "Testing srmAbortRequest [SRM_SUCCESS] - "
    echo ${clientDir}/srm2_testAbortRequest -e ${endpoint} -r ${requestToken} >> ${file}
    ${clientDir}/srm2_testAbortRequest -e ${endpoint} -r ${requestToken} >> ${file}
    getStatus;
    printStatus;
  else
    echo -n "Testing srmRm [SRM_SUCCESS] - "
    echo ${clientDir}/srm2_testRm -e ${endpoint} -s ${endpoint}${userdir}/${testfile1} >> ${file}
    ${clientDir}/srm2_testRm -e ${endpoint} -s ${endpoint}${userdir}/${testfile1} >> ${file} 2>&1
    getStatus
    printStatus
 fi
fi

echo -n "Testing srmMv [SRM_SUCCESS] - "
echo ${clientDir}/srm2_testMv ${endpoint}${userdir}/${testfile} ${endpoint}${userdir}/${testfile1}  >> ${file}
${clientDir}/srm2_testMv ${endpoint}${userdir}/${testfile} ${endpoint}${userdir}/${testfile1} >> ${file} 2>&1
getStatus
printStatus

echo -n "Testing srmRm [SRM_SUCCESS] - "
echo ${clientDir}/srm2_testRm -e ${endpoint} -s ${endpoint}${userdir}/${testfile1} >> ${file}
${clientDir}/srm2_testRm -e ${endpoint} -s ${endpoint}${userdir}/${testfile1} >> ${file} 2>&1
getStatus
printStatus

#Test minimal input parameters
echo -n "Test srmReserveSpace [SRM_AUTHORIZATION_FAILURE | SRM_SUCCESS] - "
echo ${clientDir}/srm2_testReserveSpace -e ${endpoint} -t 0 -c "foo" >> ${file}
${clientDir}/srm2_testReserveSpace -e ${endpoint} -t 0 -c "foo" >> ${file} 2>&1
getStatus
printStatus;

if [[ $status = "SRM_SUCCESS" ]]; then
    echo -n "Test srmGetSpaceToken - "
    echo ${clientDir}/srm2_testGetSpaceToken ${endpoint} "foo"  >> ${file}
    ${clientDir}/srm2_testGetSpaceToken ${endpoint} "foo" >> ${file} 2>&1
    getStatus
    printStatus;
    getSpaceToken;
else 
    echo "Skipping srmGetSpaceToken test since could not reserve space.";
    echo "Test srmGetSpaceToken - Skipped" >> ${file} 2>&1;
fi

if [[ -n $spaceToken ]]; then
    echo -n "Test srmStatusOfReserveSpace - "
    echo ${clientDir}/srm2_testStatusOfReserveSpaceRequest -e ${endpoint} -r $spaceToken >> ${file}
    ${clientDir}/srm2_testStatusOfReserveSpaceRequest -e ${endpoint} -r $spaceToken >> ${file} 2>&1
    getStatus
    printStatus;

    echo -n "Test srmGetSpaceMetadata - "
    echo ${clientDir}/srm2_testGetSpaceMetaData ${endpoint} ${spaceToken} >> ${file}
    ${clientDir}/srm2_testGetSpaceMetaData ${endpoint} ${spaceToken} >> ${file} 2>&1
    getStatus
    printStatus;

    echo -n "Test srmReleaseSpace - "
    echo ${clientDir}/srm2_testReleaseSpace -e ${endpoint} -x ${spaceToken} >> ${file}
    ${clientDir}/srm2_testReleaseSpace -e ${endpoint} -x ${spaceToken} >> ${file} 2>&1
    getStatus
    printStatus;
else
    echo "No space token found - skipping rest of space tests";
fi


# Check if we are going to do recall tests...
if [[ -n $recallTokenD ]]; then
    echo "Performing Recall test; press ctrl-C to interrupt";
    interrupt=0;
    echo -n "    Testing GetSpaceToken [SRM_SUCCESS] - ";
    echo ${clientDir}/srm2_testGetSpaceToken ${endpoint} ${recallTokenD} >> ${file};
    ${clientDir}/srm2_testGetSpaceToken ${endpoint} ${recallTokenD} >> ${file} 2>&1;
    getStatus;
    printStatus;
    if [[ ${status} == "0" ]]; then
       getSpaceToken;
       trap 'echo ""; interrupt=1' 2;
       echo "    Performing PUT cycle...";
       putCycle "   "
       if [[ ${interrupt} -eq 0 ]]; then
          echo -n "    Waiting for migration (Press ctrl-C to interrupt) .";
          while (( !interrupt )); do
             # Loop of Ls and look for FileLocality
            echo ${clientDir}/srm2_testLs -e ${endpoint} -s ${endpoint}${userdir}/${testfile} -V >> ${file};
            ${clientDir}/srm2_testLs -e ${endpoint} -s ${endpoint}${userdir}/${testfile} -V >> ${file} 2>&1;
            getFileLocality
            if [[ ${fileLocality} = "ONLINE_AND_NEARLINE" || ${fileLocality} = "NEARLINE" ]]; then
               # File is migrated; now purge it drom the space
               echo "";
               echo -n "    Testing srmPurgeFromSpace [SRM_SUCCESS} - ";
               echo ${clientDir}/srm2_testPurgeFromSpace -e ${endpoint} -s ${endpoint}${userdir}/${testfile} -x ${spaceToken} >> ${file};
               ${clientDir}/srm2_testPurgeFromSpace -e ${endpoint} -s ${endpoint}${userdir}/${testfile} -x ${spaceToken} >> ${file} 2>&1;
               getStatus;
               printStatus;
               if [[ ${status} == "0" ]]; then
                  # Now try to bring online
                  echo -n "    Testing srmBringOnline [SRM_REQUEST_QUEUED] - "
                  echo ${clientDir}/srm2_testBringOnline -e ${endpoint} -s ${endpoint}${userdir}/${testfile} -x ${spaceToken} >> ${file};
                  ${clientDir}/srm2_testBringOnline -e ${endpoint} -s ${endpoint}${userdir}/${testfile} -x ${spaceToken} >> ${file} 2>&1;
                  getStatus;
                  printStatus;
                  if [[ ${status} == "17" ]]; then
                     getRequestToken;
                     echo -n "    Waiting for file to come online."
                     while (( !interrupt )); do
                        echo ${clientDir}/srm2_testLs -e ${endpoint} -s ${endpoint}${userdir}/${testfile} -V >> ${file};
                        ${clientDir}/srm2_testLs -e ${endpoint} -s ${endpoint}${userdir}/${testfile} -V >> ${file} 2>&1;
                        getFileLocality;
                        if [[ ${fileLocality} = "ONLINE_AND_NEARLINE" ]]; then
                           echo "";
                           echo -n "   File back online - checking StatusOfBringOnline [SRM_SUCCESS} - ";
                           echo ${clientDir}/srm2_testStatusOfBringOnline -e ${endpoint} -r $requestToken}  >> ${file};
                           ${clientDir}/srm2_testStatusOfBringOnline -e ${endpoint} -r $requestToken}  >> ${file} 2>&1;
                           getStatus;
                           printStatus;
                           break;
                        elif [[ ${fileLocality} = "NEARLINE" ]]; then
                           echo -n ".";
                           sleep 30;
                        else
                           echo "Breaking out of loop prematurely!";
                           break;
                        fi;
                     done;
                  else
                     echo "Bring online request failed";
                  fi  # End of BringOnline status check
               fi  # End of purge from space check 
            fi  # End of fileLocality check for migration
            echo -n ".";
            sleep 60;
          done;
       fi # End of interrupt test
    fi
else
    echo "No space token description supplied - skipping recall tests";
fi
exit

