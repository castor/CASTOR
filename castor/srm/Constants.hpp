/******************************************************************************
 *                      Constants.hpp
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Here are defined all constants used in Castor.
 * This includes :
 *   - Ids of objects (OBJ_***)
 *   - Ids of services (SVC_***)
 *   - Ids of persistent representations (REP_***)
 *
 * @author castor-dev@cern.ch
 *****************************************************************************/

#ifndef SRM_CONSTANTS_HPP 
#define SRM_CONSTANTS_HPP 1

namespace castor {
 namespace srm {

  /**
   * Ids of objects. Each persistent or serializable type has one
   */
  enum ObjectsIds {
    OBJ_INVALID = 1000,
    OBJ_StageRequest = 1002,
    OBJ_StorageArea = 1003,
    OBJ_SrmUser = 1004,
    OBJ_UserFile = 1005,
    OBJ_SubRequest = 1008,
    OBJ_CopySubRequest = 1009,
    OBJ_Request = 1010
  };

  /**
   * Nb of objectIds currently existing
   */
  static const unsigned int ObjectsIdsNb = 11;

  /**
   * Names of the differents objects, used to display
   * correctly the ObjectsIds enum
   */
  extern const char* ObjectsIdStrings[11];

  /**
   * Offset to be used to access ObjectsIdStrings[]
   */
  static const unsigned int ObjectsIdsOffset = 1000;

} } /* end of namespace castor/srm */

#endif // SRM_CONSTANTS_HPP
