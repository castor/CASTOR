/******************************************************************************
 *                      CastorClient.cpp
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * A C++ based Castor client, with synch and asynch capabilities.
 *
 * @author castor-dev@cern.ch
 *****************************************************************************/

// Include Files
#include <errno.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sstream>

#include "h/Cpwd.h"
#include "h/Cgetopt.h"
#include "h/common.h"
#include "h/patchlevel.h"
#include "castor/log/log.hpp"
#include "castor/io/ClientSocket.hpp"
#include "castor/io/ServerSocket.hpp"
#include "castor/Constants.hpp"
#include "castor/System.hpp"
#include "castor/IClient.hpp"
#include "castor/IObject.hpp"
#include "castor/MessageAck.hpp"
#include "castor/rh/Client.hpp"
#include "castor/rh/Response.hpp"
#include "castor/stager/SubRequest.hpp"
#include "castor/client/BaseClient.hpp"
#include "castor/client/VectorResponseHandler.hpp"

#include "castor/exception/Exception.hpp"
#include "castor/exception/Communication.hpp"
#include "castor/exception/InvalidArgument.hpp"

// Local includes
#include "castor/srm/SrmConstants.hpp"
#include "castor/srm/CastorClient.hpp"

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
castor::srm::CastorClient::CastorClient(unsigned int callbackPort) throw() :
  m_callbackPort(callbackPort),
  m_callbackSocket(0),
  m_nfds(0),
  m_stopped(false) {
}

//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
castor::srm::CastorClient::~CastorClient() throw() {
  if (0 != m_callbackSocket) delete m_callbackSocket;
}

//------------------------------------------------------------------------------
// asynchSendRequest
//------------------------------------------------------------------------------
void castor::srm::CastorClient::asynchSendRequest(const Cuuid_t& reqUuid,
  castor::stager::FileRequest& req,
  const std::string& rhHost, const unsigned int rhPort,
  castor::srm::FileIdMap& fileIds)
  throw(castor::exception::Exception) {
    
  if (0 == m_callbackSocket) {
    castor::exception::Exception e;
    e.getMessage() << "Callback socket not yet opened, cannot send a request";
    throw e;
  }

  // Client
  unsigned short port;
  unsigned long ip;
  m_callbackSocket->getPortIp(port, ip);
  castor::rh::Client *client = new castor::rh::Client();
  client->setIpAddress(ip);
  client->setPort(port);
  client->setVersion(MAJORVERSION*1000000+MINORVERSION*10000+MAJORRELEASE*100+MINORRELEASE);
  req.setClient(client);
  // Mask
  mode_t mask = umask(0);
  umask(mask);
  req.setMask(mask);
  // Pid
  req.setPid(getpid());
  // Machine
  errno = 0;
  std::string hostName;
  hostName = castor::System::getHostName();
  if (hostName == "") {
    castor::exception::Exception e(errno);
    e.getMessage() << "Could not get the localhost"
                   <<  strerror(errno);
    throw e;
  }
  req.setMachine(hostName);
  // Username
  errno = 0;
  struct passwd *pw = Cgetpwuid(req.euid());
  if (0 == pw) {
    castor::exception::Exception e(EINVAL);
    e.getMessage() << "Unknown User" << std::endl;
    throw e;
  } else {
    req.setUserName(pw->pw_name);
  }
  // userTag = req UUID, to allow forcing it on Castor
  req.setUserTag(req.reqId());
  
  // "Calling stager"  
  for(unsigned int i = 0; i < req.subRequests().size(); i++) {
    std::list<castor::log::Param> params = {
      castor::log::Param("NSFILEID", fileIds[req.subRequests()[i]->fileName()].fileid),
      castor::log::Param("REQID", reqUuid),
      castor::log::Param("ReqType", castor::ObjectsIdStrings[req.type()]),
      castor::log::Param("StagerHost", rhHost),
      castor::log::Param("SvcClass", req.svcClassName()),
      castor::log::Param("FileName", req.subRequests()[i]->fileName()),
      castor::log::Param("CastorReqId", req.reqId())
    };
    castor::log::write(LOG_INFO, "Calling Stager", params);
  }

  // Create a socket
  castor::io::ClientSocket sock(rhPort, rhHost);
  sock.connect();
  // Send the request
  sock.sendObject(req);
  // Wait for acknowledgment
  castor::IObject* obj = sock.readObject();
  castor::MessageAck* ack =
    dynamic_cast<castor::MessageAck*>(obj);
  if (0 == ack) {
    castor::exception::InvalidArgument e;
    e.getMessage() << "No Acknowledgement from the server";
    throw e;
  }
  if (!ack->status()) {
    castor::exception::Exception e(ack->errorCode());
    e.getMessage() << ack->errorMessage();
    delete ack;
    throw e;
  }
  
  if(ack->requestId() != req.reqId()) {
    // The castor instance to which we are talking does not have the
    // ability to force the request UUID, which is required.
    // Fail here so a proper message is logged.
    castor::exception::Exception e;
    e.getMessage() << "Fatal configuration error: the Request Handler "
                   << "does not allow forcing of the request UUID, "
                   << "please check your castor.conf for the "
                   << "RH/SRMHostsList parameter";
    delete ack;
    throw e;
  }
  else {
    // The request has been successfully sent, nothing to do
    delete ack;
  }
}


//------------------------------------------------------------------------------
// sendRequest
//------------------------------------------------------------------------------
std::vector<castor::rh::Response*> castor::srm::CastorClient::sendRequest(const Cuuid_t& reqUuid,
  castor::stager::Request& req,
  const std::string& rhHost, const unsigned int rhPort,
  castor::srm::FileIdMap& fileIds)
  throw(castor::exception::Exception) {
  
  struct stage_options opts;
  opts.stage_host = (char*)rhHost.c_str();
  opts.stage_port = rhPort;
  opts.service_class = (char*)req.svcClassName().c_str();
  
  // Mask
  mode_t mask = umask(0);
  umask(mask);
  req.setMask(mask);

  // "Calling stager"
  castor::stager::FileRequest* freq = dynamic_cast<castor::stager::FileRequest*>(&req);
  if(freq != 0) {
    for(unsigned int i = 0; i < freq->subRequests().size(); i++) {
      std::list<castor::log::Param> params = {
        castor::log::Param("NSFILEID", fileIds[freq->subRequests()[i]->fileName()].fileid),
        castor::log::Param("REQID", reqUuid),
        castor::log::Param("ReqType", castor::ObjectsIdStrings[req.type()]),
        castor::log::Param("StagerHost", rhHost),
        castor::log::Param("SvcClass", opts.service_class),
        castor::log::Param("FileName", freq->subRequests()[i]->fileName()),
      };
      castor::log::write(LOG_INFO, "Calling Stager", params);
    }
  }
  else {
    std::list<castor::log::Param> params = {
      castor::log::Param("REQID", reqUuid),
      castor::log::Param("ReqType", castor::ObjectsIdStrings[req.type()]),
      castor::log::Param("StagerHost", rhHost),
      castor::log::Param("SvcClass", opts.service_class)
    };
    castor::log::write(LOG_INFO, "Calling Stager", params);
  }

  // Using the VectorResponseHandler which stores everything in a vector
  std::vector<castor::rh::Response*> respvec;
  castor::client::VectorResponseHandler rh(&respvec);
  castor::client::BaseClient client(stage_getClientTimeout());
  client.setOptions(&opts);
  if(req.euid() == 0) {
    req.setEuid(castor::srm::SrmConstants::getInstance()->getStageUid());
    req.setEgid(castor::srm::SrmConstants::getInstance()->getStageGid());
  }
  client.setAuthorizationId(req.euid(), req.egid());
  
  // Send the request and wait for the responses. Any exception is
  // forwarded to the caller
  std::string reqId = client.sendRequest(&req, &rh);
  req.setReqId(reqId);
  
  if(respvec.size() == 0) {
    castor::exception::Exception e;
    e.getMessage() << "No responses received";
    throw e;
  }    
  
  // "Stager returned"
  std::list<castor::log::Param> params = {
    castor::log::Param("REQID", reqUuid),
    castor::log::Param("ReqType", castor::ObjectsIdStrings[req.type()]),
    castor::log::Param("CastorReqId", reqId),
    castor::log::Param("NbResponses", respvec.size())
  };
  castor::log::write(LOG_DEBUG, "Stager returned", params);
  
  return respvec;
}

//------------------------------------------------------------------------------
// init
//------------------------------------------------------------------------------
void castor::srm::CastorClient::init()
  throw (castor::exception::Exception) {
    
  // Create callback socket, bind and listen; this can throw exceptions
  m_callbackSocket = new castor::io::ServerSocket(m_callbackPort, true);
  m_callbackSocket->listen();
}

//------------------------------------------------------------------------------
// run
//------------------------------------------------------------------------------
void castor::srm::CastorClient::run(void* param)
  throw (castor::exception::Exception) {

  castor::server::DynamicThreadPool* pool = (castor::server::DynamicThreadPool*)param;
  unsigned int i, j;
  
  // Add the listening socket to the pollfd structure
  m_fds[0].fd = m_callbackSocket->socket();
  m_fds[0].events = POLLIN;
  m_nfds = 1;

  // Wait for and listen to responses
  for(;;) {
    // Wait for a POLLIN event on the list of file descriptors
    errno = 0;
    int rc = poll(m_fds, m_nfds, 1000);   // 1 sec timeout to be able to detect a shutdown
    
    if(m_stopped) {
      // We're shutting down, just exit the loop
      break;
    }
    if (rc == 0) {
      // poll timed out, go to next iteration
      continue;
    }
    else if (rc < 0) {
      if (errno == EINTR || errno == EAGAIN) {
        // A signal was caught during poll(), ignore
        continue;
      }
      // "Communication error reading from Castor" message
      std::list<castor::log::Param> params =
       {castor::log::Param("ErrorMessage", sstrerror(errno))};
      castor::log::write(LOG_WARNING, "Communication error reading from Castor", params);
      continue;
    }

    // Loop over the file descriptors
    unsigned int nfds = m_nfds;
    for (i = 0; i < nfds; i++) {
      if (m_fds[i].revents == 0) {
        continue;  // Nothing of interest
      }
      // Unexpected result?
      if ((m_fds[i].revents & POLLIN) != POLLIN) {
        std::stringstream ss;
        ss << "Unexpected result from poll(): revents = "
           << m_fds[i].revents << ", should be POLLIN (" << POLLIN << ")";
        // "Communication error reading from Castor" message
        std::list<castor::log::Param> params =
         {castor::log::Param("ErrorMessage", ss.str())};
        castor::log::write(LOG_WARNING, "Communication error reading from Castor", params);
        usleep(100000);    // wait a bit to avoid thrashing in case of looping errors
        continue;
      }
      if (m_fds[i].revents & POLLERR) {
        // "Communication error reading from Castor" message
        std::list<castor::log::Param> params =
         {castor::log::Param("ErrorMessage", "POLLERR detected in result from poll()")};
        castor::log::write(LOG_WARNING, "Communication error reading from Castor", params);
      }
      
      // Is the listening descriptor readable? If so, this indicates that there
      // is a new incoming connection
      if (m_fds[i].fd == m_callbackSocket->socket()) {
        try {
          // Accept the incoming connection, and set a short transfer timeout:
          // we don't want to get stuck and we know that the sender is either
          // the stager or a job, so they will normally send the data in one go.
          castor::io::ServerSocket* socket = m_callbackSocket->accept();
          socket->setTimeout(10);
          // Register the new file descriptor in the accepted connections list and
          // the pollfd structure
          m_connected.push_back(socket);
          m_fds[m_nfds].fd = socket->socket();
          m_fds[m_nfds].events = POLLIN;
          m_nfds++;
        }
        catch (castor::exception::Exception& socketErr) {
          // we got an unexpected exception on accept()
          std::list<castor::log::Param> params =
            {castor::log::Param("ErrorMessage", socketErr.getMessage().str())};
          castor::log::write(LOG_ERR, "Unexpected exception accepting a Castor connection", params);
          usleep(100000);    // wait a bit to avoid thrashing in case of looping errors
          continue;
        }
      }
      else {
        // This is not the listening socket therefore we have new data to read.
        // Locate the socket in the list of accepted connections
        castor::io::ServerSocket* socket = 0;
        for (j = 0; j < m_connected.size(); j++) {
          if (m_fds[i].fd == m_connected[j]->socket()) {
            socket = m_connected[j];
            break;
          }
        }
        // The socket was not found?
        if (socket == 0) {
          // "Communication error reading from Castor" message
          std::list<castor::log::Param> params =
           {castor::log::Param("ErrorMessage", "POLLIN event received for unknown socket")};
          castor::log::write(LOG_WARNING, "Communication error reading from Castor", params);
          continue;
        }

        // Read object from socket
        castor::IObject *obj = 0;
        try {
          obj = socket->readObject();
        } catch (castor::exception::Exception e) {
          // Ignore "Connection closed by remote end" errors. This is just the
          // request replier of the stager terminating the connection because it
          // has nothing else to send.
          if (e.code() == 1016) {
            serrno = 0;    // Reset serrno to 0

            // Remove the socket from the accepted connections list and set the
            // file descriptor in the pollfd structures to -1. Later on the
            // structure will be cleaned up
            delete socket;
            m_connected.erase(m_connected.begin() + j);
            m_fds[i].fd = -1;
            
            // Keep looping for other responses
            continue;
          }
        }

        // Cast response into IOResponse
        castor::rh::IOResponse* res = dynamic_cast<castor::rh::IOResponse *>(obj);
        if (res == 0) {
          // Not an IOResponse? This in principle cannot happen.
          // Ignore EndResponses coming from jobs and log any other
          if(obj && obj->type() != castor::OBJ_EndResponse) {
            // "Communication error reading from Castor" message
            std::list<castor::log::Param> params =
            {castor::log::Param("ErrorMessage", "Received response was not an IOResponse"),
             castor::log::Param("ActualType", castor::ObjectsIdStrings[obj->type()])};
            castor::log::write(LOG_WARNING, "Communication error reading from Castor", params);
          }
          continue;
        }
      
        try {
          // Asynchronously handle the response.
          // Note that we make sure we feed the response into the queue,
          // which may block for some time the listening loop. In such a case
          // the stager will actually wait on its side, so we're safe.
          pool->addTask(res, true);
        } catch (castor::exception::Exception e) {
          if(e.code() == EPERM) {
            // we're shutting down, just exit the loop
            return;
          }
        }
      }
    }

    // Compress the pollfd structure removing entries where the file descriptor
    // is negative. We do not need to move back the events and revents fields
    // because the events will always be POLLIN in this case, and revents is
    // output.
    for (i = 0; i < m_nfds; i++) {
      if (m_fds[i].fd == -1) {
        for (j = i; j < m_nfds; j++) {
          m_fds[j].fd = m_fds[j + 1].fd;
        }
        m_nfds--;
      }
    }
  }
  
  // Free resources
  for (i = 0; i < m_connected.size(); i++) {
    if (m_connected[i] != 0) delete m_connected[i];
  }
  m_connected.clear();
}
