/******************************************************************************
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

#pragma once

// Include files
#include "osdep.h"
#include <vector>
#include <mutex>

namespace castor {

  namespace gc {

    /**
     * Deletion State
     */
    class DeletionState {

    public:

      /// Constructor
      DeletionState() : m_removedSize(0), m_filesTotal(0), m_filesFailed(0), m_filesRemoved(0) {};

      /// Destructor
      ~DeletionState() {
        for (auto it = m_deletedFiles.begin();
             it != m_deletedFiles.end();
             it++) delete(*it);
        for (auto it = m_failedFiles.begin();
             it != m_failedFiles.end();
             it++) delete(*it);
      }

      /// adding successfully deleted file
      void fileDeleted(u_signed64 id, u_signed64 size) {
        std::lock_guard<std::mutex> guard(m_mutex);
        m_filesTotal++;
        m_filesRemoved++;
        m_removedSize += size;
        m_deletedFiles.push_back(new u_signed64(id));
      }

      /// adding file that failed deletion
      void fileDeletionFailed(u_signed64 id) {
        std::lock_guard<std::mutex> guard(m_mutex);
        m_filesTotal++;
        m_filesFailed++;
        m_failedFiles.push_back(new u_signed64(id));
      }

      /// accessor to deletedFiles
      std::vector<u_signed64*> deletedFiles() {
        return m_deletedFiles;
      }

      /// accessor to failedFiles
      std::vector<u_signed64*> failedFiles() {
        return m_failedFiles;
      }

      /// accessor to removedSize
      u_signed64 removedSize() {
        return m_removedSize;
      }

      /// accessor to filesTotal
      unsigned long filesTotal() {
        return m_filesTotal;
      }

      /// accessor to filesFailed
      unsigned long filesFailed() {
        return m_filesFailed;
      }

      /// accessor to filesRemoved
      unsigned long filesRemoved() {
        return m_filesRemoved;
      }

    private:

      /// mutex protecting concurrent access to this object
      std::mutex m_mutex;

      /// list of deleted file ids
      std::vector<u_signed64*> m_deletedFiles;

      /// list of failed files ids
      std::vector<u_signed64*> m_failedFiles;

      /// total size deleted
      u_signed64 m_removedSize;

      /// total number of files dealt with
      unsigned long m_filesTotal;

      /// total number of files that failed deletion
      unsigned long m_filesFailed;

      /// total number of files deleted successfully
      unsigned long m_filesRemoved;

    };

  } // End of namespace gc

} // End of namespace castor

