/******************************************************************************
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Deletion thread used to check periodically whether files need to be deleted
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

#pragma once

// Include files
#include <mutex>
#include <condition_variable>
#include <cassert>

namespace castor {

  namespace gc {

    /**
     * MultiCompletion class
     * A little helper class for multi completion handling
     * when calling many asynchronously methods
     * Code mainly copied (and simplified) from the Ceph project
     */
    class MultiCompletion {

    public:
      
      MultiCompletion() : m_pending_complete(0), m_building(true) {};

      void wait_for_complete() {
        std::unique_lock<std::mutex> guard(m_lock);
        while (m_pending_complete)
          m_cond.wait(guard);
      }
      
      void add_request() {
        std::unique_lock<std::mutex> guard(m_lock);
        assert(m_building);
        m_pending_complete++;
      }
      
      void complete_request()
      {
        std::unique_lock<std::mutex> guard(m_lock);
        assert(m_pending_complete);
        int count = --m_pending_complete;
        if (!count && !m_building) {
          m_cond.notify_one();
        }
      }

      void finish_adding_requests()
      {
        std::unique_lock<std::mutex> guard(m_lock);
        assert(m_building);
        m_building = false;
        if (!m_pending_complete)
          m_cond.notify_one();
      }

    private:

      /// mutex insuring thread safety
      std::mutex m_lock;

      /// condition variable for waking up waiters
      std::condition_variable m_cond;

      /// number of ongoing request, pending for completion
      int m_pending_complete;
      
      /// true until finish_adding_requests is called
      bool m_building;

    };

  } // End of namespace gc

} // End of namespace castor
