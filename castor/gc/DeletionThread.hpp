/******************************************************************************
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

#pragma once

// Include files
#include "castor/exception/Exception.hpp"
#include "castor/stager/GCLocalFile.hpp"
#include "castor/server/IThread.hpp"
#include "castor/gc/MultiCompletion.hpp"
#include "castor/gc/DeletionState.hpp"

#include "osdep.h"

namespace castor {

  namespace gc {

    /**
     * Deletion Thread
     */
    class DeletionThread: public castor::server::IThread {

    public:

      /**
       * Default constructor
       */
      DeletionThread();

      /**
       * Constructor
       * @param startDelay
       */
      DeletionThread(int startDelay);

      /**
       * Default destructor
       */
      virtual ~DeletionThread() throw() {};

      /// Not implemented
      virtual void init() {};

      /// Method called periodically to check whether files need to be deleted.
      virtual void run(void *param);

      /// Not implemented
      virtual void stop() {};

    private:

      /**
       * Asynchronously removes the file.
       * Note that it's actually synchronous for local files, even if
       * the asynchronous API is respected
       * @param file the file to remove
       * @param multi_completion the completion handler to be used when over
       * @param state 
       */
      void gcAsyncRemoveFilePath (castor::stager::GCLocalFile *file,
                                  MultiCompletion *multi_completion,
                                  DeletionState *state);

    private:

      /// The name of the diskserver on which the GC daemon is running
      std::string m_diskServerName;

      /** The interval at which the GC's main loop runs
       *  This interval represents the average waiting time. The actual waiting
       *  time for a given iteration will be randomly chosen (with flat distribution)
       *   between 0.5 and 1.5 times this number
       */
      int m_interval;

      /// The number of seconds to delay the first invocation of the run method
      int m_startDelay;

    };

  } // End of namespace gc

} // End of namespace castor

