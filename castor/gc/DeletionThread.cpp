/******************************************************************************
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Deletion thread used to check periodically whether files need to be deleted
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

#include "castor/gc/DeletionThread.hpp"
#include "castor/gc/CephGlobals.hpp"
#include "castor/Services.hpp"
#include "castor/Constants.hpp"
#include "castor/stager/IGCSvc.hpp"
#include "castor/System.hpp"
#include "castor/log/log.hpp"
#include "getconfent.h"
#include "serrno.h"

#include <vector>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <stdlib.h>

// Definitions
#define DEFAULT_GCINTERVAL 200


//-----------------------------------------------------------------------------
// Constructor
//-----------------------------------------------------------------------------
castor::gc::DeletionThread::DeletionThread(int startDelay):
  m_interval(DEFAULT_GCINTERVAL),
  m_startDelay(startDelay) {};



//-----------------------------------------------------------------------------
// Run
//-----------------------------------------------------------------------------
void castor::gc::DeletionThread::run(void*) {

  // "Starting deletion thread"
  castor::log::write(LOG_INFO, "Starting deletion thread");
  sleep(m_startDelay);

  // Get RemoteGCSvc
  castor::IService* svc =
    castor::BaseObject::services()->
    service("RemoteGCSvc", castor::SVC_REMOTEGCSVC);
  if (0 == svc) {
    // "Could not get RemoteStagerSvc"
    std::list<castor::log::Param> params =
      {castor::log::Param("Function", "DeletionThread::run")};
    castor::log::write(LOG_ERR, "Could not get RemoteStagerSvc", params);
    return;
  }

  castor::stager::IGCSvc *gcSvc =
    dynamic_cast<castor::stager::IGCSvc*>(svc);
  if (0 == gcSvc) {
    // "Got a bad RemoteStagerSvc"
    std::list<castor::log::Param> params =
      {castor::log::Param("ID", svc->id()),
       castor::log::Param("Name", svc->name()),
       castor::log::Param("Function", "DeletionThread::run")};
    castor::log::write(LOG_ERR, "Got a bad RemoteStagerSvc", params);
    return;
  }

  // Determine the name of the diskserver
  try {
    m_diskServerName = castor::System::getHostName();
  } catch (castor::exception::Exception& e) {
    // "Exception caught trying to getHostName"
    std::list<castor::log::Param> params =
      {castor::log::Param("Code", strerror(e.code())),
       castor::log::Param("Message", e.getMessage().str())};
    castor::log::write(LOG_ERR, "Exception caught trying to getHostName", params);
    return;
  }

  // Get the GC Interval
  char *value = 0;
  if ((value = getenv("GC_INTERVAL")) ||
      (value = getconfent("GC", "Interval", 0))) {
    m_interval = atoi(value);
  }
  // initialize random generator for random sleep times around m_interval
  srand(time(NULL));

  // Endless loop
  for (;;) {
    // "Checking for garbage"
    castor::log::write(LOG_DEBUG, "Checking for garbage");

    // Get new sleep interval if the environment has been changed
    int intervalnew;
    if ((value = getenv("GC_INTERVAL")) ||
	(value = getconfent("GC", "Interval", 0))) {
      intervalnew = atoi(value);
      if (intervalnew != m_interval) {
	m_interval = intervalnew;

	// "Sleep interval changed"
	std::list<castor::log::Param> params =
	  {castor::log::Param("Interval", m_interval)};
	castor::log::write(LOG_INFO, "Sleep interval changed", params);
      }
    }
    // Compute the actual waiting time for this round
    int sleepDuration = (rand()%m_interval)+m_interval/2;

    // Retrieve list of files to delete
    std::vector<castor::stager::GCLocalFile*>* files2Delete = 0;
    try {
      files2Delete = gcSvc->selectFiles2Delete(m_diskServerName);
    } catch (castor::exception::Exception& e) {

      // "Error caught while looking for garbage files"
      std::list<castor::log::Param> params =
        {castor::log::Param("Type", sstrerror(e.code())),
	 castor::log::Param("Message", e.getMessage().str())};
      castor::log::write(LOG_ERR, "Error caught while looking for garbage files", params);

      // "Sleeping"
      std::list<castor::log::Param> params2 =
        {castor::log::Param("Duration", sleepDuration)};
      castor::log::write(LOG_DEBUG, "Sleeping", params2);
      sleep(sleepDuration);
      continue;
    }

    // Delete files
    if (0 < files2Delete->size()) {

      // "Found files to garbage collect. Starting removal"
      std::list<castor::log::Param> params =
        {castor::log::Param("NbFiles", files2Delete->size())};
      castor::log::write(LOG_INFO, "Found files to garbage collect. Starting removal", params);

      // Prepare for asynchronous deletion of files
      DeletionState state;
      MultiCompletion multi_completion;

      // Loop over the files and delete them
      for(std::vector<castor::stager::GCLocalFile*>::iterator it =
            files2Delete->begin();
          it != files2Delete->end();
          it++) {
        // Construct Cns invariant
        multi_completion.add_request();
        gcAsyncRemoveFilePath(*it, &multi_completion, &state);
      }
      multi_completion.finish_adding_requests();
      multi_completion.wait_for_complete();

      // Log to DLF
      if (0 < state.filesTotal()) {
        // "Summary of files removed"
        std::list<castor::log::Param> params =
          {castor::log::Param("FilesRemoved", state.filesRemoved()),
           castor::log::Param("FailedFiles", state.filesFailed()),
           castor::log::Param("TotalFiles", state.filesTotal()),
           castor::log::Param("SpaceFreed", state.removedSize())};
        castor::log::write(LOG_INFO, "Summary of files removed", params);
      }

      // Inform stager of the deletion/failure
      if (state.deletedFiles().size() > 0) {
        try {
          std::vector<u_signed64*> files = state.deletedFiles();
          gcSvc->filesDeleted(files);
        } catch (castor::exception::Exception& e) {
          // "Error caught while informing stager..."
          std::list<castor::log::Param> params =
            {castor::log::Param("Code", sstrerror(e.code())),
	     castor::log::Param("Error", e.getMessage().str())};
          castor::log::write(LOG_ERR, "Error caught while informing stager of the files deleted", params);
        }
      }
      if (state.failedFiles().size() > 0) {
        try {
          std::vector<u_signed64*> files = state.failedFiles();
          gcSvc->filesDeletionFailed(files);
        } catch (castor::exception::Exception& e) {
          // "Error caught while informing stager..."
          std::list<castor::log::Param> params =
            {castor::log::Param("Code", sstrerror(e.code())),
	     castor::log::Param("Error", e.getMessage().str())};
          castor::log::write(LOG_ERR, "Error caught while informing stager of the failed deletions", params);
        }
      }
    } else {
      // "No garbage files found"
      castor::log::write(LOG_DEBUG, "No garbage files found");
      // Sleeping
      std::list<castor::log::Param> params2 =
        {castor::log::Param("Duration", m_interval)};
      castor::log::write(LOG_DEBUG, "Sleeping", params2);
      sleep(m_interval);
    }
  } // End of loop
}

void deletionOk(castor::gc::DeletionState *state,
                castor::stager::GCLocalFile *file,
                u_signed64 fileSize, time_t fileAge) {
  state->fileDeleted(file->diskCopyId(), fileSize);
  // "Removed file successfully"
  std::list<castor::log::Param> params =
    {castor::log::Param("NSFILEID", file->fileId()),
     castor::log::Param("Filename", file->fileName()),
     castor::log::Param("FileSize", fileSize),
     castor::log::Param("FileAge", fileAge),
     castor::log::Param("LastAccessTime", file->lastAccessTime()),
     castor::log::Param("NbAccesses", file->nbAccesses()),
     castor::log::Param("GcWeight", file->gcWeight()),
     castor::log::Param("GcType", file->gcTriggeredBy()),
     castor::log::Param("SvcClass", file->svcClassName()),
     castor::log::Param("DiskCopy", file->diskCopyId())};
  castor::log::write(LOG_INFO, "Removed file successfully", params);
}

void deletionFailed(castor::gc::DeletionState *state,
                    castor::stager::GCLocalFile *file,
                    std::string msg, int errorCode) {
  state->fileDeletionFailed(file->diskCopyId());
  // "Failed to remove file"
  std::list<castor::log::Param> params =
    {castor::log::Param("NSFILEID", file->fileId()),
     castor::log::Param("Filename", file->fileName()),
     castor::log::Param("Message", msg),
     castor::log::Param("Type", errorCode),
     castor::log::Param("DiskCopy", file->diskCopyId())};
  castor::log::write(LOG_WARNING, "Failed to remove file", params);
}

//-----------------------------------------------------------------------------
// gcAsyncRemoveFilePath
//-----------------------------------------------------------------------------
struct AioData {
  AioData(castor::gc::DeletionState *_state,
          castor::stager::GCLocalFile *_file,
          castor::gc::MultiCompletion *_multi_completion,
          libradosstriper::RadosStriper *_striper,
          std::string &_objectName) :
    state(_state), file(_file),
    multi_completion(_multi_completion), striper(_striper),
    objectName(_objectName) {}
  castor::gc::DeletionState *state;
  castor::stager::GCLocalFile *file;
  time_t pmtime;
  uint64_t pmsize;
  castor::gc::MultiCompletion *multi_completion;
  libradosstriper::RadosStriper *striper;
  std::string objectName;
};

static void aioRemoveComplete(rados_completion_t c, void *arg) {
  // get out results of the sync stat
  AioData *cdata = reinterpret_cast<AioData*>(arg);
  int rc = rados_aio_get_return_value(c);
  rados_aio_release(c);
  if (rc) {
    deletionFailed(cdata->state, cdata->file, "Asynchronous remove failed", rc);
  } else {
    deletionOk(cdata->state, cdata->file, cdata->pmsize, time(NULL)-(cdata->pmtime));
  }
  cdata->multi_completion->complete_request();
  delete cdata;
}

static void aioStatComplete(rados_completion_t c, void *arg) {
  // get out results of the sync stat
  AioData *cdata = reinterpret_cast<AioData*>(arg);
  int rc = rados_aio_get_return_value(c);
  rados_aio_release(c);
  if (rc) {
    deletionFailed(cdata->state, cdata->file, "Asynchronous stat failure", rc);
    cdata->multi_completion->complete_request();
    return;
  }
  std::list<castor::log::Param> params =
    {castor::log::Param("NSFILEID", cdata->file->fileId()),
     castor::log::Param("Filename", cdata->file->fileName()),
     castor::log::Param("DiskCopy", cdata->file->diskCopyId())};
  castor::log::write(LOG_DEBUG, "Stat before final remove was succesful", params);
  // async stat was successful, let's go for async remove now
  librados::AioCompletion *rados_completion =
    librados::Rados::aio_create_completion(cdata, aioRemoveComplete, 0);
  rc = cdata->striper->aio_remove(cdata->objectName, rados_completion);
  if (rc) {
    deletionFailed(cdata->state, cdata->file, "Failed to launch asynchronous remove", rc);
    cdata->multi_completion->complete_request();
    return;
  }
}

void castor::gc::DeletionThread::gcAsyncRemoveFilePath
(castor::stager::GCLocalFile *file,
 castor::gc::MultiCompletion *multi_completion,
 castor::gc::DeletionState *state) {
  std::list<castor::log::Param> params =
    {castor::log::Param("NSFILEID", file->fileId()),
     castor::log::Param("Filename", file->fileName()),
     castor::log::Param("GcType", file->gcTriggeredBy()),
     castor::log::Param("SvcClass", file->svcClassName()),
     castor::log::Param("DiskCopy", file->diskCopyId())};
  castor::log::write(LOG_DEBUG, "Deleting file", params);
  if (!file->fileName().empty() and file->fileName()[0] == '/') {
    // regular file, call POSIX API
    try {
      struct stat64 fileinfo;
      if (::stat64(file->fileName().c_str(), &fileinfo) ) {
        castor::exception::Exception e(errno);
        e.getMessage() << "Failed to stat file " << file->fileName();
        throw e;
      }
      if (unlink(file->fileName().c_str()) < 0) {
        castor::exception::Exception e(errno);
        e.getMessage() << "Failed to unlink file " << file->fileName();
        throw e;
      }
      deletionOk(state, file, fileinfo.st_size, time(NULL) - fileinfo.st_ctime);
    } catch (castor::exception::Exception &e) {
      deletionFailed(state, file, e.getMessage().str(), e.code());
    }
    multi_completion->complete_request();
  } else {
    // ceph file
    try {
      int colonPos = file->fileName().find(':');
      std::string pool = file->fileName().substr(0, colonPos);
      std::string objectName = file->fileName().substr(colonPos+1);
      libradosstriper::RadosStriper *striper = getRadosStriper(pool);
      if (0 == striper) {
        castor::exception::Exception e(EINVAL);
        e.getMessage() << "Failed to connect to ceph while unlinking file " << file->fileName()
                       << " in pool " << pool;
        throw e;
      }
      AioData *data= new AioData(state, file, multi_completion, striper, objectName);
      librados::AioCompletion *rados_completion =
        librados::Rados::aio_create_completion(data, aioStatComplete, 0);
      int rc = striper->aio_stat(objectName, rados_completion,
                                 &data->pmsize, &data->pmtime);
      if (rc) {
        rados_completion->release();
        castor::exception::Exception e(-rc);
        e.getMessage() << "Failed to stat ceph file " << file->fileName()
                       << " in pool " << pool;
        throw e;
      }
    } catch (castor::exception::Exception &e) {
      deletionFailed(state, file, e.getMessage().str(), e.code());
      multi_completion->complete_request();
    }
  }
}

