/******************************************************************************
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * @(#)GcDaemon.cpp,v 1.10 $Release$ 2005/03/31 15:17:24 sponcec3
 *
 * Garbage collector daemon handling the deletion of local files on a
 * filesystem. Makes remote calls to the stager to know what to delete and to
 * update the catalog
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

// Include files
#include "castor/gc/GcDaemon.hpp"
#include "castor/gc/DeletionThread.hpp"
#include "castor/gc/SynchronizationThread.hpp"
#include "castor/log/SyslogLogger.hpp"
#include "castor/server/SignalThreadPool.hpp"
#include "castor/server/BaseThreadPool.hpp"
#include "castor/log/log.hpp"
#include "Cgetopt.h"
#include "getconfent.h"

#include <iostream>
#include <sys/time.h>
#include <string.h>

//-----------------------------------------------------------------------------
// main
//-----------------------------------------------------------------------------
int main(int argc, char *argv[]) {

  try {
    castor::log::init(new castor::log::SyslogLogger("gcd"));

    castor::gc::GcDaemon daemon(std::cout, std::cerr);

    // Randomize the start-up of the daemon between 1 and 15 minutes.
    char *value;
    int startDelay = 1;
    if ((value = getenv("GC_IMMEDIATESTART")) ||
	      (value = getconfent("GC", "ImmediateStart", 0))) {
      if (!strcasecmp(value, "yes") || !strcmp(value, "1")) {
        startDelay = 0;
      }
    }
    if (startDelay){
      timeval tv;
      gettimeofday(&tv, NULL);
      srand(tv.tv_usec * tv.tv_sec);
      startDelay = 60 + (int) (900.0 * rand() / (RAND_MAX + 60.0));
    }

    // "Garbage Collector started successfully"
    std::list<castor::log::Param> params =
      {castor::log::Param("StartDelay", startDelay)};
    castor::log::write(LOG_INFO, "Garbage Collector started successfully", params);

    // Create the deletion thread
    daemon.addThreadPool
      (new castor::server::SignalThreadPool
       ("Deletion",
        	new castor::gc::DeletionThread(startDelay)));
            daemon.getThreadPool('D')->setNbThreads(1);

    // Create the Synchronization thread
    daemon.addThreadPool
      (new castor::server::SignalThreadPool
       ("Synchronization",
        	new castor::gc::SynchronizationThread(startDelay)));
            daemon.getThreadPool('S')->setNbThreads(1);

    // Start daemon as the stager superuser
    daemon.parseCommandLine(argc, argv);
    const bool runAsStagerSuperuser = true;
    daemon.start(runAsStagerSuperuser);
    return 0;

  } catch (castor::exception::Exception& e) {
    std::cerr << "Caught exception: "
	      << sstrerror(e.code()) << std::endl
	      << e.getMessage().str() << std::endl;

    // "Exception caught when starting Garbage Collector daemon"
    std::list<castor::log::Param> params =
      {castor::log::Param("Code", sstrerror(e.code())),
       castor::log::Param("Message", e.getMessage().str())};
    castor::log::write(LOG_ERR, "Starting synchronization thread", params);
  } catch (...) {
    std::cerr << "Caught exception!" << std::endl;
  }

  return 1;
}


//-----------------------------------------------------------------------------
// Constructor
//-----------------------------------------------------------------------------
castor::gc::GcDaemon::GcDaemon(std::ostream &stdOut, std::ostream &stdErr):
  castor::server::MultiThreadedDaemon(stdOut, stdErr) {
}
