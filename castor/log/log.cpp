/******************************************************************************
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Interface to the CASTOR logging system
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

#include "castor/exception/Exception.hpp"
#include "castor/log/log.hpp"

/**
 * The logger to be used by the CASTOR logging systsem.
 */
static castor::log::Logger *s_logger = NULL;

//------------------------------------------------------------------------------
// init
//------------------------------------------------------------------------------
void castor::log::init(castor::log::Logger *logger) {
  if(s_logger) {
    throw castor::exception::Exception("Failed to initialise logging system"
      ": Logging system already initialised");
  }

  s_logger = logger;
}

//------------------------------------------------------------------------------
// shutdown
//------------------------------------------------------------------------------
void castor::log::shutdown() {
  delete s_logger;
  s_logger = NULL;
}

//------------------------------------------------------------------------------
// instance
//------------------------------------------------------------------------------
castor::log::Logger &castor::log::instance() {
  if(NULL == s_logger) {
    throw castor::exception::Exception("Failed to get CASTOR logger"
      ": Logger does not exist");
  }
  return *s_logger;
}

//------------------------------------------------------------------------------
// prepareForFork
//------------------------------------------------------------------------------
void castor::log::prepareForFork() {
  try {
    instance().prepareForFork();
  } catch(castor::exception::Exception &ex) {
    throw castor::exception::Exception(
     std::string("Failed to prepare logger for call to fork(): ") +
       ex.getMessage().str());
  }
}

//------------------------------------------------------------------------------
// write
//------------------------------------------------------------------------------
void castor::log::write(
  const int priority,
  const std::string &msg,
  const std::list<castor::log::Param> &params) {
  if(s_logger) (*s_logger)(priority, msg, params);
}

//------------------------------------------------------------------------------
// write
//------------------------------------------------------------------------------
void castor::log::write(
  const int priority,
  const std::string &msg,
  const std::string &rawParams,
  const struct timeval &timeStamp,
  const std::string &progName,
  const int pid) {
  const std::list<Param> params;
  if(s_logger) (*s_logger)(priority, msg, params, rawParams, timeStamp,
    progName, pid);
}

//------------------------------------------------------------------------------
// getProgramName
//------------------------------------------------------------------------------
std::string castor::log::getProgramName() {
  if(s_logger) {
    return (*s_logger).getProgramName();
  } else {
    return "";
  }
}

//------------------------------------------------------------------------------
// operator<<
//------------------------------------------------------------------------------
std::ostream& operator<<(std::ostream& out, const Cuuid_t& uuid) {
  char uuidstr[CUUID_STRING_LEN + 1];
  memset(uuidstr, '\0', CUUID_STRING_LEN + 1);
  Cuuid2string(uuidstr, CUUID_STRING_LEN + 1, &uuid);
  out << uuidstr;
  return out;
}
