DROP TABLE BiggestOnTapeTempTable;
DROP TABLE BiggestOnDiskTempTable;
DROP FUNCTION sec2date;
DROP FUNCTION duration2char;
DROP FUNCTION size2char;
DROP PROCEDURE biggestOnTape;
DROP PROCEDURE biggestOnDisk;
