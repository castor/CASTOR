.\" Copyright (C) 2003  CERN
.\" This program is free software; you can redistribute it and/or
.\" modify it under the terms of the GNU General Public License
.\" as published by the Free Software Foundation; either version 2
.\" of the License, or (at your option) any later version.
.\" This program is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\" You should have received a copy of the GNU General Public License
.\" along with this program; if not, write to the Free Software
.\" Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
.TH CASTOR-TAPE-ACS-DISMOUNT "1castor" "$Date: 2013/10/09 14:00:00 $" CASTOR "CASTOR"
.SH NAME
castor-tape-acs-dismount \- dismount a volume
.SH SYNOPSIS
.BI "castor-tape-acs-dismount [options] VID DRIVE_SLOT"

.SH DESCRIPTION
\fBWarning\fP, \fBcastor-tape-acs-dismount\fP is a developer tool and is
therefore subject to change in and even removal from a future release of CASTOR.
End users should not rely on this tool to operate their CASTOR installation.

\fBcastor-tape-acs-dismount\fP dismounts the volume with the specfied \fBVID\fP
from the drive located in the specified \fBDRIVE_SLOT\fP of the tape library.
The format of \fBDRIVE_SLOT\fP is as follows:

.B ACS:LSM:panel:transport

Please note that this command-line tool communicates directly with the CSI of 
the ACS system. This command-line tool does not communicate with any CASTOR
daemons.

.SH OPTIONS
.TP
\fB\-d, \-\-debug
Turns on the printing of debug information.
.TP
\fB\-f, \-\-force
Force the dismount.
.TP
\fB\-h, \-\-help
Prints the usage message.
.TP
\fB\-q, \-\-query SECONDS
Time wait between queries to ACLS for responses.
\fBSECONDS\fP must be an integer value greater than 0.
The default value of \fBSECONDS\fP is 10.
.TP
\fB\-t, \-\-timeout SECONDS
Time to wait for the dismount operation to conclude.
\fBSECONDS\fP must be an integer value greater than 0.
The default value of \fBSECONDS\fP is 610.

.SH "RETURN CODES"
.TP
\fB 0
Ok.
.TP
\fB 1
Command failed.

.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
