.\" Copyright (C) 2003  CERN
.\" This program is free software; you can redistribute it and/or
.\" modify it under the terms of the GNU General Public License
.\" as published by the Free Software Foundation; either version 2
.\" of the License, or (at your option) any later version.
.\" This program is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\" You should have received a copy of the GNU General Public License
.\" along with this program; if not, write to the Free Software
.\" Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
.TH ACSD "8castor" "$Date: 2014/03/24 14:44:00 $" CASTOR "CASTOR"
.SH NAME
acsd \- Automated Cartdridge System daemon
.SH SYNOPSIS
.BI "acsd [OPTIONS]"

.SH DESCRIPTION
\fBacsd\fP is the daemon responsible for controlling ACS mount and ACS dismount 
requests.
.P
When \fBacsd\fP is executed it immediately forks with the parent
terminating and the child running in the background.  If required the
\fB\-f, \-\-foreground\fP option can be used to stop \fBacsd\fP from
forking and keep the parent process in the foreground.

.SH OPTIONS
.TP
\fB\-f, \-\-foreground
Remain in the foreground.
.TP
\fB\-h, \-\-help
Prints the usage message.
.TP
\fB\-c, \-\-config <config-file>
Set the location of the CASTOR configuration file (castor.conf).  The default location is /etc/castor/castor.conf.

.SH CASTOR CONFIGURATION PARAMETERS
The acsd daemon reads and uses the following CASTOR configuration
parameters which are specified within the CASTOR configuration file (the
default location is /etc/castor/castor.conf).

.TP
\fBAcsDaemon CmdTimeout
The maximum time to wait in seconds for a tape-library command to conclude.

.TP
\fBAcsDaemon Port
The TCP/IP port on which the CASTOR ACS daemon listens for incoming Zmq
connections from the tape server.

.TP
\fBAcsDaemon QueryInterval
Time to wait in seconds between queries to the tape Library.

.SH FILES
.TP
.B /etc/castor/castor.conf
Default location of the CASTOR configuration file.
.TP
.B /var/log/castor/acsd.log
Default location of the acsd log file.

.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
