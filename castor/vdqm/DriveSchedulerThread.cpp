/******************************************************************************
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/


#include "castor/BaseAddress.hpp"
#include "castor/Constants.hpp"
#include "castor/IService.hpp"
#include "castor/Services.hpp"
#include "castor/server/NotifierThread.hpp"
#include "castor/vdqm/DriveSchedulerThread.hpp"
#include "castor/vdqm/IVdqmSvc.hpp"
#include "castor/log/log.hpp"
#include "getconfent.h"
#include "rtcp_constants.h"
#include "vdqm_constants.h"

#include <stdlib.h>

//-----------------------------------------------------------------------------
// run
//-----------------------------------------------------------------------------
void castor::vdqm::DriveSchedulerThread::run(void*) {

  Cuuid_t                cuuid    = nullCuuid;
  castor::vdqm::IVdqmSvc *vdqmSvc = NULL;

  Cuuid_create(&cuuid);

  try {
    vdqmSvc = getDbVdqmSvc();
  } catch(castor::exception::Exception &e) {
    // "Could not get DbVdqmSvc"
    std::list<castor::log::Param> params = {
      castor::log::Param("REQID", cuuid),
      castor::log::Param("Function", "castor::vdqm::DriveSchedulerThread::run"),
      castor::log::Param("Message", e.getMessage().str()),
      castor::log::Param("Code", e.code())
    };
    castor::log::write(LOG_ERR, "Could not get DbVdqmSvc",
                       params);

    return;
  }


  // 1 = drive allocated, 0 = no possible allocation found, -1 possible
  // allocation found, but invalidated by other threads
  int allocationResult = 0;

  try {
    u_signed64  tapeDriveId;
    std::string tapeDriveName;
    u_signed64  tapeRequestId;
    std::string tapeRequestVid;

    allocationResult = vdqmSvc->allocateDrive(&tapeDriveId, &tapeDriveName,
      &tapeRequestId, &tapeRequestVid);
    vdqmSvc->commit();

    // If a drive was allocated or a possible drive allocation was found, but
    // was invalidated by other threads
    if((allocationResult == 1) || (allocationResult == -1)){
      std::list<castor::log::Param> param = {
        castor::log::Param("REQID", cuuid),
        castor::log::Param("tapeDriveID"  , tapeDriveId),
        castor::log::Param("driveName"    , tapeDriveName),
        castor::log::Param("tapeRequestID", tapeRequestId),
        castor::log::Param("tapeVID"      , tapeRequestVid)};

      if(allocationResult == 1) { // Drive allocated
        castor::log::write(LOG_INFO,
                           "Tape drive allocated", param);
      } else { // Invalidated drive allocation
        castor::log::write(LOG_DEBUG,
                           "Invalidated tape drive allocation", param);
      }
    }
  } catch (castor::exception::Exception &ex) {
    std::list<castor::log::Param> params = {
      castor::log::Param("REQID", cuuid),
      castor::log::Param("Function", __PRETTY_FUNCTION__),
      castor::log::Param("Message", ex.getMessage().str()),
      castor::log::Param("Code", ex.code())};
    castor::log::write(LOG_ERR,
                       "Error occured when determining if there is matching free drive and waiting request", params);
  }

  // If a drive was allocated
  if(allocationResult == 1) {
    // Notiify the RTCP job submitter threads
    castor::server::NotifierThread::getInstance()->doNotify('J');
  }

  // Delete any old volume priorities
  try {
    // Start with the maximum age of a volume priority being the compile-time
    // default
    unsigned int maxVolPriorityAge = s_maxVolPriorityAge;

    // Overwrite the compile-time default with the value in the configuration
    // file if there is one and it is greater than 0
    char *const maxVolPriorityAgeConfigStr =
      getconfent("VDQM", "MAXVOLPRIORITYAGE", 0);
    if(maxVolPriorityAgeConfigStr != NULL) {
      int const maxVolPriorityAgeConfig = atoi(maxVolPriorityAgeConfigStr);

      if(maxVolPriorityAgeConfig > 0) {
        maxVolPriorityAge = (unsigned int)maxVolPriorityAgeConfig;
      }
    }

    unsigned int prioritiesDeleted =
      vdqmSvc->deleteOldVolPriorities(maxVolPriorityAge);

    if(prioritiesDeleted > 0) {
      std::list<castor::log::Param> params = {
        castor::log::Param("REQID", cuuid),
        castor::log::Param("prioritiesDeleted",prioritiesDeleted)};
      castor::log::write(LOG_INFO,
                         "Deleted old volume priorities", params);;
    }
  } catch(castor::exception::Exception &ex) {
    std::list<castor::log::Param> params = {
      castor::log::Param("REQID", cuuid),
      castor::log::Param("Function", __PRETTY_FUNCTION__),
      castor::log::Param("Message", ex.getMessage().str()),
      castor::log::Param("Code", ex.code())
    };
    castor::log::write(LOG_ERR,
                       "Error occurred whilst deleting old volume priorities", params);
  }
}


//-----------------------------------------------------------------------------
// getDbVdqmSvc
//-----------------------------------------------------------------------------
castor::vdqm::IVdqmSvc *castor::vdqm::DriveSchedulerThread::getDbVdqmSvc()
  
{
  castor::Services *svcs = castor::BaseObject::services();
  castor::IService *svc = svcs->service("DbVdqmSvc", castor::SVC_DBVDQMSVC);
  castor::vdqm::IVdqmSvc *vdqmSvc = dynamic_cast<castor::vdqm::IVdqmSvc*>(svc);

  if (0 == vdqmSvc) {
    castor::exception::Exception ex;
    ex.getMessage()
      << "Failed to cast castor::IService* to castor::vdqm::IVdqmSvc*";

    throw ex;
  }

  return vdqmSvc;
}
