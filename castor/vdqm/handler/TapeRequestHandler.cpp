/******************************************************************************
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 *
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/
 
#include "castor/exception/InvalidArgument.hpp"
#include "castor/vdqm/ClientIdentification.hpp"
#include "castor/vdqm/VdqmTape.hpp"

#include "castor/IObject.hpp"
#include "castor/Constants.hpp"
#include "castor/Services.hpp"
#include "castor/BaseAddress.hpp"

#include "castor/vdqm/DatabaseHelper.hpp"
#include "castor/vdqm/DeviceGroupName.hpp"
#include "castor/vdqm/DevTools.hpp"
#include "castor/vdqm/OldProtocolInterpreter.hpp"
#include "castor/vdqm/SocketHelper.hpp"
#include "castor/vdqm/TapeAccessSpecification.hpp"
#include "castor/vdqm/TapeRequest.hpp"
#include "castor/vdqm/TapeDrive.hpp"
#include "castor/vdqm/TapeDriveStatusCodes.hpp"
#include "castor/vdqm/TapeServer.hpp"
#include "castor/vdqm/handler/TapeRequestHandler.hpp"
#include "castor/log/log.hpp"

#define VDQMSERV 1

#include "common.h" //for getconfent
#include "Ctape_constants.h"
#include "Cupv_constants.h"
#include "net.h"
#include "vdqm_constants.h"
#include "vmgr_api.h"
#include "vmgr_struct.h"
 
#include <sys/types.h>
#include <memory>
#include <string> 
#include <string.h>
#include <time.h>
#include <vector>

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
castor::vdqm::handler::TapeRequestHandler::TapeRequestHandler() 
   {
}


//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
castor::vdqm::handler::TapeRequestHandler::~TapeRequestHandler() throw() {
}

//------------------------------------------------------------------------------
// newTapeRequest
//------------------------------------------------------------------------------
void castor::vdqm::handler::TapeRequestHandler::newTapeRequest(
  const vdqmHdr_t &header, vdqmVolReq_t &msg, const Cuuid_t &cuuid)
   {
  
  // Used to get information about the tape
  struct vmgr_tape_info_byte_u64 tape_info;
  
  int rc = 0;
  char *p = NULL;


  //The parameters of the old vdqm VolReq Request
  std::list<castor::log::Param> params =
    {castor::log::Param("REQID", cuuid),
     castor::log::Param("client_name", msg.client_name),
     castor::log::Param("clientUID", msg.clientUID),
     castor::log::Param("clientGID", msg.clientGID),
     castor::log::Param("client_host", msg.client_host),
     castor::log::Param("client_port", msg.client_port),
     castor::log::Param("volid", msg.volid),
     castor::log::Param("mode", msg.mode),
     castor::log::Param("dgn", msg.dgn),
     castor::log::Param("drive",
       (*msg.drive == '\0' ? "***" : msg.drive)),
     castor::log::Param("server",
       (*msg.server == '\0' ? "***" : msg.server))};
  castor::log::write(LOG_DEBUG, "The parameters of the old vdqm VolReq Request",
                     params);
  
  //------------------------------------------------------------------------
  //The Tape related informations
  TapeRequest newTapeReq;
  newTapeReq.setCreationTime(0); // Will be set by a database trigger
  // The modification time will only be changed,
  // in case that an error occures during the tape assignment
  // procedure with RTCPD
  newTapeReq.setModificationTime(newTapeReq.creationTime());

  // We don't allow client to set priority
  newTapeReq.setPriority(VDQM_PRIORITY_NORMAL);

  // Set the remote copy type
  if(header.reqtype == VDQM4_TAPEBRIDGE_VOL_REQ) {
    newTapeReq.setRemoteCopyType("AGGREGATOR");
  } else {
    newTapeReq.setRemoteCopyType("RTCPD");
  }
   
  //The client related informations
  std::unique_ptr<ClientIdentification>
    clientData(new castor::vdqm::ClientIdentification());
  clientData->setMachine(msg.client_host);
  clientData->setUserName(msg.client_name);
  clientData->setPort(msg.client_port);
  clientData->setEuid(msg.clientUID);
  clientData->setEgid(msg.clientGID);
  clientData->setMagic(header.magic);
   
  // Select the tape or create one if it does not already exist
  std::unique_ptr<VdqmTape> tape(ptr_IVdqmService->selectOrCreateTape(
    msg.volid));                                     
  // The requested tape server: Return value is NULL if the server name is
  // empty
  std::unique_ptr<TapeServer> reqTapeServer(
    ptr_IVdqmService->selectOrCreateTapeServer(msg.server, false));
  
  memset(&tape_info,'\0',sizeof(tape_info));
  
  // "Try to get information about the tape from the VMGR daemon" message
  std::list<castor::log::Param> params2 = {castor::log::Param("REQID", cuuid)};
  castor::log::write(LOG_DEBUG,"Try to get information about the tape from the VMGR daemon", params2);     
  // Create a connection to vmgr daemon, to obtain more information about the
  // tape, like its density and its tapeModel.
  rc = vmgr_querytape_byte_u64(tape->vid().c_str(), 0, &tape_info, msg.dgn);    
  if ( rc == -1) {
    castor::exception::Exception ex(EVQDGNINVL);
    ex.getMessage() << "Errors, while using to vmgr_querytape: "
                    << "serrno = " << serrno
                    << std::endl;
    throw ex;      
  }
  
  // Validate the requested tape Access for the specific tape model.
  // In case of errors, the return value is NULL
  std::unique_ptr<TapeAccessSpecification> tapeAccessSpec(
    ptr_IVdqmService->selectTapeAccessSpecification(msg.mode,
    tape_info.density, tape_info.model));

  if (0 == tapeAccessSpec.get()) {
    castor::exception::Exception ex(EVQDGNINVL);
    ex.getMessage()
      << "The specified tape access mode doesn't exist in the db"
      << std::endl;
    throw ex;
  }
  
  // The requested device group name. If the entry doesn't yet exist, 
  // it will be created.
  std::unique_ptr<DeviceGroupName> dgName(
    ptr_IVdqmService->selectDeviceGroupName(msg.dgn));
    
  if(0 == dgName.get()) {
    castor::exception::Exception ex(EVQDGNINVL);
    ex.getMessage() << "DGN " <<  msg.dgn
                    << " does not exist in the db" << std::endl;
    throw ex;
  }

  // Connect the tapeRequest with the additional information
  newTapeReq.setClient(clientData.release()); // Tape request becomes owner
  newTapeReq.setTapeAccessSpecification(tapeAccessSpec.get());
  newTapeReq.setRequestedSrv(reqTapeServer.get());
  newTapeReq.setTape(tape.get());
  newTapeReq.setDeviceGroupName(dgName.get());
  
  // Set priority for tpwrite
  if ( (tapeAccessSpec->accessMode() == WRITE_ENABLE) &&
       ((p = getconfent("VDQM","WRITE_PRIORITY",0)) != NULL) ) {
    if ( strcmp(p,"YES") == 0 ) {
      newTapeReq.setPriority(VDQM_PRIORITY_MAX);
    }
  }
    
  // Request priority changed
  std::list<castor::log::Param> params3 = {
    castor::log::Param("REQID", cuuid),
    castor::log::Param("priority", newTapeReq.priority())};
  castor::log::write(LOG_DEBUG,
                     "Request priority changed",
                     params3);
  
  // Verify that the request doesn't exist, 
  // by calling IVdqmSvc->checkTapeRequest
  castor::log::write(LOG_DEBUG,
                     "Verify that the request doesn't exist, by calling IVdqmSvc->checkTapeRequest",
                     params3);

  // Verify that the request doesn't (yet) exist. If it doesn't exist,
  // the return value should be true.
  bool requestNotExists =
    ptr_IVdqmService->checkTapeRequest(&newTapeReq);
  if ( requestNotExists == false ) {
    castor::exception::Exception ex(EVQALREADY);
    ex.getMessage() << "Input request already queued " << std::endl;
    throw ex;
  }
    
  // Try to store Request into the data base
  std::list<castor::log::Param> params4 = {castor::log::Param("REQID", cuuid)};
  castor::log::write(LOG_DEBUG, "Try to store Request into the data base", params4);

  // Add the record to the volume queue
  castor::vdqm::DatabaseHelper::storeRepresentation(&newTapeReq, cuuid);
    
  // Now the newTapeReq has the id of its 
  // row representatioon in the db table.
  // 
  // Please note: Because of the restrictions of the old
  // protocol, we must convert the 64bit id into a 32bit number.
  // This is still sufficient to have a unique TapeRequest ID, because
  // the requests don't stay for long time in the db.
  msg.VolReqID = (unsigned int)newTapeReq.id();  
}


//------------------------------------------------------------------------------
// deleteTapeRequest
//------------------------------------------------------------------------------
void castor::vdqm::handler::TapeRequestHandler::deleteTapeRequest(
  const vdqmVolReq_t *const volumeRequest, const Cuuid_t cuuid,
  castor::io::ServerSocket &sock)  {

  // The tape request may be associated with a drive, and if so a lock will
  // be required on both the request and the drive.  Care must be taken as
  // locks are to be taken first on a drive and then on its associated tape
  // request, otherwise a deadlock may occur.
  std::unique_ptr<TapeRequest> tapeReq(
    ptr_IVdqmService->selectTapeRequest(volumeRequest->VolReqID));
    
  if ( tapeReq.get() == NULL ) {
    // If we don't find the tapeRequest in the db it is normally not a big
    // deal, because the assigned tape drive has probably already finished
    // the transfer.

    // "Couldn't find the tape request in db. Maybe it is already deleted?"
    // message
    std::list<castor::log::Param> params = {
      castor::log::Param("REQID", cuuid),
      castor::log::Param("Function", __PRETTY_FUNCTION__),
      castor::log::Param("tapeRequestID", volumeRequest->VolReqID)};
    castor::log::write(LOG_WARNING,
                       "Couldn't find the tape request in db. Maybe it is already deleted?", params);

    return;
  }

  // Sanity check - make sure the TapeRequest object has its associated
  // ClientIdentification object
  if(tapeReq->client() == NULL) {
    castor::exception::Exception ie;

    ie.getMessage() <<
      "Failed to delete tape request"
      ": Tape request does not have its associated ClientIdentification object"
      ": volumeRequest=" << volumeRequest->VolReqID;

    throw ie;
  }

  // The user requesting the delete must be either the owner of the tape
  // request or a tape operator
  if(volumeRequest->clientUID != tapeReq->client()->euid() ||
    volumeRequest->clientGID != tapeReq->client()->egid()) {

      SocketHelper::checkCupvPermissions(sock, volumeRequest->clientUID,
        volumeRequest->clientGID, P_TAPE_OPERATOR, "P_TAPE_OPERATOR",
        "VDQM_DEL_VOLREQ");
  }

/* TEMPORARILY REMOVED TAKING OF TAPE REQUEST LOCK DUE TO DEADLOCK

  // It is now safe to take a lock on the tape request as a lock has already
  // been taken on the associated tape drive if there is one.
  if(!ptr_IVdqmService->selectTapeRequestForUpdate(volumeRequest->VolReqID)) {
    // If we don't find the tapeRequest in the db it is normally not a big
    // deal, because the assigned tape drive has probably already finished
    // the transfer.

    // "Couldn't find the tape request in db. Maybe it is already deleted?"
    // message
    std::list<castor::log::Param> params = {
      castor::log::Param("REQID", cuuid),
      castor::log::Param("Function", __PRETTY_FUNCTION__),
      castor::log::Param("tapeRequestID", volumeRequest->VolReqID)};
      castor::log::write(LOG_WARNING,
      "Couldn't find the tape request in db. Maybe it is already deleted?", params);

    return;
  }
*/

  // castor::db::ora::OraVdqmSvc::selectTapeRequest uses fillObj to fill the
  // member TapeRequest::m_tapeDrive, but the memory referenced by this
  // member is not owned (i.e. deleted in the destructor of TapeRequest) by
  // TapeRequest.  Use an unique_ptr to manage the TapeDrive heap object.
  std::unique_ptr<TapeDrive> tapeDrive(tapeReq->tapeDrive());
    
  // OK, now we have the tapeReq and its client :-)
  if ( tapeDrive.get() ) {
    // If we are here, the TapeRequest is already assigned to a TapeDrive
    //TODO:
    //
    // PROBLEM:
    // Can only remove request, if it is not assigned to a drive. Otherwise
    // it should be cleanup by resetting the drive status to RELEASE + FREE.
    // Mark it as UNKNOWN until a unit status clarifies what has happened.
    //
    
    std::list<castor::log::Param> param = {
      castor::log::Param("REQID", cuuid),
      castor::log::Param("Function", __PRETTY_FUNCTION__),
      castor::log::Param("driveName", tapeDrive->driveName()),
      castor::log::Param("oldStatus",
        castor::vdqm::DevTools::tapeDriveStatus2Str(tapeDrive->status())),
      castor::log::Param("newStatus",
        castor::vdqm::DevTools::tapeDriveStatus2Str(STATUS_UNKNOWN))};
    castor::log::write(LOG_INFO, "Tape drive state transition",
                       param);

    // Set new TapeDriveStatusCode
    tapeDrive->setStatus(STATUS_UNKNOWN);
    
    // Modification time will be updated by a database trigger
      
    // Update the data base. To avoid a deadlock, the tape drive has to be 
    // updated first!
    try {
      castor::vdqm::DatabaseHelper::updateRepresentation(tapeDrive.get(),
        cuuid);
    }catch(castor::exception::Exception& ex) {
      // Log an error message and re-throw
      std::list<castor::log::Param> params = {
        castor::log::Param("REQID", cuuid),
        castor::log::Param("Function", __PRETTY_FUNCTION__),
        castor::log::Param("Message", "Failed to update tape drive: "
          + ex.getMessage().str()),
        castor::log::Param("errorCode", ex.code()),
        castor::log::Param("tapeRequestID", volumeRequest->VolReqID)};
      castor::log::write(LOG_WARNING, "Exception caught", params);

      throw ex;
    }
    try {
      castor::vdqm::DatabaseHelper::updateRepresentation(tapeReq.get(), cuuid);
    }catch(castor::exception::Exception& ex) {
      // Log an error message and re-throw
      std::list<castor::log::Param> params = {
        castor::log::Param("REQID", cuuid),
        castor::log::Param("Function", __PRETTY_FUNCTION__),
        castor::log::Param("Message", "Failed to update tape request: "
          + ex.getMessage().str()),
        castor::log::Param("errorCode", ex.code()),
        castor::log::Param("tapeRequestID", volumeRequest->VolReqID)};
      castor::log::write(LOG_WARNING, "Exception caught", params);

      throw ex;
    }

    castor::exception::Exception ex(EVQREQASS);
    ex.getMessage() <<
      "TapeRequest is assigned to a TapeDrive. Can't delete it at the moment."
      << " drive=" << tapeDrive->driveName()
      << std::endl;
    throw ex;

  // Else the TapeRequest is not assigned to a TapeDrive
  } else {

    // Remove the TapeRequest from the db (queue)
    castor::Services *svcs = castor::BaseObject::services();
    castor::BaseAddress ad;
    ad.setCnvSvcName("DbCnvSvc");
    ad.setCnvSvcType(castor::SVC_DBCNV);

    try {
      svcs->deleteRep(&ad, tapeReq.get(), false);
    } catch(castor::exception::Exception& ex) {
      // Log an error message and re-throw
      std::list<castor::log::Param> params = {
        castor::log::Param("REQID", cuuid),
        castor::log::Param("Function", __PRETTY_FUNCTION__),
        castor::log::Param("Message", "Failed to delete tape request: "
          + ex.getMessage().str()),
        castor::log::Param("errorCode", ex.code()),
        castor::log::Param("tapeRequestID", volumeRequest->VolReqID)};
      castor::log::write(LOG_WARNING, "Exception caught", params);

      throw ex;
    }
  
    // "TapeRequest and its ClientIdentification removed" message
    std::list<castor::log::Param> params = {
      castor::log::Param("REQID", cuuid),
      castor::log::Param("tapeRequestID", tapeReq->id())};
    castor::log::write(LOG_INFO,
                       "TapeRequest and its ClientIdentification removed", params);
  }
}


//------------------------------------------------------------------------------
// getQueuePosition
//------------------------------------------------------------------------------
int castor::vdqm::handler::TapeRequestHandler::getQueuePosition(
  const vdqmVolReq_t *const volumeRequest, const Cuuid_t cuuid) 
   {
    
  //To store the db related informations
  int queuePosition = 
    ptr_IVdqmService->getQueuePosition(volumeRequest->VolReqID);

  // Queue position of TapeRequest
  std::list<castor::log::Param> params = {
    castor::log::Param("REQID", cuuid),
    castor::log::Param("Queue position", queuePosition),
    castor::log::Param("tapeRequestID", volumeRequest->VolReqID)};
  castor::log::write(LOG_DEBUG, "Queue position of TapeRequest",
                     params);

  // Generate a log message if the TapeRequest was not found
  if(queuePosition == -1) {
    std::list<castor::log::Param> params = {
      castor::log::Param("REQID", cuuid),
      castor::log::Param("tapeRequestID", volumeRequest->VolReqID)};
    castor::log::write(LOG_INFO,
                       "Tape request not in queue", params);
  }

  return queuePosition;
}


//------------------------------------------------------------------------------
// sendTapeRequestQueue
//------------------------------------------------------------------------------
void castor::vdqm::handler::TapeRequestHandler::sendTapeRequestQueue(
  vdqmHdr_t *const header, vdqmVolReq_t *const volumeRequest,
  castor::vdqm::OldProtocolInterpreter *const oldProtInterpreter,
  const Cuuid_t)  {

  std::string dgn    = "";
  std::string server = "";
    
  if ( *(volumeRequest->dgn)    != '\0' ) dgn    = volumeRequest->dgn;
  if ( *(volumeRequest->server) != '\0' ) server = volumeRequest->server;

  try {
    // Results from the database
    castor::vdqm::IVdqmSvc::VolReqMsgList volReqs;

    // This method call retrieves the request queue from the database. The
    // result depends on the parameters. If the paramters are not specified,
    // then information about all of the requests is returned.
    ptr_IVdqmService->getTapeRequestQueue(volReqs, dgn, server);

    // If there is a result to send to the client
    if(volReqs.size() > 0 ) {
      for(castor::vdqm::IVdqmSvc::VolReqMsgList::iterator it = volReqs.begin();
        it != volReqs.end(); it++) {

/*
        //"Send information for showqueues command" message
        std::list<castor::log::Param> param = {
          castor::log::Param("REQID", cuuid),
          castor::log::Param("message", "TapeRequest info"),
          castor::log::Param("tapeRequestID", (*it)->VolReqID)};
          castor::log::write(LOG_DEBUG,
          "Send information for showqueues command", param);
*/
        //Send informations to the client
        oldProtInterpreter->sendToOldClient(header, *it, NULL);
      }
    }
  } catch (castor::exception::Exception& ex) {

    // To inform the client about the end of the queue, we send again a 
    // volumeRequest with the VolReqID = -1
    volumeRequest->VolReqID = -1;
    
    oldProtInterpreter->sendToOldClient(header, volumeRequest, NULL);

    throw ex;
  }

  // To inform the client about the end of the queue, we send again a 
  // volumeRequest with the VolReqID = -1
  volumeRequest->VolReqID = -1;
  
  oldProtInterpreter->sendToOldClient(header, volumeRequest, NULL);
}
