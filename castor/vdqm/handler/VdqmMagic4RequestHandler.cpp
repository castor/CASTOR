/******************************************************************************
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

#include "castor/exception/NotSupported.hpp"
#include "castor/io/ServerSocket.hpp"
#include "castor/vdqm/SocketHelper.hpp"
#include "castor/vdqm/handler/TapeRequestHandler.hpp"
#include "castor/vdqm/handler/VdqmMagic4RequestHandler.hpp"
#include "castor/log/log.hpp"
#include "Cupv_api.h"
#include "vdqm_constants.h"

#include <iostream>


void castor::vdqm::handler::VdqmMagic4RequestHandler::handleAggregatorVolReq(
  castor::io::ServerSocket&, const Cuuid_t &cuuid,
  const vdqmHdr_t &header, vdqmVolReq_t &msg)
   {

  std::list<castor::log::Param> param = {
    castor::log::Param("REQID", cuuid),
    castor::log::Param("VolReqID" , msg.VolReqID),
    castor::log::Param("DrvReqID" , msg.DrvReqID),
    castor::log::Param("priority" , msg.priority),
    castor::log::Param("client_port" , msg.client_port),
    castor::log::Param("recvtime" , msg.recvtime),
    castor::log::Param("clientUID", msg.clientUID),
    castor::log::Param("clientGID", msg.clientGID),
    castor::log::Param("mode", msg.mode),
    castor::log::Param("client_host", msg.client_host),
    castor::log::Param("volid", msg.volid),
    castor::log::Param("server", msg.server),
    castor::log::Param("drive", msg.drive),
    castor::log::Param("dgn", msg.dgn),
    castor::log::Param("client_name", msg.client_name)};
  castor::log::write(LOG_INFO,
                     "Handle VDQM4_AGGREGATOR_VOL_REQ", param);

  TapeRequestHandler requestHandler;
  requestHandler.newTapeRequest(header, msg, cuuid);
}
