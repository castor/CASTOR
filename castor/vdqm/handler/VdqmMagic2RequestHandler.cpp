/******************************************************************************
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

#include "castor/exception/NotSupported.hpp"
#include "castor/vdqm/handler/VdqmMagic2RequestHandler.hpp"
#include "castor/log/log.hpp"
#include "vdqm_constants.h"

#include <iostream>


void castor::vdqm::handler::VdqmMagic2RequestHandler::handleVolPriority(
  const Cuuid_t &cuuid, vdqmVolPriority_t &msg)
   {
  std::list<castor::log::Param> param = {
    castor::log::Param("REQID", cuuid),
    castor::log::Param("priority"    , msg.priority),
    castor::log::Param("clientUID"   , msg.clientUID),
    castor::log::Param("clientGID"   , msg.clientGID),
    castor::log::Param("clientHost"  , msg.clientHost),
    castor::log::Param("TPVID"         , msg.vid),
    castor::log::Param("tpMode"      , msg.tpMode),
    castor::log::Param("lifespanType", msg.lifespanType)};
  castor::log::write(LOG_INFO,
                     "Handle VDQM2_VOL_PRIORITY", param);

  ptr_IVdqmService->setVolPriority(msg.priority, msg.clientUID, msg.clientGID,
    msg.clientHost, msg.vid, msg.tpMode, msg.lifespanType);
}
