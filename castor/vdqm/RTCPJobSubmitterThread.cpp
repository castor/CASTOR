/******************************************************************************
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

#include "castor/BaseAddress.hpp"
#include "castor/Constants.hpp"
#include "castor/IService.hpp"
#include "castor/PortNumbers.hpp"
#include "castor/Services.hpp"
#include "castor/vdqm/ClientIdentification.hpp"
#include "castor/vdqm/DatabaseHelper.hpp"
#include "castor/vdqm/DeviceGroupName.hpp"
#include "castor/vdqm/DevTools.hpp"
#include "castor/vdqm/IVdqmSvc.hpp"
#include "castor/vdqm/RemoteCopyConnection.hpp"
#include "castor/vdqm/RTCPJobSubmitterThread.hpp"
#include "castor/vdqm/TapeAccessSpecification.hpp"
#include "castor/vdqm/TapeDrive.hpp"
#include "castor/vdqm/TapeRequest.hpp"
#include "castor/vdqm/TapeServer.hpp"
#include "castor/vdqm/VdqmTape.hpp"
#include "castor/log/log.hpp"
#include "rtcp_constants.h"

#include <memory>
#include <time.h>

//-----------------------------------------------------------------------------
// constructor
//-----------------------------------------------------------------------------
castor::vdqm::RTCPJobSubmitterThread::RTCPJobSubmitterThread()
  throw() {
}


//-----------------------------------------------------------------------------
// destructor
//-----------------------------------------------------------------------------
castor::vdqm::RTCPJobSubmitterThread::~RTCPJobSubmitterThread()
  throw() {
}


//-----------------------------------------------------------------------------
// select
//-----------------------------------------------------------------------------
castor::IObject* castor::vdqm::RTCPJobSubmitterThread::select()
  throw() {

  Cuuid_t                cuuid    = nullCuuid;
  castor::vdqm::IVdqmSvc *vdqmSvc = NULL;
  castor::IObject        *obj     = NULL;


  Cuuid_create(&cuuid);

  try {
    vdqmSvc = getDbVdqmSvc();
  } catch(castor::exception::Exception& e) {
    // "Could not get DbVdqmSvc"
    std::list<castor::log::Param> params = {
      castor::log::Param("REQID", cuuid),
      castor::log::Param("Function",
        "castor::vdqm::RTCPJobSubmitterThread::select"),
      castor::log::Param("Message", e.getMessage().str()),
      castor::log::Param("Code", e.code())
    };
    castor::log::write(LOG_ERR, "Could not get DbVdqmSvc", params);

    return NULL;
  }

  try
  {
    obj = vdqmSvc->requestToSubmit();
    vdqmSvc->commit();
  } catch (castor::exception::Exception& e) {
    std::list<castor::log::Param> params = {
      castor::log::Param("REQID", cuuid),
      castor::log::Param("Function",
        "castor::vdqm::RTCPJobSubmitterThread::select"),
      castor::log::Param("Message", e.getMessage().str()),
      castor::log::Param("Code", e.code())
    };
    castor::log::write(LOG_ERR,
                       "Error occured when determining if there is matching free drive and waiting request", params);

    return NULL;
  }

  return obj;
}


//------------------------------------------------------------------------------
// Inner class TapeRequestAutoPtr constructor
//------------------------------------------------------------------------------
castor::vdqm::RTCPJobSubmitterThread::TapeRequestAutoPtr::TapeRequestAutoPtr(
  castor::vdqm::TapeRequest *const tapeRequest) throw() :
  m_tapeRequest(tapeRequest) {
}


//------------------------------------------------------------------------------
// Inner class TapeRequestAutoPtr get method
//------------------------------------------------------------------------------
castor::vdqm::TapeRequest
  *castor::vdqm::RTCPJobSubmitterThread::TapeRequestAutoPtr::get() throw() {
  return m_tapeRequest;
}


//------------------------------------------------------------------------------
// Inner class TapeDriveAutoPtr destructor
//------------------------------------------------------------------------------
castor::vdqm::RTCPJobSubmitterThread::TapeRequestAutoPtr::~TapeRequestAutoPtr()
throw() {
  // Return if there is no tape request to delete
  if(m_tapeRequest == NULL) {
    return;
  }

  delete m_tapeRequest->tape();
  m_tapeRequest->setTape(NULL);

  delete m_tapeRequest->requestedSrv();
  m_tapeRequest->setRequestedSrv(NULL);

  delete m_tapeRequest->deviceGroupName();
  m_tapeRequest->setDeviceGroupName(NULL);

  delete m_tapeRequest->tapeAccessSpecification();
  m_tapeRequest->setTapeAccessSpecification(NULL);

  if(m_tapeRequest->tapeDrive() != NULL) {

    delete m_tapeRequest->tapeDrive()->deviceGroupName();
    m_tapeRequest->tapeDrive()->setDeviceGroupName(NULL);

    // Remove the request's drive from the request's tape server to break the
    // circular dependency
    m_tapeRequest->tapeDrive()->tapeServer()->removeTapeDrives(
      m_tapeRequest->tapeDrive());

    delete  m_tapeRequest->tapeDrive()->tapeServer();
    m_tapeRequest->tapeDrive()->setTapeServer(NULL);

    delete m_tapeRequest->tapeDrive();
    m_tapeRequest->setTapeDrive(NULL);
  }

  delete m_tapeRequest;
}


//-----------------------------------------------------------------------------
// process
//-----------------------------------------------------------------------------
void castor::vdqm::RTCPJobSubmitterThread::process(castor::IObject *param)
  throw() {

  Cuuid_t                cuuid    = nullCuuid;
  castor::vdqm::IVdqmSvc *vdqmSvc = NULL;


  Cuuid_create(&cuuid);

  // Create an auto pointer to delete the tape request object when it goes out
  // of scope.
  TapeRequestAutoPtr request(dynamic_cast<castor::vdqm::TapeRequest*>(param));

  // If the dynamic cast failed
  if(request.get() == NULL) {
    castor::exception::Exception e;

    e.getMessage()
      << "Failed to cast param to castor::vdqm::TapeRequest*";

    throw e;
  }

  try {
    vdqmSvc = getDbVdqmSvc();
  } catch(castor::exception::Exception& e) {
    // "Could not get DbVdqmSvc"
    std::list<castor::log::Param> params = {
      castor::log::Param("REQID", cuuid),
      castor::log::Param("Function",
        "castor::vdqm::RTCPJobSubmitterThread::process"),
      castor::log::Param("Message", e.getMessage().str()),
      castor::log::Param("Code", e.code())
    };
    castor::log::write(LOG_ERR, "Could not get DbVdqmSvc", params);

    return;
  }

  TapeDrive *tapeDrive       = request.get()->tapeDrive();
  bool      requestSubmitted = false;

  try {

    // Submit remote copy job
    submitJob(cuuid, request.get());
    requestSubmitted = true;

  } catch(castor::exception::Exception& ex) {
    std::list<castor::log::Param> params = {
      castor::log::Param("REQID", cuuid),
      castor::log::Param("tapeDriveID", tapeDrive->id()),
      castor::log::Param("driveName", tapeDrive->driveName()),
      castor::log::Param("tapeRequestID", request.get()->id()),
      castor::log::Param("Message", ex.getMessage().str()),
      castor::log::Param("Code", ex.code())};
    castor::log::write(LOG_ERR,
                       "Failed to submit remote copy job", params);

    try {
      bool       driveExists            = false;
      int        driveStatusBefore      = -1;
      int        driveStatusAfter       = -1;
      u_signed64 runningRequestIdBefore = 0;
      u_signed64 runningRequestIdAfter  = 0;
      bool       requestExists          = false;
      int        requestStatusBefore    = -1;
      int        requestStatusAfter     = -1;
      u_signed64 requestDriveIdBefore   = 0;
      u_signed64 requestDriveIdAfter    = 0;

      // Reset drive and request
      vdqmSvc->resetDriveAndRequest(
        tapeDrive->id(),
        request.get()->id(),
        driveExists,
        driveStatusBefore,
        driveStatusAfter,
        runningRequestIdBefore,
        runningRequestIdAfter,
        requestExists,
        requestStatusBefore,
        requestStatusAfter,
        requestDriveIdBefore,
        requestDriveIdAfter);

      std::list<castor::log::Param> params = {
        castor::log::Param("REQID", cuuid),
        castor::log::Param("tapeDriveID", tapeDrive->id()),
        castor::log::Param("driveName", tapeDrive->driveName()),
        castor::log::Param("tapeRequestID", request.get()->id()),
        castor::log::Param("driveExists", driveExists),
        castor::log::Param("driveStatusBef", driveStatusBefore),
        castor::log::Param("driveStatusAft", driveStatusAfter),
        castor::log::Param("runningRequestIdBef", runningRequestIdBefore),
        castor::log::Param("runningRequestIdAft",runningRequestIdAfter),
        castor::log::Param("requestExists", requestExists),
        castor::log::Param("requestStatusBef", requestStatusBefore),
        castor::log::Param("requestStatusAft", requestStatusAfter),
        castor::log::Param("requestDriveIdBef", requestDriveIdBefore),
        castor::log::Param("requestDriveIdAft", requestDriveIdAfter)};
      castor::log::write(LOG_INFO,
                         "Reset drive and request", params);

    } catch(castor::exception::Exception& ex) {
      std::list<castor::log::Param> params = {
        castor::log::Param("REQID", cuuid),
        castor::log::Param("tapeDriveID", tapeDrive->id()),
        castor::log::Param("driveName", tapeDrive->driveName()),
        castor::log::Param("tapeRequestID", request.get()->id()),
        castor::log::Param("Message", ex.getMessage().str()),
        castor::log::Param("Code", ex.code())};
      castor::log::write(LOG_ERR,
                         "Failed to write reset of drive and request to the database", params);
    }
  }

  if(requestSubmitted) {
    try {
      bool       driveExists            = false;
      int        driveStatusBefore      = -1;
      int        driveStatusAfter       = -1;
      u_signed64 runningRequestIdBefore = 0;
      u_signed64 runningRequestIdAfter  = 0;
      bool       requestExists          = FALSE;
      int        requestStatusBefore    = -1;
      int        requestStatusAfter     = -1;
      u_signed64 requestDriveIdBefore   = 0;
      u_signed64 requestDriveIdAfter    = 0;

      // Write request submitted state change to the database
      if(!vdqmSvc->requestSubmitted(
        tapeDrive->id(),
        request.get()->id(),
        driveExists,
        driveStatusBefore,
        driveStatusAfter,
        runningRequestIdBefore,
        runningRequestIdAfter,
        requestExists,
        requestStatusBefore,
        requestStatusAfter,
        requestDriveIdBefore,
        requestDriveIdAfter)) {

        std::list<castor::log::Param> params = {
          castor::log::Param("REQID", cuuid),
          castor::log::Param("tapeDriveID", tapeDrive->id()),
          castor::log::Param("driveName", tapeDrive->driveName()),
          castor::log::Param("tapeRequestID", request.get()->id()),
          castor::log::Param("driveExists", driveExists),
          castor::log::Param("driveStatusBef", driveStatusBefore),
          castor::log::Param("driveStatusAft", driveStatusAfter),
          castor::log::Param("runningRequestIdBef", runningRequestIdBefore),
          castor::log::Param("runningRequestIdAft",runningRequestIdAfter),
          castor::log::Param("requestExists", requestExists),
          castor::log::Param("requestStatusBef", requestStatusBefore),
          castor::log::Param("requestStatusAf", requestStatusAfter),
          castor::log::Param("requestDriveIdBef", requestDriveIdBefore),
          castor::log::Param("requestDriveIdAft", requestDriveIdAfter)};
        castor::log::write(LOG_ERR,
                           "Failed to move the state of the request from REQUEST_BEINGSUBMITTED to REQUEST_SUBMITTED", params);
      }
    } catch(castor::exception::Exception& ex) {
      std::list<castor::log::Param> params = {
        castor::log::Param("REQID", cuuid),
        castor::log::Param("tapeDriveID", tapeDrive->id()),
        castor::log::Param("driveName", tapeDrive->driveName()),
        castor::log::Param("tapeRequestID", request.get()->id()),
        castor::log::Param("Message", ex.getMessage().str()),
        castor::log::Param("Code", ex.code())};
      castor::log::write(LOG_ERR,
                         "Failed to write request submitted state change to the database", params);
    }
  } // if(requestSubmitted)

  try
  {
    // Commit the changes to the DB
    castor::BaseAddress ad;
    ad.setCnvSvcName("DbCnvSvc");
    ad.setCnvSvcType(castor::SVC_DBCNV);
    services()->commit(&ad);
  } catch(castor::exception::Exception& ex) {
    std::list<castor::log::Param> params = {
      castor::log::Param("REQID", cuuid),
      castor::log::Param("Function", "RTCPJobSubmitterThread::process"),
      castor::log::Param("Message", "Failed to commit changes" +
        ex.getMessage().str()),
      castor::log::Param("Code", ex.code())
    };
    castor::log::write(LOG_ERR,
                       "Error occurred whilst allocating a free drive to a tape request", params);
  }
}


//-----------------------------------------------------------------------------
// getDbVdqmSvc
//-----------------------------------------------------------------------------
castor::vdqm::IVdqmSvc *castor::vdqm::RTCPJobSubmitterThread::getDbVdqmSvc()
  
{
  castor::Services *svcs = castor::BaseObject::services();
  castor::IService *svc = svcs->service("DbVdqmSvc", castor::SVC_DBVDQMSVC);
  castor::vdqm::IVdqmSvc *vdqmSvc = dynamic_cast<castor::vdqm::IVdqmSvc*>(svc);

  if (0 == vdqmSvc) {
    castor::exception::Exception ex;
    ex.getMessage()
      << "Failed to cast castor::IService* to castor::vdqm::IVdqmSvc*";

    throw ex;
  }

  return vdqmSvc;
}


//-----------------------------------------------------------------------------
// submitJob
//-----------------------------------------------------------------------------
void castor::vdqm::RTCPJobSubmitterThread::submitJob(const Cuuid_t &cuuid,
  castor::vdqm::TapeRequest *request) {
  castor::vdqm::ClientIdentification *client = request->client();
  castor::vdqm::TapeDrive *tapeDrive  = request->tapeDrive();
  castor::vdqm::DeviceGroupName *dgn = tapeDrive->deviceGroupName();
  castor::vdqm::TapeServer *tapeServer = tapeDrive->tapeServer();
  const std::string remoteCopyType = request->remoteCopyType();
  const unsigned short port = TAPESERVER_VDQMPORT;
  RemoteCopyConnection connection(port, tapeServer->serverName());

  try {
    connection.connect();
  } catch (castor::exception::Exception& e) {
    castor::exception::Exception ie;

    ie.getMessage()
      << "Failed to connect to " << remoteCopyType << ": "
      << e.getMessage().str();

    throw ie;
  }

  bool acknSucc = true;

  {
    const u_signed64 now = time(NULL);
    const u_signed64 requestAgeSecs = now > request->creationTime() ?
      now - request->creationTime() : 0;
    std::list<castor::log::Param> params = {
      castor::log::Param("REQID", cuuid),
      castor::log::Param("remoteCopyType", remoteCopyType),
      castor::log::Param("tapeRequestID", request->id()),
      castor::log::Param("clientUserName", client->userName()),
      castor::log::Param("clientMachine", client->machine()),
      castor::log::Param("clientPort", client->port()),
      castor::log::Param("clientEuid", client->euid()),
      castor::log::Param("clientEgid", client->egid()),
      castor::log::Param("deviceGroupName", dgn->dgName()),
      castor::log::Param("tapeDriveName", tapeDrive->driveName()),
      castor::log::Param("tapeServerName", tapeServer->serverName()),
      castor::log::Param("tapeServerPort", port),
      castor::log::Param("requestAgeSecs", requestAgeSecs)};
    castor::log::write(LOG_INFO, "Sending remote copy job", params);
  }

  try {
    acknSucc = connection.sendJob(cuuid, remoteCopyType.c_str(), request->id(),
      client->userName(), client->machine(), client->port(), client->euid(),
      client->egid(), dgn->dgName(), tapeDrive->driveName());
  } catch (castor::exception::Exception& e) {
    castor::exception::Exception ie;

    ie.getMessage()
      << "Failed to send job to " << remoteCopyType << ": "
      << e.getMessage().str();

    throw ie;
  }

  if(!acknSucc) {
    castor::exception::Exception ie;

    ie.getMessage() <<
      "Did not receive an acknSucc from sending a job to " << remoteCopyType;

    throw ie;
  }
}
