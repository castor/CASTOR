/******************************************************************************
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/
 
#include "castor/log/SyslogLogger.hpp"
#include "castor/server/BaseThreadPool.hpp"
#include "castor/server/SignalThreadPool.hpp"
#include "castor/server/TCPListenerThreadPool.hpp"
#include "castor/vdqm/DriveSchedulerThread.hpp"
#include "castor/vdqm/RequestHandlerThread.hpp"
#include "castor/vdqm/RTCPJobSubmitterThread.hpp"
#include "castor/vdqm/VdqmServer.hpp"
#include "castor/log/log.hpp"

#include <iostream>
#include <stdio.h>
#include <string>


//------------------------------------------------------------------------------
// main method
//------------------------------------------------------------------------------
int main(int argc, char *argv[]) {
  try {
    castor::log::init(new castor::log::SyslogLogger("vdqmd"));

    castor::vdqm::VdqmServer       server(std::cout, std::cerr);
    Cuuid_t                        cuuid                       = cuuid;
    castor::server::BaseThreadPool *requestHandlerThreadPool   = NULL;
    castor::server::BaseThreadPool *driveSchedulerThreadPool   = NULL;
    castor::server::BaseThreadPool *rtcpJobSubmitterThreadPool = NULL;

    Cuuid_create(&cuuid);

    server.logStart(cuuid, argc, argv);
    server.parseCommandLine(argc, argv);
    server.initDatabaseService(cuuid);


    //------------------------
    // Create the thread pools
    //------------------------

    {
      int vdqmPort = 0;

      try {
        vdqmPort = server.getListenPort();
      } catch(castor::exception::InvalidConfigEntry &ex) {
        std::list<castor::log::Param> params = {
          castor::log::Param("REQID", cuuid),
          castor::log::Param("invalidValue", ex.getEntryValue())};
        castor::log::write(LOG_ERR,
                           "The VDQM PORT configuration entry is invalid", params);

        std::cerr << std::endl << "Error: " << ex.getMessage().str()
          << std::endl;

        return 1;
      }

      server.addThreadPool(
        new castor::server::TCPListenerThreadPool("RequestHandlerThreadPool",
          new castor::vdqm::RequestHandlerThread(), vdqmPort));

      std::list<castor::log::Param> params = {
        castor::log::Param("REQID", cuuid),
        castor::log::Param("vdqmPort", vdqmPort)};
      castor::log::write(LOG_INFO,
                         "Set the VDQM PORT", params);
    }

    {
      int timeout = 0;

      try {
        timeout = server.getSchedulerTimeout();
      } catch(castor::exception::InvalidConfigEntry &ex) {
        std::list<castor::log::Param> params = {
          castor::log::Param("REQID", cuuid),
          castor::log::Param("invalidValue", ex.getEntryValue())};
        castor::log::write(LOG_ERR,
                           "The VDQM SCHEDULERTIMEOUT configuration entry is invalid", params);

        std::cerr << std::endl << "Error: " << ex.getMessage().str()
          << std::endl;

        return 1;
      }

      server.addThreadPool(
        new castor::server::SignalThreadPool("DriveSchedulerThreadPool",
          new castor::vdqm::DriveSchedulerThread(), timeout));

      std::list<castor::log::Param> params = {
        castor::log::Param("REQID", cuuid),
        castor::log::Param("timeout", timeout)};
      castor::log::write(LOG_INFO,
                         "Set VDQM SCHEDULERTIMEOUT", params);
    }

    {
      int timeout = 0;

      try {
        timeout = server.getRTCPJobSubmitterTimeout();
      } catch(castor::exception::InvalidConfigEntry &ex) {
        std::list<castor::log::Param> params = {
          castor::log::Param("REQID", cuuid),
          castor::log::Param("invalidValue", ex.getEntryValue())};
        castor::log::write(LOG_ERR,
                           "The VDQM RTCPJOBSUBMITTERTIMEOUT configuration entry is invalid", params);

        std::cerr << std::endl << "Error: " << ex.getMessage().str()
          << std::endl;

        return 1;
      }

      server.addThreadPool(
        new castor::server::SignalThreadPool("JobSubmitterThreadPool",
          new castor::vdqm::RTCPJobSubmitterThread(), timeout));

      std::list<castor::log::Param> params = {
        castor::log::Param("REQID", cuuid),
        castor::log::Param("timeout", timeout)};
      castor::log::write(LOG_INFO,
                         "Set VDQM RTCPJOBSUBMITTERTIMEOUT", params);
    }

    // Add a dedicated UDP thread pool for getting wakeup notifications
    {
      int notifyPort = 0;

      try {
        notifyPort = server.getNotifyPort();
      } catch(castor::exception::InvalidConfigEntry &ex) {
        std::list<castor::log::Param> params = {
          castor::log::Param("REQID", cuuid),
          castor::log::Param("invalidValue", ex.getEntryValue())};
        castor::log::write(LOG_ERR,
                           "The VDQM NOTIFYPORT configuration entry is invalid", params);

        std::cerr << std::endl << "Error: " << ex.getMessage().str()
          << std::endl;

        return 1;
      }

      server.addNotifierThreadPool(notifyPort);

      std::list<castor::log::Param> params = {
        castor::log::Param("REQID", cuuid),
        castor::log::Param("notifyPort", notifyPort)};
      castor::log::write(LOG_INFO,
                         "Set the VDQM NOTIFYPORT", params);
    }

    //----------------------------------------------
    // Set the number of threads in each thread pool
    //----------------------------------------------

    requestHandlerThreadPool = server.getThreadPool('R');
    if(requestHandlerThreadPool == NULL) {
      std::cerr << "Failed to get RequestHandlerThreadPool" << std::endl;
      return 1;
    }

    {
      int nbThreads = server.getRequestHandlerThreadNumber();

      requestHandlerThreadPool->setNbThreads(nbThreads);

      std::list<castor::log::Param> params = {
        castor::log::Param("REQID", cuuid),
        castor::log::Param("nbThreads", nbThreads)};
      castor::log::write(LOG_INFO,
                         "Set the number of request handler threads", params);
    }

    driveSchedulerThreadPool = server.getThreadPool('D');
    if(driveSchedulerThreadPool == NULL) {
      std::cerr << "Failed to get DriveSchedulerThreadPool" << std::endl;
      return 1;
    }

    {
      int nbThreads = server.getSchedulerThreadNumber();

      driveSchedulerThreadPool->setNbThreads(nbThreads);

      std::list<castor::log::Param> params = {
        castor::log::Param("REQID", cuuid),
        castor::log::Param("nbThreads", nbThreads)};
      castor::log::write(LOG_INFO,
                         "Set the number of scheduler threads", params);
    }

    rtcpJobSubmitterThreadPool = server.getThreadPool('J');
    if(rtcpJobSubmitterThreadPool == NULL) {
      std::cerr << "Failed to get JobSubmitterThreadPool" << std::endl;
      return 1;
    }

    {
      int nbThreads = server.getRTCPJobSubmitterThreadNumber();

      rtcpJobSubmitterThreadPool->setNbThreads(nbThreads);

      std::list<castor::log::Param> params = {
        castor::log::Param("REQID", cuuid),
        castor::log::Param("nbThreads", nbThreads)};
      castor::log::write(LOG_INFO,
                         "Set the number of RTCP job submitter threads", params);
    }


    try {

      //-----------------
      // Start the server
      //-----------------
      const bool runAsStagerSuperuser = true;
      server.start(runAsStagerSuperuser);
      
    } catch (castor::exception::Exception &e) {
      std::cerr << "Failed to start VDQM server : "
                << sstrerror(e.code()) << std::endl
                << e.getMessage().str() << std::endl;

      return 1;
    } catch (...) {
      std::cerr << "Failed to start VDQM server : Caught general exception!"
        << std::endl;
    
      return 1;
    }

  } catch(castor::exception::Exception &e) {
    std::cerr << "Failed to start VDQM server : "
              << sstrerror(e.code()) << std::endl
              << e.getMessage().str() << std::endl;

    return 1;
  }
  
  return 0;
}
