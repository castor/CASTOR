.\" Copyright (C) 2003  CERN
.\" This program is free software; you can redistribute it and/or
.\" modify it under the terms of the GNU General Public License
.\" as published by the Free Software Foundation; either version 2
.\" of the License, or (at your option) any later version.
.\" This program is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\" You should have received a copy of the GNU General Public License
.\" along with this program; if not, write to the Free Software
.\" Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
.TH VDQM "8castor" "$Date: 2009/08/18 09:42:56 $" CASTOR "VDQM server daemon"
.SH NAME
vdqmd \- Volume and Drive Queue Manager (VDQM) server
.SH SYNOPSIS
.BI "vdqmd [ -f ] [ -c ] [ -h ] [ -r num ] [ -j num ] [ -s num ]"

.SH DESCRIPTION
.B vdqmd
is the central CASTOR server handling all tape volume and drive queues.
It runs as a daemon on a host which must be known to all clients and
tape servers.  The following methods are tried in sequence by clients and
tape servers to determine this connection information:
.TP
1. Read the environment variables:
\fBVDQM_HOST\fP
.br
\fBVDQM_PORT\fP
.TP
2. Read the VDQM entries in /etc/castor/castor.conf:
\fBVDQM HOST\fP
.br
\fBVDQM PORT\fP
.TP
3. Use compile time constants:
The VDQM host value is set through the
\fBVdqmHost\fP macro in the \fBconfig/site.def\fP file in the \fBCASTOR2\fP
build directory.  The VDQM port number is hard coded to be always \fB5012\fP.
.P
Please note that the environment variable \fBVDQM_HOST\fP, the
\fB/etc/castor/castor.conf\fP entry \fBVDQM HOST\fP and the \fBVdqmHost\fP
macro contain both the VDQM primary and replica hosts.
The format can be either of the following:
.RS
\fBprimary_host<space>replica\fP
.br
\fBprimary_host<tab>replica\fP
.RE
.P
The purpose of the replica server is to reduce the impact of a crash of the
primary server.  VDQM clients will always try to contact the primary server
first and if it is not responding, then they will try the replica.
.P
The \fBvdqmd\fP responds to requests from the \fBshowqueues(1)\fP and
\fBvdqm_admin(1)\fP utilities.
.P
The \fBvdqmd\fP is a stateless server.  This means the \fBvdqmd\fP
stores its current state using a database server.  If the \fBvdqmd\fP
is restarted, then it will retrieve its state from the database server.  The
\fBvdqmd\fP reads the database connection details from the following file:
.RS
\fB/etc/castor/ORAVDQMCONFIG\fP
.RE
.P
The contents of this file will be similar to the following:
\fB
.RS
.nf
#
## DB Configuration
#
DbCnvSvc       user    vdqm_writer
DbCnvSvc       passwd  XXXXXXXX
DbCnvSvc       dbName  vdqm_database
.fi
.RE
\fP
.SH OPTIONS
.TP
\fB\-f, \-\-foreground
Remain in the foreground
.TP
\fB\-c, \-\-config config-file
Set the location of the configuration file.  The default is
\fB/etc/castor/castor.conf\fP
.TP
\fB\-h, \-\-help
Get usage information.
.TP
\fB\-r, \-\-requestHandlerThreads num
Set the number of request handler threads.  The default is 20.
.TP
\fB\-j, \-\-RTCPJobSubmitterThreads num
Set the number of RTCP job submitter threads.  The default is 1.
.TP
\fB\-s, \-\-schedulerThreads num
Set the number of scheduler threads.  The default is 1.


.SH FILES
.TP
.B /etc/castor/castor.conf
CASTOR configuration file.
.TP
.B /etc/castor/ORAVDQMCONFIG
Database connection details.
.TP
.B /var/log/castor/vdqm.log
Default location of the VDQM server log file.

.SH SEE ALSO
.BR showqueues(1) ,
.BR vdqm_admin(1) ,
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
