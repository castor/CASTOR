.\" Copyright (C) 2005 by CERN IT/ADC
.\" All rights reserved
.\"
.TH VDQMDBINIT "1castor" "$Date: 2008/07/21 12:49:14 $" CASTOR "Initializes VDQM database with static values"
.SH NAME
vdqmDBInit
.SH SYNOPSIS
.B vdqmDBInit [-c|--config config-file] [-h|--help]
.SH DESCRIPTION
.B vdqmDBInit
Initializes VDQM database with static values. Connects to
VMGR daemon to retrieve information and fills
TapeAccessSpecification and DeviceGroupName tables
accordingly.
.SH OPTIONS
.TP
\fB\-c, \-\-config config-file
Set the location of the configuration file.  The default is
\fB/etc/castor/castor.conf\fP
.TP
\fB\-h, \-\-help
Get usage information.

.SH EXAMPLES
.fi
# vdqmDBInit

.fi
Try to update DeviceGroupName table in db...
.fi
New DeviceGroupName row inserted into db: dgName = 3592A0, libraryName = IBM_LIB0
.fi
New DeviceGroupName row inserted into db: dgName = 3592B1, libraryName = IBM_LIB1
.fi
New DeviceGroupName row inserted into db: dgName = 994BR4, libraryName = STK_ACS4
.fi
New DeviceGroupName row inserted into db: dgName = 994BR5, libraryName = STK_ACS5
.fi
 ...
.fi

Try to update TapeAccessSpecification table in db...
.fi
New TapeAccessSpecification row inserted into db: accessMode = 0, density = 500G, tapeModel = 3592
.fi
New TapeAccessSpecification row inserted into db: accessMode = 1, density = 500G, tapeModel = 3592
.fi
New TapeAccessSpecification row inserted into db: accessMode = 0, density = 500GC, tapeModel = 3592
.fi
New TapeAccessSpecification row inserted into db: accessMode = 1, density = 500GC, tapeModel = 3592
.fi
New TapeAccessSpecification row inserted into db: accessMode = 0, density = 200G, tapeModel = 9940
.fi
New TapeAccessSpecification row inserted into db: accessMode = 1, density = 200G, tapeModel = 9940
.fi
New TapeAccessSpecification row inserted into db: accessMode = 0, density = 200GC, tapeModel = 9940
.fi
New TapeAccessSpecification row inserted into db: accessMode = 1, density = 200GC, tapeModel = 9940
.fi
 ...
.ft
.fi
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
