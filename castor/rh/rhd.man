.lf 8 rhd.man
.TH RHD "8castor" "2007/08/16 16:50:00 CERN IT-FIO" CASTOR "request handler"
.SH NAME
rhd \- start the CASTOR Request Handler daemon
.SH SYNOPSIS
.B rhd
[
.BI -f
]
[
.BI -h
]
[
.BI -s
]
[
.BI -c " <config-file>"
]
[
.BI -p " <port>"
]
[
.BI -n
]
[
.BI -m
]
[
.BI -R
.BI <nb\ threads>
]
.SH DESCRIPTION
.LP
The
.B rhd
command starts the CASTOR request handler daemon,
which is the user entry point into the CASTOR system.
.LP

.SH OPTIONS

.TP
.BI \-f,\ \-\-foreground
runs in foreground mode (no daemonization).
.TP
.BI \-h,\ \-\-help
displays command usage help.
.TP
.BI \-s,\ \-\-secure
runs the daemon in secure mode
.TP
.BI \-c,\ \-\-config\ <config-file>
specifies the location of the configuration file to use. The default value
is /etc/castor/castor.conf.
.TP
.BI \-p,\ \-\-port\ <port>
specifies the port on which to listen for client request. The default port for the
unsecure daemon is 9002 and for the secure is 9007.
The port can also be overwritten using the RH_PORT enviroment variable or
by having a RH/PORT entry in the /etc/castor/castor.conf file.
.TP
.BI \-n,\ \-\-no-wait
when no thread is available to process incoming requests, the Request Handler's
listener normally waits for a thread to become idle; this may result in the service
refusing further connections. By specifying --no-wait, in such cases the listener
immediately terminates the incoming connections and logs a warning message.
.TP
.BI \-m,\ \-\-metrics
enables internal metrics collection, usually into \fI/var/spool/castor/rhd.<role>.xml\fR.
.TP
.BI \-R,\ \-\-Rthreads\ <nb\ threads>
specifies the number of threads to use to handle client requests. Default is 20.

.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
