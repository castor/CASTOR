/******************************************************************************
 *
 * This file is part of the Castor project.
 * See http://castor.web.cern.ch/castor
 *
 * Copyright (C) 2003  CERN
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

#include "castor/io/io.hpp"
#include "castor/legacymsg/CommonMarshal.hpp"
#include "castor/legacymsg/RtcpMarshal.hpp"
#include "castor/legacymsg/VdqmMarshal.hpp"
#include "castor/legacymsg/VdqmProxyTcpIp.hpp"
#include "castor/log/log.hpp"
#include "castor/utils/SmartFd.hpp"
#include "castor/utils/utils.hpp"
#include "rtcp_constants.h"
#include "vdqm_constants.h"

//------------------------------------------------------------------------------
// constructor
//------------------------------------------------------------------------------
castor::legacymsg::VdqmProxyTcpIp::VdqmProxyTcpIp(
  const std::string &vdqmHostName, const unsigned short vdqmPort,
  const int netTimeout) throw():
  m_vdqmHostName(vdqmHostName),
  m_vdqmPort(vdqmPort),
  m_netTimeout(netTimeout) {
}

//------------------------------------------------------------------------------
// destructor
//------------------------------------------------------------------------------
castor::legacymsg::VdqmProxyTcpIp::~VdqmProxyTcpIp() throw() {
}

//------------------------------------------------------------------------------
// setDriveDown
//------------------------------------------------------------------------------
void castor::legacymsg::VdqmProxyTcpIp::setDriveDown(const std::string &server,
  const std::string &unitName, const std::string &dgn) {
  try {
    // Check parameters
    if(server.empty()) {
      castor::exception::Exception ex;
      ex.getMessage() << "Invalid parameter: server is an empty string";
      throw ex;
    }
    if(unitName.empty()) {
      castor::exception::Exception ex;
      ex.getMessage() << "Invalid parameter: unitName is an empty string";
      throw ex;
    }
    if(dgn.empty()) {
      castor::exception::Exception ex;      
      ex.getMessage() << "Invalid parameter: dgn is an empty string";
      throw ex;
    }

    legacymsg::VdqmDrvRqstMsgBody rqst;
    rqst.status = VDQM_UNIT_DOWN;
    castor::utils::copyString(rqst.server, server);
    castor::utils::copyString(rqst.drive, unitName);
    castor::utils::copyString(rqst.dgn, dgn);

    sendRqstRecvReply(rqst);
  } catch(castor::exception::Exception &ne) {
    castor::exception::Exception ex;
    ex.getMessage() << "Failed to set state of tape drive"
      ": server=" << server << " unitName=" << unitName << " dgn=" << dgn <<
      " state=down: " << ne.getMessage().str();
    throw ex;
  }
}

//------------------------------------------------------------------------------
// setDriveUp
//------------------------------------------------------------------------------
void castor::legacymsg::VdqmProxyTcpIp::setDriveUp(const std::string &server,
  const std::string &unitName, const std::string &dgn) {
  try {
    // Check parameters
    if(server.empty()) {
      castor::exception::Exception ex;
      ex.getMessage() << "Invalid parameter: server is an empty string";
      throw ex;
    }
    if(unitName.empty()) {
      castor::exception::Exception ex;
      ex.getMessage() << "Invalid parameter: unitName is an empty string";
      throw ex;
    }
    if(dgn.empty()) {
      castor::exception::Exception ex;
      ex.getMessage() << "Invalid parameter: dgn is an empty string";
      throw ex;
    }

    legacymsg::VdqmDrvRqstMsgBody rqst;
    rqst.status = VDQM_UNIT_UP;
    castor::utils::copyString(rqst.server, server);
    castor::utils::copyString(rqst.drive, unitName);
    castor::utils::copyString(rqst.dgn, dgn);

    sendRqstRecvReply(rqst);
  } catch(castor::exception::Exception &ne) {
    castor::exception::Exception ex;
    ex.getMessage() << "Failed to set state of tape drive"
      ": server=" << server << " unitName=" << unitName << " dgn=" << dgn <<
      " state=up: " << ne.getMessage().str();
    throw ex;
  }
}

//------------------------------------------------------------------------------
// assignDrive
//------------------------------------------------------------------------------
void castor::legacymsg::VdqmProxyTcpIp::assignDrive(const std::string &server,
  const std::string &unitName, const std::string &dgn,
  const uint32_t mountTransactionId, const pid_t sessionPid) {
  try {
    // Check parameters
    if(server.empty()) {
      castor::exception::Exception ex;
      ex.getMessage() << "Invalid parameter: server is an empty string";
      throw ex;
    }
    if(unitName.empty()) {
      castor::exception::Exception ex;
      ex.getMessage() << "Invalid parameter: unitName is an empty string";
      throw ex;
    }
    if(dgn.empty()) {
      castor::exception::Exception ex;
      ex.getMessage() << "Invalid parameter: dgn is an empty string";
      throw ex;
    }

    legacymsg::VdqmDrvRqstMsgBody rqst;
    rqst.status = VDQM_UNIT_ASSIGN;
    rqst.volReqId = mountTransactionId;
    rqst.jobId = sessionPid;
    castor::utils::copyString(rqst.server, server);
    castor::utils::copyString(rqst.drive, unitName);
    castor::utils::copyString(rqst.dgn, dgn);

    sendRqstRecvReply(rqst);
  } catch(castor::exception::Exception &ne) {
    castor::exception::Exception ex;
    ex.getMessage() << "Failed to assign drive"
      ": server=" << server << " unitName=" << unitName << " dgn=" << dgn <<
      " mountTransactionId=" << mountTransactionId << " sessionPid=" <<
      sessionPid << ": " << ne.getMessage().str();
    throw ex;
  }
}

//------------------------------------------------------------------------------
// tapeMounted
//------------------------------------------------------------------------------
void castor::legacymsg::VdqmProxyTcpIp::tapeMounted(const std::string &server,
  const std::string &unitName, const std::string &dgn, const std::string &vid,
  const pid_t sessionPid) {
  try {
    // Check parameters
    if(server.empty()) {
      castor::exception::Exception ex;
      ex.getMessage() << "Invalid parameter: server is an empty string";
      throw ex;
    }
    if(unitName.empty()) {
      castor::exception::Exception ex;
      ex.getMessage() << "Invalid parameter: unitName is an empty string";
      throw ex;
    }
    if(dgn.empty()) {
      castor::exception::Exception ex;
      ex.getMessage() << "Invalid parameter: dgn is an empty string";
      throw ex;
    }
    if(vid.empty()) {
      castor::exception::Exception ex;
      ex.getMessage() << "Invalid parameter: vid is an empty string"; 
      throw ex;
    } 

    legacymsg::VdqmDrvRqstMsgBody rqst;
    rqst.status = VDQM_VOL_MOUNT;
    rqst.jobId = sessionPid;
    castor::utils::copyString(rqst.volId, vid);
    castor::utils::copyString(rqst.server, server);
    castor::utils::copyString(rqst.drive, unitName);
    castor::utils::copyString(rqst.dgn, dgn);

    sendRqstRecvReply(rqst);
  } catch(castor::exception::Exception &ne) {
    castor::exception::Exception ex;
    ex.getMessage() << "Failed to notify vdqm that tape is mounted"
      ": server=" << server << " unitName=" << unitName << " dgn=" << dgn <<
      " vid=" << vid << " sessionPid=" << sessionPid << ": " <<
      ne.getMessage().str();
    throw ex;
  }
}

//------------------------------------------------------------------------------
// releaseDrive
//------------------------------------------------------------------------------
void castor::legacymsg::VdqmProxyTcpIp::releaseDrive(const std::string &server,
  const std::string &unitName, const std::string &dgn, const bool forceUnmount,
  const pid_t sessionPid) {

  const int status = forceUnmount ? VDQM_UNIT_RELEASE | VDQM_FORCE_UNMOUNT :
    VDQM_UNIT_RELEASE;

  try {
    // Check parameters
    if(server.empty()) {
      castor::exception::Exception ex;
      ex.getMessage() << "Invalid parameter: server is an empty string";
      throw ex;
    }
    if(unitName.empty()) {
      castor::exception::Exception ex;
      ex.getMessage() << "Invalid parameter: unitName is an empty string";
      throw ex;
    }
    if(dgn.empty()) {
      castor::exception::Exception ex;
      ex.getMessage() << "Invalid parameter: dgn is an empty string"; 
      throw ex;
    } 

    legacymsg::VdqmDrvRqstMsgBody rqst;
    rqst.status = status;
    rqst.jobId = sessionPid;
    castor::utils::copyString(rqst.server, server);
    castor::utils::copyString(rqst.drive, unitName);
    castor::utils::copyString(rqst.dgn, dgn);

    sendRqstRecvReply(rqst);
  } catch(castor::exception::Exception &ne) {
    castor::exception::Exception ex;
    ex.getMessage() << "Failed to release tape drive"
      ": server=" << server << " unitName=" << unitName << " dgn=" << dgn <<
      " forceUnmount=" << forceUnmount << " sessionPid=" << sessionPid << ": "
      << ne.getMessage().str();
    throw ex;
  }
}

//------------------------------------------------------------------------------
// sendRqstRecvReply
//------------------------------------------------------------------------------
castor::legacymsg::VdqmDrvRqstMsgBody castor::legacymsg::VdqmProxyTcpIp::
  sendRqstRecvReply(const legacymsg::VdqmDrvRqstMsgBody &rqst) {
  castor::utils::SmartFd fd(connectToVdqm());
  writeDriveStatusMsg(fd.get(), rqst);
  readCommitAck(fd.get());
  const legacymsg::MessageHeader header = readDriveStatusMsgHeader(fd.get());
  const legacymsg::VdqmDrvRqstMsgBody reply = readDriveStatusMsgBody(fd.get(),
    header.lenOrStatus);
  writeCommitAck(fd.get());
  return reply;
}

//------------------------------------------------------------------------------
// connectToVdqm
//------------------------------------------------------------------------------
int castor::legacymsg::VdqmProxyTcpIp::connectToVdqm() const  {
  castor::utils::SmartFd smartConnectSock;
  try {
    smartConnectSock.reset(io::connectWithTimeout(m_vdqmHostName, m_vdqmPort,
      m_netTimeout));
  } catch(castor::exception::Exception &ne) {
    castor::exception::Exception ex;
    ex.getMessage() << "Failed to connect to vdqm on host " << m_vdqmHostName
      << " port " << m_vdqmPort << ": " << ne.getMessage().str();
    throw ex;
  }

  return smartConnectSock.release();
}

//------------------------------------------------------------------------------
// writeDriveStatusMsg
//------------------------------------------------------------------------------
void castor::legacymsg::VdqmProxyTcpIp::writeDriveStatusMsg(const int fd,
  const legacymsg::VdqmDrvRqstMsgBody &body) {
  char buf[VDQM_MSGBUFSIZ];
  const size_t len = legacymsg::marshal(buf, body);

  try {
    io::writeBytes(fd, m_netTimeout, len, buf);
  } catch(castor::exception::Exception &ne) {
    castor::exception::Exception ex;
    ex.getMessage() << "Failed to write drive status message: "
      << ne.getMessage().str();
    throw ex;
  }
}

//------------------------------------------------------------------------------
// readCommitAck
//------------------------------------------------------------------------------
void castor::legacymsg::VdqmProxyTcpIp::readCommitAck(const int fd) {
  legacymsg::MessageHeader ack;

  try {
    ack = readAck(fd);
  } catch(castor::exception::Exception &ne) {
    castor::exception::Exception ex;
    ex.getMessage() << "Failed to read VDQM_COMMIT ack: " <<
      ne.getMessage().str();
    throw ex;
  }

  if(VDQM_MAGIC != ack.magic) {
    castor::exception::Exception ex;
    ex.getMessage() << "Failed to read VDQM_COMMIT ack: Invalid magic"
      ": expected=0x" << std::hex << VDQM_MAGIC << " actual=" << ack.magic;
    throw ex;
  }

  if(VDQM_COMMIT == ack.reqType) {
    // Read a successful VDQM_COMMIT ack
    return;
  } else if(0 < ack.reqType) {
    // VDQM_COMMIT ack is reporting an error
    const std::string errStr = utils::serrnoToString(ack.reqType);
    castor::exception::Exception ex;
    ex.getMessage() << "VDQM_COMMIT ack reported an error: " << errStr;
    throw ex;
  } else {
    // VDQM_COMMIT ack contains an invalid request type
    castor::exception::Exception ex;
    ex.getMessage() << "VDQM_COMMIT ack contains an invalid request type"
      ": reqType=" << ack.reqType;
    throw ex;
  }
}

//------------------------------------------------------------------------------
// readAck
//------------------------------------------------------------------------------
castor::legacymsg::MessageHeader castor::legacymsg::VdqmProxyTcpIp::readAck(
  const int fd) {
  char buf[12]; // Magic + type + len
  legacymsg::MessageHeader ack;

  try {
    io::readBytes(fd, m_netTimeout, sizeof(buf), buf);
  } catch(castor::exception::Exception &ne) {
    castor::exception::Exception ex;
    ex.getMessage() << "Failed to read ack: "
      << ne.getMessage().str();
    throw ex;
  }

  const char *bufPtr = buf;
  size_t bufLen = sizeof(buf);
  legacymsg::unmarshal(bufPtr, bufLen, ack);

  return ack;
}

//------------------------------------------------------------------------------
// readDriveStatusMsgHeader
//------------------------------------------------------------------------------
castor::legacymsg::MessageHeader castor::legacymsg::VdqmProxyTcpIp::
  readDriveStatusMsgHeader(const int fd) {
  char buf[12]; // Magic + type + len
  legacymsg::MessageHeader header;

  try {
    io::readBytes(fd, m_netTimeout, sizeof(buf), buf);
  } catch(castor::exception::Exception &ne) {
    castor::exception::Exception ex;
    ex.getMessage() << "Failed to read header of drive status message: "
      << ne.getMessage().str();
    throw ex;
  }

  const char *bufPtr = buf;
  size_t bufLen = sizeof(buf);
  legacymsg::unmarshal(bufPtr, bufLen, header);

  if(VDQM_MAGIC != header.magic) {
    castor::exception::Exception ex;
    ex.getMessage() << "Failed to read header of drive status message"
      ": Invalid magic: expected=0x" << std::hex << VDQM_MAGIC << " actual=0x"
      << header.magic;
    throw ex;
  }
  if(VDQM_DRV_REQ != header.reqType) {
    castor::exception::Exception ex;
    ex.getMessage() << "Failed to read header of drive status message"
      ": Invalid request type: expected=0x" << std::hex << VDQM_DRV_REQ <<
      " actual=0x" << header.reqType;
    throw ex;
  }

  // The length of the message body is checked later, just before it is read in
  // to memory

  return header;
}

//------------------------------------------------------------------------------
// readDriveStatusMsgBody
//------------------------------------------------------------------------------
castor::legacymsg::VdqmDrvRqstMsgBody castor::legacymsg::VdqmProxyTcpIp::
  readDriveStatusMsgBody(const int fd, const uint32_t bodyLen) {
  char buf[VDQM_MSGBUFSIZ];

  if(sizeof(buf) < bodyLen) {
    castor::exception::Exception ex;
    ex.getMessage() << "Failed to read body of drive status message"
      ": Maximum body length exceeded: max=" << sizeof(buf) <<
      " actual=" << bodyLen;
    throw ex;
  }

  try {
    io::readBytes(fd, m_netTimeout, bodyLen, buf);
  } catch(castor::exception::Exception &ne) {
    castor::exception::Exception ex;
    ex.getMessage() << "Failed to read body of drive status message"
      ": " << ne.getMessage().str();
    throw ex;
  }

  legacymsg::VdqmDrvRqstMsgBody body;
  const char *bufPtr = buf;
  size_t bufLen = sizeof(buf);
  legacymsg::unmarshal(bufPtr, bufLen, body);
  return body;
}

//------------------------------------------------------------------------------
// writeCommitAck
//------------------------------------------------------------------------------
void castor::legacymsg::VdqmProxyTcpIp::writeCommitAck(const int fd) {
  legacymsg::MessageHeader ack;
  ack.magic = VDQM_MAGIC;
  ack.reqType = VDQM_COMMIT;
  ack.lenOrStatus = 0;

  char buf[12]; // magic + reqType + len
  legacymsg::marshal(buf, ack);

  try {
    io::writeBytes(fd, m_netTimeout, sizeof(buf), buf);
  } catch(castor::exception::Exception &ne) {
    castor::exception::Exception ex;
    ex.getMessage() << "Failed to write VDQM_COMMIT ack: " <<
      ne.getMessage().str();
    throw ex;
  }
}

//------------------------------------------------------------------------------
// tapeUnmounted
//------------------------------------------------------------------------------
void castor::legacymsg::VdqmProxyTcpIp::tapeUnmounted(
  const std::string &server, const std::string &unitName,
  const std::string &dgn, const std::string &vid) {
  try {
    // Check parameters
    if(server.empty()) {
      castor::exception::Exception ex;
      ex.getMessage() << "Invalid parameter: server is an empty string";
      throw ex;
    }
    if(unitName.empty()) {
      castor::exception::Exception ex;
      ex.getMessage() << "Invalid parameter: unitName is an empty string";
      throw ex;
    }
    if(dgn.empty()) {
      castor::exception::Exception ex;
      ex.getMessage() << "Invalid parameter: dgn is an empty string"; 
      throw ex;
    } 
    if(vid.empty()) {
      castor::exception::Exception ex;
      ex.getMessage() << "Invalid parameter: vid is an empty string";
      throw ex;
    }

    legacymsg::VdqmDrvRqstMsgBody rqst;
    rqst.status = VDQM_VOL_UNMOUNT;
    castor::utils::copyString(rqst.volId, vid);
    castor::utils::copyString(rqst.server, server);
    castor::utils::copyString(rqst.drive, unitName);
    castor::utils::copyString(rqst.dgn, dgn);

    sendRqstRecvReply(rqst);
  } catch(castor::exception::Exception &ne) {
    castor::exception::Exception ex;
    ex.getMessage() << "Failed to notify vdqm that tape " << vid <<
      " was unmounted from tape drive " << unitName << ": " <<
      ne.getMessage().str();
    throw ex;
  }
}

//------------------------------------------------------------------------------
// getDriveStatus
//------------------------------------------------------------------------------
int castor::legacymsg::VdqmProxyTcpIp::getDriveStatus(const std::string &server,
  const std::string &unitName, const std::string &dgn) {
  try {
    // Check parameters
    if(unitName.empty()) {
      castor::exception::Exception ex;
      ex.getMessage() << "Invalid parameter: unitName is an empty string";
      throw ex;
    }
    if(dgn.empty()) {
      castor::exception::Exception ex;
      ex.getMessage() << "Invalid parameter: dgn is an empty string";
      throw ex;
    }

    legacymsg::VdqmDrvRqstMsgBody rqst;
    rqst.status = VDQM_UNIT_QUERY;
    castor::utils::copyString(rqst.server, server);
    castor::utils::copyString(rqst.drive, unitName);
    castor::utils::copyString(rqst.dgn, dgn);

    const legacymsg::VdqmDrvRqstMsgBody reply = sendRqstRecvReply(rqst);

    return reply.status;
  } catch(castor::exception::Exception &ne) {
    castor::exception::Exception ex;
    ex.getMessage() << "Failed to query vdqm for status of drive " << unitName
      << " in dgn " << dgn << ": " << ne.getMessage().str();
    throw ex;
  }
}
