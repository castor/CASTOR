.\" Copyright (C) 2002 by CERN/IT/DS/HSM
.\" All rights reserved
.\" 
.TH "Cupvdelete" "1castor" "$Date: 2007/08/08 16:14:38 $" "CASTOR" "UPV Administrator commands"
.SH "NAME"
Cupvdelete \- Deletes an entry from the user privilege validation system
.SH "SYNOPSIS"
.B Cupvdelete
(
.BI \-\-uid " uid " | \-\-user " username"
) (
.BI \-\-gid " gid " | \-\-group " groupname"
)
.BI \-\-src " source_host"
.BI \-\-tgt " target_host"
.SH "DESCRIPTION"
.B Cupvdelete
Deletes an entry from the user privilege validation system.
.TP 
.I uid/user
is the user id/user name of the user for whom the authorization is requested.
.TP 
.I gid/group
is the group id/group name of the user for whom authorization is requested.
.TP 
.I source_host
the regular expression specifying the source host.
.TP 
.I target_host
the regular expression specifying target host.

.SH "EXIT STATUS"
This program returns 0 if the operation was successful, or 1 if the operation
failed or if the authorization is rejected. 

.SH "SEE ALSO"
.BR Cupvadd(1),
.B Cupvcheck(1) ,
.B Cupvlist(1) ,
.B Cupvmodify(1)
.B Cupv_add(3) ,
.B Cupv_delete(3) ,
.B Cupv_list(3) ,
.B Cupv_modify(3)
.SH "AUTHOR"
\fBCASTOR\fP Team <castor.support@cern.ch>
