.\" Copyright (C) 2002 by CERN/IT/DS/HSM
.\" All rights reserved
.\" 
.TH "Cupv_check" "3castor" "$Date: 2009/07/23 12:22:06 $" "CASTOR" "UPV Library Functions"
.SH "NAME"
Cupv_check \- Checks that a given user has the right to perform an operation, according to the user privilege validation system
.SH "SYNOPSIS"
\fB#include "Cupv_api.h"\fR
.sp
.BI "int Cupv_check (uid_t " uid , 
.BI "gid_t "  gid , 
.BI "char *" source_host , 
.BI "char *" target_host , 
.BI "int "  privilege ) 
.SH "DESCRIPTION"
.B Cupv_check
checks the rights of a user in the CASTOR User Privilege Validation system. The permissions are always specified in this way: a given privilege on a target machine (see below) is given  to a given uid/gid from a source machine.
.TP
If several privileges are specified at the same time, in the "privilege" field, it checks that the the user has both rights (logical AND).
.TP 
.I uid
is the user id of the user for whom the authorization is requested.
.TP 
.I gid
is the group id of the user for whom authorization is requested.
.TP 
.I source_host
the source host or NULL. In this case, the cupvd fills in this field with the result of "getpeername" on the connection.
.TP 
.I target_host
the target host or NULL. In this case, the cupvd fills in this field with the result of "getpeername" on the connection.
.TP 
.I privilege
is the privilege requested. It must be one of the following:
.IP 
.BR P_OPERATOR, 
.BR P_TAPE_OPERATOR, 
.BR P_GRP_ADMIN, 
.BR P_ADMIN, 
.BR P_UPV_ADMIN, 
.BR P_TAPE_SYSTEM 
.SH "RETURN VALUE"
This routine returns 0 if the authorization is granted, or \-1 if the operation
failed or if the authorization is rejected. In the latter case,
.B serrno
is set appropriately.
.SH "ERRORS"
.TP 1.3i
.B EACCES
Permission denied.
.TP 
.B EINVAL
a bad uid/gid or privilege has been passed as argument.
.TP 
.B SENOSHOST
Host unknown.
.TP 
.B SENOSSERV
Service unknown.
.TP 
.B SECOMERR
Communication error.
.TP 
.B SEINTERNAL
Internal CUPV error.
.TP 
.B SECUPVNACT
Server is not running or is being shutdown.
.SH "SEE ALSO"
.B Castor_limits(4), Cupv_add(3), Cupv_delete(3), Cupv_list(3), Cupv_modify(3)
.SH "AUTHOR"
\fBCASTOR\fP Team <castor.support@cern.ch>
