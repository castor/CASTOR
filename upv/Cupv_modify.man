.\" Copyright (C) 2002 by CERN/IT/DS/HSM
.\" All rights reserved
.\" 
.TH "Cupv_modify" "3castor" "$Date: 2002/10/16 08:04:34 $" "CASTOR" "UPV Library Functions"
.SH "NAME"
Cupv_modify \- Modifies an entry in the user privilege validation system.
.SH "SYNOPSIS"
\fB#include "Cupv_api.h"\fR
.sp
.BI "int Cupv_modify (uid_t " uid , 
.BI "gid_t "  gid , 
.BI "char *" source_host , 
.BI "char *" target_host,  ) 
.BI "char *" new_source_host , 
.BI "char *" new_target_host,
.BI "int " privilege ) 
.SH "DESCRIPTION"
.B Cupv_modify
Modifies the rights of a user from the CASTOR User Privilege Validation system. The permissions are always specified in this way: a given privilege level on a target machine (see below) is given  to a given uid/gid from a source machine.
.TP 
.I uid
is the user id of the user for whom the authorization is requested.
.TP 
.I gid
is the group id of the user for whom authorization is requested.
.TP 
.I source_host
is a regular expression specifying the source host(s).
.TP 
.I target_host
is a regular expression specifying the target host(s).
.TP 
.I new_source_host
is a regular expression specifying the new source host(s).
.TP 
.I new_target_host
is a regular expression specifying the new target host(s).
.TP 
.I privilege
is the privilege requested. It must be one of the following:
.IP 
.BR P_OPERATOR, 
.BR P_TAPE_OPERATOR, 
.BR P_GRP_ADMIN, 
.BR P_ADMIN, 
.BR P_UPV_ADMIN, 
.BR P_TAPE_SYSTEM 
.SH "RETURN VALUE"
This routine returns 0 if the call was successful, \-1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH "ERRORS"
.TP 1.3i
.B EACCES
Permission denied.
.TP 
.B EINVAL
a bad uid/gid or privilege has been passed as argument.
.TP 
.B SENOSHOST
Host unknown.
.TP 
.B SENOSSERV
Service unknown.
.TP 
.B SECOMERR
Communication error.
.TP 
.B SEINTERNAL
Internal CUPV error.
.TP 
.B SECUPVNACT
Server is not running or is being shutdown.
.SH "SEE ALSO"
.BR Castor_limits(4), Cupv_add(3), Cupv_delete(3),
.B Cupv_list(3)
.SH "AUTHOR"
\fBCASTOR\fP Team <castor.support@cern.ch>
