.\" Copyright (C) 2002 by CERN/IT/DS/HSM
.\" All rights reserved
.\" 
.TH "Cupvlist" "1castor" "$Date: 2002/10/16 08:05:23 $" "CASTOR" "UPV Administrator commands"
.SH "NAME"
Cupvlist \- Lists the rights in to the user privilege validation system
.SH "SYNOPSIS"
.B Cupvlist
[
.BI \-\-uid " uid " | \-\-user " username"
] [
.BI \-\-gid " gid " | \-\-group " groupname"
] [
.BI \-\-src " source_host"
] [
.BI \-\-tgt " target_host"
] [
.BI \-\-priv " privilege"
]
.SH "DESCRIPTION"
.B Cupvlist
Lists the rights of a user in the CASTOR User Privilege Validation system, allowing to put some filters on the entries that will be displayed.
.TP 
.I uid/user
is the user id/user name of the user for whom the authorization is requested.
.TP 
.I gid/group
is the group id/group name of the user for whom authorization is requested.
.TP 
.I source_host
the regular expression specifying the source host.
.TP 
.I target_host
the regular expression specifying target host.
.TP 
.I privilege
is the privilege requested. It must be one of the following:
.IP 
.BR OPERATOR, 
.BR TP_OPER, 
.BR GRP_ADMIN, 
.BR ADMIN, 
.BR UPV_ADMIN, 
.BR TAPE_SYSTEM 
or a combination separated with "|".
.SH "EXIT STATUS"
This program returns 0 if the operation was successful, or 1 if the operation
failed or if the authorization is rejected. 
.SH "SEE ALSO"
.BR Cupvadd(1),
.B Cupvcheck(1) ,
.B Cupvdelete(1) ,
.B Cupvmodify(1)
.B Cupv_add(3) ,
.B Cupv_delete(3) ,
.B Cupv_list(3) ,
.B Cupv_modify(3)
.SH "AUTHOR"
\fBCASTOR\fP Team <castor.support@cern.ch>
