.\" Copyright (C) 2000-2003 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH VMGRENTERDENMAP "1castor" "$Date: 2003/10/29 07:48:58 $" CASTOR "vmgr Administrator Commands"
.SH NAME
vmgrenterdenmap \- enter a new quadruplet model/media_letter/density/capacity
in the CASTOR Volume Manager
.SH SYNOPSIS
.B vmgrenterdenmap
.BI -d " density"
[
.BI --ml " media_letter"
]
.BI --mo " model"
.BI --nc " native_capacity"
.SH DESCRIPTION
.B vmgrenterdenmap
enters a new quadruplet model/media_letter/density/capacity in the CASTOR Volume
Manager.
.TP
.I density
is alphanumeric. It must be at most CA_MAXDENLEN characters long.
For example:
.br
.B 10G
or
.B 10GC
for IBM 3590 cartridges,
.BR 10G ,
.BR 10GC ,
.BR 25G ,
.BR 25GC ,
.B 50G
or
.B 50GC
for Redwood cartridges,
.B 20G
or
.B 20GC
for STK 9840 cassettes.
.TP
.I media_letter
is the media identification letter. For example "A", "B" or "C" for SD3 (Redwood
),
"J" for IBM 3590, "R" for STK 9840.
.TP
.I model
is the model of cartridge.
It must be at most CA_MAXMODELLEN characters long.
.TP
.I native_capacity
in bytes. The number may also have a suffix K/M/G/T/P/H to indicate kilo/mega/giga/tera/peta/hexabytes or Ki/Mi/Gi/Ti/Pi/Hi for their powers of 2 version.
For a multi-sided media like DVD, the native capacity corresponds to one side.
.LP
This command requires ADMIN privilege in the Cupv database.
.SH EXAMPLES
.nf
.ft CW
vmgrenterdenmap --mo 9840 --ml R -d 20GC --nc 20Gi
vmgrenterdenmap --mo 8500 -d 8500C --nc 5Gi
.ft
.fi
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR Cupvlist(1) ,
.B vmgr_enterdenmap(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
