.\" Copyright (C) 2001-2002 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH VMGRMODIFYLIBRARY "1castor" "$Date: 2008/11/06 16:45:44 $" CASTOR "vmgr Administrator Commands"
.SH NAME
vmgrmodifylibrary \- modify the definition of an existing tape library in the CASTOR Volume Manager
.SH SYNOPSIS
.B vmgrmodifylibrary
.BI --name " library_name"
[
.BI --capacity " nb_slots"
] [
.BI --status " status"
]
.SH DESCRIPTION
.B vmgrmodifylibrary
modifies the definition of an existing tape library in the CASTOR Volume Manager.
.TP
.I library_name
is the name of the tape library.
It must be at most CA_MAXTAPELIBLEN characters long.
.TP
.I capacity
is the new total number of slots.
.TP
.I status
is the status of the library, either ONLINE or OFFLINE.
.LP
Only
.I library_name
is mandatory. When an option is not specified, the field is not modified.
.LP
This command requires ADMIN privilege in the Cupv database.
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR Cupvlist(1) ,
.B vmgr_modifylibrary(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
