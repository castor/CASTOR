.\" Copyright (C) 2000-2002 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH VMGR_LISTTAPE_BYTE_U64 "3castor" "$Date: 2002/02/07 06:46:24 $" CASTOR "vmgr Library Functions"
.SH NAME
vmgr_listtape_byte_u64 \- list all existing tape entries in the CASTOR Volume Manager
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "vmgr_api.h"\fR
.sp
.BI "struct vmgr_tape_info_byte_u64 *vmgr_listtape_byte_u64 (char *" vid ,
.BI "char *" pool_name ,
.BI "int " flags ,
.BI "vmgr_list *" listp )
.SH DESCRIPTION
.B vmgr_listtape_byte_u64
lists all existing tape entries in the CASTOR Volume Manager.
.TP
.I vid
if not NULL, restricts the list to the media having this volume visual identifier.
.TP
.I pool_name
if not NULL, restricts the list to the tapes belonging to this pool.
.TP
.I flags
may be one of the following constants:
.RS
.TP
.B VMGR_LIST_BEGIN
the first call for a volume must have this flag set to allocate buffers and
initialize pointers.
.TP
.B VMGR_LIST_CONTINUE
all the following calls must have this flag set.
.TP
.B VMGR_LIST_END
final call to terminate the list and free resources.
.RE
.SH RETURN VALUE
This routine returns a pointer to a structure containing the current tape entry
if the operation was successful or NULL if all entries have been returned
or if the operation failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOMEM
memory could not be allocated for the output buffer.
.TP
.B EFAULT
.I listp
is a NULL pointer.
.TP
.B EINVAL
The length of
.I pool_name
exceeds
.BR CA_MAXPOOLNAMELEN .
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.TP
.B EVMGRNACT
Volume manager is not running or is being shutdown.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR vmgr_entertape(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
