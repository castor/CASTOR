.\" Copyright (C) 1999-2003 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH VMGR_QUERYTAPE "3castor" "$Date: 2003/11/17 06:46:30 $" CASTOR "vmgr Library Functions"
.SH NAME
vmgr_querytape \- query the CASTOR Volume Manager about a tape
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "vmgr_api.h"\fR
.sp
.BI "int vmgr_querytape (const char *" vid ,
.BI "int " side ,
.BI "struct vmgr_tape_info *" tape_info ,
.BI "char *" dgn )
.SH DESCRIPTION
.B vmgr_querytape
queries the CASTOR Volume Manager about a tape.
.P
Please note that users of the vmgr client API are encouraged to use the
.B vmgr_querytape_byte_u64
function instead of
.B vmgr_querytape.
The former supports tape sizes up to 16 Exibytes whereas the latter only
supports tape sizes up to 2 Teribytes.
If the
.B vmgr_querytape
function is passed the VID of a tape with more than 2 Teribytes of free space,
then the function will store a value of exactly 2 Teribytes into the
.I estimated_free_space
member of the tape_info output parameter.
The
.B vmgr_querytape_byte_u64
function defines free space to be in bytes and represented by a 64-bit unsigned
integer.
The
.B vmgr_querytape
defines free space to be in kibibytes and represented by a
signed 32-bit integer.
The
.B vmgr_querytape_byte_u64
function has been introduced to help represent all CASTOR sizes in the same
way.
.TP
.I vid
is the volume visual identifier.
It must be at most CA_MAXVIDLEN characters long.
.TP
.I dgn
points at a buffer to receive the device group name corresponding to
the tape.
The buffer must be at least CA_MAXDGNLEN+1 characters long.
.TP
.I etime
indicates the time the volume was entered in the Volume Manager.
.TP
.I free_space
in kbytes.
.TP
.I nbfiles
number of files written on the tape.
.TP
.I rcount
number of times the volume has been mounted for read.
.TP
.I wcount
number of times the volume has been mounted for write.
.TP
.I rhost
indicates the last tape server where the tape was mounted for read.
.TP
.I whost
indicates the last tape server where the tape was mounted for write.
.TP
.I rjid
is the job id of the last read request.
.TP
.I wjid
is the job id of the last write request.
.TP
.I rtime
indicates the last time the volume was mounted for read.
.TP
.I wtime
indicates the last time the volume was mounted for write.
.TP
.I status
will be returned as a combination of
.BR ARCHIVED ,
.BR DISABLED ,
.BR EXPORTED ,
.BR TAPE_BUSY ,
.BR TAPE_FULL
and
.BR TAPE_RDONLY .
.LP
Only
.I vid
or
.I tape_info
are mandatory. When a pointer to a field is NULL, the field value is not returned.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOENT
The named volume does not exist.
.TP
.B EFAULT
.I vid
or
.I tape_info
is a NULL pointer.
.TP
.B EINVAL
The length of
.I vid
exceeds
.BR CA_MAXVIDLEN .
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.TP
.B EVMGRNACT
Volume manager is not running or is being shutdown.
.SH SEE ALSO
.BR Castor_limits(4) ,
.B vmgr_entertape(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
