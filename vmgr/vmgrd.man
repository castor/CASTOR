.\" Copyright (C) 1999-2001 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH VMGRD "8castor" "$Date: 2009/08/18 09:43:02 $" CASTOR "vmgr Administrator Commands"
.SH NAME
vmgrd \- start the volume manager
.SH SYNOPSIS
.B vmgrd
[
.BI -f
] [
.BI -c " config_file"
] [
.BI -l " log_file"
] [
.BI -t " nbthreads"
]
.SH DESCRIPTION
.LP
The
.B vmgrd
command starts the volume manager.
This command is usually executed at system startup time
.RB ( /etc/rc.local ).
This will read the volume manager configuration file,
create a pool of threads and look for requests.
Each of them is processed in a thread which opens a connection to the
database server if necessary.
When a request has been completed, the thread becomes idle until it is allocated
to another request.
The connection to the database server is kept open between 2 requests.
If the volume manager is being shutdown or not active, the requests are
automatically retried by the client API.
.LP
All error messages and statistical information are kept in a log.
.LP
The volume manager listen port number can be defined on client hosts
(stagers) and on the machine where the volume manager runs in either of the
following ways:
.RS
.LP
setting an environment variable VMGR_PORT
.RS
.HP
setenv VMGR_PORT 5013
.RE
.LP
an entry in
.B /etc/castor/castor.conf
like:
.RS
.HP
VMGR	PORT	5013
.RE
.LP
an entry in
.B /etc/services
like:
.RS
.HP
vmgr           5013/tcp                        # CASTOR volume manager
.RE
.RE
.LP
If none of these methods is used, the default port number is taken from the
definition of VMGR_PORT in vmgr_constants.h.
.LP
The volume manager host name can be defined on client hosts
in either of the following ways:
.RS
.LP
setting an environment variable VMGR_HOST, for example:
.RS
.HP
setenv VMGR_HOST castor5
.RE
.LP
an entry in
.B /etc/castor/castor.conf
for example:
.RS
.HP
VMGR	HOST	castor5
.RE
.RE
.LP
If none of these methods is used, the default host is localhost.
.LP
The volume manager database keeps the information about tapes like vid, vsn,
device group, density and status.
When the Volume Manager is started for the first time, the database must be
initialized. This can be done by executing the script
.B vmgr_db_init.sh
which defines the tape pool "default", the models of cartridges and the
densities supported by CASTOR.
.LP
The volume manager configuration file contains password information for the
database and must be readable/writable only by root.
It contains a single line in the format:
.RS
.HP
username/password@server
.RE
.sp
where 'username' and 'password' are the credentials to login to the database
instance identified by 'server'.
.LP
In the log each entry has a timestamp.
All entries corresponding to one request have the same request id.
For each user command there is one message VMG92 giving information about
the requestor (hostname, uid, gid) and one message VMG98 giving the command
itself.
The completion code of the command is also logged.
.SH TAPE STATES
A tape can be in one or more of the following states within the database of the vmgrd daemon:
.TP
.BI BUSY 
A
.B BUSY
tape is a tape that has been reserved by a stager for writing or is currently being written to by a tape server.  A tape is not put in the
.B BUSY
state for recalls.
A tape is automatically put in the
.B BUSY
state by the CASTOR system.
An operator should not manually put a tape in the
.B BUSY
state.
.TP
.BI FULL
A
.B FULL
tape is a tape from which data can be read from but no longer written to.
The CASTOR system will automatically put a tape in the
.B FULL
state when it detects the physical end of tape or if the total amount of data written to the tape exceeds the capacity specified in the VMGR.
An operator may manually put a tape in the
.B FULL
state in order to stop the CASTOR system from writing to that tape and if they want to repack that tape.
Repack will only repack a tape if is in the
.B FULL
state.
.TP
.BI DISABLED
A
.B DISABLED
tape is a tape that is temporarily not accessible by the CASTOR system.
A tape is automatically put in the
.B DISABLED
state by the CASTOR system in the event of some types of media and drive errors.
An operator may manually put a tape in the
.B DIABLED
state in order to stop the CASTOR system from accessing that tape.
A copy of a file on a
.B DISABLED
tape is counted as a valid copy in the total number of copies on tape.
Under no circumstances will CASTOR try to create a new copy of a file on tape because it found a copy on a
.B DISABLED
tape.
Repack will not repack a
.B DISABLED
tape because by definition such a tape is not accessible by the CASTOR system.
.TP
.BI RDONLY
A
.B RDONLY
tape is a tape from which data can be read from but not written to.
An operator may manually put a tape in the
.B RDONLY
state when they wish to block the CASTOR system from writing to that tape.
A
.B RDONLY
tape can be repacked if it is also in the
.B FULL
state.
.TP
.BI ARCHIVED
An
.B ARCHIVED
tape is a tape that is stored outside of the tape libraries connected to the CASTOR system.
An
.B ARCHIVED
tape is by definition not reachable by CASTOR.
An operator should manually put a tape in the
.B ARCHIVED
state before removing the tape from the libraries attached to the CASTOR system.
A copy of a file on an
.B ARCHIVED
tape is counted as a valid copy in the total number of copies on tape.
Under no circumstances will CASTOR try to create a new copy of a file on tape because it found a copy on an
.B ARCHIVED
tape.
Repack will not repack an
.B ARCHIVED 
tape because by definition such a tape is not accessible by the CASTOR system.
.TP
.BI EXPORTED
An
.B EXPORTED
tape is a tape under repair.
The tape is outside of the tape libraries connected to the CASTOR system and is most probably off site for repair.
An operator should manually put a tape in the
.B EXPORTED
state when they send it off site for repair.
A copy of a file on an
.B EXPORTED
tape is not counted as a valid copy in the total number of copies on tape.
If CASTOR detects the number of valid copies on tape is less than that specified by the file-class of the file, then it will try to create a new copy of a file on tape.
Repack will not repack an
.B EXPORTED 
tape because by definition such a tape is not accessible by the CASTOR system.
.SH OPTIONS
.TP
.BI -f
Remain in the foreground.
.TP
.BI -c " config_file"
Specifies a different path for the VMGR configuration file.
.TP
.BI -l " log_file"
Specifies a different path for the VMGR log file.
.TP
.BI -t " nbthreads"
Specifies the number of threads. Default is 6.
.SH FILES
.TP 1.5i
.B /etc/VMGRCONFIG
configuration file
.TP
.B /var/log/castor/vmgrd.log
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
