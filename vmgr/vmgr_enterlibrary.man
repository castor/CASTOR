.\" Copyright (C) 2001-2002 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH VMGR_ENTERLIBRARY "3castor" "$Date: 2002/08/23 12:43:03 $" CASTOR "vmgr Library Functions"
.SH NAME
vmgr_enterlibrary \- define a new tape library in the CASTOR Volume Manager
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "vmgr_api.h"\fR
.sp
.BI "int vmgr_enterlibrary (const char *" library_name ,
.BI "int " capacity ,
.BI "int " status )
.SH DESCRIPTION
.B vmgr_enterlibrary
defines a new tape library in the CASTOR Volume Manager.
.TP
.I library_name
is the name of the library.
It must be at most CA_MAXTAPELIBLEN characters long.
.TP
.I capacity
is the total number of slots.
.LP
This function requires ADMIN privilege in the Cupv database.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B EFAULT
.I library_name
is a NULL pointer.
.TP
.B EEXIST
The named library exists already.
.TP
.B EINVAL
The length of
.I library_name
exceeds
.B CA_MAXTAPELIBLEN
or capacity <= 0.
.TP
.B ENOSPC
The volume manager database is full.
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.TP
.B EVMGRNACT
Volume manager is not running or is being shutdown.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR Cupvlist(1) ,
.BR vmgr_deletelibrary(3) ,
.B vmgr_modifylibrary(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
