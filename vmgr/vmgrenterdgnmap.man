.\" Copyright (C) 2001-2002 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH VMGRENTERDGNMAP "1castor" "$Date: 2002/08/23 12:43:05 $" CASTOR "vmgr Administrator Commands"
.SH NAME
vmgrenterdgnmap \- enter a new triplet dgn/model/library in the CASTOR Volume Manager
.SH SYNOPSIS
.B vmgrenterdgnmap
.BI -g " dgn"
.BI --library " library_name"
.BI --mo " model"
.SH DESCRIPTION
.B vmgrenterdgnmap
enters a new triplet dgn/model/library in the CASTOR Volume Manager.
.TP
.I dgn
is the device group name associated with the pair model/library.
It must be at most CA_MAXDGNLEN characters long.
.TP
.I library_name
is the name of the library.
It must be at most CA_MAXTAPELIBLEN characters long.
.TP
.I model
is the model of cartridge.
It must be at most CA_MAXMODELLEN characters long.
.LP
This command requires ADMIN privilege in the Cupv database.
.SH EXAMPLES
.nf
.ft CW
vmgrenterdgnmap -g 9840R --library STK_ACS0 --mo 9840
vmgrenterdgnmap -g NTRE3 --library STK_ACS3 --mo 3590E
.ft
.fi
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR Cupvlist(1) ,
.B vmgr_enterdgnmap(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
