.\" Copyright (C) 1999-2002 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH VMGR_GETTAPE "3castor" "$Date: 2003/10/29 08:53:46 $" CASTOR "vmgr Library Functions"
.SH NAME
vmgr_gettape \- get a tape volume to store a given amount of data
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "vmgr_api.h"\fR
.sp
.BI "int vmgr_gettape (const char *" poolname ,
.BI "u_signed64 " Size ,
.BI "const char *" Condition ,
.BI "char *" vid ,
.BI "char *" vsn ,
.BI "char *" dgn ,
.BI "char *" density ,
.BI "char *" lbltype ,
.BI "char *" model ,
.BI "int *" side ,
.BI "int *" fseq ,
.BI "u_signed64 *" estimated_free_space )
.SH DESCRIPTION
.B vmgr_gettape
gets a tape volume from the specified tape-pool to store a given amount of
data.  The status of the tape is changed to TAPE_BUSY.
.PP
The
.B vmgrd
daemon tries to satisfy the
.B vmgr_gettape
function by looking for the fullest tape that can satisfy the request, or if
there is no such tape then the tape with the most amount of free space.  The
.B vmgrd
daemon purposely injects some randomness into its choice.
In the case of finding the fullest tape, the
.B vmgrd
daemon chooses at random from the five fullest tapes (or less if there are less
than five such tapes).
In the case of finding the tape with the most amount of free space, the
.B vmgrd
daemon chooses at random from the five emptiest tapes (or less if there are
less than five such tapes).

.TP
.I poolname
specifies the name of the pool from which the cartridge should be taken.
If the pointer is NULL, the volume is taken from the default pool.
.TP
.I Size
Amount of data to be stored (in bytes).
.TP
.I Condition
Optional SQL SELECT statement to be used to choose the cartridge.
.TP
.I vid
points at a buffer to receive the volume visual identifier of the tape.
The buffer must be at least CA_MAXVIDLEN+1 characters long.
.TP
.I vsn
points at a buffer to receive the magnetically recorded volume serial number of
the tape.
The buffer must be at least CA_MAXVSNLEN+1 characters long.
.TP
.I dgn
points at a buffer to receive the device group name corresponding to
the tape.
The buffer must be at least CA_MAXDGNLEN+1 characters long.
.TP
.I density
points at a buffer to receive the alphanumeric density corresponding to
the tape.
The buffer must be at least CA_MAXDENLEN+1 characters long.
.TP
.I lbltype
points at a buffer to receive the label type corresponding to the tape.
The buffer must be at least CA_MAXLBLTYPLEN+1 characters long.
.TP
.I model
points at a buffer to receive the model of cartridge.
The buffer must be at least CA_MAXMODELLEN+1 characters long.
.TP
.I estimated_free_space
receives the estimated free space on this volume. It can be smaller than
.I Size
if no tape has enough free space.
.LP
Only
.I vid
and
.I Size
are mandatory. When a pointer to a field is NULL, the field value is not returned.
.LP
If the tape belongs to a private pool, the requester must match the ownership or
have ADMIN privilege in the Cupv database.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOENT
The named pool does not exist.
.TP
.B EACCES
The tape belongs to a private pool and the requester does not match the
ownership and does not have ADMIN privilege in Cupv database.
.TP
.B EFAULT
.I vid
is a NULL pointer.
.TP
.B EINVAL
The length of
.I poolname
exceeds
.B CA_MAXPOOLNAMELEN
or
.I Size
is <= 0.
.TP
.B ENOSPC
No volume with enough free space.
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.TP
.B EVMGRNACT
Volume manager is not running or is being shutdown.
.SH SEE ALSO
.BR Castor_limits(4) ,
.B vmgr_updatetape(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
