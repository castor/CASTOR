.\" Copyright (C) 2001-2002 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH VMGRDELETEPOOL "1castor" "$Date: 2002/08/23 12:43:05 $" CASTOR "vmgr Administrator Commands"
.SH NAME
vmgrdeletepool \- remove the definition of a pool of tapes from the CASTOR Volume Manager
.SH SYNOPSIS
.B vmgrdeletepool
.BI --name " pool_name"
.SH DESCRIPTION
.B vmgrdeletepool
removes the definition of a pool of tapes from the CASTOR Volume Manager.
.TP
.I pool_name
is the name of tape pool.
It must be at most CA_MAXPOOLNAMELEN characters long.
.LP
This command requires ADMIN privilege in the Cupv database.
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR Cupvlist(1) ,
.BR vmgr_deletepool(3) ,
.B vmgrenterpool(1)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
