.\" Copyright (C) 1999-2003 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH VMGR_MODIFYTAPE "3castor" "$Date: 2003/10/13 12:34:36 $" CASTOR "vmgr Library Functions"
.SH NAME
vmgr_modifytape \- modify an existing tape in the CASTOR Volume Manager
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "vmgr_api.h"\fR
.sp
.BI "int vmgr_modifytape (const char *" vid ,
.BI "char *" vsn ,
.BI "char *" library ,
.BI "char *" density ,
.BI "char *" lbltype ,
.BI "char *" manufacturer ,
.BI "char *" sn ,
.BI "char *" poolname ,
.BI "int " status )
.SH DESCRIPTION
.B vmgr_modifytape
modifies an existing tape in the CASTOR Volume Manager.
.TP
.I vid
is the volume visual identifier.
It must be at most CA_MAXVIDLEN characters long.
.TP
.I vsn
specifies the magnetically recorded volume serial number of the tape.
It must be at most six characters long.
.TP
.I library
is the name of the library.
It must be at most CA_MAXTAPELIBLEN characters long.
The list of libraries can be obtained with
.BR vmgrlistlibrary .
.TP
.I density
The list of supported alphanumeric densities can be obtained with
.BR vmgrlistdenmap .
They are for example:
.BR 35G ,
.BR 35GC ,
.B 40G
or
.B 40GC
for DLT cartridges,
.B 10G
or
.B 10GC
for IBM 3590 cartridges,
.B 40G
or
.B 40GC
for IBM 3590E cartridges,
.B 20G
or
.B 20GC
for STK 9840 cassettes,
.B 60G
or
.B 60GC
for STK 9940 cartridges,
.B 100G
or
.B 100GC
for LTO cartridges.
.TP
.I lbltype
may be
.BR al ,
.BR aul ,
.B nl
or
.BR sl .
.TP
.I manufacturer
is an alphanumeric field CA_MAXMANUFLEN bytes long.
.TP
.I sn
The cartridge serial number is an alphanumeric field CA_MAXSNLEN bytes long.
.TP
.I poolname
is the tape pool name. It must be at most CA_MAXPOOLNAMELEN characters long.
The list of pools can be obtained with
.BR vmgrlistpool .
.TP
.I status
can be set to 0 or a combination of
.BR TAPE_FULL ,
.BR ARCHIVED ,
.BR DISABLED ,
.BR EXPORTED
and
.BR TAPE_RDONLY .
.LP
Only
.I vid
is mandatory. When a pointer to a field is NULL or a numeric value is negative,
the field is not modified.
.LP
This function requires TP_OPER privilege in the Cupv database.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOENT
The named volume does not exist.
.TP
.B EACCES
The requestor is not super-user.
.TP
.B EFAULT
.I vid
is a NULL pointer.
.TP
.B EEXIST
The named volume contains files.
.TP
.B EINVAL
The length of
.I vid
exceeds
.B CA_MAXVIDLEN
or the library/density/poolname does not exist.
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.TP
.B EVMGRNACT
Volume manager is not running or is being shutdown.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR Cupvlist(1) ,
.B vmgr_entertape(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
