.\" Copyright (C) 1999-2002 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH VMGRLISTTAPE "1castor" "$Date: 2008/02/28 14:21:09 $" CASTOR "vmgr Administrator Commands"
.SH NAME
vmgrlisttape \- query the CASTOR Volume Manager about a tape or list all existing tapes
.SH SYNOPSIS
.B vmgrlisttape
[
.BI -P " pool_name"
] [
.BI -V " vid"
] [
.B -x
] [
.B -s
]
.SH DESCRIPTION
.B vmgrlisttape
queries the CASTOR Volume Manager about a given tape or lists all existing tapes
in tabular form:
.HP 1.2i
vid		volume visual identifier.
.HP
side		optional side number.
.HP
vsn		magnetically recorded volume serial number of the tape.
.HP
library		name of the tape library.
.HP
density		alphanumeric density.
.HP
lbltype		label type.
.HP
model		model of cartridge.
.HP
media_letter	media identification letter. For example
"A", "B" or "C" for SD3 (Redwood), "J" for IBM 3590, "R" for STK 9840.
.HP
manufacturer
.HP
sn		cartridge serial number.
.HP
poolname
.HP
etime		indicates the time the volume was entered in the Volume Manager.
.HP
free_space
.HP
nbfiles		number of files written on the tape.
.HP
rcount		number of times the volume has been mounted for read.
.HP
wcount		number of times the volume has been mounted for write.
.HP
rhost		indicates the last tape server where the tape was mounted for read.
.HP
whost		indicates the last tape server where the tape was mounted for write.
.HP
rjid		is the job id of the last read request.
.HP
wjid		is the job id of the last write request.
.HP
rtime		indicates the last time the volume was mounted for read.
.HP
wtime		indicates the last time the volume was mounted for write.
.HP
status		can be
.BR FULL ,
.BR BUSY ,
.BR RDONLY ,
.BR ARCHIVED ,
.B EXPORTED
or
.BR DISABLED .
.SH OPTIONS
.TP
.BI \-P " pool_name"
restricts the list to the tapes belonging to this pool.
.TP
.BI \-V " vid"
restricts the list to the tape having this volume visual identifier.
It must be at most CA_MAXVIDLEN characters long.
.TP
.B \-x
extended format.
If this option is not set, only vid, vsn, library, density, lbltype, poolname,
free_space, last access (read or write) and status are given.
.TP
.B \-s
display the output in a simplified format more suited to parsing by scripts.
.SH EXAMPLES
.nf
.ft CW
vmgrlisttape -V RT0001
RT0001   RT0001 STK_ACS3 20GC     al  default          20.00GB 00000000

vmgrlisttape -V RT0003 -x
RT0003   RT0003 STK_ACS3 20GC     al  9840   R  STK          123456789 \\
         default         20010101  17.14GB    237     4     1 tpsrv001 \\
         tpsrv002        25453      39123 20010130 20001211 DISABLED
.ft
.fi
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR vmgr_entertape(3) ,
.BR vmgrentertape(1) ,
.BR vmgr_listtape(3) ,
.BR vmgr_querytape(3) ,
.BR nslisttape(1) ,
.BR nslistsegs(1)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
