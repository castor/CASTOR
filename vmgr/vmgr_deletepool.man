.\" Copyright (C) 1999-2002 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH VMGR_DELETEPOOL "3castor" "$Date: 2002/08/23 12:43:02 $" CASTOR "vmgr Library Functions"
.SH NAME
vmgr_deletepool \- remove the definition of a pool of tapes from the CASTOR Volume Manager
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "vmgr_api.h"\fR
.sp
.BI "int vmgr_deletepool (const char *" pool_name )
.SH DESCRIPTION
.B vmgr_deletepool
removes the definition of a pool of tapes from the CASTOR Volume Manager.
.TP
.I pool_name
is the name of tape pool.
It must be at most CA_MAXPOOLNAMELEN characters long.
.LP
This function requires ADMIN privilege in the Cupv database.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOENT
The named pool does not exist.
.TP
.B EFAULT
.I pool_name
is a NULL pointer.
.TP
.B EEXIST
There are tapes in this pool.
.TP
.B EINVAL
The length of
.I pool_name
exceeds CA_MAXPOOLNAMELEN.
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.TP
.B EVMGRNACT
Volume manager is not running or is being shutdown.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR Cupvlist(1) ,
.B vmgr_enterpool(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
