.\" Copyright (C) 1999-2003 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH VMGR_QRYTAPEBLKSZ "3castor" "$Date: 2003/11/17 06:46:30 $" CASTOR "vmgr Library Functions"
.SH NAME
vmgr_qrytapeblksz \- query the CASTOR Volume Manager about a tape
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "vmgr_api.h"\fR
.sp
.BI "int vmgr_qrytapeblksz (const char *const "vid ,
.BI "const int " side ,
.BI "struct vmgr_tape_info_byte_u64_with_blksz *const "tape_info ,
.BI "char *const "dgn )
.SH DESCRIPTION
.B vmgr_qrytapeblksz
queries the CASTOR Volume Manager about a tape which includes return the
blocksize in bytes to be used when transfering data files to and from the tape.
.TP
.I vid
is the volume visual identifier.
It must be at most CA_MAXVIDLEN characters long.
.TP
.I tape_info
point at a data structure to be used to receive the description of the tape.
.TP
.I dgn
points at a character buffer to be used to receive the device group name
corresponding to the tape.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOENT
The named volume does not exist.
.TP
.B EFAULT
.I vid
or
.I tape_info
is a NULL pointer.
.TP
.B EINVAL
The length of
.I vid
exceeds
.BR CA_MAXVIDLEN .
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.TP
.B EVMGRNACT
Volume manager is not running or is being shutdown.
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
