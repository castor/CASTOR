.\" Copyright (C) 1999-2003 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH VMGR_ENTERMODEL "3castor" "$Date: 2003/10/29 07:48:59 $" CASTOR "vmgr Library Functions"
.SH NAME
vmgr_entermodel \- enter a new model of cartridge in the CASTOR Volume Manager
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "vmgr_api.h"\fR
.sp
.BI "int vmgr_entermodel (const char *" model ,
.BI "char *" media_letter ,
.BI "int " media_cost )
.SH DESCRIPTION
.B vmgr_entermodel
enters a new model of cartridge in the CASTOR Volume Manager.
.TP
.I model
is the model of cartridge.
It must be at most CA_MAXMODELLEN characters long.
.TP
.I media_letter
is the media identification letter. For example "A", "B" or "C" for SD3 (Redwood
),
"J" for IBM 3590, "R" for STK 9840.
.TP
.I media_cost
in Swiss Francs.
.LP
This function requires ADMIN privilege in the Cupv database.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B EFAULT
.I model
is a NULL pointer.
.TP
.B EEXIST
The named model exists already.
.TP
.B EINVAL
The length of
.I model
exceeds
.BR CA_MAXMODELLEN .
.TP
.B ENOSPC
The volume manager database is full.
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.TP
.B EVMGRNACT
Volume manager is not running or is being shutdown.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR Cupvlist(1) ,
.BR vmgr_deletemodel(3) ,
.B vmgr_modifymodel(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
