.\" Copyright (C) 2000-2002 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH VMGR_DELETEDENMAP "3castor" "$Date: 2002/08/23 12:43:01 $" CASTOR "vmgr Library Functions"
.SH NAME
vmgr_deletedenmap \- delete a triplet model/media_letter/density in the CASTOR
Volume Manager
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "vmgr_api.h"\fR
.sp
.BI "int vmgr_deletedenmap (const char *" model ,
.BI "char *" media_letter ,
.BI "char *" density )
.SH DESCRIPTION
.B vmgr_deletedenmap
deletes a triplet model/media_letter/density in the CASTOR Volume Manager.
.TP
.I model
is the model of cartridge.
It must be at most CA_MAXMODELLEN characters long.
.TP
.I media_letter
is the media identification letter. For example "A", "B" or "C" for SD3 (Redwood),
"J" for IBM 3590, "R" for STK 9840.
.TP
.I density
is alphanumeric.
.LP
This function requires ADMIN privilege in the Cupv database.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOENT
The named triplet does not exist.
.TP
.B EFAULT
.I model
or
.I density
is a NULL pointer.
.TP
.B EINVAL
The length of
.I model
exceeds
.B CA_MAXMODELLEN
or the length of
.I density
exceeds
.BR CA_MAXDENLEN .
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.TP
.B EVMGRNACT
Volume manager is not running or is being shutdown.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR Cupvlist(1) ,
.B vmgr_enterdenmap(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
