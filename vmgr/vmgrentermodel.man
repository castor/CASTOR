.\" Copyright (C) 2000-2003 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH VMGRENTERMODEL "1castor" "$Date: 2003/10/29 07:48:59 $" CASTOR "vmgr Administrator Commands"
.SH NAME
vmgrentermodel \- enter a new model of cartridge in the CASTOR Volume Manager
.SH SYNOPSIS
.B vmgrentermodel
[
.BI --mc " media_cost"
] [
.BI --ml " media_letter"
]
.BI --mo " model"
.SH DESCRIPTION
.B vmgrentermodel
enters a new model of cartridge in the CASTOR Volume Manager.
.TP
.I media_cost
in Swiss Francs.
.TP
.I media_letter
is the media identification letter. For example "A", "B" or "C" for SD3 (Redwood
),
"J" for IBM 3590, "R" for STK 9840.
.TP
.I model
is the model of cartridge.
It must be at most CA_MAXMODELLEN characters long.
.LP
This command requires ADMIN privilege in the Cupv database.
.SH EXAMPLES
.nf
.ft CW
vmgrentermodel --mo SD3 --ml C --mc 48
vmgrentermodel --mo 3590 --ml J --mc 45
vmgrentermodel --mo DLT --mc 51
.ft
.fi
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR Cupvlist(1) ,
.B vmgr_entermodel(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
