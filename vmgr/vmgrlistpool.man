.\" Copyright (C) 2000 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH VMGRLISTPOOL "1castor" "$Date: 2008/02/28 14:21:09 $" CASTOR "vmgr Administrator Commands"
.SH NAME
vmgrlistpool \- query the CASTOR Volume Manager about a given pool or list all existing tape pools
.SH SYNOPSIS
.B vmgrlistpool
[
.BI -P "  pool_name"
] [
.B -s
]
.SH DESCRIPTION
.B vmgrlistpool
queries the CASTOR Volume Manager about a given pool or lists all existing tape
pools in tabular form:
.HP 1.2i
poolname		name of the tape pool.
.HP
pool_user		the pool is restricted to this user.
.HP
pool_group	the pool is restricted to this group.
.HP
capacity		the native pool capacity.
.HP
free_space	estimated free space in the pool.
.SH OPTIONS
.TP
.I pool_name
is the name of the tape pool.
It must be at most CA_MAXPOOLNAMELEN characters long.
.TP
.I s
display the output in a simplified format more suited to parsing by scripts.
.SH EXAMPLES
.nf
.ft CW
vmgrlistpool -P alicemdc
alicemdc        alicemdc z2     CAPACITY    1.22TB FREE  887.39GB ( 71.0%)

vmgrlistpool -P default
default         -        -      CAPACITY    3.74TB FREE    3.54TB ( 94.6%)
.ft
.fi
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR vmgr_enterpool(3) ,
.BR vmgrenterpool(1) ,
.B vmgr_querypool(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
