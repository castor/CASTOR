.\" Copyright (C) 2000-2003 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH VMGR_ENTERDENMAP_BYTE_U64 "3castor" "$Date: 2003/10/29 07:48:58 $" CASTOR "vmgr Library Functions"
.SH NAME
vmgr_enterdenmap_byte_u64 \- enter a new quadruplet model/media_letter/density/capacity
in the CASTOR Volume Manager
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "vmgr_api.h"\fR
.sp
.BI "int vmgr_enterdenmap_byte_u64 (const char *" model ,
.BI "char *" media_letter ,
.BI "char *" density ,
.BI "us_signed64 " native_capacity_byte_u64 )
.SH DESCRIPTION
.B vmgr_enterdenmap_byte_u64
enters a new quadruplet model/media_letter/density/capacity in the CASTOR Volume
Manager.
.TP
.I model
is the model of cartridge.
It must be at most CA_MAXMODELLEN characters long.
.TP
.I media_letter
is the media identification letter. For example "A", "B" or "C" for SD3 (Redwood
),
"J" for IBM 3590, "R" for STK 9840.
.TP
.I density
is alphanumeric. It must be at most CA_MAXDENLEN characters long.
For example:
.br
.B 10G
or
.B 10GC
for IBM 3590 cartridges,
.BR 10G ,
.BR 10GC ,
.BR 25G ,
.BR 25GC ,
.B 50G
or
.B 50GC
for Redwood cartridges,
.B 20G
or
.B 20GC
for STK 9840 cassettes.
.TP
.I native_capacity_byte_u64
in Mbytes.
For a multi-sided media like DVD, the native capacity corresponds to one side.
.LP
This function requires ADMIN privilege in the Cupv database.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B EFAULT
.I model
or
.I density
is a NULL pointer.
.TP
.B EEXIST
The named quadruplet exists already.
.TP
.B EINVAL
The length of
.I model
exceeds
.BR CA_MAXMODELLEN ,
the length of
.I density
exceeds
.BR CA_MAXDENLEN ,
or
.I native_capacity_byte_u64
<= 0.
.TP
.B ENOSPC
The volume manager database is full.
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.TP
.B EVMGRNACT
Volume manager is not running or is being shutdown.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR Cupvlist(1) ,
.B vmgr_deletedenmap(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
