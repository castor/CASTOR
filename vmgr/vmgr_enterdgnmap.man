.\" Copyright (C) 2001-2002 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH VMGR_ENTERDGNMAP "3castor" "$Date: 2002/08/23 12:43:03 $" CASTOR "vmgr Library Functions"
.SH NAME
vmgr_enterdgnmap \- enter a new triplet dgn/model/library in the CASTOR Volume Manager
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "vmgr_api.h"\fR
.sp
.BI "int vmgr_enterdgnmap (const char *" dgn ,
.BI "char *" model ,
.BI "char *" library )
.SH DESCRIPTION
.B vmgr_enterdgnmap
enters a new triplet dgn/model/library in the CASTOR Volume Manager.
.TP
.I dgn
is the device group name associated with the pair model/library.
It must be at most CA_MAXDGNLEN characters long.
.TP
.I library
is the name of the library.
It must be at most CA_MAXTAPELIBLEN characters long.
.TP
.I model
is the model of cartridge.
It must be at most CA_MAXMODELLEN characters long.
.LP
This function requires ADMIN privilege in the Cupv database.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B EFAULT
.IR dgn ,
.I model
or
.I library
is a NULL pointer.
.TP
.B EEXIST
The named triplet exists already.
.TP
.B EINVAL
The length of
.I dgn
exceeds
.B CA_MAXDGNLEN
or the length of
.I model
exceeds
.B CA_MAXMODELLEN
or the length of
.I library
exceeds
.BR CA_MAXTAPELIBLEN .
.TP
.B ENOSPC
The volume manager database is full.
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.TP
.B EVMGRNACT
Volume manager is not running or is being shutdown.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR Cupvlist(1) ,
.B vmgr_deletedgnmap(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
