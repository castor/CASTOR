.\" Copyright (C) 2000-2003 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH VMGR_LISTDENMAP_BYTE_U64 "3castor" "$Date: 2003/10/29 07:48:59 $" CASTOR "vmgr Library Functions"
.SH NAME
vmgr_listdenmap_byte_u64 \- list all existing quadruplets model/media_letter/density/capacity
in the CASTOR Volume Manager
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "vmgr_api.h"\fR
.sp
.BI "struct vmgr_tape_denmap_byte_u64 *vmgr_listdenmap_byte_u64 (int " flags ,
.BI "vmgr_list *" listp )
.SH DESCRIPTION
.B vmgr_listdenmap_byte_u64
lists all existing quadruplets model/media_letter/density/capacity in the CASTOR
Volume Manager.
.TP
.I flags
may be one of the following constants:
.RS
.TP
.B VMGR_LIST_BEGIN
the first call must have this flag set to allocate buffers and
initialize pointers.
.TP
.B VMGR_LIST_CONTINUE
all the following calls must have this flag set.
.TP
.B VMGR_LIST_END
final call to terminate the list and free resources.
.RE
.SH RETURN VALUE
This routine returns a pointer to a structure containing the current quadruplet
if the operation was successful or NULL if all quadruplets have been returned
or if the operation failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOMEM
memory could not be allocated for the output buffer.
.TP
.B EFAULT
.I listp
is a NULL pointer.
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.TP
.B EVMGRNACT
Volume manager is not running or is being shutdown.
.SH SEE ALSO
.BR vmgr_enterdenmap_byte_u64(3) ,
.B vmgrlistdenmap(1)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
