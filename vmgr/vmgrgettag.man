.\" Copyright (C) 2003 by CERN/IT/GD/CT
.\" All rights reserved
.\"
.TH VMGRGETTAG "1castor" "$Date: 2003/10/28 11:13:26 $" CASTOR "vmgr User Commands"
.SH NAME
vmgrgettag \- get the tag associated with a tape volume in the CASTOR Volume Manager
.SH SYNOPSIS
.B vmgrgettag
.B -V
.I vid
.SH DESCRIPTION
.B vmgrgettag
gets the tag associated with a tape volume in the CASTOR Volume Manager.
.TP
.I vid
is the volume visual identifier.
It must be at most CA_MAXVIDLEN characters long.
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH SEE ALSO
.BR Castor_limits(4) ,
.B vmgr_gettag(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
