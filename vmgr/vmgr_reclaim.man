.\" Copyright (C) 2000-2002 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH VMGR_RECLAIM "3castor" "$Date: 2002/08/23 12:15:47 $" CASTOR "vmgr Library Functions"
.SH NAME
vmgr_reclaim \- reset tape volume content information in the CASTOR Volume Manager
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "vmgr_api.h"\fR
.sp
.BI "int vmgr_reclaim (const char *" vid )
.SH DESCRIPTION
.B vmgr_reclaim
resets the tape volume content information (number of files, free space and
status) in the CASTOR Volume Manager.
.TP
.I vid
is the volume visual identifier of the tape.
It must be at most CA_MAXVIDLEN characters long.
.LP
This function requires ADMIN privilege in the Cupv database.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOENT
The named volume does not exist.
.TP
.B EACCES
The requestor does not have the necessary privilege.
.TP
.B EFAULT
.I vid
is a NULL pointer.
.TP
.B EINVAL
The length of
.I vid
exceeds
.B CA_MAXVIDLEN
or bad parameter value.
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.TP
.B EVMGRNACT
Volume manager is not running or is being shutdown.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR Cupvlist(1) ,
.BR reclaim(1) ,
.B vmgr_querytape(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
