.\" Copyright (C) 2000-2003 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH VMGR_ENTERDENMAP "3castor" "$Date: 2003/10/29 07:48:58 $" CASTOR "vmgr Library Functions"
.SH NAME
vmgr_enterdenmap \- enter a new quadruplet model/media_letter/density/capacity
in the CASTOR Volume Manager
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "vmgr_api.h"\fR
.sp
.BI "int vmgr_enterdenmap (const char *" model ,
.BI "char *" media_letter ,
.BI "char *" density ,
.BI "int " native_capacity )
.SH DESCRIPTION
.B vmgr_enterdenmap
enters a new quadruplet model/media_letter/density/capacity in the CASTOR Volume
Manager.
.P
Please note that users of the vmgr client API are encouraged to use the
.B vmgr_enterdenmap_byte_u64
function instead of
.B vmgr_enterdenmap.
The former defines native capacity to be in bytes and represented by a 64-bit
unsigned integer.  The
.B vmgr_enterdenmap_byte_u64
function has been introduced to help represent all CASTOR sizes in the same
way.
.TP
.I model
is the model of cartridge.
It must be at most CA_MAXMODELLEN characters long.
.TP
.I media_letter
is the media identification letter. For example "A", "B" or "C" for SD3 (Redwood
),
"J" for IBM 3590, "R" for STK 9840.
.TP
.I density
is alphanumeric. It must be at most CA_MAXDENLEN characters long.
For example:
.br
.B 10G
or
.B 10GC
for IBM 3590 cartridges,
.BR 10G ,
.BR 10GC ,
.BR 25G ,
.BR 25GC ,
.B 50G
or
.B 50GC
for Redwood cartridges,
.B 20G
or
.B 20GC
for STK 9840 cassettes.
.TP
.I native_capacity
in Mbytes.
For a multi-sided media like DVD, the native capacity corresponds to one side.
.LP
This function requires ADMIN privilege in the Cupv database.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B EFAULT
.I model
or
.I density
is a NULL pointer.
.TP
.B EEXIST
The named quadruplet exists already.
.TP
.B EINVAL
The length of
.I model
exceeds
.BR CA_MAXMODELLEN ,
the length of
.I density
exceeds
.BR CA_MAXDENLEN ,
or native_capacity <= 0.
.TP
.B ENOSPC
The volume manager database is full.
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.TP
.B EVMGRNACT
Volume manager is not running or is being shutdown.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR Cupvlist(1) ,
.B vmgr_deletedenmap(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
