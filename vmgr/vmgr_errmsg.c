/*
 * Copyright (C) 1999-2003 by CERN/IT/PDP/DM
 * All rights reserved
 */

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <sys/types.h>
#include "vmgr.h"
#include "vmgr_api.h"

/*	vmgr_seterrbuf - set receiving buffer for error messages */

int vmgr_seterrbuf(char *buffer, int buflen)
{
  struct vmgr_api_thread_info *thip;

  if (vmgr_apiinit (&thip))
    return (-1);
  thip->errbufp = buffer;
  thip->errbuflen = buflen;
  return (0);
}

/* vmgr_errmsg - send error message to user defined client buffer or to stderr */

int vmgr_errmsg(char *func, char *msg, ...)
{
  va_list args;
  char prtbuf[PRTBUFSZ];
  int save_errno;
  struct vmgr_api_thread_info *thip;

  save_errno = errno;
  if (vmgr_apiinit (&thip))
    return (-1);
  va_start (args, msg);
  if (func)
    sprintf (prtbuf, "%s: ", func);
  else
    *prtbuf = '\0';
  vsprintf (prtbuf + strlen(prtbuf), msg, args);
  va_end (args);
  if (thip->errbufp) {
    if ((int)strlen (prtbuf) < thip->errbuflen) {
      strcpy (thip->errbufp, prtbuf);
    } else {
      strncpy (thip->errbufp, prtbuf, thip->errbuflen - 2);
      thip->errbufp[thip->errbuflen-2] = '\n';
      thip->errbufp[thip->errbuflen-1] = '\0';
    }
  } else {
    fprintf (stderr, "%s", prtbuf);
  }
  errno = save_errno;
  return (0);
}
