.\" Copyright (C) 2000-2003 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH VMGR "1castor" "$Date: 2003/11/24 06:44:14 $" CASTOR "vmgr Commands"
.SH DESCRIPTION
A vmgr daemon manages the tape pool space.
A tape pool consists in a set of cartridges.
It can be private to an experiment or public.
Use
.B man
command for details of the commands recognized by the vmgr:
.TP 1.5i
vmgrdeletepool
remove the definition of a pool of tapes from the CASTOR Volume Manager Database (admin only)
.TP
vmgrdeletetape
remove a tape from the CASTOR Volume Manager Database (admin only)
.TP
vmgrdeltag
delete the tag associated with a tape volume in the CASTOR Volume Manager
.TP
vmgrenterdenmap
enter a new triplet model/media_letter/density in the CASTOR Volume Manager Database (admin only)
.TP
vmgrenterdgnmap
enter a new triplet dgn/model/library in the CASTOR Volume Manager (admin only)
.TP
vmgrenterlibrary
define a new tape library in the CASTOR Volume Manager (admin only)
.TP
vmgrentermodel
enter a new model of cartridge in the CASTOR Volume Manager Database (admin only)
.TP
vmgrenterpool
define a new tape pool (admin only)
.TP
vmgrentertape
enter a new tape in the CASTOR Volume Manager Database (admin or operator only)
.TP
vmgrgettag
get the tag associated with a tape volume in the CASTOR Volume Manager
.TP
vmgrlistdenmap
list all existing triplets model/media_letter/density
.TP
vmgrlistdgnmap
list all existing triplets dgn/model/library in the CASTOR Volume Manager
.TP
vmgrlistlibrary
query the CASTOR Volume Manager about a given library or list all existing tape libraries
.TP
vmgrlistmodel
query the CASTOR Volume Manager about a given model or list all existing cartridge models
.TP
vmgrlistpool
query the CASTOR Volume Manager about a given tape pool or list all existing pools
.TP
vmgrlisttape
query the CASTOR Volume Manager about a tape or list all existing tapes
.TP
vmgrmodifylibrary
modify the definition of an existing tape library in the CASTOR Volume Manager (admin only)
.TP
vmgrmodifypool
modify the definition of an existing tape pool in the CASTOR Volume Manager (admin only)
.TP
vmgrmodifytape
modify an existing tape volume entry in the CASTOR Volume Manager (admin or operator only)
.TP
vmgrsettag
add/replace a tag associated with a tape volume in the CASTOR Volume Manager
.TP
vmgrshutdown
shutdown the CASTOR Volume Manager (admin only)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
