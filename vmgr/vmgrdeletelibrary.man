.\" Copyright (C) 2002 by CERN/IT/DS/HSM
.\" All rights reserved
.\"
.TH VMGRDELETELIBRARY "1castor" "$Date: 2002/08/23 12:43:04 $" CASTOR "vmgr Administrator Commands"
.SH NAME
vmgrdeletelibrary \- remove the definition of a tape library from the CASTOR Volume Manager
.SH SYNOPSIS
.B vmgrdeletelibrary
.BI --name " library_name"
.SH DESCRIPTION
.B vmgrdeletelibrary
removes the definition of a tape library from the CASTOR Volume Manager.
.TP
.I library_name
is the name of the tape library.
It must be at most CA_MAXTAPELIBLEN characters long.
.LP
This command requires ADMIN privilege in the Cupv database.
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR Cupvlist(1) ,
.BR vmgr_deletelibrary(3) ,
.B vmgrenterlibrary(1)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
