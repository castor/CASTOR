.\" Copyright (C) 1999-2002 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH VMGR_DELETETAPE "3castor" "$Date: 2002/10/16 12:00:55 $" CASTOR "vmgr Library Functions"
.SH NAME
vmgr_deletetape \- remove a tape from the CASTOR Volume Manager
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "vmgr_api.h"\fR
.sp
.BI "int vmgr_deletetape (const char *" vid )
.SH DESCRIPTION
.B vmgr_deletetape
removes a tape from the CASTOR Volume Manager.
.TP
.I vid
is the volume visual identifier.
It must be at most CA_MAXVIDLEN characters long.
.LP
This function requires ADMIN privilege in the Cupv database.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOENT
The named volume does not exist.
.TP
.B EACCES
The effective user ID of the requestor does not match the owner ID of the tape
pool this tape belongs to and the requestor is not super-user.
.TP
.B EFAULT
.I vid
is a NULL pointer.
.TP
.B EEXIST
The named volume still contains files.
.TP
.B EINVAL
The length of
.I vid
exceeds
.BR CA_MAXVIDLEN .
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.TP
.B EVMGRNACT
Volume manager is not running or is being shutdown.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR Cupvlist(1) ,
.B vmgr_entertape(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
