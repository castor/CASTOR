.\" Copyright (C) 2000-2003 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH VMGRLISTDENMAP "1castor" "$Date: 2003/11/24 06:59:49 $" CASTOR "vmgr Administrator Commands"
.SH NAME
vmgrlistdenmap \- list all existing quadruplets model/media_letter/density/capacity
in the CASTOR Volume Manager
.SH SYNOPSIS
.B vmgrlistdenmap
.SH DESCRIPTION
.B vmgrlistdenmap
lists all existing quadruplets model/media_letter/density/capacity in the CASTOR
Volume Manager in tabular form:
.HP 1.2i
model		model of cartridge.
.HP
media_letter	media identification letter.
.HP
density		alphanumeric density.
.HP
capacity		native capacity (without compression).
For a multi-sided media like DVD, the native capacity corresponds to one side.
.SH EXAMPLES
.nf
.ft CW
vmgrlistdenmap
3480     38000    200.00M
3480     38KC     200.00M
3490   E 38KD     800.00M
3490   E 38KDC    800.00M
3590   J 10G       10.00G
3590   J 10GC      10.00G
3590   J 20G       20.00G
3590   J 20GC      20.00G
3590E  K 20G       20.00G
3590E  K 20GC      20.00G
3590E  K 40G       40.00G
3590E  K 40GC      40.00G
8200     8200       2.00G
8200     8200C      2.00G
8200     8500       5.00G
8200     8500C      5.00G
9840   R 20G       20.00G
9840   R 20GC      20.00G
9940   P 60G       60.00G
9940   P 60GC      60.00G
9940   P 200G     200.00G
9940   P 200GC    200.00G
CT3    D 10G       10.00G
CT3    D 10GC      10.00G
CT4    D 20G       20.00G
CT4    D 20GC      20.00G
CT4    D 35G       35.00G
CT4    D 35GC      35.00G
CT4    D 40G       40.00G
CT4    D 40GC      40.00G
DAT120   DDS        4.00G
DAT120   DDSC       4.00G
DAT60    DDS        1.30G
DAT60    DDSC       1.30G
DAT90    DDS        2.00G
DAT90    DDSC       2.00G
DVD      FMT        4.70G
LTO1   L 100G     100.00G
LTO1   L 100GC    100.00G
LTO2   L 200G     200.00G
LTO2   L 200GC    200.00G
SD1M     RAW       43.00G
SD1L     RAW       96.00G
SD3S   A 10G       10.00G
SD3S   A 10GC      10.00G
SD3M   B 25G       25.00G
SD3M   B 25GC      25.00G
SD3L   C 50G       50.00G
SD3L   C 50GC      50.00G
SDLT   S 110G     110.00G
SDLT   S 110GC    110.00G
.ft
.fi
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH SEE ALSO
.BR vmgr_enterdenmap(3) ,
.B vmgrenterdenmap(1)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
