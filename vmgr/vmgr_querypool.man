.\" Copyright (C) 2000 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH VMGR_QUERYPOOL "3castor" "$Date: 2001/09/26 09:13:57 $" CASTOR "vmgr Library Functions"
.SH NAME
vmgr_querypool \- query about a tape pool in the CASTOR Volume Manager
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "vmgr_api.h"\fR
.sp
.BI "int vmgr_querypool (const char *" pool_name ,
.BI "uid_t *" pool_user ,
.BI "gid_t *" pool_group ,
.BI "u_signed64 *" capacity ,
.BI "u_signed64 *" tot_free_space )
.SH DESCRIPTION
.B vmgr_querypool
queries about a tape pool in the CASTOR Volume Manager.
.TP
.I pool_name
is the name of the tape pool.
It must be at most CA_MAXPOOLNAMELEN characters long.
.TP
.I pool_user
if the value returned is greater than zero, the tape pool is restricted to this
user uid.
.TP
.I pool_group
if the value returned is greater than zero, the tape pool is restricted to this
group gid.
.TP
.I capacity
in bytes.
.TP
.I tot_free_space
in bytes.
.LP
Only
.I pool_name
is mandatory. When a pointer to a field is NULL, the field value is not returned.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOENT
The named pool does not exist.
.TP
.B EFAULT
.I pool_name
is a NULL pointer.
.TP
.B EINVAL
The length of
.I pool_name
exceeds
.BR CA_MAXPOOLNAMELEN .
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.TP
.B EVMGRNACT
Volume manager is not running or is being shutdown.
.SH SEE ALSO
.BR Castor_limits(4) ,
.B vmgr_enterpool(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
