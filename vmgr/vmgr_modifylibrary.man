.\" Copyright (C) 2001-2002 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH VMGR_MODIFYLIBRARY "3castor" "$Date: 2002/08/23 12:43:03 $" CASTOR "vmgr Library Functions"
.SH NAME
vmgr_modifylibrary \- modify the definition of an existing tape library in the CASTOR Volume Manager
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "vmgr_api.h"\fR
.sp
.BI "int vmgr_modifylibrary (const char *" library_name ,
.BI "int " capacity ,
.BI "int " status )
.SH DESCRIPTION
.B vmgr_modifylibrary
modifies the definition of an existing tape library in the CASTOR Volume Manager.
.TP
.I library_name
is the name of the tape library.
It must be at most CA_MAXTAPELIBLEN characters long.
.TP
.I capacity
is the new total number of slots.
.LP
Only
.I library_name
is mandatory. When a pointer to an alphanumeric field is NULL or a numeric field
value is negative or zero, the field is not modified.
.LP
This function requires ADMIN privilege in the Cupv database.
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOENT
The named library does not exist.
.TP
.B EFAULT
.I library_name
is a NULL pointer.
.TP
.B EINVAL
The length of
.I library_name
exceeds
.BR CA_MAXTAPELIBLEN .
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.TP
.B EVMGRNACT
Volume manager is not running or is being shutdown.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR Cupvlist(1) ,
.B vmgr_enterlibrary(3)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
