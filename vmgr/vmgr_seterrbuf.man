.\" Copyright (C) 1999-2000 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH VMGR_SETERRBUF "3castor" "$Date: 2001/09/26 09:13:57 $" CASTOR "vmgr Library Functions"
.SH NAME
vmgr_seterrbuf \- set receiving buffer for error messages
.SH SYNOPSIS
.BI "int vmgr_seterrbuf (char *" buffer ,
.BI "int " buflen )
.SH DESCRIPTION
.B vmgr_seterrbuf
tells the Volume Manager client API the address and the size of the buffer
to be used for error messages. If this routine is not called, the messages
are printed on
.BR stderr .
.SH RETURN VALUE
This routine returns 0 if the operation was successful or -1 if the operation
failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOMEM
memory could not be allocated for the thread specific information.
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
