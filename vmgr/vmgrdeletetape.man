.\" Copyright (C) 2000-2002 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH VMGRDELETETAPE "1castor" "$Date: 2002/08/23 12:43:05 $" CASTOR "vmgr Administrator Commands"
.SH NAME
vmgrdeletetape \- remove a tape from the CASTOR Volume Manager
.SH SYNOPSIS
.B vmgrdeletetape
.BI -V " vid"
.SH DESCRIPTION
.B vmgrdeletetape
removes a tape from the CASTOR Volume Manager.
.TP
.I vid
is the volume visual identifier.
It must be at most CA_MAXVIDLEN characters long.
.LP
This command requires ADMIN privilege in the Cupv database.
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH SEE ALSO
.BR Castor_limits(4) ,
.BR Cupvlist(1) ,
.BR vmgr_deletetape(3) ,
.B vmgrentertape(1)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
