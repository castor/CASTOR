.\" Copyright (C) 2000 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH VMGR_LISTPOOL "3castor" "$Date: 2001/09/26 09:13:57 $" CASTOR "vmgr Library Functions"
.SH NAME
vmgr_listpool \- list all existing tape pool entries in the CASTOR Volume Manager
.SH SYNOPSIS
.B #include <sys/types.h>
.br
\fB#include "vmgr_api.h"\fR
.sp
.BI "struct vmgr_tape_pool *vmgr_listpool (int " flags ,
.BI "vmgr_list *" listp )
.SH DESCRIPTION
.B vmgr_listpool
lists all existing tape pool entries in the CASTOR Volume Manager.
.TP
.I flags
may be one of the following constants:
.RS
.TP
.B VMGR_LIST_BEGIN
the first call must have this flag set to allocate buffers and
initialize pointers.
.TP
.B VMGR_LIST_CONTINUE
all the following calls must have this flag set.
.TP
.B VMGR_LIST_END
final call to terminate the list and free resources.
.RE
.SH RETURN VALUE
This routine returns a pointer to a structure containing the current pool entry
if the operation was successful or NULL if all entries have been returned
or if the operation failed. In the latter case,
.B serrno
is set appropriately.
.SH ERRORS
.TP 1.3i
.B ENOMEM
memory could not be allocated for the output buffer.
.TP
.B EFAULT
.I listp
is a NULL pointer.
.TP
.B SENOSHOST
Host unknown.
.TP
.B SENOSSERV
Service unknown.
.TP
.B SECOMERR
Communication error.
.TP
.B EVMGRNACT
Volume manager is not running or is being shutdown.
.SH SEE ALSO
.BR vmgr_enterpool(3) ,
.B vmgrlistpool(1)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
