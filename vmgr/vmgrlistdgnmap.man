.\" Copyright (C) 2001 by CERN/IT/PDP/DM
.\" All rights reserved
.\"
.TH VMGRLISTDGNMAP "1castor" "$Date: 2001/09/26 09:13:57 $" CASTOR "vmgr Administrator Commands"
.SH NAME
vmgrlistdgnmap \- list all existing triplets dgn/model/library in the CASTOR Volume Manager
.SH SYNOPSIS
.B vmgrlistdgnmap
.SH DESCRIPTION
.B vmgrlistdgnmap
lists all existing triplets dgn/model/library in the CASTOR Volume
Manager in tabular form:
.HP 1.2i
dgn		device group name.
.HP
model		model of cartridge.
.HP
library		tape library name.
.SH EXAMPLES
.nf
.ft CW
vmgrlistdgnmap
9840R  9840   STK_ACS0
9840R3 9840   STK_ACS3
9940R3 9940   STK_ACS3
9940R4 9940   STK_ACS4
NTRE3  3590E  STK_ACS3
.ft
.fi
.SH EXIT STATUS
This program returns 0 if the operation was successful or >0 if the operation
failed.
.SH SEE ALSO
.BR vmgr_enterdgnmap(3) ,
.B vmgrenterdgnmap(1)
.SH AUTHOR
\fBCASTOR\fP Team <castor.support@cern.ch>
